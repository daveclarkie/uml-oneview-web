﻿
/* ============================================================= */
/*             Password Complexity Function                      */
/* ============================================================= */
/* Main Functions                                                */
/*  testPassword(password[,meterElement="meter"]) [0-9]          */
/* ============================================================= */

function testPassword(passwd,meter){
    if (typeof meter==undefined){meter="meter";}   // set default name for meter even if it doesn't exist
    var P=passwd;
	var origP=getValue(P);
	var scl=50;

    if (origP>scl){origP==scl;}
    if (origP<0){origP==0;}

    var fv=Math.round((origP/scl)*15);
    if (fv>15){fv=15}
    if (fv<0){fv=0}
    
    var bgc="";
    var strength="[0] Do not use";
    var rv=0;
    switch (fv){
        case 1: bgc="#ff0000"; break;   
        case 2: bgc="#ff3000"; break;   
        case 3: bgc="#ff6000"; break;   
        case 4: bgc="#ff8000"; break;   
        case 5: bgc="#ffa000"; break;   
        case 6: bgc="#ffc000"; break;   
        case 7: bgc="#ffe000"; break;   
        case 8: bgc="#ffff00"; strength="[1] Minimal"; rv=1; break;   
        case 9: bgc="#e0ff00"; strength="[2] Weak"; rv=2; break;   
        case 10: bgc="#c0ff00"; strength="[3] Improving"; rv=3; break;   
        case 11: bgc="#a0ff00"; strength="[4] Medium"; rv=4; break;   
        case 12: bgc="#80ff00"; strength="[5] Acceptable"; rv=5; break;   
        case 13: bgc="#60ff00"; strength="[6] Better"; rv=6; break;   
        case 14: bgc="#30ff00"; strength="[7] Good"; rv=7; break;   
        case 15: bgc="#00ff00"; strength="[8+] Recommended"; rv=8;; break;   
        case 16: bgc="#00ff00"; strength="[8+] Recommended"; rv=9;; break;   
        default: bgc="#ff0000"; strength="[0] Do not use"; break;   
    }
    
    chgNamedElementText(meter,chgReplace,strength);
    try {
        var mtr=document.getElementById(meter);
        mtr.style.backgroundColor=bgc;         
    }
    catch(e){
    }            
    
    return rv;
}

function getValue(pwd){
    var iL=0.0;
    var iT=1.0;
    var iLC=0.0;
    var iUC=0.0;
    var iDG=0.0;
    var iSC=0.0;
    var i=0;
    var c="";
    var	p=pwd.split("");
    var rp="";

    for (i=0;i<p.length;i++){
		iL++;
		c=p[i];
		rp+=c;
		if (isLower(c)){iLC++;}
		if (isUpper(c)){iUC++;}
		if (isNumeric(c)){iDG++;}
		if (isSpecial(c)){iSC++;}
	}

    // weighted volume
    if (iLC>0){iT*=(iLC*1)}
    if (iUC>0){iT*=(iUC*1)}
    if (iDG>0){iT*=(iDG*1)}
    if (iSC>0){iT*=(iSC*1)}


	// bonus points for length
	if (iL>3){
		var hmm=checkRuns(pwd);
		hmm+=checkRuns(pwd.toLowerCase())
		hmm+=checkRuns(replaceLeet(pwd))
		//hmm+=checkRuns(replaceNumeric(pwd))
		//hmm+=checkRuns(replaceBoth(pwd));
		hmm/=3;
		hmm/=iL;
        iT*=hmm;
		if (iL>7){iT++;}
		if (iL>16){iT++;}

        //iT=Math.pow(iT*4);
	} else {
    	iT=0;
	}
    // check for sequential values
	return iT;
}

function checkRuns(src){
    var chain = new Array();
    var links = src.split("");
    var chainLength=links.length-2;
    var curLink=0;
    if (chainLength>0) {
        for (link=0;link<chainLength;link++){
            curLink=link;
            var a=links[curLink].charCodeAt(0);
            curLink++;
            var b=links[curLink].charCodeAt(0);
            curLink++;
            var c=links[curLink].charCodeAt(0);
            
            chain[link]=0;
            chain[link]+=(Math.abs(a-b));
            chain[link]+=(Math.abs(b-c));
            if (chain[link]>0){
                chain[link]/=3.0;
            }
            if (chain[link]<1.3){chain[link]=-50}
        }
        var dist=0.0;
        var sz=0.0; 
        chain.sort(function(a,b){return a - b});
        if ((chainLength-2)>1) {
            for (var i=1;i<chainLength-1;i++){
                 dist+=chain[i];
                 sz++;
            }
        }else{
            for (var i=0;i<chainLength;i++){
                 dist+=chain[i];
                 sz++;
            }
        }
        return dist/sz;
    } else {
        return 1;
    }
}


function replaceNumeric(orig){
    var nw=rpl(orig,"0","o");
    nw=rpl(nw,"1","i");
    nw=rpl(nw,"2","z");
    nw=rpl(nw,"3","e");
    nw=rpl(nw,"4","a");
    nw=rpl(nw,"5","s");
    nw=rpl(nw,"6","g");
    nw=rpl(nw,"7","t");
    nw=rpl(nw,"8","b");
    nw=rpl(nw,"9","p");
    nw=nw.toLowerCase();
    return nw;
}

function replaceLeet(orig){
    var nw=rpl(orig,"@","a");
    nw=rpl(nw,"!","i");
    nw=rpl(nw,"[\$]","s");
    nw=rpll(nw,/\/\\\/\\/g,"m");
    nw=rpll(nw,/\/\\\//g,"n");
    nw=rpll(nw,/\|</g,"k");
    nw=rpll(nw,/\|\?/g,"r");
    nw=rpll(nw,/\/-\\/g,"a");
    nw=rpll(nw,/\|-\|/g,"h");
    nw=rpll(nw,/\|\)/g,"d");
    nw=rpll(nw,/\(/g,"c");
    nw=rpl(nw,"£","e");
    nw=rpl(nw,"&","b");
    nw=nw.toLowerCase();
    return nw;
}

function replaceBoth(orig){
    var nw=replaceNumeric(orig);
    nw=replaceLeet(nw);
    return nw;
}

