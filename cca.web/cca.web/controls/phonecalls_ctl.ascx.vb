Partial Class phonecalls_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ophonecalls As Objects.phonecalls
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_phones_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.phone_fk.Items.Count=0 Then
        Me.phone_fk.DataSource=CDC.ReadDataTable(CO)
        Me.phone_fk.DataTextField = "value"
        Me.phone_fk.DataValueField = "pk"
        Try
            Me.phone_fk.DataBind
        Catch Ex as Exception
            Me.phone_fk.SelectedValue=-1
            Me.phone_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ophonecalls is Nothing then
ophonecalls = new Objects.phonecalls(currentuser,actualuser,pk,CDC)
End If
Me.phone_fk.SelectedValue=ophonecalls.phone_fk
Me.calldate.Text=ophonecalls.calldate.ToString("dd MMM yyyy")
If Me.calldate.Text="01 Jan 0001" then Me.calldate.Text=""
Me.calldate.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.calldate.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=ophonecalls.phonecall_pk
Status=ophonecalls.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ophonecalls is Nothing then
ophonecalls = new Objects.phonecalls(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ophonecalls.phone_fk=Me.phone_fk.SelectedValue
ophonecalls.calldate=CommonFN.CheckEmptyDate(Me.calldate.Text)
result=ophonecalls.Save()
if not result then throw ophonecalls.LastError
_CurrentPk=ophonecalls.phonecall_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ophonecalls.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecalls is Nothing then
   ophonecalls = new Objects.phonecalls(currentuser,actualuser,pk,CDC)
  End If
  ophonecalls.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecalls is Nothing then
   ophonecalls = new Objects.phonecalls(currentuser,actualuser,pk,CDC)
  End If
  ophonecalls.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecalls is Nothing then
   ophonecalls = new Objects.phonecalls(currentuser,pk,CDC)
  End If
  ophonecalls.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.phone_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkphone_fk.Visible=IIF(RL>=0,True,False)
  Me.calldate.Enabled=IIF(WL>=0,True,False)
  Me.blkcalldate.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

