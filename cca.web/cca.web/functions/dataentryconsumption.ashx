<%@ WebHandler Language="VB" Class="function_dataentryconsumption" %>
Public Class function_dataentryconsumption
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        
        Dim Msg As String = ""
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        
        Dim month As Integer = -1
        Dim year As Integer = -1
        Dim meterpoint As String = ""
        Dim total As Decimal = 0
        Dim eligible As Decimal = 0
        
        Try
            month = RF("b").ToString.Trim()
        Catch ex As Exception
            month = -1
        End Try
        Try
            year = RF("c").ToString.Trim()
        Catch ex As Exception
            year = -1
        End Try
        Try
            total = RF("d").ToString.Trim()
        Catch ex As Exception
            total = 0
        End Try
        Try
            eligible = RF("e").ToString.Trim()
        Catch ex As Exception
            eligible = 0
        End Try
        
        Dim amp As New agreementmeterpoints(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)
        Try
            meterpoint = amp.meterpoint_fk
        Catch ex As Exception
            meterpoint = -1
        End Try

        
        Dim outputvalue As Integer = 0
        Dim CO As New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_dataconsumptions_duplicatecheck"
        CO.Parameters.AddWithValue("@meterpoint", meterpoint)
        CO.Parameters.AddWithValue("@month", month)
        CO.Parameters.AddWithValue("@year", year)
        CDC.ReadScalarValue(outputvalue, CO)
        
        If outputvalue = 0 Then
            Dim dec As New dataconsumptions(SM.currentuser, SM.actualuser, CDC)
            dec.meterpoint_fk = meterpoint
            dec.month_fk = month
            dec.year_fk = year
            dec.totalconsumption = total
            dec.eligibleconsumption = eligible
            dec.dataimportmethod_fk = 1
            If dec.Save() Then : Msg = "Saved" : End If
                
        Else
            Dim dec As New dataconsumptions(SM.currentuser, SM.actualuser, outputvalue, CDC)
            dec.month_fk = month
            dec.year_fk = year
            dec.totalconsumption = total
            dec.eligibleconsumption = eligible
            dec.dataimportmethod_fk = 1
            If dec.Save() Then : Msg = "Updated" : End If
        
        End If
        
        
        Try
            
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class