﻿
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports Dundas.Charting.WebControl.ChartTypes

Imports System.Drawing



Partial Class corecca_chart
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Shared cDefault As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cOne As Color = System.Drawing.ColorTranslator.FromHtml("#095783") ' Dark Green
    Shared cTwo As Color = System.Drawing.ColorTranslator.FromHtml("#2D04A2") ' Blue / Purple
    Shared cThree As Color = System.Drawing.ColorTranslator.FromHtml("#D5360D") ' Red
    Shared cFour As Color = System.Drawing.ColorTranslator.FromHtml("#DEA317") ' Light Orange
    Shared cFive As Color = System.Drawing.ColorTranslator.FromHtml("#44B934") ' Green
    Shared cSix As Color = System.Drawing.ColorTranslator.FromHtml("#0694d8") ' Light Blue
    Shared cSeven As Color = System.Drawing.ColorTranslator.FromHtml("#b760ee") ' Lilac
    Shared cEight As Color = System.Drawing.ColorTranslator.FromHtml("#7a5a10") ' Brown

    Shared colorSet() As Color = {cDefault, cOne, cTwo, cThree, cFour, cFive, cSix, cSeven, cEight}


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_dataconsumptions_comparisonoverview"
        CO.Parameters.AddWithValue("@varMeterList", "887,885")
        CO.Parameters.AddWithValue("@date_start", "01 Jan 2015")
        CO.Parameters.AddWithValue("@date_end", "01 Dec 2015")
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        grdPivot.DataSource = Local.GetInversedDataTable(dt, "date", "meter", "totalconsumption", "0", True)
        grdPivot.DataBind()
        CO.Dispose()

        dataconsumptionsanalysischart(dt)

    End Sub

    Private Sub dataconsumptionsanalysischart(dt As DataTable)


        With chartPivot
            .Serializer.Reset()
            .Visible = True
            .Series.Clear()
            .Titles.Clear()
            .Legends.Clear()
            .Legends.Add("Default")

            ' legend stuff
            .Legends(0).Docking = LegendDocking.Bottom
            .Legends(0).Alignment = StringAlignment.Far
            .Legends(0).Reversed = AutoBool.False
            .Legends(0).Position.Auto = True
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
            .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))
            ' Zooming
            For Each chartarea1 As ChartArea In .ChartAreas
                chartarea1.CursorX.UserEnabled = True
            Next
            ' Setup Frame
            .BorderColor = System.Drawing.ColorTranslator.FromHtml("#009530") ' Corp Green
            .BorderStyle = ChartDashStyle.Solid
            .BorderWidth = 2
            .BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            'Setup Toolbar
            '.UI.Toolbar.Enabled = True
            '.UI.Toolbar.Docking = ToolbarDocking.Top
            '.UI.Toolbar.Placement = ToolbarPlacement.InsideChart
            '.UI.Toolbar.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
            '.UI.Toolbar.BorderSkin.FrameBackColor = System.Drawing.ColorTranslator.FromHtml("#6EBB1F") ' Corp Green

            .ChartAreas.Add("Default")
            .ChartAreas(0).CursorX.UserEnabled = True
            .Width = "870"
            .Height = "450"
            .Titles.Add("Consumption Analysis")
            .ChartAreas("Default").AxisY.Title = "Consumption (unit)"
            .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
            .PaletteCustomColors = colorSet
        End With



        Dim CN As New SqlConnection(CDC.ConnectionString(0))
        CN.Open()
        CO = New SqlCommand
        CO.Connection = CN
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_dataconsumptions_comparisonoverview"
        CO.Parameters.AddWithValue("@varMeterList", "887,885")
        CO.Parameters.AddWithValue("@date_start", "01 Jan 2015")
        CO.Parameters.AddWithValue("@date_end", "01 Dec 2015")

        Dim reader As SqlDataReader = CO.ExecuteReader
        System.Diagnostics.Debug.WriteLine(CDC.CurrentConnectionReader)

        Dim dr As DataTableReader
        dr = dt.CreateDataReader
        chartPivot.Visible = True
        chartPivot.DataBindCrossTab(reader, "meter", "date", "totalconsumption", "")


        Dim series As Series
        For Each series In chartPivot.Series
            series.EmptyPointStyle.Color = Color.Transparent
            series.Type = SeriesChartType.Line
            series.BorderWidth = 2
            series.XValueType = ChartValueTypes.DateTime
            series.Type = Dundas.Charting.WebControl.SeriesChartType.Line

            'MakeGroupLabels(chartPivot, series, "date")
        Next



    End Sub

    Protected Overloads Sub MakeGroupLabels(ByVal chart As Chart, ByVal series As Series, ByVal ParamArray fields() As String)
        ' This is how many rows of labels we need to make.
        Dim numLabels As Integer = fields.Length

        Dim chartArea As ChartArea = chart.ChartAreas(series.ChartArea)
        Dim group As String() = New String(numLabels - 1) {}
        Dim labels As CustomLabel() = New CustomLabel(numLabels - 1) {}

        ' Initialize each row's current label.
        For i As Integer = 0 To numLabels - 1
            group(i) = ""
        Next

        ' For x-value indexed series, count from 1.
        Dim xValue As Integer = 1

        ' Set the PointWidth default value if it's null
        If series("PointWidth") Is Nothing Then
            series("PointWidth") = "0.8"
        End If

        ' Iterate over each point in the series.  Check the group fields and 
        ' update the ending value for each label or start a new label.
        For Each dataPoint As DataPoint In series.Points
            For i As Integer = 0 To numLabels - 1
                If group(i) = dataPoint(fields(i)) Then
                    ' Still in the same group.  Update the custom label's end position.
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                Else
                    ' Create a new label here with the group text.
                    group(i) = dataPoint(fields(i))
                    labels(i) = New CustomLabel()
                    labels(i).From = xValue - Double.Parse(series("PointWidth")) / 2
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                    labels(i).RowIndex = (i + 1)
                    labels(i).Text = group(i)
                    labels(i).LabelMark = LabelMark.LineSideMark
                    chartArea.AxisX.CustomLabels.Add(labels(i))
                End If
            Next
            xValue += 1
        Next
    End Sub


End Class
