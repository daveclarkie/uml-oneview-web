﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="cclratepercentagemanagement.aspx.vb" Inherits="backend_cca_cclratepercentagemanagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current CCL Rate Percentages</h5>
<asp:Repeater ID="rptCCLRatePercentages" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:100px;">
            <h4>Fuel</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid From</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid To</h4>
        </td>
        <td style="width:80px;">
            <h4>CCL Percentage</h4>
        </td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("cclratepercentage_pk") %>' style='background-color:<%# Container.DataItem("live")%>;'>
            <td>
                <%# Container.DataItem("fuel")%>
            </td>
            <td>
                <%# Container.DataItem("validfrom")%>
            </td>
            <td>
                <%# Container.DataItem("validto")%>
            </td>
            <td>
                <%# Container.DataItem("cclpercentage")%>
            </td>
            
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:100px">
                <a href='cclratepercentagemanagement.aspx?pk=-1'><h6>New Percentage</h6></a> 
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
                
       
<asp:PlaceHolder ID="CCLRatePercentageControl" runat="server" />
<table id="CCLRatePercentageButton" runat="server" style="width:100%;">
    <tr style="height:10px;">
                            
    </tr>
    <tr>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton runat="server" ID="btnSaveCCLRatePercentageControl" Text="<h6>Save</h6>" />
        </td>
        <td style="width:60%;"></td>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton ID="btnCancelCCLRatePercentageControl" runat="server" Text="<h4>Cancel</h4>" />
        </td>
    </tr>
</table>
</asp:Content>