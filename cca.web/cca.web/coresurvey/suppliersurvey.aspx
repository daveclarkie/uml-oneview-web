﻿<%@ Page Language="VB" MasterPageFile="~/design/masters/external_survey.master" AutoEventWireup="false" CodeFile="suppliersurvey.aspx.vb" Inherits="coresurvey_survey" title="Survey Centre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <h3>Welcome to the Schneider Electric Survey Centre</h3>

    <asp:Label ID="lbl_errormessage" runat="server" ForeColor="Red" ></asp:Label><br />
    <asp:Panel ID="pnl_survey" runat="server" Visible="false">
        <asp:table ID="tbl_survey" runat="server" HorizontalAlign="Left">
            <asp:TableRow ID="tr_survey" runat="server" HorizontalAlign="Left" >
            
            
            </asp:TableRow>
        </asp:table>


        <asp:PlaceHolder ID="SurveyControl" runat="server" />
        
    </asp:Panel>
<table id="AgreementTargetButton" runat="server">
            <tr style="height:10px;">
                            
            </tr>
            <tr>
                <td style="text-align:center;">
                    <asp:Linkbutton runat="server" ID="btn_savesurvey" Text="<h6>Save</h6>" Width="100%" />
                </td>
                
            </tr>
        </table>
    <asp:Panel ID="pnl_complete" runat="server" Visible="false">
        <asp:Label ID="lbl_completemessage" runat="server" Text="Thank you for taking time to complete the survey."></asp:Label>

    </asp:Panel>


</asp:Content>


