<%@ Control Language="VB" AutoEventWireup="false" CodeFile="personphones.ascx.vb" Inherits="controls_combo_personphone" %>
<%@ Register Src="~/controls/combo/userphone_combo_ctl.ascx" TagName="combo" TagPrefix="uc1" %>
<h5>Stored phones</h5>    
<asp:Repeater EnableViewState="false" ID="rpt" runat="server">
    <HeaderTemplate><ul class="combolist"></HeaderTemplate> 
    <FooterTemplate></ul></FooterTemplate> 
    <ItemTemplate>
        <li id='node_<%# Container.DataItem("userphone_pk") %>'>
            <a class="remove" title="Remove"  href='javascript:removeNode(<%# Container.DataItem("userphone_pk") %>)'>X</a>
            <a href='persons.aspx?pk=<%=PSM.TargetUser%>&md=ph&spk=<%# Container.DataItem("userphone_pk") %><%=Local.noCache %>'><%#Container.DataItem("linktext")%></a><br />
            <span class="item"><%#Container.DataItem("subtext")%></span></li>
    </ItemTemplate>
</asp:Repeater>


<uc1:combo ID="userphone_combo_ctl" runat="server" />




<script type="text/javascript" language="javascript">

function removeNode(pk){
    var selItem="node_"+pk;
    setReqHttp();
    if (hasXmlhttp==true){
        xmlhttp.open("GET","/functions/removephone.ashx?pk="+pk+noCache(),true);
        xmlhttp.send(null);   
        var nd=document.getElementById(selItem);
        nd.parentNode.removeChild(nd);   
    }
   else
   {
    alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
   } 
}

</script>
