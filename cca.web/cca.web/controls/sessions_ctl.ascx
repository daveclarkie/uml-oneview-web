<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sessions_ctl.ascx.vb" Inherits="sessions_ctl" %>
<h5>sessions</h5>
<ul class='formcontrol'>
<li runat="server" id="blksessionid">
<span class='label'>sessionid</span>
<asp:TextBox EnableViewState="false" ID="sessionid" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkip">
<span class='label'>ip</span>
<asp:TextBox EnableViewState="false" ID="ip" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcurrentuser">
<span class='label'>currentuser</span>
<asp:TextBox EnableViewState="false" ID="currentuser" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkactualuser">
<span class='label'>actualuser</span>
<asp:TextBox EnableViewState="false" ID="actualuser" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetuser">
<span class='label'>targetuser</span>
<asp:TextBox EnableViewState="false" ID="targetuser" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkmainid">
<span class='label'>mainid</span>
<asp:TextBox EnableViewState="false" ID="mainid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blksubid">
<span class='label'>subid</span>
<asp:TextBox EnableViewState="false" ID="subid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blklinkid">
<span class='label'>linkid</span>
<asp:TextBox EnableViewState="false" ID="linkid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkPersonPageMode">
<span class='label'>PersonPageMode</span>
<asp:TextBox EnableViewState="false" ID="PersonPageMode" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkAgreementPageMode">
<span class='label'>AgreementPageMode</span>
<asp:TextBox EnableViewState="false" ID="AgreementPageMode" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkredirected">
<span class='label'>redirected</span>
<asp:TextBox EnableViewState="false" ID="redirected" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blknoticecount">
<span class='label'>noticecount</span>
<asp:TextBox EnableViewState="false" ID="noticecount" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetAgreement">
<span class='label'>targetAgreement</span>
<asp:TextBox EnableViewState="false" ID="targetAgreement" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetAgreementSecondary">
<span class='label'>targetAgreementSecondary</span>
<asp:TextBox EnableViewState="false" ID="targetAgreementSecondary" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetDataEntry">
<span class='label'>targetDataEntry</span>
<asp:TextBox EnableViewState="false" ID="targetDataEntry" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkDataEntryPageMode">
<span class='label'>DataEntryPageMode</span>
<asp:TextBox EnableViewState="false" ID="DataEntryPageMode" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkmatrix">
<span class='label'>matrix</span>
<asp:TextBox EnableViewState="false" ID="matrix" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkmatrixprice">
<span class='label'>matrixprice</span>
<asp:TextBox EnableViewState="false" ID="matrixprice" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkmatrixdetail">
<span class='label'>matrixdetail</span>
<asp:TextBox EnableViewState="false" ID="matrixdetail" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkMatrixPageMode">
<span class='label'>MatrixPageMode</span>
<asp:TextBox EnableViewState="false" ID="MatrixPageMode" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetBubble">
<span class='label'>targetBubble</span>
<asp:TextBox EnableViewState="false" ID="targetBubble" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktargetBubbleSecondary">
<span class='label'>targetBubbleSecondary</span>
<asp:TextBox EnableViewState="false" ID="targetBubbleSecondary" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkBubblePageMode">
<span class='label'>BubblePageMode</span>
<asp:TextBox EnableViewState="false" ID="BubblePageMode" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCustomerPageMode">
<span class='label'>CustomerPageMode</span>
<asp:TextBox EnableViewState="false" ID="CustomerPageMode" runat="server" cssClass="input_num"  />
</li>
</ul>

