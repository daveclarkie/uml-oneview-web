<%@ Control Language="VB" AutoEventWireup="false" CodeFile="suppliers_ctl.ascx.vb" Inherits="suppliers_ctl" %>
<h5>suppliers</h5>
<ul class='formcontrol'>
<li runat="server" id="blksupplier_name">
<span class='label'>supplier_name</span>
<asp:TextBox EnableViewState="false" ID="supplier_name" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blksupply_electricity">
<span class='label'>supply_electricity</span>
<asp:TextBox EnableViewState="false" ID="supply_electricity" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blksupply_gas">
<span class='label'>supply_gas</span>
<asp:TextBox EnableViewState="false" ID="supply_gas" runat="server" cssClass="input_num"  />
</li>
</ul>

