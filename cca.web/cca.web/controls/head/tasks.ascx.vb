Imports System.Data.SqlClient

Partial Class headctls_tasks
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Dim _label As String = ""
    Dim _tasks As Integer = 0
    Dim _taskType As TaskCountType = TaskCountType.Today

    Public ReadOnly Property Tasks() As Integer
        Get
            Return _tasks
        End Get
    End Property

    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public ReadOnly Property State() As String
        Get
            Select Case TaskType
                Case TaskCountType.Overdue
                    Select Case _tasks
                        Case 0 : Return " green"
                        Case Else : Return " red"
                    End Select
                Case TaskCountType.Today
                    Select Case _tasks
                        Case Is < 5 : Return " green"
                        Case Is < 10 : Return " yellow"
                        Case Else : Return " red"
                    End Select
            End Select
            Return ""
        End Get
    End Property

    Public Property TaskType() As TaskCountType
        Get
            Return _taskType
        End Get
        Set(ByVal value As TaskCountType)
            _taskType = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Refresh()
    End Sub

    Public Sub Refresh()
        'If CDC Is Nothing Then CDC = New DataCommon
        Dim CO As New SqlCommand

        Select Case TaskType
            Case TaskCountType.Today
                CO.CommandText = "rsp_usertaskstoday"
            Case TaskCountType.Overdue
                CO.CommandText = "rsp_usertasksoverdue"
            Case Else
                CO.Dispose()
                Exit Sub
        End Select

        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@user_pk", SM.currentuser)
        CDC.ReadScalarValue(_tasks, CO)
        CO.Dispose()

        Select Case TaskType
            Case TaskCountType.Overdue
                ' TODO: SM.TaskCount = _tasks
        End Select

        Dim ctl As String = ""
        ctl &= "<a href=""./tasks.aspx"" class=""notifylink"">"
        ctl &= "<div class=""notify"
        ctl &= State
        ctl &= """><span class=""count"">"
        ctl &= Tasks
        ctl &= "</span><br /><span class=""counttype"">"
        ctl &= Label
        ctl &= "</span></div></a>"
        taskitem.Text = ctl
    End Sub
End Class
