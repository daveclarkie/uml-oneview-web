﻿function removeFuelConversion(pk) {
    var selItem = "node_" + pk;
    setReqHttp();
    if (hasXmlhttp == true) {
        xmlhttp.open("GET", "/functions/removefuelconversion.ashx?pk=" + pk + noCache(), true);
        xmlhttp.send(null);
        var nd = document.getElementById(selItem);
        nd.parentNode.removeChild(nd);
    }
    else {
        alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
    }
} //removeFuelConversion