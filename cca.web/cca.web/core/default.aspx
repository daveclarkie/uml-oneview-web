<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="core_Default" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
        <h3>Welcome</h3>
        <br />
        <asp:Label ID="lblotherusers" runat="server"></asp:Label>

        <%  If ccaSystemAccess Then%>
        <h3>CCA Reports Status</h3>
        <asp:Repeater ID="rptCCAReportStatus" runat="server">
            <HeaderTemplate>    
                <table>
                    <tr>
                        <td style="width:150px;">
                            <h4>Name</h4>
                        </td>
                        <td style="width:150px;">
                            <h4>Type</h4>
                        </td>
                        <td style="width:300px;">
                            <h4>Status</h4>
                        </td>
                        <td style="width:15px;">
                            
                        </td>
                    </tr>
            </HeaderTemplate> 
            <ItemTemplate>
                <tr id='node_<%# Container.DataItem("report_pk") %>' style='color:<%# container.dataitem("forecolour")%>;' >
                    <td><%# Container.DataItem("reportname")%></td>
                    <td><%# Container.DataItem("reporttypename")%></td>
                    <td><%# Container.DataItem("reportstatus")%></td>
                    <% If BackendAccess Then%>
                    <td><a href='javascript:reportmaintenance(<%# Container.DataItem("javascript") %>)'><%# Container.DataItem("javascriptname") %></a></td>
                    <% End If%>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate> 
        </asp:Repeater>
        <% End If%>


        
</asp:Content>

