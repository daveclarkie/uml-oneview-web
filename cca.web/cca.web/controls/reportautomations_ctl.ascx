<%@ Control Language="VB" AutoEventWireup="false" CodeFile="reportautomations_ctl.ascx.vb" Inherits="reportautomations_ctl" %>
<h5>reportautomations</h5>
<ul class='formcontrol'>
<li runat="server" id="blkreport_fk">
<span class='label'>report_fk</span>
<asp:DropDownList ID="report_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkuser_fk">
<span class='label'>user_fk</span>
<asp:DropDownList ID="user_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkemail_fk">
<span class='label'>email_fk</span>
<asp:DropDownList ID="email_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkfrequency_fk">
<span class='label'>frequency_fk</span>
<asp:DropDownList ID="frequency_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkstartdate">
<span class='label'>startdate</span>
<asp:TextBox EnableViewState="false" ID="startdate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkreportformat_fk">
<span class='label'>reportformat_fk</span>
<asp:DropDownList ID="reportformat_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

