Partial Class matrix1sources_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omatrix1sources As Objects.matrix1sources
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_matrixfiles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.matrixfile_fk.Items.Count=0 Then
        Me.matrixfile_fk.DataSource=CDC.ReadDataTable(CO)
        Me.matrixfile_fk.DataTextField = "value"
        Me.matrixfile_fk.DataValueField = "pk"
        Try
            Me.matrixfile_fk.DataBind
        Catch Ex as Exception
            Me.matrixfile_fk.SelectedValue=-1
            Me.matrixfile_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_suppliers_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.supplier_fk.Items.Count=0 Then
        Me.supplier_fk.DataSource=CDC.ReadDataTable(CO)
        Me.supplier_fk.DataTextField = "value"
        Me.supplier_fk.DataValueField = "pk"
        Try
            Me.supplier_fk.DataBind
        Catch Ex as Exception
            Me.supplier_fk.SelectedValue=-1
            Me.supplier_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_distributions_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.distribution_fk.Items.Count=0 Then
        Me.distribution_fk.DataSource=CDC.ReadDataTable(CO)
        Me.distribution_fk.DataTextField = "value"
        Me.distribution_fk.DataValueField = "pk"
        Try
            Me.distribution_fk.DataBind
        Catch Ex as Exception
            Me.distribution_fk.SelectedValue=-1
            Me.distribution_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_profiles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.profile_fk.Items.Count=0 Then
        Me.profile_fk.DataSource=CDC.ReadDataTable(CO)
        Me.profile_fk.DataTextField = "value"
        Me.profile_fk.DataValueField = "pk"
        Try
            Me.profile_fk.DataBind
        Catch Ex as Exception
            Me.profile_fk.SelectedValue=-1
            Me.profile_fk.DataBind
        End Try
    End If
            Me.contractlength.Text="0"
            Me.standingcharge.Text="0.00"
            Me.rate_single.Text="0.00"
            Me.rate_day.Text="0.00"
            Me.rate_night.Text="0.00"
            Me.rate_other.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omatrix1sources is Nothing then
omatrix1sources = new Objects.matrix1sources(currentuser,actualuser,pk,CDC)
End If
Me.matrixfile_fk.SelectedValue=omatrix1sources.matrixfile_fk
Me.supplier_fk.SelectedValue=omatrix1sources.supplier_fk
Me.productname.Text=omatrix1sources.productname
Me.distribution_fk.SelectedValue=omatrix1sources.distribution_fk
Me.profile_fk.SelectedValue=omatrix1sources.profile_fk
Me.contractlength.Text=omatrix1sources.contractlength
Me.contractlength.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.contractlength.Attributes.Add("onBlur", "clearNotice()")
Me.validfrom.Text=omatrix1sources.validfrom.ToString("dd MMM yyyy")
If Me.validfrom.Text="01 Jan 0001" then Me.validfrom.Text=""
Me.validfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.validfrom.Attributes.Add("onBlur", "clearNotice()")
Me.validto.Text=omatrix1sources.validto.ToString("dd MMM yyyy")
If Me.validto.Text="01 Jan 0001" then Me.validto.Text=""
Me.validto.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.validto.Attributes.Add("onBlur", "clearNotice()")
Me.standingcharge.Text=omatrix1sources.standingcharge
Me.standingcharge.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.standingcharge.Attributes.Add("onBlur", "clearNotice()")
Me.rate_single.Text=omatrix1sources.rate_single
Me.rate_single.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.rate_single.Attributes.Add("onBlur", "clearNotice()")
Me.rate_day.Text=omatrix1sources.rate_day
Me.rate_day.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.rate_day.Attributes.Add("onBlur", "clearNotice()")
Me.rate_night.Text=omatrix1sources.rate_night
Me.rate_night.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.rate_night.Attributes.Add("onBlur", "clearNotice()")
Me.rate_other.Text=omatrix1sources.rate_other
Me.rate_other.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.rate_other.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=omatrix1sources.matrixsource_pk
Status=omatrix1sources.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omatrix1sources is Nothing then
omatrix1sources = new Objects.matrix1sources(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omatrix1sources.matrixfile_fk=Me.matrixfile_fk.SelectedValue
omatrix1sources.supplier_fk=Me.supplier_fk.SelectedValue
omatrix1sources.productname=Me.productname.Text
omatrix1sources.distribution_fk=Me.distribution_fk.SelectedValue
omatrix1sources.profile_fk=Me.profile_fk.SelectedValue
omatrix1sources.contractlength=Me.contractlength.Text
omatrix1sources.validfrom=CommonFN.CheckEmptyDate(Me.validfrom.Text)
omatrix1sources.validto=CommonFN.CheckEmptyDate(Me.validto.Text)
omatrix1sources.standingcharge=Me.standingcharge.Text
omatrix1sources.rate_single=Me.rate_single.Text
omatrix1sources.rate_day=Me.rate_day.Text
omatrix1sources.rate_night=Me.rate_night.Text
omatrix1sources.rate_other=Me.rate_other.Text
result=omatrix1sources.Save()
if not result then throw omatrix1sources.LastError
_CurrentPk=omatrix1sources.matrixsource_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omatrix1sources.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1sources is Nothing then
   omatrix1sources = new Objects.matrix1sources(currentuser,actualuser,pk,CDC)
  End If
  omatrix1sources.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1sources is Nothing then
   omatrix1sources = new Objects.matrix1sources(currentuser,actualuser,pk,CDC)
  End If
  omatrix1sources.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1sources is Nothing then
   omatrix1sources = new Objects.matrix1sources(currentuser,pk,CDC)
  End If
  omatrix1sources.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.matrixfile_fk.Enabled=IIF(WL>=99,True,False)
  Me.blkmatrixfile_fk.Visible=IIF(RL>=99,True,False)
  Me.supplier_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksupplier_fk.Visible=IIF(RL>=0,True,False)
  Me.productname.Enabled=IIF(WL>=0,True,False)
  Me.blkproductname.Visible=IIF(RL>=0,True,False)
  Me.distribution_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkdistribution_fk.Visible=IIF(RL>=0,True,False)
  Me.profile_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkprofile_fk.Visible=IIF(RL>=0,True,False)
  Me.contractlength.Enabled=IIF(WL>=0,True,False)
  Me.blkcontractlength.Visible=IIF(RL>=0,True,False)
  Me.validfrom.Enabled=IIF(WL>=0,True,False)
  Me.blkvalidfrom.Visible=IIF(RL>=0,True,False)
  Me.validto.Enabled=IIF(WL>=0,True,False)
  Me.blkvalidto.Visible=IIF(RL>=0,True,False)
  Me.standingcharge.Enabled=IIF(WL>=0,True,False)
  Me.blkstandingcharge.Visible=IIF(RL>=0,True,False)
  Me.rate_single.Enabled=IIF(WL>=0,True,False)
  Me.blkrate_single.Visible=IIF(RL>=0,True,False)
  Me.rate_day.Enabled=IIF(WL>=0,True,False)
  Me.blkrate_day.Visible=IIF(RL>=0,True,False)
  Me.rate_night.Enabled=IIF(WL>=0,True,False)
  Me.blkrate_night.Visible=IIF(RL>=0,True,False)
  Me.rate_other.Enabled=IIF(WL>=0,True,False)
  Me.blkrate_other.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

