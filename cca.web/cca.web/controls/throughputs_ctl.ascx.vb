Partial Class throughputs_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents othroughputs As Objects.throughputs
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_units_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.unit_fk.Items.Count=0 Then
        Me.unit_fk.DataSource=CDC.ReadDataTable(CO)
        Me.unit_fk.DataTextField = "value"
        Me.unit_fk.DataValueField = "pk"
        Try
            Me.unit_fk.DataBind
        Catch Ex as Exception
            Me.unit_fk.SelectedValue=-1
            Me.unit_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If othroughputs is Nothing then
othroughputs = new Objects.throughputs(currentuser,actualuser,pk,CDC)
End If
Me.throughputname.Text=othroughputs.throughputname
Me.unit_fk.SelectedValue=othroughputs.unit_fk
_CurrentPk=othroughputs.throughput_pk
Status=othroughputs.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If othroughputs is Nothing then
othroughputs = new Objects.throughputs(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
othroughputs.throughputname=Me.throughputname.Text
othroughputs.unit_fk=Me.unit_fk.SelectedValue
result=othroughputs.Save()
if not result then throw othroughputs.LastError
_CurrentPk=othroughputs.throughput_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=othroughputs.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If othroughputs is Nothing then
   othroughputs = new Objects.throughputs(currentuser,actualuser,pk,CDC)
  End If
  othroughputs.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If othroughputs is Nothing then
   othroughputs = new Objects.throughputs(currentuser,actualuser,pk,CDC)
  End If
  othroughputs.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If othroughputs is Nothing then
   othroughputs = new Objects.throughputs(currentuser,pk,CDC)
  End If
  othroughputs.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.throughputname.Enabled=IIF(WL>=0,True,False)
  Me.blkthroughputname.Visible=IIF(RL>=0,True,False)
  Me.unit_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkunit_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

