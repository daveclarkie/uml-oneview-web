<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bills_ctl.ascx.vb" Inherits="bills_ctl" %>
<h5>bills</h5>
<ul class='formcontrol'>
<li runat="server" id="blkfueltype">
<span class='label'>fueltype</span>
<asp:TextBox EnableViewState="false" ID="fueltype" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blksite_fk">
<span class='label'>site_fk</span>
<asp:DropDownList ID="site_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkbilltype">
<span class='label'>billtype</span>
<asp:TextBox EnableViewState="false" ID="billtype" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkbilledfrom">
<span class='label'>billedfrom</span>
<asp:TextBox EnableViewState="false" ID="billedfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkbilledto">
<span class='label'>billedto</span>
<asp:TextBox EnableViewState="false" ID="billedto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkaddedby">
<span class='label'>addedby</span>
<asp:TextBox EnableViewState="false" ID="addedby" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

