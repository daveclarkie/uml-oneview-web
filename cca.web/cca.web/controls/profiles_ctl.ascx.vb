Partial Class profiles_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oprofiles As Objects.profiles
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oprofiles is Nothing then
oprofiles = new Objects.profiles(currentuser,actualuser,pk,CDC)
End If
Me.code.Text=oprofiles.code
Me.description.Text=oprofiles.description
_CurrentPk=oprofiles.profile_pk
Status=oprofiles.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oprofiles is Nothing then
oprofiles = new Objects.profiles(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oprofiles.code=Me.code.Text
oprofiles.description=Me.description.Text
result=oprofiles.Save()
if not result then throw oprofiles.LastError
_CurrentPk=oprofiles.profile_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oprofiles.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oprofiles is Nothing then
   oprofiles = new Objects.profiles(currentuser,actualuser,pk,CDC)
  End If
  oprofiles.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oprofiles is Nothing then
   oprofiles = new Objects.profiles(currentuser,actualuser,pk,CDC)
  End If
  oprofiles.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oprofiles is Nothing then
   oprofiles = new Objects.profiles(currentuser,pk,CDC)
  End If
  oprofiles.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.code.Enabled=IIF(WL>=0,True,False)
  Me.blkcode.Visible=IIF(RL>=0,True,False)
  Me.description.Enabled=IIF(WL>=0,True,False)
  Me.blkdescription.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

