<%@ Control Language="VB" AutoEventWireup="false" CodeFile="helpdeskworkorderdetails_ctl.ascx.vb" Inherits="helpdeskworkorderdetails_ctl" %>
<h5>helpdeskworkorderdetails</h5>
<ul class='formcontrol'>
<li runat="server" id="blkhelpdesk_id">
<span class='label'>helpdesk_id</span>
<asp:TextBox EnableViewState="false" ID="helpdesk_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkcreated_time">
<span class='label'>created_time</span>
<asp:TextBox EnableViewState="false" ID="created_time" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkrequester">
<span class='label'>requester</span>
<asp:TextBox EnableViewState="false" ID="requester" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkrequester_id">
<span class='label'>requester_id</span>
<asp:TextBox EnableViewState="false" ID="requester_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktechnician">
<span class='label'>technician</span>
<asp:TextBox EnableViewState="false" ID="technician" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktechnician_id">
<span class='label'>technician_id</span>
<asp:TextBox EnableViewState="false" ID="technician_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkclient">
<span class='label'>client</span>
<asp:TextBox EnableViewState="false" ID="client" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktender_type">
<span class='label'>tender_type</span>
<asp:TextBox EnableViewState="false" ID="tender_type" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcountry">
<span class='label'>country</span>
<asp:TextBox EnableViewState="false" ID="country" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcontract_renewal">
<span class='label'>contract_renewal</span>
<asp:TextBox EnableViewState="false" ID="contract_renewal" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blktender_due">
<span class='label'>tender_due</span>
<asp:TextBox EnableViewState="false" ID="tender_due" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkstatus">
<span class='label'>status</span>
<asp:TextBox EnableViewState="false" ID="hdstatus" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcommodity">
<span class='label'>commodity</span>
<asp:TextBox EnableViewState="false" ID="commodity" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcompleted_time">
<span class='label'>completed_time</span>
<asp:TextBox EnableViewState="false" ID="completed_time" runat="server" cssClass="input_dtm" />
</li>
</ul>

