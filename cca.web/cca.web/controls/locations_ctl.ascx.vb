Partial Class locations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents olocations As Objects.locations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_countys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.county_fk.Items.Count=0 Then
        Me.county_fk.DataSource=CDC.ReadDataTable(CO)
        Me.county_fk.DataTextField = "value"
        Me.county_fk.DataValueField = "pk"
        Try
            Me.county_fk.DataBind
        Catch Ex as Exception
            Me.county_fk.SelectedValue=-1
            Me.county_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_countrys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.country_fk.Items.Count=0 Then
        Me.country_fk.DataSource=CDC.ReadDataTable(CO)
        Me.country_fk.DataTextField = "value"
        Me.country_fk.DataValueField = "pk"
        Try
            Me.country_fk.DataBind
        Catch Ex as Exception
            Me.country_fk.SelectedValue=-1
            Me.country_fk.DataBind
        End Try
    End If
            Me.longitude.Text="0.00"
            Me.latitude.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If olocations is Nothing then
olocations = new Objects.locations(currentuser,actualuser,pk,CDC)
End If
Me.buildingprefix.Text=olocations.buildingprefix
Me.streetnumber.Text=olocations.streetnumber
Me.street.Text=olocations.street
Me.district.Text=olocations.district
Me.town.Text=olocations.town
Me.county_fk.SelectedValue=olocations.county_fk
Me.country_fk.SelectedValue=olocations.country_fk
Me.postcode.Text=olocations.postcode
Me.longitude.Text=olocations.longitude
Me.longitude.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.longitude.Attributes.Add("onBlur", "clearNotice()")
Me.latitude.Text=olocations.latitude
Me.latitude.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.latitude.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=olocations.location_pk
Status=olocations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If olocations is Nothing then
olocations = new Objects.locations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
olocations.buildingprefix=Me.buildingprefix.Text
olocations.streetnumber=Me.streetnumber.Text
olocations.street=Me.street.Text
olocations.district=Me.district.Text
olocations.town=Me.town.Text
olocations.county_fk=Me.county_fk.SelectedValue
olocations.country_fk=Me.country_fk.SelectedValue
olocations.postcode=Me.postcode.Text
olocations.longitude=Me.longitude.Text
olocations.latitude=Me.latitude.Text
result=olocations.Save()
if not result then throw olocations.LastError
_CurrentPk=olocations.location_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=olocations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If olocations is Nothing then
   olocations = new Objects.locations(currentuser,actualuser,pk,CDC)
  End If
  olocations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If olocations is Nothing then
   olocations = new Objects.locations(currentuser,actualuser,pk,CDC)
  End If
  olocations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If olocations is Nothing then
   olocations = new Objects.locations(currentuser,pk,CDC)
  End If
  olocations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.buildingprefix.Enabled=IIF(WL>=0,True,False)
  Me.blkbuildingprefix.Visible=IIF(RL>=0,True,False)
  Me.streetnumber.Enabled=IIF(WL>=0,True,False)
  Me.blkstreetnumber.Visible=IIF(RL>=0,True,False)
  Me.street.Enabled=IIF(WL>=0,True,False)
  Me.blkstreet.Visible=IIF(RL>=0,True,False)
  Me.district.Enabled=IIF(WL>=0,True,False)
  Me.blkdistrict.Visible=IIF(RL>=0,True,False)
  Me.town.Enabled=IIF(WL>=0,True,False)
  Me.blktown.Visible=IIF(RL>=0,True,False)
  Me.county_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkcounty_fk.Visible=IIF(RL>=0,True,False)
  Me.country_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkcountry_fk.Visible=IIF(RL>=0,True,False)
  Me.postcode.Enabled=IIF(WL>=0,True,False)
  Me.blkpostcode.Visible=IIF(RL>=0,True,False)
  Me.longitude.Enabled=IIF(WL>=0,True,False)
  Me.blklongitude.Visible=IIF(RL>=0,True,False)
  Me.latitude.Enabled=IIF(WL>=0,True,False)
  Me.blklatitude.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

