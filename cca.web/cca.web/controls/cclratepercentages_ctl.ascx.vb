Partial Class cclratepercentages_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents occlratepercentages As Objects.cclratepercentages
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_fuels_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.fuel_fk.Items.Count=0 Then
        Me.fuel_fk.DataSource=CDC.ReadDataTable(CO)
        Me.fuel_fk.DataTextField = "value"
        Me.fuel_fk.DataValueField = "pk"
        Try
            Me.fuel_fk.DataBind
        Catch Ex as Exception
            Me.fuel_fk.SelectedValue=-1
            Me.fuel_fk.DataBind
        End Try
    End If
            Me.cclpercentage.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If occlratepercentages is Nothing then
occlratepercentages = new Objects.cclratepercentages(currentuser,actualuser,pk,CDC)
End If
Me.fuel_fk.SelectedValue=occlratepercentages.fuel_fk
Me.valid_from.Text=occlratepercentages.valid_from.ToString("dd MMM yyyy")
If Me.valid_from.Text="01 Jan 0001" then Me.valid_from.Text=""
Me.valid_from.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.valid_from.Attributes.Add("onBlur", "clearNotice()")
Me.valid_to.Text=occlratepercentages.valid_to.ToString("dd MMM yyyy")
If Me.valid_to.Text="01 Jan 0001" then Me.valid_to.Text=""
Me.valid_to.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.valid_to.Attributes.Add("onBlur", "clearNotice()")
Me.cclpercentage.Text=occlratepercentages.cclpercentage
Me.cclpercentage.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.cclpercentage.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=occlratepercentages.cclratepercentage_pk
Status=occlratepercentages.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If occlratepercentages is Nothing then
occlratepercentages = new Objects.cclratepercentages(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
occlratepercentages.fuel_fk=Me.fuel_fk.SelectedValue
occlratepercentages.valid_from=CommonFN.CheckEmptyDate(Me.valid_from.Text)
occlratepercentages.valid_to=CommonFN.CheckEmptyDate(Me.valid_to.Text)
occlratepercentages.cclpercentage=Me.cclpercentage.Text
result=occlratepercentages.Save()
if not result then throw occlratepercentages.LastError
_CurrentPk=occlratepercentages.cclratepercentage_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=occlratepercentages.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If occlratepercentages is Nothing then
   occlratepercentages = new Objects.cclratepercentages(currentuser,actualuser,pk,CDC)
  End If
  occlratepercentages.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If occlratepercentages is Nothing then
   occlratepercentages = new Objects.cclratepercentages(currentuser,actualuser,pk,CDC)
  End If
  occlratepercentages.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If occlratepercentages is Nothing then
   occlratepercentages = new Objects.cclratepercentages(currentuser,pk,CDC)
  End If
  occlratepercentages.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.fuel_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkfuel_fk.Visible=IIF(RL>=0,True,False)
  Me.valid_from.Enabled=IIF(WL>=0,True,False)
  Me.blkvalid_from.Visible=IIF(RL>=0,True,False)
  Me.valid_to.Enabled=IIF(WL>=0,True,False)
  Me.blkvalid_to.Visible=IIF(RL>=0,True,False)
  Me.cclpercentage.Enabled=IIF(WL>=0,True,False)
  Me.blkcclpercentage.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

