Imports System.Data.SqlClient

Partial Class headctls_notifications
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Dim _label As String = ""
    Dim _notices As Integer = 0

    Public ReadOnly Property Notices() As Integer
        Get
            Return _notices
        End Get
    End Property

    Public ReadOnly Property State() As String
        Get
            Select Case _notices
                Case 0 : Return " green"
                Case Else : Return " red"
            End Select
        End Get
    End Property

    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Refresh()
    End Sub

    Public Sub Refresh()
        Dim CO As New SqlCommand("rsp_usernoticestoread")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@user_pk", SM.currentuser)
        CDC.ReadScalarValue(_notices, CO)
        CO.Dispose()
        ' TODO: SM.NoticeCount = _notices
        Dim ctl As String = ""
        ctl &= "<a href=""./notify.aspx"" class=""notifylink"">"
        ctl &= "<div class=""notify"
        ctl &= State
        ctl &= """><span class=""count"">"
        ctl &= Notices
        ctl &= "</span><br /><span class=""counttype"">"
        ctl &= Label
        ctl &= "</span></div></a>"
        notifyitem.Text = ctl
    End Sub

End Class
