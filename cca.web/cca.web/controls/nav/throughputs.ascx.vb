﻿
Partial Class controls_nav_throughputs
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim CO As SqlClient.SqlCommand
    Dim dt As DataTable

    Dim iAgreement As Integer = 0

    Public Function LoadList() As Boolean
        If isViable() Then

            'Agreement Meters
            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementthroughputs_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            dt = CDC.ReadDataTable(CO)
            rptThroughputs.DataSource = dt
            rptThroughputs.DataBind()

            dt.Dispose()
            dt = Nothing
            CO.Dispose()
            CO = Nothing
            Return True
        Else
            Return False
        End If
    End Function

    Private Function isViable() As Boolean
        Try

            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                iAgreement = Integer.Parse(Request.QueryString("pk"))
                If SM.targetAgreement <> iAgreement Then
                    SM.targetAgreement = iAgreement
                    SM.Save()
                End If
            End If
            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
