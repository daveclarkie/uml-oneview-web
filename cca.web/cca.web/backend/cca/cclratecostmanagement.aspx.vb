﻿
Partial Class backend_cca_cclratecostmanagement
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        Try
            PSM = SM

            validpk = Integer.TryParse(Request.QueryString("pk"), pk)

            If Not (Request.QueryString("r") Is Nothing) Then 'reason
                AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/core/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Dim cc As Object
    Dim validpk As Boolean = False
    Dim pk As Integer = 0

    Private Sub PageLoad()
        RefreshList()
    End Sub

    Private Sub DisplayPage()

        '' ===============================================
        ''  Which Action?
        '' ===============================================

    End Sub

    Private Sub RefreshList()

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_cclratecosts_display"
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptCCLRateCosts.DataSource = dt
        rptCCLRateCosts.DataBind()
        CO.Dispose()

    End Sub


    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control)
        TargetControl.Controls.Clear()

        If Request.QueryString("pk") Is Nothing Then
            TargetButton.Visible = False
            RefreshList()
        Else
            cc = LoadControl(controlpath)
            cc.CDC = CDC
            cc.SM = SM
            cc.Read(SM.currentuser, SM.actualuser, Request.QueryString("pk"))
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        End If

    End Sub
    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PageLoad()
    End Sub

End Class
