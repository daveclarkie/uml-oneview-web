
Partial Class design_masters_systems
    Inherits DaveMaster
    Public PSM As MySM
    Public Sub InitMaster() Handles MyBase.InitSpecificMaster
        ' Place any page specific initialisation code here
        PSM = SM
        CheckSecurity()
    End Sub
    Public Sub CompleteMaster() Handles MyBase.CompleteSpecificMaster
        ' Place any page specific initialisation code here
    End Sub

    Private Sub CheckSecurity()
        If Not Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC) Then
            Local.Bounce(SM, Response, "~/core/default.aspx", "not+a+member+of+systems")
        End If
        Dim u As New users(SM.currentuser, SM.actualuser, SM.currentuser, CDC)

        If u.disconnect Then
            u.disconnect = False
            u.Save()
            Local.FOAD(Response, SM, "disconnected+so+you+need+to+log+in+again")
        End If

        Dim actlog As New activitylogs(SM.currentuser, SM.actualuser, CDC)
        actlog.ipaddress = Request.UserHostAddress
        actlog.useragent = Request.UserAgent
        actlog.user_fk = SM.currentuser
        actlog.Save()
        actlog = Nothing

    End Sub

End Class

