Partial Class logreports_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ologreports As Objects.logreports
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.target.Text="0"
            Me.period.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ologreports is Nothing then
ologreports = new Objects.logreports(currentuser,actualuser,pk,CDC)
End If
Me.reporturl.Text=ologreports.reporturl
Me.reporttype.Text=ologreports.reporttype
Me.target.Text=ologreports.target
Me.target.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.target.Attributes.Add("onBlur", "clearNotice()")
Me.period.Text=ologreports.period
Me.period.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.period.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=ologreports.logreport_pk
Status=ologreports.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ologreports is Nothing then
ologreports = new Objects.logreports(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ologreports.reporturl=Me.reporturl.Text
ologreports.reporttype=Me.reporttype.Text
ologreports.target=Me.target.Text
ologreports.period=Me.period.Text
result=ologreports.Save()
if not result then throw ologreports.LastError
_CurrentPk=ologreports.logreport_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ologreports.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreports is Nothing then
   ologreports = new Objects.logreports(currentuser,actualuser,pk,CDC)
  End If
  ologreports.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreports is Nothing then
   ologreports = new Objects.logreports(currentuser,actualuser,pk,CDC)
  End If
  ologreports.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreports is Nothing then
   ologreports = new Objects.logreports(currentuser,pk,CDC)
  End If
  ologreports.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.reporturl.Enabled=IIF(WL>=0,True,False)
  Me.blkreporturl.Visible=IIF(RL>=0,True,False)
  Me.reporttype.Enabled=IIF(WL>=0,True,False)
  Me.blkreporttype.Visible=IIF(RL>=0,True,False)
  Me.target.Enabled=IIF(WL>=0,True,False)
  Me.blktarget.Visible=IIF(RL>=0,True,False)
  Me.period.Enabled=IIF(WL>=0,True,False)
  Me.blkperiod.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

