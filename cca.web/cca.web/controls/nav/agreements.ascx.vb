﻿
Imports Microsoft.Reporting.WebForms

Partial Class controls_nav_agreements
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim CO As SqlClient.SqlCommand
    Dim dt As DataTable

    Dim iAgreement As Integer = 0

    Public Function LoadList() As Boolean
        If isViable() Then

            Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetagreement, CDC)
            Dim f As New federations(SM.currentuser, SM.actualuser, a.federation_fk, CDC)
            Dim at As New agreementtypes(SM.currentuser, SM.actualuser, a.agreementtype_fk, CDC)
            Dim am As New agreementmeasures(SM.currentuser, SM.actualuser, a.agreementmeasure_fk, CDC)

            CO = New SqlCommand("rsp_agreement_currentversionlevel")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetagreement)
            CDC.ReadScalarValue(lblAgreementVersion.Text, CO)

            lblFederation.Text = f.shortcode
            lblAgreementName.Text = a.agreementname
            lblAgreementNumber.Text = a.agreementnumber
            lblAgreementType.Text = at.typename
            lblAgreementMeasure.Text = am.measurename

            'Selected Site
            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreements_list"
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetagreement)
            CO.Parameters.AddWithValue("@mode", 1)     ' this one only
            dt = CDC.ReadDataTable(CO)
            rptVersions.DataSource = dt
            rptVersions.DataBind()
            If rptVersions.DataSource.rows.count = 0 Then : pnlOtherVersions.Visible = False : End If

            DeploymentLocation.NavigateUrl = "file://" & FileLocation



            If SM.targetDataEntry > 0 And SM.AgreementPageMode = 6 Then
                'Selected Meter
                Dim smp As New agreementmeterpoints(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)
                Dim dr As DataRow
                CO = New SqlClient.SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_meterpoint_details"
                CO.Parameters.AddWithValue("@meterpoint_pk", smp.meterpoint_fk)
                dr = CDC.ReadDataRow(CO)
                Dim fuel As New fuels(SM.currentuser, SM.actualuser, dr("fuel_fk"), CDC)

                lblFuelName.Text = fuel.fuel
                lblMeter.Text = dr("meter")
            End If




            Return True
        Else
            Return False
        End If
    End Function


    Private Function isViable() As Boolean
        Try

            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                iAgreement = Integer.Parse(Request.QueryString("pk"))
                If SM.targetagreement <> iAgreement Then
                    SM.targetagreement = iAgreement
                    SM.Save()
                End If
            End If
            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub open_window()
        If SM.targetAgreement > 0 Then
            Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
            Dim o As New organisations(SM.currentuser, SM.actualuser, a.organisation_fk, CDC)

            CO = New SqlCommand("rsp_systemparameters_latest")
            CO.CommandType = CommandType.StoredProcedure
            Dim systemparameter_pk As Integer = -1
            CDC.ReadScalarValue(systemparameter_pk, CO)
            Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

            Dim exploreInfo As New System.Diagnostics.ProcessStartInfo()
            exploreInfo.FileName = "explorer.exe"

            exploreInfo.Arguments = sys.customerlocation & o.customername & "\Energy Management\"
            'Response.Write(exploreInfo.Arguments.ToString())
            System.Diagnostics.Process.Start(exploreInfo)

        End If
    End Sub

    Protected Sub lnkCustomerFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustomerFolder.Click
        open_window()
    End Sub


    Public ReadOnly Property FileLocation() As String
        Get

            Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
            Dim o As New organisations(SM.currentuser, SM.actualuser, a.organisation_fk, CDC)

            CO = New SqlCommand("rsp_systemparameters_latest")
            CO.CommandType = CommandType.StoredProcedure
            Dim systemparameter_pk As Integer = -1
            CDC.ReadScalarValue(systemparameter_pk, CO)
            Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)
            Dim retval As String = sys.customerlocation & o.customername & "\Energy Management\"
            Return retval
        End Get
    End Property

    Protected Sub btnExportAgreementData_Click(sender As Object, e As System.EventArgs) Handles btnExportAgreementData.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


        Dim ag As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        ' Load Agreement Data Export Report
        rp.Read(16)

        If rp.rowstatus = 0 Then

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport


            'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)
            'Set the report server URL and report path
            'serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
            'serverReport.ReportPath = "/Customer Reports/Energy Management/Phase 2 - Absolute CCLA MTS"
            serverReport.ReportServerUrl = New Uri(rp.reportserverurl)
            serverReport.ReportPath = rp.reportpath


            Dim Agreement As New Microsoft.Reporting.WebForms.ReportParameter()
            Agreement.Name = "agreement_pk"
            Agreement.Values.Add(SM.targetAgreement)


            Dim filePath As String = sys.datalocation & "Reports\16.One View Data Export - Agreement\" & SM.targetAgreement & "\"       'HttpContext.Current.Server.MapPath("~/App_Data/DS/")
            If (Not System.IO.Directory.Exists(filePath)) Then
                System.IO.Directory.CreateDirectory(filePath)
            End If

            Dim datestring As String = Now.ToString("yyyyMMdd_HHmmss_")

            Dim rt As New reporttypes(SM.currentuser, SM.actualuser, rp.reporttype_fk, CDC)

            Dim rl As New reportlogs(SM.currentuser, SM.actualuser, CDC)


            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Agreement}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("Excel", Nothing, mimeType, encoding, extension, streamids, warnings)

            rl.report_fk = rp.report_pk
            rl.runfromurl = Request.Url.AbsoluteUri
            rl.param1name = "agreement_pk"
            rl.param1value = SM.targetAgreement
            rl.param2name = ""
            rl.param2value = ""
            rl.param3name = ""
            rl.param3value = ""
            rl.param4name = ""
            rl.param4value = ""
            rl.savedpath = filePath & datestring & rp.reportname + "." + extension
            rl.Save()

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' \\uk-ol1-file-01\Group Shares\Data\Reports\00.CCA MTS Reporting\
            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + datestring & "One_View_Data_Export" + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)

            IO.File.WriteAllBytes(filePath & datestring & rp.reportname + "." + extension, bytes)

            Response.Flush()
            Response.[End]()


        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If
    End Sub

    Private Sub btnExportAgreementAuditData_Click(sender As Object, e As EventArgs) Handles btnExportAgreementAuditData.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


        Dim ag As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        ' Load One View Agreement Audit Report
        rp.Read(18)

        If rp.rowstatus = 0 Then

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport


            'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)
            'Set the report server URL and report path
            'serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
            'serverReport.ReportPath = "/Customer Reports/Energy Management/Phase 2 - Absolute CCLA MTS"
            serverReport.ReportServerUrl = New Uri(rp.reportserverurl)
            serverReport.ReportPath = rp.reportpath


            Dim Agreement As New Microsoft.Reporting.WebForms.ReportParameter()
            Agreement.Name = "agreement_pk"
            Agreement.Values.Add(SM.targetAgreement)


            Dim filePath As String = sys.datalocation & "Reports\18.One View Agreement Audit Report\" & SM.targetAgreement & "\"       'HttpContext.Current.Server.MapPath("~/App_Data/DS/")
            If (Not System.IO.Directory.Exists(filePath)) Then
                System.IO.Directory.CreateDirectory(filePath)
            End If

            Dim datestring As String = Now.ToString("yyyyMMdd_HHmmss_")

            Dim rt As New reporttypes(SM.currentuser, SM.actualuser, rp.reporttype_fk, CDC)

            Dim rl As New reportlogs(SM.currentuser, SM.actualuser, CDC)


            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Agreement}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("Excel", Nothing, mimeType, encoding, extension, streamids, warnings)

            rl.report_fk = rp.report_pk
            rl.runfromurl = Request.Url.AbsoluteUri
            rl.param1name = "agreement_pk"
            rl.param1value = SM.targetAgreement
            rl.param2name = ""
            rl.param2value = ""
            rl.param3name = ""
            rl.param3value = ""
            rl.param4name = ""
            rl.param4value = ""
            rl.savedpath = filePath & datestring & rp.reportname + "." + extension
            rl.Save()

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' \\uk-ol1-file-01\Group Shares\Data\Reports\00.CCA MTS Reporting\
            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + datestring & "One_View_Agreement_Audit_Export" + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)

            IO.File.WriteAllBytes(filePath & datestring & rp.reportname + "." + extension, bytes)

            Response.Flush()
            Response.[End]()


        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If
    End Sub

End Class
