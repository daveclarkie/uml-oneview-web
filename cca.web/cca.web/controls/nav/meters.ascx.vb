﻿
Partial Class controls_nav_meters
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim CO As SqlClient.SqlCommand
    Dim dt As DataTable

    Dim iAgreement As Integer = 0

    Public Function LoadList() As Boolean
        If isViable() Then

            'Agreement Meters
            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementmeterpoints_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            dt = CDC.ReadDataTable(CO)
            rptMeterPoints.DataSource = dt
            rptMeterPoints.DataBind()

            ''Selected Meter
            'Dim smp As New agreementmeterpoints(SM.currentuser, SM.actualuser, SM.targetAgreementSecondary, CDC)
            'Dim dr As DataRow
            'CO = New SqlClient.SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_meterpoint_details"
            'CO.Parameters.AddWithValue("@meterpoint_pk", smp.meterpoint_fk)
            'dr = CDC.ReadDataRow(CO)
            'Dim f As New fuels(SM.currentuser, SM.actualuser, dr("fuel_fk"), CDC)
            'Dim u As New units(SM.currentuser, SM.actualuser, dr("unit_fk"), CDC)

            'lblMeter.Text = dr("meter")
            'lblFuel.Text = f.fuel
            'lblUnit.Text = u.unitname

            'CO = New SqlClient.SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_meterpoint_latestdata"
            'CO.Parameters.AddWithValue("@meterpoint_pk", smp.meterpoint_fk)
            'CDC.ReadScalarValue(lblDataEntered.Text, CO)



            ''Selected Throughput
            'Dim atp As New agreementthroughputs(SM.currentuser, SM.actualuser, SM.targetAgreementSecondary, CDC)
            'Dim tp As New throughputs(SM.currentuser, SM.actualuser, atp.throughput_fk, CDC)
            'u = New units(SM.currentuser, SM.actualuser, tp.unit_fk, CDC)

            'lblThroughputName.Text = tp.throughputname

            'lblThroughputUnit.Text = u.unitname

            'CO = New SqlClient.SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_throughput_latestdata"
            'CO.Parameters.AddWithValue("@agreementthroughput_pk", SM.targetAgreementSecondary)
            'CDC.ReadScalarValue(lblThroughputLatestData.Text, CO)


            dt.Dispose()
            dt = Nothing
            CO.Dispose()
            CO = Nothing
            Return True
        Else
            Return False
        End If
    End Function


    Private Function isViable() As Boolean
        Try

            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                iAgreement = Integer.Parse(Request.QueryString("pk"))
                If SM.targetagreement <> iAgreement Then
                    SM.targetagreement = iAgreement
                    SM.Save()
                End If
            End If
            If Request.RawUrl.ToLower.Contains("/agreements.aspx") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
