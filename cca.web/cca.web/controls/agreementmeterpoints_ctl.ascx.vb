Partial Class agreementmeterpoints_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreementmeterpoints As Objects.agreementmeterpoints
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_frequencys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.datafrequency_fk.Items.Count = 0 Then
            Me.datafrequency_fk.DataSource = CDC.ReadDataTable(CO)
            Me.datafrequency_fk.DataTextField = "value"
            Me.datafrequency_fk.DataValueField = "pk"
            Try
                Me.datafrequency_fk.DataBind()
            Catch Ex As Exception
                Me.datafrequency_fk.SelectedValue = -1
                Me.datafrequency_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rsp_agreementthroughputs_lookup_agreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
        If Me.agreementthroughput_fk.Items.Count = 0 Then
            Me.agreementthroughput_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreementthroughput_fk.DataTextField = "value"
            Me.agreementthroughput_fk.DataValueField = "pk"
            Try
                Me.agreementthroughput_fk.DataBind()
            Catch Ex As Exception
                Me.agreementthroughput_fk.SelectedValue = -1
                Me.agreementthroughput_fk.DataBind()
            End Try
        End If

        CO = New SqlClient.SqlCommand("rkg_meterpoints_lookup_organisation")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
        If Me.meterpoint_fk.Items.Count = 0 Then
            Me.meterpoint_fk.DataSource = CDC.ReadDataTable(CO)
            Me.meterpoint_fk.DataTextField = "value"
            Me.meterpoint_fk.DataValueField = "pk"
            Try
                Me.meterpoint_fk.DataBind()
            Catch Ex As Exception
                Me.meterpoint_fk.SelectedValue = -1
                Me.meterpoint_fk.DataBind()
            End Try
        End If

        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oagreementmeterpoints Is Nothing Then
            oagreementmeterpoints = New objects.agreementmeterpoints(currentuser, actualuser, pk, CDC)
        End If

        Me.agreement_fk.SelectedValue = SM.targetAgreement
        Me.meterpoint_fk.Text = oagreementmeterpoints.meterpoint_fk
        Me.datafrequency_fk.SelectedValue = oagreementmeterpoints.datafrequency_fk
        Me.agreementthroughput_fk.SelectedValue = oagreementmeterpoints.agreementthroughput_fk
        _CurrentPk = oagreementmeterpoints.agreementmeterpoint_pk
        Status = oagreementmeterpoints.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementmeterpoints Is Nothing Then
            oagreementmeterpoints = New Objects.agreementmeterpoints(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreementmeterpoints.agreement_fk = SM.targetAgreement
            oagreementmeterpoints.meterpoint_fk = Me.meterpoint_fk.SelectedValue
            oagreementmeterpoints.datafrequency_fk = Me.datafrequency_fk.SelectedValue
            oagreementmeterpoints.agreementthroughput_fk = Me.agreementthroughput_fk.SelectedValue
            result = oagreementmeterpoints.Save()
            If Not result Then Throw oagreementmeterpoints.LastError
            _CurrentPk = oagreementmeterpoints.agreementmeterpoint_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreementmeterpoints.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementmeterpoints Is Nothing Then
            oagreementmeterpoints = New Objects.agreementmeterpoints(currentuser, actualuser, pk, CDC)
        End If
        oagreementmeterpoints.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementmeterpoints Is Nothing Then
            oagreementmeterpoints = New Objects.agreementmeterpoints(currentuser, actualuser, pk, CDC)
        End If
        oagreementmeterpoints.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementmeterpoints Is Nothing Then
            oagreementmeterpoints = New Objects.agreementmeterpoints(currentuser, pk, CDC)
        End If
        oagreementmeterpoints.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()

        Dim ag As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)

        Dim RL As Integer = cca.common.Security.ReadLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Me.agreement_fk.Enabled = IIf(WL >= 10, True, False)
        Me.blkagreement_fk.Visible = IIf(RL >= 10, True, False)
        Me.meterpoint_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkmeterpoint_fk.Visible = IIf(RL >= 0, True, False)
        Me.datafrequency_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkdatafrequency_fk.Visible = IIf(RL >= 0, True, False)

        If ag.federation_fk = 9 Then 'MPMA
            Me.agreementthroughput_fk.Enabled = IIf(WL >= 0, True, False)
            Me.blkagreementthroughput_fk.Visible = IIf(RL >= 0, True, False)
        Else
            Me.agreementthroughput_fk.Enabled = IIf(WL >= 100, True, False)
            Me.blkagreementthroughput_fk.Visible = IIf(RL >= 100, True, False)
        End If

        
    End Sub
End Class

