<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrix1details_ctl.ascx.vb" Inherits="matrix1details_ctl" %>
<h5>matrix1details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkprofile_fk">
<span class='label'>Profile Class</span>
<asp:DropDownList ID="profile_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmetertimeswitchcode">
<span class='label'>MTC</span>
<asp:TextBox EnableViewState="false" ID="metertimeswitchcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blklinelossfactor">
<span class='label'>LLF</span>
<asp:TextBox EnableViewState="false" ID="linelossfactor" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkdistribution_fk">
<span class='label'>Distribution</span>
<asp:DropDownList ID="distribution_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkuniqueidentifier">
<span class='label'>Unique Identifier</span>
<asp:TextBox EnableViewState="false" ID="uniqueidentifier" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcheckdigit">
<span class='label'>Check Digit</span>
<asp:TextBox EnableViewState="false" ID="checkdigit" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkconsumptionsingle">
<span class='label'>Single Consumption</span>
<asp:TextBox EnableViewState="false" ID="consumptionsingle" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkconsumptionday">
<span class='label'>Day Consumption</span>
<asp:TextBox EnableViewState="false" ID="consumptionday" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkconsumptionnight">
<span class='label'>Night Consumption</span>
<asp:TextBox EnableViewState="false" ID="consumptionnight" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkconsumptionother">
<span class='label'>Other Consumption</span>
<asp:TextBox EnableViewState="false" ID="consumptionother" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkcurrent_standingcharge2">
<span class='label'>Current Standing Charge</span>
<asp:TextBox EnableViewState="false" ID="current_standingcharge2" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcurrent_ratesingle">
<span class='label'>Current Single Rate</span>
<asp:TextBox EnableViewState="false" ID="current_ratesingle" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcurrent_rateday">
<span class='label'>Current Day Rate</span>
<asp:TextBox EnableViewState="false" ID="current_rateday" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcurrent_ratenight">
<span class='label'>Current Night Rate</span>
<asp:TextBox EnableViewState="false" ID="current_ratenight" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcurrent_rateother">
<span class='label'>Current Other Rate</span>
<asp:TextBox EnableViewState="false" ID="current_rateother" runat="server" cssClass="input_dbl" />
</li>
</ul>

