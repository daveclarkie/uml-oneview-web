<%@ WebHandler Language="VB" Class="function_dataanalysisrem" %>
Public Class function_dataanalysisrem
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Try
        
            Dim rf As Object = Request.QueryString
            Dim dca As New dataconsumptionanalysis(SM.currentuser, SM.actualuser, CDC)
            
            If rf("pk") <> "" Then
                dca.Read(Integer.Parse(rf("pk")))
                If dca.Delete() Then
                    Response.Write("OK")
                Else
                    Response.Write("Fail:Can't Delete")
                End If
            Else
                Response.Write("Fail:No item specified")
            End If
            
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class