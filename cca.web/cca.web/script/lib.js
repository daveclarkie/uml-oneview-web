﻿/* ============================================================= */
/*         JS object/property/method/function tests              */
/* ============================================================= */

// Test for innerText vs textContent (everything vs IE)
var hasInnerText =(document.getElementsByTagName("head")[0].innerText != undefined) ? true : false;
// Can we use HttpRequests
var hasXmlhttp=false;
var xmlhttp;
var blockedhttp="Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.";

/* ============================================================= */
/*                   JS common variables                         */
/* ============================================================= */

// chgMode constants
var chgAfter=0;
var chgBefore=1;
var chgClear=2;
var chgReplace=3;

// class based image loading variables

var waitImagesLoaded=0;
var waitClassName='wait';
var waitImageUrl='../design/images/ajax-loader.gif';
var waitAltTag='...';

/*
Example:
var [className]ImagesLoaded=0;
var [className]ClassName='[className]';
var [className]ImageUrl='../design/images/ajax-loader.gif';
var [className]AltTag='...';
*/


/* ============================================================= */
/*                   JS common functions                         */
/* ============================================================= */

function showhide(trg,src){
        var tlm=document.getElementById(trg);
        var slm=document.getElementById(src);
        

        if (tlm.style.display=='none'){
            chgElementText(slm,chgReplace,'[Hide]'); 
            tlm.style.display='';
        }else{
            chgElementText(slm,chgReplace,'[Show]'); 
            tlm.style.display='none';
        }


}
function showHide(trg){
        var tlm=document.getElementById(trg);
        if (tlm.style.display=='none'){
            tlm.style.display='';
        }else{
            tlm.style.display='none';
        }


}




/* ============================================================= */
/*                   Dynamic Text Updates                        */
/* ============================================================= */
/* Main Functions                                                */
/*  chgNamedElementText (id,chgMode[,content])                   */
/*  chgElementText (object,chgMode[,content])                    */
/* ============================================================= */

// change content of supplied element using chgMode
function chgNamedElementText(elemname,mode,content){
    try {
        var elem=document.getElementById(elemname);
        switch (mode){
            case chgAfter: addElementText(elem,content); break;   
            case chgBefore: preElementText(elem,content); break;   
            case chgClear: clrElementText(elem); break;   
            case chgReplace: setElementText(elem,content); break;   
            default: setElementText(elem,content); break;   
        }
    }
    catch(e){
    }            
}
// change content of supplied element using chgMode
function chgElementText(elem,mode,content){
    switch (mode){
        case chgAfter: addElementText(elem,content); break;   
        case chgBefore: preElementText(elem,content); break;   
        case chgClear: clrElementText(elem); break;   
        case chgReplace: setElementText(elem,content); break;   
        default: setElementText(elem,content); break;   
    }
}
// Set content of supplied element
function setElementText(elem,content){
    if (content.indexOf("</")>-1||content.indexOf("/>")>-1){
            elem.innerHTML=content;
   } else {  
        if (hasInnerText){
            elem.innerText=content;
        }else{
            elem.textContent=content;
        }
    } 
}
// Clear content of supplied element
function clrElementText(elem){
    if (hasInnerText){
        elem.innerText="";
    }else{
        elem.textContent="";
    }
}
// Append to content of supplied element
function addElementText(elem,content){
    if (content.indexOf("</")>-1||content.indexOf("/>")>-1){
            elem.innerHTML+=content;
   } else {  
        if (hasInnerText){
            elem.innerText+=content;
        }else{
            elem.textContent+=content;
        }
    }
}
// Prepend to content of supplied element
function preElementText(elem,content){
    if (content.indexOf("</")>-1||content.indexOf("/>")>-1){
            elem.innerHTML=content+elem.innerHTML;
   } else {  
        if (hasInnerText){
            elem.innerText=content+elem.innerText;
        }else{
            elem.textContent=content+elem.textContent;
        }
    }
}

/* ============================================================= */
/*                   Variable Content Testing                    */
/* ============================================================= */
/* Main Functions                                                */
/*  isNumeric(content) {True/False}                              */
/*  isLower(content) {True/False}                                */
/*  isUpper(content) {True/False}                                */
/*  isSpecial(content) {True/False}                              */
/* ============================================================= */
function isNumeric(val){
	return !isNaN(parseFloat(val)) && isFinite(val);
}
function isLower(val){
	var m=val.match(/[a-z]/g);
	if (m==null||m=="") {return false;}else{return true;}
}
function isUpper(val){
	var m=val.match(/[A-Z]/g);
	if (m==null||m=="") {return false;}else{return true;}
}
function isSpecial(val){
	var t=(!isNumeric(val))&&(!isLower(val))&&(!isUpper(val));
	return t;
}

/* ============================================================= */
/*                   Content Conversion                          */
/* ============================================================= */
/* Main Functions                                                */
/*  intToByte(content) {Hex String}                              */
/* ============================================================= */
function intToByte(val) {
    return Number(val).toString(16);
}

/* ============================================================= */
/*          Load Images When Required Based On Class             */
/* ============================================================= */
/* Main Functions                                                */
/*  setImages(className)                                         */
/* ============================================================= */
function setImages(cls){
    try {
        var imagesLoaded=eval(cls+"ImagesLoaded");
        var className=eval(cls+"ClassName");
        var imageUrl=eval(cls+"ImageUrl");
        var altTag=eval(cls+"AltTag");
        
        if (imagesLoaded==0){
            eval(cls+"ImagesLoaded=1");
            var elems=document.getElementsByTagName("img");
            for (var i=0;i<elems.length;i++){

                // if (elems[i].getAttribute('class')==className){  // Bloody IE!!!
                if (elems[i].className==className){
                    elems[i].setAttribute('src',imageUrl);
                    elems[i].setAttribute('alt',altTag);
                }
            }
        }
    }
    catch (e){
        debugW(e); 
    }
}

/* ============================================================= */
/*             Regex Based Replace Functions                     */
/* ============================================================= */
/* Main Functions                                                */
/*  rpll(originalText,regExp,replacementText) (string)           */
/*  rpl(originalText,targetText,replacementText) (string)        */
/* ============================================================= */
function rpll(full,rx,neu){
	return full.replace(rx,neu);
}
function rpl(full,old,neu){
	var rx=new RegExp(old,"g");
	return full.replace(rx,neu);
}


/* ============================================================= */
/*             create XML parser from a given string                     */
/* ============================================================= */

function loadXMLString(txt)
{
if (window.DOMParser)
  {
  parser=new DOMParser();
  xmlDoc=parser.parseFromString(txt,"text/xml");
  }
else // Internet Explorer
  {
  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
  xmlDoc.async="false";
  xmlDoc.loadXML(txt);
  }
return xmlDoc;
}


/* ============================================================= */
/*             Request External Page                     */
/* ============================================================= */
/* Main Functions                                                */
/*  rpll(originalText,regExp,replacementText) (string)           */
/*  rpl(originalText,targetText,replacementText) (string)        */
/* ============================================================= */


function setReqHttp(){
    if (hasXmlhttp==true){return;}
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
// JScript gives us Conditional compilation, we can cope with old IE versions.
// and security blocked creation of the objects.
 try {
  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  hasXmlhttp=true;
 } catch (e) {
  try {
   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  hasXmlhttp=true;
  } catch (E) {
   xmlhttp = false;
  }
 }
@end @*/
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	    try {
		    xmlhttp = new XMLHttpRequest();
            hasXmlhttp=true;
	    } catch (e) {
		    xmlhttp=false;
	    }
    }
    if (!xmlhttp && window.createRequest) {
	    try {
		    xmlhttp = window.createRequest();
            hasXmlhttp=true;
	    } catch (e) {
		    xmlhttp=false;
	    }
    }
}

//wait
/*
showAsyncAction(trgname,1,"wait"){




*/
// Callback function 

var trgt,icontrgt;

function callback_addc2p(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail"){
                alert("Failed to add client.\nTry again later.");
            } else if (rspn=="Dupe"){
                alert("Client already added.");
            }else{
                rspn=rpl(rspn,"OK:","");
                var newLink = document.createElement('a'); 
                var newLi = document.createElement('li');
                var newLinkText = document.createTextNode(' [Remove]');
                var newClient = document.createTextNode(sClient+' ');
                newLink.setAttribute("href","javascript:remc2p("+rspn+");"); 
                newLink.appendChild(newLinkText);
                newLi.appendChild(newClient);
                newLi.setAttribute("id","li_"+rspn); 
                newLi.appendChild(newLink);
                trgt.appendChild(newLi);
            }
            showAsyncAction(icontrgt,0,"wait") 
        }
    }
}

function addc2p(client,clientid,itemid,packid,typeid){
    icontrgt="assocclients";
    trgt=document.getElementById("activeusers");
    sClient=client;
    sClientid=clientid;
    sItemid=itemid;
    sPackid=packid;
   sTypeid=typeid; 
    setReqHttp();
    if (hasXmlhttp==true){
        showAsyncAction(icontrgt,1,"wait") 
        xmlhttp.open("GET","../functions/addcli2prd.aspx?ci="+clientid+"&ii="+itemid+"&pi="+packid+"&ti="+typeid+noCache());
        callback_addc2p();
        xmlhttp.send(null);   
    }
   else
   {
    alert(blockedhttp);
   } 
}

var sLinkId;

function callback_remc2p(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail"){
                alert("Failed to remove client because:-\n"+rpl(rspn,"Fail:",""));
            }else{
                var li=document.getElementById("li_"+sLinkId);
                trgt.removeChild(li);
            }
            showAsyncAction(icontrgt,0,"wait") 
        }
    }
}

function remc2p(linkid){
    icontrgt="assocclients";
    trgt=document.getElementById("activeusers");
    sLinkId=linkid;
    setReqHttp();
    if (hasXmlhttp==true){
        showAsyncAction(icontrgt,1,"wait") 
        xmlhttp.open("GET","../functions/remcli2prd.aspx?li="+linkid+noCache());
        callback_remc2p();
        xmlhttp.send(null);   
    }
   else
   {
    alert(blockedhttp);
   } 
}

function saveNote(elemNote,packOrUserId,packOrUserType) {
    var rqNote=newRequest();
    var rspn=""; 
    var noteText=document.getElementById(elemNote);
    var upload=document.getElementById('<%=FileUploadDocument.ClientID%>').FileName;
     
    if ( typeof rqNote != "boolean" ){
        var keys="p="+packOrUserId+"&t="+packOrUserType+"&url="+upload+"&n="+escape(noteText.value.replace(/\n\r/g, '<br />').replace(/\u00A3/g, '&pound;')); 
        rqNote.open("POST","/functions/addnote.ashx?a=0"+noCache(),false);
        rqNote.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        rqNote.send(keys);
        rspn=rqNote.responseText;
        alert(rspn);
        if ( rspn=="Note Saved" ) {window.location.reload();}
    } else {
        alert(blockedhttp); 
    }
}
