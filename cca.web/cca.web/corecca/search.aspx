<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="corecca_search" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Search</h3>
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <ul id="" class='formcontrol'>
                <li><span class="label">Federation</span><asp:Dropdownlist ID="ddlfederation" runat="server" CssClass="input_ddl" /></li>
                <li><span class="label">Agreement Number</span><asp:TextBox ID="agreement" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Agreement Name</span><asp:TextBox ID="agreementname" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Target Unit Identifier (TUID)</span><asp:TextBox ID="tuid" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Facility Number</span><asp:TextBox ID="facility" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Organisation</span><asp:TextBox ID="organisation" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Meter</span><asp:TextBox ID="meter" runat="server" CssClass="input_str" /></li>
                <li><span class="label">&nbsp;</span><asp:LinkButton ID="btnSearch" runat="server" Text="Locate" CausesValidation="False" /><asp:LinkButton ID="btnAgreementSearch" runat="server" Text="" CausesValidation="False" /></li>
            </ul>
            <asp:LinkButton ID="btnCreate" runat="server" Text="" CausesValidation="False" />
        </asp:Panel>
        
            <div id="frmCreateNewUser" class="modal_control"><div id="rspCreateNewUser"></div></div>
            
        <asp:Repeater ID="rptAgreement" runat="server">
            <HeaderTemplate>
                <h3>Results - Agreements</h3>
                <table >
                    <tr>
                        <td>
                                
                        </td>
                        <td>
                            <h4>Type</h4>
                        </td>
                        <td>
                            <h4>Agreement</h4>
                        </td>
                        <td>
                            <h4>Agreement Name</h4>
                        </td>
                        <td>
                            <h4>Organisation</h4>
                        </td>
                    </tr>
            </HeaderTemplate> 
            <ItemTemplate>
                    <tr>
                        <td style="width:60px;">
                            <%# returnselect(Container.DataItem("rowstatus"), Container.DataItem("url"))%>
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("type")%>
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("federationshort")%>/<%# Container.DataItem("agreementnumber")%> / <%# Container.DataItem("facilitynumber")%> 
                        </td>
                        <td style="width:230px;">
                            <%# Container.DataItem("agreementname")%> 
                        </td>
                        <td style="width:300px;">
                            <%# Container.DataItem("customer")%>
                        </td>
                    </tr>
            </ItemTemplate> 
            <FooterTemplate>
                
                </table>
            </FooterTemplate>
            </asp:Repeater>

            <table>
                <tr style="height:10px;"></tr>
                <tr>
                    <td style="width:150px;">
                        <asp:Label ID="lblSearchPage" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchPrev" runat="server" text="" OnClick="lnkPrev_Click" />
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchNext" runat="server" text="" OnClick="lnkNext_Click" />
                    </td>
                </tr>
            </table>

            <script language="javascript" type="text/javascript">
                var srchFederation = document.getElementById("<%=ddlfederation.ClientID %>");
                var srchBtn = document.getElementById("<%=btnSearch.ClientID %>");
                var srchAgreement = document.getElementById("<%=agreement.ClientID %>");
                var srchAgreementName = document.getElementById("<%=agreementname.ClientID %>");
                var srchTUID = document.getElementById("<%=tuid.ClientID %>");
                var srchFacility = document.getElementById("<%=facility.ClientID %>");
                var srchMeter = document.getElementById("<%=meter.ClientID %>");

                setEvent(srchFederation, "change", doSearch);
                setEvent(srchAgreement, "keyup", doSearch);
                setEvent(srchAgreementName, "keyup", doSearch);
                setEvent(srchTUID, "keyup", doSearch);
                setEvent(srchFacility, "keyup", doSearch);
                setEvent(srchMeter, "keyup", doSearch);
                
                //setOnLoad(killBtn);
            </script>


</asp:Content>

