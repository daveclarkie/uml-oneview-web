﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="eligibilitymanagement.aspx.vb" Inherits="backend_cca_eligibilitymanagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current Thresholds</h5>
<asp:Repeater ID="rptEligibilityThreshold" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:80px;">
            
        </td>
        <td style="width:80px;">
            <h4>Threshold</h4>
        </td>
        <td style="width:80px;">
            <h4>Correction</h4>
        </td>
        <td style="width:100px;">
            <h4>Submission From</h4>
        </td>
        <td style="width:100px;">
            <h4>Submission To</h4>
        </td>
        <td style="width:100px;">
            <h4>Reporting From</h4>
        </td>
        <td style="width:100px;">
            <h4>Reporting To</h4>
        </td>
        <td></td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("eligibilitythreshold_pk") %>'>
            <td>
                <a href='eligibilitymanagement.aspx?pk=<%# Container.DataItem("eligibilitythreshold_pk")%>'><h6>Edit</h6></a>
            </td>
            <td>
                <%# Container.DataItem("threshold")%>
            </td>
            <td>
                <%# Container.DataItem("correctionvalue")%>
            </td>
            <td>
                <%# Container.DataItem("submissionfrom")%>
            </td>
            <td>
                <%# Container.DataItem("submissionto")%>
            </td>
            <td>
                <%# Container.DataItem("reportfrom")%>
            </td>
            <td>
                <%# Container.DataItem("reportto")%>
            </td>
            <td>
                <a class="remove" title="Remove"  href='javascript:removeThreshold(<%# Container.DataItem("eligibilitythreshold_pk") %>)'>X</a>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:100px">
                <a href='eligibilitymanagement.aspx?pk=-1'><h6>New Threshold</h6></a> 
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
                    
<asp:PlaceHolder ID="EligibilityThresholdControl" runat="server" />
<table id="EligibilityThresholdButton" runat="server" style="width:100%;">
    <tr style="height:10px;">
                            
    </tr>
    <tr>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton runat="server" ID="btnSaveEligibilityThreshold" Text="<h6>Save</h6>" />
        </td>
        <td style="width:60%;"></td>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton ID="btnCancelEligibilityThreshold" runat="server" Text="<h4>Cancel</h4>" />
        </td>
    </tr>
</table>
</asp:Content>

