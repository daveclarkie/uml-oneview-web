<%@ Control Language="VB" AutoEventWireup="false" CodeFile="notify.ascx.vb" Inherits="mainctls_notify" %>
<div class="inner">
    <div class="content">
        <h3><asp:Label ID="lblTitle" runat="server" text="No Active Notices" />&nbsp;</h3>
            <asp:Label ID="lblContent" runat="server" cssclass="frame" />
        <asp:LinkButton ID="btnConfirm" runat="server" Text="Mark as read" />
    </div>
    <div class="navigation">
    
        <h3>New Notices</h3>

        <ul class="navigationmenu">
            <asp:Repeater ID="rptlist" runat="server">
                <ItemTemplate>
                    <li>
                        <a href='notify.aspx?notice=<%# Container.DataItem("notificationtarget_pk") %>'><span><%# Container.DataItem("title") %></span></a><br />Published: <%#CDate(Container.DataItem("created")).ToString("dd MMM yyyy")%><br />
                            <span class="item"><%#Container.DataItem("segment")%>...</span>
                    </li>
                </ItemTemplate>
            </asp:Repeater>    
        </ul>

        <h3>Unread Notices</h3>

        <ul class="navigationmenu">
            <asp:Repeater ID="rptlistunread" runat="server">
                <ItemTemplate>
                    <li>
                        <a href='notify.aspx?notice=<%# Container.DataItem("notificationtarget_pk") %>'><span><%# Container.DataItem("title") %></span></a><br />Published: <%#CDate(Container.DataItem("created")).ToString("dd MMM yyyy")%><br />
                            <span class="item"><%#Container.DataItem("segment")%>...</span>
                    </li>
                </ItemTemplate>
            </asp:Repeater>    
        </ul>

        <h3>Archive</h3>

        <ul class="navigationmenu">
            <asp:Repeater ID="rptlistarchive" runat="server">
                <ItemTemplate>
                    <li>
                        <a href='notify.aspx?notice=<%# Container.DataItem("notificationtarget_pk") %>'><span><%# Container.DataItem("title") %></span></a><br />Published: <%#CDate(Container.DataItem("created")).ToString("dd MMM yyyy")%><br />
                            <span class="item"><%#Container.DataItem("segment")%>...</span>
                    </li>
                </ItemTemplate>
            </asp:Repeater>    
        </ul>
    </div>
</div>