<%@ Control Language="VB" AutoEventWireup="false" CodeFile="messages_ctl.ascx.vb" Inherits="messages_ctl" %>
<h5>messages</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmessage">
<span class='label'>message</span>
<asp:TextBox EnableViewState="false" ID="message" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blktargetuser_fk">
<span class='label'>targetuser_fk</span>
<asp:DropDownList ID="targetuser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmessagetype_fk">
<span class='label'>messagetype_fk</span>
<asp:DropDownList ID="messagetype_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

