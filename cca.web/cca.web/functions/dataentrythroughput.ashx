<%@ WebHandler Language="VB" Class="function_dataentrythroughput" %>
Public Class function_dataentrythroughput
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        
        Dim Msg As String = ""
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        
        Dim month As Integer = -1
        Dim year As Integer = -1
        Dim throughput As Integer = -1
        Dim total As Decimal = 0
        Dim eligible As Decimal = 0
        
        Try
            month = RF("b").ToString.Trim()
        Catch ex As Exception
            month = -1
        End Try
        Try
            year = RF("c").ToString.Trim()
        Catch ex As Exception
            year = -1
        End Try
        Try
            total = RF("d").ToString.Trim()
        Catch ex As Exception
            total = -1
        End Try
        Try
            eligible = RF("e").ToString.Trim()
        Catch ex As Exception
            eligible = -1
        End Try
        
        Dim atp As New agreementthroughputs(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)
        Try
            throughput = atp.throughput_fk
        Catch ex As Exception
            throughput = -1
        End Try
        
        
        Dim outputvalue As Integer = 0
        Dim CO As New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_datathroughputs_duplicatecheck"
        CO.Parameters.AddWithValue("@throughput", throughput)
        CO.Parameters.AddWithValue("@month", month)
        CO.Parameters.AddWithValue("@year", year)
        CDC.ReadScalarValue(outputvalue, CO)
        
        If outputvalue = 0 Then
            Dim det As New datathroughputs(SM.currentuser, SM.actualuser, CDC)
            det.throughput_fk = throughput
            det.month_fk = month
            det.year_fk = year
            det.totalthroughput = total
            det.eligiblethroughput = eligible
            det.dataimportmethod_fk = 1
            If det.Save() Then : Msg = "" : End If
        Else
            Dim det As New datathroughputs(SM.currentuser, SM.actualuser, outputvalue, CDC)
            det.month_fk = month
            det.year_fk = year
            det.totalthroughput = total
            det.eligiblethroughput = eligible
            det.dataimportmethod_fk = 1
            If det.Save() Then : Msg = "Updated" : End If
        End If
               
        Try
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class