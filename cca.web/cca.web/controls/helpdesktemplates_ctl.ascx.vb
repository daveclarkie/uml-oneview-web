Partial Class helpdesktemplates_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ohelpdesktemplates As Objects.helpdesktemplates
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_helpdesktemplategroups_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.helpdesktemplategroup_fk.Items.Count=0 Then
        Me.helpdesktemplategroup_fk.DataSource=CDC.ReadDataTable(CO)
        Me.helpdesktemplategroup_fk.DataTextField = "value"
        Me.helpdesktemplategroup_fk.DataValueField = "pk"
        Try
            Me.helpdesktemplategroup_fk.DataBind
        Catch Ex as Exception
            Me.helpdesktemplategroup_fk.SelectedValue=-1
            Me.helpdesktemplategroup_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ohelpdesktemplates is Nothing then
ohelpdesktemplates = new Objects.helpdesktemplates(currentuser,actualuser,pk,CDC)
End If
Me.templatename.Text=ohelpdesktemplates.templatename
Me.helpdesktemplategroup_fk.SelectedValue=ohelpdesktemplates.helpdesktemplategroup_fk
Me.templatedisplayname.Text=ohelpdesktemplates.templatedisplayname
_CurrentPk=ohelpdesktemplates.helpdesktemplate_pk
Status=ohelpdesktemplates.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ohelpdesktemplates is Nothing then
ohelpdesktemplates = new Objects.helpdesktemplates(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ohelpdesktemplates.templatename=Me.templatename.Text
ohelpdesktemplates.helpdesktemplategroup_fk=Me.helpdesktemplategroup_fk.SelectedValue
ohelpdesktemplates.templatedisplayname=Me.templatedisplayname.Text
result=ohelpdesktemplates.Save()
if not result then throw ohelpdesktemplates.LastError
_CurrentPk=ohelpdesktemplates.helpdesktemplate_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ohelpdesktemplates.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesktemplates is Nothing then
   ohelpdesktemplates = new Objects.helpdesktemplates(currentuser,actualuser,pk,CDC)
  End If
  ohelpdesktemplates.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesktemplates is Nothing then
   ohelpdesktemplates = new Objects.helpdesktemplates(currentuser,actualuser,pk,CDC)
  End If
  ohelpdesktemplates.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesktemplates is Nothing then
   ohelpdesktemplates = new Objects.helpdesktemplates(currentuser,pk,CDC)
  End If
  ohelpdesktemplates.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.templatename.Enabled=IIF(WL>=1,True,False)
  Me.blktemplatename.Visible=IIF(RL>=1,True,False)
  Me.helpdesktemplategroup_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdesktemplategroup_fk.Visible=IIF(RL>=0,True,False)
  Me.templatedisplayname.Enabled=IIF(WL>=1,True,False)
  Me.blktemplatedisplayname.Visible=IIF(RL>=1,True,False)
  End Sub
End Class

