<%@ Control Language="VB" AutoEventWireup="false" CodeFile="users_ctl.ascx.vb" Inherits="users_ctl" %>
<h5>users</h5>
<ul class='formcontrol'>
<li runat="server" id="blkusername">
<span class='label'>username</span>
<asp:TextBox EnableViewState="false" ID="username" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkencpassword">
<span class='label'>encpassword</span>
<asp:TextBox EnableViewState="false" ID="encpassword" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkreadlevel">
<span class='label'>readlevel</span>
<asp:TextBox EnableViewState="false" ID="readlevel" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkwritelevel">
<span class='label'>writelevel</span>
<asp:TextBox EnableViewState="false" ID="writelevel" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkdisconnect">
<span class='label'>disconnect</span>
<asp:CheckBox ID="disconnect" runat="server" cssClass="input_chk" />
</li>
</ul>

