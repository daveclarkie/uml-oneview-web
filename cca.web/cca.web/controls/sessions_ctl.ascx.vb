Partial Class sessions_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osessions As Objects.sessions
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.currentuser.Text="0"
            Me.actualuser.Text="0"
            Me.targetuser.Text="0"
            Me.mainid.Text="0"
            Me.subid.Text="0"
            Me.linkid.Text="0"
            Me.PersonPageMode.Text="0"
            Me.AgreementPageMode.Text="0"
            Me.redirected.Text="0"
            Me.noticecount.Text="0"
            Me.targetAgreement.Text="0"
            Me.targetAgreementSecondary.Text="0"
            Me.targetDataEntry.Text="0"
            Me.DataEntryPageMode.Text="0"
            Me.matrix.Text="0"
            Me.matrixprice.Text="0"
            Me.matrixdetail.Text="0"
            Me.MatrixPageMode.Text="0"
            Me.targetBubble.Text="0"
            Me.targetBubbleSecondary.Text="0"
            Me.BubblePageMode.Text="0"
            Me.CustomerPageMode.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osessions is Nothing then
osessions = new Objects.sessions(currentuser,actualuser,pk,CDC)
End If
Me.sessionid.Text=osessions.sessionid
Me.ip.Text=osessions.ip
Me.currentuser.Text=osessions.currentuser
Me.currentuser.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.currentuser.Attributes.Add("onBlur", "clearNotice()")
Me.actualuser.Text=osessions.actualuser
Me.actualuser.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.actualuser.Attributes.Add("onBlur", "clearNotice()")
Me.targetuser.Text=osessions.targetuser
Me.targetuser.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetuser.Attributes.Add("onBlur", "clearNotice()")
Me.mainid.Text=osessions.mainid
Me.mainid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.mainid.Attributes.Add("onBlur", "clearNotice()")
Me.subid.Text=osessions.subid
Me.subid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.subid.Attributes.Add("onBlur", "clearNotice()")
Me.linkid.Text=osessions.linkid
Me.linkid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.linkid.Attributes.Add("onBlur", "clearNotice()")
Me.PersonPageMode.Text=osessions.PersonPageMode
Me.PersonPageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.PersonPageMode.Attributes.Add("onBlur", "clearNotice()")
Me.AgreementPageMode.Text=osessions.AgreementPageMode
Me.AgreementPageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.AgreementPageMode.Attributes.Add("onBlur", "clearNotice()")
Me.redirected.Text=osessions.redirected
Me.redirected.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.redirected.Attributes.Add("onBlur", "clearNotice()")
Me.noticecount.Text=osessions.noticecount
Me.noticecount.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.noticecount.Attributes.Add("onBlur", "clearNotice()")
Me.targetAgreement.Text=osessions.targetAgreement
Me.targetAgreement.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetAgreement.Attributes.Add("onBlur", "clearNotice()")
Me.targetAgreementSecondary.Text=osessions.targetAgreementSecondary
Me.targetAgreementSecondary.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetAgreementSecondary.Attributes.Add("onBlur", "clearNotice()")
Me.targetDataEntry.Text=osessions.targetDataEntry
Me.targetDataEntry.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetDataEntry.Attributes.Add("onBlur", "clearNotice()")
Me.DataEntryPageMode.Text=osessions.DataEntryPageMode
Me.DataEntryPageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.DataEntryPageMode.Attributes.Add("onBlur", "clearNotice()")
Me.matrix.Text=osessions.matrix
Me.matrix.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.matrix.Attributes.Add("onBlur", "clearNotice()")
Me.matrixprice.Text=osessions.matrixprice
Me.matrixprice.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.matrixprice.Attributes.Add("onBlur", "clearNotice()")
Me.matrixdetail.Text=osessions.matrixdetail
Me.matrixdetail.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.matrixdetail.Attributes.Add("onBlur", "clearNotice()")
Me.MatrixPageMode.Text=osessions.MatrixPageMode
Me.MatrixPageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.MatrixPageMode.Attributes.Add("onBlur", "clearNotice()")
Me.targetBubble.Text=osessions.targetBubble
Me.targetBubble.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetBubble.Attributes.Add("onBlur", "clearNotice()")
Me.targetBubbleSecondary.Text=osessions.targetBubbleSecondary
Me.targetBubbleSecondary.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.targetBubbleSecondary.Attributes.Add("onBlur", "clearNotice()")
Me.BubblePageMode.Text=osessions.BubblePageMode
Me.BubblePageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.BubblePageMode.Attributes.Add("onBlur", "clearNotice()")
Me.CustomerPageMode.Text=osessions.CustomerPageMode
Me.CustomerPageMode.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.CustomerPageMode.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=osessions.session_pk
Status=osessions.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osessions is Nothing then
osessions = new Objects.sessions(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osessions.sessionid=Me.sessionid.Text
osessions.ip=Me.ip.Text
osessions.currentuser=Me.currentuser.Text
osessions.actualuser=Me.actualuser.Text
osessions.targetuser=Me.targetuser.Text
osessions.mainid=Me.mainid.Text
osessions.subid=Me.subid.Text
osessions.linkid=Me.linkid.Text
osessions.PersonPageMode=Me.PersonPageMode.Text
osessions.AgreementPageMode=Me.AgreementPageMode.Text
osessions.redirected=Me.redirected.Text
osessions.noticecount=Me.noticecount.Text
osessions.targetAgreement=Me.targetAgreement.Text
osessions.targetAgreementSecondary=Me.targetAgreementSecondary.Text
osessions.targetDataEntry=Me.targetDataEntry.Text
osessions.DataEntryPageMode=Me.DataEntryPageMode.Text
osessions.matrix=Me.matrix.Text
osessions.matrixprice=Me.matrixprice.Text
osessions.matrixdetail=Me.matrixdetail.Text
osessions.MatrixPageMode=Me.MatrixPageMode.Text
osessions.targetBubble=Me.targetBubble.Text
osessions.targetBubbleSecondary=Me.targetBubbleSecondary.Text
osessions.BubblePageMode=Me.BubblePageMode.Text
osessions.CustomerPageMode=Me.CustomerPageMode.Text
result=osessions.Save()
if not result then throw osessions.LastError
_CurrentPk=osessions.session_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osessions.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osessions is Nothing then
   osessions = new Objects.sessions(currentuser,actualuser,pk,CDC)
  End If
  osessions.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osessions is Nothing then
   osessions = new Objects.sessions(currentuser,actualuser,pk,CDC)
  End If
  osessions.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osessions is Nothing then
   osessions = new Objects.sessions(currentuser,pk,CDC)
  End If
  osessions.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.sessionid.Enabled=IIF(WL>=0,True,False)
  Me.blksessionid.Visible=IIF(RL>=0,True,False)
  Me.ip.Enabled=IIF(WL>=0,True,False)
  Me.blkip.Visible=IIF(RL>=0,True,False)
  Me.currentuser.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrentuser.Visible=IIF(RL>=0,True,False)
  Me.actualuser.Enabled=IIF(WL>=0,True,False)
  Me.blkactualuser.Visible=IIF(RL>=0,True,False)
  Me.targetuser.Enabled=IIF(WL>=0,True,False)
  Me.blktargetuser.Visible=IIF(RL>=0,True,False)
  Me.mainid.Enabled=IIF(WL>=0,True,False)
  Me.blkmainid.Visible=IIF(RL>=0,True,False)
  Me.subid.Enabled=IIF(WL>=0,True,False)
  Me.blksubid.Visible=IIF(RL>=0,True,False)
  Me.linkid.Enabled=IIF(WL>=0,True,False)
  Me.blklinkid.Visible=IIF(RL>=0,True,False)
  Me.PersonPageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkPersonPageMode.Visible=IIF(RL>=0,True,False)
  Me.AgreementPageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkAgreementPageMode.Visible=IIF(RL>=0,True,False)
  Me.redirected.Enabled=IIF(WL>=0,True,False)
  Me.blkredirected.Visible=IIF(RL>=0,True,False)
  Me.noticecount.Enabled=IIF(WL>=0,True,False)
  Me.blknoticecount.Visible=IIF(RL>=0,True,False)
  Me.targetAgreement.Enabled=IIF(WL>=0,True,False)
  Me.blktargetAgreement.Visible=IIF(RL>=0,True,False)
  Me.targetAgreementSecondary.Enabled=IIF(WL>=0,True,False)
  Me.blktargetAgreementSecondary.Visible=IIF(RL>=0,True,False)
  Me.targetDataEntry.Enabled=IIF(WL>=0,True,False)
  Me.blktargetDataEntry.Visible=IIF(RL>=0,True,False)
  Me.DataEntryPageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkDataEntryPageMode.Visible=IIF(RL>=0,True,False)
  Me.matrix.Enabled=IIF(WL>=0,True,False)
  Me.blkmatrix.Visible=IIF(RL>=0,True,False)
  Me.matrixprice.Enabled=IIF(WL>=0,True,False)
  Me.blkmatrixprice.Visible=IIF(RL>=0,True,False)
  Me.matrixdetail.Enabled=IIF(WL>=0,True,False)
  Me.blkmatrixdetail.Visible=IIF(RL>=0,True,False)
  Me.MatrixPageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkMatrixPageMode.Visible=IIF(RL>=0,True,False)
  Me.targetBubble.Enabled=IIF(WL>=0,True,False)
  Me.blktargetBubble.Visible=IIF(RL>=0,True,False)
  Me.targetBubbleSecondary.Enabled=IIF(WL>=0,True,False)
  Me.blktargetBubbleSecondary.Visible=IIF(RL>=0,True,False)
  Me.BubblePageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkBubblePageMode.Visible=IIF(RL>=0,True,False)
  Me.CustomerPageMode.Enabled=IIF(WL>=0,True,False)
  Me.blkCustomerPageMode.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

