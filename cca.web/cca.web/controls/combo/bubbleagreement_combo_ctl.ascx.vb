Partial Class bubbleagreement_combo_ctl
   Inherits DaveControl
   Public PSM As MySM

   Public WithEvents obubbleagreement As Objects.bubbleagreements
   Public WithEvents oagreement As Objects.agreements
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=-1
   Dim _Status as Integer=0
   Dim _STCurrentPk as Integer=-1
   Dim _STStatus as Integer=0

   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       ControlInit
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub

   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property

   Public ReadOnly Property STCurrentPk() as Integer
        Get
            Return _STCurrentPk
        End Get
   End Property

   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property

  Public Sub ControlInit()
CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
Me.organisation_fk.DataTextField = "value"
Me.organisation_fk.DataValueField = "pk"
Try
Me.organisation_fk.DataBind
Catch Ex as Exception
Me.organisation_fk.SelectedValue=-1
Me.organisation_fk.DataBind
End Try
CO=new SqlClient.SqlCommand("rkg_federations_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.federation_fk.DataSource=CDC.ReadDataTable(CO)
Me.federation_fk.DataTextField = "value"
Me.federation_fk.DataValueField = "pk"
Try
Me.federation_fk.DataBind
Catch Ex as Exception
Me.federation_fk.SelectedValue=-1
Me.federation_fk.DataBind
End Try
CO=new SqlClient.SqlCommand("rkg_agreementtypes_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.agreementtype_fk.DataSource=CDC.ReadDataTable(CO)
Me.agreementtype_fk.DataTextField = "value"
Me.agreementtype_fk.DataValueField = "pk"
Try
Me.agreementtype_fk.DataBind
Catch Ex as Exception
Me.agreementtype_fk.SelectedValue=-1
Me.agreementtype_fk.DataBind
End Try
CO=new SqlClient.SqlCommand("rkg_agreementmeasures_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.agreementmeasure_fk.DataSource=CDC.ReadDataTable(CO)
Me.agreementmeasure_fk.DataTextField = "value"
Me.agreementmeasure_fk.DataValueField = "pk"
Try
Me.agreementmeasure_fk.DataBind
Catch Ex as Exception
Me.agreementmeasure_fk.SelectedValue=-1
Me.agreementmeasure_fk.DataBind
End Try
  End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
End Sub
Public Sub SetObjects(ByVal CurrentUser as Integer,ByVal ActualUser as Integer,Optional ByVal pk as Integer=-1)
    If obubbleagreement is Nothing then
        obubbleagreement = new Objects.bubbleagreements(currentuser,actualuser,pk,CDC)
        If obubbleagreement.agreement_fk>0 then
            oagreement = new Objects.agreements(currentuser,actualuser,obubbleagreement.agreement_fk,CDC)
        Else
            oagreement = new Objects.agreements(currentuser,actualuser,-1,CDC)
        End If
    End If
    If oagreement is Nothing then
        If obubbleagreement.agreement_fk>0 then
            oagreement = new Objects.agreements(currentuser,actualuser,obubbleagreement.agreement_fk,CDC)
        Else
            oagreement = new Objects.agreements(currentuser,actualuser,-1,CDC)
        End If
    End If
End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1)
    SetObjects(currentuser,actualuser,pk)
Try
Me.organisation_fk.SelectedValue=oagreement.organisation_fk
Catch exorganisation_fk as Exception
Me.organisation_fk.SelectedValue=-1
End Try
Me.agreementname.Text=oagreement.agreementname
Try
Me.federation_fk.SelectedValue=oagreement.federation_fk
Catch exfederation_fk as Exception
Me.federation_fk.SelectedValue=-1
End Try
Me.agreementnumber.Text=oagreement.agreementnumber
Me.facilitynumber.Text=oagreement.facilitynumber
Me.targetunitidentifier.Text=oagreement.targetunitidentifier
Me.baseyearstart.Text=oagreement.baseyearstart.ToString("dd MMM yyyy")
If Me.baseyearstart.Text="01 Jan 0001" Then Me.baseyearstart.Text=""
Me.baseyearstart.Attributes.Add("onFocus", "setNotice('Format: D MMM YYYY eg 1 Jan 1980')")
Me.baseyearstart.Attributes.Add("onBlur", "clearNotice()")
Me.baseyearend.Text=oagreement.baseyearend.ToString("dd MMM yyyy")
If Me.baseyearend.Text="01 Jan 0001" Then Me.baseyearend.Text=""
Me.baseyearend.Attributes.Add("onFocus", "setNotice('Format: D MMM YYYY eg 1 Jan 1980')")
Me.baseyearend.Attributes.Add("onBlur", "clearNotice()")
Try
Me.agreementtype_fk.SelectedValue=oagreement.agreementtype_fk
Catch exagreementtype_fk as Exception
Me.agreementtype_fk.SelectedValue=-1
End Try
Try
Me.agreementmeasure_fk.SelectedValue=oagreement.agreementmeasure_fk
Catch exagreementmeasure_fk as Exception
Me.agreementmeasure_fk.SelectedValue=-1
End Try
SM.subId=obubbleagreement.agreement_fk
Me.submeterinstalled.Checked=oagreement.submeterinstalled
SM.subId=obubbleagreement.agreement_fk
_CurrentPk=obubbleagreement.bubbleagreement_pk
_STCurrentPk=oagreement.agreement_pk
Status=obubbleagreement.rowstatus OR oagreement.rowstatus
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1) as boolean
    SetObjects(currentuser,actualuser,pk)
oagreement.organisation_fk=Me.organisation_fk.SelectedValue
oagreement.agreementname=Me.agreementname.Text
oagreement.federation_fk=Me.federation_fk.SelectedValue
oagreement.agreementnumber=Me.agreementnumber.Text
oagreement.facilitynumber=Me.facilitynumber.Text
oagreement.targetunitidentifier=Me.targetunitidentifier.Text
oagreement.baseyearstart=CommonFN.CheckEmptyDate(Me.baseyearstart.Text)
oagreement.baseyearend=CommonFN.CheckEmptyDate(Me.baseyearend.Text)
oagreement.agreementtype_fk=Me.agreementtype_fk.SelectedValue
oagreement.agreementmeasure_fk=Me.agreementmeasure_fk.SelectedValue
oagreement.agreement_fk=SM.subId
oagreement.submeterinstalled=Me.submeterinstalled.Checked
Dim STResult as boolean=False
Try
STResult=oagreement.Save()
if not STResult then throw oagreement.LastError
_STCurrentPk=oagreement.agreement_pk
SM.subId=oagreement.agreement_pk
Catch Ex as Exception
STResult=False
LastError=Ex
SM.subId=-1
End Try
Try
obubbleagreement.bubble_fk=SM.targetuser
obubbleagreement.agreement_fk=SM.subId
Catch MainEx as Exception
LastError=MainEx
End Try
Dim result as boolean=false
Try
Result=obubbleagreement.Save()
if not Result then throw obubbleagreement.LastError
_CurrentPk=obubbleagreement.bubbleagreement_pk
Catch Ex as Exception
Result=False
LastError=Ex
End Try
SM.linkId=_CurrentPk
Status=obubbleagreement.rowstatus OR oagreement.rowstatus
Return result AND STResult
End Function
Public Function Enable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If obubbleagreement is Nothing then
obubbleagreement = new Objects.bubbleagreements(currentuser,actualuser,pk,CDC)
End If
obubbleagreement.rowstatus=0
Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If obubbleagreement is Nothing then
obubbleagreement = new Objects.bubbleagreements(currentuser,actualuser,pk,CDC)
End If
obubbleagreement.rowstatus=1
Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If obubbleagreement is Nothing then
obubbleagreement = new Objects.bubbleagreements(currentuser,actualuser,pk,CDC)
End If
obubbleagreement.rowstatus=2
Return Save(currentuser,actualuser,pk)
End Function
Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
Me.agreementname.Enabled=IIF(WL>=0,True,False)
Me.blkagreementname.Visible=IIF(RL>=0,True,False)
Me.federation_fk.Enabled=IIF(WL>=0,True,False)
Me.blkfederation_fk.Visible=IIF(RL>=0,True,False)
Me.agreementnumber.Enabled=IIF(WL>=0,True,False)
Me.blkagreementnumber.Visible=IIF(RL>=0,True,False)
Me.facilitynumber.Enabled=IIF(WL>=0,True,False)
Me.blkfacilitynumber.Visible=IIF(RL>=0,True,False)
Me.targetunitidentifier.Enabled=IIF(WL>=0,True,False)
Me.blktargetunitidentifier.Visible=IIF(RL>=0,True,False)
Me.baseyearstart.Enabled=IIF(WL>=0,True,False)
Me.blkbaseyearstart.Visible=IIF(RL>=0,True,False)
Me.baseyearend.Enabled=IIF(WL>=0,True,False)
Me.blkbaseyearend.Visible=IIF(RL>=0,True,False)
Me.agreementtype_fk.Enabled=IIF(WL>=0,True,False)
Me.blkagreementtype_fk.Visible=IIF(RL>=0,True,False)
Me.agreementmeasure_fk.Enabled=IIF(WL>=0,True,False)
Me.blkagreementmeasure_fk.Visible=IIF(RL>=0,True,False)
Me.submeterinstalled.Enabled=IIF(WL>=0,True,False)
Me.blksubmeterinstalled.Visible=IIF(RL>=0,True,False)
End Sub
End Class
