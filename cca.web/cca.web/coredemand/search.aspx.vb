Partial Class coredemand_search
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Public Sub Search()
        SearchAgreements()
    End Sub

    Public Sub SearchAgreements()

        Dim sfSearchType As String = LimitSearchTerms(ddlSearchType.SelectedItem.Value)
        Dim sfSearchString As String = LimitSearchTerms(searchstring.Text)

        Dim sfProspects As Integer = 0
        If ddlprospects.SelectedValue = "Yes" Then
            sfProspects = 1
        Else
            sfProspects = 0
        End If

        If sfSearchString.Length > 0 Then
            CO = New SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_contractmanagersearch"
            CDC = New DataCommon
            CO.Parameters.AddWithValue("@searchtype", sfSearchType)
            CO.Parameters.AddWithValue("@searchstring", sfSearchString)
            CO.Parameters.AddWithValue("@includeprospects", sfProspects)
            CO.Parameters.AddWithValue("@w", "%")
            CO.Parameters.AddWithValue("@user", SM.currentuser)

            Dim dt As DataTable = CDC.ReadDataTable(CO)
            
            rptContractManagerResults.DataSource = dt
            rptContractManagerResults.DataBind()

            If dt.Rows.Count = 0 Then AddNotice("setNotice('No " & sfSearchType & "s matching your criteria were found.');")

            CO.Dispose()
            CO = Nothing
        Else
            rptContractManagerResults.DataSource = Nothing
            rptContractManagerResults.DataBind()
            AddNotice("setNotice('No search criteria entered');")
        End If

    End Sub
    
    Private Sub SetSearchRanges()
        If Not Page.IsPostBack Then
            Try
                ddlprospects.Items.Clear()
                ddlprospects.Items.Add("Yes")
                ddlprospects.Items.Add("No")

                ddlSearchType.Items.Clear()
                ddlSearchType.Items.Add("Organisation")
                ddlSearchType.Items.Add("Site")
                ddlSearchType.Items.Add("Meter")


            Catch ex As Exception
            End Try
        End If
    End Sub


    '================================
    '   "Validation" Procs
    '================================

    Private Function LimitSearchTerms(ByVal original As String) As String
        CDC.Sanitize(original)
        Return original
    End Function

    Private Function LimitSearchTermsNumeric(ByVal original As String) As Integer
        CDC.Sanitize(original)
        Dim i As Integer = 0
        If Integer.TryParse(original, i) Then
            Return i
        Else
            Return 0
        End If
    End Function


    '================================
    '   Page Specific Events
    '================================

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Search()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ViewState("Page") = 1
        Search()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetSearchRanges()
    End Sub

    Public Function returnselect(ByVal rowstatus As Integer, ByVal url As String) As String
        Dim _return As String = ""

        If rowstatus = 1 Then
            _return = "<a href='" & url & Local.noCache & "'><h6>customer</h6></a>"
        Else
            _return = "<a href='" & url & Local.noCache & "'><h6 style='background-color:red;'>prospect</h6></a>"
        End If
        Return _return
    End Function


End Class
