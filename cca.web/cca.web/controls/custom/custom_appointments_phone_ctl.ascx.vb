Partial Class custom_appointments_phone_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        ctlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oappointments As objects.appointments
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
   
    Public Sub ctlInit()
        If _read Then Exit Sub
        If SM.mainid > -1 Then
            CO = New SqlClient.SqlCommand("rkg_organisations_read")
            CO.Parameters.AddWithValue("@organisation_pk", SM.mainid)
            CO.CommandType = CommandType.StoredProcedure
            If Me.organisation_fk.Items.Count = 0 Then
                Me.organisation_fk.DataSource = CDC.ReadDataTable(CO)
                Me.organisation_fk.DataTextField = "customername"
                Me.organisation_fk.DataValueField = "organisation_pk"
                Try
                    Me.organisation_fk.DataBind()
                Catch Ex As Exception
                    Me.organisation_fk.SelectedValue = -1
                    Me.organisation_fk.DataBind()
                End Try
            End If
        Else
            CO = New SqlClient.SqlCommand("rkg_organisations_lookup")
            CO.CommandType = CommandType.StoredProcedure
            If Me.organisation_fk.Items.Count = 0 Then
                Me.organisation_fk.DataSource = CDC.ReadDataTable(CO)
                Me.organisation_fk.DataTextField = "value"
                Me.organisation_fk.DataValueField = "pk"
                Try
                    Me.organisation_fk.DataBind()
                Catch Ex As Exception
                    Me.organisation_fk.SelectedValue = -1
                    Me.organisation_fk.DataBind()
                End Try
            End If
        End If

        CO = New SqlClient.SqlCommand("rsp_sites_lookup_appointments")
        CO.Parameters.AddWithValue("@organisation_fk", Me.organisation_fk.SelectedItem.Value)
        CO.CommandType = CommandType.StoredProcedure
        If Me.site_fk.Items.Count = 0 Then
            Me.site_fk.DataSource = CDC.ReadDataTable(CO)
            Me.site_fk.DataTextField = "value"
            Me.site_fk.DataValueField = "pk"
            Try
                Me.site_fk.DataBind()
            Catch Ex As Exception
                Me.site_fk.SelectedValue = -1
                Me.site_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_appointmenttypes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.appointmenttype_fk.Items.Count = 0 Then
            Me.appointmenttype_fk.DataSource = CDC.ReadDataTable(CO)
            Me.appointmenttype_fk.DataTextField = "value"
            Me.appointmenttype_fk.DataValueField = "pk"
            Try
                Me.appointmenttype_fk.DataBind()
            Catch Ex As Exception
                Me.appointmenttype_fk.SelectedValue = -1
                Me.appointmenttype_fk.DataBind()
            End Try
        End If






        CO = New SqlClient.SqlCommand("rkg_appointmentstatuses_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.appointmentstatus_fk.Items.Count = 0 Then
            Me.appointmentstatus_fk.DataSource = CDC.ReadDataTable(CO)
            Me.appointmentstatus_fk.DataTextField = "value"
            Me.appointmentstatus_fk.DataValueField = "pk"
            Try
                Me.appointmentstatus_fk.DataBind()
            Catch Ex As Exception
                Me.appointmentstatus_fk.SelectedValue = -1
                Me.appointmentstatus_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_appointmentpurposes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.appointmentpurpose_fk.Items.Count = 0 Then
            Me.appointmentpurpose_fk.DataSource = CDC.ReadDataTable(CO)
            Me.appointmentpurpose_fk.DataTextField = "value"
            Me.appointmentpurpose_fk.DataValueField = "pk"
            Try
                Me.appointmentpurpose_fk.DataBind()
            Catch Ex As Exception
                Me.appointmentpurpose_fk.SelectedValue = -1
                Me.appointmentpurpose_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oappointments Is Nothing Then
            oappointments = New objects.appointments(currentuser, actualuser, pk, CDC)
        End If
        Me.organisation_fk.SelectedValue = oappointments.organisation_fk
        Me.site_fk.SelectedValue = oappointments.site_fk
        Me.appointmenttype_fk.SelectedValue = oappointments.appointmenttype_fk
        Me.appointment.Text = oappointments.appointment.ToString("dd MMM yyyy")
        If Me.appointment.Text = "01 Jan 0001" Then Me.appointment.Text = ""
        Me.appointment.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.appointment.Attributes.Add("onBlur", "clearNotice()")
        Me.phone_fk.SelectedValue = oappointments.phone_fk
        Me.appointmentstatus_fk.SelectedValue = oappointments.appointmentstatus_fk
        Me.appointmentpurpose_fk.SelectedValue = oappointments.appointmentpurpose_fk
        Me.appointmentpurposeother.Text = oappointments.appointmentpurposeother
        _CurrentPk = oappointments.appointment_pk
        Status = oappointments.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oappointments Is Nothing Then
            oappointments = New objects.appointments(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oappointments.organisation_fk = Me.organisation_fk.SelectedValue
            oappointments.site_fk = Me.site_fk.SelectedValue
            oappointments.appointmenttype_fk = Me.appointmenttype_fk.SelectedValue
            oappointments.appointment = CommonFN.CheckEmptyDate(Me.appointment.Text)
            oappointments.phone_fk = Me.phone_fk.SelectedValue
            oappointments.appointmentstatus_fk = Me.appointmentstatus_fk.SelectedValue
            oappointments.appointmentpurpose_fk = Me.appointmentpurpose_fk.SelectedValue
            oappointments.appointmentpurposeother = Me.appointmentpurposeother.Text
            result = oappointments.Save()
            If Not result Then Throw oappointments.LastError
            _CurrentPk = oappointments.appointment_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oappointments.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oappointments Is Nothing Then
            oappointments = New objects.appointments(currentuser, actualuser, pk, CDC)
        End If
        oappointments.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oappointments Is Nothing Then
            oappointments = New objects.appointments(currentuser, actualuser, pk, CDC)
        End If
        oappointments.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oappointments Is Nothing Then
            oappointments = New objects.appointments(currentuser, pk, CDC)
        End If
        oappointments.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Me.organisation_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkorganisation_fk.Visible = IIf(RL >= 0, True, False)
        Me.site_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blksite_fk.Visible = IIf(RL >= 0, True, False)
        Me.appointmenttype_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkappointmenttype_fk.Visible = IIf(RL >= 0, True, False)
        Me.appointment.Enabled = IIf(WL >= 0, True, False)
        Me.blkappointment.Visible = IIf(RL >= 0, True, False)
        Me.phone_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkphone_fk.Visible = IIf(RL >= 0, True, False)
        Me.appointmentstatus_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkappointmentstatus_fk.Visible = IIf(RL >= 0, True, False)
        Me.appointmentpurpose_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkappointmentpurpose_fk.Visible = IIf(RL >= 0, True, False)
        Me.appointmentpurposeother.Enabled = IIf(WL >= 0, True, False)
        Me.blkappointmentpurposeother.Visible = IIf(RL >= 0, True, False)
    End Sub

End Class

