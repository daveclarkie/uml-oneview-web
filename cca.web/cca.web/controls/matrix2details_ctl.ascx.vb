Partial Class matrix2details_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omatrix2details As Objects.matrix2details
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.consumption.Text="0"
            Me.current_standingcharge.Text="0.00"
            Me.current_rate.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omatrix2details is Nothing then
omatrix2details = new Objects.matrix2details(currentuser,actualuser,pk,CDC)
End If
Me.mprn.Text=omatrix2details.mprn
Me.postcode.Text=omatrix2details.postcode
Me.consumption.Text=omatrix2details.consumption
Me.consumption.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.consumption.Attributes.Add("onBlur", "clearNotice()")
Me.current_standingcharge.Text=omatrix2details.current_standingcharge
Me.current_standingcharge.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_standingcharge.Attributes.Add("onBlur", "clearNotice()")
Me.current_rate.Text=omatrix2details.current_rate
Me.current_rate.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_rate.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=omatrix2details.matrixdetail_pk
Status=omatrix2details.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omatrix2details is Nothing then
omatrix2details = new Objects.matrix2details(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omatrix2details.mprn=Me.mprn.Text
omatrix2details.postcode=Me.postcode.Text
omatrix2details.consumption=Me.consumption.Text
omatrix2details.current_standingcharge=Me.current_standingcharge.Text
omatrix2details.current_rate=Me.current_rate.Text
result=omatrix2details.Save()
if not result then throw omatrix2details.LastError
_CurrentPk=omatrix2details.matrixdetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omatrix2details.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2details is Nothing then
   omatrix2details = new Objects.matrix2details(currentuser,actualuser,pk,CDC)
  End If
  omatrix2details.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2details is Nothing then
   omatrix2details = new Objects.matrix2details(currentuser,actualuser,pk,CDC)
  End If
  omatrix2details.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2details is Nothing then
   omatrix2details = new Objects.matrix2details(currentuser,pk,CDC)
  End If
  omatrix2details.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.mprn.Enabled=IIF(WL>=0,True,False)
  Me.blkmprn.Visible=IIF(RL>=0,True,False)
  Me.postcode.Enabled=IIF(WL>=0,True,False)
  Me.blkpostcode.Visible=IIF(RL>=0,True,False)
  Me.consumption.Enabled=IIF(WL>=0,True,False)
  Me.blkconsumption.Visible=IIF(RL>=0,True,False)
  Me.current_standingcharge.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_standingcharge.Visible=IIF(RL>=0,True,False)
  Me.current_rate.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_rate.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

