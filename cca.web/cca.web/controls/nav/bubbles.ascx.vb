﻿
Imports Microsoft.Reporting.WebForms

Partial Class controls_nav_bubbles
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim CO As SqlClient.SqlCommand
    Dim dt As DataTable

    Dim iAgreement As Integer = 0

    Public Function LoadList() As Boolean
        If isViable() Then

            Dim a As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
            Dim f As New federations(SM.currentuser, SM.actualuser, a.federation_fk, CDC)
            Dim at As New agreementtypes(SM.currentuser, SM.actualuser, a.agreementtype_fk, CDC)
            Dim am As New agreementmeasures(SM.currentuser, SM.actualuser, a.agreementmeasure_fk, CDC)


            lblAgreementName.Text = a.bubblename

            

            DeploymentLocation.NavigateUrl = "file://" & FileLocation




            Return True
        Else
            Return False
        End If
    End Function


    Private Function isViable() As Boolean
        Try

            If Request.RawUrl.ToLower.Contains("/bubbles.aspx") Then
                iAgreement = Integer.Parse(Request.QueryString("pk"))
                If SM.targetBubble <> iAgreement Then
                    SM.targetBubble = iAgreement
                    SM.Save()
                End If
            End If
            If Request.RawUrl.ToLower.Contains("/bubbles.aspx") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function



    Public ReadOnly Property FileLocation() As String
        Get

            Dim a As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
            Dim o As New organisations(SM.currentuser, SM.actualuser, a.organisation_fk, CDC)

            CO = New SqlCommand("rsp_systemparameters_latest")
            CO.CommandType = CommandType.StoredProcedure
            Dim systemparameter_pk As Integer = -1
            CDC.ReadScalarValue(systemparameter_pk, CO)
            Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)
            Dim retval As String = sys.customerlocation & o.customername & "\Energy Management\"
            Return retval
        End Get
    End Property

    Protected Sub btnExportAgreementData_Click(sender As Object, e As System.EventArgs) Handles btnExportAgreementData.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


        Dim ag As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        ' Load Agreement Data Export Report
        rp.Read(17)

        If rp.rowstatus = 0 Then

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport


            'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)
            'Set the report server URL and report path
            'serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
            'serverReport.ReportPath = "/Customer Reports/Energy Management/Phase 2 - Absolute CCLA MTS"
            serverReport.ReportServerUrl = New Uri(rp.reportserverurl)
            serverReport.ReportPath = rp.reportpath


            Dim Agreement As New Microsoft.Reporting.WebForms.ReportParameter()
            Agreement.Name = "bubble_pk"
            Agreement.Values.Add(SM.targetBubble)


            Dim filePath As String = sys.datalocation & "Reports\17.One View Data Export - Bubble\" & SM.targetBubble & "\"       'HttpContext.Current.Server.MapPath("~/App_Data/DS/")
            If (Not System.IO.Directory.Exists(filePath)) Then
                System.IO.Directory.CreateDirectory(filePath)
            End If

            Dim datestring As String = Now.ToString("yyyyMMdd_HHmmss_")

            Dim rt As New reporttypes(SM.currentuser, SM.actualuser, rp.reporttype_fk, CDC)

            Dim rl As New reportlogs(SM.currentuser, SM.actualuser, CDC)


            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Agreement}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("Excel", Nothing, mimeType, encoding, extension, streamids, warnings)

            rl.report_fk = rp.report_pk
            rl.runfromurl = Request.Url.AbsoluteUri
            rl.param1name = "bubble_pk"
            rl.param1value = SM.targetBubble
            rl.param2name = ""
            rl.param2value = ""
            rl.param3name = ""
            rl.param3value = ""
            rl.param4name = ""
            rl.param4value = ""
            rl.savedpath = filePath & datestring & rp.reportname + "." + extension
            rl.Save()

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' \\uk-ol1-file-01\Group Shares\Data\Reports\00.CCA MTS Reporting\
            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + datestring & "One_View_Data_Export" + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)

            IO.File.WriteAllBytes(filePath & datestring & rp.reportname + "." + extension, bytes)

            Response.Flush()
            Response.[End]()


        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If
    End Sub
End Class
