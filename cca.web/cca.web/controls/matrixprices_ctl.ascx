<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrixprices_ctl.ascx.vb" Inherits="matrixprices_ctl" %>
<ul class='formcontrol'>
<li runat="server" id="blkmatrixtype_fk">
<span class='label'>Matrix Type<span class='pop'>This identifies which matrix type the price is for</span></span>
<asp:DropDownList ID="matrixtype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmatrixdetail_fk">
<span class='label'>Detail<span class='pop'>This identifies the particular row that will be used in the detail table</span></span>
<asp:TextBox ID="matrixdetail_fk" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmatrix_fk">
<span class='label'>Matrix</span>
<asp:TextBox ID="matrix_fk" runat="server" cssClass="input_str" />
</li>
</ul>

