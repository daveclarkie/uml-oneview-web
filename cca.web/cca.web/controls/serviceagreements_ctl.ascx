<%@ Control Language="VB" AutoEventWireup="false" CodeFile="serviceagreements_ctl.ascx.vb" Inherits="serviceagreements_ctl" %>
<h5>serviceagreements</h5>
<ul class='formcontrol'>
<li runat="server" id="blkserviceproduct_fk">
<span class='label'>Product<span class='pop'>Which product is the Service Agreement for</span></span>
<asp:DropDownList ID="serviceproduct_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkdate_from">
<span class='label'>Valid From</span>
<asp:TextBox EnableViewState="false" ID="date_from" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkdate_to">
<span class='label'>Valid To</span>
<asp:TextBox EnableViewState="false" ID="date_to" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkotherinformation">
<span class='label'>Other Information<span class='pop'>Any other relevant information about this Service Agreement</span></span>
<asp:TextBox EnableViewState="false" ID="otherinformation" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkrollingcontractclause">
<span class='label'>Rolling?<span class='pop'>Does the agreement include the Rolling Contract clause?</span></span>
<asp:CheckBox ID="rollingcontractclause" runat="server" cssClass="input_chk" />
</li>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Customer Name</span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkserviceagreementnumber">
<span class='label'>Agreement Number<span class='pop'>This is the internal identifier from Orderbook / bFO</span></span>
<asp:TextBox EnableViewState="false" ID="serviceagreementnumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

