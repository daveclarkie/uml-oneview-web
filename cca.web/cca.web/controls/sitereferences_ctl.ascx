<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sitereferences_ctl.ascx.vb" Inherits="sitereferences_ctl" %>
<br />
<ul class='formcontrol'>
<li runat="server" id="blksite_fk">
<span class='label'>site_fk</span>
<asp:TextBox ID="site_fk" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkreference">
<span class='label'>Reference</span>
<asp:TextBox EnableViewState="false" ID="reference" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkreferencepurpose">
<span class='label'>Purpose</span>
<asp:TextBox EnableViewState="false" ID="referencepurpose" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

