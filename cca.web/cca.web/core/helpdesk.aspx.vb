Imports System.Net
Imports System.Xml

Partial Class core_helpdesk
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM


    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here

        CO = New SqlClient.SqlCommand("rkg_helpdesktemplates_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.helpdesktemplates.Items.Count = 0 Then
            Me.helpdesktemplates.DataSource = CDC.ReadDataTable(CO)
            Me.helpdesktemplates.DataTextField = "value"
            Me.helpdesktemplates.DataValueField = "pk"
            Try
                Me.helpdesktemplates.DataBind()
            Catch Ex As Exception
                Me.helpdesktemplates.SelectedValue = -1
                Me.helpdesktemplates.DataBind()
            End Try
        End If

    End Sub

   
    Protected Sub helpdesktemplates_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles helpdesktemplates.SelectedIndexChanged

        panel_response.Visible = False
        panel_nhu.Visible = False


        Select Case helpdesktemplates.SelectedItem.Value
            Case Is = 1 'New CCA App User

                panel_nhu.Visible = True
            Case Else

        End Select


    End Sub

    Protected Sub nhu_submit_Click(sender As Object, e As System.EventArgs) Handles nhu_submit.Click

        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim URL As String = sys.helpdeskurl & "/servlets/RequestServlet"

        Dim title As String = "Please add " & nhu_forename.Text.ToString & " " & nhu_surname.Text.ToString & " to the CCA Application"

        Dim Description As String = ""
        Description &= "<br />"
        Description &= "<b>New User Details</b><br />"
        Description &= "Forename: " & nhu_forename.Text.ToString & "<br />"
        Description &= "Surname: " & nhu_surname.Text.ToString & "<br />"
        Description &= "Email Address: " & nhu_emailaddress.Text.ToString & "<br />"

        Dim reqtemplate As String = helpdesktemplates.SelectedItem.Text

        Dim targetURL As String = "/servlets/RequestServlet"
        Dim operation As String = "AddRequest"

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", SM.currentuser)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)


        If helpdesklogindetail_pk > 0 Then
            Dim hd As New helpdesklogindetails(SM.currentuser, SM.actualuser, helpdesklogindetail_pk, CDC)
            Dim hd_domain As New helpdeskdomains(SM.currentuser, SM.actualuser, hd.helpdeskdomain_fk, CDC)
            Dim hd_logon As New helpdesklogondomains(SM.currentuser, SM.actualuser, hd.helpdesklogondomain_fk, CDC)


            Dim username As String = hd.username
            Dim password As String = hd.password
            Dim domain_name As String = hd_domain.domain_name
            Dim logonDomainName As String = hd_logon.logon_domain_name

            Dim postData As String = ""
            postData &= "targetURL=" & targetURL
            postData &= "&reqtemplate=" & reqtemplate
            postData &= "&username=" & username
            postData &= "&password=" & password
            postData &= "&logonDomainName=" & logonDomainName
            If domain_name.Length > 0 Then postData &= "&DOMAIN_NAME=" & domain_name
            postData &= "&operation=" & operation

            postData &= "&description=" & Description
            postData &= "&title=" & title


            Me.formResponse.Text = HTMLResponse.PostTo(URL, postData)
            Me.panelForm.Visible = False
            Me.panel_response.Visible = True

        Else

            Me.formResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
            Me.formResponse.ForeColor = Drawing.Color.Red

            Me.panelForm.Visible = False
            Me.panel_response.Visible = True

        End If
    End Sub
End Class
