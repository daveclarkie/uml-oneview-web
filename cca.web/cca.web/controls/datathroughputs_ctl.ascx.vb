Partial Class datathroughputs_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents odatathroughputs As Objects.datathroughputs
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _ThroughputPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_throughputs_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.throughput_fk.Items.Count = 0 Then
            Me.throughput_fk.DataSource = CDC.ReadDataTable(CO)
            Me.throughput_fk.DataTextField = "value"
            Me.throughput_fk.DataValueField = "pk"
            Try
                Me.throughput_fk.DataBind()
            Catch Ex As Exception
                Me.throughput_fk.SelectedValue = -1
                Me.throughput_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_months_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.month_fk.Items.Count = 0 Then
            Me.month_fk.DataSource = CDC.ReadDataTable(CO)
            Me.month_fk.DataTextField = "value"
            Me.month_fk.DataValueField = "pk"
            Try
                Me.month_fk.DataBind()
            Catch Ex As Exception
                Me.month_fk.SelectedValue = -1
                Me.month_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_years_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.year_fk.Items.Count = 0 Then
            Me.year_fk.DataSource = CDC.ReadDataTable(CO)
            Me.year_fk.DataTextField = "value"
            Me.year_fk.DataValueField = "pk"
            Try
                Me.year_fk.DataBind()
            Catch Ex As Exception
                Me.year_fk.SelectedValue = -1
                Me.year_fk.DataBind()
            End Try
        End If
        Me.totalthroughput.Text = "0.00"
        Me.eligiblethroughput.Text = "0.00"
        CO = New SqlClient.SqlCommand("rkg_dataimportmethods_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.dataimportmethod_fk.Items.Count = 0 Then
            Me.dataimportmethod_fk.DataSource = CDC.ReadDataTable(CO)
            Me.dataimportmethod_fk.DataTextField = "value"
            Me.dataimportmethod_fk.DataValueField = "pk"
            Try
                Me.dataimportmethod_fk.DataBind()
            Catch Ex As Exception
                Me.dataimportmethod_fk.SelectedValue = -1
                Me.dataimportmethod_fk.DataBind()
            End Try
        End If


        Dim atp As New agreementthroughputs(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)
        _ThroughputPk = atp.throughput_fk
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If odatathroughputs Is Nothing Then
            odatathroughputs = New Objects.datathroughputs(currentuser, actualuser, pk, CDC)
        End If
        Me.throughput_fk.SelectedValue = _ThroughputPk
        Me.month_fk.SelectedValue = odatathroughputs.month_fk
        Me.year_fk.SelectedValue = odatathroughputs.year_fk
        Me.totalthroughput.Text = odatathroughputs.totalthroughput
        Me.totalthroughput.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.totalthroughput.Attributes.Add("onBlur", "clearNotice()")
        Me.eligiblethroughput.Text = odatathroughputs.eligiblethroughput
        Me.eligiblethroughput.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.eligiblethroughput.Attributes.Add("onBlur", "clearNotice()")
        Me.dataimportmethod_fk.SelectedValue = 1


        If pk > 0 Then

            Dim im As New objects.dataimportmethods(SM.currentuser, SM.actualuser, odatathroughputs.dataimportmethod_fk, CDC)



            If odatathroughputs.dataimportmethod_fk = -1 Then
                importedmethod.Text = "."
            Else
                importedmethod.Text = im.methodname.ToString()
            End If



            CO = New SqlCommand("SELECT dbo.formatpersonname(" & odatathroughputs.creator_fk & ", 3)")
            CO.CommandType = CommandType.Text
            CDC.ReadScalarValue(created.Text, CO)

            CO = New SqlCommand("SELECT dbo.formatpersonname(" & odatathroughputs.editor_fk & ", 3)")
            CO.CommandType = CommandType.Text
            CDC.ReadScalarValue(modified.Text, CO)

            created.Text = odatathroughputs.created & " by " & created.Text
            modified.Text = odatathroughputs.modified & " by " & modified.Text

        End If

        _CurrentPk = odatathroughputs.datathroughput_pk
        Status = odatathroughputs.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If odatathroughputs Is Nothing Then
            odatathroughputs = New Objects.datathroughputs(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            odatathroughputs.throughput_fk = Me.throughput_fk.SelectedValue
            odatathroughputs.month_fk = Me.month_fk.SelectedValue
            odatathroughputs.year_fk = Me.year_fk.SelectedValue
            odatathroughputs.totalthroughput = Me.totalthroughput.Text
            odatathroughputs.eligiblethroughput = Me.eligiblethroughput.Text
            odatathroughputs.dataimportmethod_fk = Me.dataimportmethod_fk.SelectedValue
            result = odatathroughputs.Save()
            If Not result Then Throw odatathroughputs.LastError
            _CurrentPk = odatathroughputs.datathroughput_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = odatathroughputs.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If odatathroughputs Is Nothing Then
            odatathroughputs = New Objects.datathroughputs(currentuser, actualuser, pk, CDC)
        End If
        odatathroughputs.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If odatathroughputs Is Nothing Then
            odatathroughputs = New Objects.datathroughputs(currentuser, actualuser, pk, CDC)
        End If
        odatathroughputs.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If odatathroughputs Is Nothing Then
            odatathroughputs = New Objects.datathroughputs(currentuser, pk, CDC)
        End If
        odatathroughputs.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.throughput_fk.Enabled = IIf(WL >= 10, True, False)
        Me.blkthroughput_fk.Visible = IIf(RL >= 10, True, False)
        Me.month_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkmonth_fk.Visible = IIF(RL >= 0, True, False)
        Me.year_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkyear_fk.Visible = IIF(RL >= 0, True, False)
        Me.totalthroughput.Enabled = IIF(WL >= 0, True, False)
        Me.blktotalthroughput.Visible = IIF(RL >= 0, True, False)
        Me.eligiblethroughput.Enabled = IIF(WL >= 0, True, False)
        Me.blkeligiblethroughput.Visible = IIF(RL >= 0, True, False)
        Me.dataimportmethod_fk.Enabled = IIf(WL >= 10, True, False)
        Me.blkdataimportmethod_fk.Visible = IIf(RL >= 10, True, False)

        Me.month_fk.Attributes.Add("onChange", "javascript:setMonth('" + Me.month_fk.ClientID + "');")
        Me.year_fk.Attributes.Add("onChange", "javascript:setYear('" + Me.year_fk.ClientID + "');")

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "window-script", "setMonth('" + Me.month_fk.ClientID + "');setYear('" + Me.year_fk.ClientID + "');", True)


    End Sub
End Class

