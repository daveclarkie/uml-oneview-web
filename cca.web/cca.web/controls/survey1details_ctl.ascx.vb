Partial Class survey1details_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents osurvey1details As objects.survey1details
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        Me.helpdesk_id.Text = "0"
        CO = New SqlClient.SqlCommand("rkg_users_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.selecteduser_fk.Items.Count = 0 Then
            Me.selecteduser_fk.DataSource = CDC.ReadDataTable(CO)
            Me.selecteduser_fk.DataTextField = "value"
            Me.selecteduser_fk.DataValueField = "pk"
            Try
                Me.selecteduser_fk.DataBind()
            Catch Ex As Exception
                Me.selecteduser_fk.SelectedValue = -1
                Me.selecteduser_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question1_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question1_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question1_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question1_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question1_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question1_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question1_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question2_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question2_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question2_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question2_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question2_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question2_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question2_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question3_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question3_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question3_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question3_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question3_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question3_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question3_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question4_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question4_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question4_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question4_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question4_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question4_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question4_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question5_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question5_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question5_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question5_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question5_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question5_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question5_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question6_surveyanswerfivescale_fk.Items.Count = 0 Then
            Me.question6_surveyanswerfivescale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question6_surveyanswerfivescale_fk.DataTextField = "value"
            Me.question6_surveyanswerfivescale_fk.DataValueField = "pk"
            Try
                Me.question6_surveyanswerfivescale_fk.DataBind()
            Catch Ex As Exception
                Me.question6_surveyanswerfivescale_fk.SelectedValue = -1
                Me.question6_surveyanswerfivescale_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If osurvey1details Is Nothing Then
            osurvey1details = New Objects.survey1details(currentuser, actualuser, pk, CDC)
        End If
        Me.helpdesk_id.Text = osurvey1details.helpdesk_id
        Me.helpdesk_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.helpdesk_id.Attributes.Add("onBlur", "clearNotice()")
        Me.selecteduser_fk.SelectedValue = osurvey1details.selecteduser_fk
        Me.question1_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question1_surveyanswerfivescale_fk
        Me.question2_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question2_surveyanswerfivescale_fk
        Me.question3_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question3_surveyanswerfivescale_fk
        Me.question4_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question4_surveyanswerfivescale_fk
        Me.question5_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question5_surveyanswerfivescale_fk
        Me.comments.Text = osurvey1details.comments
        Me.question6_surveyanswerfivescale_fk.SelectedValue = osurvey1details.question6_surveyanswerfivescale_fk
        Me.comments2.Text = osurvey1details.comments2
        _CurrentPk = osurvey1details.surveydetail_pk
        Status = osurvey1details.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey1details Is Nothing Then
            osurvey1details = New Objects.survey1details(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            osurvey1details.helpdesk_id = Me.helpdesk_id.Text
            osurvey1details.selecteduser_fk = Me.selecteduser_fk.SelectedValue
            osurvey1details.question1_surveyanswerfivescale_fk = Me.question1_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.question2_surveyanswerfivescale_fk = Me.question2_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.question3_surveyanswerfivescale_fk = Me.question3_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.question4_surveyanswerfivescale_fk = Me.question4_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.question5_surveyanswerfivescale_fk = Me.question5_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.comments = Me.comments.Text
            osurvey1details.question6_surveyanswerfivescale_fk = Me.question6_surveyanswerfivescale_fk.SelectedValue
            osurvey1details.comments2 = Me.comments2.Text
            _CurrentPk = osurvey1details.surveydetail_pk
            result = osurvey1details.Save()
            If Not result Then Throw osurvey1details.LastError
            _CurrentPk = osurvey1details.surveydetail_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = osurvey1details.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey1details Is Nothing Then
            osurvey1details = New Objects.survey1details(currentuser, actualuser, pk, CDC)
        End If
        osurvey1details.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey1details Is Nothing Then
            osurvey1details = New Objects.survey1details(currentuser, actualuser, pk, CDC)
        End If
        osurvey1details.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey1details Is Nothing Then
            osurvey1details = New Objects.survey1details(currentuser, pk, CDC)
        End If
        osurvey1details.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.helpdesk_id.Enabled = IIf(WL >= 0, True, False)
        Me.blkhelpdesk_id.Visible = IIf(RL >= 0, True, False)
        Me.selecteduser_fk.Enabled = IIf(WL >= 99, True, False)
        Me.blkselecteduser_fk.Visible = IIf(RL >= 99, True, False)
        Me.question1_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion1_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.question2_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion2_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.question3_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion3_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.question4_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion4_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.question5_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion5_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.comments.Enabled = IIF(WL >= 0, True, False)
        Me.blkcomments.Visible = IIF(RL >= 0, True, False)
        Me.question6_surveyanswerfivescale_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkquestion6_surveyanswerfivescale_fk.Visible = IIF(RL >= 0, True, False)
        Me.comments2.Enabled = IIF(WL >= 0, True, False)
        Me.blkcomments2.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

