<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bubbles_ctl.ascx.vb" Inherits="bubbles_ctl" %>
<h5>bubbles</h5>
<ul class='formcontrol'>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation<span class='pop'>Name of the company in Contract Manager that the site belongs to? (for meter filtering purposes)</span></span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkbubblename">
<span class='label'>Bubble Name<span class='pop'>Name you wish to use for the bubble for reporting purposes</span></span>
<asp:TextBox EnableViewState="false" ID="bubblename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfederation_fk">
<span class='label'>Federation<span class='pop'>This is the Federation the bubble belongs to</span></span>
<asp:DropDownList ID="federation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktargetunitidentifier">
<span class='label'>TUID<span class='pop'>This is the Target Unit Identifier</span></span>
<asp:TextBox EnableViewState="false" ID="targetunitidentifier" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkagreementtype_fk">
<span class='label'>Bubble Type</span>
<asp:DropDownList ID="agreementtype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementmeasure_fk">
<span class='label'>Bubble Measure</span>
<asp:DropDownList ID="agreementmeasure_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkbubble_fk">
<span class='label'>Original Agreement</span>
<asp:DropDownList ID="bubble_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkincludecrcsaving">
<span class='label'>Include CRC Savings<span class='pop'>Tick this box to include the CRC exemption savings for CCAs that would otherwise be reporting its energy in CRC. Note, the savings refer only to the energy within the CCA boundary and not total organisational CRC savings.</span></span>
<asp:CheckBox ID="includecrcsaving" runat="server" cssClass="input_chk" />
</li>
</ul>

