Partial Class organisations_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oorganisations As Objects.organisations
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        Me.Custid.Text = "0"
        CO = New SqlClient.SqlCommand("rkg_countys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.county_fk.Items.Count = 0 Then
            Me.county_fk.DataSource = CDC.ReadDataTable(CO)
            Me.county_fk.DataTextField = "value"
            Me.county_fk.DataValueField = "pk"
            Try
                Me.county_fk.DataBind()
            Catch Ex As Exception
                Me.county_fk.SelectedValue = -1
                Me.county_fk.DataBind()
            End Try
        End If
        Me.prodDecisionPoint.Text = "0"
        Me.prodElecneg.Text = "0"
        Me.prodgasneg.Text = "0"
        Me.prodccl.Text = "0"
        Me.prodenergyaction.Text = "0"
        Me.prodbillaudit.Text = "0"
        Me.proddataserv.Text = "0"
        Me.ProdWaterAudit.Text = "0"
        Me.ProdWaterMgmt.Text = "0"
        Me.ProdCRCAttain.Text = "0"
        Me.ProdCRCMaintain.Text = "0"
        Me.custorprospect.Text = "0"
        Me.CCLexemption.Text = "0"
        Me.CCLcertapplied_activesupply.Text = "0"
        Me.CCLcertapplied_pendingsupply.Text = "0"
        Me.prodTelco.Text = "0"
        Me.prodMT.Text = "0"
        Me.prodTRIAD.Text = "0"
        Me.pricesensitivity.Text = "0"
        Me.umlloyaltyvalue.Text = "0"
        Me.FKaccmgrid.Text = "0"
        Me.ProdBillCheck.Text = "0"
        Me.intUserCreatedBy.Text = "0"
        Me.intUserModifiedBy.Text = "0"
        Me.intOptimaCustomerPK.Text = "0"
        Me.intDataServicesGroupSend.Text = "0"
        Me.intMandCRef.Text = "0"
        Me.intEnCoreRef.Text = "0"
        Me.intEnterpriseManagerFK.Text = "0"
        CO = New SqlClient.SqlCommand("rkg_countrys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.country_fk.Items.Count = 0 Then
            Me.country_fk.DataSource = CDC.ReadDataTable(CO)
            Me.country_fk.DataTextField = "value"
            Me.country_fk.DataValueField = "pk"
            Try
                Me.country_fk.DataBind()
            Catch Ex As Exception
                Me.country_fk.SelectedValue = -1
                Me.country_fk.DataBind()
            End Try
        End If
        Me.intSubjectExpertFK.Text = "0"
        Me.intSummitRef.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oorganisations Is Nothing Then
            oorganisations = New Objects.organisations(currentuser, actualuser, pk, CDC)
        End If
        Me.Custid.Text = oorganisations.Custid
        Me.Custid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.Custid.Attributes.Add("onBlur", "clearNotice()")
        Me.customername.Text = oorganisations.customername
        Me.address_1.Text = oorganisations.address_1
        Me.address_2.Text = oorganisations.address_2
        Me.address_3.Text = oorganisations.address_3
        Me.Address_4.Text = oorganisations.Address_4
        Me.county_fk.SelectedValue = oorganisations.county_fk
        Me.Pcode.Text = oorganisations.Pcode
        Me.Contact.Text = oorganisations.Contact
        Me.coregnum.Text = oorganisations.coregnum
        Me.prodDecisionPoint.Text = oorganisations.prodDecisionPoint
        Me.prodDecisionPoint.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodDecisionPoint.Attributes.Add("onBlur", "clearNotice()")
        Me.prodElecneg.Text = oorganisations.prodElecneg
        Me.prodElecneg.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodElecneg.Attributes.Add("onBlur", "clearNotice()")
        Me.prodgasneg.Text = oorganisations.prodgasneg
        Me.prodgasneg.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodgasneg.Attributes.Add("onBlur", "clearNotice()")
        Me.prodccl.Text = oorganisations.prodccl
        Me.prodccl.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodccl.Attributes.Add("onBlur", "clearNotice()")
        Me.prodenergyaction.Text = oorganisations.prodenergyaction
        Me.prodenergyaction.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodenergyaction.Attributes.Add("onBlur", "clearNotice()")
        Me.prodbillaudit.Text = oorganisations.prodbillaudit
        Me.prodbillaudit.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodbillaudit.Attributes.Add("onBlur", "clearNotice()")
        Me.proddataserv.Text = oorganisations.proddataserv
        Me.proddataserv.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.proddataserv.Attributes.Add("onBlur", "clearNotice()")
        Me.ProdWaterAudit.Text = oorganisations.ProdWaterAudit
        Me.ProdWaterAudit.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.ProdWaterAudit.Attributes.Add("onBlur", "clearNotice()")
        Me.ProdWaterMgmt.Text = oorganisations.ProdWaterMgmt
        Me.ProdWaterMgmt.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.ProdWaterMgmt.Attributes.Add("onBlur", "clearNotice()")
        Me.ProdCRCAttain.Text = oorganisations.ProdCRCAttain
        Me.ProdCRCAttain.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.ProdCRCAttain.Attributes.Add("onBlur", "clearNotice()")
        Me.ProdCRCMaintain.Text = oorganisations.ProdCRCMaintain
        Me.ProdCRCMaintain.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.ProdCRCMaintain.Attributes.Add("onBlur", "clearNotice()")
        Me.custorprospect.Text = oorganisations.custorprospect
        Me.custorprospect.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.custorprospect.Attributes.Add("onBlur", "clearNotice()")
        Me.CCLexemption.Text = oorganisations.CCLexemption
        Me.CCLexemption.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CCLexemption.Attributes.Add("onBlur", "clearNotice()")
        Me.CCLexemptionfromdate.Text = oorganisations.CCLexemptionfromdate.ToString("dd MMM yyyy")
        If Me.CCLexemptionfromdate.Text = "01 Jan 0001" Then Me.CCLexemptionfromdate.Text = ""
        Me.CCLexemptionfromdate.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.CCLexemptionfromdate.Attributes.Add("onBlur", "clearNotice()")
        Me.CCLcertapplied_activesupply.Text = oorganisations.CCLcertapplied_activesupply
        Me.CCLcertapplied_activesupply.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CCLcertapplied_activesupply.Attributes.Add("onBlur", "clearNotice()")
        Me.CCLcertapplied_pendingsupply.Text = oorganisations.CCLcertapplied_pendingsupply
        Me.CCLcertapplied_pendingsupply.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CCLcertapplied_pendingsupply.Attributes.Add("onBlur", "clearNotice()")
        Me.prodTelco.Text = oorganisations.prodTelco
        Me.prodTelco.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodTelco.Attributes.Add("onBlur", "clearNotice()")
        Me.prodMT.Text = oorganisations.prodMT
        Me.prodMT.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodMT.Attributes.Add("onBlur", "clearNotice()")
        Me.prodTRIAD.Text = oorganisations.prodTRIAD
        Me.prodTRIAD.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodTRIAD.Attributes.Add("onBlur", "clearNotice()")
        Me.pricesensitivity.Text = oorganisations.pricesensitivity
        Me.pricesensitivity.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.pricesensitivity.Attributes.Add("onBlur", "clearNotice()")
        Me.umlloyaltyvalue.Text = oorganisations.umlloyaltyvalue
        Me.umlloyaltyvalue.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.umlloyaltyvalue.Attributes.Add("onBlur", "clearNotice()")
        Me.VATnum.Text = oorganisations.VATnum
        Me.FKaccmgrid.Text = oorganisations.FKaccmgrid
        Me.FKaccmgrid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.FKaccmgrid.Attributes.Add("onBlur", "clearNotice()")
        Me.ProdBillCheck.Text = oorganisations.ProdBillCheck
        Me.ProdBillCheck.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.ProdBillCheck.Attributes.Add("onBlur", "clearNotice()")
        Me.dtmCreated.Text = oorganisations.dtmCreated.ToString("dd MMM yyyy")
        If Me.dtmCreated.Text = "01 Jan 0001" Then Me.dtmCreated.Text = ""
        Me.dtmCreated.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.dtmCreated.Attributes.Add("onBlur", "clearNotice()")
        Me.dtmModified.Text = oorganisations.dtmModified.ToString("dd MMM yyyy")
        If Me.dtmModified.Text = "01 Jan 0001" Then Me.dtmModified.Text = ""
        Me.dtmModified.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.dtmModified.Attributes.Add("onBlur", "clearNotice()")
        Me.intUserCreatedBy.Text = oorganisations.intUserCreatedBy
        Me.intUserCreatedBy.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intUserCreatedBy.Attributes.Add("onBlur", "clearNotice()")
        Me.intUserModifiedBy.Text = oorganisations.intUserModifiedBy
        Me.intUserModifiedBy.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intUserModifiedBy.Attributes.Add("onBlur", "clearNotice()")
        Me.intOptimaCustomerPK.Text = oorganisations.intOptimaCustomerPK
        Me.intOptimaCustomerPK.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intOptimaCustomerPK.Attributes.Add("onBlur", "clearNotice()")
        Me.intDataServicesGroupSend.Text = oorganisations.intDataServicesGroupSend
        Me.intDataServicesGroupSend.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intDataServicesGroupSend.Attributes.Add("onBlur", "clearNotice()")
        Me.bitDisabled.Checked = oorganisations.bitDisabled
        Me.varCustomerNote.Text = oorganisations.varCustomerNote
        Me.intMandCRef.Text = oorganisations.intMandCRef
        Me.intMandCRef.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intMandCRef.Attributes.Add("onBlur", "clearNotice()")
        Me.intEnCoreRef.Text = oorganisations.intEnCoreRef
        Me.intEnCoreRef.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intEnCoreRef.Attributes.Add("onBlur", "clearNotice()")
        Me.varAXRef.Text = oorganisations.varAXRef
        Me.intEnterpriseManagerFK.Text = oorganisations.intEnterpriseManagerFK
        Me.intEnterpriseManagerFK.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intEnterpriseManagerFK.Attributes.Add("onBlur", "clearNotice()")
        Me.country_fk.SelectedValue = oorganisations.country_fk
        Me.intSubjectExpertFK.Text = oorganisations.intSubjectExpertFK
        Me.intSubjectExpertFK.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intSubjectExpertFK.Attributes.Add("onBlur", "clearNotice()")
        Me.intSummitRef.Text = oorganisations.intSummitRef
        Me.intSummitRef.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intSummitRef.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = oorganisations.organisation_pk
        Status = oorganisations.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oorganisations Is Nothing Then
            oorganisations = New Objects.organisations(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oorganisations.Custid = Me.Custid.Text
            oorganisations.customername = Me.customername.Text
            oorganisations.address_1 = Me.address_1.Text
            oorganisations.address_2 = Me.address_2.Text
            oorganisations.address_3 = Me.address_3.Text
            oorganisations.Address_4 = Me.Address_4.Text
            oorganisations.county_fk = Me.county_fk.SelectedValue
            oorganisations.Pcode = Me.Pcode.Text
            oorganisations.Contact = Me.Contact.Text
            oorganisations.coregnum = Me.coregnum.Text
            oorganisations.prodDecisionPoint = Me.prodDecisionPoint.Text
            oorganisations.prodElecneg = Me.prodElecneg.Text
            oorganisations.prodgasneg = Me.prodgasneg.Text
            oorganisations.prodccl = Me.prodccl.Text
            oorganisations.prodenergyaction = Me.prodenergyaction.Text
            oorganisations.prodbillaudit = Me.prodbillaudit.Text
            oorganisations.proddataserv = Me.proddataserv.Text
            oorganisations.ProdWaterAudit = Me.ProdWaterAudit.Text
            oorganisations.ProdWaterMgmt = Me.ProdWaterMgmt.Text
            oorganisations.ProdCRCAttain = Me.ProdCRCAttain.Text
            oorganisations.ProdCRCMaintain = Me.ProdCRCMaintain.Text
            oorganisations.custorprospect = Me.custorprospect.Text
            oorganisations.CCLexemption = Me.CCLexemption.Text
            oorganisations.CCLexemptionfromdate = CommonFN.CheckEmptyDate(Me.CCLexemptionfromdate.Text)
            oorganisations.CCLcertapplied_activesupply = Me.CCLcertapplied_activesupply.Text
            oorganisations.CCLcertapplied_pendingsupply = Me.CCLcertapplied_pendingsupply.Text
            oorganisations.prodTelco = Me.prodTelco.Text
            oorganisations.prodMT = Me.prodMT.Text
            oorganisations.prodTRIAD = Me.prodTRIAD.Text
            oorganisations.pricesensitivity = Me.pricesensitivity.Text
            oorganisations.umlloyaltyvalue = Me.umlloyaltyvalue.Text
            oorganisations.VATnum = Me.VATnum.Text
            oorganisations.FKaccmgrid = Me.FKaccmgrid.Text
            oorganisations.ProdBillCheck = Me.ProdBillCheck.Text
            oorganisations.dtmCreated = CommonFN.CheckEmptyDate(Me.dtmCreated.Text)
            oorganisations.dtmModified = CommonFN.CheckEmptyDate(Me.dtmModified.Text)
            oorganisations.intUserCreatedBy = Me.intUserCreatedBy.Text
            oorganisations.intUserModifiedBy = Me.intUserModifiedBy.Text
            oorganisations.intOptimaCustomerPK = Me.intOptimaCustomerPK.Text
            oorganisations.intDataServicesGroupSend = Me.intDataServicesGroupSend.Text
            oorganisations.bitDisabled = Me.bitDisabled.Checked
            oorganisations.varCustomerNote = Me.varCustomerNote.Text
            oorganisations.intMandCRef = Me.intMandCRef.Text
            oorganisations.intEnCoreRef = Me.intEnCoreRef.Text
            oorganisations.varAXRef = Me.varAXRef.Text
            oorganisations.intEnterpriseManagerFK = Me.intEnterpriseManagerFK.Text
            oorganisations.country_fk = Me.country_fk.SelectedValue
            oorganisations.intSubjectExpertFK = Me.intSubjectExpertFK.Text
            oorganisations.intSummitRef = Me.intSummitRef.Text
            result = oorganisations.Save()
            If Not result Then Throw oorganisations.LastError
            _CurrentPk = oorganisations.organisation_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oorganisations.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oorganisations Is Nothing Then
            oorganisations = New Objects.organisations(currentuser, actualuser, pk, CDC)
        End If
        oorganisations.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oorganisations Is Nothing Then
            oorganisations = New Objects.organisations(currentuser, actualuser, pk, CDC)
        End If
        oorganisations.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oorganisations Is Nothing Then
            oorganisations = New Objects.organisations(currentuser, pk, CDC)
        End If
        oorganisations.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.Custid.Enabled = IIF(WL >= 0, True, False)
        Me.blkCustid.Visible = IIF(RL >= 0, True, False)
        Me.customername.Enabled = IIF(WL >= 0, True, False)
        Me.blkcustomername.Visible = IIF(RL >= 0, True, False)
        Me.address_1.Enabled = IIF(WL >= 0, True, False)
        Me.blkaddress_1.Visible = IIF(RL >= 0, True, False)
        Me.address_2.Enabled = IIF(WL >= 0, True, False)
        Me.blkaddress_2.Visible = IIF(RL >= 0, True, False)
        Me.address_3.Enabled = IIF(WL >= 0, True, False)
        Me.blkaddress_3.Visible = IIF(RL >= 0, True, False)
        Me.Address_4.Enabled = IIF(WL >= 0, True, False)
        Me.blkAddress_4.Visible = IIF(RL >= 0, True, False)
        Me.county_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkcounty_fk.Visible = IIF(RL >= 0, True, False)
        Me.Pcode.Enabled = IIF(WL >= 0, True, False)
        Me.blkPcode.Visible = IIF(RL >= 0, True, False)
        Me.country_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkcountry_fk.Visible = IIf(RL >= 0, True, False)
        Me.Contact.Enabled = IIf(WL >= 0, True, False)
        Me.blkContact.Visible = IIF(RL >= 0, True, False)
        Me.coregnum.Enabled = IIF(WL >= 0, True, False)
        Me.blkcoregnum.Visible = IIF(RL >= 0, True, False)
        Me.prodDecisionPoint.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodDecisionPoint.Visible = IIf(RL >= 99, True, False)
        Me.prodElecneg.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodElecneg.Visible = IIf(RL >= 99, True, False)
        Me.prodgasneg.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodgasneg.Visible = IIf(RL >= 99, True, False)
        Me.prodccl.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodccl.Visible = IIf(RL >= 99, True, False)
        Me.prodenergyaction.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodenergyaction.Visible = IIf(RL >= 99, True, False)
        Me.prodbillaudit.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodbillaudit.Visible = IIf(RL >= 99, True, False)
        Me.proddataserv.Enabled = IIf(WL >= 99, True, False)
        Me.blkproddataserv.Visible = IIf(RL >= 99, True, False)
        Me.ProdWaterAudit.Enabled = IIf(WL >= 99, True, False)
        Me.blkProdWaterAudit.Visible = IIf(RL >= 99, True, False)
        Me.ProdWaterMgmt.Enabled = IIf(WL >= 99, True, False)
        Me.blkProdWaterMgmt.Visible = IIf(RL >= 99, True, False)
        Me.ProdCRCAttain.Enabled = IIf(WL >= 99, True, False)
        Me.blkProdCRCAttain.Visible = IIf(RL >= 99, True, False)
        Me.ProdCRCMaintain.Enabled = IIf(WL >= 99, True, False)
        Me.blkProdCRCMaintain.Visible = IIf(RL >= 99, True, False)
        Me.custorprospect.Enabled = IIf(WL >= 99, True, False)
        Me.blkcustorprospect.Visible = IIf(RL >= 99, True, False)
        Me.CCLexemption.Enabled = IIf(WL >= 99, True, False)
        Me.blkCCLexemption.Visible = IIf(RL >= 99, True, False)
        Me.CCLexemptionfromdate.Enabled = IIf(WL >= 99, True, False)
        Me.blkCCLexemptionfromdate.Visible = IIf(RL >= 99, True, False)
        Me.CCLcertapplied_activesupply.Enabled = IIf(WL >= 99, True, False)
        Me.blkCCLcertapplied_activesupply.Visible = IIf(RL >= 99, True, False)
        Me.CCLcertapplied_pendingsupply.Enabled = IIf(WL >= 99, True, False)
        Me.blkCCLcertapplied_pendingsupply.Visible = IIf(RL >= 99, True, False)
        Me.prodTelco.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodTelco.Visible = IIf(RL >= 99, True, False)
        Me.prodMT.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodMT.Visible = IIf(RL >= 99, True, False)
        Me.prodTRIAD.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodTRIAD.Visible = IIf(RL >= 99, True, False)
        Me.pricesensitivity.Enabled = IIf(WL >= 99, True, False)
        Me.blkpricesensitivity.Visible = IIf(RL >= 99, True, False)
        Me.umlloyaltyvalue.Enabled = IIf(WL >= 99, True, False)
        Me.blkumlloyaltyvalue.Visible = IIf(RL >= 99, True, False)
        Me.VATnum.Enabled = IIF(WL >= 0, True, False)
        Me.blkVATnum.Visible = IIF(RL >= 0, True, False)
        Me.FKaccmgrid.Enabled = IIF(WL >= 0, True, False)
        Me.blkFKaccmgrid.Visible = IIF(RL >= 0, True, False)
        Me.ProdBillCheck.Enabled = IIf(WL >= 99, True, False)
        Me.blkProdBillCheck.Visible = IIf(RL >= 99, True, False)
        Me.dtmCreated.Enabled = IIf(WL >= 99, True, False)
        Me.blkdtmCreated.Visible = IIf(RL >= 99, True, False)
        Me.dtmModified.Enabled = IIf(WL >= 99, True, False)
        Me.blkdtmModified.Visible = IIf(RL >= 99, True, False)
        Me.intUserCreatedBy.Enabled = IIf(WL >= 99, True, False)
        Me.blkintUserCreatedBy.Visible = IIf(RL >= 99, True, False)
        Me.intUserModifiedBy.Enabled = IIf(WL >= 99, True, False)
        Me.blkintUserModifiedBy.Visible = IIf(RL >= 99, True, False)
        Me.intOptimaCustomerPK.Enabled = IIf(WL >= 99, True, False)
        Me.blkintOptimaCustomerPK.Visible = IIf(RL >= 99, True, False)
        Me.intDataServicesGroupSend.Enabled = IIf(WL >= 99, True, False)
        Me.blkintDataServicesGroupSend.Visible = IIf(RL >= 99, True, False)
        Me.bitDisabled.Enabled = IIf(WL >= 99, True, False)
        Me.blkbitDisabled.Visible = IIf(RL >= 99, True, False)
        Me.varCustomerNote.Enabled = IIf(WL >= 99, True, False)
        Me.blkvarCustomerNote.Visible = IIf(RL >= 99, True, False)
        Me.intMandCRef.Enabled = IIf(WL >= 99, True, False)
        Me.blkintMandCRef.Visible = IIf(RL >= 99, True, False)
        Me.intEnCoreRef.Enabled = IIf(WL >= 99, True, False)
        Me.blkintEnCoreRef.Visible = IIf(RL >= 99, True, False)
        Me.varAXRef.Enabled = IIF(WL >= 0, True, False)
        Me.blkvarAXRef.Visible = IIF(RL >= 0, True, False)
        Me.intEnterpriseManagerFK.Enabled = IIf(WL >= 99, True, False)
        Me.blkintEnterpriseManagerFK.Visible = IIf(RL >= 99, True, False)
        Me.intSubjectExpertFK.Enabled = IIf(WL >= 99, True, False)
        Me.blkintSubjectExpertFK.Visible = IIf(RL >= 99, True, False)
        Me.intSummitRef.Enabled = IIf(WL >= 99, True, False)
        Me.blkintSummitRef.Visible = IIf(RL >= 99, True, False)
    End Sub
End Class

