Imports System.Net.Mail
Imports System.Net

Partial Class custom_addpersons_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        ctlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents opersons As objects.persons
    Public WithEvents oemails As objects.emails

    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_titles_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.title_fk.Items.Count = 0 Then
            Me.title_fk.DataSource = CDC.ReadDataTable(CO)
            Me.title_fk.DataTextField = "value"
            Me.title_fk.DataValueField = "pk"
            Try
                Me.title_fk.DataBind()
            Catch Ex As Exception
                Me.title_fk.SelectedValue = -1
                Me.title_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_organisations_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.organisation_fk.Items.Count = 0 Then
            Me.organisation_fk.DataSource = CDC.ReadDataTable(CO)
            Me.organisation_fk.DataTextField = "value"
            Me.organisation_fk.DataValueField = "pk"
            Try
                Me.organisation_fk.DataBind()
            Catch Ex As Exception
                Me.organisation_fk.SelectedValue = -1
                Me.organisation_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If opersons Is Nothing Then
            opersons = New objects.persons(currentuser, actualuser, pk, CDC)
        End If
        _CurrentPk = opersons.person_pk
        Status = opersons.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Integer
        Dim result As Integer = 0
        Dim vPK As Integer = 0

        Dim vForename As String = Me.forename.Text
        Dim vSurname As String = Me.surname.Text
        Dim vEmail As String = Me.email.Text

        'Check User  - Forename, Surname, Email
        If Not ValidateEmail(vEmail) Then
            AddNotice("setNotice('Inavlid Email');")

        ElseIf CheckEmailAddress(vEmail) > 0 Then
            AddNotice("setNotice('Email Already Exists');")

        ElseIf CheckPersonDetails(vForename, vSurname, vEmail) > 0 Then ' this should never really fire
            AddNotice("setNotice('Person Already Exists');")
        Else
            Try
                vPK = CreateNewPerson(vForename, vSurname, vEmail)
                _CurrentPk = vPK
                result = vPK
            Catch Ex As Exception
                result = 0
                LastError = Ex
            End Try

        End If

        Return result
    End Function

    Public Function ValidateEmail(ByVal strCheck As String) As Boolean
        Try
            Dim vEmailAddress As New System.Net.Mail.MailAddress(strCheck)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function CheckEmailAddress(ByVal emailaddress As String) As Integer
        Dim result As Integer = 0
        CO = New SqlClient.SqlCommand("rsp_addperson_checkemailaddress")
        CO.Parameters.AddWithValue("@email", emailaddress)
        CO.CommandType = CommandType.StoredProcedure
        CDC.ReadScalarValue(result, CO)
        Return result
    End Function

    Public Function CheckPersonDetails(ByVal forename As String, ByVal surname As String, ByVal emailaddress As String) As Integer
        Dim result As Integer = 0
        CO = New SqlClient.SqlCommand("rsp_addperson_checkpersondetails")
        CO.Parameters.AddWithValue("@forename", forename)
        CO.Parameters.AddWithValue("@surname", surname)
        CO.Parameters.AddWithValue("@email", emailaddress)
        CO.CommandType = CommandType.StoredProcedure
        CDC.ReadScalarValue(result, CO)
        Return result
    End Function

    Private Function CreateNewPerson(ByVal forename As String, ByVal surname As String, ByVal email As String, Optional ByVal readlevel As Integer = 1, Optional ByVal writelevel As Integer = 1, Optional ByVal password As String = "") As Integer
        Dim result As Integer = 0
        Dim username As String = Nothing
        Dim useremail As String = ""
        Dim epwd() As Byte = Nothing


        Dim CO As New SqlCommand
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure

        CO.CommandText = "rsp_CreateUserName"
        CO.Parameters.AddWithValue("@fn", forename.ToLower)
        CO.Parameters.AddWithValue("@sn", surname.ToLower)
        CDC.ReadScalarValue(username, CO)
        CO.Parameters.Clear()

        If password.Length < 5 Then
            CO.CommandText = "rsp_CreatePasswd"
            CDC.ReadScalarValue(password, CO)
            CO.Parameters.Clear()
        End If

        CO.CommandText = "rsp_CreatePasswdEnc"
        CO.Parameters.AddWithValue("@pwd", password)
        CDC.ReadScalarValue(epwd, CO)
        CO.Parameters.Clear()

        Dim u As New users(SM.currentuser, SM.actualuser, CDC)
        u.username = username
        u.encpassword = epwd
        u.readlevel = readlevel
        u.writelevel = writelevel
        If u.Save() Then
            Dim p As New persons(SM.currentuser, SM.actualuser, CDC)
            p.forename = forename
            p.surname = surname
            p.user_fk = u.user_pk
            p.middlename = ""
            p.jobtitle = ""
            If p.Save() Then
                Dim e As New emails(SM.currentuser, SM.actualuser, CDC)
                e.email = email
                If e.Save() Then
                    Dim ue As New useremails(SM.currentuser, SM.actualuser, CDC)
                    ue.user_fk = u.user_pk
                    ue.email_fk = e.email_pk
                    ue.contactlocation_fk = 2
                    If ue.Save() Then
                        useremail = ue.useremail_pk
                    End If
                End If
            End If

            If email.EndsWith("@ems.schneider-electric.com") Then
                Dim ug As New usergroups(SM.currentuser, SM.actualuser, CDC) ' add to the staff group
                ug.user_fk = u.user_pk
                ug.group_fk = 4
                ug.Save()
            End If

        End If

        Dim strBody As String = ""

        strBody &= "UserID: " & u.user_pk & "<br />"
        strBody &= "Username: " & u.username & "<br />"
        strBody &= "Password: " & password & "<br />"
        strBody &= "Email: " & email & "<br />"
        strBody &= "useremail_pk: " & useremail & "<br />"
        strBody &= "URL: <a href='http://oneview.mceg.local'>oneview.mceg.local</a>"


        SendSMTP("donotreply@ems.schneider-electric.com", email, "One View User Account Created", strBody, "", "HTML")
        result = u.user_pk
        Return result
    End Function
    Private Sub SendSMTP(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String, Optional ByVal format As String = "Text", Optional ByVal BCC As String = "", Optional ByVal User As String = "", Optional ByVal Pass As String = "")
        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
        htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P>"
        htmlMessageBody = htmlMessageBody + strBody
        htmlMessageBody = htmlMessageBody + "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"

        strTo = "dave.clarke@ems.schneider-electric.com"

        Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
        With insMail
            .Subject = strSubject
            If format = "HTML" Then : .IsBodyHtml = True : Else : .IsBodyHtml = True : End If
            .Body = htmlMessageBody
            If BCC.Length > 0 Then
                .Bcc.Add(New MailAddress(BCC))
            End If
            .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
            If Not strAttachments.Equals(String.Empty) Then
                Dim strFile As String
                Dim strAttach() As String = strAttachments.Split(";"c)
                For Each strFile In strAttach
                    .Attachments.Add(New System.Net.Mail.Attachment(strFile.Trim()))
                Next
            End If
        End With
        Dim smtp As New System.Net.Mail.SmtpClient

        If User.Length > 0 Then
            smtp.Credentials = New NetworkCredential(User, Pass)
        Else
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials
        End If

        smtp.Host = "10.44.33.23"
        smtp.Port = 25
        smtp.Send(insMail)
    End Sub


    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If opersons Is Nothing Then
            opersons = New objects.persons(currentuser, actualuser, pk, CDC)
        End If
        opersons.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If opersons Is Nothing Then
            opersons = New objects.persons(currentuser, actualuser, pk, CDC)
        End If
        opersons.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If opersons Is Nothing Then
            opersons = New objects.persons(currentuser, pk, CDC)
        End If
        opersons.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Me.title_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blktitle_fk.Visible = IIf(RL >= 0, True, False)
        Me.forename.Enabled = IIf(WL >= 0, True, False)
        Me.blkforename.Visible = IIf(RL >= 0, True, False)
        Me.middlename.Enabled = IIf(WL >= 0, True, False)
        Me.blkmiddlename.Visible = IIf(RL >= 0, True, False)
        Me.surname.Enabled = IIf(WL >= 0, True, False)
        Me.blksurname.Visible = IIf(RL >= 0, True, False)
        Me.jobtitle.Enabled = IIf(WL >= 0, True, False)
        Me.blkjobtitle.Visible = IIf(RL >= 0, True, False)
        Me.organisation_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkorganisation_fk.Visible = IIf(RL >= 0, True, False)
        Me.email.Enabled = IIf(WL >= 0, True, False)
        Me.blkemail.Visible = IIf(RL >= 0, True, False)
    End Sub
End Class

