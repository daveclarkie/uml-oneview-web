﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="meters.ascx.vb" Inherits="controls_nav_meters" %>
 <%If LoadList() Then%>
    <div class="navigationmenu" id="pnlMeterPoints" runat="server">
        <h4>Meters</h4>
        
        <asp:Repeater ID="rptMeterPoints" runat="server">
            <HeaderTemplate>
                <table>
            </HeaderTemplate>
            <ItemTemplate>
            
                <tr>
                    <td style="width:130px;"><%# IIf(Len(Container.DataItem("meter")) > 20, Left(Container.DataItem("meter"), 17) & "...", Container.DataItem("meter"))%></td>
                    <td style="width:100px;"><%# Container.DataItem("fuelname")%></td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
<%End If%>
