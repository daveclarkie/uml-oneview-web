﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bubbles.ascx.vb" Inherits="controls_nav_bubbles" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

 <%If LoadList() Then%>
    <h3>Selected Bubble</h3>
       <ul class="navigationmenu">
            <table>
                <tr>
                    <td style="width:80px;"><b>Name</b></td>
                    <td><asp:Label ID="lblAgreementName" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="btnExportAgreementData" runat="server" class="button_orangesmall" Text="Export One View Data" Width="180px"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <asp:LinkButton ID="lnkCustomerFolder" runat="server" Visible="false"><h6>Open Customer Folder</h6></asp:LinkButton>
                        <asp:HyperLink Text='spoon'  id="DeploymentLocation" runat="server" Target="_blank" Visible="false" ></asp:HyperLink>
                    </td>
                </tr>

            </table>
       </ul>
    
       
                <rsweb:ReportViewer Visible="false" ID="myReportViewer" AsyncRendering="true" ShowPrintButton="false" runat="server" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                    </rsweb:ReportViewer> 
       </div>
        <%End If%>
