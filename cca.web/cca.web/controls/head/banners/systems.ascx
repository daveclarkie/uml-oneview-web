<%@ Control Language="VB" AutoEventWireup="false" CodeFile="systems.ascx.vb" Inherits="design_elements_banners_systems" %>
<%@ Register Src="../systems.ascx" TagName="systems" TagPrefix="uc1" %>
<h1 id="logo">Schneider Electric</h1>
<div id="right">
    <ul id="notifications">
        <li><uc1:systems ID="ntfFailed" runat="server" Label="Failed Logins" SystemItemType="FailedLogins" /></li>
        <li><uc1:systems ID="ntfActive" runat="server" Label="Active Users" SystemItemType="ActiveUsers" /></li>
        <li><uc1:systems ID="ntfMultiple" runat="server" Label="Multiple Logins" SystemItemType="MultipleLogins" /></li>
        <li><uc1:systems ID="ntfDormant" runat="server" Label="Dormant Accounts" SystemItemType="DormantAccounts" /></li>
        <li><uc1:systems ID="ntfSessions" runat="server" Label="Active Sessions" SystemItemType="ActiveSessions" /></li>
        
    </ul>
</div>