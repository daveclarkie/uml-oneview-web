Partial Class helpdeskworkorderdetails_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents ohelpdeskworkorderdetails As Objects.helpdeskworkorderdetails
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        Me.helpdesk_id.Text = "0"
        Me.requester_id.Text = "0"
        Me.technician_id.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If ohelpdeskworkorderdetails Is Nothing Then
            ohelpdeskworkorderdetails = New Objects.helpdeskworkorderdetails(currentuser, actualuser, pk, CDC)
        End If
        Me.helpdesk_id.Text = ohelpdeskworkorderdetails.helpdesk_id
        Me.helpdesk_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.helpdesk_id.Attributes.Add("onBlur", "clearNotice()")
        Me.created_time.Text = ohelpdeskworkorderdetails.created_time.ToString("dd MMM yyyy")
        If Me.created_time.Text = "01 Jan 0001" Then Me.created_time.Text = ""
        Me.created_time.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.created_time.Attributes.Add("onBlur", "clearNotice()")
        Me.requester.Text = ohelpdeskworkorderdetails.requester
        Me.requester_id.Text = ohelpdeskworkorderdetails.requester_id
        Me.requester_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.requester_id.Attributes.Add("onBlur", "clearNotice()")
        Me.technician.Text = ohelpdeskworkorderdetails.technician
        Me.technician_id.Text = ohelpdeskworkorderdetails.technician_id
        Me.technician_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.technician_id.Attributes.Add("onBlur", "clearNotice()")
        Me.client.Text = ohelpdeskworkorderdetails.client
        Me.tender_type.Text = ohelpdeskworkorderdetails.tender_type
        Me.country.Text = ohelpdeskworkorderdetails.country
        Me.contract_renewal.Text = ohelpdeskworkorderdetails.contract_renewal.ToString("dd MMM yyyy")
        If Me.contract_renewal.Text = "01 Jan 0001" Then Me.contract_renewal.Text = ""
        Me.contract_renewal.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.contract_renewal.Attributes.Add("onBlur", "clearNotice()")
        Me.tender_due.Text = ohelpdeskworkorderdetails.tender_due.ToString("dd MMM yyyy")
        If Me.tender_due.Text = "01 Jan 0001" Then Me.tender_due.Text = ""
        Me.tender_due.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.tender_due.Attributes.Add("onBlur", "clearNotice()")
        Me.hdstatus.Text = ohelpdeskworkorderdetails.status
        Me.commodity.Text = ohelpdeskworkorderdetails.commodity
        Me.completed_time.Text = ohelpdeskworkorderdetails.completed_time.ToString("dd MMM yyyy")
        If Me.completed_time.Text = "01 Jan 0001" Then Me.completed_time.Text = ""
        Me.completed_time.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.completed_time.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = ohelpdeskworkorderdetails.helpdeskworkorderdetail_pk
        Status = ohelpdeskworkorderdetails.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ohelpdeskworkorderdetails Is Nothing Then
            ohelpdeskworkorderdetails = New Objects.helpdeskworkorderdetails(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            ohelpdeskworkorderdetails.helpdesk_id = Me.helpdesk_id.Text
            ohelpdeskworkorderdetails.created_time = CommonFN.CheckEmptyDate(Me.created_time.Text)
            ohelpdeskworkorderdetails.requester = Me.requester.Text
            ohelpdeskworkorderdetails.requester_id = Me.requester_id.Text
            ohelpdeskworkorderdetails.technician = Me.technician.Text
            ohelpdeskworkorderdetails.technician_id = Me.technician_id.Text
            ohelpdeskworkorderdetails.client = Me.client.Text
            ohelpdeskworkorderdetails.tender_type = Me.tender_type.Text
            ohelpdeskworkorderdetails.country = Me.country.Text
            ohelpdeskworkorderdetails.contract_renewal = CommonFN.CheckEmptyDate(Me.contract_renewal.Text)
            ohelpdeskworkorderdetails.tender_due = CommonFN.CheckEmptyDate(Me.tender_due.Text)
            ohelpdeskworkorderdetails.status = Me.hdstatus.Text
            ohelpdeskworkorderdetails.commodity = Me.commodity.Text
            ohelpdeskworkorderdetails.completed_time = CommonFN.CheckEmptyDate(Me.completed_time.Text)
            result = ohelpdeskworkorderdetails.Save()
            If Not result Then Throw ohelpdeskworkorderdetails.LastError
            _CurrentPk = ohelpdeskworkorderdetails.helpdeskworkorderdetail_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = ohelpdeskworkorderdetails.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ohelpdeskworkorderdetails Is Nothing Then
            ohelpdeskworkorderdetails = New Objects.helpdeskworkorderdetails(currentuser, actualuser, pk, CDC)
        End If
        ohelpdeskworkorderdetails.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ohelpdeskworkorderdetails Is Nothing Then
            ohelpdeskworkorderdetails = New Objects.helpdeskworkorderdetails(currentuser, actualuser, pk, CDC)
        End If
        ohelpdeskworkorderdetails.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ohelpdeskworkorderdetails Is Nothing Then
            ohelpdeskworkorderdetails = New Objects.helpdeskworkorderdetails(currentuser, pk, CDC)
        End If
        ohelpdeskworkorderdetails.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.helpdesk_id.Enabled = IIF(WL >= 0, True, False)
        Me.blkhelpdesk_id.Visible = IIF(RL >= 0, True, False)
        Me.created_time.Enabled = IIF(WL >= 0, True, False)
        Me.blkcreated_time.Visible = IIF(RL >= 0, True, False)
        Me.requester.Enabled = IIF(WL >= 0, True, False)
        Me.blkrequester.Visible = IIF(RL >= 0, True, False)
        Me.requester_id.Enabled = IIF(WL >= 0, True, False)
        Me.blkrequester_id.Visible = IIF(RL >= 0, True, False)
        Me.technician.Enabled = IIF(WL >= 0, True, False)
        Me.blktechnician.Visible = IIF(RL >= 0, True, False)
        Me.technician_id.Enabled = IIF(WL >= 0, True, False)
        Me.blktechnician_id.Visible = IIF(RL >= 0, True, False)
        Me.client.Enabled = IIF(WL >= 0, True, False)
        Me.blkclient.Visible = IIF(RL >= 0, True, False)
        Me.tender_type.Enabled = IIF(WL >= 0, True, False)
        Me.blktender_type.Visible = IIF(RL >= 0, True, False)
        Me.country.Enabled = IIF(WL >= 0, True, False)
        Me.blkcountry.Visible = IIF(RL >= 0, True, False)
        Me.contract_renewal.Enabled = IIF(WL >= 0, True, False)
        Me.blkcontract_renewal.Visible = IIF(RL >= 0, True, False)
        Me.tender_due.Enabled = IIF(WL >= 0, True, False)
        Me.blktender_due.Visible = IIF(RL >= 0, True, False)
        Me.hdstatus.Enabled = IIf(WL >= 0, True, False)
        Me.blkstatus.Visible = IIF(RL >= 0, True, False)
        Me.commodity.Enabled = IIF(WL >= 0, True, False)
        Me.blkcommodity.Visible = IIF(RL >= 0, True, False)
        Me.completed_time.Enabled = IIF(WL >= 0, True, False)
        Me.blkcompleted_time.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

