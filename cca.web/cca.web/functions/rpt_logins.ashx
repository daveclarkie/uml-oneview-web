<%@ WebHandler Language="VB" Class="rpt_logins" %>
Public Class rpt_logins
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim gv As GridView
        Dim rpt As Repeater
        Dim dt As DataTable
        Dim CO As New SqlCommand
        Dim rq As Object = Request.QueryString
        Dim showGrid As Boolean = False
        Dim download As Boolean = False
        
        Try
            Response.Clear()
            Try
                If rq("g") = "1" Then showGrid = True
            Catch ex As Exception
            End Try

            Try
                If rq("d") = "1" Then download = True
            Catch ex As Exception
            End Try


            CO.CommandText = "[rpt_userlogins]"
            CO.CommandType = CommandType.StoredProcedure
            If rq("r") = "" Then
                CO.Parameters.AddWithValue("@user", 0)
            Else
                CO.Parameters.AddWithValue("@user", rq("r"))
            End If
            
            
            
            dt = CDC.ReadDataTable(CO)
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
            
            Select Case showGrid
                Case True
                    gv = New GridView
                    Dim col As DataColumn
                    Dim colfield As TemplateField
                    For Each col In dt.Columns
                        colfield = New TemplateField()
                        colfield.HeaderTemplate = New GridViewTemplate(ListItemType.Header, col.ColumnName)
                        colfield.ItemTemplate = New GridViewTemplate(ListItemType.Item, col.ColumnName)
                        gv.Columns.Add(colfield)
                    Next
                    
                    gv.EnableViewState = False
                    gv.AutoGenerateColumns = False
                    gv.AllowPaging = False
                    gv.AllowSorting = False
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.RenderControl(htmlWrite)
                    
                Case False
                    rpt = New Repeater
                    rpt.EnableViewState = False
                    rpt.ItemTemplate = New ListItemTemplate()
                    rpt.DataSource = dt
                    rpt.DataBind()
                    rpt.RenderControl(htmlWrite)
            End Select
            
            
            
            If download Then
                Response.AddHeader("content-disposition", "attachment;filename=report-" & rq("nc") & ".xls")
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = "application/vnd.xls"
                Response.Write("<html><head><style>.text { mso-number-format:\@; } </style></head><body>")
                Response.Write(stringWrite.ToString.Replace("<td>", "<td class='text'>"))
                Response.Write("</body></html>")
            Else
                Response.ContentType = "text/html"
                Response.Write(stringWrite.ToString)
            End If
            
        Catch LE As Exception
            Response.Write(LE.Message)
        End Try
    End Sub

    Public Class ListItemTemplate
        Implements ITemplate
        Public Sub New()
        End Sub

        Private Sub ITemplate_InstantiateIn(ByVal container As System.Web.UI.Control) Implements ITemplate.InstantiateIn
            Dim lit As New Literal
            AddHandler lit.DataBinding, AddressOf lbl_DataBinding
            container.Controls.Add(lit)
        End Sub

        Private Sub lbl_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim lit As Literal = DirectCast(sender, Literal)
            Dim data As Object = DirectCast(lit.NamingContainer, RepeaterItem).DataItem
            Dim dbr As DataRowView = DirectCast(data, DataRowView)

            Dim s As String = "Mode not supported"
        
            's &= "<li><a href='javascript:reqpermissions("
            's &= dbr.Item(0)
            's &= ")' >[ i ]</a>"
            's &= dbr.Item(7)
            's &= " ::: "
            's &= dbr.Item(6)
            's &= "</li>"
        
            lit.Text = s
        

        End Sub

    End Class
End Class


