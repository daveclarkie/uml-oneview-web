Partial Class design_elements_navigation_user
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub

    Dim isSales As Boolean
    Dim isStaff As Boolean
    Dim isAdmin As Boolean
    Dim isSystem As Boolean
    Dim isBackend As Boolean

    Dim CO As New SqlClient.SqlCommand()

    Public ReadOnly Property currentuserforename() As String
        Get
            CO = New SqlClient.SqlCommand()
            CO.CommandType = CommandType.Text
            CO.CommandText = "select dbo.formatpersonname(" & SM.currentuser & ",9)"
            Dim retval As String = ""
            CDC.ReadScalarValue(retval, CO)
            Return retval
        End Get
    End Property

    Public ReadOnly Property HasAssistantAccess() As Boolean
        Get
            Dim CO As New SqlClient.SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_user_hasassistantaccess"
            CO.Parameters.AddWithValue("@user_fk", SM.actualuser)
            Dim retval As Integer = 0
            CDC.ReadScalarValue(retval, CO)
            Return retval
        End Get
    End Property

    Public ReadOnly Property SearchAccess() As Boolean
        Get
            'isStaff = Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
            'isAdmin = Security.GroupMember(SM.currentuser, SystemGroups.Admin, CDC)
            'isSystem = Security.GroupMember(SM.currentuser, SystemGroups.Systems, CDC)
            'Return (isSales Or isStaff Or isAdmin Or isSystem)
            Return True
        End Get
    End Property

    Public ReadOnly Property BackendAccess() As Boolean
        Get
            isBackend = Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC)
            Return isBackend
        End Get
    End Property

    Public ReadOnly Property ccaSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.CCASystem, CDC)
        End Get
    End Property

    Public ReadOnly Property demandSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.DemandSystem, CDC)
        End Get
    End Property


    Public ReadOnly Property matrixSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.MatrixSystem, CDC)
        End Get
    End Property

    Public ReadOnly Property cmSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.ContractManagerSystem, CDC)
        End Get
    End Property




    Public ReadOnly Property AppointmentAccess() As Boolean
        Get
            Dim CO As New SqlClient.SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_hasappointments"
            CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
            Dim retval As Integer = 0
            CDC.ReadScalarValue(retval, CO)
            Return retval
        End Get
    End Property

    Public ReadOnly Property PasswordTask() As Integer
        Get
            Dim CO As New SqlClient.SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_mypasswordtask"
            CO.Parameters.AddWithValue("@user", SM.currentuser)
            Dim retval As Integer = 0
            CDC.ReadScalarValue(retval, CO)
            Return retval
        End Get
    End Property


End Class
