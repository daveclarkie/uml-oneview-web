Partial Class appointmenttypes_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oappointmenttypes As Objects.appointmenttypes
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oappointmenttypes is Nothing then
oappointmenttypes = new Objects.appointmenttypes(currentuser,actualuser,pk,CDC)
End If
Me.appointmenttypename.Text=oappointmenttypes.appointmenttypename
Me.foreignkeyname.Text=oappointmenttypes.foreignkeyname
_CurrentPk=oappointmenttypes.appointmenttype_pk
Status=oappointmenttypes.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oappointmenttypes is Nothing then
oappointmenttypes = new Objects.appointmenttypes(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oappointmenttypes.appointmenttypename=Me.appointmenttypename.Text
oappointmenttypes.foreignkeyname=Me.foreignkeyname.Text
result=oappointmenttypes.Save()
if not result then throw oappointmenttypes.LastError
_CurrentPk=oappointmenttypes.appointmenttype_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oappointmenttypes.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointmenttypes is Nothing then
   oappointmenttypes = new Objects.appointmenttypes(currentuser,actualuser,pk,CDC)
  End If
  oappointmenttypes.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointmenttypes is Nothing then
   oappointmenttypes = new Objects.appointmenttypes(currentuser,actualuser,pk,CDC)
  End If
  oappointmenttypes.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointmenttypes is Nothing then
   oappointmenttypes = new Objects.appointmenttypes(currentuser,pk,CDC)
  End If
  oappointmenttypes.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.appointmenttypename.Enabled=IIF(WL>=0,True,False)
  Me.blkappointmenttypename.Visible=IIF(RL>=0,True,False)
  Me.foreignkeyname.Enabled=IIF(WL>=0,True,False)
  Me.blkforeignkeyname.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

