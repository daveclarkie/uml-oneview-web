Partial Class design_elements_navigation_systems
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Private Sub doLogin(ByVal UID As Integer)
        Dim loginOK As Boolean = False
        Dim CO As New SqlClient.SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_Login1"

        CO.Parameters.AddWithValue("@uid", UID)

        Dim dr As DataRow = CDC.ReadDataRow(CO)

        If Not dr Is Nothing Then
            Dim usr As New users(SM.currentuser, SM.actualuser, dr("user_pk"), CDC)
            If usr.disconnect Then usr.disconnect = False : usr.Save()
            SM.currentuser = dr("user_pk")
            SM.actualuser = dr("user_pk")
            Dim u As New userloginlogs(SM.currentuser, SM.actualuser, CDC)
            u.attemptresult = True
            u.creator_fk = SM.currentuser
            u.editor_fk = SM.currentuser
            u.ipaddress = Request.UserHostAddress
            u.useragentstring = Request.UserAgent
            u.rowstatus = 0
            u.username = SM.UserName(SM.currentuser)
            u.probableuser_fk = SM.currentuser
            u.userpassw = "Auto"
            u.Save()
            loginOK = True
        End If
        dr = Nothing
        CO = Nothing
        If loginOK Then
            Local.Bounce(SM, Response, "~/core/default.aspx", "loginok")
        End If

    End Sub

    Protected Sub clickLogin(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim user_pk As Integer = 0
        If Not Integer.TryParse(e.CommandArgument, user_pk) Then Exit Sub
        doLogin(user_pk)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If dgr.Items.Count = 0 Then
            Dim CO As New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "[rsp_lstuserstodebug]"
            CO.Parameters.AddWithValue("@user_fk", SM.actualuser)
            dgr.DataSource = CDC.ReadDataTable(CO)
            dgr.DataTextField = "value"
            dgr.DataValueField = "pk"
            dgr.DataBind()
        End If
    End Sub
    Public ReadOnly Property HasDebuggingAccess() As Boolean
        Get
            Return Security.GroupMember(SM.actualuser, SystemGroups.Admin, CDC) And Security.GroupMember(SM.actualuser, SystemGroups.AccessToBackendSystems, CDC)
        End Get
    End Property

    Protected Sub btndgr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndgr.Click
        doLogin(dgr.SelectedValue)
    End Sub

End Class
