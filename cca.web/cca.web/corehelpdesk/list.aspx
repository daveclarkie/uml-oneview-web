﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="list.aspx.vb" Inherits="corehelpdesk_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<h3>Helpdesk</h3> 
<br />

<asp:Repeater ID="rptHelpdeskQueues" runat="server" >
    <HeaderTemplate><table>
        <tr>
            <td style="width:75px;">
                <h4>ID</h4>
            </td>
            <td style="width:500px;">
                <h4>Title</h4>
            </td>
            <td style="width:200px;">
                <h4>Template</h4>
            </td>
            <td style="width:200px;">
                <h4>Assigned To</h4>
            </td>
            <td style="width:200px;">
                <h4>Created</h4>
            </td>
        </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <a href='ticket.aspx?workorderid=<%# Container.DataItem("workorderid")%>'><%# Container.DataItem("workorderid")%></a>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("subject")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%#Left(Container.DataItem("template"), 15)%>...
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("technician")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("created")%>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyData" Text="No Tickets Available" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>





</asp:Content>

