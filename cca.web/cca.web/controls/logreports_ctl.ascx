<%@ Control Language="VB" AutoEventWireup="false" CodeFile="logreports_ctl.ascx.vb" Inherits="logreports_ctl" %>
<h5>logreports</h5>
<ul class='formcontrol'>
<li runat="server" id="blkreporturl">
<span class='label'>reporturl</span>
<asp:TextBox EnableViewState="false" ID="reporturl" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkreporttype">
<span class='label'>reporttype</span>
<asp:TextBox EnableViewState="false" ID="reporttype" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktarget">
<span class='label'>target</span>
<asp:TextBox EnableViewState="false" ID="target" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkperiod">
<span class='label'>period</span>
<asp:TextBox EnableViewState="false" ID="period" runat="server" cssClass="input_num"  />
</li>
</ul>

