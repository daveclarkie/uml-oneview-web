
Partial Class functions_grpswt
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand
    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Try

            Dim rf As Object = Request.QueryString
            Dim ug As New usergroups(SM.currentuser, SM.actualuser, CDC)
            If rf("pk") <> "" Then
                ug.Read(Integer.Parse(rf("pk")))
                Select Case ug.rowstatus
                    Case 0 : ug.Disable()
                    Case 1 : ug.Enable()
                End Select
                Response.Write("OK")
            Else
                Response.Write("Fail:No item specified")
            End If

        Catch Ex As Exception
            Response.Clear()
            Response.Write("Fail:" & Ex.Message)
        End Try

        Try
            Response.End()
        Catch ex As Exception
        End Try


    End Sub
End Class
