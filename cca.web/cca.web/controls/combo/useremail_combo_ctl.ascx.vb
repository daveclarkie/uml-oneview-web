Partial Class useremail_combo_ctl
   Inherits DaveControl
   Public PSM As MySM

   Public WithEvents ouseremail As Objects.useremails
   Public WithEvents oemail As Objects.emails
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=-1
   Dim _Status as Integer=0
   Dim _STCurrentPk as Integer=-1
   Dim _STStatus as Integer=0

   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       ControlInit
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub

   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property

   Public ReadOnly Property STCurrentPk() as Integer
        Get
            Return _STCurrentPk
        End Get
   End Property

   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property

  Public Sub ControlInit()
CO=new SqlClient.SqlCommand("rkg_contactlocations_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.contactlocation_fk.DataSource=CDC.ReadDataTable(CO)
Me.contactlocation_fk.DataTextField = "value"
Me.contactlocation_fk.DataValueField = "pk"
Try
Me.contactlocation_fk.DataBind
Catch Ex as Exception
Me.contactlocation_fk.SelectedValue=-1
Me.contactlocation_fk.DataBind
End Try
  End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
End Sub
Public Sub SetObjects(ByVal CurrentUser as Integer,ByVal ActualUser as Integer,Optional ByVal pk as Integer=-1)
    If ouseremail is Nothing then
        ouseremail = new Objects.useremails(currentuser,actualuser,pk,CDC)
        If ouseremail.email_fk>0 then
            oemail = new Objects.emails(currentuser,actualuser,ouseremail.email_fk,CDC)
        Else
            oemail = new Objects.emails(currentuser,actualuser,-1,CDC)
        End If
    End If
    If oemail is Nothing then
        If ouseremail.email_fk>0 then
            oemail = new Objects.emails(currentuser,actualuser,ouseremail.email_fk,CDC)
        Else
            oemail = new Objects.emails(currentuser,actualuser,-1,CDC)
        End If
    End If
End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1)
    SetObjects(currentuser,actualuser,pk)
Me.email.Text=oemail.email
SM.subId=ouseremail.email_fk
Try
Me.contactlocation_fk.SelectedValue=ouseremail.contactlocation_fk
Catch excontactlocation_fk as Exception
Me.contactlocation_fk.SelectedValue=-1
End Try
_CurrentPk=ouseremail.useremail_pk
_STCurrentPk=oemail.email_pk
Status=ouseremail.rowstatus OR oemail.rowstatus
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1) as boolean
    SetObjects(currentuser,actualuser,pk)
oemail.email=Me.email.Text
Dim STResult as boolean=False
Try
STResult=oemail.Save()
if not STResult then throw oemail.LastError
_STCurrentPk=oemail.email_pk
SM.subId=oemail.email_pk
Catch Ex as Exception
STResult=False
LastError=Ex
SM.subId=-1
End Try
Try
ouseremail.user_fk=SM.targetuser
ouseremail.email_fk=SM.subId
ouseremail.contactlocation_fk=Me.contactlocation_fk.SelectedValue
Catch MainEx as Exception
LastError=MainEx
End Try
Dim result as boolean=false
Try
Result=ouseremail.Save()
if not Result then throw ouseremail.LastError
_CurrentPk=ouseremail.useremail_pk
Catch Ex as Exception
Result=False
LastError=Ex
End Try
SM.linkId=_CurrentPk
Status=ouseremail.rowstatus OR oemail.rowstatus
Return result AND STResult
End Function
Public Function Enable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouseremail is Nothing then
ouseremail = new Objects.useremails(currentuser,actualuser,pk,CDC)
End If
ouseremail.rowstatus=0
Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouseremail is Nothing then
ouseremail = new Objects.useremails(currentuser,actualuser,pk,CDC)
End If
ouseremail.rowstatus=1
Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouseremail is Nothing then
ouseremail = new Objects.useremails(currentuser,actualuser,pk,CDC)
End If
ouseremail.rowstatus=2
Return Save(currentuser,actualuser,pk)
End Function
Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
Me.email.Enabled=IIF(WL>=0,True,False)
Me.blkemail.Visible=IIF(RL>=0,True,False)
Me.contactlocation_fk.Enabled=IIF(WL>=0,True,False)
Me.blkcontactlocation_fk.Visible=IIF(RL>=0,True,False)
End Sub
End Class
