Partial Class agreementfuels_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreementfuels As objects.agreementfuels
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
                agreement_fk.SelectedValue = SM.targetAgreement
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rsp_lkp_fuels_agreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.Add(New SqlClient.SqlParameter("@agreement", SM.targetAgreement))
        If Me.fuel_fk.Items.Count = 0 Then
            Me.fuel_fk.DataSource = CDC.ReadDataTable(CO)
            Me.fuel_fk.DataTextField = "value"
            Me.fuel_fk.DataValueField = "pk"
            Try
                Me.fuel_fk.DataBind()
            Catch Ex As Exception
                Me.fuel_fk.SelectedValue = -1
                Me.fuel_fk.DataBind()
            End Try
        End If
        Me.eligibility.Text = "0.00"
        Me.relief.Text = "0.00"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oagreementfuels Is Nothing Then
            oagreementfuels = New Objects.agreementfuels(currentuser, actualuser, pk, CDC)
        End If
        Me.agreement_fk.SelectedValue = oagreementfuels.agreement_fk
        Me.fuel_fk.SelectedValue = oagreementfuels.fuel_fk
        Me.eligibility.Text = oagreementfuels.eligibility
        Me.eligibility.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.eligibility.Attributes.Add("onBlur", "clearNotice()")
        Me.validfrom.Text = oagreementfuels.validfrom.ToString("dd MMM yyyy")
        If Me.validfrom.Text = "01 Jan 0001" Then Me.validfrom.Text = ""
        Me.validfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.validfrom.Attributes.Add("onBlur", "clearNotice()")
        Me.validto.Text = oagreementfuels.validto.ToString("dd MMM yyyy")
        If Me.validto.Text = "01 Jan 0001" Then Me.validto.Text = ""
        Me.validto.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.validto.Attributes.Add("onBlur", "clearNotice()")
        Me.relief.Text = oagreementfuels.relief
        Me.relief.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.relief.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = oagreementfuels.agreementfuel_pk
        Status = oagreementfuels.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementfuels Is Nothing Then
            oagreementfuels = New Objects.agreementfuels(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreementfuels.agreement_fk = Me.agreement_fk.SelectedValue
            oagreementfuels.fuel_fk = Me.fuel_fk.SelectedValue
            oagreementfuels.eligibility = Me.eligibility.Text
            oagreementfuels.validfrom = CommonFN.CheckEmptyDate(Me.validfrom.Text)
            oagreementfuels.validto = CommonFN.CheckEmptyDate(Me.validto.Text)
            oagreementfuels.relief = Me.relief.Text
            result = oagreementfuels.Save()
            If Not result Then Throw oagreementfuels.LastError
            _CurrentPk = oagreementfuels.agreementfuel_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreementfuels.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementfuels Is Nothing Then
            oagreementfuels = New Objects.agreementfuels(currentuser, actualuser, pk, CDC)
        End If
        oagreementfuels.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementfuels Is Nothing Then
            oagreementfuels = New Objects.agreementfuels(currentuser, actualuser, pk, CDC)
        End If
        oagreementfuels.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementfuels Is Nothing Then
            oagreementfuels = New Objects.agreementfuels(currentuser, pk, CDC)
        End If
        oagreementfuels.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.agreement_fk.Enabled = IIf(WL >= 10, True, False)
        Me.blkagreement_fk.Visible = IIf(RL >= 10, True, False)
        Me.fuel_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkfuel_fk.Visible = IIF(RL >= 0, True, False)
        Me.eligibility.Enabled = IIF(WL >= 0, True, False)
        Me.blkeligibility.Visible = IIF(RL >= 0, True, False)
        Me.validfrom.Enabled = IIF(WL >= 0, True, False)
        Me.blkvalidfrom.Visible = IIF(RL >= 0, True, False)
        Me.validto.Enabled = IIF(WL >= 0, True, False)
        Me.blkvalidto.Visible = IIF(RL >= 0, True, False)
        Me.relief.Enabled = IIF(WL >= 0, True, False)
        Me.blkrelief.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

