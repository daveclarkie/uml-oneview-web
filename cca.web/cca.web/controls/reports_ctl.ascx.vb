Partial Class reports_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oreports As Objects.reports
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_reporttypes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.reporttype_fk.Items.Count=0 Then
        Me.reporttype_fk.DataSource=CDC.ReadDataTable(CO)
        Me.reporttype_fk.DataTextField = "value"
        Me.reporttype_fk.DataValueField = "pk"
        Try
            Me.reporttype_fk.DataBind
        Catch Ex as Exception
            Me.reporttype_fk.SelectedValue=-1
            Me.reporttype_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oreports is Nothing then
oreports = new Objects.reports(currentuser,actualuser,pk,CDC)
End If
Me.reportname.Text=oreports.reportname
Me.reporttype_fk.SelectedValue=oreports.reporttype_fk
Me.reportserverurl.Text=oreports.reportserverurl
Me.reportpath.Text=oreports.reportpath
_CurrentPk=oreports.report_pk
Status=oreports.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oreports is Nothing then
oreports = new Objects.reports(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oreports.reportname=Me.reportname.Text
oreports.reporttype_fk=Me.reporttype_fk.SelectedValue
oreports.reportserverurl=Me.reportserverurl.Text
oreports.reportpath=Me.reportpath.Text
result=oreports.Save()
if not result then throw oreports.LastError
_CurrentPk=oreports.report_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oreports.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreports is Nothing then
   oreports = new Objects.reports(currentuser,actualuser,pk,CDC)
  End If
  oreports.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreports is Nothing then
   oreports = new Objects.reports(currentuser,actualuser,pk,CDC)
  End If
  oreports.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreports is Nothing then
   oreports = new Objects.reports(currentuser,pk,CDC)
  End If
  oreports.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.reportname.Enabled=IIF(WL>=0,True,False)
  Me.blkreportname.Visible=IIF(RL>=0,True,False)
  Me.reporttype_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkreporttype_fk.Visible=IIF(RL>=0,True,False)
  Me.reportserverurl.Enabled=IIF(WL>=0,True,False)
  Me.blkreportserverurl.Visible=IIF(RL>=0,True,False)
  Me.reportpath.Enabled=IIF(WL>=0,True,False)
  Me.blkreportpath.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

