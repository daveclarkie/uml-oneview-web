<%@ Control Language="VB" AutoEventWireup="false" CodeFile="meterpoints_ctl.ascx.vb" Inherits="meterpoints_ctl" %>
<h5>meterpoints</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmeter_fk">
<span class='label'>meter_fk</span>
<asp:DropDownList ID="meter_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktablename">
<span class='label'>tablename</span>
<asp:TextBox EnableViewState="false" ID="tablename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmeter">
<span class='label'>meter</span>
<asp:TextBox EnableViewState="false" ID="meter" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkchannel">
<span class='label'>channel</span>
<asp:TextBox EnableViewState="false" ID="channel" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkfuel_fk">
<span class='label'>fuel_fk</span>
<asp:DropDownList ID="fuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkunit_fk">
<span class='label'>unit_fk</span>
<asp:DropDownList ID="unit_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkumlsite_fk">
<span class='label'>umlsite_fk</span>
<asp:DropDownList ID="umlsite_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksitename">
<span class='label'>sitename</span>
<asp:TextBox EnableViewState="false" ID="sitename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkdisplayname">
<span class='label'>displayname</span>
<asp:TextBox EnableViewState="false" ID="displayname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

