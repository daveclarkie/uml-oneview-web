
Partial Class controls_plugins_seekpersongroup
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public ReadOnly Property ControlName() As String
        Get
            Return Me.ID.Replace(Me.IdSeparator, "_")
        End Get
    End Property

    Dim _TargetFunctionName As String
    Public Property TargetFunctionName() As String
        Get
            Return _TargetFunctionName
        End Get
        Set(ByVal value As String)
            _TargetFunctionName = value
        End Set
    End Property

    Dim _TargetValueControlName As String
    Public Property TargetValueControlName() As String
        Get
            Return _TargetValueControlName
        End Get
        Set(ByVal value As String)
            _TargetValueControlName = value
        End Set
    End Property

    Dim _TargetDisplayControlName As String
    Public Property TargetDisplayControlName() As String
        Get
            Return _TargetDisplayControlName
        End Get
        Set(ByVal value As String)
            _TargetDisplayControlName = value
        End Set
    End Property

    Dim _Label As String
    Public Property Label() As String
        Get
            Return _Label
        End Get
        Set(ByVal value As String)
            _Label = value
        End Set
    End Property

    Dim _FilterGroup As String
    Public Property FilterGroup() As String
        Get
            Return _FilterGroup
        End Get
        Set(ByVal value As String)
            _FilterGroup = value
        End Set
    End Property


End Class
