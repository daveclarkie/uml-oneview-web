<%@ Page Language="VB" MasterPageFile="~/design/masters/external.master" AutoEventWireup="false" CodeFile="logout.aspx.vb" Inherits="logout" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
   <h3>Log Out Complete</h3> 
    You have been logged out of the system. As an extra precaution, in the interests
    of security, we suggest you also close your browser.
</asp:Content>

