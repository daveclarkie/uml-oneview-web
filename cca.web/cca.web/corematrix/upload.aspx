<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="upload.aspx.vb" Inherits="corematrix_upload" title="import data" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        <h3>Import Data</h3> 
        
            <asp:Panel ID="Panel1" runat="server">
                
                <ul class='formcontrol'>
                    <li runat="server" id="blkfuel_fk">
                        <span class='label'>Matrix Type</span>
                        <asp:DropDownList ID="matrixtype_fk" runat="server" cssClass="input_ddl" />
                    </li>
                    <li runat="server" id="blkfile">
                        <span class='label'>File</span>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </li>
                    <li></li>
                    <li runat="server" id="blkupload">
                        <span class='label' style="visibility:hidden;">.</span>
                        <asp:linkButton ID="btnUpload" runat="server" Text="<h6>Upload</h6>" OnClick="btnUpload_Click" Width="250px" />
                    </li>
                    <li runat="server" id="blkmessage">
                        <span class='label' style="visibility:hidden;">.</span>
                        <asp:Label ID="lblmessage" runat="server" Text="" />
                    </li>
                </ul>

                
            </asp:Panel>

            <asp:Panel ID="Panel2" runat="server" Visible="false" >
                <asp:Label ID="Label5" runat="server" Text="File Name"/>
                <asp:Label ID="lblFileName" runat="server" Text=""/>
                <br />

                <asp:Repeater ID="rptStageOne" runat="server">
                        <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <h4>0</h4>
                                </td>
                                <td>
                                    <h4>1</h4>
                                </td>
                                <td>
                                    <h4>2</h4>
                                </td>
                                <td>
                                    <h4>3</h4>
                                </td>
                                <td>
                                    <h4>4</h4>
                                </td>
                            </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Container.DataItem(0)%>
                                    </td>
                                    <td>
                                        <%# Container.DataItem(1)%>
                                    </td>
                                    <td>
                                        <%# Container.DataItem(2)%>
                                    </td>
                                    <td>
                                        <%# Container.DataItem(3)%>
                                    </td>
                                    <td>
                                        <%# Container.DataItem(4)%>
                                    </td>
                                </tr>
                        </ItemTemplate> 
                         
                        <FooterTemplate>
                                <tr style="height:5px;">
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                <br />
                <asp:Button ID="btnSave" runat="server" Text="Save" 
                      OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                      OnClick="btnCancel_Click" />        
             </asp:Panel>
      
    </div>      
</div>
</asp:Content>

