<%@ Page Language="VB" MasterPageFile="~/design/masters/external.master" AutoEventWireup="false" CodeFile="disconnect.aspx.vb" Inherits="disconnect" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Previous Session Expired</h3>
    You have been logged out of the system due to your previous session expiring. Click <a href="/login.aspx">here</a> to log in.
</asp:Content>

