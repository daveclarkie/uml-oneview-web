<%@ Control Language="VB" AutoEventWireup="false" CodeFile="persons_ctl.ascx.vb" Inherits="persons_ctl" %>
<h5>persons</h5>
<ul class='formcontrol'>
<li runat="server" id="blktitle_fk">
<span class='label'>Title</span>
<asp:DropDownList ID="title_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkforename">
<span class='label'>Forename</span>
<asp:TextBox EnableViewState="false" ID="forename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmiddlename">
<span class='label'>middle name</span>
<asp:TextBox EnableViewState="false" ID="middlename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blksurname">
<span class='label'>surname</span>
<asp:TextBox EnableViewState="false" ID="surname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkjobtitle">
<span class='label'>job title</span>
<asp:TextBox EnableViewState="false" ID="jobtitle" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation</span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmanageruser_fk">
<span class='label'>Manager</span>
<asp:DropDownList ID="manageruser_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

