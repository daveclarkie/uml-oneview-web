﻿Imports System.Collections.Generic
Imports System.IO
Imports System.Net


Partial Class corehelpdesk_ticket
    Inherits DavePage
    Public PSM As MySM
    Dim CO As SqlCommand

    Dim validworkorderid As Boolean = False
    Dim workorderid As Integer = -1



    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        validworkorderid = Integer.TryParse(Request.QueryString("workorderid"), workorderid)

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        CO = New SqlClient.SqlCommand("rsp_servicedesk_ticket_conversations")
        CO.Parameters.AddWithValue("@workorderid", workorderid)
        CO.CommandType = CommandType.StoredProcedure
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptHelpdeskConversations.DataSource = dt
        rptHelpdeskConversations.DataBind()
        CO.Dispose()
        rptHelpdeskConversations.Visible = True


        CO = New SqlClient.SqlCommand("rsp_servicedesk_ticket_attachments")
        CO.Parameters.AddWithValue("@workorderid", workorderid)
        CO.CommandType = CommandType.StoredProcedure
        dt = CDC.ReadDataTable(CO, 0)
        rptHelpdeskAttachments.DataSource = dt
        rptHelpdeskAttachments.DataBind()
        CO.Dispose()
        rptHelpdeskAttachments.Visible = True

        CO = New SqlClient.SqlCommand("rsp_servicedesk_ticket_info")
        CO.Parameters.AddWithValue("@workorderid", workorderid)
        CO.CommandType = CommandType.StoredProcedure
        dt = CDC.ReadDataTable(CO, 0)
        rpthelpdeskinfo.DataSource = dt
        rpthelpdeskinfo.DataBind()
        CO.Dispose()
        rpthelpdeskinfo.Visible = True




    End Sub
    '
    Protected Sub rptHelpdeskAttachments_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If rptHelpdeskAttachments.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyDataAttachments"), Label)
                lblFooter.Visible = True
            End If
        End If
    End Sub
    Protected Sub rptHelpdeskConversations_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If rptHelpdeskConversations.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyDataConversations"), Label)
                lblFooter.Visible = True
            End If
        End If
    End Sub

    Public Sub rptHelpdeskAttachments_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptHelpdeskAttachments.ItemCommand

        Dim arg As String() = New String(1) {}
        arg = e.CommandArgument.ToString().Split(";"c)


        If e.CommandName = "download" Then

            Dim filename As String = arg(0)
            Dim path As String = arg(1)
            Dim bts As Byte() = System.IO.File.ReadAllBytes(path)
            Response.Clear()

            Dim ext As String = System.IO.Path.GetExtension(filename)
            Dim vContentType As String = CustomContentType(ext)
            Try
                Response.ClearHeaders()
                Response.AddHeader("Content-Type", vContentType)
                Response.AddHeader("Content-Length", bts.Length.ToString())
                Response.AddHeader("Content-Disposition", "attachment; filename=" & Replace(filename, " ", "_"))
                Response.BinaryWrite(bts)
                Response.Flush()
            Catch ex As Exception

            End Try
        End If
    End Sub

    'Public Shared Function GetFileContentType(ByVal fileextension As String) As String
    '    Const DEFAULT_CONTENT_TYPE As String = "application/unknown"
    '    Dim regkey, fileextkey As RegistryKey
    '    Dim filecontenttype As String

    '    Try
    '        regkey = Registry.ClassesRoot
    '        fileextkey = regkey.OpenSubKey(fileextension)
    '        filecontenttype = fileextkey.GetValue("Content Type", DEFAULT_CONTENT_TYPE).ToString()
    '        fileextkey = Nothing
    '        regkey = Nothing
    '    Catch
    '        filecontenttype = DEFAULT_CONTENT_TYPE
    '    End Try

    '    Return filecontenttype
    'End Function
    Function CustomContentType(ByVal FileExtension As String) As String
        Dim d As New Dictionary(Of String, String)
        'Images'
        d.Add(".bmp", "image/bmp")
        d.Add(".gif", "image/gif")
        d.Add(".jpeg", "image/jpeg")
        d.Add(".jpg", "image/jpeg")
        d.Add(".png", "image/png")
        d.Add(".tif", "image/tiff")
        d.Add(".tiff", "image/tiff")
        'Documents'
        d.Add(".doc", "application/msword")
        d.Add(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
        d.Add(".pdf", "application/pdf")
        'Slideshows'
        d.Add(".ppt", "application/vnd.ms-powerpoint")
        d.Add(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation")
        'Data'
        d.Add(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        d.Add(".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12")
        d.Add(".xls", "application/vnd.ms-excel")
        d.Add(".csv", "text/csv")
        d.Add(".xml", "text/xml")
        d.Add(".txt", "text/plain")
        'Compressed Folders'
        d.Add(".zip", "application/zip")
        'Audio'
        d.Add(".ogg", "application/ogg")
        d.Add(".mp3", "audio/mpeg")
        d.Add(".wma", "audio/x-ms-wma")
        d.Add(".wav", "audio/x-wav")
        'Video'
        d.Add(".wmv", "audio/x-ms-wmv")
        d.Add(".swf", "application/x-shockwave-flash")
        d.Add(".avi", "video/avi")
        d.Add(".mp4", "video/mp4")
        d.Add(".mpeg", "video/mpeg")
        d.Add(".mpg", "video/mpeg")
        d.Add(".qt", "video/quicktime")
        'Other - Custom'
        d.Add(".msg", "application/vnd.ms-outlook")
        Return d(FileExtension)
    End Function
End Class
