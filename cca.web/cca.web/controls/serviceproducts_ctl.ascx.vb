Partial Class serviceproducts_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oserviceproducts As Objects.serviceproducts
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_departments_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.department_fk.Items.Count=0 Then
        Me.department_fk.DataSource=CDC.ReadDataTable(CO)
        Me.department_fk.DataTextField = "value"
        Me.department_fk.DataValueField = "pk"
        Try
            Me.department_fk.DataBind
        Catch Ex as Exception
            Me.department_fk.SelectedValue=-1
            Me.department_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oserviceproducts is Nothing then
oserviceproducts = new Objects.serviceproducts(currentuser,actualuser,pk,CDC)
End If
Me.productname.Text=oserviceproducts.productname
Me.productdescription.Text=oserviceproducts.productdescription
Me.department_fk.SelectedValue=oserviceproducts.department_fk
_CurrentPk=oserviceproducts.serviceproduct_pk
Status=oserviceproducts.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oserviceproducts is Nothing then
oserviceproducts = new Objects.serviceproducts(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oserviceproducts.productname=Me.productname.Text
oserviceproducts.productdescription=Me.productdescription.Text
oserviceproducts.department_fk=Me.department_fk.SelectedValue
result=oserviceproducts.Save()
if not result then throw oserviceproducts.LastError
_CurrentPk=oserviceproducts.serviceproduct_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oserviceproducts.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceproducts is Nothing then
   oserviceproducts = new Objects.serviceproducts(currentuser,actualuser,pk,CDC)
  End If
  oserviceproducts.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceproducts is Nothing then
   oserviceproducts = new Objects.serviceproducts(currentuser,actualuser,pk,CDC)
  End If
  oserviceproducts.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceproducts is Nothing then
   oserviceproducts = new Objects.serviceproducts(currentuser,pk,CDC)
  End If
  oserviceproducts.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.productname.Enabled=IIF(WL>=0,True,False)
  Me.blkproductname.Visible=IIF(RL>=0,True,False)
  Me.productdescription.Enabled=IIF(WL>=0,True,False)
  Me.blkproductdescription.Visible=IIF(RL>=0,True,False)
  Me.department_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkdepartment_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

