<%@ Control Language="VB" AutoEventWireup="false" CodeFile="dataconsumptions_ctl.ascx.vb" Inherits="dataconsumptions_ctl" %>
<h5>Consumption</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmeterpoint_fk">
<span class='label'>Meter</span>
<asp:TextBox EnableViewState="false" ID="meterpoint_fk" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmonth_fk">
<span class='label'>Month</span>
<asp:DropDownList ID="month_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkyear_fk">
<span class='label'>Year</span>
<asp:DropDownList ID="year_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktotalconsumption">
<span class='label'>Total Consumption<span class='pop'>This is the total consumption for the meter during the period</span></span>
<asp:TextBox EnableViewState="false" ID="totalconsumption" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkeligibleconsumption">
<span class='label'>Eligible Consumption<span class='pop'>This is the eligible consumption for the meter during the period</span></span>
<asp:TextBox EnableViewState="false" ID="eligibleconsumption" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkdataimportmethod_fk">
<span class='label'>Import Method</span>
<asp:DropDownList ID="dataimportmethod_fk" runat="server" cssClass="input_ddl" />
</li>


<li runat="server" id="Li1">
<span class='label'>The data was imported via </span>
<asp:label ID="importedmethod" runat="server" cssClass="input_str" />
</li>

<li runat="server" id="Li2">
<span class='label'>Created</span>
<asp:label ID="created" runat="server" cssClass="input_str" />
</li>

<li runat="server" id="Li3">
<span class='label'>Last Modified</span>
<asp:label ID="modified" runat="server" cssClass="input_str" />
</li>

</ul>

