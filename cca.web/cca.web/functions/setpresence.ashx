<%@ WebHandler Language="VB" Class="setpresence" %>
Public Class setpresence
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim msg As String = ""
        
        Try
            Dim actlog As New activitylogs(SM.currentuser, SM.actualuser, CDC)
            actlog.ipaddress = Request.UserHostAddress
            actlog.useragent = Request.UserAgent
            actlog.user_fk = SM.currentuser
            actlog.Save()
            actlog = Nothing
            Response.Write("OK|1")
        Catch LE As Exception
            Response.Write("FAIL|" & LE.Message)
        End Try
    End Sub
 
End Class

