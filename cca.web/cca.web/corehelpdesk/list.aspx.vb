﻿Partial Class corehelpdesk_list
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Dim validsite As Boolean = False
    Dim hdsite As String = ""



    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        hdsite = Request.QueryString("site")

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        CO = New SqlClient.SqlCommand("rsp_servicedesk_ticketsbysite")
        CO.Parameters.AddWithValue("@sitename", hdsite)
        CO.CommandType = CommandType.StoredProcedure
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptHelpdeskQueues.DataSource = dt
        rptHelpdeskQueues.DataBind()
        CO.Dispose()
        rptHelpdeskQueues.Visible = True

    End Sub



End Class
