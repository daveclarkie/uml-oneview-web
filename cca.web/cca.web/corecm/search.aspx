<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="corecm_search" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Search</h3>
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <ul id="" class='formcontrol'>
                <li><span class="label">Search Type</span><asp:Dropdownlist ID="ddlSearchType" runat="server" CssClass="input_ddl" /></li>
                <li><span class="label">Criteria</span><asp:TextBox ID="searchstring" runat="server" CssClass="input_str" /></li>
                <li style="visibility:hidden;"><span class="label">Include Prospects?</span><asp:Dropdownlist ID="ddlprospects" runat="server" CssClass="input_ddl" /></li>
                <li><span class="label">&nbsp;</span><asp:LinkButton ID="btnSearch" runat="server" Text="Locate" CausesValidation="False" /><asp:LinkButton ID="btnCustomerSearch" runat="server" Text="" CausesValidation="False" /></li>
            </ul>
            <asp:LinkButton ID="btnCreate" runat="server" Text="" CausesValidation="False" />
        </asp:Panel>
                  <h3>Results - Contract Manager</h3>
        <asp:Repeater ID="rptContractManagerResults" runat="server">
            <HeaderTemplate>
                <table >
                    <tr>
                        <td>
                                
                        </td>
                        <td>
                            <h4>Organisation</h4>
                        </td>
                        <td>
                            <h4>Site Name</h4>
                        </td>
                        <td>
                            <h4>Meter</h4>
                        </td>
                    </tr> 
            </HeaderTemplate> 
            <ItemTemplate>
                    <tr>
                        <td style="width:70px;">
                            <%# returnselect(Container.DataItem("rowstatus"), Container.DataItem("url"))%>
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("organisation")%>
                        </td>
                        <td style="width:300px;">
                            <%# Container.DataItem("sitename")%>
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("meter")%>
                        </td>
                    </tr>
            </ItemTemplate> 
            <FooterTemplate>
                
                </table>
            </FooterTemplate>
            </asp:Repeater>
            <asp:Repeater ID="rptContractManagerGroupSearch" runat="server">
            <HeaderTemplate>
                <table >
                    <tr>
                        <td>
                                
                        </td>
                        <td>
                            <h4>ID</h4>
                        </td>
                        <td>
                            <h4>Organisation Name</h4>
                        </td>
                        <td>
                            <h4>EP Sites</h4>
                        </td>
                        <td>
                            <h4>EP OOC</h4>
                        </td>
                        <td>
                            <h4>NG Sites</h4>
                        </td>
                        <td>
                            <h4>NG OOC</h4>
                        </td>
                    </tr> 
            </HeaderTemplate> 
            <ItemTemplate>
                    <tr>
                        <td style="width:70px;">
                            
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("custid")%>
                        </td>
                        <td style="width:300px;">
                            <%# Container.DataItem("customername")%>
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("EP")%>
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("EP_OOC")%>
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("NG")%>
                        </td>
                        <td style="width:70px;">
                            <%# Container.DataItem("NG_OOC")%>
                        </td>
                    </tr>
            </ItemTemplate> 
            <FooterTemplate>
                
                </table>
            </FooterTemplate>
            </asp:Repeater>
            <table>
                <tr style="height:10px;"></tr>
                <tr>
                    <td style="width:150px;">
                        <asp:Label ID="lblSearchPage" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchPrev" runat="server" text="" OnClick="lnkPrev_Click" />
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchNext" runat="server" text="" OnClick="lnkNext_Click" />
                    </td>
                </tr>
            </table>

            <script language="javascript" type="text/javascript">
                var srchType = document.getElementById("<%=ddlSearchType.ClientID %>");
                var srchBtn = document.getElementById("<%=btnSearch.ClientID %>");
                var srchString = document.getElementById("<%=searchstring.ClientID %>");

                setEvent(srchType, "change", doSearch);
                setEvent(srchString, "keyup", doSearch);

                //setOnLoad(killBtn);
            </script>

</asp:Content>

