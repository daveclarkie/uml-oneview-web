Partial Class departments_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents odepartments As Objects.departments
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.owner_user_fk.Items.Count=0 Then
        Me.owner_user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.owner_user_fk.DataTextField = "value"
        Me.owner_user_fk.DataValueField = "pk"
        Try
            Me.owner_user_fk.DataBind
        Catch Ex as Exception
            Me.owner_user_fk.SelectedValue=-1
            Me.owner_user_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If odepartments is Nothing then
odepartments = new Objects.departments(currentuser,actualuser,pk,CDC)
End If
Me.departmentname.Text=odepartments.departmentname
Me.owner_user_fk.SelectedValue=odepartments.owner_user_fk
_CurrentPk=odepartments.department_pk
Status=odepartments.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If odepartments is Nothing then
odepartments = new Objects.departments(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
odepartments.departmentname=Me.departmentname.Text
odepartments.owner_user_fk=Me.owner_user_fk.SelectedValue
result=odepartments.Save()
if not result then throw odepartments.LastError
_CurrentPk=odepartments.department_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=odepartments.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odepartments is Nothing then
   odepartments = new Objects.departments(currentuser,actualuser,pk,CDC)
  End If
  odepartments.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odepartments is Nothing then
   odepartments = new Objects.departments(currentuser,actualuser,pk,CDC)
  End If
  odepartments.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odepartments is Nothing then
   odepartments = new Objects.departments(currentuser,pk,CDC)
  End If
  odepartments.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.departmentname.Enabled=IIF(WL>=0,True,False)
  Me.blkdepartmentname.Visible=IIF(RL>=0,True,False)
  Me.owner_user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkowner_user_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

