<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreementtargets_ctl.ascx.vb" Inherits="agreementtargets_ctl" %>
<h5>Agreement Target</h5>
<ul class='formcontrol'>
<li runat="server" id="blkagreement_fk">
<span class='label'>Agreement</span>
<asp:DropDownList ID="agreement_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktargetdescription">
<span class='label'>Description</span>
<asp:TextBox EnableViewState="false" ID="targetdescription" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktargetstart">
<span class='label'>Target Start</span>
<asp:TextBox EnableViewState="false" ID="targetstart" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blktargetend">
<span class='label'>Target End</span>
<asp:TextBox EnableViewState="false" ID="targetend" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blktarget">
<span class='label'>Target</span>
<asp:TextBox EnableViewState="false" ID="target" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkeacarbonsurplus">
<span class='label'>Carbon Surplus<span class='pop'>This is the Carbon Surplus from the previous milestone, reported by the EA.</span></span>
<asp:TextBox EnableViewState="false" ID="eacarbonsurplus" runat="server" cssClass="input_num"  />
</li>
</ul>

