﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreements.ascx.vb" Inherits="controls_nav_agreements" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

 <%If LoadList() Then%>
    <h3>Selected Agreement</h3>
       <ul class="navigationmenu">
            <table>
                <tr>
                    <td style="width:80px;"><b>Version</b></td>
                    <td><asp:Label ID="lblAgreementVersion" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width:80px;"><b>Name</b></td>
                    <td><asp:Label ID="lblAgreementName" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width:80px;"><b>Federation</b></td>
                    <td><asp:Label ID="lblFederation" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><b>Agreement</b></td>
                    <td><asp:Label ID="lblAgreementNumber" runat="server"></asp:Label></td>
                </tr>
                
                <tr>
                    <td><b>Type</b></td>
                    <td><asp:Label ID="lblAgreementType" runat="server"></asp:Label></td>
                </tr>
                
                <tr>
                    <td><b>Measure</b></td>
                    <td><asp:Label ID="lblAgreementMeasure" runat="server"></asp:Label></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="btnExportAgreementData" runat="server" class="button_orangesmall" Text="Export One View Data" Width="180px"></asp:LinkButton>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="btnExportAgreementAuditData" runat="server" class="button_orangesmall" Text="Export Audit Data" Width="180px"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <asp:LinkButton ID="lnkCustomerFolder" runat="server" Visible="false"><h6>Open Customer Folder</h6></asp:LinkButton>
                        <asp:HyperLink Text='spoon'  id="DeploymentLocation" runat="server" Target="_blank" Visible="false" ></asp:HyperLink>
                    </td>
                </tr>

            </table>
       </ul>
       <%If SM.targetDataEntry > 0 Then%>
       <h3>Selected Meter</h3>
       <ul class="navigationmenu">
            <table>
                <tr>
                    <td style="width:80px;"><b>Fuel</b></td>
                    <td><asp:Label ID="lblFuelName" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width:80px;"><b>Meter</b></td>
                    <td><asp:Label ID="lblMeter" runat="server"></asp:Label></td>
                </tr>
            </table>
       </ul>
       <%End If%>
       <div class="navigationmenu" id="pnlOtherVersions" runat="server">
       <h3>Other Versions</h3>
            <asp:Repeater ID="rptVersions" runat="server">
            <HeaderTemplate>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <h4>Number</h4>
                    </td>
                </tr>        
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <a href="agreements.aspx?pk=<%# Container.DataItem("agreement_pk")%>&pg=ag"><h6> <%# Container.DataItem("version")%></h6></a>                        
                    </td>
                    <td>
                        <%# Container.DataItem("agreementnumber")%> 
                    </td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
       
                <rsweb:ReportViewer Visible="false" ID="myReportViewer" AsyncRendering="true" ShowPrintButton="false" runat="server" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                    </rsweb:ReportViewer> 
       </div>
        <%End If%>
