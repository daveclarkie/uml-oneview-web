<%@ Control Language="VB" AutoEventWireup="false" CodeFile="survey3details_ctl.ascx.vb" Inherits="survey3details_ctl" %>

<ul class='surveyformcontrol'>
<li runat="server" id="blkselecteduser_fk">
<asp:label id="lblselecteduser_fk" runat="server" CssClass="label">selecteduser_fk</asp:label>
<asp:DropDownList ID="selecteduser_fk" runat="server" cssClass="input_ddl" Width="150px" />
</li>
<li runat="server" id="blksupplier_fk" style="height:50px; border-bottom-style:none;">
<asp:label id="lblsupplier_fk" runat="server" CssClass="label">Supplier</asp:label>
<asp:TextBox ID="suppliername" runat="server" cssClass="input_strheader" />
</li>
<li runat="server" id="blksurvey_fk">
<asp:label id="lblsurvey_fk" runat="server" CssClass="label">survey_fk</asp:label>
<asp:DropDownList ID="survey_fk" runat="server" cssClass="input_ddl" Width="150px" />
</li>
<li runat="server" id="blkquestion1_surveyanswerfrequencyfivescale_fk" style="height:50px;">
<asp:label id="lblquestion1" runat="server" CssClass="label" Text="When invited to participate in a tender, how frequently does Supplier X submit an offer?"></asp:label>
<asp:DropDownList ID="question1_surveyanswerfrequencyfivescale_fk" runat="server" cssClass="input_ddl" Width="150px"></asp:DropDownList>
</li>
<li runat="server" id="blkquestion2_surveyanswerfrequencyfivescale_fk" style="height:50px;">
<asp:label id="lblquestion2" runat="server" CssClass="label" Text="How frequently does Supplier X submit offers on time?"></asp:label>
<asp:DropDownList ID="question2_surveyanswerfrequencyfivescale_fk" runat="server" cssClass="input_ddl" Width="150px"></asp:DropDownList>
</li>
<li runat="server" id="blkquestion3_surveyanswersixscale_fk" style="height:50px;">
<asp:label id="lblquestion3" runat="server" CssClass="label" Text="Supplier X generally submits offers that are complete and accurate."></asp:label>
<asp:DropDownList ID="question3_surveyanswersixscale_fk" runat="server" cssClass="input_ddl" Width="150px"></asp:DropDownList>
</li>
<li runat="server" id="blkquestion4_surveyanswersixscale_fk" style="height:50px;">
<asp:label id="lblquestion4" runat="server" CssClass="label" Text="When participating in tenders, Supplier X consistently provides a definitive, timely answer regarding credit decisions."></asp:label>
<asp:DropDownList ID="question4_surveyanswersixscale_fk" runat="server" cssClass="input_ddl" Width="150px"></asp:DropDownList>
</li>
<li runat="server" id="blkquestion5_surveyanswerfrequencyfivescale_fk" style="height:50px;">
<asp:label id="lblquestion5" runat="server" CssClass="label" Text="When submitting a contract for execution, how often does Supplier X send the correct version."></asp:label>
<asp:DropDownList ID="question5_surveyanswerfrequencyfivescale_fk" runat="server" cssClass="input_ddl" Width="150px"></asp:DropDownList>
</li>
<li runat="server" id="blksummary1_surveytimetocomplete_fk" style="height:50px;">
<asp:label id="lblsummary1" runat="server" CssClass="label" Text="Please let us know how much time it took for you to fill the survey."></asp:label>
<asp:DropDownList ID="summary1_surveytimetocomplete_fk" runat="server" cssClass="input_ddl" Width="150px" />
</li>
<li runat="server" id="blksummary2_surveyeaseofcomplete_fk" style="height:50px;">
<asp:label id="lblsummary2" runat="server" CssClass="label" Text="How easy did you find it to fill the survey?"></asp:label>
<asp:DropDownList ID="summary2_surveyeaseofcomplete_fk" runat="server" cssClass="input_ddl" Width="150px" />
</li>
<li runat="server" id="blksummary3_otherinformation" style="height:100px;">
<asp:label id="lblsummary3" runat="server" CssClass="label" Text="Are there any questions or areas you feel this survey has not addressed that should be? "></asp:label>
<asp:TextBox EnableViewState="false" ID="summary3_otherinformation" TextMode="Multiline" runat="server" cssClass="input_box" Width="148px" Height="100px" />
</li>

</ul>
<asp:DropDownList ID="supplier_fk" runat="server" cssClass="input_ddl"  Width="150px"></asp:DropDownList>


