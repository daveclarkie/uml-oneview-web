﻿
Partial Class backend_cca_eligibilitymanagement
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        Try
            PSM = SM

            validpk = Integer.TryParse(Request.QueryString("pk"), pk)

            If Not (Request.QueryString("r") Is Nothing) Then 'reason
                AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/core/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Dim cc As Object
    Dim validpk As Boolean = False
    Dim pk As Integer = 0


    Private Sub PageLoad()
        InitEligibilityThreshold(Page.IsPostBack)
    End Sub

    Private Sub DisplayPage()

        '' ===============================================
        ''  Which Action?
        '' ===============================================

    End Sub

    Private Sub RefreshList()

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_eligibilitythreshold_display"
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptEligibilityThreshold.DataSource = dt
        rptEligibilityThreshold.DataBind()
        CO.Dispose()

    End Sub


    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control)
        TargetControl.Controls.Clear()

        If Request.QueryString("pk") Is Nothing Then
            TargetButton.Visible = False
            RefreshList()
        Else
            cc = LoadControl(controlpath)
            cc.CDC = CDC
            cc.SM = SM
            cc.Read(SM.currentuser, SM.actualuser, Request.QueryString("pk"))
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        End If

    End Sub
    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Boolean = True
        If primary = True Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetAgreement)
        Else
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetAgreementSecondary)
        End If

        If Not (resDet) Then
            Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
            Else
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
            End If
        Else
            AddNotice("setNotice('Record Saved');")
            CancelContol()
        End If
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("pk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub

    Private Sub InitEligibilityThreshold(ByVal postback As Boolean)
        SetControl("~/controls/eligibilitythresholds_ctl.ascx", EligibilityThresholdControl, EligibilityThresholdButton)
    End Sub
    Protected Sub btnSaveEligibilityThreshold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEligibilityThreshold.Click
        SaveContol(True)
    End Sub
    Protected Sub btnCancelEligibilityThreshold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelEligibilityThreshold.Click
        CancelContol()
    End Sub
    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PageLoad()
    End Sub

End Class
