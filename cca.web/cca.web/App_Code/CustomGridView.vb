﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Web.UI.WebControls

Public Class CustomGridView
    
        Inherits DetailsView
        Private _fields As System.Web.UI.WebControls.DataControlFieldCollection
    Public Shadows Property Fields As System.Web.UI.WebControls.DataControlFieldCollection
        Get
            Return _fields
        End Get
        Set(ByVal value As System.Web.UI.WebControls.DataControlFieldCollection)
            _fields = value
        End Set
    End Property
    End Class