Partial Class notetypes_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents onotetypes As Objects.notetypes
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If onotetypes is Nothing then
onotetypes = new Objects.notetypes(currentuser,actualuser,pk,CDC)
End If
Me.typename.Text=onotetypes.typename
Me.controlname.Text=onotetypes.controlname
_CurrentPk=onotetypes.notetype_pk
Status=onotetypes.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If onotetypes is Nothing then
onotetypes = new Objects.notetypes(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
onotetypes.typename=Me.typename.Text
onotetypes.controlname=Me.controlname.Text
result=onotetypes.Save()
if not result then throw onotetypes.LastError
_CurrentPk=onotetypes.notetype_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=onotetypes.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotetypes is Nothing then
   onotetypes = new Objects.notetypes(currentuser,actualuser,pk,CDC)
  End If
  onotetypes.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotetypes is Nothing then
   onotetypes = new Objects.notetypes(currentuser,actualuser,pk,CDC)
  End If
  onotetypes.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotetypes is Nothing then
   onotetypes = new Objects.notetypes(currentuser,pk,CDC)
  End If
  onotetypes.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.typename.Enabled=IIF(WL>=0,True,False)
  Me.blktypename.Visible=IIF(RL>=0,True,False)
  Me.controlname.Enabled=IIF(WL>=0,True,False)
  Me.blkcontrolname.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

