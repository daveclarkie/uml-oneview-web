Partial Class ldzpostcodes_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oldzpostcodes As Objects.ldzpostcodes
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_ldzs_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.ldz_fk.Items.Count=0 Then
        Me.ldz_fk.DataSource=CDC.ReadDataTable(CO)
        Me.ldz_fk.DataTextField = "value"
        Me.ldz_fk.DataValueField = "pk"
        Try
            Me.ldz_fk.DataBind
        Catch Ex as Exception
            Me.ldz_fk.SelectedValue=-1
            Me.ldz_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oldzpostcodes is Nothing then
oldzpostcodes = new Objects.ldzpostcodes(currentuser,actualuser,pk,CDC)
End If
Me.ldz_fk.SelectedValue=oldzpostcodes.ldz_fk
Me.postcode.Text=oldzpostcodes.postcode
_CurrentPk=oldzpostcodes.ldzpostcode_pk
Status=oldzpostcodes.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oldzpostcodes is Nothing then
oldzpostcodes = new Objects.ldzpostcodes(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oldzpostcodes.ldz_fk=Me.ldz_fk.SelectedValue
oldzpostcodes.postcode=Me.postcode.Text
result=oldzpostcodes.Save()
if not result then throw oldzpostcodes.LastError
_CurrentPk=oldzpostcodes.ldzpostcode_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oldzpostcodes.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oldzpostcodes is Nothing then
   oldzpostcodes = new Objects.ldzpostcodes(currentuser,actualuser,pk,CDC)
  End If
  oldzpostcodes.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oldzpostcodes is Nothing then
   oldzpostcodes = new Objects.ldzpostcodes(currentuser,actualuser,pk,CDC)
  End If
  oldzpostcodes.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oldzpostcodes is Nothing then
   oldzpostcodes = new Objects.ldzpostcodes(currentuser,pk,CDC)
  End If
  oldzpostcodes.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.ldz_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkldz_fk.Visible=IIF(RL>=0,True,False)
  Me.postcode.Enabled=IIF(WL>=0,True,False)
  Me.blkpostcode.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

