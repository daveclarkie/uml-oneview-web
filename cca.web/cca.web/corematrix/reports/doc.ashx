﻿<%@ WebHandler Language="VB" Class="corematrix_reports_doc" %>
Public Class corematrix_reports_doc
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon
        SM = New MySM(context.Session.SessionID, Request, CDC)
        context.Server.ScriptTimeout = 10000
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()

        Dim flc As Integer = 0
        Dim dom As String = ""
        Dim site As String = "http://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("PATH_INFO")
        Dim localpath As String = ""
        Dim ref As String = Request.ServerVariables("HTTP_REFERER")
        Dim host As String = Request.ServerVariables("HTTP_HOST")
        Dim who As Integer = SM.currentuser
        Dim Matrix As Integer = SM.matrix
        site = site.Replace("/corematrix/reports/doc.ashx", "")
        
        Dim CO As New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)

        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)
        localpath = sys.datalocation & "Matrix\Reports\"
        
        Try
            dom = host.Split(".".ToCharArray, 2)(1)
        Catch ex As Exception
            dom = host
        End Try

        If ref = Nothing Then ref = ""
        If ref.Contains(site) Then

            CDC = New DataCommon

            CO = New SqlCommand
            Dim matrixtype As Integer = -1
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_matrixtype_matrix_pk"
            CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
            CDC.ReadScalarValue(matrixtype, CO)
            CO.Dispose()
            
            Dim mt As New matrixtypes(SM.currentuser, SM.actualuser, matrixtype, CDC)
            
            Dim url As String = site & mt.reporturl
            
            Dim pdf As Byte()
            Dim nc As String = Local.noCache
            url = url & "?u=" & who & "&m=" & Matrix & "&s=" & SM.session_pk & nc

            Try
                pdf = PdfConversion.SingleConvert(url)
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", "inline; filename=" & Now.Ticks & ".pdf")
                Response.AddHeader("Content-Length", pdf.Length - 1)
                Response.BinaryWrite(pdf)

                Dim wc As New System.Net.WebClient
                'Dim file As String
                'file = "c:\Matrix\Reports\"
                'file &= "matrix-"
                'file &= SM.matrix
                'file &= "\"
                
                'Dim d As documentlogs
                Dim c As String = ""
                wc.Encoding = Encoding.UTF8

                c &= wc.DownloadString(New System.Uri(url))

                IO.Directory.CreateDirectory(localpath)
                
                Response.Write(localpath)
                
                localpath &= nc.Replace("&nc=", "")
                localpath &= ".htm"
                                            
                Try
                    'Dim sym As New Encryption.Symmetric(Encryption.Symmetric.Provider.Rijndael, True)
                    'Dim key As New Encryption.Data("Cl9wn - ShO3d!")
                    'Dim encryptedData As Encryption.Data
                    'sym.KeySizeBits = 256
                    'sym.Key = key
                    'encryptedData = sym.Encrypt(New Encryption.Data(c))
                    
                    Dim s As String = c
                        
                    Dim fs As New IO.FileStream(localpath, IO.FileMode.CreateNew, IO.FileAccess.ReadWrite)
                    Dim sw As New IO.BinaryWriter(fs)
                    sw.Write(s)
                    sw.Flush()
                    sw.Close()
                    fs.Close()
                    fs.Dispose()
                   
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                End Try
 
            Catch ex As Exception
                Response.ContentType = "text/html"
                Response.Write("<html><body>Unable to process report, please contact Dave Clarke<br />" & url & "</body></html>")

            End Try
        End If

        Try
            Response.Flush()
        Catch ex As Exception
        End Try

        Response.Close()
        Response.End()

    End Sub

End Class