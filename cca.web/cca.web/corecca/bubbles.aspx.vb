Imports Microsoft.Reporting.WebForms
Imports System.Net
Imports System.Xml

Partial Class corecca_bubbles
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        validpk = Integer.TryParse(Request.QueryString("pk"), pk)
        validspk = Integer.TryParse(Request.QueryString("spk"), spk)

        If Not validpk Or pk = 0 Then
            ' we shouldn't be here
            Local.Bounce(SM, Response, "~/corecca/search.aspx", "no+valid+bubble+id")
        End If

        ' ===============================================
        '  New Target User?
        '        - Yes - Set Target User for session
        '        - No  - No action required  
        ' ===============================================

        If Not (SM.targetBubble = pk) Then
            SM.targetBubble = pk
            SM.mainid = SM.targetBubble
        End If

        If Not (SM.targetBubbleSecondary = spk) Then
            SM.targetBubbleSecondary = spk
        End If

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        CheckSessionValues()

        ' ===============================================
        '  Which Tab?
        ' ===============================================
        Try

            Select Case ModeCode
                Case "dt" : CurrentDisplay = BubblePageMode.Details
                    vwSub = vwBubble
                Case "tg" : CurrentDisplay = BubblePageMode.Targets
                    vwSub = vwBubbleTargets
                Case "ag" : CurrentDisplay = BubblePageMode.Agreements
                    vwSub = vwBubbleAgreements
                Case "rp" : CurrentDisplay = BubblePageMode.Reports
                    vwSub = vwBubbleReports
                Case Else
                    ' we shouldn't be here
                    Local.Bounce(SM, Response, "~/corecca/search.aspx", "invalid+display+mode")
            End Select

            If SM.targetBubble = -1 And Not CurrentDisplay = BubblePageMode.Details Then
                Local.Bounce(SM, Response, "~/corecca/bubbles.aspx", "No+Bubble&pk=-1&pg=ag")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/corecca/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Private Sub CheckSessionValues()
        'load agreement
        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property

    Dim cc As Object
    Dim maintab As String = "search"
    Dim due As DateTime
    Dim diff As Long

    Dim validpk As Boolean = False
    Dim validspk As Boolean = False
    Dim CurrentAction As Local.Action = Local.Action.None
    Dim CurrentDisplay As BubblePageMode = BubblePageMode.Details

    Dim vwSub As System.Web.UI.WebControls.View = vwBubble

    Dim pk As Integer = 0
    Dim spk As Integer = 0
    Dim md As Integer = 0

    Public Mode As BubblePageMode = BubblePageMode.Search

    Public Function TabSummary(ByVal item As String) As String
        Dim output As String = ""
        Select Case item
            'Case "sites" : output = sitecount
            'Case "meters" : output = 1
        End Select
        Return "(" & output & ")"
    End Function
    Private Sub SetMode()
        If Page.IsPostBack Then
            Mode = SM.AgreementPageMode
        Else
            SM.targetBubble = Request.QueryString("pk")
            Mode = BubblePageMode.Search
            Select Case Request.QueryString("pg")
                Case "dt" : Mode = BubblePageMode.Details
                Case "tg" : Mode = BubblePageMode.Targets
                Case "ag" : Mode = BubblePageMode.Agreements
                Case "rp" : Mode = BubblePageMode.Reports
                Case Else : Mode = BubblePageMode.Search
            End Select
            SM.BubblePageMode = Mode
        End If
    End Sub

    Private Sub PageLoad()
        BubbleNav.Visible = True

        Select Case SM.BubblePageMode
            Case BubblePageMode.Details : InitDetails(Page.IsPostBack)
            Case BubblePageMode.Targets : InitTargets(Page.IsPostBack)
            Case BubblePageMode.Agreements : InitAgreements(Page.IsPostBack)
            Case BubblePageMode.Reports : InitReports(Page.IsPostBack)
            Case Else
                Response.Redirect("search.aspx")
        End Select
    End Sub
    Private Sub DisplayPage()

        '' ===============================================
        ''  Which Action?
        '' ===============================================

        If validspk Then
            CurrentAction = Local.Action.Read
        Else
            CurrentAction = Local.Action.None
        End If

        'Me.mvDetail.ActiveViewIndex = CurrentDisplay
        'If Not vwSub Is Nothing Then mvDetail.SetActiveView(vwSub)

        'Select Case CurrentDisplay
        '    Case Local.AgreementDisplay.Agreement : SetAgreement()
        'End Select

    End Sub
    Private Sub RefreshList()

        If Mode = BubblePageMode.Details Then



        ElseIf Mode = BubblePageMode.Targets Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_bubbletargets_bubble"
            CO.Parameters.AddWithValue("@bubble", SM.targetBubble)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptBubbleTarget.DataSource = dt
            rptBubbleTarget.DataBind()
            CO.Dispose()

        ElseIf Mode = BubblePageMode.Agreements Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_bubbleagreements_bubble"
            CO.Parameters.AddWithValue("@bubble", SM.targetBubble)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptBubbleAgreements.DataSource = dt
            rptBubbleAgreements.DataBind()
            CO.Dispose()


        ElseIf Mode = BubblePageMode.Reports Then
        End If
    End Sub

    Private Property TotalRowCount() As Integer
        Get
            Dim o As Object = ViewState("TotalRowCount")
            If o Is Nothing Then
                Return -1
            Else
                Return CInt(o)
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TotalRowCount") = value
        End Set
    End Property
    Private ReadOnly Property PageIndex() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageIndex")) Then
                Return Convert.ToInt32(Request.QueryString("pageIndex"))
            Else
                Return 0
            End If
        End Get
    End Property
    Private ReadOnly Property PageSize() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageSize")) Then
                Return Convert.ToInt32(Request.QueryString("pageSize"))
            Else
                Return 4
            End If
        End Get
    End Property
    Private ReadOnly Property PageCount() As Integer
        Get
            If TotalRowCount <= 0 OrElse PageSize <= 0 Then
                Return 1
            Else
                Return ((TotalRowCount + PageSize) - 1) / PageSize
            End If
        End Get
    End Property
    Protected Sub lnkPrev_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        If ViewState("Page") > 1 Then
            ViewState("Page") = Convert.ToInt32(ViewState("Page")) - 1

            ' Reload control
            RefreshList()
        End If
    End Sub
    Protected Sub lnkNext_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the next page
        ViewState("Page") = Convert.ToInt32(ViewState("Page")) + 1

        ' Reload control
        RefreshList()
    End Sub

    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Boolean = True
        If primary = True Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetBubble)
        Else
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetBubbleSecondary)
        End If

        If Not (resDet) Then
            Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
            Else
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
            End If
        Else
            AddNotice("setNotice('Record Saved');")
            Dim filtered As New NameValueCollection(Request.QueryString)
            Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
            Dim url As String = ""

            If primary = True Then
                nameValueCollection.Remove("pk")
                url = HttpContext.Current.Request.Path + "?pk=" + cc.currentpk.ToString + "&" + nameValueCollection.ToString + "&r=Record+Saved"
            Else
                nameValueCollection.Remove("spk")
                url = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString + "&r=Record+Saved"
            End If
            Response.Redirect(url)
            'RefreshList()
            PageLoad()
        End If
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("spk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub
    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control, ByVal isBubble As Boolean)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM
        If isBubble Then
            cc.Read(SM.currentuser, SM.actualuser, SM.targetBubble)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        Else
            cc.Read(SM.currentuser, SM.actualuser, SM.targetBubbleSecondary)
            If Request.QueryString("spk") Is Nothing Then
                TargetControl.Controls.Clear()
                TargetButton.Visible = False
                RefreshList()
            Else
                TargetControl.Controls.Add(cc)
                TargetButton.Visible = True
                RefreshList()
            End If
        End If


    End Sub

    Private Sub InitDetails(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwBubble)
        SetControl("~/controls/bubbles_ctl.ascx", BubbleControl, BubbleButton, True)
    End Sub
    Private Sub InitTargets(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwBubbleTargets)
        SetControl("~/controls/bubbletargets_ctl.ascx", BubbleTargetControl, BubbleTargetButton, False)
    End Sub
    Private Sub InitAgreements(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwBubbleAgreements)
        SetControl("~/controls/bubbleagreements_ctl.ascx", BubbleAgreementControl, BubbleAgreementButton, False)
    End Sub
    Private Sub InitReports(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwBubbleReports)

        Dim bu As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        If bu.agreementtype_fk = 5 Then ' MPMA Agreement
            rp.Read(15)
        Else ' Non MPMA Types 
            If bu.federation_fk = 5 Then        ' Cold Store Distribution Federation
                rp.Read(7)
            ElseIf bu.federation_fk = 21 Then   ' British Cement Association
                rp.Read(8)
            Else                                ' Others
                rp.Read(9)
            End If
        End If

        If rp.rowstatus = 0 Then

            CO = New SqlClient.SqlCommand("rsp_lkp_bubbletargets_bubble")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@bubble_fk", SM.targetBubble)
            If Me.ddltarget.Items.Count = 0 Then
                Me.ddltarget.DataSource = CDC.ReadDataTable(CO)
                Me.ddltarget.DataTextField = "value"
                Me.ddltarget.DataValueField = "pk"
                Try
                    Me.ddltarget.DataBind()
                Catch Ex As Exception
                    Me.ddltarget.SelectedValue = -1
                    Me.ddltarget.DataBind()
                End Try
            End If

        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", SM.currentuser)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

        If helpdesklogindetail_pk = 0 Then
            Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
            Me.lblformResponse.ForeColor = Drawing.Color.Red
            Me.panelError.Visible = False
            Me.panelErrorResponse.Visible = True
        End If

    End Sub

    Protected Sub rptReportDataCheck_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If rptReportDataCheck.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyData"), Label)
                lblFooter.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnSaveBubble_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBubble.Click
        SaveContol(True)
    End Sub
    Protected Sub btnSaveBubbleTarget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBubbleTarget.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveBubbleAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBubbleAgreement.Click
        SaveContol(False)
    End Sub

    Protected Sub btnCancelBubbleTarget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelBubbleTarget.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelBubbleAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelBubbleAgreement.Click
        CancelContol()
    End Sub

    Public Function SelectedItemName() As String
        Dim itemclass As String = ""
        Select Case Mode
            Case BubblePageMode.Details
                itemclass = ""
            Case Else
                Dim b As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
                itemclass &= "for - " & b.bubblename
        End Select

        Return itemclass
    End Function

    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""
        Select Case Mode
            Case BubblePageMode.Details : itemclass = IIf(item = "bubble", "selected", "")
            Case BubblePageMode.Targets : itemclass = IIf(item = "target", "selected", "")
            Case BubblePageMode.Agreements : itemclass = IIf(item = "agreement", "selected", "")
            Case BubblePageMode.Reports : itemclass = IIf(item = "report", "selected", "")
            Case BubblePageMode.Search : itemclass = ""
        End Select
        Return itemclass
    End Function
    Public Function IsStaff() As Boolean
        Return cca.common.Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
    End Function
    Public Function bubbleStatus() As Integer
        Dim _return As Integer = -1

        Dim b As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        Try
            _return = b.rowstatus
        Catch ex As Exception
        End Try

        Return _return
    End Function

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMode()
        PageLoad()
    End Sub

    Protected Sub ddltarget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltarget.SelectedIndexChanged
        If ddltarget.SelectedValue = -1 Then
            ddlperiod.Items.Clear()
            ddlperiod.Items.Add("Select...")

            btnExportInvoice.Visible = False
            btnExportInvoice.Enabled = False
            btnExportInvoice.Text = "<h4>Export Invoice Data</h4>"

            rptReportDataCheck.Visible = False
            rptReportDataCheck.DataSource = Nothing
            rptReportDataCheck.DataBind()

        Else
            CO = New SqlClient.SqlCommand("rsp_lkp_periods_bubbletarget")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@bubbletarget_fk", ddltarget.SelectedItem.Value)
            Me.ddlperiod.Items.Clear()
            Me.ddlperiod.DataSource = CDC.ReadDataTable(CO)
            Me.ddlperiod.DataTextField = "value"
            Me.ddlperiod.DataValueField = "pk"
            Try
                Me.ddlperiod.DataBind()
            Catch Ex As Exception
                Me.ddlperiod.SelectedValue = -1
                Me.ddlperiod.DataBind()
            End Try

            btnExportInvoice.Visible = False
            btnExportInvoice.Enabled = True
            btnExportInvoice.Text = "<h6>Export Invoice Data</h6>"

        End If
    End Sub

    Protected Sub ddlperiod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlperiod.SelectedIndexChanged
        If ddlperiod.SelectedIndex = -1 Then
            btnRunReport.Enabled = False
            btnRunReport.Text = "<h4>Run Report</h4>"

            rptReportDataCheck.Visible = False
            rptReportDataCheck.DataSource = Nothing
            rptReportDataCheck.DataBind()
        Else
            btnRunReport.Enabled = True
            btnRunReport.Text = "<h6>Run Report</h6>"

            'CO = New SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_bubble_reportdatacheck_targetdate"
            'CO.Parameters.AddWithValue("@bubbletarget_fk", ddltarget.SelectedItem.Value)
            'CO.Parameters.AddWithValue("@targetperiod", ddlperiod.SelectedItem.Value)
            'Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            'rptReportDataCheck.DataSource = dt
            'rptReportDataCheck.DataBind()
            'CO.Dispose()
            rptReportDataCheck.Visible = True
        End If
    End Sub


    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


        Dim bu As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)
        If bu.agreementtype_fk = 5 Then ' MPMA Agreement
            rp.Read(15)
        Else ' Relative Agreement Types
            If bu.federation_fk = 5 Then        ' Cold Store Distribution Federation
                rp.Read(7)
            ElseIf bu.federation_fk = 21 Then   ' British Cement Association
                rp.Read(8)
            Else                                ' Others
                rp.Read(9)
            End If
        End If

        If rp.rowstatus = 0 Then

            Dim qsBubbleTarget As Integer = ddltarget.SelectedValue
            Dim qsPeriod As Integer = ddlperiod.SelectedValue

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport

            'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)
            'Set the report server URL and report path

            serverReport.ReportServerUrl = New Uri(rp.reportserverurl)
            serverReport.ReportPath = rp.reportpath


            Dim Target As New Microsoft.Reporting.WebForms.ReportParameter()
            Target.Name = "Target"
            Target.Values.Add(qsBubbleTarget)

            Dim Period As New Microsoft.Reporting.WebForms.ReportParameter()
            Period.Name = "Period"
            Period.Values.Add(qsPeriod)

            Dim filePath As String = sys.datalocation & "Reports\00.CCA MTS Reporting\Bubble_" & SM.targetBubble & "\"       'HttpContext.Current.Server.MapPath("~/App_Data/DS/")
            If (Not System.IO.Directory.Exists(filePath)) Then
                System.IO.Directory.CreateDirectory(filePath)
            End If

            Dim datestring As String = Now.ToString("yyyyMMdd_HHmmss_")

            Dim rt As New reporttypes(SM.currentuser, SM.actualuser, rp.reporttype_fk, CDC)
            Dim rl As New reportlogs(SM.currentuser, SM.actualuser, CDC)



            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Target, Period}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

            rl.report_fk = rp.report_pk
            rl.runfromurl = Request.Url.AbsoluteUri
            rl.param1name = "target"
            rl.param1value = qsBubbleTarget
            rl.param2name = "period"
            rl.param2value = qsPeriod
            rl.param3name = ""
            rl.param3value = ""
            rl.param4name = ""
            rl.param4value = ""
            rl.savedpath = filePath & datestring & rp.reportname + "." + extension
            rl.Save()

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' \\uk-ol1-file-01\Group Shares\Data\Reports\00.CCA MTS Reporting\
            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + datestring + "_" + rp.reportname + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)

            IO.File.WriteAllBytes(filePath & datestring & rp.reportname + "." + extension, bytes)

            Response.Flush()
            Response.[End]()
        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If

    End Sub

    Protected Sub btnExportInvoice_Click(sender As Object, e As System.EventArgs) Handles btnExportInvoice.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim qsBubbleTarget As Integer = ddltarget.SelectedValue

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        'Set the report server URL and report path
        serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
        serverReport.ReportPath = "/Operational Reports/Energy Management/CCA Invoiced Consumption Export"

        Dim Target As New Microsoft.Reporting.WebForms.ReportParameter()
        Target.Name = "agreementtarget_pk"
        Target.Values.Add(qsBubbleTarget)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Target}
        serverReport.SetParameters(parameters)

        Dim warnings As Warning() = {}
        Dim streamids As String() = {}
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim extension As String = ""
        Dim deviceInfo As String = ""
        deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
        Dim bytes As Byte() = serverReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamids, warnings)

        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType

        ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
        Response.AddHeader("Content-Disposition", "attachment; filename=CCA Invoice Data Export." + extension)
        'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.[End]()
    End Sub

    Protected Sub btnLogError_Click(sender As Object, e As System.EventArgs) Handles btnLogError.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim URL As String = sys.helpdeskurl & "/servlets/RequestServlet"

        Dim x As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)

        Dim title As String = "CCA Application Reporting Issue | Bubble | " & x.bubblename

        Dim Description As String = ""
        Description &= "<b>Bubble Report</b>"
        Description &= "<br />"
        Description &= "Description (optional):<br />" & Me.txtErrorDescription.Text.ToString & "<br /><br />"
        Description &= "<br />"
        Description &= "<b>Milestone Tracking Service Report (MTS)</b><br />"
        Description &= "Target: " & ddltarget.SelectedItem.Text & " (" & ddltarget.SelectedItem.Value & ") <br />"
        If ddlperiod.Items.Count > 0 Then
            Description &= "Period: " & ddlperiod.SelectedItem.Text & " (" & ddlperiod.SelectedItem.Value & ") <br />"
        End If
        Description &= "<br />"
        Description &= "<br />"

        Description &= "Requested created via API link from application"


        Dim reqtemplate As String = "CCA Application Reporting Issue"

        Dim targetURL As String = "/servlets/RequestServlet"
        Dim operation As String = "AddRequest"
        Dim technician As String = "Dave Clarke"

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", SM.currentuser)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

        Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
        Me.lblformResponse.ForeColor = Drawing.Color.Red
        Me.panelError.Visible = False
        Me.panelErrorResponse.Visible = True

        If helpdesklogindetail_pk > 0 Then
            Dim hd As New helpdesklogindetails(SM.currentuser, SM.actualuser, helpdesklogindetail_pk, CDC)
            Dim hd_domain As New helpdeskdomains(SM.currentuser, SM.actualuser, hd.helpdeskdomain_fk, CDC)
            Dim hd_logon As New helpdesklogondomains(SM.currentuser, SM.actualuser, hd.helpdesklogondomain_fk, CDC)


            Dim username As String = hd.username
            Dim password As String = hd.password
            Dim domain_name As String = hd_domain.domain_name
            Dim logonDomainName As String = hd_logon.logon_domain_name

            Dim postData As String = ""
            postData &= "targetURL=" & targetURL
            postData &= "&reqtemplate=" & reqtemplate
            postData &= "&username=" & username
            postData &= "&password=" & password
            postData &= "&logonDomainName=" & logonDomainName
            If domain_name.Length > 0 Then postData &= "&DOMAIN_NAME=" & domain_name
            postData &= "&operation=" & operation

            postData &= "&Application URL=" & Request.Url.AbsoluteUri
            postData &= "&description=" & Description
            postData &= "&title=" & title


            PostTo(URL, postData)
        Else
            Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
            Me.lblformResponse.ForeColor = Drawing.Color.Red
            Me.panelError.Visible = False
            Me.panelErrorResponse.Visible = True

        End If

    End Sub

    Protected Sub PostTo(url As String, postData As String)
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
        myWebRequest.Method = "POST"
        Dim byteArray As Byte() = System.Text.Encoding.[Default].GetBytes(postData)
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        myWebRequest.ContentLength = byteArray.Length
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        'Response.Write((DirectCast(myWebResponse, HttpWebResponse)).StatusDescription)
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = "<?xml version='1.0'?>" & reader.ReadToEnd()
        'Response.Write(responseFromServer + ":")

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim formResponse As String = "<table>"
        Dim outcome As String = xmlDoc.SelectSingleNode("/operation/operationstatus").InnerText
        formResponse &= "<tr><td width='100px'>Outcome</td> <td>" & outcome & "</td></tr>"
        If outcome = "Success" Then
            formResponse &= "<tr><td colspan='2'>The request ID is " & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "</td></tr>"
            formResponse &= "<tr><td colspan='2'><a style='color:blue;text-decoration:underline;' target='_blank' href='" & sys.helpdeskurl & "/WorkOrder.do?woMode=viewWO&woID=" & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "&&fromListView=true'>View Here</a></td></tr>"
        ElseIf outcome = "Failure" Then
            Dim xmlReason As String = xmlDoc.SelectSingleNode("/operation/message").InnerText
            formResponse &= "<tr><td>Reason</td> <td>" & xmlReason & "</td></tr>"
            If xmlReason = "Invalid UserName or Password" Then
                formResponse &= "<tr><td></td> <td>Please contact <a style='color:blue;text-decoration:underline;' href='mailto:dave.clarke@ems.schneider-electric.com?Subject=CCA App Helpdesk User Account Error'>Dave Clarke</a></td></tr>"
            End If
        End If
        formResponse &= "</table>"

        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Me.lblformResponse.Text = formResponse
        Me.lblformResponse.ForeColor = Drawing.Color.Black
        Me.panelError.Visible = False
        Me.panelErrorResponse.Visible = True

    End Sub

    Public Function returnselect(ByVal url As String) As String
        Dim _return As String = ""
        _return = "<a href='" & url & Local.noCache & "' target='_blank'><h6>view</h6></a>"
        Return _return
    End Function

End Class
