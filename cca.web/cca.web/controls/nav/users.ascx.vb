Imports System.Data.SqlClient

Partial Class navctls_users
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim _listType As UserListType = UserListType.Recent
    Dim _label As String = "Recent"

    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public Property ListType() As UserListType
        Get
            Return _listType
        End Get
        Set(ByVal value As UserListType)
            _listType = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CO As New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        Select Case _listType
            Case UserListType.Popular
                CO.CommandText = "rsp_mypopularusers"
            Case UserListType.Recent
                CO.CommandText = "rsp_myrecentusers"
            Case Else
                CO.Dispose()
                Exit Sub
        End Select
        CO.Parameters.AddWithValue("@cu", SM.currentuser)
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        CO.Dispose()
        rptuser.DataSource = dt
        rptuser.DataBind()
    End Sub
End Class
