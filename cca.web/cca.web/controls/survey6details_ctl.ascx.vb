Partial Class survey6details_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents osurvey6details As Objects.survey6details
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_users_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.selecteduser_fk.Items.Count = 0 Then
            Me.selecteduser_fk.DataSource = CDC.ReadDataTable(CO)
            Me.selecteduser_fk.DataTextField = "value"
            Me.selecteduser_fk.DataValueField = "pk"
            Try
                Me.selecteduser_fk.DataBind()
            Catch Ex As Exception
                Me.selecteduser_fk.SelectedValue = -1
                Me.selecteduser_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_suppliers_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.supplier_fk.Items.Count = 0 Then
            Me.supplier_fk.DataSource = CDC.ReadDataTable(CO)
            Me.supplier_fk.DataTextField = "value"
            Me.supplier_fk.DataValueField = "pk"
            Try
                Me.supplier_fk.DataBind()
            Catch Ex As Exception
                Me.supplier_fk.SelectedValue = -1
                Me.supplier_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.survey_fk.Items.Count = 0 Then
            Me.survey_fk.DataSource = CDC.ReadDataTable(CO)
            Me.survey_fk.DataTextField = "value"
            Me.survey_fk.DataValueField = "pk"
            Try
                Me.survey_fk.DataBind()
            Catch Ex As Exception
                Me.survey_fk.SelectedValue = -1
                Me.survey_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question1_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question1_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question1_surveyanswersixscale_fk.DataTextField = "value"
            Me.question1_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question1_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question1_surveyanswersixscale_fk.SelectedValue = -1
                Me.question1_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question2_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question2_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question2_surveyanswersixscale_fk.DataTextField = "value"
            Me.question2_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question2_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question2_surveyanswersixscale_fk.SelectedValue = -1
                Me.question2_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question3_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question3_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question3_surveyanswersixscale_fk.DataTextField = "value"
            Me.question3_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question3_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question3_surveyanswersixscale_fk.SelectedValue = -1
                Me.question3_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question4_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question4_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question4_surveyanswersixscale_fk.DataTextField = "value"
            Me.question4_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question4_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question4_surveyanswersixscale_fk.SelectedValue = -1
                Me.question4_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question5_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question5_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question5_surveyanswersixscale_fk.DataTextField = "value"
            Me.question5_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question5_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question5_surveyanswersixscale_fk.SelectedValue = -1
                Me.question5_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyanswersixscales_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.question6_surveyanswersixscale_fk.Items.Count = 0 Then
            Me.question6_surveyanswersixscale_fk.DataSource = CDC.ReadDataTable(CO)
            Me.question6_surveyanswersixscale_fk.DataTextField = "value"
            Me.question6_surveyanswersixscale_fk.DataValueField = "pk"
            Try
                Me.question6_surveyanswersixscale_fk.DataBind()
            Catch Ex As Exception
                Me.question6_surveyanswersixscale_fk.SelectedValue = -1
                Me.question6_surveyanswersixscale_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveytimetocompletes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.summary1_surveytimetocomplete_fk.Items.Count = 0 Then
            Me.summary1_surveytimetocomplete_fk.DataSource = CDC.ReadDataTable(CO)
            Me.summary1_surveytimetocomplete_fk.DataTextField = "value"
            Me.summary1_surveytimetocomplete_fk.DataValueField = "pk"
            Try
                Me.summary1_surveytimetocomplete_fk.DataBind()
            Catch Ex As Exception
                Me.summary1_surveytimetocomplete_fk.SelectedValue = -1
                Me.summary1_surveytimetocomplete_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_surveyeaseofcompletes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.summary2_surveyeaseofcomplete_fk.Items.Count = 0 Then
            Me.summary2_surveyeaseofcomplete_fk.DataSource = CDC.ReadDataTable(CO)
            Me.summary2_surveyeaseofcomplete_fk.DataTextField = "value"
            Me.summary2_surveyeaseofcomplete_fk.DataValueField = "pk"
            Try
                Me.summary2_surveyeaseofcomplete_fk.DataBind()
            Catch Ex As Exception
                Me.summary2_surveyeaseofcomplete_fk.SelectedValue = -1
                Me.summary2_surveyeaseofcomplete_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0, Optional rownumber As Integer = 0)
        ctlInit()
        If osurvey6details Is Nothing Then
            osurvey6details = New objects.survey6details(currentuser, actualuser, pk, CDC)
        End If
        Dim oSupplier As New objects.suppliers(currentuser, actualuser, osurvey6details.supplier_fk, CDC)
        Dim oUser As New objects.users(currentuser, actualuser, osurvey6details.selecteduser_fk, CDC)
        Me.selecteduser_fk.SelectedValue = osurvey6details.selecteduser_fk
        Me.lblsupplier_fk.Text = "User: " & oUser.username & " (" & oUser.user_pk & ") <br />Supplier Survey: Billing Team"
        Me.supplier_fk.SelectedValue = osurvey6details.supplier_fk
        Me.suppliername.Text = oSupplier.supplier_name
        Me.survey_fk.SelectedValue = osurvey6details.survey_fk
        Me.question1_surveyanswersixscale_fk.SelectedValue = osurvey6details.question1_surveyanswersixscale_fk
        Me.question2_surveyanswersixscale_fk.SelectedValue = osurvey6details.question2_surveyanswersixscale_fk
        Me.question3_surveyanswersixscale_fk.SelectedValue = osurvey6details.question3_surveyanswersixscale_fk
        Me.question4_surveyanswersixscale_fk.SelectedValue = osurvey6details.question4_surveyanswersixscale_fk
        Me.question5_surveyanswersixscale_fk.SelectedValue = osurvey6details.question5_surveyanswersixscale_fk
        Me.question6_surveyanswersixscale_fk.SelectedValue = osurvey6details.question6_surveyanswersixscale_fk
        Me.summary1_surveytimetocomplete_fk.SelectedValue = osurvey6details.summary1_surveytimetocomplete_fk
        Me.summary2_surveyeaseofcomplete_fk.SelectedValue = osurvey6details.summary2_surveyeaseofcomplete_fk
        Me.summary3_otherinformation.Text = osurvey6details.summary3_otherinformation
        _CurrentPk = osurvey6details.surveydetail_pk
        Status = osurvey6details.rowstatus
        Dim strWidth As String = "150px"
        If rownumber > 0 Then
            strWidth = "150px"
            'Hide all the questions
            Me.lblselecteduser_fk.Visible = False
            Me.lblsupplier_fk.Visible = False
            Me.lblsurvey_fk.Visible = False
            Me.lblquestion1.Visible = False
            Me.lblquestion2.Visible = False
            Me.lblquestion3.Visible = False
            Me.lblquestion4.Visible = False
            Me.lblquestion5.Visible = False
            Me.lblquestion6.Visible = False

            Me.lblsummary1.Visible = False
            Me.lblsummary2.Visible = False
            Me.lblsummary3.Visible = False

            Me.summary1_surveytimetocomplete_fk.Visible = False
            Me.summary2_surveyeaseofcomplete_fk.Visible = False
            Me.summary3_otherinformation.Visible = False

        Else
            strWidth = "355px"
        End If

        Me.blkselecteduser_fk.Style.Item("width") = strWidth
        Me.blksupplier_fk.Style.Item("width") = strWidth
        Me.blksurvey_fk.Style.Item("width") = strWidth
        Me.blkquestion1_surveyanswersixscale_fk.Style.Item("width") = strWidth
        Me.blkquestion2_surveyanswersixscale_fk.Style.Item("width") = strWidth
        Me.blkquestion3_surveyanswersixscale_fk.Style.Item("width") = strWidth
        Me.blkquestion4_surveyanswersixscale_fk.Style.Item("width") = strWidth
        Me.blkquestion5_surveyanswersixscale_fk.Style.Item("width") = strWidth
        Me.blkquestion6_surveyanswersixscale_fk.Style.Item("width") = strWidth

        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey6details Is Nothing Then
            osurvey6details = New Objects.survey6details(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            osurvey6details.selecteduser_fk = Me.selecteduser_fk.SelectedValue
            osurvey6details.supplier_fk = Me.supplier_fk.SelectedValue
            osurvey6details.survey_fk = Me.survey_fk.SelectedValue
            osurvey6details.question1_surveyanswersixscale_fk = Me.question1_surveyanswersixscale_fk.SelectedValue
            osurvey6details.question2_surveyanswersixscale_fk = Me.question2_surveyanswersixscale_fk.SelectedValue
            osurvey6details.question3_surveyanswersixscale_fk = Me.question3_surveyanswersixscale_fk.SelectedValue
            osurvey6details.question4_surveyanswersixscale_fk = Me.question4_surveyanswersixscale_fk.SelectedValue
            osurvey6details.question5_surveyanswersixscale_fk = Me.question5_surveyanswersixscale_fk.SelectedValue
            osurvey6details.question6_surveyanswersixscale_fk = Me.question6_surveyanswersixscale_fk.SelectedValue
            osurvey6details.summary1_surveytimetocomplete_fk = Me.summary1_surveytimetocomplete_fk.SelectedValue
            osurvey6details.summary2_surveyeaseofcomplete_fk = Me.summary2_surveyeaseofcomplete_fk.SelectedValue
            osurvey6details.summary3_otherinformation = Me.summary3_otherinformation.Text
            result = osurvey6details.Save()
            If Not result Then Throw osurvey6details.LastError
            _CurrentPk = osurvey6details.surveydetail_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = osurvey6details.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey6details Is Nothing Then
            osurvey6details = New Objects.survey6details(currentuser, actualuser, pk, CDC)
        End If
        osurvey6details.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey6details Is Nothing Then
            osurvey6details = New Objects.survey6details(currentuser, actualuser, pk, CDC)
        End If
        osurvey6details.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osurvey6details Is Nothing Then
            osurvey6details = New Objects.survey6details(currentuser, pk, CDC)
        End If
        osurvey6details.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Me.selecteduser_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkselecteduser_fk.Visible = IIf(RL >= 99, True, False)
        Me.suppliername.Enabled = IIf(WL >= 0, True, False)
        Me.supplier_fk.Visible = IIf(WL >= 99, True, False)
        Me.blksupplier_fk.Visible = IIf(RL >= 0, True, False)
        Me.survey_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blksurvey_fk.Visible = IIf(RL >= 99, True, False)
        Me.question1_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion1_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.question2_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion2_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.question3_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion3_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.question4_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion4_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.question5_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion5_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.question6_surveyanswersixscale_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkquestion6_surveyanswersixscale_fk.Visible = IIf(RL >= 0, True, False)
        Me.summary1_surveytimetocomplete_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blksummary1_surveytimetocomplete_fk.Visible = IIf(RL >= 0, True, False)
        Me.summary2_surveyeaseofcomplete_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blksummary2_surveyeaseofcomplete_fk.Visible = IIf(RL >= 0, True, False)
        Me.summary3_otherinformation.Enabled = IIf(WL >= 0, True, False)
        Me.blksummary3_otherinformation.Visible = IIf(RL >= 0, True, False)
    End Sub
End Class

