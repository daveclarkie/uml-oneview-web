Partial Class note3details_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents onote3details As Objects.note3details
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_persons_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.person_fk.Items.Count=0 Then
        Me.person_fk.DataSource=CDC.ReadDataTable(CO)
        Me.person_fk.DataTextField = "value"
        Me.person_fk.DataValueField = "pk"
        Try
            Me.person_fk.DataBind
        Catch Ex as Exception
            Me.person_fk.SelectedValue=-1
            Me.person_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.location_fk.Items.Count=0 Then
        Me.location_fk.DataSource=CDC.ReadDataTable(CO)
        Me.location_fk.DataTextField = "value"
        Me.location_fk.DataValueField = "pk"
        Try
            Me.location_fk.DataBind
        Catch Ex as Exception
            Me.location_fk.SelectedValue=-1
            Me.location_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If onote3details is Nothing then
onote3details = new Objects.note3details(currentuser,actualuser,pk,CDC)
End If
Me.person_fk.SelectedValue=onote3details.person_fk
Me.location_fk.SelectedValue=onote3details.location_fk
_CurrentPk=onote3details.notedetail_pk
Status=onote3details.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If onote3details is Nothing then
onote3details = new Objects.note3details(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
onote3details.person_fk=Me.person_fk.SelectedValue
onote3details.location_fk=Me.location_fk.SelectedValue
result=onote3details.Save()
if not result then throw onote3details.LastError
_CurrentPk=onote3details.notedetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=onote3details.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onote3details is Nothing then
   onote3details = new Objects.note3details(currentuser,actualuser,pk,CDC)
  End If
  onote3details.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onote3details is Nothing then
   onote3details = new Objects.note3details(currentuser,actualuser,pk,CDC)
  End If
  onote3details.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onote3details is Nothing then
   onote3details = new Objects.note3details(currentuser,pk,CDC)
  End If
  onote3details.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.person_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkperson_fk.Visible=IIF(RL>=0,True,False)
  Me.location_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocation_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

