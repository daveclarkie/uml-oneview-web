
Partial Class functions_deactusr
    Inherits DavePage : Public PSM As MySM = SM

    Dim AllowAction As Boolean = True
    Dim AccDen As Boolean = False
    Dim AccAct As Boolean = False
    Dim AccDel As Boolean = False
    Dim BadData As Boolean = False
    Dim u As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Response.Buffer = True

        Try
            AllowAction = AllowAction Or Integer.TryParse(Request.QueryString("u"), u)
            If Not (u > 0) Then
                AllowAction = False
                BadData = True
            Else
                AllowAction = AllowAction Or Security.ReferalCheck(Request)
                AllowAction = AllowAction Or Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC)
                If AllowAction = False Then AccDen = True
            End If

            Dim CO As New SqlClient.SqlCommand

            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "[rsp_userstatus]"
            CO.Parameters.AddWithValue("@user", u)
            Dim userstatus As Integer = -1
            CDC.ReadScalarValue(userstatus, CO)

            Select Case userstatus
                Case 1
                    AccAct = True
                    AllowAction = False
                Case 2
                    AccDel = True
                    AllowAction = False
            End Select
            If AllowAction Then

                Dim user As New users(SM.currentuser, SM.actualuser, u, CDC)
                If user.Disable Then
                    Response.Write("OK:" & u)
                Else
                    Throw New IndexOutOfRangeException(user.LastError.Message)
                End If

            Else
                If AccDen Then
                    Response.Write("Fail:Account was not modified.You do not have the required access rights.")
                ElseIf BadData Then
                    Response.Write("Fail:Account was not modified. Invalid data was supplied.")
                ElseIf AccDel Then
                    Response.Write("Fail:Account was not modified. This account has been deleted.")
                ElseIf AccAct Then
                    Response.Write("Fail:Account was not modified. This account is already inactive.")
                End If

            End If

        Catch Ex As Exception
            Response.Clear()
            Response.Write("Fail:" & Ex.Message)
        End Try

    End Sub
End Class
