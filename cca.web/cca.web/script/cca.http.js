﻿// JScript File
function newRequest(){
    var RQ=false;
    /*@cc_on @*/
    /*@if (@_jscript_version >= 5)
        try { RQ = new ActiveXObject("Msxml2.XMLHTTP");} 
        catch (e) {
            try  { RQ = new ActiveXObject("Microsoft.XMLHTTP");} 
            catch (E) { RQ = false;}}
    @end @*/
    if (!RQ && typeof XMLHttpRequest!='undefined') {
	    try { RQ = new XMLHttpRequest();} 
	    catch (e) { RQ=false;}
    }
    if (!RQ && window.createRequest) {
	    try { RQ = window.createRequest();} 
	    catch (e) { RQ=false;}
    }
    return RQ;
}

function setCallback(RQ, fnc){
    var cbSet=false;
    if (RQ.attachEvent) { RQ.attachEvent('onreadystatechange', fnc); cbSet=true;}
    if (!cbSet && RQ.addEventListener) { RQ.addEventListener('readystatechange', fnc, false); cbSet=true;}
    if (!cbSet) { RQ.onreadystatechange=fnc; cbSet=true; }
    if (!cbSet) { alert("This function is either disabled or not supported by your browser");} 
   return cbSet; 
}

function readXML(txt){
    if (window.DOMParser) { dp=new DOMParser(); xmlObj=dp.parseFromString(txt,"text/xml");}
    else { xmlObj=new ActiveXObject("Microsoft.XMLDOM"); xmlObj.async="false"; xmlObj.loadXML(txt);}
   if (!xmlObj){ alert("This function is either disabled or not supported by your browser");} 
    return xmlObj;
}

