Partial Class backend_userstructuremanagement
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_userstructuremanagement_populate"
            managerList.DataTextField = "username"
            managerList.DataValueField = "user_pk"
            managerList.DataSource = CDC.ReadDataTable(CO)
            managerList.DataBind()
        End If


        If Not Security.GroupMember(SM.actualuser, SystemGroups.ManageUsers, CDC) Then
            Local.Bounce(SM, Response, "~/default.aspx", "Access+Denied")
        End If

        Me.managerList.Attributes.Add("onChange", "loadMembers('" + Me.managerList.ClientID + "');")



    End Sub

End Class
