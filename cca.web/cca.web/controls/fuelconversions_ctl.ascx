<%@ Control Language="VB" AutoEventWireup="false" CodeFile="fuelconversions_ctl.ascx.vb" Inherits="fuelconversions_ctl" %>
<h5>fuelconversions</h5>
<ul class='formcontrol'>
<li runat="server" id="blksourcefuel_fk">
<span class='label'>Source Fuel</span>
<asp:DropDownList ID="sourcefuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksourceunit_fk">
<span class='label'>Unit</span>
<asp:DropDownList ID="sourceunit_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktargetfuel_fk">
<span class='label'>Target</span>
<asp:DropDownList ID="targetfuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktargetunit_fk">
<span class='label'>Unit</span>
<asp:DropDownList ID="targetunit_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkvalidfrom">
<span class='label'>Valid From</span>
<asp:TextBox EnableViewState="false" ID="validfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkvalidto">
<span class='label'>Valid To</span>
<asp:TextBox EnableViewState="false" ID="validto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkconversionfactor">
<span class='label'>Conversion Factor</span>
<asp:TextBox EnableViewState="false" ID="conversionfactor" runat="server" cssClass="input_dbl" />
</li>
</ul>

