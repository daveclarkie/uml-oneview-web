Partial Class helpdesklogindetails_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ohelpdesklogindetails As Objects.helpdesklogindetails
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_helpdeskdomains_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.helpdeskdomain_fk.Items.Count=0 Then
        Me.helpdeskdomain_fk.DataSource=CDC.ReadDataTable(CO)
        Me.helpdeskdomain_fk.DataTextField = "value"
        Me.helpdeskdomain_fk.DataValueField = "pk"
        Try
            Me.helpdeskdomain_fk.DataBind
        Catch Ex as Exception
            Me.helpdeskdomain_fk.SelectedValue=-1
            Me.helpdeskdomain_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_helpdesklogondomains_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.helpdesklogondomain_fk.Items.Count=0 Then
        Me.helpdesklogondomain_fk.DataSource=CDC.ReadDataTable(CO)
        Me.helpdesklogondomain_fk.DataTextField = "value"
        Me.helpdesklogondomain_fk.DataValueField = "pk"
        Try
            Me.helpdesklogondomain_fk.DataBind
        Catch Ex as Exception
            Me.helpdesklogondomain_fk.SelectedValue=-1
            Me.helpdesklogondomain_fk.DataBind
        End Try
    End If
            Me.helpdeskuserid.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ohelpdesklogindetails is Nothing then
ohelpdesklogindetails = new Objects.helpdesklogindetails(currentuser,actualuser,pk,CDC)
End If
Me.user_fk.SelectedValue=ohelpdesklogindetails.user_fk
Me.username.Text=ohelpdesklogindetails.username
Me.password.Text=ohelpdesklogindetails.password
Me.helpdeskdomain_fk.SelectedValue=ohelpdesklogindetails.helpdeskdomain_fk
Me.helpdesklogondomain_fk.SelectedValue=ohelpdesklogindetails.helpdesklogondomain_fk
Me.helpdeskuserid.Text=ohelpdesklogindetails.helpdeskuserid
Me.helpdeskuserid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.helpdeskuserid.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=ohelpdesklogindetails.helpdesklogindetail_pk
Status=ohelpdesklogindetails.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ohelpdesklogindetails is Nothing then
ohelpdesklogindetails = new Objects.helpdesklogindetails(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ohelpdesklogindetails.user_fk=Me.user_fk.SelectedValue
ohelpdesklogindetails.username=Me.username.Text
ohelpdesklogindetails.password=Me.password.Text
ohelpdesklogindetails.helpdeskdomain_fk=Me.helpdeskdomain_fk.SelectedValue
ohelpdesklogindetails.helpdesklogondomain_fk=Me.helpdesklogondomain_fk.SelectedValue
ohelpdesklogindetails.helpdeskuserid=Me.helpdeskuserid.Text
result=ohelpdesklogindetails.Save()
if not result then throw ohelpdesklogindetails.LastError
_CurrentPk=ohelpdesklogindetails.helpdesklogindetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ohelpdesklogindetails.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesklogindetails is Nothing then
   ohelpdesklogindetails = new Objects.helpdesklogindetails(currentuser,actualuser,pk,CDC)
  End If
  ohelpdesklogindetails.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesklogindetails is Nothing then
   ohelpdesklogindetails = new Objects.helpdesklogindetails(currentuser,actualuser,pk,CDC)
  End If
  ohelpdesklogindetails.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdesklogindetails is Nothing then
   ohelpdesklogindetails = new Objects.helpdesklogindetails(currentuser,pk,CDC)
  End If
  ohelpdesklogindetails.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.user_fk.Enabled=IIF(WL>=9,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=9,True,False)
  Me.username.Enabled=IIF(WL>=0,True,False)
  Me.blkusername.Visible=IIF(RL>=0,True,False)
  Me.password.Enabled=IIF(WL>=0,True,False)
  Me.blkpassword.Visible=IIF(RL>=0,True,False)
  Me.helpdeskdomain_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdeskdomain_fk.Visible=IIF(RL>=0,True,False)
  Me.helpdesklogondomain_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdesklogondomain_fk.Visible=IIF(RL>=0,True,False)
  Me.helpdeskuserid.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdeskuserid.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

