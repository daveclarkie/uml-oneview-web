<%@ WebHandler Language="VB" Class="function_managegroups" %>
Public Class function_managegroups
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse
    Dim CO As SqlCommand
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim msg As String = ""
        Dim dt As DataTable
        Dim dr As DataRow
                
        Dim group_pk As Integer
        Dim method As Integer
        
             
        
        Try
            group_pk = RQ("b").ToString.Trim()
        Catch ex As Exception
            group_pk = -1
        End Try
        Try
            method = RQ("c").ToString.Trim()
        Catch ex As Exception
            method = -1
        End Try

        Try
          
            If method = 0 Then
          
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_groupmanagement_activemember"
                CO.Parameters.AddWithValue("@group", group_pk)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                    
                    
                    
                    msg &= "<li id='AcUser_" & dr("usergroup_pk") & "'>"
                    msg &= "<a href ='javascript:Remove(""AcUser_" & dr("usergroup_pk") & """," & dr("usergroup_pk") & "," & dr("user_fk") & "," & dr("group_fk") & ",""" & dr("username") & """);'>"
                    msg &= "<img src='/design/images/icon_remove.png' alt='remove' class='manageGroup_Icon'  />"
                    msg &= "   "
                    msg &= dr("username")
                    msg &= "</a>"
                    msg &= "</li>"
                    
                Next
            ElseIf method = 1 Then
            
            
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_groupmanagement_availablemember"
                CO.Parameters.AddWithValue("@group", group_pk)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                  
                    msg &= "<li id='AvUser_" & dr("user_fk") & "'>"
                    msg &= "<a href ='javascript:Add(""AvUser_" & dr("user_fk") & """," & dr("usergroup_pk") & "," & dr("user_fk") & "," & dr("group_fk") & ",""" & dr("username") & """);'>"
                    msg &= "<img src='/design/images/icon_add.png' alt='add' class='manageGroup_Icon'  />"
                    msg &= "   "
                    msg &= dr("username")
                    msg &= "</a>"
                    msg &= "</li>"
                    
                Next
            
            End If
                
        Catch LE As Exception
            msg = "FAIL:" & LE.Message
        End Try
        Response.Write(msg)
        
    End Sub

End Class