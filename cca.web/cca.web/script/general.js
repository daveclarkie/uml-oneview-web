﻿var dateFormat="Format: D MMM YYYY eg 1 Jan 1980<br /> If you are unsure of the exact date then enter '1' for the day of  the date.<br /> If you are unsure of the month then enter 'Jan'.<br /> e.g. If  'I moved in to the house during 1981'  you would enter '1 Jan 1981'.<br /> If you are unsure of the year, use the year closest to your approximate recollection";
function debugW(newValue) {
    chgNamedElementText("debug",chgAfter,newValue);
}

function removeParameter(url, parameter) {
    var urlparts = url.split('?');

    if (urlparts.length >= 2) {
        var urlBase = urlparts.shift(); //get first part, and remove from array
        var queryString = urlparts.join("?"); //join it back up

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = queryString.split(/[&;]/g);
        for (var i = pars.length; i-- > 0; )               //reverse iteration as may be destructive
            if (pars[i].lastIndexOf(prefix, 0) !== -1)   //idiom for string.startsWith
                pars.splice(i, 1);
        url = urlBase + '?' + pars.join('&');
    }
    return url;
}

function setNotice(newValue) {
    provideSysNotices();
   var hdr=document.getElementById("systemnotices");
    if (hdr.style.backgroundColor!="#009530") {
        hdr.style.backgroundColor = "#ff0000";
    } 
    chgNamedElementText("formatinfo",chgReplace,newValue);
}

function clearNotice() {
    provideSysNotices();
   var hdr=document.getElementById("systemnotices");
   hdr.style.backgroundColor = "#009530";
    chgNamedElementText("formatinfo",chgClear);
}

function setError(newValue) {
    provideSysNotices();
   var hdr=document.getElementById("systemnotices");
    hdr.style.backgroundColor="#ff0000";
    chgNamedElementText("probleminfo",chgReplace,newValue);
}

function clearError() {
    provideSysNotices();
   var hdr=document.getElementById("systemnotices");
   hdr.style.backgroundColor = "#009530";
    chgNamedElementText("probleminfo",chgClear);
}

var rszr;
function showAsyncAction(trgname,state,cls){
    setImages(cls);
    if (rszr!=null){rszr.instant(); rszr=null;}
    rszr=new TDeR(trgname);
    if (state==1){
        rszr.curSize=rszr.minSize; rszr.intervalId=setInterval('rszr.doExpand()',refreshRate);
    } else {
        rszr.curSize=rszr.maxSize; rszr.intervalId=setInterval('rszr.doShrink()',refreshRate);
    }
}

/* ============================================================= */
/*  DO NOT CACHE THE F$(K!n PAGE*/
/* ============================================================= */
function noCache(){
	var date = new Date();
	var ticks = date.getTime();
	return "&nc="+ticks;
}


function setOnLoad(functionObj){
    var isSet=false;
	if (!isSet && window.attachEvent) {window.attachEvent('onload', functionObj); isSet=true;}
	if (!isSet && window.addEventListener) {window.addEventListener('load', functionObj, false); isSet=true;}
	if (!isSet) {document.addEventListener('load', functionObj, false); isSet=true;} 
	return isSet;
}

function setEvent(element,eventName,functionObj){
    var isSet=false;
	if (!isSet && element.attachEvent) {element.attachEvent('on'+eventName, functionObj); isSet=true;}
	if (!isSet && element.addEventListener) {element.addEventListener(eventName, functionObj, false); isSet=true;}
	return isSet;
}

function showCredits(){
    var doc="/credits.aspx";
    var mw=window.open(doc,"docWin");
}

function setCredits(){
        var elem=document.getElementById("logo");
        if ( elem ) {
            setEvent(elem,"click",showCredits)        
        }
}

// if System Notices is not a part of the page then add it
function provideSysNotices(){
    try {
            var locElem=document.getElementById("systemnotices");
            if ( !locElem ) {
                var mkElem=document.getElementById("ln");
                if ( !mkElem ) {mkElem=document.createElement("ln"); mkElem.setAttribute('style','position:fixed;top:20px;right:10px;width:200px;background-color:#ffffff;'); document.getElementById("aspnetForm").appendChild(mkElem);} 
                var sn=document.createElement("h3");
                sn.setAttribute('style','background-color:green;'); 
                sn.setAttribute('id','systemnotices');
                var fi=document.createElement("div");
                fi.setAttribute('id','formatinfo');
                var pi=document.createElement("div");
                pi.setAttribute('id','probleminfo');

                var st=document.createTextNode("System Notices");
                var pt=document.createTextNode(" ");
                var ft=document.createTextNode(" ");

                pi.appendChild(pt);
                fi.appendChild(ft);
                sn.appendChild(st);
                mkElem.appendChild(sn);
                mkElem.appendChild(fi);
                mkElem.appendChild(pi);
                 
            }
    }
    catch (e){
        alert(e);
    }
}


// nuke auto-complete

var pw=0;

function badBrowser(){

// looping over all input fields

    var lms=document.getElementsByTagName("input")
    var lm; 
    var l; 
    var rl;
    var f=0;
    var pwdbox;
    var btnsub; 
    for (lm in lms){

        if ( isNumeric(lm) ){
            l=lms[lm];
        } else {
            rl=rpl(lm,'\\$','_');
            l=document.getElementById(rl);
        }
        if ( l ) {
            if (l.type=="password" && f==0) {
                pwdbox=l;
                pwdbox.onkeyup=function(){
                    pw++;
                }        
                pwdbox.onfocus=function(){
                    clearNotice(); 
                    pw=0;
                }        
                pwdbox.onblur=function(){
                    if (this.value.length > pw){this.value="";pw=0;setNotice("Pasted or auto-completed password detected.");}
                }
               f=1; 
            } // if password
            if (l.type=="submit" && f==1){
                btnsub=l;
                var origSubmit=btnsub.onclick;
                btnsub.onclick=function(){
                    if (pwdbox.value.length > pw){
                        pwdbox.value="";
                        pw=0;
                        setNotice("Pasted or auto-completed password detected.");
                    } else {
                        origSubmit();
                    }
                }
                break;
            } // if submit
        } // if l
    } // next
}








setOnLoad(setCredits);
setOnLoad(badBrowser);









