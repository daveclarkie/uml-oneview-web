<%@ WebHandler Language="VB" Class="function_manage_cca_selected_analysis_dataconsumptionmeters" %>
Public Class function_manage_cca_selected_analysis_dataconsumptionmeters
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse
    Dim CO As SqlCommand
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim msg As String = ""
        Dim dt As DataTable
        Dim dr As DataRow
                
        Dim agreement_pk As Integer
        Dim method As Integer
        
             
        
        Try
            agreement_pk = RQ("b").ToString.Trim()
        Catch ex As Exception
            agreement_pk = -1
        End Try
        Try
            method = RQ("c").ToString.Trim()
        Catch ex As Exception
            method = -1
        End Try

        Try
          
            If method = 0 Then
          
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_cca_selected_analysis_dataconsumptionmeters"
                CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
                CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                    
                    msg &= "<li id='AcMeter_" & dr("dataconsumptionanalysis_pk") & "'>"
                    msg &= "<a href ='javascript:Remove(""AcMeter_" & dr("dataconsumptionanalysis_pk") & """," & dr("dataconsumptionanalysis_pk") & "," & dr("agreement_fk") & "," & dr("user_fk") & ",""" & dr("meter") & """,""" & dr("meterpoint_fk") & """);'>"
                    msg &= "<img src='/design/images/icon_remove.png' alt='remove' class='manageGroup_Icon'  />"
                    msg &= "   "
                    msg &= dr("meter")
                    msg &= "</a>"
                    msg &= "</li>"
                    
                Next
            ElseIf method = 1 Then
            
            
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_cca_available_analysis_dataconsumptionmeters"
                CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
                CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                  
                    msg &= "<li id='AvMeter_" & dr("agreementmeterpoint_pk") & "'>"
                    msg &= "<a href ='javascript:Add(""AvMeter_" & dr("agreementmeterpoint_pk") & """," & dr("agreement_fk") & "," & dr("user_fk") & ",""" & dr("meter") & """,""" & dr("meterpoint_fk") & """);'>"
                    msg &= "<img src='/design/images/icon_add.png' alt='add' class='manageGroup_Icon'  />"
                    msg &= "   "
                    msg &= dr("meter")
                    msg &= "</a>"
                    msg &= "</li>"
                    
                Next
                
            ElseIf method = 2 Then
            
            
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_agreementtargets_agreement"
                CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                  
                    msg &= "<li id='SetDate_" & dr("agreementtarget_pk") & "'>"
                    msg &= "<a href ='javascript:SetDate(""SetDate_" & dr("agreementtarget_pk") & """,""01 " & dr("targetstart") & """,""01 " & dr("targetend") & """);'>"
                    msg &= "<img src='/design/images/icon_add.png' alt='add' class='manageGroup_Icon'  />"
                    msg &= "   "
                    msg &= "(" & dr("targetstart") & " to " & dr("targetend") & ") - " & dr("targetdescription")
                    msg &= "</a>"
                    msg &= "</li>"
                    
                Next
                'TargetsAnalysisList
                 
                
            End If
                
        Catch LE As Exception
            msg = "FAIL:" & LE.Message
        End Try
        Response.Write(msg)
        
    End Sub

End Class