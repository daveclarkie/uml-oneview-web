<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreements_ctl.ascx.vb" Inherits="agreements_ctl" %>
<h5>agreements</h5>
<ul class='formcontrol'>
<li runat="server" id="blkagreementstage_fk">
<span class='label'>Agreement Stage</span>
<asp:DropDownList ID="agreementstage_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation<span class='pop'>Name of the company in Contract Manager that the site belongs to? (for meter filtering purposes)</span></span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementname">
<span class='label'>Agreement Name<span class='pop'>Name you wish to use for the agreement for reporting purposes</span></span>
<asp:TextBox EnableViewState="false" ID="agreementname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfederation_fk">
<span class='label'>Federation<span class='pop'>This is the Federation the agreement belongs to</span></span>
<asp:DropDownList ID="federation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementnumber">
<span class='label'>Agreement Number</span>
<asp:TextBox EnableViewState="false" ID="agreementnumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfacilitynumber">
<span class='label'>Facility Number</span>
<asp:TextBox EnableViewState="false" ID="facilitynumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktargetunitidentifier">
<span class='label'>TUID<span class='pop'>This is the Target Unit Identifier</span></span>
<asp:TextBox EnableViewState="false" ID="targetunitidentifier" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkbaseyearstart">
<span class='label'>Base Year Start</span>
<asp:TextBox EnableViewState="false" ID="baseyearstart" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkbaseyearend">
<span class='label'>Base Year End</span>
<asp:TextBox EnableViewState="false" ID="baseyearend" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkagreementtype_fk">
<span class='label'>Agreement Type</span>
<asp:DropDownList ID="agreementtype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementmeasure_fk">
<span class='label'>Agreement Measure</span>
<asp:DropDownList ID="agreementmeasure_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreement_fk">
<span class='label'>Original Agreement</span>
<asp:DropDownList ID="agreement_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksubmeterinstalled">
<span class='label'>Failed 70 / 30 ?<span class='pop'>Has the agreement failed the 70/30?</span></span>
<asp:CheckBox ID="submeterinstalled" runat="server" cssClass="input_chk" />
</li>
<li runat="server" id="blknotes">
<span class='label'>Notes<span class='pop'>Any notes you need to add to this agreement.</span></span>
<asp:TextBox EnableViewState="false" ID="notes" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkagreementstart">
<span class='label'>Agreement Start Date</span>
<asp:TextBox EnableViewState="false" ID="agreementstart" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkincludecrcsaving">
<span class='label'>Include CRC Savings<span class='pop'>Tick this box to include the CRC exemption savings for CCAs that would otherwise be reporting its energy in CRC. Note, the savings refer only to the energy within the CCA boundary and not total organisational CRC savings.</span></span>
<asp:CheckBox ID="includecrcsaving" runat="server" cssClass="input_chk" />
</li>
<li runat="server" id="blktechnicaluser_fk">
<span class='label'>Technical Analyst</span>
<asp:DropDownList ID="technicaluser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkreportinguser_fk">
<span class='label'>Reporting Analyst</span>
<asp:DropDownList ID="reportinguser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkaccountmanageruser_fk">
<span class='label'>Account Manager</span>
<asp:DropDownList ID="accountmanageruser_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

