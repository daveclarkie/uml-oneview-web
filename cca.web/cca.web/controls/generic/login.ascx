<%@ Control Language="VB" AutoEventWireup="false" CodeFile="login.ascx.vb" Inherits="mainctls_login" %>

<div class="login-page-body">
    <div class="login-page">
            <img src="../../design/images/logo_live.png"  />
                    <br />
                    <br />
        <div class="form">

            <ul class="login-form">
                <li>
                    <span class="label">User Name</span>
                    <asp:TextBox ID="username" CssClass="input_str" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="username" runat="server" ErrorMessage="*" CssClass="error" />
                </li>
                <li>
                    <span class="label">Password</span>
                    <asp:TextBox ID="password" CssClass="input_pwd" runat="server" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="password" runat="server" ErrorMessage="*" CssClass="error" />
                </li>
                <li>
                    <span class="label">&nbsp;</span>
                    <asp:Button ID="btnLogin" runat="server" Text="Login" />
                </li>
                <li>
                    <span class="label">&nbsp;</span><asp:Label ID="lblMessage" runat="server" Text="" CssClass="error" />&nbsp;
                </li>
                <li>
                    Can't remember your login details? <a href="./logindetails.aspx">Click here</a>
                </li>
            </ul>
        </div>
    </div>
    <asp:Label ID="lblversion" runat="server" Text="" CssClass="error" />
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script  src="js/index.js"></script>