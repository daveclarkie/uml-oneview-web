Partial Class bubbles_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents obubbles As Objects.bubbles
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_federations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.federation_fk.Items.Count=0 Then
        Me.federation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.federation_fk.DataTextField = "value"
        Me.federation_fk.DataValueField = "pk"
        Try
            Me.federation_fk.DataBind
        Catch Ex as Exception
            Me.federation_fk.SelectedValue=-1
            Me.federation_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_agreementtypes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.agreementtype_fk.Items.Count=0 Then
        Me.agreementtype_fk.DataSource=CDC.ReadDataTable(CO)
        Me.agreementtype_fk.DataTextField = "value"
        Me.agreementtype_fk.DataValueField = "pk"
        Try
            Me.agreementtype_fk.DataBind
        Catch Ex as Exception
            Me.agreementtype_fk.SelectedValue=-1
            Me.agreementtype_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_agreementmeasures_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.agreementmeasure_fk.Items.Count=0 Then
        Me.agreementmeasure_fk.DataSource=CDC.ReadDataTable(CO)
        Me.agreementmeasure_fk.DataTextField = "value"
        Me.agreementmeasure_fk.DataValueField = "pk"
        Try
            Me.agreementmeasure_fk.DataBind
        Catch Ex as Exception
            Me.agreementmeasure_fk.SelectedValue=-1
            Me.agreementmeasure_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_bubbles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.bubble_fk.Items.Count=0 Then
        Me.bubble_fk.DataSource=CDC.ReadDataTable(CO)
        Me.bubble_fk.DataTextField = "value"
        Me.bubble_fk.DataValueField = "pk"
        Try
            Me.bubble_fk.DataBind
        Catch Ex as Exception
            Me.bubble_fk.SelectedValue=-1
            Me.bubble_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If obubbles is Nothing then
obubbles = new Objects.bubbles(currentuser,actualuser,pk,CDC)
End If
Me.organisation_fk.SelectedValue=obubbles.organisation_fk
Me.bubblename.Text=obubbles.bubblename
Me.federation_fk.SelectedValue=obubbles.federation_fk
Me.targetunitidentifier.Text=obubbles.targetunitidentifier
Me.agreementtype_fk.SelectedValue=obubbles.agreementtype_fk
Me.agreementmeasure_fk.SelectedValue=obubbles.agreementmeasure_fk
Me.bubble_fk.SelectedValue=obubbles.bubble_fk
Me.includecrcsaving.Checked=obubbles.includecrcsaving
_CurrentPk=obubbles.bubble_pk
Status=obubbles.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If obubbles is Nothing then
obubbles = new Objects.bubbles(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
obubbles.organisation_fk=Me.organisation_fk.SelectedValue
obubbles.bubblename=Me.bubblename.Text
obubbles.federation_fk=Me.federation_fk.SelectedValue
obubbles.targetunitidentifier=Me.targetunitidentifier.Text
obubbles.agreementtype_fk=Me.agreementtype_fk.SelectedValue
obubbles.agreementmeasure_fk=Me.agreementmeasure_fk.SelectedValue
obubbles.bubble_fk=Me.bubble_fk.SelectedValue
obubbles.includecrcsaving=Me.includecrcsaving.Checked
result=obubbles.Save()
if not result then throw obubbles.LastError
_CurrentPk=obubbles.bubble_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=obubbles.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbles is Nothing then
   obubbles = new Objects.bubbles(currentuser,actualuser,pk,CDC)
  End If
  obubbles.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbles is Nothing then
   obubbles = new Objects.bubbles(currentuser,actualuser,pk,CDC)
  End If
  obubbles.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbles is Nothing then
   obubbles = new Objects.bubbles(currentuser,pk,CDC)
  End If
  obubbles.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  Me.bubblename.Enabled=IIF(WL>=0,True,False)
  Me.blkbubblename.Visible=IIF(RL>=0,True,False)
  Me.federation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkfederation_fk.Visible=IIF(RL>=0,True,False)
  Me.targetunitidentifier.Enabled=IIF(WL>=0,True,False)
  Me.blktargetunitidentifier.Visible=IIF(RL>=0,True,False)
  Me.agreementtype_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkagreementtype_fk.Visible=IIF(RL>=0,True,False)
  Me.agreementmeasure_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkagreementmeasure_fk.Visible=IIF(RL>=0,True,False)
  Me.bubble_fk.Enabled=IIF(WL>=1,True,False)
  Me.blkbubble_fk.Visible=IIF(RL>=1,True,False)
  Me.includecrcsaving.Enabled=IIF(WL>=0,True,False)
  Me.blkincludecrcsaving.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

