<%@ Control Language="VB" AutoEventWireup="false" CodeFile="custom_appointments_visit_ctl.ascx.vb" Inherits="custom_appointments_visit_ctl" %>


<h5>appointments</h5>
<ul class='formcontrol'>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation<span class='pop'>Name of the Client</span></span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksite_fk">
<span class='label'>Site Name<span class='pop'>If the appointment is regarding a specific site.</span></span>
<asp:DropDownList ID="site_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkappointmenttype_fk">
<span class='label'>Appointment Type<span class='pop'>The type of appointment you wish to make.</span></span>
<asp:DropDownList ID="appointmenttype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkappointment">
<span class='label'>When<span class='pop'>When do you want to make the appointment for?</span></span>
<asp:TextBox EnableViewState="false" ID="appointment" runat="server" cssClass="datepicker" />
<asp:TextBox EnableViewState="false" ID="appointmenttime" runat="server" cssClass="timepicker" />
</li>
<li runat="server" id="blklocation_fk">
<span class='label'>Address</span>
<asp:DropDownList ID="location_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkappointmentstatus_fk">
<span class='label'>Status<span class='pop'>The current status of the appointment.</span></span>
<asp:DropDownList ID="appointmentstatus_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkappointmentpurpose_fk">
<span class='label'>appointmentpurpose_fk</span>
<asp:DropDownList ID="appointmentpurpose_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkappointmentpurposeother">
<span class='label'>appointmentpurposeother</span>
<asp:TextBox EnableViewState="false" ID="appointmentpurposeother" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
</ul>

