<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreementfuels_ctl.ascx.vb" Inherits="agreementfuels_ctl" %>
<h5>Add Fuel to the Agreement</h5>
<ul class='formcontrol'>
<li runat="server" id="blkagreement_fk">
<span class='label'>Agreement</span>
<asp:DropDownList ID="agreement_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkfuel_fk">
<span class='label'>Fuel</span>
<asp:DropDownList ID="fuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkeligibility">
<span class='label'>Eligibility<span class='pop'>This is the eligibilty of the fuel for the agreement for the period specified</span></span>
<asp:TextBox EnableViewState="false" ID="eligibility" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkvalidfrom">
<span class='label'>Valid From</span>
<asp:TextBox EnableViewState="false" ID="validfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkvalidto">
<span class='label'>Valid To</span>
<asp:TextBox EnableViewState="false" ID="validto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkrelief">
<span class='label'>Relief<span class='pop'>This is the relief that will actually be applied for the fuel for the period specified</span></span>
<asp:TextBox EnableViewState="false" ID="relief" runat="server" cssClass="input_dbl" />
</li>
</ul>

