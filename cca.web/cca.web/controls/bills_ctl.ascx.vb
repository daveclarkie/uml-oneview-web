Partial Class bills_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents obills As Objects.bills
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_sites_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.site_fk.Items.Count=0 Then
        Me.site_fk.DataSource=CDC.ReadDataTable(CO)
        Me.site_fk.DataTextField = "value"
        Me.site_fk.DataValueField = "pk"
        Try
            Me.site_fk.DataBind
        Catch Ex as Exception
            Me.site_fk.SelectedValue=-1
            Me.site_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If obills is Nothing then
obills = new Objects.bills(currentuser,actualuser,pk,CDC)
End If
Me.fueltype.Text=obills.fueltype
Me.site_fk.SelectedValue=obills.site_fk
Me.billtype.Text=obills.billtype
Me.billedfrom.Text=obills.billedfrom.ToString("dd MMM yyyy")
If Me.billedfrom.Text="01 Jan 0001" then Me.billedfrom.Text=""
Me.billedfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.billedfrom.Attributes.Add("onBlur", "clearNotice()")
Me.billedto.Text=obills.billedto.ToString("dd MMM yyyy")
If Me.billedto.Text="01 Jan 0001" then Me.billedto.Text=""
Me.billedto.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.billedto.Attributes.Add("onBlur", "clearNotice()")
Me.addedby.Text=obills.addedby
_CurrentPk=obills.bill_pk
Status=obills.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If obills is Nothing then
obills = new Objects.bills(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
obills.fueltype=Me.fueltype.Text
obills.site_fk=Me.site_fk.SelectedValue
obills.billtype=Me.billtype.Text
obills.billedfrom=CommonFN.CheckEmptyDate(Me.billedfrom.Text)
obills.billedto=CommonFN.CheckEmptyDate(Me.billedto.Text)
obills.addedby=Me.addedby.Text
result=obills.Save()
if not result then throw obills.LastError
_CurrentPk=obills.bill_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=obills.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obills is Nothing then
   obills = new Objects.bills(currentuser,actualuser,pk,CDC)
  End If
  obills.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obills is Nothing then
   obills = new Objects.bills(currentuser,actualuser,pk,CDC)
  End If
  obills.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obills is Nothing then
   obills = new Objects.bills(currentuser,pk,CDC)
  End If
  obills.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.fueltype.Enabled=IIF(WL>=0,True,False)
  Me.blkfueltype.Visible=IIF(RL>=0,True,False)
  Me.site_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksite_fk.Visible=IIF(RL>=0,True,False)
  Me.billtype.Enabled=IIF(WL>=0,True,False)
  Me.blkbilltype.Visible=IIF(RL>=0,True,False)
  Me.billedfrom.Enabled=IIF(WL>=0,True,False)
  Me.blkbilledfrom.Visible=IIF(RL>=0,True,False)
  Me.billedto.Enabled=IIF(WL>=0,True,False)
  Me.blkbilledto.Visible=IIF(RL>=0,True,False)
  Me.addedby.Enabled=IIF(WL>=0,True,False)
  Me.blkaddedby.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

