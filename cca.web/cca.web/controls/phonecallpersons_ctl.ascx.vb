Partial Class phonecallpersons_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ophonecallpersons As Objects.phonecallpersons
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_phonecalls_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.phonecall_fk.Items.Count=0 Then
        Me.phonecall_fk.DataSource=CDC.ReadDataTable(CO)
        Me.phonecall_fk.DataTextField = "value"
        Me.phonecall_fk.DataValueField = "pk"
        Try
            Me.phonecall_fk.DataBind
        Catch Ex as Exception
            Me.phonecall_fk.SelectedValue=-1
            Me.phonecall_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_persons_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.person_fk.Items.Count=0 Then
        Me.person_fk.DataSource=CDC.ReadDataTable(CO)
        Me.person_fk.DataTextField = "value"
        Me.person_fk.DataValueField = "pk"
        Try
            Me.person_fk.DataBind
        Catch Ex as Exception
            Me.person_fk.SelectedValue=-1
            Me.person_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ophonecallpersons is Nothing then
ophonecallpersons = new Objects.phonecallpersons(currentuser,actualuser,pk,CDC)
End If
Me.phonecall_fk.SelectedValue=ophonecallpersons.phonecall_fk
Me.person_fk.SelectedValue=ophonecallpersons.person_fk
_CurrentPk=ophonecallpersons.phonecallperson_pk
Status=ophonecallpersons.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ophonecallpersons is Nothing then
ophonecallpersons = new Objects.phonecallpersons(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ophonecallpersons.phonecall_fk=Me.phonecall_fk.SelectedValue
ophonecallpersons.person_fk=Me.person_fk.SelectedValue
result=ophonecallpersons.Save()
if not result then throw ophonecallpersons.LastError
_CurrentPk=ophonecallpersons.phonecallperson_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ophonecallpersons.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallpersons is Nothing then
   ophonecallpersons = new Objects.phonecallpersons(currentuser,actualuser,pk,CDC)
  End If
  ophonecallpersons.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallpersons is Nothing then
   ophonecallpersons = new Objects.phonecallpersons(currentuser,actualuser,pk,CDC)
  End If
  ophonecallpersons.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallpersons is Nothing then
   ophonecallpersons = new Objects.phonecallpersons(currentuser,pk,CDC)
  End If
  ophonecallpersons.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.phonecall_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkphonecall_fk.Visible=IIF(RL>=0,True,False)
  Me.person_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkperson_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

