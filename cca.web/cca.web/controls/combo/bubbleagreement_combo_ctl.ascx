<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="bubbleagreement_combo_ctl.ascx.vb" Inherits="bubbleagreement_combo_ctl" %>
<h5>Detail</h5>
<ul class='formcontrol'>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation<span class='pop'>Name of the company in Contract Manager that the site belongs to? (for meter filtering purposes)</span></span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementname">
<span class='label'>Agreement Name<span class='pop'>Name you wish to use for the agreement for reporting purposes</span></span>
<asp:TextBox EnableViewState="false" ID="agreementname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfederation_fk">
<span class='label'>Federation<span class='pop'>This is the Federation the agreement belongs to</span></span>
<asp:DropDownList ID="federation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementnumber">
<span class='label'>Agreement Number</span>
<asp:TextBox EnableViewState="false" ID="agreementnumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfacilitynumber">
<span class='label'>Facility Number</span>
<asp:TextBox EnableViewState="false" ID="facilitynumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktargetunitidentifier">
<span class='label'>TUID<span class='pop'>This is the Target Unit Identifier</span></span>
<asp:TextBox EnableViewState="false" ID="targetunitidentifier" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkbaseyearstart">
<span class='label'>Base Year Start</span>
<asp:TextBox EnableViewState="false" ID="baseyearstart" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkbaseyearend">
<span class='label'>Base Year End</span>
<asp:TextBox EnableViewState="false" ID="baseyearend" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkagreementtype_fk">
<span class='label'>Agreement Type</span>
<asp:DropDownList ID="agreementtype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementmeasure_fk">
<span class='label'>Agreement Measure</span>
<asp:DropDownList ID="agreementmeasure_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksubmeterinstalled">
<span class='label'>Sub Meters<span class='pop'>Are there sub meters installed on the agreement?  This is used when working out the waited average for the taper</span></span>
<asp:CheckBox ID="submeterinstalled" runat="server" cssClass="input_chk" />
</li>
</ul>
