
Partial Class functions_actusr
    Inherits DavePage : Public PSM As MySM = SM


    Dim AllowAction As Boolean = True
    Dim AccDen As Boolean = False
    Dim AccAct As Boolean = False
    Dim AccDel As Boolean = False
    Dim BadData As Boolean = False

    Dim u As Integer = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Response.Buffer = True

        Try
            AllowAction = AllowAction Or Integer.TryParse(Request.QueryString("u"), u)
            If Not (u > 0) Then
                AllowAction = False
                BadData = True
            Else
                AllowAction = AllowAction Or Security.ReferalCheck(Request)
                AllowAction = AllowAction Or Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC)
                If AllowAction = False Then AccDen = True
            End If

            Dim CO As New SqlClient.SqlCommand

            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "[rsp_userstatus]"
            CO.Parameters.AddWithValue("@user", u)
            Dim userstatus As Integer = -1
            CDC.ReadScalarValue(userstatus, CO)

            Select Case userstatus
                Case 0
                    AccAct = True
                    AllowAction = False
                Case 2
                    AccDel = True
                    AllowAction = False
            End Select
            If AllowAction Then
                Dim result As Boolean = False
                Dim log As String = ""
                Dim user_pk As Integer = 0
                Dim userfound As Boolean = False
                Dim dr As DataRow

                Dim newpassword As String = ""

                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_NewPassword"
                CO.Parameters.Clear()
                CDC.ReadScalarValue(newpassword, CO)

                Dim user As New users(SM.currentuser, SM.actualuser, u, CDC)

                CO.CommandText = "rsp_Enc"
                CO.Parameters.AddWithValue("@value", newpassword)
                CDC.ReadScalarValue(user.encpassword, CO)
                user.rowstatus = 0
                userfound = user.Save()

                Dim TargetAccounts As String = ""
                Dim cSmtp As New SMTP

                CO.CommandText = "[rsp_useremails_forsystemmail]"
                CO.Parameters.Clear()
                CO.Parameters.AddWithValue("@user_pk", u)
                Dim dt As DataTable = CDC.ReadDataTable(CO)

                For Each dr In dt.Rows
                    TargetAccounts &= dr("email")
                    TargetAccounts &= ","
                Next

                If TargetAccounts.Length > 1 Then
                    TargetAccounts = TargetAccounts.Trim(",".ToCharArray)
                End If
                cSmtp.MsgTo = TargetAccounts.Split(",".ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
                ' "Your password for ISA / SMDExchange ( http://" & Request.ServerVariables("HTTP_HOST") & "/ ) has been reset to: " & newpassword

                Dim msg As String = ""
                msg &= "Your Schneider Electric ( http://" & Request.ServerVariables("HTTP_HOST") & "/ ) account has been activated." & vbCrLf

                If Request.ServerVariables("HTTP_HOST").StartsWith("test.smd") Then
                    msg &= vbCrLf
                    msg &= ""
                    msg &= vbCrLf
                End If


                Dim emailpk As Integer = 0

                CO.CommandText = "rsp_primaryemail"
                Dim edr As DataRow = CDC.ReadDataRow(CO)
                emailpk = edr("email_pk")
                edr = Nothing

                Dim em As New emails(SM.currentuser, SM.actualuser, emailpk, CDC)

                msg &= "You can now log in using:" & vbCrLf
                msg &= "Username: " & em.email & vbCrLf
                msg &= "Password: " & newpassword & vbCrLf


                msg &= vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & "This is an automated message from an account that is unable to receive email. This email account is not associated with a human being. Replying to this message will result in the mail server refusing to deliver your response."

                cSmtp.TextBodyContent = msg

                'cSmtp.UserDomain = "systems.domain.co.uk"
                'cSmtp.User = "noreply@domain.co.uk"
                'cSmtp.Port = 25
                'cSmtp.Pass = "$y5t3ms!"
                'cSmtp.Subject = "ISA Systems Message: Account Activation"
                'cSmtp.Server = "mail.domain.co.uk"
                Try
                    result = cSmtp.SMTPSend()
                Catch ex As Exception
                    log = cSmtp.Log
                End Try
                cSmtp.Dispose()
                If Not result Then
                    Response.Write("Fail:Unable to send email notification:" & log)
                Else
                    Response.Write("OK:" & u)
                End If
            Else
                If AccDen Then
                    Response.Write("Fail:Account was not modified.You do not have the required access rights.")
                ElseIf BadData Then
                    Response.Write("Fail:Account was not modified. Invalid data was supplied.")
                ElseIf AccDel Then
                    Response.Write("Fail:Account was not modified. This account has been deleted.")
                ElseIf AccAct Then
                    Response.Write("Fail:Account was not modified. This account is already active.")
                End If
            End If
        Catch Ex As Exception
            Response.Write("Fail:" & Ex.Message)
        End Try

        Try
            Response.Flush()
            Response.End()
        Catch ex As Exception
        End Try


    End Sub
End Class
