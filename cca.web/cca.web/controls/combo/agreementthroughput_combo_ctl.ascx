<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="agreementthroughput_combo_ctl.ascx.vb" Inherits="agreementthroughput_combo_ctl" %>
<h5>Detail</h5>
<ul class='formcontrol'>
<li runat="server" id="blkthroughputname">
<span class='label'>Name</span>
<asp:TextBox EnableViewState="false" ID="throughputname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkunit_fk">
<span class='label'>Unit</span>
<asp:DropDownList ID="unit_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkdatafrequency_fk">
<span class='label'>Data Frequency<span class='pop'>As a guide, how often should the data be received</span></span>
<asp:DropDownList ID="datafrequency_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>
