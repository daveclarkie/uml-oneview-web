<%@ Control Language="VB" AutoEventWireup="false" CodeFile="locations_ctl.ascx.vb" Inherits="locations_ctl" %>
<h5>locations</h5>
<ul class='formcontrol'>
<li runat="server" id="blkbuildingprefix">
<span class='label'>Building Prefix</span>
<asp:TextBox EnableViewState="false" ID="buildingprefix" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkstreetnumber">
<span class='label'>Street Number</span>
<asp:TextBox EnableViewState="false" ID="streetnumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkstreet">
<span class='label'>Street Name</span>
<asp:TextBox EnableViewState="false" ID="street" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkdistrict">
<span class='label'>District</span>
<asp:TextBox EnableViewState="false" ID="district" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktown">
<span class='label'>Town</span>
<asp:TextBox EnableViewState="false" ID="town" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcounty_fk">
<span class='label'>County</span>
<asp:DropDownList ID="county_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkcountry_fk">
<span class='label'>Country</span>
<asp:DropDownList ID="country_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkpostcode">
<span class='label'>Post Code</span>
<asp:TextBox EnableViewState="false" ID="postcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blklongitude">
<span class='label'>Longitude</span>
<asp:TextBox EnableViewState="false" ID="longitude" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blklatitude">
<span class='label'>Latitude</span>
<asp:TextBox EnableViewState="false" ID="latitude" runat="server" cssClass="input_dbl" />
</li>
</ul>

