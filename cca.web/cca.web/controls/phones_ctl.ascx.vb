Partial Class phones_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ophones As Objects.phones
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_countrys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.country_fk.Items.Count=0 Then
        Me.country_fk.DataSource=CDC.ReadDataTable(CO)
        Me.country_fk.DataTextField = "value"
        Me.country_fk.DataValueField = "pk"
        Try
            Me.country_fk.DataBind
        Catch Ex as Exception
            Me.country_fk.SelectedValue=-1
            Me.country_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ophones is Nothing then
ophones = new Objects.phones(currentuser,actualuser,pk,CDC)
End If
Me.country_fk.SelectedValue=ophones.country_fk
Me.phonenumber.Text=ophones.phonenumber
_CurrentPk=ophones.phone_pk
Status=ophones.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ophones is Nothing then
ophones = new Objects.phones(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ophones.country_fk=Me.country_fk.SelectedValue
ophones.phonenumber=Me.phonenumber.Text
result=ophones.Save()
if not result then throw ophones.LastError
_CurrentPk=ophones.phone_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ophones.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophones is Nothing then
   ophones = new Objects.phones(currentuser,actualuser,pk,CDC)
  End If
  ophones.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophones is Nothing then
   ophones = new Objects.phones(currentuser,actualuser,pk,CDC)
  End If
  ophones.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophones is Nothing then
   ophones = new Objects.phones(currentuser,pk,CDC)
  End If
  ophones.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.country_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkcountry_fk.Visible=IIF(RL>=0,True,False)
  Me.phonenumber.Enabled=IIF(WL>=0,True,False)
  Me.blkphonenumber.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

