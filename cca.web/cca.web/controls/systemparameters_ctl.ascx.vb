Partial Class systemparameters_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osystemparameters As Objects.systemparameters
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osystemparameters is Nothing then
osystemparameters = new Objects.systemparameters(currentuser,actualuser,pk,CDC)
End If
Me.customerlocation.Text=osystemparameters.customerlocation
Me.ssrsdomain.Text=osystemparameters.ssrsdomain
Me.ssrsusername.Text=osystemparameters.ssrsusername
Me.ssrspassword.Text=osystemparameters.ssrspassword
Me.ssrsurl.Text=osystemparameters.ssrsurl
Me.importfolder.Text=osystemparameters.importfolder
Me.matriximportfolder.Text=osystemparameters.matriximportfolder
Me.datalocation.Text=osystemparameters.datalocation
Me.smtpserver.Text=osystemparameters.smtpserver
Me.helpdeskurl.Text=osystemparameters.helpdeskurl
Me.applicationdirectory.Text=osystemparameters.applicationdirectory
Me.intervaldatadirectory.Text=osystemparameters.intervaldatadirectory
Me.tyrrelldatadirectory.Text=osystemparameters.tyrrelldatadirectory
Me.helpdeskRESTAPIurl.Text=osystemparameters.helpdeskRESTAPIurl
Me.helpdeskRESTAPIkey.Text=osystemparameters.helpdeskRESTAPIkey
_CurrentPk=osystemparameters.systemparameter_pk
Status=osystemparameters.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osystemparameters is Nothing then
osystemparameters = new Objects.systemparameters(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osystemparameters.customerlocation=Me.customerlocation.Text
osystemparameters.ssrsdomain=Me.ssrsdomain.Text
osystemparameters.ssrsusername=Me.ssrsusername.Text
osystemparameters.ssrspassword=Me.ssrspassword.Text
osystemparameters.ssrsurl=Me.ssrsurl.Text
osystemparameters.importfolder=Me.importfolder.Text
osystemparameters.matriximportfolder=Me.matriximportfolder.Text
osystemparameters.datalocation=Me.datalocation.Text
osystemparameters.smtpserver=Me.smtpserver.Text
osystemparameters.helpdeskurl=Me.helpdeskurl.Text
osystemparameters.applicationdirectory=Me.applicationdirectory.Text
osystemparameters.intervaldatadirectory=Me.intervaldatadirectory.Text
osystemparameters.tyrrelldatadirectory=Me.tyrrelldatadirectory.Text
osystemparameters.helpdeskRESTAPIurl=Me.helpdeskRESTAPIurl.Text
osystemparameters.helpdeskRESTAPIkey=Me.helpdeskRESTAPIkey.Text
result=osystemparameters.Save()
if not result then throw osystemparameters.LastError
_CurrentPk=osystemparameters.systemparameter_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osystemparameters.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osystemparameters is Nothing then
   osystemparameters = new Objects.systemparameters(currentuser,actualuser,pk,CDC)
  End If
  osystemparameters.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osystemparameters is Nothing then
   osystemparameters = new Objects.systemparameters(currentuser,actualuser,pk,CDC)
  End If
  osystemparameters.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osystemparameters is Nothing then
   osystemparameters = new Objects.systemparameters(currentuser,pk,CDC)
  End If
  osystemparameters.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.customerlocation.Enabled=IIF(WL>=0,True,False)
  Me.blkcustomerlocation.Visible=IIF(RL>=0,True,False)
  Me.ssrsdomain.Enabled=IIF(WL>=0,True,False)
  Me.blkssrsdomain.Visible=IIF(RL>=0,True,False)
  Me.ssrsusername.Enabled=IIF(WL>=0,True,False)
  Me.blkssrsusername.Visible=IIF(RL>=0,True,False)
  Me.ssrspassword.Enabled=IIF(WL>=0,True,False)
  Me.blkssrspassword.Visible=IIF(RL>=0,True,False)
  Me.ssrsurl.Enabled=IIF(WL>=0,True,False)
  Me.blkssrsurl.Visible=IIF(RL>=0,True,False)
  Me.importfolder.Enabled=IIF(WL>=0,True,False)
  Me.blkimportfolder.Visible=IIF(RL>=0,True,False)
  Me.matriximportfolder.Enabled=IIF(WL>=0,True,False)
  Me.blkmatriximportfolder.Visible=IIF(RL>=0,True,False)
  Me.datalocation.Enabled=IIF(WL>=0,True,False)
  Me.blkdatalocation.Visible=IIF(RL>=0,True,False)
  Me.smtpserver.Enabled=IIF(WL>=0,True,False)
  Me.blksmtpserver.Visible=IIF(RL>=0,True,False)
  Me.helpdeskurl.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdeskurl.Visible=IIF(RL>=0,True,False)
  Me.applicationdirectory.Enabled=IIF(WL>=0,True,False)
  Me.blkapplicationdirectory.Visible=IIF(RL>=0,True,False)
  Me.intervaldatadirectory.Enabled=IIF(WL>=0,True,False)
  Me.blkintervaldatadirectory.Visible=IIF(RL>=0,True,False)
  Me.tyrrelldatadirectory.Enabled=IIF(WL>=0,True,False)
  Me.blktyrrelldatadirectory.Visible=IIF(RL>=0,True,False)
  Me.helpdeskRESTAPIurl.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdeskRESTAPIurl.Visible=IIF(RL>=0,True,False)
  Me.helpdeskRESTAPIkey.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdeskRESTAPIkey.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

