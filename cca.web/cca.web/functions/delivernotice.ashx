<%@ WebHandler Language="VB" Class="function_delivernotice" %>
Public Class function_delivernotice
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse
    Dim CO As SqlCommand
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim msg As String = ""
        Dim dt As DataTable
        Dim dr As DataRow
                
        Dim npk As Integer
       
        
             
        
        Try
            npk = RQ("npk").ToString.Trim()
        Catch ex As Exception
            npk = -1
        End Try
    

        Try
                               
            Dim n As New notifications(SM.currentuser, SM.actualuser, npk, CDC)
            Dim nt As New notificationtargets(SM.currentuser, SM.actualuser, CDC)
            
            
            
            
            If n.approved = True Then
                
            
            
            
            
            
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = ""
                'CO.Parameters.AddWithValue("@npk", npk)
                dt = CDC.ReadDataTable(CO)
                For Each dr In dt.Rows
                    nt.notification_fk = npk
                    nt.user_fk = dr("user_fk")
                    nt.Save()     
                Next
            End If
                
        Catch LE As Exception
            msg = "FAIL:" & LE.Message
        End Try
        Response.Write(msg)
        
    End Sub

End Class