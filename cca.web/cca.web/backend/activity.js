﻿// JScript File

// loadusers

function loadusers(pk){
    user_pk=pk;
    clear();
    reqLogins(pk);
    reqClients(pk);
    reqSubs(pk);
    reqDocs(pk);
    reqSales(pk);
    reqFails(pk);
    reqSalesAndFails(pk);
}

function clearobject(objname){
    var clrobj=document.getElementById(objname);
    try
        { 
            while (clrobj.hasChildNodes)
            {
                clrobj.removeChild(clrobj.lastChild);
            }
        }
   catch (e) {
   } 
}

function clear(){
            clearobject("lstlogins");
            clearobject("lstclients");
            clearobject("lstsalesandfails");
            clearobject("lstsales");
            clearobject("lstfails");
            clearobject("lstdocs");
            clearobject("lstsubs");
}

var rqLogins;
function reqLogins(pk){
    rqLogins=newRequest();
    setCallback(rqLogins,setLogins);
    if ( typeof rqLogins != "boolean" ){
        rqLogins.open("GET","/functions/rpt_logins.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqLogins,setLogins);
        rqLogins.send();
    }
}
function setLogins(){
        if (rqLogins.readyState==4) {chgElementText(document.getElementById('lstlogins'),chgReplace,rqLogins.responseText);}
}

var rqClients;
function reqClients(pk){
    rqClients=newRequest();
    setCallback(rqClients,setClients);
    if ( typeof rqClients != "boolean" ){
        rqClients.open("GET","/functions/rpt_clientadditions.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqClients,setClients);
        rqClients.send();
    }
}
function setClients(){
        if (rqClients.readyState==4) {chgElementText(document.getElementById('lstclients'),chgReplace,rqClients.responseText);}
}

var rqSubs;
function reqSubs(pk){
    rqSubs=newRequest();
    setCallback(rqSubs,setSubs);
    if ( typeof rqSubs != "boolean" ){
        rqSubs.open("GET","/functions/rpt_submissions.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqSubs,setSubs);
        rqSubs.send();
    }
}
function setSubs(){
        if (rqSubs.readyState==4) {chgElementText(document.getElementById('lstsubs'),chgReplace,rqSubs.responseText);}
}

var rqDocs;
function reqDocs(pk){
    rqDocs=newRequest();
    setCallback(rqDocs,setDocs);
    if ( typeof rqDocs != "boolean" ){
        rqDocs.open("GET","/functions/rpt_proddocs.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqDocs,setDocs);
        rqDocs.send();
    }
}
function setDocs(){
        if (rqDocs.readyState==4) {chgElementText(document.getElementById('lstdocs'),chgReplace,rqDocs.responseText);}
}

var rqSales;
function reqSales(pk){
    rqSales=newRequest();
    setCallback(rqSales,setSales);
    if ( typeof rqSales != "boolean" ){
        rqSales.open("GET","/functions/rpt_soldsales.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqSales,setSales);
        rqSales.send();
    }
}
function setSales(){
        if (rqSales.readyState==4) {chgElementText(document.getElementById('lstsales'),chgReplace,rqSales.responseText);}
}

var rqFails;
function reqFails(pk){
    rqFails=newRequest();
    setCallback(rqFails,setFails);
    if ( typeof rqFails != "boolean" ){
        rqFails.open("GET","/functions/rpt_failsales.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqFails,setFails);
        rqFails.send();
    }
}
function setFails(){
        if (rqFails.readyState==4) {chgElementText(document.getElementById('lstfails'),chgReplace,rqFails.responseText);}
}

var rqSalesAndFails;
function reqSalesAndFails(pk){
    rqSalesAndFails=newRequest();
    setCallback(rqSalesAndFails,setSalesAndFails);
    if ( typeof rqSalesAndFails != "boolean" ){
        rqSalesAndFails.open("GET","/functions/rpt_failandsales.ashx?g=1&r="+pk+noCache(),false);
        setCallback(rqSalesAndFails,setSalesAndFails);
        rqSalesAndFails.send();
    }
}
function setSalesAndFails(){
        if (rqSalesAndFails.readyState==4) {chgElementText(document.getElementById('lstsalesandfails'),chgReplace,rqSalesAndFails.responseText);}
}

