Partial Class dataconsumptionanalysis_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents odataconsumptionanalysis As Objects.dataconsumptionanalysis
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_agreements_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.agreement_fk.Items.Count=0 Then
        Me.agreement_fk.DataSource=CDC.ReadDataTable(CO)
        Me.agreement_fk.DataTextField = "value"
        Me.agreement_fk.DataValueField = "pk"
        Try
            Me.agreement_fk.DataBind
        Catch Ex as Exception
            Me.agreement_fk.SelectedValue=-1
            Me.agreement_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If odataconsumptionanalysis is Nothing then
odataconsumptionanalysis = new Objects.dataconsumptionanalysis(currentuser,actualuser,pk,CDC)
End If
Me.meterpoint_fk.Text=odataconsumptionanalysis.meterpoint_fk
Me.agreement_fk.SelectedValue=odataconsumptionanalysis.agreement_fk
Me.user_fk.SelectedValue=odataconsumptionanalysis.user_fk
_CurrentPk=odataconsumptionanalysis.dataconsumptionanalysis_pk
Status=odataconsumptionanalysis.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If odataconsumptionanalysis is Nothing then
odataconsumptionanalysis = new Objects.dataconsumptionanalysis(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
odataconsumptionanalysis.meterpoint_fk=Me.meterpoint_fk.Text
odataconsumptionanalysis.agreement_fk=Me.agreement_fk.SelectedValue
odataconsumptionanalysis.user_fk=Me.user_fk.SelectedValue
result=odataconsumptionanalysis.Save()
if not result then throw odataconsumptionanalysis.LastError
_CurrentPk=odataconsumptionanalysis.dataconsumptionanalysis_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=odataconsumptionanalysis.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odataconsumptionanalysis is Nothing then
   odataconsumptionanalysis = new Objects.dataconsumptionanalysis(currentuser,actualuser,pk,CDC)
  End If
  odataconsumptionanalysis.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odataconsumptionanalysis is Nothing then
   odataconsumptionanalysis = new Objects.dataconsumptionanalysis(currentuser,actualuser,pk,CDC)
  End If
  odataconsumptionanalysis.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If odataconsumptionanalysis is Nothing then
   odataconsumptionanalysis = new Objects.dataconsumptionanalysis(currentuser,pk,CDC)
  End If
  odataconsumptionanalysis.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.meterpoint_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkmeterpoint_fk.Visible=IIF(RL>=0,True,False)
  Me.agreement_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkagreement_fk.Visible=IIF(RL>=0,True,False)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

