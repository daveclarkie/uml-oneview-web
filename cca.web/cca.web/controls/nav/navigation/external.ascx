<%@ Control Language="VB" AutoEventWireup="false" CodeFile="external.ascx.vb" Inherits="design_elements_navigation_external" %>
<h3>Navigation</h3>
<ul class="navigationmenu">
<li><a href="/login.aspx">Log In</a></li>
<%--<li><a href="/logindetails.aspx">Password Reset</a></li>--%>
<%  If allowedRemoteHost And isTestSystem Then%>
<li><asp:LinkButton ID="dgr1" runat="server" Text="Clarkie" CausesValidation="False" OnCommand="clickLogin" CommandArgument="1"  /></li>
<% End If %>

</ul>
<div class="warn">Unauthorised use of this system is prohibited under the Computer Misuse Act 1990. </div>
<asp:Label ID="lbl_hostpath" runat="server" Visible ="false"></asp:Label>