Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration

Partial Class corematrix_upload
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Dim dt As DataTable
    Dim _newfile As String = ""

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        CO = New SqlClient.SqlCommand("rkg_matrixtypes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.matrixtype_fk.Items.Count = 0 Then
            Me.matrixtype_fk.DataSource = CDC.ReadDataTable(CO)
            Me.matrixtype_fk.DataTextField = "value"
            Me.matrixtype_fk.DataValueField = "pk"
            Try
                Me.matrixtype_fk.DataBind()
            Catch Ex As Exception
                Me.matrixtype_fk.SelectedValue = -1
                Me.matrixtype_fk.DataBind()
            End Try
        End If
    End Sub
    Public Sub PageLoad()

    End Sub
    '================================
    '   "Spaghetti" Procs
    '================================


    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PageLoad()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        If FileUpload1.HasFile Then
            Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim FilePath As String = Path.GetDirectoryName(FileUpload1.PostedFile.FileName)
            Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim strnow As String = ""
            Dim filenameok As Boolean = False

            Do While Not filenameok
                strnow = Replace(Replace(Replace(Now.ToString, "/", ""), ":", ""), " ", "")
                _newfile = Server.MapPath("/corematrix/files/") + strnow + FileName
                If Not File.Exists(_newfile) Then filenameok = True
            Loop

            FileUpload1.SaveAs(_newfile)
            If File.Exists(_newfile) Then
                lblFileName.Text = FileName
                Panel2.Visible = True
                dt = CSVtoDataTable(Server.MapPath("/corematrix/files/"), strnow + FileName)

                Dim mf As New matrixfiles(SM.currentuser, SM.actualuser, CDC)
                mf.originalfilename = FileName
                mf.savedfilename = strnow + FileName
                If mf.Save Then
                    ImportRecords(dt, mf.matrixfile_pk)
                    System.IO.File.Move(_newfile, sys.matriximportfolder + strnow + FileName)
                End If

            End If
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs)
        Panel2.Visible = False
    End Sub
    Private Function CSVtoDataTable(ByVal filepath As String, ByVal filename As String) As DataTable
        Dim _return As DataTable = Nothing

        Dim connStr As String = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & filepath & ";Extended Properties=""Text;HDR=No;FMT=Delimited"""
        Dim Conn As New Odbc.OdbcConnection(connStr)
        Dim ds As DataSet
        Dim DataAdapter As New Odbc.OdbcDataAdapter("SELECT * FROM [" + filename + "]", Conn)
        ds = New DataSet

        Try
            DataAdapter.Fill(ds)
            _return = ds.Tables(0)
            ' Catch and display database errors
        Catch ex As Odbc.OdbcException
            Dim odbcError As Odbc.OdbcError
            For Each odbcError In ex.Errors
                AddNotice("setNotice('" + ex.Message + "');")
            Next
        End Try

        ' Cleanup
        Conn.Dispose()
        DataAdapter.Dispose()
        ds.Dispose()

        Return _return
    End Function

    Private Sub ImportRecords(ByVal table As DataTable, ByVal matrixfile_pk As Integer)

        Try
            Dim count As Integer = 0
            dt = table
            Dim m As Object
            Dim mt As matrixtypes
            Dim fileok As Boolean = True

            Select Case matrixtype_fk.SelectedItem.Value
                Case 1
                    If Not dt.Columns.Count = 12 Then 'Invalid File - Not enough columns
                        fileok = False
                        AddNotice("setError('Not enough columns in file');")
                    ElseIf Not dt.Columns.Item(0).ColumnName = "Supplier" Then
                        fileok = False
                        AddNotice("setError('Column 0: Supplier');")
                    ElseIf Not dt.Columns.Item(1).ColumnName = "ProductName" Then
                        fileok = False
                        AddNotice("setError('Column 1: ProductName');")
                    ElseIf Not dt.Columns.Item(2).ColumnName = "Region" Then
                        fileok = False
                        AddNotice("setError('Column 2: Region');")
                    ElseIf Not dt.Columns.Item(3).ColumnName = "Profile" Then
                        fileok = False
                        AddNotice("setError('Column 3: Profile');")
                    ElseIf Not dt.Columns.Item(4).ColumnName = "ContractLength" Then
                        fileok = False
                        AddNotice("setError('Column 4: ContractLength');")
                    ElseIf Not dt.Columns.Item(5).ColumnName = "validfrom" Then
                        fileok = False
                        AddNotice("setError('Column 5: validfrom');")
                    ElseIf Not dt.Columns.Item(6).ColumnName = "validto" Then
                        fileok = False
                        AddNotice("setError('Column 6: validto');")
                    ElseIf Not dt.Columns.Item(7).ColumnName = "Standing Charge" Then
                        fileok = False
                        AddNotice("setError('Column 7: Standing Charge');")
                    ElseIf Not dt.Columns.Item(8).ColumnName = "Single Rate" Then
                        fileok = False
                        AddNotice("setError('Column 8: Single Rate');")
                    ElseIf Not dt.Columns.Item(9).ColumnName = "Day Rate" Then
                        fileok = False
                        AddNotice("setError('Column 9: Day Rate');")
                    ElseIf Not dt.Columns.Item(10).ColumnName = "Night Rate" Then
                        fileok = False
                        AddNotice("setError('Column 10: Night Rate');")
                    ElseIf Not dt.Columns.Item(11).ColumnName = "Other Rate" Then
                        fileok = False
                        AddNotice("setError('Column 11: Other Rate');")
                    End If
                Case Else
                    fileok = False
                    AddNotice("setError('Type not been configured');")
            End Select
            If fileok Then
                For Each dr As DataRow In dt.Rows

                    Select Case matrixtype_fk.SelectedItem.Value
                        Case 1
                            Dim supplier_pk As Integer = -1
                            Dim profile_fk As Integer = -1

                            m = New matrix1sources(SM.currentuser, SM.actualuser, CDC)
                            mt = New matrixtypes(SM.currentuser, SM.actualuser, matrixtype_fk.SelectedItem.Value, CDC)
                            CO = New SqlCommand
                            CO.CommandType = CommandType.StoredProcedure
                            CO.CommandText = "rsp_suppliers_exists"
                            CO.Parameters.AddWithValue("@supplier_name", dr(0))
                            CDC.ReadScalarValue(supplier_pk, CO)

                            CO = New SqlCommand
                            CO.CommandType = CommandType.StoredProcedure
                            CO.CommandText = "rsp_profiles_code"
                            CO.Parameters.AddWithValue("@profile", dr(3))
                            CDC.ReadScalarValue(profile_fk, CO)

                            If supplier_pk > 0 Then
                                m.matrixfile_fk = matrixfile_pk
                                m.supplier_fk = supplier_pk
                                m.productname = dr(1)
                                m.distribution_fk = dr(2)
                                m.profile_fk = profile_fk
                                m.contractlength = dr(4)
                                m.validfrom = dr(5)
                                m.validto = dr(6)
                                If Not IsDBNull(dr(7)) Then : m.standingcharge = dr(7) : End If
                                If Not IsDBNull(dr(8)) Then : m.rate_single = dr(8) : End If
                                If Not IsDBNull(dr(9)) Then : m.rate_day = dr(9) : End If
                                If Not IsDBNull(dr(10)) Then : m.rate_night = dr(10) : End If
                                If Not IsDBNull(dr(11)) Then : m.rate_other = dr(11) : End If

                                If Not m.Exists Then
                                    m.Save()
                                    count += 1
                                Else
                                    AddNotice("setError('Price already exists');")
                                End If
                            Else
                                AddNotice("setError('Invalid Supplier (" + dr(0) + ") - row: " & dt.Rows.IndexOf(dr) & "');")
                            End If
                    End Select
                Next
            lblmessage.ForeColor = System.Drawing.Color.Green
            lblmessage.Text = CStr(count) & " records inserted."
            End If
        Catch ex As Exception
            lblmessage.ForeColor = System.Drawing.Color.Red
            lblmessage.Text = ex.Message
        Finally
            CO.Dispose()
            Panel1.Visible = True
            Panel2.Visible = False
        End Try
    End Sub

End Class
