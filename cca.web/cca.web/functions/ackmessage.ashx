<%@ WebHandler Language="VB" Class="ackmessage" %>
Public Class ackmessage
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Try
            Dim pk As Integer = Request.QueryString("pk")

            Dim um As New usermessages(SM.currentuser, SM.actualuser, pk, CDC)
            If um.user_fk = SM.CurrentUser Then
                um.Disable()
                Response.Write("OK|1")
            Else
                Response.Write("FAIL|Not authorised, this message will self expire.")
            End If
        Catch LE As Exception
            Response.Write("FAIL|" & LE.Message)
        End Try
    End Sub
 
End Class

