Partial Class cm_rfqsites_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ocm_rfqsites As Objects.cm_rfqsites
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_cm_rfqs_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.cm_rfq_fk.Items.Count=0 Then
        Me.cm_rfq_fk.DataSource=CDC.ReadDataTable(CO)
        Me.cm_rfq_fk.DataTextField = "value"
        Me.cm_rfq_fk.DataValueField = "pk"
        Try
            Me.cm_rfq_fk.DataBind
        Catch Ex as Exception
            Me.cm_rfq_fk.SelectedValue=-1
            Me.cm_rfq_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_sites_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.site_fk.Items.Count=0 Then
        Me.site_fk.DataSource=CDC.ReadDataTable(CO)
        Me.site_fk.DataTextField = "value"
        Me.site_fk.DataValueField = "pk"
        Try
            Me.site_fk.DataBind
        Catch Ex as Exception
            Me.site_fk.SelectedValue=-1
            Me.site_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ocm_rfqsites is Nothing then
ocm_rfqsites = new Objects.cm_rfqsites(currentuser,actualuser,pk,CDC)
End If
Me.cm_rfq_fk.SelectedValue=ocm_rfqsites.cm_rfq_fk
Me.site_fk.SelectedValue=ocm_rfqsites.site_fk
_CurrentPk=ocm_rfqsites.cm_rfqsite_pk
Status=ocm_rfqsites.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ocm_rfqsites is Nothing then
ocm_rfqsites = new Objects.cm_rfqsites(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ocm_rfqsites.cm_rfq_fk=Me.cm_rfq_fk.SelectedValue
ocm_rfqsites.site_fk=Me.site_fk.SelectedValue
result=ocm_rfqsites.Save()
if not result then throw ocm_rfqsites.LastError
_CurrentPk=ocm_rfqsites.cm_rfqsite_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ocm_rfqsites.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqsites is Nothing then
   ocm_rfqsites = new Objects.cm_rfqsites(currentuser,actualuser,pk,CDC)
  End If
  ocm_rfqsites.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqsites is Nothing then
   ocm_rfqsites = new Objects.cm_rfqsites(currentuser,actualuser,pk,CDC)
  End If
  ocm_rfqsites.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqsites is Nothing then
   ocm_rfqsites = new Objects.cm_rfqsites(currentuser,pk,CDC)
  End If
  ocm_rfqsites.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.cm_rfq_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkcm_rfq_fk.Visible=IIF(RL>=0,True,False)
  Me.site_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksite_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

