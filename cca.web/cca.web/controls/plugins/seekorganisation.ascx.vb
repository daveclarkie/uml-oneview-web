﻿Imports System.Data.SqlClient

Partial Class controls_plugins_seekorganisation
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public ReadOnly Property ControlName() As String
        Get
            Return Me.ClientID.Replace(Me.IdSeparator, "_")
        End Get
    End Property

    Public Property Enabled() As Boolean
        Get
            Return Me.organisation_name.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.organisation_name.Enabled = value
        End Set
    End Property

    Private Sub SetOrganisationName(ByVal pk As Integer)
        If CDC Is Nothing Then
            CDC = CType(Me.Page, DavePage).CDC
            SM = CType(Me.Page, DavePage).SM
        End If
        Dim org As New organisations(SM.currentuser, SM.actualuser, pk, CDC)
        If org.customername = "system" Then
            organisation_name.Text = "Select..."
        Else
            organisation_name.Text = org.customername
        End If
        org = Nothing
    End Sub


    Public Property fxValue() As Integer
        Get
            Dim pk As Integer = -1
            Try
                pk = Integer.Parse(organisation_pk.Value)
            Catch ex As Exception
                pk = -1
            End Try
            SetOrganisationName(pk)
            Return pk
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then value = -1
            organisation_pk.Value = value.ToString
            SetOrganisationName(value)
        End Set
    End Property

End Class
