﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="seekorganisation.ascx.vb" Inherits="controls_plugins_seekorganisation" %>
<div>
    <asp:HiddenField ID="organisation_pk" runat="server" EnableViewState="true" value="-1" />
    <asp:Label ID="organisation_name" runat="server" CssClass="input_span" Text="Select..." />
    <a href="javascript:clearOrganisation('<%=ControlName %>');" class="remove" visible="<%=Enabled.ToString %>"  title="Remove Organisation">X</a>&nbsp;
    <a href="javascript:findOrganisation('<%=ControlName %>');" class="locate" visible="<%=Enabled.ToString %>" title="Locate Organisation">?</a>&nbsp;
    <a href="javascript:showOrganisation('<%=ControlName %>');" class="inform" title="Organisation Details">i</a>
</div>
<script language="javascript" type="text/javascript">

    var trgOrg;
    var pkOrg;
    var elemOrg;

    function clearOrganisation(ctl) {
        var tmp
        trgOrg = ctl;
        tmp = document.getElementById(trgOrg + '_organisation_pk');
        pkOrg = "-1";
        tmp.value = pkOrg;
        chgNamedElementText(trgOrg + '_organisation_name', chgReplace, 'Select...');
    }

    function setOrganisation(pk, org) {
        var tmp
        tmp = document.getElementById(trgOrg + '_organisation_pk');
        tmp.value = pk;
        chgNamedElementText(trgOrg + '_organisation_name', chgReplace, org);
    }

    function showOrganisation(ctl) {
        trgOrg = ctl;
        pkOrg = document.getElementById(trgOrg + '_organisation_pk').value;
        var tlm = provideOrgDisplay(trgOrg + '_organisation_name');
        chgNamedElementText(tlm.id, chgReplace, loadOrgDetail(pkOrg));
    }

    function findOrganisation(ctl) {
        trgOrg = ctl;
        pkOrg = document.getElementById(trgOrg + '_organisation_pk').value;
        var tlm = provideOrgDisplay(trgOrg + '_organisation_name');
        chgNamedElementText(tlm.id, chgReplace, loadOrgSearch());
        elemOrg = tlm;
    }

    function searchOrganisation() {
        var orgName = document.getElementById('orgname_search').value;
        var orgPcode = document.getElementById('orgpcode_search').value;
        chgNamedElementText(elemOrg.id, chgReplace, loadOrgResults(orgName, orgPcode));
    }

    function createOrganisation(orgName, orgPcode) {
        chgNamedElementText(elemOrg.id, chgReplace, loadOrgCreate(orgName, orgPcode));
    }

    function loadOrgCreate(orgName, orgPcode) {
        var rqOrg = newRequest();
        var rspn = "";
        if (typeof rqOrg != "boolean") {
            var keys = "n=" + orgName + "&p=" + orgPcode + "&c=1";
            rqOrg.open("POST", "/functions/orgsearch.ashx?a=0" + noCache(), false);
            rqOrg.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            rqOrg.send(keys);
            rspn = rqOrg.responseText;
        }
        return rspn;
    }

    function loadOrgResults(orgName, orgPcode) {
        var rqOrg = newRequest();
        var rspn = "";
        if (typeof rqOrg != "boolean") {
            var keys = "n=" + orgName + "&p=" + orgPcode;
            rqOrg.open("POST", "/functions/orgsearch.ashx?a=0" + noCache(), false);
            rqOrg.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            rqOrg.send(keys);
            rspn = rqOrg.responseText;
        }
        return rspn;
    }

    function loadOrgDetail() {
        var rqOrg = newRequest();
        var rspn = "";
        if (typeof rqOrg != "boolean") {
            rqOrg.open("GET", "/functions/orgdetail.ashx?p=" + pkOrg + noCache(), false);
            rqOrg.send();
            rspn = rqOrg.responseText;
        }
        return rspn;
    }

    function loadOrgSearch() {
        var rqOrg = newRequest();
        var rspn = "";
        if (typeof rqOrg != "boolean") {
            rqOrg.open("GET", "/functions/orgsearch.ashx?a=0" + noCache(), false);
            rqOrg.send();
            rspn = rqOrg.responseText;
        }
        return rspn;
    }






    function provideOrgDisplay(ctl) {
        var eTarget;
        var eParent;
        var eTmp;

        try { eTarget = document.getElementById("orgDisplay"); } catch (e) { }
        try { eParent = eTarget.parentNode; } catch (e) { }
        try { eParent.removeChild(eTarget); eTarget = null; } catch (e) { }
        if (ctl == "") { ctl = "ctl00_content_pnlMain"; }
        if (!eTarget) {
            eParent = document.getElementById(ctl);
            eTmp = document.createElement("div");
            eTmp.setAttribute('id', 'orgDisplay');
            eTmp.setAttribute('class', 'modal_control_org');
            eTmp.setAttribute('style', 'display:;');
            eParent.appendChild(eTmp);
        }
        return document.getElementById("orgDisplay");
    }



</script>
