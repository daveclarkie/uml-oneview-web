Partial Class reportlogs_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oreportlogs As Objects.reportlogs
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_reports_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.report_fk.Items.Count=0 Then
        Me.report_fk.DataSource=CDC.ReadDataTable(CO)
        Me.report_fk.DataTextField = "value"
        Me.report_fk.DataValueField = "pk"
        Try
            Me.report_fk.DataBind
        Catch Ex as Exception
            Me.report_fk.SelectedValue=-1
            Me.report_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oreportlogs is Nothing then
oreportlogs = new Objects.reportlogs(currentuser,actualuser,pk,CDC)
End If
Me.report_fk.SelectedValue=oreportlogs.report_fk
Me.runfromurl.Text=oreportlogs.runfromurl
Me.param1name.Text=oreportlogs.param1name
Me.param1value.Text=oreportlogs.param1value
Me.param2name.Text=oreportlogs.param2name
Me.param2value.Text=oreportlogs.param2value
Me.param3name.Text=oreportlogs.param3name
Me.param3value.Text=oreportlogs.param3value
Me.param4name.Text=oreportlogs.param4name
Me.param4value.Text=oreportlogs.param4value
Me.savedpath.Text=oreportlogs.savedpath
_CurrentPk=oreportlogs.reportlog_pk
Status=oreportlogs.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oreportlogs is Nothing then
oreportlogs = new Objects.reportlogs(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oreportlogs.report_fk=Me.report_fk.SelectedValue
oreportlogs.runfromurl=Me.runfromurl.Text
oreportlogs.param1name=Me.param1name.Text
oreportlogs.param1value=Me.param1value.Text
oreportlogs.param2name=Me.param2name.Text
oreportlogs.param2value=Me.param2value.Text
oreportlogs.param3name=Me.param3name.Text
oreportlogs.param3value=Me.param3value.Text
oreportlogs.param4name=Me.param4name.Text
oreportlogs.param4value=Me.param4value.Text
oreportlogs.savedpath=Me.savedpath.Text
result=oreportlogs.Save()
if not result then throw oreportlogs.LastError
_CurrentPk=oreportlogs.reportlog_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oreportlogs.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportlogs is Nothing then
   oreportlogs = new Objects.reportlogs(currentuser,actualuser,pk,CDC)
  End If
  oreportlogs.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportlogs is Nothing then
   oreportlogs = new Objects.reportlogs(currentuser,actualuser,pk,CDC)
  End If
  oreportlogs.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportlogs is Nothing then
   oreportlogs = new Objects.reportlogs(currentuser,pk,CDC)
  End If
  oreportlogs.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.report_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkreport_fk.Visible=IIF(RL>=0,True,False)
  Me.runfromurl.Enabled=IIF(WL>=0,True,False)
  Me.blkrunfromurl.Visible=IIF(RL>=0,True,False)
  Me.param1name.Enabled=IIF(WL>=0,True,False)
  Me.blkparam1name.Visible=IIF(RL>=0,True,False)
  Me.param1value.Enabled=IIF(WL>=0,True,False)
  Me.blkparam1value.Visible=IIF(RL>=0,True,False)
  Me.param2name.Enabled=IIF(WL>=0,True,False)
  Me.blkparam2name.Visible=IIF(RL>=0,True,False)
  Me.param2value.Enabled=IIF(WL>=0,True,False)
  Me.blkparam2value.Visible=IIF(RL>=0,True,False)
  Me.param3name.Enabled=IIF(WL>=0,True,False)
  Me.blkparam3name.Visible=IIF(RL>=0,True,False)
  Me.param3value.Enabled=IIF(WL>=0,True,False)
  Me.blkparam3value.Visible=IIF(RL>=0,True,False)
  Me.param4name.Enabled=IIF(WL>=0,True,False)
  Me.blkparam4name.Visible=IIF(RL>=0,True,False)
  Me.param4value.Enabled=IIF(WL>=0,True,False)
  Me.blkparam4value.Visible=IIF(RL>=0,True,False)
  Me.savedpath.Enabled=IIF(WL>=0,True,False)
  Me.blksavedpath.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

