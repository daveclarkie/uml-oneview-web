<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="agreements.aspx.vb" Inherits="corecca_agreements" title="agreements" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<div class="inner" >
<div id="output" style="display:none; position:absolute; left:350px; top:20px;">

</div>
    <div class="content">
        
        <%  If agreementStatus() = 0 Then%>
            <h3>Agreement - Managed</h3> 
        <%  ElseIf agreementStatus() = 1 Then%>
            <h3 style="background-color:Red;">Agreement - Not Managed</h3>
        <% End If%>

        

            <ul id="innertab">
                <li class='<%=LIi("agreement")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=ag<%= Local.noCache %>">Detail</a></li>
                <li class='<%=LIi("summary")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=sm<%= Local.noCache %>">Summary</a></li>
                <li class='<%=LIi("target")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=tg<%= Local.noCache %>">Targets</a></li>
                <li class='<%=LIi("throughput")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=at<%= Local.noCache %>">Throughputs</a></li>
                <li class='<%=LIi("meterpoint")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=am<%= Local.noCache %>">Meters</a></li>
                <li class='<%=LIi("fuel")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=af<%= Local.noCache %>">Fuels</a></li>
                <li class='<%=LIi("report")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=rp<%= Local.noCache %>">Reports</a></li>
                <%--<li class='<%=LIi("appointments")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=ap<%= Local.noCache %>">Appointments</a></li>--%>
                <li class='<%=LIi("notes")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=nt<%= Local.noCache %>">Notes</a></li>
                <%--<li class='<%=LIi("requests")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=rq<%= Local.noCache %>">Requests</a></li>--%>
                <li class='<%=LIi("dataanalysis")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=da<%= Local.noCache %>" >Analysis</a></li>
                <li class='<%=LIi("activitylog")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=al<%= Local.noCache %>">Activity</a></li>
                <%--<li class='<%=LIi("contacts")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=ct<%= Local.noCache %>">Contacts</a></li>--%>
                <%--<li class='<%=LIi("submission")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=sb<%= Local.noCache %>">Submission</a></li>--%>
                <li class='<%=LIi("ranking")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=rk<%= Local.noCache %>">Ranking</a></li>
                <li class='<%=LIi("overview")%>'><a href="agreements.aspx?pk=<%=PSM.targetagreement %>&pg=ov<%= Local.noCache %>">Finance</a></li>
                <li class='<%=LIi("dataentry")%>'>Data Entry</li>
            </ul>

            <asp:MultiView runat="server" ID="mvDetail">
            
                <asp:View ID="vwAgreementOverview" runat="server">
                     <table>
                        <tr>
                            <td>
                                <h3>AX Contracts for <asp:label class="label" runat="server" id="lblAgreementServiceAgreementsCustomer"></asp:label></h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Repeater ID="rptAgreementServiceAgreements" runat="server">
                                    <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width:5px;">
                                
                                            </td>
                                            <td style="width:70px;">
                                                <h4>Contract ID</h4>
                                            </td>
                                            <td style="width:250px;">
                                                <h4>Product</h4>
                                            </td>
                                            <td style="width:70px;">
                                                <h4>From</h4>
                                            </td>
                                            <td style="width:70px;">
                                                <h4>To</h4>
                                            </td>
                                            <%--<td>
                                                <h4>Last Incoiced</h4>
                                            </td>--%>
                                            <td style="width:70px;">
                                                <h4>CY Value</h4>
                                            </td>
                                            <td style="width:70px;">
                                                <h4>Total Value</h4>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </HeaderTemplate> 
                                    <ItemTemplate>
                                        <tr id='node_<%# Container.DataItem("contractlineid") %>' style="border:0.5px solid black;">
                                            <td style="text-align:center;">
                                            </td>
                                            <td>
                                                <%# Container.DataItem("contractid")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("productname")%>
                                            </td>
                                            <td>
                                                <%#DateTime.Parse(Container.DataItem("firstcontractstart").ToString()).ToString("dd-MM-yyyy")%>
                                            </td>
                                            <td>
                                                <%#DateTime.Parse(Container.DataItem("lastcontractend").ToString()).ToString("dd-MM-yyyy")%>
                                            </td>
                                            <%--<td>
                                                <%#DateTime.Parse(Container.DataItem("lastinvoiced").ToString()).ToString("dd-MM-yyyy")%>
                                            </td>--%>
                                            <td align="right">
                                                <%# DataBinder.Eval(Container.DataItem, "cycontractvaluegbp", "{0:c}")%>
                                            </td>
                                            <td align="right">
                                                <%# DataBinder.Eval(Container.DataItem, "totalinvoicingactualgbp", "{0:c}")%></td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </ItemTemplate> 
                                    <FooterTemplate>
                                        <tr style="height:5px;">
                                        </tr>
                                    </table>
                                    </FooterTemplate> 
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>

                </asp:View>


                <asp:View ID="vwAgreement" runat="server">
                    <asp:PlaceHolder ID="AgreementControl" runat="server" />
                    <table id="AgreementButton" runat="server">
                        <tr>
                            
                            <td style="text-align:center; width:20%;">
                                <asp:LinkButton ID="btnSaveAgreement" runat="server" CausesValidation="true" 
                                    Text="&lt;h6&gt;Save Agreement&lt;/h6&gt;" />
                            </td>
                            <td style="width:20%;">
                            </td>
                            <td style="width:20%; text-align:center;">
                                <asp:LinkButton ID="btnManageAgreement" runat="server" CausesValidation="true" 
                                    Text="&lt;h6&gt;Manage&lt;/h6&gt;" />
                            </td>
                            <td style="width:20%;">
                            </td>
                            <td style="text-align:center; width:20%;">
                                <% If cca.common.Security.GroupMember(SM.actualuser, SystemGroups.CreateAgreementSnapshots, CDC) Then%>
                                <%  If isLatestAgreement() Then%>
                                <asp:LinkButton ID="btnSnapshotAgreement" runat="server" 
                                    CausesValidation="true" Text="&lt;h6&gt;Snapshot&lt;/h6&gt;" />
                                <%Else%>
                                <h6>
                                    This is not the latest version of the agreement</h6>
                                <% End if %><% End If%>
                            </td>
                            
                        </tr>
                    </table>

                   

                </asp:View>

                <asp:View ID="vwAgreementNotes" runat="server">
                    <table style="width:100%; text-align:center;">
                        <tr>
                            <td>
                                <a href='agreements.aspx?pg=nt&pk=<%= sm.targetagreement %>&type=4&spk=-1'><h6>Add Agreement Note</h6></a> 
                            </td>
                            <td>
                                <a href='agreements.aspx?pg=nt&pk=<%= sm.targetagreement %>&type=1&spk=-1'><h6>Add Customer Note</h6></a> 
                            </td>
                            <td>
                                <a href='agreements.aspx?pg=nt&pk=<%= sm.targetagreement %>&type=2&spk=-1'><h6>Add Site Note</h6></a> 
                            </td>
                            <td>
                                <a href='agreements.aspx?pg=nt&pk=<%= sm.targetagreement %>&type=3&spk=-1'><h6>Add Meter Note</h6></a> 
                            </td>
                        </tr>
                    </table>
                    <asp:PlaceHolder ID="AgreementNotesControl" runat="server" />            
                    <table id="AgreementNotesButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementNotes" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelAgreementNotes" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="height:5px;">
                               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>Filter: </td>
                                        <td>
                                            <asp:DropDownList ID="NotesFilter" runat="server" Width="200px">
                                               <asp:ListItem Value="0" Text="Show All"></asp:ListItem>
                                                <asp:ListItem Value="-1" Text="Agreement Only"></asp:ListItem>
                                                <asp:ListItem Value="-2" Text="Customer Only"></asp:ListItem>
                                                <asp:ListItem Value="-3" Text="Site Only"></asp:ListItem>
                                                <asp:ListItem Value="-4" Text="Meter Only"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="My Notes Only"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sorting: </td>
                                        <td>
                                            <asp:DropDownList ID="NotesSortBy" runat="server" Width="200px">
                                                <asp:ListItem Value="modified DESC">Date (Newest First)</asp:ListItem>
                                                <asp:ListItem Value="modified">Date (Oldest First)</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Button runat="server" ID="NotesRefreshRepeater" Text="Refresh" Width="150px" BackColor="#E47F00" ForeColor="#FFFFFF" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:5px;">
                               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>Notes related to the Agreement</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptAgreementNotes" runat="server">
                                    <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width:5px;">
                                
                                            </td>
                                            <td style="width:70px;">
                                                <h4>Note Type</h4>
                                            </td>
                                            <td style="width:110px;">
                                                <h4>User</h4>
                                            </td>
                                            <td style="width:70px;">
                                                <h4>Date</h4>
                                            </td>
                                            <td style="width:345px;">
                                                <h4>Note</h4>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </HeaderTemplate> 
                                    <ItemTemplate>
                                        <tr id='node_<%# Container.DataItem("note_pk") %>' style="border-bottom: 1px solid black;">
                                            <td>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("type")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("user")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("modified")%>
                                            </td>
                                            <td style="padding-top:10px;padding-bottom:10px;">
                                                <%# Container.DataItem("note").ToString().Replace(Environment.NewLine,"<br />")%>
                                            </td>
                                            <td>
                                                <a class="remove" title="Remove"  href='javascript:removeNote(<%# Container.DataItem("note_pk") %>)'>X</a>                                
                                            </td>
                                        </tr>
                                    </ItemTemplate> 
                                    <FooterTemplate>
                                        <tr style="height:5px;">
                                        </tr>
                                    </table>
                                    </FooterTemplate> 
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwAgreementRequests" runat="server">
                    <h5>Requests linked to this Agreement</h5>
                    
                    <table style="width:150px; text-align:center;">
                        <tr>
                            <td>
                                <a href='agreements.aspx?pg=rq&pk=<%= sm.targetagreement %>&spk=-1'><h6>Link New Request</h6></a> 
                            </td>
                        </tr>
                    </table>

                    <asp:PlaceHolder ID="AgreementRequestsControl" runat="server" />            
                    <table id="AgreementRequestsTable" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementRequests" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnCancelAgreementRequests" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>

                    <asp:Repeater ID="rptAgreementRequests" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td style="width:40px;">
                                
                            </td>
                            <td>
                                <h4>ID</h4>
                            </td>
                            <td>
                                <h4>Title</h4>
                            </td>
                            <td>
                                <h4>Status</h4>
                            </td>
                            <td>
                                <h4>Requester</h4>
                            </td>
                            <td>
                                <h4>Created</h4>
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr id='node_<%# Container.DataItem("workorderid") %>'>
                                <td style="text-align:center;">
                                    <a href='http://uk-ed0-sdsk-02.mceg.local/WorkOrder.do?woMode=viewWO&woID=<%# Container.DataItem("workorderid")%>' target='_blank'><h6>view</h6></a>
                                </td>
                                <td>
                                    <%# Container.DataItem("workorderid")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("title")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("status")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("requester")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("created")%>
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeTarget(<%# Container.DataItem("workorderid") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        </FooterTemplate> 
                    </asp:Repeater>
                    
                </asp:View>

                <asp:View ID="vwAgreementTargets" runat="server">
                    <h5>Current Targets <%= selecteditemname()%></h5>
                    <asp:Repeater ID="rptAgreementTarget" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td style="width:40px;">
                                
                            </td>
                            <td>
                                <h4>Description</h4>
                            </td>
                            <td>
                                <h4>Start</h4>
                            </td>
                            <td>
                                <h4>End</h4>
                            </td>
                            <td>
                                <h4>Target</h4>
                            </td>
                            <td>
                                <h4>EA Carbon Surplus</h4>
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr id='node_<%# Container.DataItem("agreementtarget_pk") %>'>
                                <td style="text-align:center;">
                                    <a href='agreements.aspx?pg=tg&pk=<%# sm.targetAgreement %>&spk=<%# Container.DataItem("agreementtarget_pk") %>'><h6>edit</h6></a>
                                </td>
                                <td>
                                    <%# Container.DataItem("targetdescription")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("targetstart")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("targetend")%>
                                </td>
                                <td>
                                    <%# FormatNumber(Container.DataItem("target"),3)%> %
                                </td>
                                <td>
                                    <%# Container.DataItem("eacarbonsurplus")%>
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeTarget(<%# Container.DataItem("agreementtarget_pk") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        <table style="width:150px; text-align:center;">
                            <tr>
                                <td>
                                    <a href='agreements.aspx?pg=tg&pk=<%= sm.targetagreement %>&spk=-1'><h6>New Target</h6></a> 
                                </td>
                            </tr>
                        </table>
                        </FooterTemplate> 
                    </asp:Repeater>
                    
                    <asp:PlaceHolder ID="AgreementTargetControl" runat="server" />            
                    <table id="AgreementTargetButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementTarget" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelAgreementTarget" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwAgreementMeterPoints" runat="server">
                <h5>Current Meter Points <%= selecteditemname()%></h5>
                
                <asp:Repeater ID="rptAgreementMeterPoints" runat="server">
                    <HeaderTemplate><table>
                    <tr>
                        <td style="width:40px;">
                                
                        </td>
                        <td style="width:40px;">
                                
                        </td>
                        <td style="width:50px;">
                            <h4>ID</h4>
                        </td>
                        <td style="width:200px;">
                            <h4>Identifier</h4>
                        </td>
                        <td style="width:80px;">
                            <h4>Fuel</h4>
                        </td>
                        <td style="width:50px;">
                            <h4>Missing</h4>
                        </td>
                        <td style="width:150px;">
                            <h4>Current Supplier</h4>
                        </td>
                        <td style="width:20px;">
                            <h4>T</h4>
                        </td>
                        <td></td>
                    </tr>
                </HeaderTemplate> 
                
                <ItemTemplate>
                    <tr id='node_<%# Container.DataItem("agreementmeterpoint_pk") %>'>
                        <td style="text-align:center;">
                            <a href='agreements.aspx?pg=am&pk=<%# sm.targetAgreement %>&spk=<%# Container.DataItem("agreementmeterpoint_pk") %>'><h6>edit</h6></a>
                        </td>
                        <td style="text-align:center;">
                            <a href='agreements.aspx?pg=dm&pk=<%# sm.targetAgreement %>&dpk=<%# Container.DataItem("agreementmeterpoint_pk") %>'><h6>data</h6></a>
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("meterpoint_pk")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("meter")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("fuelname")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("missing")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("currentsupplier")%>                                
                        </td>
                        <td style="text-indent:0px;">
                            <%# Container.DataItem("associatedthroughput")%>
                        </td>
                        <td style="text-indent:0px;">
                            <a class="remove" title="Remove"  href='javascript:removeMeterpoint(<%# Container.DataItem("agreementmeterpoint_pk") %>)'>X</a>                                
                        </td>
                    </tr>
                </ItemTemplate> 
                    
                <FooterTemplate>
                        <tr style="height:5px;">
                        </tr>
                    </table>
                    <% If SM.targetAgreementSecondary = -1 Then%>
                        <table style="display:none; ">
                    <%  Else%>
                        <table style="width:150px; text-align:left; height: 50px; ">
                   <% End If%>
                        <tr>
                            <td>
                                <a href='agreements.aspx?pg=am&pk=<%= sm.targetAgreement %>&spk=-1' class='button_orange'>New Meter Point</a> 
                            </td>
                        </tr>
                    </table>

                </FooterTemplate> 
                </asp:Repeater>
                    


                <asp:PlaceHolder ID="AgreementMeterPointControl" runat="server" />
                    <table id="AgreementMeterPointButton" runat="server">
                    <tr style="width:100%; text-align:left; height: 50px; ">
                        <td>
                            <asp:Linkbutton runat="server" ID="btnSaveAgreementMeterPoint" Text="Save" class="button_green" /> 
                        </td>
                        <td ></td>
                        <td style="text-align:right;">
                            <asp:Linkbutton runat="server" ID="btnCancelAgreementMeterPoint" Text="Cancel" class="button_grey" />
                        </td>
                    </tr>
                </table>
                </asp:View>

                <asp:View ID="vwAgreementThroughputs" runat="server">
                <h5>Current Throughputs <%= selecteditemname()%></h5>
                <asp:Repeater ID="rptAgreementThroughputs" runat="server">
                    <HeaderTemplate><table>
                    <tr>
                        <td style="width:50px;">
                                
                        </td>
                        <td style="width:50px;">
                                
                        </td>
                         <td style="width:80px;">
                            <h4>ID</h4>
                        </td>
                        <td style="width:200px;">
                            <h4>Name</h4>
                        </td>
                        <td style="width:150px;">
                            <h4>Unit</h4>
                        </td>
                        <td></td>
                    </tr>
                </HeaderTemplate> 
                <FooterTemplate>
                        <tr style="height:5px;">
                        </tr>
                    </table>
                    <table style="width:150px; text-align:center;">
                        <tr>
                            <td>
                                <a href='agreements.aspx?pg=at&pk=<%= sm.targetagreement %>&spk=-1'><h6>New Throughput</h6></a> 
                            </td>
                        </tr>
                    </table>
                </FooterTemplate> 
                    <ItemTemplate>
                         <tr id='node_<%# Container.DataItem("agreementthroughput_pk") %>'>
                            <td style="text-align:center;">
                                <a href='agreements.aspx?pg=at&pk=<%= sm.targetagreement %>&spk=<%# Container.DataItem("agreementthroughput_pk") %>'><h6>edit</h6></a> 
                            </td>
                            <td style="text-align:center;">
                                <a href='agreements.aspx?pg=dt&pk=<%# sm.targetAgreement %>&dpk=<%# Container.DataItem("agreementthroughput_pk") %>'><h6>data</h6></a>
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("throughput_fk")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("throughputname")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("unitname")%> 
                            </td>
                            <td>
                                <a class="remove" title="Remove"  href='javascript:removeThroughput(<%# Container.DataItem("agreementthroughput_pk") %>)'>X</a>                                
                            </td>
                        </tr>
                    </ItemTemplate> 
                </asp:Repeater>
                    
                <asp:PlaceHolder ID="AgreementThroughputControl" runat="server" />                
                <table id="AgreementThroughputButton" runat="server">
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveAgreementThroughput" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton ID="btnCancelAgreementThroughput" runat="server" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
                </asp:View>

                <asp:View ID="vwAgreementFuels" runat="server">
                
                    <h5>Current Fuels <%= selecteditemname()%></h5>
                    <asp:Repeater ID="rptAgreementFuels" runat="server">
                        <HeaderTemplate><table>
                            <tr>
                                <td style="width:50px;">
                                
                                </td>
                                <td style="width:200px;">
                                    <h4>Fuel</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>From</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>To</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Eligibility</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Relief</h4>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr>
                                <td style="text-align:center;">
                                    <a href='agreements.aspx?pg=af&pk=<%# sm.targetAgreement %>&spk=<%# Container.DataItem("agreementfuel_pk") %>'><h6>edit</h6></a>
                                </td>
                                <td style="text-indent:5px;">
                                    <%# Container.DataItem("fuelname")%> 
                                </td>
                                <td style="text-indent:5px;">
                                    <%# Container.DataItem("validfrom")%> 
                                </td>
                                <td style="text-indent:5px;">
                                    <%# Container.DataItem("validto")%> 
                                </td>
                                <td style="text-indent:5px;">
                                    <%# Container.DataItem("eligibility")%> %
                                </td>
                                <td style="text-indent:5px;">
                                    <%# Container.DataItem("relief")%> %
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeAgreementFuel(<%# Container.DataItem("agreementfuel_pk") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                                <tr style="height:5px;">
                                </tr>
                            </table>
                            <table style="width:150px; text-align:center;">
                                <tr>
                                    <td>
                                        <a href='agreements.aspx?pg=af&pk=<%= sm.targetAgreement %>&spk=-1'><h6>New</h6></a> 
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate> 
                    </asp:Repeater>
                    <asp:PlaceHolder ID="AgreementFuelControl" runat="server" />       
                    <table id="AgreementFuelButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <h6><asp:Linkbutton runat="server" ID="btnSaveAgreementFuel" Text="Save" /></h6>
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;"></td>
                        </tr>
                    </table>
                </asp:View>
                
                <asp:View ID="vwAgreementReports" runat="server">
                    <h3>Milestone Tracking Service Report (MTS)</h3>
                    <table>
                        <tr>
                            <td style="width:150px;">
                                <b>Select Target</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddltarget" runat="server" AutoPostBack="true" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b>Reporting Period</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlperiod" runat="server" AutoPostBack="true" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>   
                                             
                        <tr>
                            <td style="height:5px;">
                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td valign="top">
                                <b>Data Status</b>
                            </td>
                            <td>
                                <asp:Repeater ID="rptReportDataCheck" runat="server" onitemdatabound="rptReportDataCheck_ItemDataBound">
                                    <HeaderTemplate><table>
                                        <tr>
                                            <td style="width:100px;">
                                                <h4>Fuel</h4>
                                            </td>
                                            <td style="width:140px;">
                                                <h4>Name</h4>
                                            </td>
                                            <td style="width:80px;">
                                                <h4>Missing Data</h4>
                                            </td>
                                            <td style="width:80px;">
                                                <h4>OK to Run?</h4>
                                            </td>
                                        </tr>
                                    </HeaderTemplate> 
                                    <ItemTemplate>
                                        <tr style='background-color:<%# Container.DataItem("fuel")%> ;'>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("fuel")%> 
                                            </td>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("meter")%> 
                                            </td>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("max_date")%> 
                                            </td>
                                            <td style="text-indent:0px;">
                                                <%# Container.DataItem("status")%> 
                                            </td>
                                        </tr>
                                    </ItemTemplate> 
                                    <FooterTemplate>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="lblEmptyDataFooter" Text="All data is up to date for selected period" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                        <tr>
                            <td style="height:5px;">
                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td>
                                <asp:Linkbutton runat="server" ID="btnRunReport" Text="<h4>Run Report</h4>" Enabled="false" Width="200px" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Linkbutton runat="server" ID="btnExportInvoice" Text="<h4>Export Invoice Data</h4>" Enabled="false" Width="200px" />
                            </td>
                        </tr>

                    </table>
                    <br />




                    <br />
                    <h3>Credit Notes (Latest)</h3>
                    <asp:Repeater ID="rptcreditnotes_latest" runat="server">
                            <HeaderTemplate>    
                                <table>
                                    <tr>
                                        <td style="width:80px;">
                                            <h4>Entered</h4>
                                        </td>
                                        <td style="width:70px;">
                                            <h4>Fuel</h4>
                                        </td>
                                        <td style="width:50px;">
                                            <h4>From</h4>
                                        </td>
                                        <td style="width:50px;">
                                            <h4>To</h4>
                                        </td>
                                        <td style="width:100px;">
                                            <h4>Added By</h4>
                                        </td>
                                    </tr>
                            </HeaderTemplate> 
                            <ItemTemplate>
                                <tr class="<%# container.dataitem("flagclass")%>">
                                    <td style="text-indent:5px;"><%# Container.DataItem("created")%></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("fueltype")%></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("billedfrom")%></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("billedto")%></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("addedby")%></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate> 
                        </asp:Repeater>
                    <br />

                    <h3>HMRC CCL Rate Audit Report</h3>
                    <table>
                        <tr>
                            <td style="width:150px;">
                                <b>Select Meter</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlreportmeters" runat="server" AutoPostBack="true" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Report From</b>
                            </td>
                            <td>
                                <asp:TextBox EnableViewState="false" ID="hmrcreportfrom" runat="server" cssClass="input_dtm" Width="197px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Report To</b>
                            </td>
                            <td>
                                <asp:TextBox EnableViewState="false" ID="hmrcreportto" runat="server" cssClass="input_dtm" Width="197px" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Linkbutton runat="server" ID="btnRunHMRCReport" Text="<h4>Run Report</h4>" Enabled="false" Width="200px" />
                            </td>
                        </tr>
                    </table>
                    <br />




                    <br />
                    <h3>Error Reporting</h3>
                    <asp:Panel ID="panelError" runat="server">
                        <table>
                            <tr>
                                <td style="width:150px;">
                                    <b>Description</b>
                                </td>
                                <td>
                                    <asp:TextBox EnableViewState="false" ID="txtErrorDescription" runat="server" cssClass="input_dtm" Width="297px" Height="100px" TextMode="MultiLine" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Linkbutton runat="server" ID="btnLogError" Text="<h6>Log Error</h6>" Enabled="true" Width="200px" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="panelErrorResponse" runat="server" Visible="false">
                        <asp:Label ID="lblformResponse" runat="server"></asp:Label>
                    </asp:Panel>

                    <rsweb:ReportViewer ID="myReportViewer" Height="500px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="400px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                    </rsweb:ReportViewer> 
                </asp:View>

                <asp:View ID="vwDataEntryConsumption" runat="server">
                        <table >
                            <tr>
                                <td style="width:150px;">
                                    <a href='agreements.aspx?pg=dm&pk=<%= sm.targetagreement %>&dpk=<%= sm.targetdataentry%>&spk=-1'><h6>New Consumption</h6></a> 
                                </td>
                                <td>
                                </td>
                                <td style="width:150px;">
                                    <asp:Linkbutton ID="btnCheckMeterPointConsumption" runat="server" Text="<h6>Check Missing Invoices</h6>" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>

                    <asp:PlaceHolder ID="MeterPointConsumptionControl" runat="server" />
                    <table id="MeterPointConsumptionButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <a href="javascript:saveConsumption();"><h6>Save</h6></a>
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelMeterPointConsumption" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                    <h3 style="background-color:Red;">Missing Data</h3>
                        <asp:Repeater ID="rptDataMissingConsumption" runat="server">
                            <HeaderTemplate>    
                                <table>
                                    <tr>
                                        <td style="width:50px;">
                                
                                        </td>
                                        <td style="width:80px;">
                                            <h4>Month</h4>
                                        </td>
                                        <td style="width:40px;">
                                            <h4>Year</h4>
                                        </td>
                                        <td style="width:80px;">
                                            <h4>Description</h4>
                                        </td>
                                        <td style="width:50px;">
                                            
                                        </td>
                                        <td style="width:170px;">
                                            
                                        </td>
                                        <td style="width:15px;">
                                                                                  
                                        </td>
                                        <td style="width:15px;">
                                           
                                        </td>
                                    </tr>
                            </HeaderTemplate> 
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td><%# Container.DataItem("monthname")%></td>
                                    <td><%# Container.DataItem("year")%></td>
                                    <td style="text-align:right;"><%# Container.DataItem("type")%></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate> 
                        </asp:Repeater>
                   
                    <h3>Data</h3>
                        <asp:Repeater ID="rptDataEntryConsumption" runat="server">
                            <HeaderTemplate>    
                                <table>
                                    <tr>
                                        <td style="width:50px;">
                                
                                        </td>
                                        <td style="width:80px;">
                                            <h4>Month</h4>
                                        </td>
                                        <td style="width:40px;">
                                            <h4>Year</h4>
                                        </td>
                                        <td style="width:80px;">
                                            <h4>Consumption</h4>
                                        </td>
                                        <td style="width:50px;">
                                            <h4>Eligible %</h4>
                                        </td>
                                        <td style="width:170px;">
                                            <h4>Imported</h4>
                                        </td>
                                        <td style="width:15px;">
                                            <h4>E</h4>                                            
                                        </td>
                                        <td style="width:15px;">
                                            <h4>X</h4>            
                                        </td>
                                    </tr>
                            </HeaderTemplate> 
                            <ItemTemplate>
                                <tr id='node_<%# Container.DataItem("dataconsumption_pk") %>' class="<%# container.dataitem("flagclass")%>">
                                    <td><a href='agreements.aspx?pg=dm&pk=<%= sm.targetagreement %>&dpk=<%= sm.targetDataEntry %>&spk=<%# Container.DataItem("dataconsumption_pk") %>'><h6>Edit</h6></a></td>
                                    <td><%# Container.DataItem("monthname")%></td>
                                    <td><%# Container.DataItem("year")%></td>
                                    <td style="text-align:right;"><%# Container.DataItem("totalconsumption")%></td>
                                    <td style="text-align:right;">[<%# Container.DataItem("eligiblepercentage")%>%] </a></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("created")%></td>
                                    <td style="text-indent:5px;"><%# Container.DataItem("modified")%></td>
                                    <td><a class="remove" title="Remove"  href='javascript:removeDataConsumption(<%# Container.DataItem("dataconsumption_pk") %>)'>X</a></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate> 
                        </asp:Repeater>

                        
                </asp:View>
                
                <asp:View ID="vwDataEntryThroughputs" runat="server">
                    <asp:Repeater ID="rptDataEntryThroughput" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td style="width:50px;">
                                
                                    </td>
                                    <td style="width:80px;">
                                        <h4>Month</h4>
                                    </td>
                                    <td style="width:40px;">
                                        <h4>Year</h4>
                                    </td>
                                    <td style="width:80px;">
                                        <h4>Consumption</h4>
                                    </td>
                                    <td style="width:50px;">
                                        <h4>Eligible %</h4>
                                    </td>
                                        <td style="width:170px;">
                                            <h4>Imported</h4>
                                        </td>
                                        <td style="width:15px;">
                                            <h4>E</h4>                                            
                                        </td>
                                        <td style="width:15px;">
                                            <h4>X</h4>            
                                        </td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("datathroughput_pk") %>' class="<%# container.dataitem("flagclass")%>">
                                <td><a href='agreements.aspx?pg=dt&pk=<%= sm.targetagreement %>&dpk=<%= sm.targetDataEntry %>&spk=<%# Container.DataItem("datathroughput_pk") %>'><h6>Edit</h6></a></td>
                                <td><%# Container.DataItem("monthname")%></td>
                                <td><%# Container.DataItem("year")%></td>
                                <td style="text-align:right;"><%# Container.DataItem("totalthroughput")%></td>
                                <td style="text-align:right;">[<%# Container.DataItem("eligiblepercentage")%>%] </a></td>
                                <td style="text-indent:5px;"><%# Container.DataItem("created")%></td>
                                <td style="text-indent:5px;"><%# Container.DataItem("modified")%></td>
                                <td><a class="remove" title="Remove"  href='javascript:removeDataThroughput(<%# Container.DataItem("datathroughput_pk") %>)'>X</a></td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate> 
                    </asp:Repeater>
                    
                    <table style="width:240px;">
                        <tr>
                            <td colspan="4">
                                <a href='agreements.aspx?pg=dt&pk=<%= sm.targetagreement %>&dpk=<%= sm.targetdataentry%>&spk=-1'><h6>New Throughput</h6></a> 
                            </td>
                        </tr>
                    </table>

                    <asp:PlaceHolder ID="ThroughputConsumptionControl" runat="server" />
                    <table id="ThroughputConsumptionButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <h6><a href="javascript:saveThroughput();">Save</a></h6>
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelThroughputConsumption" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwAgreementActivityLogs" runat="server">
                    <h5>Activity Logs <%= selecteditemname()%></h5>
                    <asp:Repeater ID="rptAgreementActivityLogs" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td>
                                <h4>Type</h4>
                            </td>
                            <td>
                                <h4>Description</h4>
                            </td>
                            <td>
                                <h4>Who</h4>
                            </td>
                            <td>
                                <h4>When</h4>
                            </td>
                            <td>
                                
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr id='node_<%# Container.DataItem("pk") %>'>
                                <td>
                                    <%# Container.DataItem("type")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("value")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("who")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("when")%>
                                </td>
                                <td>
                                    <a href='javascript:viewCCAReport(<%# Container.DataItem("pk") %>)'><%# IIf(IsDBNull(Container.DataItem("savedpath")), "", "VIEW")%></a>
                                    <a href='<%# Container.DataItem("savedpath")%>'>Download This</a>
                                </td>
                                <td>
                                                                  
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        
                        </FooterTemplate> 
                    </asp:Repeater>
                </asp:View>                

                <asp:View ID="vwAgreementDataAnalysis" runat="server">
                <div class="list">
                    <table>
                            <tr>
                                <td colspan="2"><h5>Meters</h5></td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:20%; font-weight:bold; ">
                                       Available<span style="display:inline-block; width:5px;"></span>
                                </td>
                                <td style="width:80%;">
                                    <ul class="list" id="AvailableAnalysisMeterList" style="width:90%;"></ul>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:20%; font-weight:bold; ">
                                       Selected<span style="display:inline-block; width:5px;"></span>
                                </td>
                                <td style="width:80%;">
                                    <ul class="list" id="ActiveAnalysisMeterList" style="width:90%;"></ul>
                                </td>
                            </tr>
                    </table>
                </div>
                <div class="list">
                    <table>
                            
                            <tr>
                                <td colspan="2"><h5>Dates</h5></td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:20%; font-weight:bold; ">
                                       Use Targets<span style="display:inline-block; width:5px;"></span>
                                </td>
                                <td style="width:80%;">
                                    <ul class="list" id="TargetsAnalysisList" style="width:90%;"></ul>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:right;font-weight:bold;">
                                       Custom From<span style="display:inline-block; width:5px"></span>
                                </td>
                                <td>
                                    <asp:TextBox ID="datefrom" runat="server" Width="90%" BackColor="#FFFFC0" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;font-weight:bold;">
                                       Custom To<span style="display:inline-block; width:5px"></span>
                                </td>
                                <td>
                                    <asp:TextBox ID="dateto" runat="server" Width="90%" BackColor="#FFFFC0" ></asp:TextBox>
                                </td>
                            </tr>
                    </table>
                </div>
                <div class="list">
                    <table>
                            
                            <tr>
                                <td colspan="2"><h5>Other</h5><span style="display:inline-block; width:5px"></span></td>
                            </tr>
                            <tr>
                                <td style="text-align:right; width:20%; font-weight:bold; ">
                                       Compare Base Year?<span style="display:inline-block; width:5px"></span>
                                </td>
                                <td style="width:80%;">
                                    <asp:CheckBox ID="chkDataAnalysis" runat="server" AutoPostBack="false" />
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="list" style="border: 0px none black;">
                    <table>
                            <tr style="height:50px;">
                                <td style="width:100px;">
                               
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkviewdateperiod" runat="server" class="button_orange" Text="View Date Period"></asp:LinkButton>
                                </td>
                                <td style="width:10px;">
                               
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkviewauditdata" runat="server" class="button_orange" Text="View Audit Data"></asp:LinkButton>
                                </td>
                                <td>
                               
                                </td>
                            </tr>
                        </table>
                </div>


                    <asp:Panel ID="View1" runat="server" ScrollBars="Horizontal" Width="1080px" Height="900px">
                
                        <rsweb:ReportViewer ID="myReportViewer1" Height="700px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="1000px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                        </rsweb:ReportViewer> 
                        <%--<asp:GridView ID="grdPivot" runat="server" Width="900px"></asp:GridView>
                        <DCWC:Chart ID="chartPivot" runat="server"></DCWC:Chart>--%>
                       


                </asp:Panel>
                </asp:View>

                <asp:View ID="vwAgreementAppointments" runat="server">
                    <asp:PlaceHolder ID="AgreementAppointmentsControl" runat="server" />
                    <table id="AgreementAppointmentsButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementAppointments" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnCancelAgreementAppointments" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>

                   

                </asp:View>

                <asp:View ID="vwAgreementContacts" runat="server">
                    <asp:PlaceHolder ID="AgreementContactsControl" runat="server" />
                    <table id="AgreementContactsButton" runat="server">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementContacts" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnCancelAgreementContacts" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
                
                <asp:View ID="vwAgreementSummary" runat="server">
                <h3>Base Year</h3>
                    <asp:Repeater ID="rptAgreementSummaryBase" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td>
                                <h4>Base Year</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Throughput&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Energy&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Carbon&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>SEC&nbsp;</h4>
                            </td>
                            <td>
                                
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr>
                                <td style="width:110px;">
                                    <%# Container.DataItem("start")%> to <%# Container.DataItem("end")%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("throughput_base"), 0)%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("primary_base_energy"), 0)%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                     <%# FormatNumber(Container.DataItem("primary_base_carbon"), 0)%>
                                </td>
                                <td style="width:50px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("performance_base"), 4)%>
                                </td>
                                <td>
                                                                  
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        
                        </FooterTemplate> 
                    </asp:Repeater>
                <br />
                <h3>Reporting Periods</h3>
                    <asp:Repeater ID="rptAgreementSummaryReporting" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td>
                                <h4>Period</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Throughput&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Energy&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Carbon&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>SEC&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Target&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>SEC Target&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Acheived&nbsp;</h4>
                            </td>
                            <td>
                                
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr style='color:<%# Container.DataItem("improvement_colour")%>'>
                                <td style="width:110px;">
                                    <%# Container.DataItem("start")%> to <%# Container.DataItem("end")%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("throughput_milestone"), 0)%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("primary_milestone_energy"), 0)%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                     <%# FormatNumber(Container.DataItem("primary_milestone_carbon"), 0)%>
                                </td>
                                <td style="width:50px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("performance_milestone"), 4)%>
                                </td>
                                <td style="width:50px; text-align:right;">
                                    <%# FormatPercent(Container.DataItem("target") / 100)%>
                                </td>
                                <td style="width:70px; text-align:right;">
                                    <%# FormatNumber(Container.DataItem("performance_target"), 4)%>
                                </td>
                                <td style="width:50px; text-align:right;">
                                    <%# FormatPercent(Container.DataItem("actualimprovement") / 100)%>
                                </td>
                                <td>
                                                                  
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        
                        </FooterTemplate> 
                    </asp:Repeater>
                <br />
                <h3>Submission</h3>
                
                    <asp:Repeater ID="rptAgreementSubmission" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td>
                                <h4>Milestone</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Target&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Production&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Energy&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>MS TD&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>MS End&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Balance&nbsp;</h4>
                            </td>
                            <td>
                                
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr>
                                <td style="width:110px;">
                                    <%# Container.DataItem("start")%> to <%# Container.DataItem("end")%>
                                </td>
                                <td style="width:80px; text-align:right;">
                                    <%# FormatPercent(Container.DataItem("target")/100)%>
                                </td>
                                <td style='width:80px; text-align:right; color:<%# Container.DataItem("colour_throughput")%>'>
                                    <%# Container.DataItem("throughput_milestone_count")%> / <%# Container.DataItem("throughput_milestone_countrequired")%>
                                </td>
                                <td style='width:80px; text-align:right; color:<%# Container.DataItem("colour_meters")%>'>
                                    <%# Container.DataItem("primary_milestone_count")%> / <%# Container.DataItem("primary_milestone_countrequired")%>
                                </td>
                                <td style='width:80px; text-align:right; color:<%# Container.DataItem("colour_co2")%>'>
                                    <%# Container.DataItem("co2whole")%>
                                </td>
                                <td style='width:80px; text-align:right; color:<%# Container.DataItem("colour_co2")%>'>
                                     <%# Container.DataItem("co2forecastwhole")%>
                                </td>
                                <td style='width:80px; text-align:right; color:<%# Container.DataItem("colour_balance")%>'>
                                    <%# Container.DataItem("rolling_co2_forecast")%>
                                </td>
                                <td>
                                                                  
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        
                        </FooterTemplate> 
                    </asp:Repeater>


                    <asp:PlaceHolder ID="AgreementSummaryControl" runat="server" />
                    <table id="AgreementSummaryButton" runat="server" visible="false">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementSummary" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnCancelAgreementSummary" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
                   
                   
                <asp:View ID="vwAgreementSubmission" runat="server">
                
                    <asp:PlaceHolder ID="AgreementSubmissionControl" runat="server" />
                    <table id="AgreementSubmissionButton" runat="server" visible="false">
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveAgreementSubmission" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnCancelAgreementSubmission" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
                
                <asp:View ID="vwAgreementRanking" runat="server">
                    <asp:Repeater ID="rptAgreementRanking" runat="server">
                        <HeaderTemplate><table>
                        <tr>
                            <td>
                                <h4>#</h4>
                            </td>
                            <td>
                                <h4>Agreement&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Required Energy&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Forecast Energy&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>CO2&nbsp;</h4>
                            </td>
                            <td style="text-align:right;">
                                <h4>Info&nbsp;</h4>
                            </td>
                            <td>
                                
                            </td>
                            <td></td>
                        </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            
                            <tr style='color:<%# Container.DataItem("line_colour")%>'>
                                <td style="width:10px;">
                                    <%# Container.DataItem("rank")%>
                                </td>
                                <td style="width:200px;">
                                    <%# Container.DataItem("label_agreementname")%>
                                </td>
                                <td style='width:110px; text-align:right;'>
                                    <%# FormatNumber(Container.DataItem("required_primary"), 0)%>
                                </td>
                                <td style='width:100px; text-align:right;'>
                                    <%# FormatNumber(Container.DataItem("forecast_primary"), 0)%>
                                </td>
                                <td style='width:70px; text-align:right; color:<%# Container.DataItem("co2_colour")%>;'>
                                    <%# FormatNumber(Container.DataItem("co2position"), 0)%>
                                </td>
                                <td style='width:100px; text-align:right;'>
                                     <%# Container.DataItem("information")%>
                                </td>
                                <td>
                                                                  
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                        
                        </FooterTemplate> 
                    </asp:Repeater>
                   
                </asp:View>

            </asp:MultiView>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="AgreementNav" runat="server" />

    </div>
</div>
</asp:Content>

