Imports System.Data.SqlClient

Partial Class controls_combo_agreementthroughput
   Inherits DaveControl
   Public PSM As MySM

   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
   End Sub

   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub

    Dim dt As DataTable
    Dim CO As SqlCommand

    Dim _PageMode As Local.Action = Local.Action.None

    Dim saved As Boolean = False

    Public Property PageMode() As Local.Action
        Get
            Return _PageMode
        End Get
        Set(ByVal value As Local.Action)
            _PageMode = value
            Reload()
        End Set
    End Property

    Public Property DetailVisible() As Boolean
        Get
            Return agreementthroughput_combo_ctl.Visible
        End Get
        Set(ByVal value As Boolean)
            If Not (saved And value) Then agreementthroughput_combo_ctl.Visible = value
            If Not (value) Then agreementthroughput_combo_ctl.Visible = value
        End Set
    End Property

    Public Sub Reload()
        Dim pk As Integer = -1

         If Integer.TryParse(Request.QueryString("spk"), pk) Then
             SM.linkId = pk
             If SM.linkId = -1 then SM.subId=-1
         End If

        DetailVisible=True
        Select Case PageMode
            Case Local.Action.Save
                SaveItem()
                saved=True
                DetailVisible=False
            Case Local.Action.Read
                LoadItem()
                LoadList()
            Case Local.Action.None
                LoadList()
                DetailVisible=False
        End Select

    End Sub

    Private Sub SetSubPageMode()
    End Sub

    Public Sub NewItem()
        SM.linkId = -1
        SM.subId = -1
        agreementthroughput_combo_ctl.Read(SM.CurrentUser, SM.ActualUser,SM.linkId)
    End Sub

    Public Sub LoadList()
        CO = New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_agreementthroughputs"
        CO.Parameters.AddWithValue("@agreement_pk", SM.Targetagreement)
        dt = CDC.ReadDataTable(CO)
        rpt.DataSource = dt
        rpt.DataBind()
        dt.Dispose()
        CO.Dispose()
    End Sub

    Public Sub LoadItem()
        If SM.Targetagreement > 0 Then
            agreementthroughput_combo_ctl.Read(SM.CurrentUser, SM.ActualUser, SM.Targetagreement)
        End If
    End Sub

    Public Sub SaveItem()
        Dim errMsg As String()
        Dim msg As String
        Dim mk As Boolean = False
        Try
            mk =agreementthroughput_combo_ctl.Save(SM.CurrentUser, SM.ActualUser, SM.Targetagreement)
        Catch ex As Exception

        End Try

        If Not mk Then
            errMsg = agreementthroughput_combo_ctl.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                msg = "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1)
            Else
                msg = "This record has not been saved. <br /> System reports the following message:-<br />" & errMsg(0).Replace("""","`").Replace("'", "`") & "."
            End If
            AddNotice("setError('" & msg & "');")
        Else
            AddNotice("setNotice('Record Saved');")
        End If
    End Sub

End Class
