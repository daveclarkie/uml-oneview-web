Partial Class usermessages_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ousermessages As Objects.usermessages
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_messages_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.message_fk.Items.Count=0 Then
        Me.message_fk.DataSource=CDC.ReadDataTable(CO)
        Me.message_fk.DataTextField = "value"
        Me.message_fk.DataValueField = "pk"
        Try
            Me.message_fk.DataBind
        Catch Ex as Exception
            Me.message_fk.SelectedValue=-1
            Me.message_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ousermessages is Nothing then
ousermessages = new Objects.usermessages(currentuser,actualuser,pk,CDC)
End If
Me.user_fk.SelectedValue=ousermessages.user_fk
Me.message_fk.SelectedValue=ousermessages.message_fk
_CurrentPk=ousermessages.usermessage_pk
Status=ousermessages.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ousermessages is Nothing then
ousermessages = new Objects.usermessages(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ousermessages.user_fk=Me.user_fk.SelectedValue
ousermessages.message_fk=Me.message_fk.SelectedValue
result=ousermessages.Save()
if not result then throw ousermessages.LastError
_CurrentPk=ousermessages.usermessage_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ousermessages.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousermessages is Nothing then
   ousermessages = new Objects.usermessages(currentuser,actualuser,pk,CDC)
  End If
  ousermessages.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousermessages is Nothing then
   ousermessages = new Objects.usermessages(currentuser,actualuser,pk,CDC)
  End If
  ousermessages.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousermessages is Nothing then
   ousermessages = new Objects.usermessages(currentuser,pk,CDC)
  End If
  ousermessages.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  Me.message_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkmessage_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

