<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrix1sources_ctl.ascx.vb" Inherits="matrix1sources_ctl" %>
<h5>matrix1sources</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmatrixfile_fk">
<span class='label'>Matrix File<span class='pop'>Which file was the price uploaded from?</span></span>
<asp:DropDownList ID="matrixfile_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksupplier_fk">
<span class='label'>Supplier Name</span>
<asp:DropDownList ID="supplier_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkproductname">
<span class='label'>Supplier Product Name</span>
<asp:TextBox EnableViewState="false" ID="productname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkdistribution_fk">
<span class='label'>Distribution</span>
<asp:DropDownList ID="distribution_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkprofile_fk">
<span class='label'>Profile Class</span>
<asp:DropDownList ID="profile_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkcontractlength">
<span class='label'>Contract Length<span class='pop'>The length of the contract in months</span></span>
<asp:TextBox EnableViewState="false" ID="contractlength" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkvalidfrom">
<span class='label'>Valid From<span class='pop'>When are the prices valid from</span></span>
<asp:TextBox EnableViewState="false" ID="validfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkvalidto">
<span class='label'>Valid To<span class='pop'>When are the prices valid to</span></span>
<asp:TextBox EnableViewState="false" ID="validto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkstandingcharge">
<span class='label'>Standing Charge<span class='pop'>Standing Charge per day</span></span>
<asp:TextBox EnableViewState="false" ID="standingcharge" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkrate_single">
<span class='label'>Single Rate</span>
<asp:TextBox EnableViewState="false" ID="rate_single" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkrate_day">
<span class='label'>Day Rate</span>
<asp:TextBox EnableViewState="false" ID="rate_day" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkrate_night">
<span class='label'>Night Rate</span>
<asp:TextBox EnableViewState="false" ID="rate_night" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkrate_other">
<span class='label'>Other Rate</span>
<asp:TextBox EnableViewState="false" ID="rate_other" runat="server" cssClass="input_dbl" />
</li>
</ul>

