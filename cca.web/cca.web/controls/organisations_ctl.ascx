<%@ Control Language="VB" AutoEventWireup="false" CodeFile="organisations_ctl.ascx.vb" Inherits="organisations_ctl" %>
<h5>organisations</h5>
<ul class='formcontrol'>
<li runat="server" id="blkCustid">
<span class='label'>organisation_pk</span>
<asp:TextBox EnableViewState="false" ID="Custid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkcustomername">
<span class='label'>Organisation Name<span class='pop'>Name of the Customer in Contract Manager</span></span>
<asp:TextBox EnableViewState="false" ID="customername" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkaddress_1">
<span class='label'>Address 1</span>
<asp:TextBox EnableViewState="false" ID="address_1" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkaddress_2">
<span class='label'>Address 2</span>
<asp:TextBox EnableViewState="false" ID="address_2" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkaddress_3">
<span class='label'>Address 3</span>
<asp:TextBox EnableViewState="false" ID="address_3" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkAddress_4">
<span class='label'>Address 4</span>
<asp:TextBox EnableViewState="false" ID="Address_4" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcounty_fk">
<span class='label'>county</span>
<asp:DropDownList ID="county_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkPcode">
<span class='label'>Post Code</span>
<asp:TextBox EnableViewState="false" ID="Pcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcountry_fk">
<span class='label'>country</span>
<asp:DropDownList ID="country_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkContact">
<span class='label'>Contact</span>
<asp:TextBox EnableViewState="false" ID="Contact" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcoregnum">
<span class='label'>Company Registration Number</span>
<asp:TextBox EnableViewState="false" ID="coregnum" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkprodDecisionPoint">
<span class='label'>prodDecisionPoint</span>
<asp:TextBox EnableViewState="false" ID="prodDecisionPoint" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodElecneg">
<span class='label'>prodElecneg</span>
<asp:TextBox EnableViewState="false" ID="prodElecneg" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodgasneg">
<span class='label'>prodgasneg</span>
<asp:TextBox EnableViewState="false" ID="prodgasneg" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodccl">
<span class='label'>prodccl</span>
<asp:TextBox EnableViewState="false" ID="prodccl" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodenergyaction">
<span class='label'>prodenergyaction</span>
<asp:TextBox EnableViewState="false" ID="prodenergyaction" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodbillaudit">
<span class='label'>prodbillaudit</span>
<asp:TextBox EnableViewState="false" ID="prodbillaudit" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkproddataserv">
<span class='label'>proddataserv</span>
<asp:TextBox EnableViewState="false" ID="proddataserv" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkProdWaterAudit">
<span class='label'>ProdWaterAudit</span>
<asp:TextBox EnableViewState="false" ID="ProdWaterAudit" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkProdWaterMgmt">
<span class='label'>ProdWaterMgmt</span>
<asp:TextBox EnableViewState="false" ID="ProdWaterMgmt" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkProdCRCAttain">
<span class='label'>ProdCRCAttain</span>
<asp:TextBox EnableViewState="false" ID="ProdCRCAttain" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkProdCRCMaintain">
<span class='label'>ProdCRCMaintain</span>
<asp:TextBox EnableViewState="false" ID="ProdCRCMaintain" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkcustorprospect">
<span class='label'>custorprospect</span>
<asp:TextBox EnableViewState="false" ID="custorprospect" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCCLexemption">
<span class='label'>CCLexemption</span>
<asp:TextBox EnableViewState="false" ID="CCLexemption" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCCLexemptionfromdate">
<span class='label'>CCLexemptionfromdate</span>
<asp:TextBox EnableViewState="false" ID="CCLexemptionfromdate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkCCLcertapplied_activesupply">
<span class='label'>CCLcertapplied_activesupply</span>
<asp:TextBox EnableViewState="false" ID="CCLcertapplied_activesupply" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCCLcertapplied_pendingsupply">
<span class='label'>CCLcertapplied_pendingsupply</span>
<asp:TextBox EnableViewState="false" ID="CCLcertapplied_pendingsupply" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodTelco">
<span class='label'>prodTelco</span>
<asp:TextBox EnableViewState="false" ID="prodTelco" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodMT">
<span class='label'>prodMT</span>
<asp:TextBox EnableViewState="false" ID="prodMT" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodTRIAD">
<span class='label'>prodTRIAD</span>
<asp:TextBox EnableViewState="false" ID="prodTRIAD" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkpricesensitivity">
<span class='label'>pricesensitivity</span>
<asp:TextBox EnableViewState="false" ID="pricesensitivity" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkumlloyaltyvalue">
<span class='label'>umlloyaltyvalue</span>
<asp:TextBox EnableViewState="false" ID="umlloyaltyvalue" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkVATnum">
<span class='label'>VAT Number</span>
<asp:TextBox EnableViewState="false" ID="VATnum" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkFKaccmgrid">
<span class='label'>Account Manager</span>
<asp:TextBox EnableViewState="false" ID="FKaccmgrid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkProdBillCheck">
<span class='label'>ProdBillCheck</span>
<asp:TextBox EnableViewState="false" ID="ProdBillCheck" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkdtmCreated">
<span class='label'>dtmCreated</span>
<asp:TextBox EnableViewState="false" ID="dtmCreated" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkdtmModified">
<span class='label'>dtmModified</span>
<asp:TextBox EnableViewState="false" ID="dtmModified" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkintUserCreatedBy">
<span class='label'>intUserCreatedBy</span>
<asp:TextBox EnableViewState="false" ID="intUserCreatedBy" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintUserModifiedBy">
<span class='label'>intUserModifiedBy</span>
<asp:TextBox EnableViewState="false" ID="intUserModifiedBy" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintOptimaCustomerPK">
<span class='label'>intOptimaCustomerPK</span>
<asp:TextBox EnableViewState="false" ID="intOptimaCustomerPK" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintDataServicesGroupSend">
<span class='label'>intDataServicesGroupSend</span>
<asp:TextBox EnableViewState="false" ID="intDataServicesGroupSend" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkbitDisabled">
<span class='label'>bitDisabled</span>
<asp:CheckBox ID="bitDisabled" runat="server" cssClass="input_chk" />
</li>
<li runat="server" id="blkvarCustomerNote">
<span class='label'>varCustomerNote</span>
<asp:TextBox EnableViewState="false" ID="varCustomerNote" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkintMandCRef">
<span class='label'>intMandCRef</span>
<asp:TextBox EnableViewState="false" ID="intMandCRef" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintEnCoreRef">
<span class='label'>intEnCoreRef</span>
<asp:TextBox EnableViewState="false" ID="intEnCoreRef" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkvarAXRef">
<span class='label'>varAXRef</span>
<asp:TextBox EnableViewState="false" ID="varAXRef" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkintEnterpriseManagerFK">
<span class='label'>intEnterpriseManagerFK</span>
<asp:TextBox EnableViewState="false" ID="intEnterpriseManagerFK" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintSubjectExpertFK">
<span class='label'>intSubjectExpertFK</span>
<asp:TextBox EnableViewState="false" ID="intSubjectExpertFK" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintSummitRef">
<span class='label'>intSummitRef</span>
<asp:TextBox EnableViewState="false" ID="intSummitRef" runat="server" cssClass="input_num"  />
</li>
</ul>

