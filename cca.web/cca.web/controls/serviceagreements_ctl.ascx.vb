Partial Class serviceagreements_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oserviceagreements As Objects.serviceagreements
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_serviceproducts_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.serviceproduct_fk.Items.Count=0 Then
        Me.serviceproduct_fk.DataSource=CDC.ReadDataTable(CO)
        Me.serviceproduct_fk.DataTextField = "value"
        Me.serviceproduct_fk.DataValueField = "pk"
        Try
            Me.serviceproduct_fk.DataBind
        Catch Ex as Exception
            Me.serviceproduct_fk.SelectedValue=-1
            Me.serviceproduct_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oserviceagreements is Nothing then
oserviceagreements = new Objects.serviceagreements(currentuser,actualuser,pk,CDC)
End If
Me.serviceproduct_fk.SelectedValue=oserviceagreements.serviceproduct_fk
Me.date_from.Text=oserviceagreements.date_from.ToString("dd MMM yyyy")
If Me.date_from.Text="01 Jan 0001" then Me.date_from.Text=""
Me.date_from.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.date_from.Attributes.Add("onBlur", "clearNotice()")
Me.date_to.Text=oserviceagreements.date_to.ToString("dd MMM yyyy")
If Me.date_to.Text="01 Jan 0001" then Me.date_to.Text=""
Me.date_to.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.date_to.Attributes.Add("onBlur", "clearNotice()")
Me.otherinformation.Text=oserviceagreements.otherinformation
Me.rollingcontractclause.Checked=oserviceagreements.rollingcontractclause
Me.organisation_fk.SelectedValue=oserviceagreements.organisation_fk
Me.serviceagreementnumber.Text=oserviceagreements.serviceagreementnumber
_CurrentPk=oserviceagreements.serviceagreement_pk
Status=oserviceagreements.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oserviceagreements is Nothing then
oserviceagreements = new Objects.serviceagreements(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oserviceagreements.serviceproduct_fk=Me.serviceproduct_fk.SelectedValue
oserviceagreements.date_from=CommonFN.CheckEmptyDate(Me.date_from.Text)
oserviceagreements.date_to=CommonFN.CheckEmptyDate(Me.date_to.Text)
oserviceagreements.otherinformation=Me.otherinformation.Text
oserviceagreements.rollingcontractclause=Me.rollingcontractclause.Checked
oserviceagreements.organisation_fk=Me.organisation_fk.SelectedValue
oserviceagreements.serviceagreementnumber=Me.serviceagreementnumber.Text
result=oserviceagreements.Save()
if not result then throw oserviceagreements.LastError
_CurrentPk=oserviceagreements.serviceagreement_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oserviceagreements.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceagreements is Nothing then
   oserviceagreements = new Objects.serviceagreements(currentuser,actualuser,pk,CDC)
  End If
  oserviceagreements.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceagreements is Nothing then
   oserviceagreements = new Objects.serviceagreements(currentuser,actualuser,pk,CDC)
  End If
  oserviceagreements.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oserviceagreements is Nothing then
   oserviceagreements = new Objects.serviceagreements(currentuser,pk,CDC)
  End If
  oserviceagreements.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.serviceproduct_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkserviceproduct_fk.Visible=IIF(RL>=0,True,False)
  Me.date_from.Enabled=IIF(WL>=0,True,False)
  Me.blkdate_from.Visible=IIF(RL>=0,True,False)
  Me.date_to.Enabled=IIF(WL>=0,True,False)
  Me.blkdate_to.Visible=IIF(RL>=0,True,False)
  Me.otherinformation.Enabled=IIF(WL>=0,True,False)
  Me.blkotherinformation.Visible=IIF(RL>=0,True,False)
  Me.rollingcontractclause.Enabled=IIF(WL>=0,True,False)
  Me.blkrollingcontractclause.Visible=IIF(RL>=0,True,False)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  Me.serviceagreementnumber.Enabled=IIF(WL>=0,True,False)
  Me.blkserviceagreementnumber.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

