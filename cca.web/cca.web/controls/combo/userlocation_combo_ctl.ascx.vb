Partial Class userlocation_combo_ctl
   Inherits DaveControl
   Public PSM As MySM

   Public WithEvents ouserlocation As Objects.userlocations
   Public WithEvents olocation As Objects.locations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=-1
   Dim _Status as Integer=0
   Dim _STCurrentPk as Integer=-1
   Dim _STStatus as Integer=0

   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       ControlInit
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub

   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property

   Public ReadOnly Property STCurrentPk() as Integer
        Get
            Return _STCurrentPk
        End Get
   End Property

   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property

  Public Sub ControlInit()
CO=new SqlClient.SqlCommand("rkg_countys_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.county_fk.DataSource=CDC.ReadDataTable(CO)
Me.county_fk.DataTextField = "value"
Me.county_fk.DataValueField = "pk"
Try
Me.county_fk.DataBind
Catch Ex as Exception
Me.county_fk.SelectedValue=-1
Me.county_fk.DataBind
End Try
CO=new SqlClient.SqlCommand("rkg_countrys_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.country_fk.DataSource=CDC.ReadDataTable(CO)
Me.country_fk.DataTextField = "value"
Me.country_fk.DataValueField = "pk"
Try
Me.country_fk.DataBind
Catch Ex as Exception
Me.country_fk.SelectedValue=-1
Me.country_fk.DataBind
End Try
CO=new SqlClient.SqlCommand("rkg_locationuses_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.locationuse_fk.DataSource=CDC.ReadDataTable(CO)
Me.locationuse_fk.DataTextField = "value"
Me.locationuse_fk.DataValueField = "pk"
Try
Me.locationuse_fk.DataBind
Catch Ex as Exception
Me.locationuse_fk.SelectedValue=-1
Me.locationuse_fk.DataBind
End Try
  End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
End Sub
Public Sub SetObjects(ByVal CurrentUser as Integer,ByVal ActualUser as Integer,Optional ByVal pk as Integer=-1)
    If ouserlocation is Nothing then
        ouserlocation = new Objects.userlocations(currentuser,actualuser,pk,CDC)
        If ouserlocation.location_fk>0 then
            olocation = new Objects.locations(currentuser,actualuser,ouserlocation.location_fk,CDC)
        Else
            olocation = new Objects.locations(currentuser,actualuser,-1,CDC)
        End If
    End If
    If olocation is Nothing then
        If ouserlocation.location_fk>0 then
            olocation = new Objects.locations(currentuser,actualuser,ouserlocation.location_fk,CDC)
        Else
            olocation = new Objects.locations(currentuser,actualuser,-1,CDC)
        End If
    End If
End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1)
    SetObjects(currentuser,actualuser,pk)
Me.buildingprefix.Text=olocation.buildingprefix
Me.streetnumber.Text=olocation.streetnumber
Me.street.Text=olocation.street
Me.district.Text=olocation.district
Me.town.Text=olocation.town
Try
Me.county_fk.SelectedValue=olocation.county_fk
Catch excounty_fk as Exception
Me.county_fk.SelectedValue=-1
End Try
Try
Me.country_fk.SelectedValue=olocation.country_fk
Catch excountry_fk as Exception
Me.country_fk.SelectedValue=-1
End Try
Me.postcode.Text=olocation.postcode
Me.longitude.Text=olocation.longitude
Me.longitude.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.longitude.Attributes.Add("onBlur", "clearNotice()")
Me.latitude.Text=olocation.latitude
Me.latitude.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.latitude.Attributes.Add("onBlur", "clearNotice()")
SM.subId=ouserlocation.location_fk
Try
Me.locationuse_fk.SelectedValue=ouserlocation.locationuse_fk
Catch exlocationuse_fk as Exception
Me.locationuse_fk.SelectedValue=-1
End Try
_CurrentPk=ouserlocation.userlocation_pk
_STCurrentPk=olocation.location_pk
Status=ouserlocation.rowstatus OR olocation.rowstatus
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1) as boolean
    SetObjects(currentuser,actualuser,pk)
olocation.buildingprefix=Me.buildingprefix.Text
olocation.streetnumber=Me.streetnumber.Text
olocation.street=Me.street.Text
olocation.district=Me.district.Text
olocation.town=Me.town.Text
olocation.county_fk=Me.county_fk.SelectedValue
olocation.country_fk=Me.country_fk.SelectedValue
olocation.postcode=Me.postcode.Text
olocation.longitude=Me.longitude.Text
olocation.latitude=Me.latitude.Text
Dim STResult as boolean=False
Try
STResult=olocation.Save()
if not STResult then throw olocation.LastError
_STCurrentPk=olocation.location_pk
SM.subId=olocation.location_pk
Catch Ex as Exception
STResult=False
LastError=Ex
SM.subId=-1
End Try
Try
ouserlocation.user_fk=SM.targetuser
ouserlocation.location_fk=SM.subId
ouserlocation.locationuse_fk=Me.locationuse_fk.SelectedValue
Catch MainEx as Exception
LastError=MainEx
End Try
Dim result as boolean=false
Try
Result=ouserlocation.Save()
if not Result then throw ouserlocation.LastError
_CurrentPk=ouserlocation.userlocation_pk
Catch Ex as Exception
Result=False
LastError=Ex
End Try
SM.linkId=_CurrentPk
Status=ouserlocation.rowstatus OR olocation.rowstatus
Return result AND STResult
End Function
Public Function Enable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserlocation is Nothing then
ouserlocation = new Objects.userlocations(currentuser,actualuser,pk,CDC)
End If
ouserlocation.rowstatus=0
Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserlocation is Nothing then
ouserlocation = new Objects.userlocations(currentuser,actualuser,pk,CDC)
End If
ouserlocation.rowstatus=1
Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserlocation is Nothing then
ouserlocation = new Objects.userlocations(currentuser,actualuser,pk,CDC)
End If
ouserlocation.rowstatus=2
Return Save(currentuser,actualuser,pk)
End Function
Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
Me.buildingprefix.Enabled=IIF(WL>=0,True,False)
Me.blkbuildingprefix.Visible=IIF(RL>=0,True,False)
Me.streetnumber.Enabled=IIF(WL>=0,True,False)
Me.blkstreetnumber.Visible=IIF(RL>=0,True,False)
Me.street.Enabled=IIF(WL>=0,True,False)
Me.blkstreet.Visible=IIF(RL>=0,True,False)
Me.district.Enabled=IIF(WL>=0,True,False)
Me.blkdistrict.Visible=IIF(RL>=0,True,False)
Me.town.Enabled=IIF(WL>=0,True,False)
Me.blktown.Visible=IIF(RL>=0,True,False)
Me.county_fk.Enabled=IIF(WL>=0,True,False)
Me.blkcounty_fk.Visible=IIF(RL>=0,True,False)
Me.country_fk.Enabled=IIF(WL>=0,True,False)
Me.blkcountry_fk.Visible=IIF(RL>=0,True,False)
Me.postcode.Enabled=IIF(WL>=0,True,False)
Me.blkpostcode.Visible=IIF(RL>=0,True,False)
Me.longitude.Enabled=IIF(WL>=0,True,False)
Me.blklongitude.Visible=IIF(RL>=0,True,False)
Me.latitude.Enabled=IIF(WL>=0,True,False)
Me.blklatitude.Visible=IIF(RL>=0,True,False)
Me.locationuse_fk.Enabled=IIF(WL>=0,True,False)
Me.blklocationuse_fk.Visible=IIF(RL>=0,True,False)
End Sub
End Class
