Partial Class agreementthroughputs_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oagreementthroughputs As Objects.agreementthroughputs
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_agreements_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.agreement_fk.Items.Count=0 Then
        Me.agreement_fk.DataSource=CDC.ReadDataTable(CO)
        Me.agreement_fk.DataTextField = "value"
        Me.agreement_fk.DataValueField = "pk"
        Try
            Me.agreement_fk.DataBind
        Catch Ex as Exception
            Me.agreement_fk.SelectedValue=-1
            Me.agreement_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_throughputs_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.throughput_fk.Items.Count=0 Then
        Me.throughput_fk.DataSource=CDC.ReadDataTable(CO)
        Me.throughput_fk.DataTextField = "value"
        Me.throughput_fk.DataValueField = "pk"
        Try
            Me.throughput_fk.DataBind
        Catch Ex as Exception
            Me.throughput_fk.SelectedValue=-1
            Me.throughput_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_frequencys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.datafrequency_fk.Items.Count=0 Then
        Me.datafrequency_fk.DataSource=CDC.ReadDataTable(CO)
        Me.datafrequency_fk.DataTextField = "value"
        Me.datafrequency_fk.DataValueField = "pk"
        Try
            Me.datafrequency_fk.DataBind
        Catch Ex as Exception
            Me.datafrequency_fk.SelectedValue=-1
            Me.datafrequency_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oagreementthroughputs is Nothing then
oagreementthroughputs = new Objects.agreementthroughputs(currentuser,actualuser,pk,CDC)
End If
Me.agreement_fk.SelectedValue=oagreementthroughputs.agreement_fk
Me.throughput_fk.SelectedValue=oagreementthroughputs.throughput_fk
Me.datafrequency_fk.SelectedValue=oagreementthroughputs.datafrequency_fk
_CurrentPk=oagreementthroughputs.agreementthroughput_pk
Status=oagreementthroughputs.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oagreementthroughputs is Nothing then
oagreementthroughputs = new Objects.agreementthroughputs(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oagreementthroughputs.agreement_fk=Me.agreement_fk.SelectedValue
oagreementthroughputs.throughput_fk=Me.throughput_fk.SelectedValue
oagreementthroughputs.datafrequency_fk=Me.datafrequency_fk.SelectedValue
result=oagreementthroughputs.Save()
if not result then throw oagreementthroughputs.LastError
_CurrentPk=oagreementthroughputs.agreementthroughput_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oagreementthroughputs.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oagreementthroughputs is Nothing then
   oagreementthroughputs = new Objects.agreementthroughputs(currentuser,actualuser,pk,CDC)
  End If
  oagreementthroughputs.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oagreementthroughputs is Nothing then
   oagreementthroughputs = new Objects.agreementthroughputs(currentuser,actualuser,pk,CDC)
  End If
  oagreementthroughputs.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oagreementthroughputs is Nothing then
   oagreementthroughputs = new Objects.agreementthroughputs(currentuser,pk,CDC)
  End If
  oagreementthroughputs.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.agreement_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkagreement_fk.Visible=IIF(RL>=0,True,False)
  Me.throughput_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkthroughput_fk.Visible=IIF(RL>=0,True,False)
  Me.datafrequency_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkdatafrequency_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

