<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrix2sources_ctl.ascx.vb" Inherits="matrix2sources_ctl" %>
<h5>matrix2sources</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmatrixfile_fk">
<span class='label'>Matrix File<span class='pop'>Which file was the price uploaded from?</span></span>
<asp:DropDownList ID="matrixfile_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksupplier_fk">
<span class='label'>Supplier Name</span>
<asp:DropDownList ID="supplier_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkproduct">
<span class='label'>Supplier Product Name</span>
<asp:TextBox EnableViewState="false" ID="product" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkldz_fk">
<span class='label'>LDZ</span>
<asp:DropDownList ID="ldz_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkband_start">
<span class='label'>Band Start<span class='pop'>kWh</span></span>
<asp:TextBox EnableViewState="false" ID="band_start" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkband_end">
<span class='label'>Band End<span class='pop'>kWh</span></span>
<asp:TextBox EnableViewState="false" ID="band_end" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkvalidfrom">
<span class='label'>Valid From<span class='pop'>When are the prices valid from</span></span>
<asp:TextBox EnableViewState="false" ID="validfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkvalidto">
<span class='label'>Valid To<span class='pop'>When are the prices valid to</span></span>
<asp:TextBox EnableViewState="false" ID="validto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkstandingcharge">
<span class='label'>Standing Charge<span class='pop'>Standing Charge per day</span></span>
<asp:TextBox EnableViewState="false" ID="standingcharge" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkrate">
<span class='label'>Rate</span>
<asp:TextBox EnableViewState="false" ID="rate" runat="server" cssClass="input_dbl" />
</li>
</ul>

