<%@ WebHandler Language="VB" Class="grouproles" %>
Public Class grouproles
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim gv As New Repeater
        Dim dt As DataTable
        Dim CO As New SqlCommand
        Dim rq As Object = Request.QueryString
        Try
            Response.Clear()
            gv.EnableViewState = False
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "[rsp_grouproles_plain]"
            If rq("g") = "" Then
                CO.Parameters.AddWithValue("@group", 0)
            Else
                CO.Parameters.AddWithValue("@group", rq("g"))
            End If
            dt = CDC.ReadDataTable(CO)
            gv.ItemTemplate = New ListItemTemplate()
            gv.DataSource = dt
            gv.DataBind()
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
            gv.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
        Catch LE As Exception
            Response.Write(LE.Message)
        End Try
    End Sub

    Public Class ListItemTemplate
        Implements ITemplate
        Public Sub New()
        End Sub

        Private Sub ITemplate_InstantiateIn(ByVal container As System.Web.UI.Control) Implements ITemplate.InstantiateIn
            Dim lit As New Literal
            AddHandler lit.DataBinding, AddressOf lbl_DataBinding
            container.Controls.Add(lit)
        End Sub

        Private Sub lbl_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim lit As Literal = DirectCast(sender, Literal)
            Dim data As Object = DirectCast(lit.NamingContainer, RepeaterItem).DataItem
            Dim dbr As DataRowView = DirectCast(data, DataRowView)

            Dim s As String = ""
        
            s &= "<li><a href='javascript:reqpermissions("
            s &= dbr.Item("role_pk")
            s &= ")' >[ i ]</a>"
            s &= dbr.Item("rolename")
            s &= " ::: "
            s &= dbr.Item("orgname")
            s &= "</li>"
        
            lit.Text = s
        

        End Sub

    End Class

End Class

