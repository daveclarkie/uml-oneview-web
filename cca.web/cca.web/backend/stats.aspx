<%@ Page Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="stats.aspx.vb" Inherits="backend_stats" title="Untitled Page" %>
<%@ Register Src="../controls/nav/notices.ascx" TagName="notices" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
<div class="content">
    <h3>System Stats</h3>
    <asp:GridView ID="grdStats" runat="server">
    </asp:GridView>
</div>
<div class="navigation">
    <uc1:notices ID="Notices1" runat="server" />
</div>
</div>
</asp:Content>

