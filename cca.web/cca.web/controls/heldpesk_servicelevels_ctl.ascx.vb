Partial Class heldpesk_servicelevels_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oheldpesk_servicelevels As Objects.heldpesk_servicelevels
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oheldpesk_servicelevels is Nothing then
oheldpesk_servicelevels = new Objects.heldpesk_servicelevels(currentuser,actualuser,pk,CDC)
End If
Me.servicelevelname.Text=oheldpesk_servicelevels.servicelevelname
_CurrentPk=oheldpesk_servicelevels.heldpesk_servicelevel_pk
Status=oheldpesk_servicelevels.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oheldpesk_servicelevels is Nothing then
oheldpesk_servicelevels = new Objects.heldpesk_servicelevels(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oheldpesk_servicelevels.servicelevelname=Me.servicelevelname.Text
result=oheldpesk_servicelevels.Save()
if not result then throw oheldpesk_servicelevels.LastError
_CurrentPk=oheldpesk_servicelevels.heldpesk_servicelevel_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oheldpesk_servicelevels.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oheldpesk_servicelevels is Nothing then
   oheldpesk_servicelevels = new Objects.heldpesk_servicelevels(currentuser,actualuser,pk,CDC)
  End If
  oheldpesk_servicelevels.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oheldpesk_servicelevels is Nothing then
   oheldpesk_servicelevels = new Objects.heldpesk_servicelevels(currentuser,actualuser,pk,CDC)
  End If
  oheldpesk_servicelevels.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oheldpesk_servicelevels is Nothing then
   oheldpesk_servicelevels = new Objects.heldpesk_servicelevels(currentuser,pk,CDC)
  End If
  oheldpesk_servicelevels.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.servicelevelname.Enabled=IIF(WL>=0,True,False)
  Me.blkservicelevelname.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

