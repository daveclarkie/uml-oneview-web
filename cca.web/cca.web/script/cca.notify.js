﻿
var sysnotice;
var cClear=0;
var cInfo=1;
var cWarn=2;
var cError=3;

function provideSystemNotices(){
    try {
            var locElem=document.getElementById("sysnotice");
            if ( !locElem ) {
                var mkElem=document.getElementById("ln");
                if ( !mkElem ) {mkElem=document.createElement("ln");} 
                var sn=document.createElement("h3");
                sn.setAttribute('id','sysnotice');
                sn.setAttribute('style','background-color:rgba(255,255,255,0);');
                var fi=document.createElement("div");
                fi.setAttribute('id','sysnoticeinfo');

                var st=document.createTextNode("System Notices");
                var pt=document.createTextNode(" ");

                pi.appendChild(pt);
                sn.appendChild(st);
                mkElem.appendChild(sn);
                mkElem.appendChild(fi);
            }
    }
    catch (e){
        alert(e);
    }
    sysnotice=document.getElementById("sysnotice"); 
}

function setColour(cl){
    var bgC=sysnotice.style.backgroundColor;
    var bgV=0;
    switch (bgC){
        case "Red": 
            bgV=cError; break;   
        case "Orange": 
            bgV=cWarn; break;   
        case "Green": 
            bgV=cInfo; break;   
        default:
            bgV=cClear;
    }
    if (cl>bgV){
        switch (cl){
            case cError: 
                bcG="Red"; break; 
            case cWarn: 
                bcG="Orange"; break; 
            case cInfo: 
                bcG="Green"; break; 
        }
    } else {
        if (cl==cClear) { bcG="Transparent";}
    }
    sysnotice.style.backgroundColor=bgC;
}

function setError(newValue) {
    if (!sysnotice) { provideSystemNotices();}
    setColour(cError);    
    chgNamedElementText("sysnoticeinfo",chgReplace,newValue);
}

function setWarning(newValue) {
    if (!sysnotice) { provideSystemNotices();}
    setColour(cWarn);    
    chgNamedElementText("sysnoticeinfo",chgReplace,newValue);
}

function setInfo(newValue) {
    if (!sysnotice) { provideSystemNotices();}
    setColour(cInfo);    
    chgNamedElementText("sysnoticeinfo",chgReplace,newValue);
}

function setClear() {
    if (!sysnotice) { provideSystemNotices();}
    setColour(cClear);    
    chgNamedElementText("sysnoticeinfo",chgClear);
}

