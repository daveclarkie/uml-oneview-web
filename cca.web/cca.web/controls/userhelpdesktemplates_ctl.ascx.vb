Partial Class userhelpdesktemplates_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ouserhelpdesktemplates As Objects.userhelpdesktemplates
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_helpdesktemplates_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.helpdesktemplate_fk.Items.Count=0 Then
        Me.helpdesktemplate_fk.DataSource=CDC.ReadDataTable(CO)
        Me.helpdesktemplate_fk.DataTextField = "value"
        Me.helpdesktemplate_fk.DataValueField = "pk"
        Try
            Me.helpdesktemplate_fk.DataBind
        Catch Ex as Exception
            Me.helpdesktemplate_fk.SelectedValue=-1
            Me.helpdesktemplate_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ouserhelpdesktemplates is Nothing then
ouserhelpdesktemplates = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
End If
Me.user_fk.SelectedValue=ouserhelpdesktemplates.user_fk
Me.helpdesktemplate_fk.SelectedValue=ouserhelpdesktemplates.helpdesktemplate_fk
_CurrentPk=ouserhelpdesktemplates.userhelpdesktemplate_pk
Status=ouserhelpdesktemplates.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ouserhelpdesktemplates is Nothing then
ouserhelpdesktemplates = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ouserhelpdesktemplates.user_fk=Me.user_fk.SelectedValue
ouserhelpdesktemplates.helpdesktemplate_fk=Me.helpdesktemplate_fk.SelectedValue
result=ouserhelpdesktemplates.Save()
if not result then throw ouserhelpdesktemplates.LastError
_CurrentPk=ouserhelpdesktemplates.userhelpdesktemplate_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ouserhelpdesktemplates.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserhelpdesktemplates is Nothing then
   ouserhelpdesktemplates = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
  End If
  ouserhelpdesktemplates.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserhelpdesktemplates is Nothing then
   ouserhelpdesktemplates = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
  End If
  ouserhelpdesktemplates.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserhelpdesktemplates is Nothing then
   ouserhelpdesktemplates = new Objects.userhelpdesktemplates(currentuser,pk,CDC)
  End If
  ouserhelpdesktemplates.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  Me.helpdesktemplate_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkhelpdesktemplate_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

