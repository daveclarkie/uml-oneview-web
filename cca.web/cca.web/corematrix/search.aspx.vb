Partial Class corematrix_search
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Public Sub Search()
        Dim sfCustomer As String = LimitSearchTerms(txtCustomername.Text)
        Dim sfMeter As String = LimitSearchTerms(txtMeternumber.Text)
        

        If sfCustomer.Length > 0 Or sfMeter.Length > 0 Then
            CO = New SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_search_matrix"
            CDC = New DataCommon
            CO.Parameters.AddWithValue("@customername", sfCustomer)
            CO.Parameters.AddWithValue("@meternumber", sfMeter)
            CO.Parameters.AddWithValue("@w", "%")
            CO.Parameters.AddWithValue("@user", SM.currentuser)

            Dim dt As DataTable = CDC.ReadDataTable(CO)
            Dim objPds As New PagedDataSource
            objPds.DataSource = dt.DefaultView
            objPds.AllowPaging = True
            objPds.PageSize = 12
            Dim curpage As Integer
            If ViewState("Page") IsNot Nothing Then
                curpage = Convert.ToInt32(ViewState("Page"))
            Else
                ViewState("Page") = 1
                curpage = 1
            End If

            objPds.CurrentPageIndex = curpage - 1
            lblSearchPage.Text = "Page: " + (curpage).ToString() + " of " + objPds.PageCount.ToString()
            If objPds.PageCount > 1 Then
                lnkSearchNext.Text = " Next "
                lnkSearchPrev.Text = " Previous "
            Else
                lnkSearchNext.Text = ""
                lnkSearchPrev.Text = ""
            End If

            rptMatrixSearch.DataSource = objPds
            rptMatrixSearch.DataBind()

            If objPds.Count = 0 Then AddNotice("setNotice('No matrices matching your criteria were found.');")

            CO.Dispose()
            CO = Nothing
        Else
            AddNotice("setNotice('No search criteria entered');")
        End If
    End Sub

    Protected Sub lnkPrev_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        If ViewState("Page") > 1 Then
            ViewState("Page") = Convert.ToInt32(ViewState("Page")) - 1

            ' Reload control
            Search()
        End If
    End Sub
    Protected Sub lnkNext_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the next page
        ViewState("Page") = Convert.ToInt32(ViewState("Page")) + 1

        ' Reload control
        Search()
    End Sub
    Protected Sub lnkFirst_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        ViewState("Page") = Convert.ToInt32(1)

        ' Reload control
        Search()
    End Sub
    Protected Sub lnkLast_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        Dim x As Integer = TotalRowCount
        ViewState("Page") = Convert.ToInt32(PageCount)

        ' Reload control
        Search()
    End Sub

    Private Property TotalRowCount() As Integer
        Get
            Dim o As Object = ViewState("TotalRowCount")
            If o Is Nothing Then
                Return -1
            Else
                Return CInt(o)
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TotalRowCount") = value
        End Set
    End Property
    Private ReadOnly Property PageIndex() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageIndex")) Then
                Return Convert.ToInt32(Request.QueryString("pageIndex"))
            Else
                Return 0
            End If
        End Get
    End Property
    Private ReadOnly Property PageSize() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageSize")) Then
                Return Convert.ToInt32(Request.QueryString("pageSize"))
            Else
                Return 4
            End If
        End Get
    End Property
    Private ReadOnly Property PageCount() As Integer
        Get
            If TotalRowCount <= 0 OrElse PageSize <= 0 Then
                Return 1
            Else
                Return ((TotalRowCount + PageSize) - 1) / PageSize
            End If
        End Get
    End Property

    '================================
    '   "Validation" Procs
    '================================

    Private Function LimitSearchTerms(ByVal original As String) As String
        CDC.Sanitize(original)
        Return original
    End Function

    Private Function LimitSearchTermsNumeric(ByVal original As String) As Integer
        CDC.Sanitize(original)
        Dim i As Integer = 0
        If Integer.TryParse(original, i) Then
            Return i
        Else
            Return 0
        End If
    End Function


    '================================
    '   Page Specific Events
    '================================

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Search()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub
    Public Function returnselect(ByVal rowstatus As Integer, ByVal matrix_pk As Integer) As String
        Dim _return As String = ""

        If rowstatus = 0 Then
            _return = "<a href='default.aspx?pk=" & matrix_pk & "&pg=mp" & Local.noCache & "'><h6>select</h6></a>"
        Else
            _return = "<a><h6 style='background-color:red;'>disabled</h6></a>"
        End If
        Return _return
    End Function

End Class
