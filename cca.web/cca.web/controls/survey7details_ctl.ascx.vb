Partial Class survey7details_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osurvey7details As Objects.survey7details
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.helpdesk_id.Text="0"
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.selecteduser_fk.Items.Count=0 Then
        Me.selecteduser_fk.DataSource=CDC.ReadDataTable(CO)
        Me.selecteduser_fk.DataTextField = "value"
        Me.selecteduser_fk.DataValueField = "pk"
        Try
            Me.selecteduser_fk.DataBind
        Catch Ex as Exception
            Me.selecteduser_fk.SelectedValue=-1
            Me.selecteduser_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.question1_surveyanswerfivescale_fk.Items.Count=0 Then
        Me.question1_surveyanswerfivescale_fk.DataSource=CDC.ReadDataTable(CO)
        Me.question1_surveyanswerfivescale_fk.DataTextField = "value"
        Me.question1_surveyanswerfivescale_fk.DataValueField = "pk"
        Try
            Me.question1_surveyanswerfivescale_fk.DataBind
        Catch Ex as Exception
            Me.question1_surveyanswerfivescale_fk.SelectedValue=-1
            Me.question1_surveyanswerfivescale_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.question2_surveyanswerfivescale_fk.Items.Count=0 Then
        Me.question2_surveyanswerfivescale_fk.DataSource=CDC.ReadDataTable(CO)
        Me.question2_surveyanswerfivescale_fk.DataTextField = "value"
        Me.question2_surveyanswerfivescale_fk.DataValueField = "pk"
        Try
            Me.question2_surveyanswerfivescale_fk.DataBind
        Catch Ex as Exception
            Me.question2_surveyanswerfivescale_fk.SelectedValue=-1
            Me.question2_surveyanswerfivescale_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.question3_surveyanswerfivescale_fk.Items.Count=0 Then
        Me.question3_surveyanswerfivescale_fk.DataSource=CDC.ReadDataTable(CO)
        Me.question3_surveyanswerfivescale_fk.DataTextField = "value"
        Me.question3_surveyanswerfivescale_fk.DataValueField = "pk"
        Try
            Me.question3_surveyanswerfivescale_fk.DataBind
        Catch Ex as Exception
            Me.question3_surveyanswerfivescale_fk.SelectedValue=-1
            Me.question3_surveyanswerfivescale_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.question4_surveyanswerfivescale_fk.Items.Count=0 Then
        Me.question4_surveyanswerfivescale_fk.DataSource=CDC.ReadDataTable(CO)
        Me.question4_surveyanswerfivescale_fk.DataTextField = "value"
        Me.question4_surveyanswerfivescale_fk.DataValueField = "pk"
        Try
            Me.question4_surveyanswerfivescale_fk.DataBind
        Catch Ex as Exception
            Me.question4_surveyanswerfivescale_fk.SelectedValue=-1
            Me.question4_surveyanswerfivescale_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_surveyanswerfivescales_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.question5_surveyanswerfivescale_fk.Items.Count=0 Then
        Me.question5_surveyanswerfivescale_fk.DataSource=CDC.ReadDataTable(CO)
        Me.question5_surveyanswerfivescale_fk.DataTextField = "value"
        Me.question5_surveyanswerfivescale_fk.DataValueField = "pk"
        Try
            Me.question5_surveyanswerfivescale_fk.DataBind
        Catch Ex as Exception
            Me.question5_surveyanswerfivescale_fk.SelectedValue=-1
            Me.question5_surveyanswerfivescale_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osurvey7details is Nothing then
osurvey7details = new Objects.survey7details(currentuser,actualuser,pk,CDC)
End If
Me.helpdesk_id.Text=osurvey7details.helpdesk_id
Me.helpdesk_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.helpdesk_id.Attributes.Add("onBlur", "clearNotice()")
Me.selecteduser_fk.SelectedValue=osurvey7details.selecteduser_fk
Me.question1_surveyanswerfivescale_fk.SelectedValue=osurvey7details.question1_surveyanswerfivescale_fk
Me.question2_surveyanswerfivescale_fk.SelectedValue=osurvey7details.question2_surveyanswerfivescale_fk
Me.question3_surveyanswerfivescale_fk.SelectedValue=osurvey7details.question3_surveyanswerfivescale_fk
Me.question4_surveyanswerfivescale_fk.SelectedValue=osurvey7details.question4_surveyanswerfivescale_fk
Me.question5_surveyanswerfivescale_fk.SelectedValue=osurvey7details.question5_surveyanswerfivescale_fk
_CurrentPk=osurvey7details.surveydetail_pk
Status=osurvey7details.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osurvey7details is Nothing then
osurvey7details = new Objects.survey7details(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osurvey7details.helpdesk_id=Me.helpdesk_id.Text
osurvey7details.selecteduser_fk=Me.selecteduser_fk.SelectedValue
osurvey7details.question1_surveyanswerfivescale_fk=Me.question1_surveyanswerfivescale_fk.SelectedValue
osurvey7details.question2_surveyanswerfivescale_fk=Me.question2_surveyanswerfivescale_fk.SelectedValue
osurvey7details.question3_surveyanswerfivescale_fk=Me.question3_surveyanswerfivescale_fk.SelectedValue
osurvey7details.question4_surveyanswerfivescale_fk=Me.question4_surveyanswerfivescale_fk.SelectedValue
osurvey7details.question5_surveyanswerfivescale_fk=Me.question5_surveyanswerfivescale_fk.SelectedValue
result=osurvey7details.Save()
if not result then throw osurvey7details.LastError
_CurrentPk=osurvey7details.surveydetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osurvey7details.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurvey7details is Nothing then
   osurvey7details = new Objects.survey7details(currentuser,actualuser,pk,CDC)
  End If
  osurvey7details.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurvey7details is Nothing then
   osurvey7details = new Objects.survey7details(currentuser,actualuser,pk,CDC)
  End If
  osurvey7details.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurvey7details is Nothing then
   osurvey7details = new Objects.survey7details(currentuser,pk,CDC)
  End If
  osurvey7details.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.helpdesk_id.Enabled=IIF(WL>=1,True,False)
  Me.blkhelpdesk_id.Visible=IIF(RL>=1,True,False)
  Me.selecteduser_fk.Enabled=IIF(WL>=1,True,False)
  Me.blkselecteduser_fk.Visible=IIF(RL>=1,True,False)
  Me.question1_surveyanswerfivescale_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkquestion1_surveyanswerfivescale_fk.Visible=IIF(RL>=0,True,False)
  Me.question2_surveyanswerfivescale_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkquestion2_surveyanswerfivescale_fk.Visible=IIF(RL>=0,True,False)
  Me.question3_surveyanswerfivescale_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkquestion3_surveyanswerfivescale_fk.Visible=IIF(RL>=0,True,False)
  Me.question4_surveyanswerfivescale_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkquestion4_surveyanswerfivescale_fk.Visible=IIF(RL>=0,True,False)
  Me.question5_surveyanswerfivescale_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkquestion5_surveyanswerfivescale_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

