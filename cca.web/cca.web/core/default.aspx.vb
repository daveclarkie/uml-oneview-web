Partial Class core_Default
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        CO = New SqlCommand
        CO.CommandText = "rsp_sys_activeusers"
        CO.CommandType = CommandType.StoredProcedure
        Dim otherusers As Integer = -1
        CDC.ReadScalarValue(otherusers, CO)

        If otherusers = 1 Then
            lblotherusers.Text = "You are the only user currently in the system."
        ElseIf otherusers = 2 Then
            lblotherusers.Text = "There is " & CStr(otherusers - 1) & " other user currently in the system."
        Else
            lblotherusers.Text = "There are " & CStr(otherusers - 1) & " other users currently in the system."
        End If



        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_reports_status_cca"
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptCCAReportStatus.DataSource = dt
        rptCCAReportStatus.DataBind()
        CO.Dispose()


    End Sub


    Public ReadOnly Property ccaSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.CCASystem, CDC)
        End Get
    End Property

    Public ReadOnly Property BackendAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC)
        End Get
    End Property
End Class
