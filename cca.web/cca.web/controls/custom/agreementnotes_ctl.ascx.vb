Partial Class agreementnotes_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents onotes As objects.notes
    Public WithEvents oorganisationnotes As objects.organisationnotes
    Public WithEvents ositenotes As objects.sitenotes
    Public WithEvents ometerpointnotes As objects.meterpointnotes
    Public WithEvents oagreementnotes As objects.agreementnotes

    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        primary_fk.Visible = True
        blkprimary_fk.Visible = True
        If _read Then Exit Sub
        If Request.QueryString("type") Is Nothing Then
            Exit Sub
        Else
            Dim nt As New notetypes(SM.currentuser, SM.actualuser, Request.QueryString("type"), CDC)
            Select Case Request.QueryString("type")
                Case 1
                    Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
                    Dim o As New organisations(SM.currentuser, SM.actualuser, a.organisation_fk, CDC)
                    Dim l As New ListItem(o.customername, a.organisation_fk, True)
                    l.Selected = True
                    primary_fk.Items.Add(l)
                    primary_fk.Visible = False
                    blkprimary_fk.Visible = False
                    lblheadernotes.Text = "<h5>Add new Customer level note - " & o.customername & "</h5>"
                    lblprimary_fk.Text = "Customer Name"
                Case 2
                    CO = New SqlClient.SqlCommand("rsp_agreementsites_lookup")
                    CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
                    CO.CommandType = CommandType.StoredProcedure
                    If primary_fk.Items.Count = 0 Then
                        primary_fk.DataSource = CDC.ReadDataTable(CO)
                        primary_fk.DataTextField = "value"
                        primary_fk.DataValueField = "pk"
                        Try
                            primary_fk.DataBind()
                        Catch Ex As Exception
                            primary_fk.SelectedValue = -1
                            primary_fk.DataBind()
                        End Try
                    End If
                    lblheadernotes.Text = "<h5>Add new Site level note</h5>"
                    lblprimary_fk.Text = "Site Name"
                Case 3
                    CO = New SqlClient.SqlCommand("rsp_agreementmeterpoint_lookup")
                    CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
                    CO.CommandType = CommandType.StoredProcedure
                    If primary_fk.Items.Count = 0 Then
                        primary_fk.DataSource = CDC.ReadDataTable(CO)
                        primary_fk.DataTextField = "value"
                        primary_fk.DataValueField = "pk"
                        Try
                            primary_fk.DataBind()
                        Catch Ex As Exception
                            primary_fk.SelectedValue = -1
                            primary_fk.DataBind()
                        End Try
                    End If
                    lblheadernotes.Text = "<h5>Add new Meter level note</h5>"
                    lblprimary_fk.Text = "Meter"
                Case 4
                    Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
                    Dim l As New ListItem(a.agreementname, a.agreement_pk, True)
                    l.Selected = True
                    primary_fk.Items.Add(l)
                    primary_fk.Visible = False
                    blkprimary_fk.Visible = False
                    lblheadernotes.Text = "<h5>Add new Agreement level note - " & a.agreementname & "</h5>"
                    lblprimary_fk.Text = "Agreement"
            End Select

        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        '  ctlInit()
        'If oagreementnotes is Nothing then
        'oagreementnotes = new Objects.agreementnotes(currentuser,actualuser,pk,CDC)
        'End If
        'Me.agreement_fk.SelectedValue=oagreementnotes.agreement_fk
        'Me.note_fk.SelectedValue=oagreementnotes.note_fk
        '_CurrentPk=oagreementnotes.agreementnote_pk
        'Status=oagreementnotes.rowstatus
        '_read=True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, ByVal type As Integer, Optional ByVal pk As Integer = 0, Optional ByVal notepk As Integer = 0) As Boolean
        Dim result As Boolean = False

        Select Case Request.QueryString("type")
            Case 1 ' Customer
                Try
                    If onotes Is Nothing Then
                        onotes = New objects.notes(currentuser, actualuser, notepk, CDC)
                    End If

                    onotes.note = note.Text
                    result = onotes.Save()
                    If Not result Then Throw onotes.LastError
                    _CurrentPk = onotes.note_pk
                    If result Then

                        If oorganisationnotes Is Nothing Then
                            oorganisationnotes = New objects.organisationnotes(currentuser, actualuser, pk, CDC)
                        End If
                        oorganisationnotes.note_fk = _CurrentPk
                        oorganisationnotes.organisation_fk = primary_fk.SelectedItem.Value
                        result = oorganisationnotes.Save()
                        If Not result Then Throw oorganisationnotes.LastError
                        _CurrentPk = oorganisationnotes.organisationnote_pk
                    End If

                Catch Ex As Exception
                    result = False
                    LastError = Ex
                End Try
                Status = oorganisationnotes.rowstatus
            Case 2 ' Site
                If primary_fk.SelectedItem.Value < 0 Then AddNotice("setNotice('No site selected');") : Return False

                Try
                    If onotes Is Nothing Then
                        onotes = New objects.notes(currentuser, actualuser, notepk, CDC)
                    End If

                    onotes.note = note.Text
                    result = onotes.Save()
                    If Not result Then Throw onotes.LastError
                    _CurrentPk = onotes.note_pk
                    If result Then

                        If ositenotes Is Nothing Then
                            ositenotes = New objects.sitenotes(currentuser, actualuser, pk, CDC)
                        End If
                        ositenotes.note_fk = _CurrentPk
                        ositenotes.site_fk = primary_fk.SelectedItem.Value
                        result = ositenotes.Save()
                        If Not result Then Throw ositenotes.LastError
                        _CurrentPk = ositenotes.sitenote_pk
                    End If

                Catch Ex As Exception
                    result = False
                    LastError = Ex
                End Try
                Status = ositenotes.rowstatus

            Case 3 ' Meter
                If primary_fk.SelectedItem.Value < 0 Then AddNotice("setNotice('No meter selected');") : Return False

                Try
                    If onotes Is Nothing Then
                        onotes = New objects.notes(currentuser, actualuser, notepk, CDC)
                    End If

                    onotes.note = note.Text
                    result = onotes.Save()
                    If Not result Then Throw onotes.LastError
                    _CurrentPk = onotes.note_pk
                    If result Then

                        If ometerpointnotes Is Nothing Then
                            ometerpointnotes = New objects.meterpointnotes(currentuser, actualuser, pk, CDC)
                        End If
                        Dim amp As New agreementmeterpoints(currentuser, actualuser, primary_fk.SelectedItem.Value, CDC)

                        ometerpointnotes.note_fk = _CurrentPk
                        ometerpointnotes.meterpoint_fk = amp.meterpoint_fk
                        result = ometerpointnotes.Save()
                        If Not result Then Throw ometerpointnotes.LastError
                        _CurrentPk = ometerpointnotes.meterpointnote_pk
                    End If

                Catch Ex As Exception
                    result = False
                    LastError = Ex
                End Try
                Status = ometerpointnotes.rowstatus

            Case 4 ' Agreement
                Try
                    If onotes Is Nothing Then
                        onotes = New objects.notes(currentuser, actualuser, notepk, CDC)
                    End If

                    onotes.note = note.Text
                    result = onotes.Save()
                    If Not result Then Throw onotes.LastError
                    _CurrentPk = onotes.note_pk
                    If result Then

                        If oagreementnotes Is Nothing Then
                            oagreementnotes = New objects.agreementnotes(currentuser, actualuser, pk, CDC)
                        End If
                        oagreementnotes.note_fk = _CurrentPk
                        oagreementnotes.agreement_fk = primary_fk.SelectedItem.Value
                        result = oagreementnotes.Save()
                        If Not result Then Throw oagreementnotes.LastError
                        _CurrentPk = oagreementnotes.agreementnote_pk
                    End If

                Catch Ex As Exception
                    result = False
                    LastError = Ex
                End Try
                Status = oagreementnotes.rowstatus
        End Select
        Return result
    End Function

    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
    End Sub
End Class

