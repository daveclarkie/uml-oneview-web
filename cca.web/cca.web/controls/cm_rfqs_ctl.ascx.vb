Partial Class cm_rfqs_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ocm_rfqs As Objects.cm_rfqs
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ocm_rfqs is Nothing then
ocm_rfqs = new Objects.cm_rfqs(currentuser,actualuser,pk,CDC)
End If
Me.rfqdescription.Text=ocm_rfqs.rfqdescription
Me.organisation_fk.SelectedValue=ocm_rfqs.organisation_fk
_CurrentPk=ocm_rfqs.cm_rfq_pk
Status=ocm_rfqs.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ocm_rfqs is Nothing then
ocm_rfqs = new Objects.cm_rfqs(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ocm_rfqs.rfqdescription=Me.rfqdescription.Text
ocm_rfqs.organisation_fk=Me.organisation_fk.SelectedValue
result=ocm_rfqs.Save()
if not result then throw ocm_rfqs.LastError
_CurrentPk=ocm_rfqs.cm_rfq_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ocm_rfqs.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqs is Nothing then
   ocm_rfqs = new Objects.cm_rfqs(currentuser,actualuser,pk,CDC)
  End If
  ocm_rfqs.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqs is Nothing then
   ocm_rfqs = new Objects.cm_rfqs(currentuser,actualuser,pk,CDC)
  End If
  ocm_rfqs.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocm_rfqs is Nothing then
   ocm_rfqs = new Objects.cm_rfqs(currentuser,pk,CDC)
  End If
  ocm_rfqs.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.rfqdescription.Enabled=IIF(WL>=0,True,False)
  Me.blkrfqdescription.Visible=IIF(RL>=0,True,False)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

