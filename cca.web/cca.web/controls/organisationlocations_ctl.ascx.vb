Partial Class organisationlocations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oorganisationlocations As Objects.organisationlocations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.location_fk.Items.Count=0 Then
        Me.location_fk.DataSource=CDC.ReadDataTable(CO)
        Me.location_fk.DataTextField = "value"
        Me.location_fk.DataValueField = "pk"
        Try
            Me.location_fk.DataBind
        Catch Ex as Exception
            Me.location_fk.SelectedValue=-1
            Me.location_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locationuses_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.locationuse_fk.Items.Count=0 Then
        Me.locationuse_fk.DataSource=CDC.ReadDataTable(CO)
        Me.locationuse_fk.DataTextField = "value"
        Me.locationuse_fk.DataValueField = "pk"
        Try
            Me.locationuse_fk.DataBind
        Catch Ex as Exception
            Me.locationuse_fk.SelectedValue=-1
            Me.locationuse_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oorganisationlocations is Nothing then
oorganisationlocations = new Objects.organisationlocations(currentuser,actualuser,pk,CDC)
End If
Me.organisation_fk.SelectedValue=oorganisationlocations.organisation_fk
Me.location_fk.SelectedValue=oorganisationlocations.location_fk
Me.locationuse_fk.SelectedValue=oorganisationlocations.locationuse_fk
_CurrentPk=oorganisationlocations.organisationlocation_pk
Status=oorganisationlocations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oorganisationlocations is Nothing then
oorganisationlocations = new Objects.organisationlocations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oorganisationlocations.organisation_fk=Me.organisation_fk.SelectedValue
oorganisationlocations.location_fk=Me.location_fk.SelectedValue
oorganisationlocations.locationuse_fk=Me.locationuse_fk.SelectedValue
result=oorganisationlocations.Save()
if not result then throw oorganisationlocations.LastError
_CurrentPk=oorganisationlocations.organisationlocation_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oorganisationlocations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oorganisationlocations is Nothing then
   oorganisationlocations = new Objects.organisationlocations(currentuser,actualuser,pk,CDC)
  End If
  oorganisationlocations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oorganisationlocations is Nothing then
   oorganisationlocations = new Objects.organisationlocations(currentuser,actualuser,pk,CDC)
  End If
  oorganisationlocations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oorganisationlocations is Nothing then
   oorganisationlocations = new Objects.organisationlocations(currentuser,pk,CDC)
  End If
  oorganisationlocations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  Me.location_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocation_fk.Visible=IIF(RL>=0,True,False)
  Me.locationuse_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocationuse_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

