<%@ Control Language="VB" AutoEventWireup="false" CodeFile="serviceproducts_ctl.ascx.vb" Inherits="serviceproducts_ctl" %>
<h5>serviceproducts</h5>
<ul class='formcontrol'>
<li runat="server" id="blkproductname">
<span class='label'>Product Name</span>
<asp:TextBox EnableViewState="false" ID="productname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkproductdescription">
<span class='label'>Product Description</span>
<asp:TextBox EnableViewState="false" ID="productdescription" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkdepartment_fk">
<span class='label'>Department<span class='pop'>Department that the product belongs to</span></span>
<asp:DropDownList ID="department_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

