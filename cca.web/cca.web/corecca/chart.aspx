﻿<%@ Page Title="chart" Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="chart.aspx.vb" Inherits="corecca_chart" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<asp:GridView ID="grdPivot" runat="server"></asp:GridView>
<DCWC:Chart ID="chartPivot" runat="server"></DCWC:Chart>

</asp:Content>

