Partial Class surveys_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osurveys As Objects.surveys
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_surveytypes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.surveytype_fk.Items.Count=0 Then
        Me.surveytype_fk.DataSource=CDC.ReadDataTable(CO)
        Me.surveytype_fk.DataTextField = "value"
        Me.surveytype_fk.DataValueField = "pk"
        Try
            Me.surveytype_fk.DataBind
        Catch Ex as Exception
            Me.surveytype_fk.SelectedValue=-1
            Me.surveytype_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg__lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.surveydetail_fk.Items.Count=0 Then
        Me.surveydetail_fk.DataSource=CDC.ReadDataTable(CO)
        Me.surveydetail_fk.DataTextField = "value"
        Me.surveydetail_fk.DataValueField = "pk"
        Try
            Me.surveydetail_fk.DataBind
        Catch Ex as Exception
            Me.surveydetail_fk.SelectedValue=-1
            Me.surveydetail_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg__lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.surveystatus_fk.Items.Count=0 Then
        Me.surveystatus_fk.DataSource=CDC.ReadDataTable(CO)
        Me.surveystatus_fk.DataTextField = "value"
        Me.surveystatus_fk.DataValueField = "pk"
        Try
            Me.surveystatus_fk.DataBind
        Catch Ex as Exception
            Me.surveystatus_fk.SelectedValue=-1
            Me.surveystatus_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osurveys is Nothing then
osurveys = new Objects.surveys(currentuser,actualuser,pk,CDC)
End If
Me.surveytype_fk.SelectedValue=osurveys.surveytype_fk
Me.surveydetail_fk.SelectedValue=osurveys.surveydetail_fk
Me.surveystatus_fk.SelectedValue=osurveys.surveystatus_fk
_CurrentPk=osurveys.survey_pk
Status=osurveys.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osurveys is Nothing then
osurveys = new Objects.surveys(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osurveys.surveytype_fk=Me.surveytype_fk.SelectedValue
osurveys.surveydetail_fk=Me.surveydetail_fk.SelectedValue
osurveys.surveystatus_fk=Me.surveystatus_fk.SelectedValue
result=osurveys.Save()
if not result then throw osurveys.LastError
_CurrentPk=osurveys.survey_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osurveys.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveys is Nothing then
   osurveys = new Objects.surveys(currentuser,actualuser,pk,CDC)
  End If
  osurveys.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveys is Nothing then
   osurveys = new Objects.surveys(currentuser,actualuser,pk,CDC)
  End If
  osurveys.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveys is Nothing then
   osurveys = new Objects.surveys(currentuser,pk,CDC)
  End If
  osurveys.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.surveytype_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksurveytype_fk.Visible=IIF(RL>=0,True,False)
  Me.surveydetail_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksurveydetail_fk.Visible=IIF(RL>=0,True,False)
  Me.surveystatus_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksurveystatus_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

