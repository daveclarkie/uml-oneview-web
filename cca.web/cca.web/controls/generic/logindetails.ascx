<%@ Control Language="VB" AutoEventWireup="false" CodeFile="logindetails.ascx.vb" Inherits="mainctls_logindetails" %>
<h3>In order to reset your password we require the following information:-</h3>
<ul class="formcontrol">
<li><span class="label">First Name</span><asp:TextBox ID="forename" runat="server" /></li>
<li><span class="label">Email Address</span><asp:TextBox ID="email" runat="server" /></li>
<li><span class="label">&nbsp;</span><asp:Button ID="btnReset" runat="server" Text="Start" /></li>
<li><asp:Label ID="lblMessage" runat="server" Text="" /></li>
<li>The password reset request will be sent to all email addresses linked to your account.</li>
</ul>