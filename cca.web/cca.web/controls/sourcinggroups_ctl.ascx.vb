Partial Class sourcinggroups_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osourcinggroups As Objects.sourcinggroups
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.hd_group_id.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osourcinggroups is Nothing then
osourcinggroups = new Objects.sourcinggroups(currentuser,actualuser,pk,CDC)
End If
Me.groupname.Text=osourcinggroups.groupname
Me.teamname.Text=osourcinggroups.teamname
Me.hd_group_id.Text=osourcinggroups.hd_group_id
Me.hd_group_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.hd_group_id.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=osourcinggroups.sourcinggroup_pk
Status=osourcinggroups.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osourcinggroups is Nothing then
osourcinggroups = new Objects.sourcinggroups(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osourcinggroups.groupname=Me.groupname.Text
osourcinggroups.teamname=Me.teamname.Text
osourcinggroups.hd_group_id=Me.hd_group_id.Text
result=osourcinggroups.Save()
if not result then throw osourcinggroups.LastError
_CurrentPk=osourcinggroups.sourcinggroup_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osourcinggroups.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osourcinggroups is Nothing then
   osourcinggroups = new Objects.sourcinggroups(currentuser,actualuser,pk,CDC)
  End If
  osourcinggroups.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osourcinggroups is Nothing then
   osourcinggroups = new Objects.sourcinggroups(currentuser,actualuser,pk,CDC)
  End If
  osourcinggroups.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osourcinggroups is Nothing then
   osourcinggroups = new Objects.sourcinggroups(currentuser,pk,CDC)
  End If
  osourcinggroups.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.groupname.Enabled=IIF(WL>=0,True,False)
  Me.blkgroupname.Visible=IIF(RL>=0,True,False)
  Me.teamname.Enabled=IIF(WL>=0,True,False)
  Me.blkteamname.Visible=IIF(RL>=0,True,False)
  Me.hd_group_id.Enabled=IIF(WL>=0,True,False)
  Me.blkhd_group_id.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

