Partial Class corematrix_Default
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        validpk = Integer.TryParse(Request.QueryString("pk"), pk)
        validppk = Integer.TryParse(SM.matrixprice, SM.matrixprice)
        validdpk = Integer.TryParse(SM.matrixdetail, SM.matrixdetail)

        If SM.matrixprice = 0 Then
            SM.matrixprice = -1
            SM.Save()
        End If

        If SM.matrixdetail = 0 Then
            SM.matrixdetail = -1
            SM.Save()
        End If

        If pk <> 0 Then
            SM.matrix = pk
            SM.Save()
        End If

        If Not validpk Then
            ' we shouldn't be here
            Local.Bounce(SM, Response, "~/corematrix/search.aspx", "no+valid+matrix+id")
        End If

        If Not (SM.matrix = pk) And pk = -1 Then
            SM.matrix = pk
            SM.Save()
        End If
        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        Try

            Select Case ModeCode
                Case "mn" : CurrentDisplay = MatrixPageMode.Matrix
                    vwSub = vwMatrix
                Case "mp" : CurrentDisplay = MatrixPageMode.Price
                    vwSub = vwPrice
                Case "md" : CurrentDisplay = MatrixPageMode.Detail
                    '                    vwSub = vwDetail
                Case "mr" : CurrentDisplay = MatrixPageMode.Report
                    '                   vwSub = vwReport
                Case Else
                    ' we shouldn't be here
                    Local.Bounce(SM, Response, "~/corematrix/search.aspx", "invalid+display+mode")
            End Select

            If SM.matrix = -1 And Not CurrentDisplay = MatrixPageMode.Matrix Then
                Local.Bounce(SM, Response, "~/corematrix/default.aspx", "No+Matrix&pk=-1&pg=mn")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/corematrix/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub
    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property
    Dim cc As Object
    Dim maintab As String = "matrix"
    
    Dim validpk As Boolean = False
    Dim validppk As Boolean = False
    Dim validdpk As Boolean = False
    Dim CurrentAction As Local.Action = Local.Action.None
    Dim CurrentDisplay As MatrixPageMode = MatrixPageMode.Matrix

    Dim vwSub As System.Web.UI.WebControls.View = vwMatrix

    Dim pk As Integer = 0
    Dim ppk As Integer = 0
    Dim dpk As Integer = 0
    Dim md As Integer = 0

    Public Mode As MatrixPageMode = MatrixPageMode.Matrix

    Private Sub SetMode()
        If Page.IsPostBack Then
            Mode = SM.MatrixPageMode
        Else
            Mode = MatrixPageMode.Search
            Select Case Request.QueryString("pg")
                Case "mn" : Mode = MatrixPageMode.Matrix
                Case "mp" : Mode = MatrixPageMode.Price
                Case "md" : Mode = MatrixPageMode.Detail
                Case "mr" : Mode = MatrixPageMode.Report
                Case Else : Mode = MatrixPageMode.Search
            End Select
            SM.MatrixPageMode = Mode
        End If
    End Sub

    Private Sub PageLoad()
        Select Case SM.MatrixPageMode
            Case MatrixPageMode.Matrix : InitMatrix(Page.IsPostBack)
            Case MatrixPageMode.Price : InitPrice(Page.IsPostBack)
            Case MatrixPageMode.Detail : InitDetail(Page.IsPostBack)
                'Case MatrixPageMode.Report : InitReport(Page.IsPostBack)
            Case Else
                Response.Redirect("search.aspx")
        End Select
    End Sub
    Private Sub DisplayPage()
        If validdpk Then
            CurrentAction = Local.Action.Read
        Else
            CurrentAction = Local.Action.None
        End If
    End Sub

    Private Sub RefreshList()

        If Mode = MatrixPageMode.Price Then
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_matrixprices_currentmatrix"
            CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptMatrixPrices.DataSource = dt
            rptMatrixPrices.DataBind()
            CO.Dispose()
        End If
    End Sub

    Private Sub SaveContol()

        Dim resDet As Boolean = True
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())


        If Mode = MatrixPageMode.Matrix Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.matrix)
            If resDet Then
                SM.matrix = cc.currentpk
                SM.Save()
                nameValueCollection.Remove("pk")
                nameValueCollection.Remove("pg")
                nameValueCollection.Remove("r")
                nameValueCollection.Add("pk", SM.matrix)
                nameValueCollection.Add("pg", "mp")
            End If
        ElseIf Mode = MatrixPageMode.Price Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.matrixprice)
            If resDet Then
                SM.matrixprice = cc.currentpk
                SM.Save()
                nameValueCollection.Remove("pg")
                nameValueCollection.Remove("r")
                nameValueCollection.Add("pg", "md")
            End If
        ElseIf Mode = MatrixPageMode.Detail Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.matrixdetail)
            If resDet Then
                SM.matrixdetail = cc.currentpk
                SM.Save()

                Dim mp As New matrixprices(SM.currentuser, SM.actualuser, SM.matrixprice, CDC)
                mp.matrixdetail_fk = SM.matrixdetail
                mp.Save()

                SM.matrixprice = -1
                SM.matrixdetail = -1
                SM.Save()

                nameValueCollection.Remove("pg")
                nameValueCollection.Remove("r")
                nameValueCollection.Add("pg", "mp")
            End If
        End If

        If Not (resDet) Then
            Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
            Else
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
            End If
        Else
            nameValueCollection.Add("r", "Record+Saved")
            Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
            Response.Redirect(url)
        End If
    End Sub
    Private Sub CancelContol()
    End Sub
    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM

        If Mode = MatrixPageMode.Matrix Then
            cc.Read(SM.currentuser, SM.actualuser, SM.matrix)
        ElseIf Mode = MatrixPageMode.Price Then
            cc.Read(SM.currentuser, SM.actualuser, SM.matrixprice)
        ElseIf Mode = MatrixPageMode.Detail Then
            cc.Read(SM.currentuser, SM.actualuser, SM.matrixdetail)
        End If

        TargetControl.Controls.Add(cc)
        TargetButton.Visible = True
        RefreshList()



    End Sub

    Private Sub InitMatrix(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwMatrix)
        SetControl("~/controls/matrix_ctl.ascx", MatrixControl, MatrixButton)
    End Sub
    Private Sub InitPrice(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwPrice)
        SetControl("~/controls/matrixprices_ctl.ascx", PriceControl, PriceButton)
    End Sub

    Private Sub InitDetail(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwDetail)
        Dim mp As New matrixprices(SM.currentuser, SM.actualuser, SM.matrixprice, CDC)

        SetControl("~/controls/matrix" & mp.matrixtype_fk & "details_ctl.ascx", DetailControl, DetailButton)
    End Sub

    Protected Sub btnSaveMatrix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMatrix.Click
        SaveContol()
    End Sub
    Protected Sub btnSavePrice_Click(sender As Object, e As System.EventArgs) Handles btnSavePrice.Click
        SaveContol()
    End Sub
    Protected Sub btnSaveDetail_Click(sender As Object, e As System.EventArgs) Handles btnSaveDetail.Click
        SaveContol()
    End Sub

    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function matrixreporturl() As String
        Dim reporturl As String = ""
        Dim matrixtype As Integer = -1

        Select Case SM.matrix
            Case Is > 0
                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_matrixtype_matrix_pk"
                CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
                CDC.ReadScalarValue(matrixtype, CO)
                CO.Dispose()
        End Select

        If matrixtype > 0 Then
            Dim mt As New matrixtypes(SM.currentuser, SM.actualuser, matrixtype, CDC)
            reporturl = "/corematrix/reports/doc.ashx" 'mt.reporturl
        End If

        Return reporturl
    End Function
    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""
        Select Case Mode
            Case MatrixPageMode.Matrix
                itemclass = IIf(item = "matrix", "selected", "")
                itemclass = IIf(item = "price", "hidden", itemclass)
                itemclass = IIf(item = "detail", "hidden", itemclass)
                itemclass = IIf(item = "report", "hidden", itemclass)

            Case MatrixPageMode.Price
                itemclass = IIf(item = "price", "selected", "")
                itemclass = IIf(item = "matrix", "hidden", itemclass)
                itemclass = IIf(item = "detail", "hidden", itemclass)

                If Not IsTypeAttached() Then
                    itemclass = IIf(item = "report", "hidden", itemclass)
                End If


            Case MatrixPageMode.Detail
                itemclass = IIf(item = "detail", "selected", "")
                itemclass = IIf(item = "matrix", "hidden", itemclass)
                itemclass = IIf(item = "price", "hidden", itemclass)
                itemclass = IIf(item = "report", "hidden", itemclass)


            Case MatrixPageMode.Report : itemclass = IIf(item = "report", "selected", "")
            Case MatrixPageMode.Search : itemclass = ""
        End Select
        Return itemclass
    End Function
    Public Function IsStaff() As Boolean
        Return Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
    End Function

    Public Function IsTypeAttached() As Boolean
        Dim _Return As Boolean

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_matrixprices_attachedtypes"
        CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
        Dim typeattached As Integer = -1
        CDC.ReadScalarValue(typeattached, CO)
        If typeattached = 0 Then
            _Return = False
        Else
            _Return = True
        End If

        Return _Return
    End Function
    Public Function matrixStatus() As Integer
        Dim _return As Integer = -1

        Dim a As New matrix(SM.currentuser, SM.actualuser, SM.matrix, CDC)
        Try
            _return = a.rowstatus
        Catch ex As Exception
        End Try

        Return _return
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMode()
        PageLoad()
    End Sub

End Class
