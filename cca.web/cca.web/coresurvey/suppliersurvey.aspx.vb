﻿Partial Class coresurvey_survey
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Dim errormessage As String = ""
    Dim survey_pk As Integer
    Dim surveyurl_pk As Integer
    Dim surveydetail_pk As Integer
    Dim selecteduser_fk As Integer

    Dim ccList() As Object
    Dim cc As Object

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lbl_errormessage.Text = Nothing
        setForm("clear")
        Dim row_number As Integer = 0

        If Request.QueryString("surveyurl") <> Nothing Then
            Dim variableurl As String = Request.QueryString("surveyurl").ToString
            CDC.Sanitize(variableurl)
            Dim CO As New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_surveyurl_retrieve"
            CO.Parameters.AddWithValue("@url", variableurl)
            CDC.ReadScalarValue(surveyurl_pk, CO)

            Dim su As New surveyurls(-1, -1, surveyurl_pk, CDC)
            survey_pk = su.survey_fk

            If surveyurl_pk = -2 Then ' expired link
                errormessage &= vbCrLf & "It appears that this survey has already been completed."
            ElseIf surveyurl_pk = -1 Then ' invalid url passed
                errormessage &= vbCrLf & "The link you are using is invalid."
            ElseIf surveyurl_pk = 0 Then ' invalid url passed
                errormessage &= vbCrLf & "The link you are using is invalid."
            Else ' match
                errormessage = Nothing
                setForm("open")

                Dim s As New surveys(-1, -1, survey_pk, CDC)
                Dim sd As Object

                CO = New SqlClient.SqlCommand("rsp_suppliersurvey_surveydetailsfromsurvey")
                CO.Parameters.AddWithValue("@survey_pk", s.survey_pk)
                CO.CommandType = CommandType.StoredProcedure
                Dim dt As New DataTable
                dt = CDC.ReadDataTable(CO)
                CO.Dispose()
                ReDim ccList(dt.Rows.Count - 1)
                For Each dr As DataRow In dt.Rows

                    surveydetail_pk = dr(0)

                    Select Case s.surveytype_fk
                        Case 3 'supplier survey - sourcing
                            sd = New survey3details(-1, -1, surveydetail_pk, CDC)
                            selecteduser_fk = sd.selecteduser_fk
                        Case 4 'supplier survey - cm
                            sd = New survey4details(-1, -1, surveydetail_pk, CDC)
                            selecteduser_fk = sd.selecteduser_fk
                        Case 5 'supplier survey - trading
                            sd = New survey5details(-1, -1, surveydetail_pk, CDC)
                            selecteduser_fk = sd.selecteduser_fk
                        Case 6 'supplier survey - billing
                            sd = New survey6details(-1, -1, surveydetail_pk, CDC)
                            selecteduser_fk = sd.selecteduser_fk
                        Case Else
                            errormessage = "Invalid Survey Type"
                            lbl_errormessage.Text = errormessage
                            Exit Sub
                    End Select

                    Dim mySurveyCell As New TableCell
                    mySurveyCell.ID = "SurveyCell" & row_number
                    If row_number = 0 Then
                        mySurveyCell.Width = 355
                    Else
                        mySurveyCell.Width = 150

                    End If
                    tr_survey.Controls.Add(mySurveyCell)

                    Dim mySurveyControl As New PlaceHolder
                    mySurveyControl.ID = "SurveyControl" & row_number
                    mySurveyCell.Controls.Add(mySurveyControl)

                    SetControl("~/controls/survey" & s.surveytype_fk & "details_ctl.ascx", mySurveyControl, surveydetail_pk, row_number)

                    row_number += 1
                Next
            End If

        ElseIf Request.QueryString("survey_pk") <> Nothing Then
            Dim CO As New SqlClient.SqlCommand
            survey_pk = Request.QueryString("survey_pk").ToString

            errormessage = Nothing
            setForm("open")

            Dim s As New surveys(-1, -1, survey_pk, CDC)
            Dim sd As Object

            CO = New SqlClient.SqlCommand("rsp_suppliersurvey_surveydetailsfromsurvey")
            CO.Parameters.AddWithValue("@survey_pk", s.survey_pk)
            CO.CommandType = CommandType.StoredProcedure
            Dim dt As New DataTable
            dt = CDC.ReadDataTable(CO)
            CO.Dispose()
            ReDim ccList(dt.Rows.Count - 1)
            For Each dr As DataRow In dt.Rows

                surveydetail_pk = dr(0)

                Select Case s.surveytype_fk
                    Case 3 'supplier survey - sourcing
                        sd = New survey3details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                    Case 4 'supplier survey - cm
                        sd = New survey4details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                    Case 5 'supplier survey - trading
                        sd = New survey5details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                    Case 6 'supplier survey - billing
                        sd = New survey6details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                    Case Else
                        errormessage = "Invalid Survey Type"
                        lbl_errormessage.Text = errormessage
                        Exit Sub
                End Select

                Dim mySurveyCell As New TableCell
                mySurveyCell.ID = "SurveyCell" & row_number
                If row_number = 0 Then
                    mySurveyCell.Width = 355
                Else
                    mySurveyCell.Width = 150

                End If
                tr_survey.Controls.Add(mySurveyCell)

                Dim mySurveyControl As New PlaceHolder
                mySurveyControl.ID = "SurveyControl" & row_number
                mySurveyCell.Controls.Add(mySurveyControl)

                SetControl("~/controls/survey" & s.surveytype_fk & "details_ctl.ascx", mySurveyControl, surveydetail_pk, row_number)

                row_number += 1
            Next

            
        Else

            If Request.QueryString("r") <> Nothing Then
                setForm("complete")
            End If
        End If

        lbl_errormessage.Text = errormessage

        Me.btn_savesurvey.Width = (355 + ((row_number - 1) * 150)) + (5 * row_number)

    End Sub
    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal PK As Integer, row_number As Integer)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM


        cc.Read(SM.currentuser, SM.actualuser, PK, row_number)
        TargetControl.Controls.Add(cc)
        ccList(row_number) = cc

    End Sub

    Protected Sub btn_savesurvey_Click(sender As Object, e As System.EventArgs) Handles btn_savesurvey.Click
        Dim x As Integer = 0
        Do While x < ccList.Length
            Dim resDet As Boolean = True
            resDet = ccList(x).Save(selecteduser_fk, selecteduser_fk, Me.surveydetail_pk)

            If Not (resDet) Then
                Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
                If errMsg.Length > 1 Then
                    lbl_errormessage.Text = "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "."
                    Exit Sub
                Else
                    lbl_errormessage.Text = "This record has not been saved because the " & errMsg(0).ToLower() & "."
                    Exit Sub
                End If
            End If
            x += 1
        Loop

        Dim surveysopen As Integer = 0
        Dim CO As New SqlClient.SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_suppliersurvey_opensurveys"
        CO.Parameters.AddWithValue("@survey_pk", Me.survey_pk)
        CDC.ReadScalarValue(surveysopen, CO)

        If surveysopen = 0 Then
            CloseSurvey()
            Dim filtered As New NameValueCollection(Request.QueryString)
            Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
            nameValueCollection.Remove("surveyurl")
            Dim url As String = HttpContext.Current.Request.Path + "?r=Survey+Complete"
            Response.Redirect(url)
        Else
            lbl_errormessage.Text = "This survey is not yet complete.  There are still " & surveysopen & " suppliers left to answer."
        End If

        

    End Sub

    Private Sub CloseSurvey()

        ' disable URL
        Dim su As New surveyurls(selecteduser_fk, selecteduser_fk, surveyurl_pk, CDC)
        su.Disable()
        ' update status of survey to 4: complete
        Dim s As New surveys(selecteduser_fk, selecteduser_fk, survey_pk, CDC)
        s.surveystatus_fk = 4
        s.Save()

    End Sub

    Private Sub setForm(ByVal stage As String)
        If stage = "open" Then
            pnl_survey.Visible = True
            pnl_complete.Visible = False
            Me.AgreementTargetButton.Visible = True
        ElseIf stage = "complete" Then
            pnl_survey.Visible = False
            pnl_complete.Visible = True
            Me.AgreementTargetButton.Visible = False
        ElseIf stage = "clear" Then
            pnl_survey.Visible = False
            pnl_complete.Visible = False
            Me.AgreementTargetButton.Visible = False
        End If
    End Sub

 

End Class
