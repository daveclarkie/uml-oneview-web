Partial Class contactlocations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ocontactlocations As Objects.contactlocations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ocontactlocations is Nothing then
ocontactlocations = new Objects.contactlocations(currentuser,actualuser,pk,CDC)
End If
Me.contactlocation.Text=ocontactlocations.contactlocation
Me.forphone.Checked=ocontactlocations.forphone
Me.foremail.Checked=ocontactlocations.foremail
_CurrentPk=ocontactlocations.contactlocation_pk
Status=ocontactlocations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ocontactlocations is Nothing then
ocontactlocations = new Objects.contactlocations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ocontactlocations.contactlocation=Me.contactlocation.Text
ocontactlocations.forphone=Me.forphone.Checked
ocontactlocations.foremail=Me.foremail.Checked
result=ocontactlocations.Save()
if not result then throw ocontactlocations.LastError
_CurrentPk=ocontactlocations.contactlocation_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ocontactlocations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocontactlocations is Nothing then
   ocontactlocations = new Objects.contactlocations(currentuser,actualuser,pk,CDC)
  End If
  ocontactlocations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocontactlocations is Nothing then
   ocontactlocations = new Objects.contactlocations(currentuser,actualuser,pk,CDC)
  End If
  ocontactlocations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ocontactlocations is Nothing then
   ocontactlocations = new Objects.contactlocations(currentuser,pk,CDC)
  End If
  ocontactlocations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.contactlocation.Enabled=IIF(WL>=0,True,False)
  Me.blkcontactlocation.Visible=IIF(RL>=0,True,False)
  Me.forphone.Enabled=IIF(WL>=0,True,False)
  Me.blkforphone.Visible=IIF(RL>=0,True,False)
  Me.foremail.Enabled=IIF(WL>=0,True,False)
  Me.blkforemail.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

