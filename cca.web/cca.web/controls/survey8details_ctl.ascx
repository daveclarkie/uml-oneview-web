<%@ Control Language="VB" AutoEventWireup="false" CodeFile="survey8details_ctl.ascx.vb" Inherits="survey8details_ctl" %>
<h5>survey8details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkhelpdesk_id">
<span class='label'>Helpdesk ID<span class='pop'>The request id of the ticket from helpdesk</span></span>
<asp:TextBox EnableViewState="false" ID="helpdesk_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkselecteduser_fk">
<span class='label'>User<span class='pop'>The user_fk of the user who should be doing the survey</span></span>
<asp:DropDownList ID="selecteduser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion1_surveyanswerfivescale_fk">
<span class='label'>Had a good knowledge of their customer</span>
<asp:DropDownList ID="question1_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion2_surveyanswerfivescale_fk">
<span class='label'>Was proactive in responding to questions</span>
<asp:DropDownList ID="question2_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion3_surveyanswerfivescale_fk">
<span class='label'>Provided appropriate and clear assumptions and inputs</span>
<asp:DropDownList ID="question3_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion4_surveyanswerfivescale_fk">
<span class='label'>Organised his/her activities and managed client expectation well and I did not need to present extra flexibility</span>
<asp:DropDownList ID="question4_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion5_surveyanswerfivescale_fk">
<span class='label'>Treated me with respect in their interactions with me</span>
<asp:DropDownList ID="question5_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

