<%@ Page Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="userstructuremanagement.aspx.vb" Inherits="backend_userstructuremanagement" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    
    <h3>Manage User Structures</h3>
    
    <ul class="formcontrol list">
    <li><br /></li>
        <li><span class="label">Manager:</span>
        <asp:DropDownList ID="managerList" runat="server" AppendDataBoundItems="True">
            <asp:ListItem Value="-1">Select . . .</asp:ListItem>
        </asp:DropDownList></li>
    <li><br /></li>
    </ul>
    
<asp:Panel ID="manageuserssection" runat="server">
    <div>
        <div class="left c2f">
                    <h3 class="c2f">Managed Users</h3>
                    
                    <ul class="list" id="ActiveUserList">
                    </ul>

        </div>

        <div class="right c2f">
                    <h3 class="c2f">Available Users</h3>
                    
                    <ul class="list" id="AvailableUserList">
                    </ul>


        </div>
    </div>
    
</asp:Panel>

</asp:Content>

