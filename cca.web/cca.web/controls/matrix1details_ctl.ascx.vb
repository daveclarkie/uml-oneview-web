Partial Class matrix1details_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omatrix1details As Objects.matrix1details
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_profiles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.profile_fk.Items.Count=0 Then
        Me.profile_fk.DataSource=CDC.ReadDataTable(CO)
        Me.profile_fk.DataTextField = "value"
        Me.profile_fk.DataValueField = "pk"
        Try
            Me.profile_fk.DataBind
        Catch Ex as Exception
            Me.profile_fk.SelectedValue=-1
            Me.profile_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_distributions_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.distribution_fk.Items.Count=0 Then
        Me.distribution_fk.DataSource=CDC.ReadDataTable(CO)
        Me.distribution_fk.DataTextField = "value"
        Me.distribution_fk.DataValueField = "pk"
        Try
            Me.distribution_fk.DataBind
        Catch Ex as Exception
            Me.distribution_fk.SelectedValue=-1
            Me.distribution_fk.DataBind
        End Try
    End If
            Me.consumptionsingle.Text="0"
            Me.consumptionday.Text="0"
            Me.consumptionnight.Text="0"
            Me.consumptionother.Text="0"
            Me.current_standingcharge2.Text="0.00"
            Me.current_ratesingle.Text="0.00"
            Me.current_rateday.Text="0.00"
            Me.current_ratenight.Text="0.00"
            Me.current_rateother.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omatrix1details is Nothing then
omatrix1details = new Objects.matrix1details(currentuser,actualuser,pk,CDC)
End If
Me.profile_fk.SelectedValue=omatrix1details.profile_fk
Me.metertimeswitchcode.Text=omatrix1details.metertimeswitchcode
Me.linelossfactor.Text=omatrix1details.linelossfactor
Me.distribution_fk.SelectedValue=omatrix1details.distribution_fk
Me.uniqueidentifier.Text=omatrix1details.uniqueidentifier
Me.checkdigit.Text=omatrix1details.checkdigit
Me.consumptionsingle.Text=omatrix1details.consumptionsingle
Me.consumptionsingle.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.consumptionsingle.Attributes.Add("onBlur", "clearNotice()")
Me.consumptionday.Text=omatrix1details.consumptionday
Me.consumptionday.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.consumptionday.Attributes.Add("onBlur", "clearNotice()")
Me.consumptionnight.Text=omatrix1details.consumptionnight
Me.consumptionnight.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.consumptionnight.Attributes.Add("onBlur", "clearNotice()")
Me.consumptionother.Text=omatrix1details.consumptionother
Me.consumptionother.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.consumptionother.Attributes.Add("onBlur", "clearNotice()")
Me.current_standingcharge2.Text=omatrix1details.current_standingcharge2
Me.current_standingcharge2.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_standingcharge2.Attributes.Add("onBlur", "clearNotice()")
Me.current_ratesingle.Text=omatrix1details.current_ratesingle
Me.current_ratesingle.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_ratesingle.Attributes.Add("onBlur", "clearNotice()")
Me.current_rateday.Text=omatrix1details.current_rateday
Me.current_rateday.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_rateday.Attributes.Add("onBlur", "clearNotice()")
Me.current_ratenight.Text=omatrix1details.current_ratenight
Me.current_ratenight.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_ratenight.Attributes.Add("onBlur", "clearNotice()")
Me.current_rateother.Text=omatrix1details.current_rateother
Me.current_rateother.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.current_rateother.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=omatrix1details.matrixdetail_pk
Status=omatrix1details.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omatrix1details is Nothing then
omatrix1details = new Objects.matrix1details(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omatrix1details.profile_fk=Me.profile_fk.SelectedValue
omatrix1details.metertimeswitchcode=Me.metertimeswitchcode.Text
omatrix1details.linelossfactor=Me.linelossfactor.Text
omatrix1details.distribution_fk=Me.distribution_fk.SelectedValue
omatrix1details.uniqueidentifier=Me.uniqueidentifier.Text
omatrix1details.checkdigit=Me.checkdigit.Text
omatrix1details.consumptionsingle=Me.consumptionsingle.Text
omatrix1details.consumptionday=Me.consumptionday.Text
omatrix1details.consumptionnight=Me.consumptionnight.Text
omatrix1details.consumptionother=Me.consumptionother.Text
omatrix1details.current_standingcharge2=Me.current_standingcharge2.Text
omatrix1details.current_ratesingle=Me.current_ratesingle.Text
omatrix1details.current_rateday=Me.current_rateday.Text
omatrix1details.current_ratenight=Me.current_ratenight.Text
omatrix1details.current_rateother=Me.current_rateother.Text
result=omatrix1details.Save()
if not result then throw omatrix1details.LastError
_CurrentPk=omatrix1details.matrixdetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omatrix1details.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1details is Nothing then
   omatrix1details = new Objects.matrix1details(currentuser,actualuser,pk,CDC)
  End If
  omatrix1details.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1details is Nothing then
   omatrix1details = new Objects.matrix1details(currentuser,actualuser,pk,CDC)
  End If
  omatrix1details.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix1details is Nothing then
   omatrix1details = new Objects.matrix1details(currentuser,pk,CDC)
  End If
  omatrix1details.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.profile_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkprofile_fk.Visible=IIF(RL>=0,True,False)
  Me.metertimeswitchcode.Enabled=IIF(WL>=0,True,False)
  Me.blkmetertimeswitchcode.Visible=IIF(RL>=0,True,False)
  Me.linelossfactor.Enabled=IIF(WL>=0,True,False)
  Me.blklinelossfactor.Visible=IIF(RL>=0,True,False)
  Me.distribution_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkdistribution_fk.Visible=IIF(RL>=0,True,False)
  Me.uniqueidentifier.Enabled=IIF(WL>=0,True,False)
  Me.blkuniqueidentifier.Visible=IIF(RL>=0,True,False)
  Me.checkdigit.Enabled=IIF(WL>=0,True,False)
  Me.blkcheckdigit.Visible=IIF(RL>=0,True,False)
  Me.consumptionsingle.Enabled=IIF(WL>=0,True,False)
  Me.blkconsumptionsingle.Visible=IIF(RL>=0,True,False)
  Me.consumptionday.Enabled=IIF(WL>=0,True,False)
  Me.blkconsumptionday.Visible=IIF(RL>=0,True,False)
  Me.consumptionnight.Enabled=IIF(WL>=0,True,False)
  Me.blkconsumptionnight.Visible=IIF(RL>=0,True,False)
  Me.consumptionother.Enabled=IIF(WL>=0,True,False)
  Me.blkconsumptionother.Visible=IIF(RL>=0,True,False)
  Me.current_standingcharge2.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_standingcharge2.Visible=IIF(RL>=0,True,False)
  Me.current_ratesingle.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_ratesingle.Visible=IIF(RL>=0,True,False)
  Me.current_rateday.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_rateday.Visible=IIF(RL>=0,True,False)
  Me.current_ratenight.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_ratenight.Visible=IIF(RL>=0,True,False)
  Me.current_rateother.Enabled=IIF(WL>=0,True,False)
  Me.blkcurrent_rateother.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

