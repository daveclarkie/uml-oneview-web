Partial Class notificationtargets_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents onotificationtargets As Objects.notificationtargets
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_notifications_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.notification_fk.Items.Count=0 Then
        Me.notification_fk.DataSource=CDC.ReadDataTable(CO)
        Me.notification_fk.DataTextField = "value"
        Me.notification_fk.DataValueField = "pk"
        Try
            Me.notification_fk.DataBind
        Catch Ex as Exception
            Me.notification_fk.SelectedValue=-1
            Me.notification_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If onotificationtargets is Nothing then
onotificationtargets = new Objects.notificationtargets(currentuser,actualuser,pk,CDC)
End If
Me.notification_fk.SelectedValue=onotificationtargets.notification_fk
Me.user_fk.SelectedValue=onotificationtargets.user_fk
_CurrentPk=onotificationtargets.notificationtarget_pk
Status=onotificationtargets.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If onotificationtargets is Nothing then
onotificationtargets = new Objects.notificationtargets(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
onotificationtargets.notification_fk=Me.notification_fk.SelectedValue
onotificationtargets.user_fk=Me.user_fk.SelectedValue
result=onotificationtargets.Save()
if not result then throw onotificationtargets.LastError
_CurrentPk=onotificationtargets.notificationtarget_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=onotificationtargets.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotificationtargets is Nothing then
   onotificationtargets = new Objects.notificationtargets(currentuser,actualuser,pk,CDC)
  End If
  onotificationtargets.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotificationtargets is Nothing then
   onotificationtargets = new Objects.notificationtargets(currentuser,actualuser,pk,CDC)
  End If
  onotificationtargets.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotificationtargets is Nothing then
   onotificationtargets = new Objects.notificationtargets(currentuser,pk,CDC)
  End If
  onotificationtargets.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.notification_fk.Enabled=IIF(WL>=0,True,False)
  Me.blknotification_fk.Visible=IIF(RL>=0,True,False)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

