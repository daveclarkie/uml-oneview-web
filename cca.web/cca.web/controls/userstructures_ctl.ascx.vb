Partial Class userstructures_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ouserstructures As Objects.userstructures
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.manageruser_fk.Items.Count=0 Then
        Me.manageruser_fk.DataSource=CDC.ReadDataTable(CO)
        Me.manageruser_fk.DataTextField = "value"
        Me.manageruser_fk.DataValueField = "pk"
        Try
            Me.manageruser_fk.DataBind
        Catch Ex as Exception
            Me.manageruser_fk.SelectedValue=-1
            Me.manageruser_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ouserstructures is Nothing then
ouserstructures = new Objects.userstructures(currentuser,actualuser,pk,CDC)
End If
Me.manageruser_fk.SelectedValue=ouserstructures.manageruser_fk
Me.user_fk.SelectedValue=ouserstructures.user_fk
_CurrentPk=ouserstructures.userstructure_pk
Status=ouserstructures.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ouserstructures is Nothing then
ouserstructures = new Objects.userstructures(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ouserstructures.manageruser_fk=Me.manageruser_fk.SelectedValue
ouserstructures.user_fk=Me.user_fk.SelectedValue
result=ouserstructures.Save()
if not result then throw ouserstructures.LastError
_CurrentPk=ouserstructures.userstructure_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ouserstructures.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserstructures is Nothing then
   ouserstructures = new Objects.userstructures(currentuser,actualuser,pk,CDC)
  End If
  ouserstructures.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserstructures is Nothing then
   ouserstructures = new Objects.userstructures(currentuser,actualuser,pk,CDC)
  End If
  ouserstructures.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserstructures is Nothing then
   ouserstructures = new Objects.userstructures(currentuser,pk,CDC)
  End If
  ouserstructures.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.manageruser_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkmanageruser_fk.Visible=IIF(RL>=0,True,False)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

