<%@ Control Language="VB" AutoEventWireup="false" CodeFile="helpdesklogindetails_ctl.ascx.vb" Inherits="helpdesklogindetails_ctl" %>
<h5>Functional Helpdesk Login Details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkuser_fk">
<span class='label'>User</span>
<asp:DropDownList ID="user_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkusername">
<span class='label'>Username</span>
<asp:TextBox EnableViewState="false" ID="username" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkpassword">
<span class='label'>Password</span>
<asp:TextBox EnableViewState="false" ID="password" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkhelpdeskdomain_fk">
<span class='label'>Domain Name</span>
<asp:DropDownList ID="helpdeskdomain_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkhelpdesklogondomain_fk">
<span class='label'>Logon Domain Name</span>
<asp:DropDownList ID="helpdesklogondomain_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkhelpdeskuserid">
<span class='label'>helpdesk user ID</span>
<asp:TextBox EnableViewState="false" ID="helpdeskuserid" runat="server" cssClass="input_num"  />
</li>
</ul>

