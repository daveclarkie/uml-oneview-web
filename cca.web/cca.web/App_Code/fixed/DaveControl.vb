Imports cca
Imports cca.common

Public Class DaveControl
    Inherits System.Web.UI.UserControl

    
    Public CDC As DataCommon
    Public SM As MySM
    Public Event InitSpecificControl()
    Public Event CompleteSpecificControl()
    Public helpdeskconnectionstring As String

    Protected Sub AddNotice(ByVal Notice As String)
        CType(Me.Page, DavePage).AddNotice(Notice)
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        CType(Me.Page, DavePage).ShareCDCSM(CDC, SM)
        RaiseEvent InitSpecificControl()

        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim subDomain As String = host.Split(".".ToCharArray, 2)(0)
        Dim xstr As String = HttpContext.Current.Server.MapPath("DataCommon.dll")

        'MsgBox("url: " & url)
        'MsgBox("path: " & path)
        'MsgBox("host: " & host)
        'MsgBox("subDomain: " & subDomain)
        'MsgBox("xstr: " & xstr)

        'Dim system As String = ""
        'Select Case True
        '    Case subDomain.StartsWith("live"), subDomain.StartsWith("dev")
        '        system = "live"
        '    Case subDomain.StartsWith("test"), host.Contains("testdev.local")
        '        system = "testing"
        '    Case subDomain.StartsWith("test"), subDomain.StartsWith("localhost"), subDomain.StartsWith("127")
        '        system = "development"
        '    Case Else
        '        system = "unauthorised"
        'End Select
        'Dim hostpath As String = IO.Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().CodeBase)

        'Select Case system
        '    Case "live"
        '        helpdeskconnectionstring = "Data Source=uk-ed0-sqlcl-01;Initial Catalog=servicedesk;Trusted_Connection=True;"
        '    Case "testing"
        '        helpdeskconnectionstring = "Data Source=uk-test-sql-01;Initial Catalog=servicedesk;Trusted_Connection=True;"
        '    Case "development"
        '        helpdeskconnectionstring = "Data Source=uk-test-sql-01;Initial Catalog=servicedesk;Trusted_Connection=True;"
        'End Select
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        RaiseEvent CompleteSpecificControl()
    End Sub

    
End Class
