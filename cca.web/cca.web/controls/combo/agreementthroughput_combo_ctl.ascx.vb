Partial Class agreementthroughput_combo_ctl
    Inherits DaveControl
    Public PSM As MySM

    Public WithEvents oagreementthroughput As Objects.agreementthroughputs
    Public WithEvents othroughput As Objects.throughputs
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = -1
    Dim _Status As Integer = 0
    Dim _STCurrentPk As Integer = -1
    Dim _STStatus As Integer = 0

    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        ControlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub

    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property

    Public ReadOnly Property STCurrentPk() As Integer
        Get
            Return _STCurrentPk
        End Get
    End Property

    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property

    Public Sub ControlInit()
        CO = New SqlClient.SqlCommand("rkg_units_lookup")
        CO.CommandType = CommandType.StoredProcedure
        Me.unit_fk.DataSource = CDC.ReadDataTable(CO)
        Me.unit_fk.DataTextField = "value"
        Me.unit_fk.DataValueField = "pk"
        Try
            Me.unit_fk.DataBind()
        Catch Ex As Exception
            Me.unit_fk.SelectedValue = -1
            Me.unit_fk.DataBind()
        End Try
        CO = New SqlClient.SqlCommand("rkg_frequencys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        Me.datafrequency_fk.DataSource = CDC.ReadDataTable(CO)
        Me.datafrequency_fk.DataTextField = "value"
        Me.datafrequency_fk.DataValueField = "pk"
        Try
            Me.datafrequency_fk.DataBind()
        Catch Ex As Exception
            Me.datafrequency_fk.SelectedValue = -1
            Me.datafrequency_fk.DataBind()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
    Public Sub SetObjects(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByVal pk As Integer = -1)
        If oagreementthroughput Is Nothing Then
            oagreementthroughput = New Objects.agreementthroughputs(currentuser, actualuser, pk, CDC)
            If oagreementthroughput.throughput_fk > 0 Then
                othroughput = New Objects.throughputs(currentuser, actualuser, oagreementthroughput.throughput_fk, CDC)
            Else
                othroughput = New Objects.throughputs(currentuser, actualuser, -1, CDC)
            End If
        End If
        If othroughput Is Nothing Then
            If oagreementthroughput.throughput_fk > 0 Then
                othroughput = New Objects.throughputs(currentuser, actualuser, oagreementthroughput.throughput_fk, CDC)
            Else
                othroughput = New Objects.throughputs(currentuser, actualuser, -1, CDC)
            End If
        End If
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1)
        SetObjects(currentuser, actualuser, pk)
        Me.throughputname.Text = othroughput.throughputname
        Try
            Me.unit_fk.SelectedValue = othroughput.unit_fk
        Catch exunit_fk As Exception
            Me.unit_fk.SelectedValue = -1
        End Try
        SM.subId = oagreementthroughput.throughput_fk
        Try
            Me.datafrequency_fk.SelectedValue = oagreementthroughput.datafrequency_fk
        Catch exdatafrequency_fk As Exception
            Me.datafrequency_fk.SelectedValue = -1
        End Try
        _CurrentPk = oagreementthroughput.agreementthroughput_pk
        _STCurrentPk = othroughput.throughput_pk
        Status = oagreementthroughput.rowstatus Or othroughput.rowstatus
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        SetObjects(currentuser, actualuser, pk)
        othroughput.throughputname = Me.throughputname.Text
        othroughput.unit_fk = Me.unit_fk.SelectedValue
        Dim STResult As Boolean = False
        Try
            STResult = othroughput.Save()
            If Not STResult Then Throw othroughput.LastError
            _STCurrentPk = othroughput.throughput_pk
            SM.subId = othroughput.throughput_pk
        Catch Ex As Exception
            STResult = False
            LastError = Ex
            SM.subId = -1
        End Try
        Try
            oagreementthroughput.agreement_fk = SM.targetAgreement
            oagreementthroughput.throughput_fk = SM.subId
            oagreementthroughput.datafrequency_fk = Me.datafrequency_fk.SelectedValue
        Catch MainEx As Exception
            LastError = MainEx
        End Try
        Dim result As Boolean = False
        Try
            Result = oagreementthroughput.Save()
            If Not Result Then Throw oagreementthroughput.LastError
            _CurrentPk = oagreementthroughput.agreementthroughput_pk
        Catch Ex As Exception
            Result = False
            LastError = Ex
        End Try
        SM.linkId = _CurrentPk
        Status = oagreementthroughput.rowstatus Or othroughput.rowstatus
        Return result And STResult
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If oagreementthroughput Is Nothing Then
            oagreementthroughput = New Objects.agreementthroughputs(currentuser, actualuser, pk, CDC)
        End If
        oagreementthroughput.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If oagreementthroughput Is Nothing Then
            oagreementthroughput = New Objects.agreementthroughputs(currentuser, actualuser, pk, CDC)
        End If
        oagreementthroughput.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If oagreementthroughput Is Nothing Then
            oagreementthroughput = New Objects.agreementthroughputs(currentuser, actualuser, pk, CDC)
        End If
        oagreementthroughput.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.throughputname.Enabled = IIF(WL >= 0, True, False)
        Me.blkthroughputname.Visible = IIF(RL >= 0, True, False)
        Me.unit_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkunit_fk.Visible = IIF(RL >= 0, True, False)
        Me.datafrequency_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkdatafrequency_fk.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class
