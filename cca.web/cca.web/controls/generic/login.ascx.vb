Imports System.DirectoryServices
Imports System.Reflection

Partial Class mainctls_login
    Inherits DaveControl
    Public PSM As MySM
    Private str As Object = HttpContext.Current.ApplicationInstance
    Private assem As Assembly = str.[GetType]().BaseType.Assembly
    Private assemblyVersion As System.Version = assem.GetName().Version

    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM

        'If GetUserName.Length > 0 Then
        '    username.Text = GetUserName
        '    'authentication.SelectedValue = 1
        'Else
        '    username.Text = Nothing
        '    'authentication.SelectedValue = 0
        'End If
        Dim p As System.Security.Principal.WindowsPrincipal = TryCast(System.Threading.Thread.CurrentPrincipal, System.Security.Principal.WindowsPrincipal)

        Dim strName1 As String = HttpContext.Current.User.Identity.Name.ToString()
        Dim strName2 As String = p.Identity.Name
        Dim strName3 As String = Request.ServerVariables("AUTH_USER") 'Finding with name
        Dim strName4 As String = Request.ServerVariables(5) 'Finding with index

        'Response.Write("1:" & strName1 & vbCrLf)
        'Response.Write("2:" & strName2 & vbCrLf)
        'Response.Write("3:" & strName3 & vbCrLf)
        'Response.Write("4:" & strName4 & vbCrLf)

        'username.Text = strName1 & " | " & strName2 & " | " & strName3 & " | " & strName4
        lblversion.Text = assemblyVersion.ToString
    End Sub

    Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, ByRef nSize As Integer) As Integer
    Public Function GetUserName() As String
        Dim iReturn As Integer
        Dim userName As String
        userName = New String(CChar(" "), 50)
        iReturn = GetUserName(userName, 50)
        GetUserName = userName.Substring(0, userName.IndexOf(Chr(0)))
    End Function

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim loginOK As Boolean = False
        Dim savedOK As Boolean = False
        Dim currentUser As Integer = -1
        Dim actualUser As Integer = -1
        Dim CO As New SqlClient.SqlCommand

        CDC.Sanitize(Me.username.Text)
        Me.password.Text = Me.password.Text.Trim




        Try
            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_Login"
            CO.Parameters.AddWithValue("@usr", Me.username.Text)
            CO.Parameters.AddWithValue("@pwd", Me.password.Text)
            Dim dr As DataRow = CDC.ReadDataRow(CO)

            If Not dr Is Nothing Then
                SM.currentuser = dr("user_pk")
                SM.actualuser = dr("user_pk")
                actualUser = dr("user_pk")
                currentUser = dr("user_pk")
                loginOK = True
            End If
            dr = Nothing
        Catch ex As Exception

        End Try

        If Not loginOK Then

            Try
                CO = New SqlClient.SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_loginattempt"
                CO.Parameters.Clear()
                CO.Parameters.AddWithValue("@usr", Me.username.Text)
                CDC.ReadScalarValue(currentUser, CO)
            Catch ex As Exception

            End Try

        End If

        CO = Nothing

        Dim u As New userloginlogs(currentUser, actualUser, CDC)
        u.attemptresult = loginOK
        u.ipaddress = Request.UserHostAddress
        u.useragentstring = Request.UserAgent
        u.rowstatus = 0
        u.username = Me.username.Text
        u.probableuser_fk = currentUser
        If loginOK Then
            u.userpassw = "N/A"
        Else
            CDC.Sanitize(Me.password.Text)
            u.userpassw = Me.password.Text
        End If

        savedOK = u.Save


        If loginOK Then
            Local.Bounce(SM, Response, "~/core/default.aspx", "loginok")
        Else
            Me.lblMessage.Text = "Login failed."
        End If

    End Sub




End Class
