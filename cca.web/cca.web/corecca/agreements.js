﻿    var month;
    var year;
    function setMonth(strmonth) {
        if (checkobject(strmonth)) {
            month = document.getElementById(strmonth).value;
        }
    }
    
    function setYear(stryear) {
        if (checkobject(stryear)) {
            year = document.getElementById(stryear).value;
        }
    }

    function checkobject(obj) {
        if (document.getElementById(obj)) { return true; } else { return false; }
    }
    
    function checkinvoice() {
        var output = document.getElementById("output");
        var total = document.getElementById("totalconsumption");
        var eligible = document.getElementById("eligibleconsumption");
        var rspn = "";
        var keys = "b=" + month + "&c=" + year;
        
        var rqAssign = newRequest();
        rqAssign.open("POST", "/functions/checkinvoiceconsumption.ashx?a=0" + noCache(), false);
        rqAssign.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        rqAssign.send(keys);
        rspn = rqAssign.responseText;

        if (rspn.substring(0, 4) == "FAIL") {
            output.style.display = '';
            chgNamedElementText("output", chgReplace, rspn);
        }
        else {
            output.style.display = '';
            if (rspn == "") {
                chgNamedElementText("output", chgReplace, "There is no consumption in for this period");
            }
            else {
                total.value = rspn;
                eligible.value = rspn;
                chgNamedElementText("output", chgReplace, "");
            }
        }

    } //checkinvoice

    function saveConsumption() {

        var output = document.getElementById("output");
        var total = document.getElementById("totalconsumption");
        var eligible = document.getElementById("eligibleconsumption");
        var keys;
        var rspn = "";

        if (eligible == null) {
            keys = "b=" + month + "&c=" + year + "&d=" + total.value;
        }
        else {
            keys = "b=" + month + "&c=" + year + "&d=" + total.value + "&e=" + eligible.value;
        }
        

        var rqAssign = newRequest();
        rqAssign.open("POST", "/functions/dataentryconsumption.ashx?a=0" + noCache(), false);
        rqAssign.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        rqAssign.send(keys);
        rspn = rqAssign.responseText;

        if (rspn.substring(0, 4) == "FAIL") {
            output.style.display = '';
            chgNamedElementText("output", chgReplace, rspn);
        }
        else {
            output.style.display = '';
            chgNamedElementText("output", chgReplace, rspn);
            //  Get current page url using JavaScript
            var currentPageUrl = "";
            if (typeof this.href === "undefined") {
                currentPageUrl = document.location.toString().toLowerCase();
            }
            else {
                currentPageUrl = this.href.toString().toLowerCase();
            }
            //location.reload();
            window.location.href = removeParameter(currentPageUrl, "spk");
        }
    } //saveConsumption

    function saveThroughput() {

        var output = document.getElementById("output");
        var total = document.getElementById("totalthroughput");
        var eligible = document.getElementById("eligiblethroughput");
        var keys;
        var rspn = "";

        if (eligible == null) {
            keys = "b=" + month + "&c=" + year + "&d=" + total.value;
        }
        else {
            keys = "b=" + month + "&c=" + year + "&d=" + total.value + "&e=" + eligible.value;
        }


        var rqAssign = newRequest();
        rqAssign.open("POST", "/functions/dataentrythroughput.ashx?a=0" + noCache(), false);
        rqAssign.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        rqAssign.send(keys);
        rspn = rqAssign.responseText;

        if (rspn.substring(0, 4) == "FAIL") {
            output.style.display = '';
            chgNamedElementText("output", chgReplace, rspn);
        }
        else {
            output.style.display = '';
            chgNamedElementText("output", chgReplace, rspn);
            location.reload();
        }
    } //saveThroughput



    function removeTarget(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removetarget.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeTarget



    function removeDataConsumption(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removedataconsumption.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeTarget


    function removeDataThroughput(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removedatathroughput.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeTarget

    function removeMeterpoint(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removemeterpoint.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeMeterpoint

    function removeThroughput(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removethroughput.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeThroughput

    function removeAgreementFuel(pk) {
        var selItem = "node_" + pk;
        setReqHttp();
        if (hasXmlhttp == true) {
            xmlhttp.open("GET", "/functions/removeagreementfuel.ashx?pk=" + pk + noCache(), true);
            xmlhttp.send(null);
            var nd = document.getElementById(selItem);
            nd.parentNode.removeChild(nd);
        }
        else {
            alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
        }
    } //removeAgreementFuel


    function loadAnalysisMeters() {

        AvailableAnalysisMeters();
        ActiveAnalysisMeters();
        AvailableAnalysisTargets();

    } //manageAnalysisMeters



    function ActiveAnalysisMeters() {

        var OutputActive = document.getElementById("ActiveAnalysisMeterList");
        var rspn = "";
        var rqSelected = newRequest();
        
        var keys = "c=0";
        rqSelected.open("GET", "/functions/manage_cca_selected_analysis_dataconsumptionmeters.ashx?" + keys + noCache(), false);
        rqSelected.send();
        rspn = rqSelected.responseText;
        chgNamedElementText("ActiveAnalysisMeterList", chgReplace, rspn);
        OutputActive.style.display = "";

    }


    function AvailableAnalysisMeters() {

        var OutputAvailable = document.getElementById("AvailableAnalysisMeterList");
        var rspn = "";
        var rqAvailable = newRequest();

        var keys = "c=1";
        rqAvailable.open("GET", "/functions/manage_cca_selected_analysis_dataconsumptionmeters.ashx?" + keys + noCache(), false);
        rqAvailable.send();
        rspn = rqAvailable.responseText;
        chgNamedElementText("AvailableAnalysisMeterList", chgReplace, rspn);
        OutputAvailable.style.display = "";


    }

    function AvailableAnalysisTargets() {

        var OutputAvailable = document.getElementById("TargetsAnalysisList");
        var rspn = "";
        var rqAvailable = newRequest();

        var keys = "c=2";
        rqAvailable.open("GET", "/functions/manage_cca_selected_analysis_dataconsumptionmeters.ashx?" + keys + noCache(), false);
        rqAvailable.send();
        rspn = rqAvailable.responseText;
        chgNamedElementText("TargetsAnalysisList", chgReplace, rspn);
        OutputAvailable.style.display = "";


    }


    // usergroup_pk,user_fk,group_fk,username

    function SetDate(sourceLI, start, end) {

        document.getElementById("ctl00_content_datefrom").value = start
        document.getElementById("ctl00_content_dateto").value = end

    }

    function Add(sourceLI, agreement_fk, user_fk, meter, meterpoint_fk) {

        var rqMembers = newRequest();
        var dataconsumptionanalysis_pk;

        var keys = "ag=" + agreement_fk + "&mp=" + meterpoint_fk + "&u=" + user_fk;
        rqMembers.open("GET", "/functions/dataanalysisadd.ashx?" + keys + noCache(), false);
        rqMembers.send();
        rspn = rqMembers.responseText;
        
        if (rspn.substring(0, 2) == "OK") {
            dataconsumptionanalysis_pk = rpl(rspn, "OK:", "");
        } else {    
            alert("Ooopsie!");
            return;
        }


        var AvailableMeter;
        var ActiveMeter;
        var li;
        var a;
        var img;

        var originalLi = document.getElementById(sourceLI);

        AvailableMeter = document.getElementById("AvailableAnalysisMeterList");  //current list object is assigned to
        ActiveMeter = document.getElementById("ActiveAnalysisMeterList");  //current list object will be assigned to

        li = document.createElement("li");
        li.id = "AcMeter_" + dataconsumptionanalysis_pk;

        a = document.createElement("a");
        a.href = 'javascript:Remove("AcMeter_' + dataconsumptionanalysis_pk + '",' + dataconsumptionanalysis_pk + ',' + agreement_fk + ',' + user_fk + ',"' + meter + '","' + meterpoint_fk + '");';

        img = document.createElement("img");
        img.src = "/design/images/icon_remove.png";
        img.alt = "remove";
        img.className = "manageGroup_Icon";

        a.appendChild(img);

        var tn;
        tn = document.createTextNode("   ");
        a.appendChild(tn);
        tn = document.createTextNode(meter);
        a.appendChild(tn);

        li.appendChild(a);
        ActiveMeter.appendChild(li);

        AvailableMeter.removeChild(originalLi);

        
    } //add.ashx


    function Remove(sourceLI, dataconsumptionanalysis_pk, agreement_fk, user_fk, meter, meterpoint_fk) {


        var rqAvailable = newRequest();

        var keys = "pk=" + dataconsumptionanalysis_pk;
        rqAvailable.open("GET", "/functions/dataanalysisrem.ashx?" + keys + noCache(), false);
        rqAvailable.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        rqAvailable.send();
        rspn = rqAvailable.responseText;
        if (rspn == "OK") {


            var AvailableMeter;
            var ActiveMeter;
            var li;
            var a;
            var img;

            var originalLi = document.getElementById(sourceLI);

            AvailableMeter = document.getElementById("AvailableAnalysisMeterList");  //current list object is assigned to
            ActiveMeter = document.getElementById("ActiveAnalysisMeterList");  //current list object will be assigned to

            li = document.createElement("li");
            li.id = "AvMeter_" + dataconsumptionanalysis_pk;
            a = document.createElement("a");
            a.href = 'javascript:Add("AvMeter_' + dataconsumptionanalysis_pk + '",' + agreement_fk + ',' + user_fk + ',"' + meter + '","' + meterpoint_fk + '");';
            // sourceLI, agreement_fk, user_fk, meter, meterpoint_fk
            img = document.createElement("img");
            img.src = "/design/images/icon_add.png";
            img.alt = "add";
            img.className = "manageGroup_Icon";
            a.appendChild(img);

            var tn;
            tn = document.createTextNode("   ");
            a.appendChild(tn);
            tn = document.createTextNode(meter);
            a.appendChild(tn);

            li.appendChild(a);
            AvailableMeter.appendChild(li);

            ActiveMeter.removeChild(originalLi);

        }
        else {

            alert(rspn.responseText);
        }

    } //rem.ashx


//    function viewCCAReport(pk) {

//        var rspn = "";
//        var rqAvailable = newRequest();

//        rqAvailable.open("GET", "/functions/viewccareport.ashx?pk=" + pk + noCache(), true);
//        rqAvailable.send();
//        rspn = rqAvailable.responseText;
//        rspn.replace(new RegExp(escapeRegExp("\"), 'g'), "/");
//        
//    } //viewCCAReport