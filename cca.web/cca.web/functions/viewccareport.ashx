<%@ WebHandler Language="VB" Class="functions_viewccareport" %>
Public Class functions_viewccareport
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim CRQ As Object = Request.QueryString
        Dim Obj As objects.reportlogs
        Dim Msg As String = "OK"
        Try
            Obj = New objects.reportlogs(SM.currentuser, SM.actualuser, Integer.Parse(CRQ("pk")), CDC)
            'Set the appropriate ContentType.

            If System.IO.File.Exists(Obj.savedpath) = True Then
                'Diagnostics.Process.Start(Obj.savedpath)
                Msg = Obj.savedpath
            Else
                Msg = "File Does Not Exist"
            End If
            
            
            'Dim pdfPath As String = Obj.savedpath
            'Dim client As New System.Net.WebClient()
            'Dim buffer As [Byte]() = client.DownloadData(pdfPath)
            'Response.ContentType = "application/pdf"
            'Response.AddHeader("content-length", buffer.Length.ToString())
            'Response.BinaryWrite(buffer)
            
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
    End Sub

End Class
