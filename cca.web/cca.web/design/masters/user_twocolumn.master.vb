Imports System.Data.SqlClient

Partial Class design_masters_user_twocolumn
    Inherits DaveMaster
    Public PSM As MySM
    Public Sub InitMaster() Handles MyBase.InitSpecificMaster
        ' Place any page specific initialisation code here
        PSM = SM
        CheckSecurity()
    End Sub
    Public Sub CompleteMaster() Handles MyBase.CompleteSpecificMaster
        ' Place any page specific initialisation code here
    End Sub

    Private Sub CheckSecurity()
        Page.Title = "Schneider Electric - One View"
        Dim pr As String = ""
        Try
            pr = Request.ServerVariables("HTTP_REFERER")
        Catch ex As Exception
            pr = ""
        End Try

        Try
            If SM.currentuser = 0 Then
                Dim p As DavePage = CType(Page, DavePage)
                p.ShareCDCSM(CDC, SM)
                AddNotice("Had to reload Session/CDC. Tell Dave.")
            End If
        Catch foo_ked As Exception
            AddNotice("Session reload barfed. Tell Dave.")
        End Try

        Try

            If SM.currentuser = 0 Then
                If pr.Contains("/core/") Then
                    Local.FOAD(Response, SM, "No+Current+User+for+Core", "~/expiry.aspx")
                    Context.ApplicationInstance.CompleteRequest()
                Else
                    Local.FOAD(Response, SM, "No+Current+User", "~/login.aspx")
                    Context.ApplicationInstance.CompleteRequest()
                End If
            End If
        Catch ex As Exception
            Local.FOAD(Response, SM, Server.UrlEncode(ex.Message), "~/expiry.aspx")
            Context.ApplicationInstance.CompleteRequest()
        End Try

        Dim secPGTask As Boolean = IIf(Request.Path.Contains("/tasks.aspx"), True, False)
        Dim secPGNotice As Boolean = IIf(Request.Path.Contains("/notify.aspx"), True, False)
        Dim secPGPerson As Boolean = IIf(Request.Path.Contains("/persons.aspx"), True, False)
        Dim secPGInvoice As Boolean = IIf(Request.Path.Contains("/invoice.aspx"), True, False)

        Dim SecTON As Boolean = secPGTask Or secPGNotice
        Dim secPages As Boolean = secPGPerson Or secPGInvoice
        Dim secHasQSI As Boolean = IIf(Request.QueryString.Count > 0, True, False)
        ' TODO: remove both when live
        ' setting this to true bypasses notifications and tasks checks
        SecTON = True
        ' setting this to false bypasses restricted page check
        'secPages = False

        If Not SecTON Then
            If SM.noticecount > 0 Then
                Local.Bounce(SM, Response, "~/core/notify.aspx", "you+have+notices+to+read")
                Context.ApplicationInstance.CompleteRequest()
            End If
        Else
            If secPages And secHasQSI Then
                Select Case True
                    Case secPGPerson
                        Dim p As Integer = 0
                        If Not Integer.TryParse(Request.QueryString("pk"), p) Then
                            ' has querystring params but not the important one
                            Local.Bounce(SM, Response, "~/core/noqs.aspx", Server.UrlEncode(Request.ServerVariables("HTTP_REFERER") & " :  Required Querystring Params are missing for user access") & "&p=" & Server.UrlEncode(Request.RawUrl))
                            Context.ApplicationInstance.CompleteRequest()
                        Else
                            'If Not HasUserAccess(p) Then
                            '    Local.Bounce(SM, Response, "~/core/denied.aspx", "access+denied&p=" & p)
                            '    Context.ApplicationInstance.CompleteRequest()
                            'End If
                        End If
                    Case secPGInvoice
                        Dim i As Integer = 0
                        If Not Integer.TryParse(Request.QueryString("pk"), i) And Request.QueryString("md") <> "" Then
                            ' has querystring params but not the important one
                            Local.Bounce(SM, Response, "~/core/noqs.aspx", Server.UrlEncode(Request.ServerVariables("HTTP_REFERER") & " :  Required Querystring Params are missing for pack access") & "&p=" & Server.UrlEncode(Request.RawUrl))
                            Context.ApplicationInstance.CompleteRequest()
                        Else
                            If Not HasPackAccess(i) Then
                                Local.Bounce(SM, Response, "search.aspx", "no+Pack+access")
                                Context.ApplicationInstance.CompleteRequest()
                            End If
                        End If
                End Select
            End If
        End If

        Dim u As New users(SM.currentuser, SM.actualuser, SM.currentuser, CDC)
        If u.disconnect Then
            u.disconnect = False
            u.Save()
            Local.FOAD(Response, SM, "disconnected+so+you+need+to+log+in+again", "~/disconnect.aspx")
            Context.ApplicationInstance.CompleteRequest()
        End If

        Dim actlog As New activitylogs(SM.currentuser, SM.actualuser, CDC)
        actlog.ipaddress = Request.UserHostAddress
        actlog.useragent = Request.UserAgent
        actlog.user_fk = SM.currentuser
        actlog.Save()
        actlog = Nothing

    End Sub

    Private Function HasUserAccess(ByVal user As Integer) As Boolean
        Dim retVal As Integer = 0
        Dim sCmd As SqlCommand
        sCmd = New SqlCommand

        sCmd.CommandText = "rsp_hasuseraccess"
        sCmd.CommandType = CommandType.StoredProcedure
        sCmd.Parameters.AddWithValue("@usr", SM.currentuser)
        sCmd.Parameters.AddWithValue("@cli", user)
        CDC.ReadScalarValue(retVal, sCmd)
        If retVal <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function HasPackAccess(ByVal invoice As Integer) As Boolean
        Dim retVal As Integer = 0
        Dim sCmd As SqlCommand
        sCmd = New SqlCommand

        sCmd.CommandText = "rsp_haspackaccess"
        sCmd.CommandType = CommandType.StoredProcedure
        sCmd.Parameters.AddWithValue("@usr", SM.currentuser)
        sCmd.Parameters.AddWithValue("@ref", invoice)
        CDC.ReadScalarValue(retVal, sCmd)
        If retVal <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class

