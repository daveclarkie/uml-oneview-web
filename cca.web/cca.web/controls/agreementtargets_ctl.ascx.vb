Partial Class agreementtargets_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreementtargets As Objects.agreementtargets
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        Me.target.Text = "0.00"
        Me.eacarbonsurplus.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oagreementtargets Is Nothing Then
            oagreementtargets = New Objects.agreementtargets(currentuser, actualuser, pk, CDC)
        End If
        If pk = -1 Then
            Me.agreement_fk.SelectedValue = SM.targetAgreement
        Else
            Me.agreement_fk.SelectedValue = oagreementtargets.agreement_fk
        End If

        Me.targetdescription.Text = oagreementtargets.targetdescription
        Me.targetstart.Text = oagreementtargets.targetstart.ToString("dd MMM yyyy")
        If Me.targetstart.Text = "01 Jan 0001" Then Me.targetstart.Text = ""
        Me.targetstart.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.targetstart.Attributes.Add("onBlur", "clearNotice()")
        Me.targetend.Text = oagreementtargets.targetend.ToString("dd MMM yyyy")
        If Me.targetend.Text = "01 Jan 0001" Then Me.targetend.Text = ""
        Me.targetend.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.targetend.Attributes.Add("onBlur", "clearNotice()")
        Me.target.Text = oagreementtargets.target
        Me.target.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.target.Attributes.Add("onBlur", "clearNotice()")
        Me.eacarbonsurplus.Text = oagreementtargets.eacarbonsurplus
        Me.eacarbonsurplus.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.eacarbonsurplus.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = oagreementtargets.agreementtarget_pk
        Status = oagreementtargets.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementtargets Is Nothing Then
            oagreementtargets = New Objects.agreementtargets(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreementtargets.agreement_fk = Me.agreement_fk.SelectedValue
            oagreementtargets.targetdescription = Me.targetdescription.Text
            oagreementtargets.targetstart = CommonFN.CheckEmptyDate(Me.targetstart.Text)
            oagreementtargets.targetend = CommonFN.CheckEmptyDate(Me.targetend.Text)
            oagreementtargets.target = Me.target.Text
            oagreementtargets.eacarbonsurplus = Me.eacarbonsurplus.Text
            result = oagreementtargets.Save()
            If Not result Then Throw oagreementtargets.LastError
            _CurrentPk = oagreementtargets.agreementtarget_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreementtargets.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementtargets Is Nothing Then
            oagreementtargets = New Objects.agreementtargets(currentuser, actualuser, pk, CDC)
        End If
        oagreementtargets.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementtargets Is Nothing Then
            oagreementtargets = New Objects.agreementtargets(currentuser, actualuser, pk, CDC)
        End If
        oagreementtargets.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementtargets Is Nothing Then
            oagreementtargets = New Objects.agreementtargets(currentuser, pk, CDC)
        End If
        oagreementtargets.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.agreement_fk.Enabled = IIf(WL >= 99, True, False)
        Me.blkagreement_fk.Visible = IIf(RL >= 99, True, False)
        Me.targetdescription.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetdescription.Visible = IIF(RL >= 0, True, False)
        Me.targetstart.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetstart.Visible = IIF(RL >= 0, True, False)
        Me.targetend.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetend.Visible = IIF(RL >= 0, True, False)
        Me.target.Enabled = IIF(WL >= 0, True, False)
        Me.blktarget.Visible = IIF(RL >= 0, True, False)
        Me.eacarbonsurplus.Enabled = IIF(WL >= 0, True, False)
        Me.blkeacarbonsurplus.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

