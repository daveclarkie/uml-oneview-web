﻿// JScript File



// loadgroups

function loadgroups(pk){
    user_pk = pk;
    //resetUserStatus();
    clearall();
    reqGroupsAvl(pk);
    //checkUserStatus();
}

// groups available
function reqGroupsAvlrefresh() {
    clearall();
    reqGroupsAvl(user_pk);
}


function reqGroupsAvl(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/grpavl.aspx?u="+pk+noCache(),true);
    set_readGroupsAvl();
    xmlhttp.send();
}
function set_readGroupsAvl(){
     xmlhttp.onreadystatechange=function() 
    {
        if (xmlhttp.readyState==4) 
        {
            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0){alert(xmlhttp.responseText);}
            else
            {
                c=xmlDoc.getElementsByTagName("results").length;
                if (c!=0){listGroupsAvl(xmlDoc);}
            }
            reqGroupsAsg(user_pk);
        }
    }   
}

function listGroupsAvl(results){
    var ctl=document.getElementById("lstavlgrp");
    var gpk;
    //var ugpk;
    var grpname;
    var orgname;
    //var actname;   
    var lstgroups = results.getElementsByTagName("avlgroups");
    for(var i=0; i<lstgroups.length; i++)
    {
        try {gpk=lstgroups[i].getElementsByTagName("group_pk")[0].childNodes[0].nodeValue;} catch (e) {gpk='N/A';}
        try {grpname=lstgroups[i].getElementsByTagName("groupname")[0].childNodes[0].nodeValue;} catch (e) {grpname='N/A';}
        try {orgname=lstgroups[i].getElementsByTagName("orgname")[0].childNodes[0].nodeValue;} catch (e) {orgname='Systems';}

        li=document.createElement("li");
        li.id="li_"+gpk;
        a=document.createElement("a");
        a.id="a_add_"+gpk;
        txt=document.createTextNode("[Add]");
        a.href="javascript:groupadd("+gpk+")";
        a.appendChild(txt);
        li.appendChild(a);
        txt=document.createTextNode(" "+grpname+" ");
        li.appendChild(txt);
        li.appendChild(document.createTextNode(" "));
        a=document.createElement("a");
        a.id="a_det_"+gpk;
        txt=document.createTextNode("[Detail]");
        a.href="javascript:reqRoles("+gpk+")";
        //a.appendChild(txt);
        li.appendChild(a);
        ctl.appendChild(li);  
    }



}

// groups assigned



function reqGroupsAsg(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/grpasg.aspx?u="+pk+noCache(),true);
    set_readGroupsAsg();
    xmlhttp.send();
}

function set_readGroupsAsg(){
     xmlhttp.onreadystatechange=function() 
    {
        if (xmlhttp.readyState==4) 
        {
            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0){alert(xmlhttp.responseText);}
            else
            {
                c=xmlDoc.getElementsByTagName("results").length;
                if (c!=0){listGroupsAsg(xmlDoc);}
            }
        }
    }   
}

function listGroupsAsg(results) {
    var ctl=document.getElementById("lstassgrp");
    var gpk;
    //var ugpk;
    var grpname;
    var orgname;
    //var actname;   
    var lstgroups=results.getElementsByTagName("asgngroups");
    for(var i=0; i<lstgroups.length; i++)
    {
        try {gpk=lstgroups[i].getElementsByTagName("group_pk")[0].childNodes[0].nodeValue;} catch (e) {gpk='N/A';}
        try {ugpk=lstgroups[i].getElementsByTagName("usergroup_pk")[0].childNodes[0].nodeValue;} catch (e) {ugpk='N/A';}
        try {grpname=lstgroups[i].getElementsByTagName("groupname")[0].childNodes[0].nodeValue;} catch (e) {grpname='N/A';}
        try {orgname=lstgroups[i].getElementsByTagName("orgname")[0].childNodes[0].nodeValue;} catch (e) {orgname='Systems';}
        try {actname=lstgroups[i].getElementsByTagName("actionname")[0].childNodes[0].nodeValue;} catch (e) {actname='N/A';}
        li=document.createElement("li");
        li.id="li_"+gpk;
        a=document.createElement("a");
        txt=document.createTextNode("[Remove]");
        a.href="javascript:groupremove("+ugpk+")";
        a.appendChild(txt);
        li.appendChild(a);
        txt=document.createTextNode(" "+grpname+" ");
        li.appendChild(txt);
        a=document.createElement("a");
        a.id="a_swp_"+ugpk;
        txt=document.createTextNode(actname);
        a.href="javascript:groupswitch("+ugpk+",'a_swp_"+ugpk+"')";
        a.appendChild(txt);
        li.appendChild(a);
        li.appendChild(document.createTextNode(" "));
        a=document.createElement("a");
        a.id="a_det_"+gpk;
        txt=document.createTextNode("[Detail]");
        a.href="javascript:reqRoles("+gpk+")";
        //a.appendChild(txt);
        li.appendChild(a);
        ctl.appendChild(li);  
    }
}


// clear


function clearall(){
    clear("group"); //assigned groups
}

function clear(opt){
    switch(opt)
    {
        case 'group':
            clearobject("lstassgrp"); //assigned groups
            clearobject("lstavlgrp"); //available groups
            break;
        default:
            break;
    } 
}

function clearobject(objname){
    var clrobj=document.getElementById(objname);
    try
        { 
            while (clrobj.hasChildNodes)
            {
                clrobj.removeChild(clrobj.lastChild);
            }
        }
   catch (e) {
   } 
}

// groups roles

var g_pk=0;
var rqRoles;
function reqRoles(pk){
    g_pk=pk;
    rqRoles=newRequest();
    setCallback(rqRoles,set_readRoles);
    if ( typeof rqRoles != "boolean" ){
        rqRoles.open("GET","/functions/grouproles.ashx?g="+pk+noCache(),false);
        setCallback(rqRoles,set_readRoles);
        rqRoles.send();
    }
}

function set_readRoles(){
        if (rqRoles.readyState==4) 
        {
            clear('role');
            chgElementText(document.getElementById('lstgrprls'),chgReplace,rqRoles.responseText); 
        }
}

function listRoles(results){
    var ctl=document.getElementById("lstgrprls");
    var pk;
    var name;
    var orgname;

    var lstroles=results.getElementsByTagName("roles");
    for(var i=0; i<lstroles.length; i++)
    {
        try {pk=lstroles[i].getElementsByTagName("role_pk")[0].childNodes[0].nodeValue;} catch (e) {pk='N/A';}
        try {name=lstroles[i].getElementsByTagName("rolename")[0].childNodes[0].nodeValue;} catch (e) {name='N/A';}
        try {orgname=lstroles[i].getElementsByTagName("orgname")[0].childNodes[0].nodeValue;} catch (e) {orgname='Systems';}
        li=document.createElement("li");
        a=document.createElement("a");
        txt=document.createTextNode("[ i ]");
        a.href="javascript:reqpermissions("+pk+")";
        a.appendChild(txt);
        li.appendChild(a);
        txt=document.createTextNode(" "+name+", "+orgname+" ");
        li.appendChild(txt);
        ctl.appendChild(li);  
    }

}

// groups permissions 


function reqpermissions(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/rlsprm.aspx?pk="+pk+noCache(),false);
    set_readpermissions();
    xmlhttp.send();
}

function set_readpermissions(){
     xmlhttp.onreadystatechange=function() 
    {
        if (xmlhttp.readyState==4) 
        {
            clear('perm');
            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0){alert(xmlhttp.responseText);}
            else
            {
                c=xmlDoc.getElementsByTagName("results").length;
                if (c!=0){listpermissions(xmlDoc);}
                else {clear('perm');}
            }
        }
    }   
}

function listpermissions(results){
    var ctl=document.getElementById("lstrlsprm");
    var pk;
    var name;

    var lstpermissions=results.getElementsByTagName("permissions");
    for(var i=0; i<lstpermissions.length; i++)
    {
        try {pk=lstpermissions[i].getElementsByTagName("permission_pk")[0].childNodes[0].nodeValue;} catch (e) {pk='N/A';}
        try {name=lstpermissions[i].getElementsByTagName("permissionname")[0].childNodes[0].nodeValue;} catch (e) {name='N/A';}
        li=document.createElement("li");
        txt=document.createTextNode(name);
        li.appendChild(txt);
        ctl.appendChild(li);  
    }
}

// groups task


function reqtasks(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/grptsk.aspx?pk="+pk+noCache(),true);
    set_readtasks();
    xmlhttp.send();
}

function set_readtasks(){
     xmlhttp.onreadystatechange=function() 
    {
        if (xmlhttp.readyState==4) 
        {
            clear('task');
            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0){alert(xmlhttp.responseText);}
            else
            {
                c=xmlDoc.getElementsByTagName("results").length;
                if (c!=0){listtasks(xmlDoc);}
                else {clear('task');}
            }
        }
    }   
}

function listtasks(results){
    var ctl=document.getElementById("lstgrptsk");
    var pk;
    var name;
    var orgname;

    var lsttasks=results.getElementsByTagName("tasks");
    for(var i=0; i<lsttasks.length; i++)
    {
        try {pk=lsttasks[i].getElementsByTagName("task_pk")[0].childNodes[0].nodeValue;} catch (e) {pk='N/A';}
        try {name=lsttasks[i].getElementsByTagName("taskname")[0].childNodes[0].nodeValue;} catch (e) {name='N/A';}
        li=document.createElement("li");
        txt=document.createTextNode(name);
        li.appendChild(txt);
        ctl.appendChild(li);  
    }
}


// groups changes


function groupswitch(pk,objname){
    setReqHttp();
    xmlhttp.open("GET","/functions/grpswt.aspx?pk="+pk+noCache(),false);
    xmlhttp.send();
    var obj=document.getElementById(objname); 
    switch (xmlhttp.responseText)
        { 
            case "OK":
                if (obj.innerText!='[Suspend]'){
                   chgElementText(obj,chgReplace,'[Suspend]'); 
                } else {
                   chgElementText(obj,chgReplace,'[Activate]'); 
                }
                break;
            default:
                alert(xmlhttp.responseText); 
        }
}

function groupadd(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/grpadd.aspx?pk="+pk+"&u="+user_pk+noCache(),false);
    xmlhttp.send();

    switch (xmlhttp.responseText)
        { 
            case "OK":
                loadgroups(user_pk);
                break;
            default:
                alert(xmlhttp.responseText); 
        }
}

function groupremove(pk){
    setReqHttp();
    xmlhttp.open("GET","/functions/grprem.aspx?pk="+pk+noCache(),false);
    xmlhttp.send();

    switch (xmlhttp.responseText)
        { 
            case "OK":
                loadgroups(user_pk);
                break;
            default:
                alert(xmlhttp.responseText); 
        }
}



// Activation 


function callback_activateUser(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail" || xmlhttp.status==404){
                if (xmlhttp.status==404){ 
                    alert("API 404");
                } else {
                    alert("Attempted action failed:-\n"+rpl(rspn,"Fail:",""));
                }
            }else{
                var lnk=document.getElementById("lnk1");
                chgElementText(lnk,chgReplace,"Active");
            }
            showAsyncAction(icontrgt,0,"wait") 
        }
    }
}

function activateUser(){
    if (user_pk!=0){
        icontrgt="img1";
        setReqHttp();
        if (hasXmlhttp==true){
            showAsyncAction(icontrgt,1,"wait") 
            xmlhttp.open("GET","../functions/actusr.aspx?u="+user_pk+noCache());
            callback_activateUser();
            xmlhttp.send(null);   
        }
       else
       {
        alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
       } 
   } else {
    alert("Select a user first.");
   }
}

function callback_deactivateUser(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail" || rspn.indexOf("404")!=-1){
                if (rspn.indexOf("404")!=-1){ 
                    alert("API 404");
                } else {
                    alert("Failed to deactivate user account:-\n"+rpl(rspn,"Fail:",""));
                }
            }else{
                var lnk=document.getElementById("lnk2");
                chgElementText(lnk,chgReplace,"Inactive");
            }
            showAsyncAction(icontrgt,0,"wait") 
        }
    }
}

function deactivateUser(){
    if (user_pk!=0){
        icontrgt="img1";
        setReqHttp();
        if (hasXmlhttp==true){
            showAsyncAction(icontrgt,1,"wait") 
            xmlhttp.open("GET","../functions/deactusr.aspx?u="+user_pk+noCache());
            callback_deactivateUser();
            xmlhttp.send(null);   
        }
       else
       {
        alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
       } 
   } else {
    alert("Select a user first.");
   }
}


var csRQ;

function checkUserStatus(){

    csRQ=newRequest();
    if ( typeof csRQ != "boolean" ){
        csRQ.open("GET","/functions/checkstatus.ashx?pk="+user_pk+noCache(),true);
        setCallback(csRQ,checkUserStatusResponse);
        csRQ.send();
    }


}


function resetUserStatus(){
    chgElementText(document.getElementById('userstatus'),chgReplace,'Not Checked'); 
    chgElementText(document.getElementById('lnk1'),chgReplace,'Activate'); 
    chgElementText(document.getElementById('lnk2'),chgReplace,'Dectivate'); 
}

function checkUserStatusResponse(){
    if (csRQ.readyState==4){
                
        var rspn=csRQ.responseText;
        var sSta,sAct,sDea
        if (rspn.substring(0,3)=="OK:"){
            var r=rspn.split(":");
            
            if (r[1]=="0"){ sSta="Active"; sAct=""; sDea="Deactivate";} 
            if (r[1]=="1"){ sSta="Suspended"; sAct="Activate"; sDea="";} 
            if (r[1]=="2"){ sSta="Deleted"; sAct=""; sDea="";} 

            chgElementText(document.getElementById('userstatus'),chgReplace,sSta); 
            chgElementText(document.getElementById('lnk1'),chgReplace,sAct); 
            chgElementText(document.getElementById('lnk2'),chgReplace,sDea); 
            document.getElementById('readlevel').value=r[2]; 
            document.getElementById('writelevel').value=r[3];

        } else {
            alert("Failed to confirm user status:-\n"+rpl(rspn,"Fail:",""));
        }
        

        csRQ = null;
    }
}

var srwRQ;

function setUserRW(){

   var rl=document.getElementById('readlevel').value;
   var wl=document.getElementById('writelevel').value;
    
    srwRQ=newRequest();
    if ( typeof srwRQ != "boolean" ){
        srwRQ.open("GET","/functions/setuserrw.ashx?pk="+user_pk+"&r="+rl+"&w="+wl+noCache(),true);
        setCallback(srwRQ,setUserRWResponse);
        srwRQ.send();
    }
}

function setUserRWResponse(){
    if (srwRQ.readyState==4){
        var rspn=srwRQ.responseText;
        if (rspn.substring(0,2)!="OK"){
            alert("Failed to confirm user status:-\n"+rpl(rspn,"Fail:",""));
        }
        srwRQ = null;
    }
}


