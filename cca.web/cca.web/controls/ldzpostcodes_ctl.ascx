<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ldzpostcodes_ctl.ascx.vb" Inherits="ldzpostcodes_ctl" %>
<h5>ldzpostcodes</h5>
<ul class='formcontrol'>
<li runat="server" id="blkldz_fk">
<span class='label'>LDZ</span>
<asp:DropDownList ID="ldz_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkpostcode">
<span class='label'>Post Code</span>
<asp:TextBox EnableViewState="false" ID="postcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

