Imports System.Data.SqlClient

Partial Class headctls_systems
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Dim _label As String = ""
    Dim _items As Integer = 0
    Dim _systemItemType As SystemItemType = SystemItemType.FailedLogins

    Public ReadOnly Property Items() As Integer
        Get
            Return _items
        End Get
    End Property

    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public ReadOnly Property State() As String
        Get
            Select Case SystemItemType
                Case SystemItemType.ActiveUsers, SystemItemType.ActiveSessions
                    Return " green"
                Case Else
                    Select Case _items
                        Case 0 : Return " green"
                        Case Is < 6 : Return " yellow"
                        Case Else : Return " red"
                    End Select
            End Select
            Return ""
        End Get
    End Property

    Public Property SystemItemType() As SystemItemType
        Get
            Return _systemItemType
        End Get
        Set(ByVal value As SystemItemType)
            _systemItemType = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Refresh()
    End Sub

    Public Sub Refresh()
        Dim ts As Long
        Dim qs As Long
        Dim qd As Long
        Dim ad As Long

        ts = Now.Ticks
        If CDC Is Nothing Then CDC = New DataCommon

        Dim CO As New SqlCommand

        Select Case SystemItemType
            Case SystemItemType.ActiveUsers
                CO.CommandText = "rsp_sys_activeusers"
            Case SystemItemType.FailedLogins
                CO.CommandText = "rsp_sys_failedlogins"
            Case SystemItemType.MultipleLogins
                CO.CommandText = "rsp_sys_multiplelogins"
            Case SystemItemType.DormantAccounts
                CO.CommandText = "rsp_sys_dormantaccounts"
            Case SystemItemType.ActivateOnLive
                CO.CommandText = "rsp_sys_activate"
                'Case SystemItemType.ActiveSessions
                'CO.CommandText = "rsp_sys_sessions"
            Case Else
                CO.Dispose()
                Exit Sub
        End Select

        CO.CommandType = CommandType.StoredProcedure
        qs = Now.Ticks
        CDC.ReadScalarValue(_items, CO)
        CO.Dispose()
        qd = Now.Ticks

        Dim ctl As String = ""
        ctl &= "<a href=""./stats.aspx?s="
        ctl &= Integer.Parse(SystemItemType).ToString
        ctl &= """ class=""notifylink"">"
        ctl &= "<div class=""notify"
        ctl &= State
        ctl &= """><span class=""count"">"
        ctl &= Items
        ctl &= "</span><br /><span class=""counttype"">"
        ctl &= Label
        ctl &= "</span></div></a>"
        systemitem.Text = ctl
        ad = Now.Ticks


        System.Diagnostics.Debug.WriteLine(SystemItemType.ToString & " ts-qs : " & (qs - ts).ToString)
        System.Diagnostics.Debug.WriteLine(SystemItemType.ToString & " qs-qd : " & (qd - qs).ToString)
        System.Diagnostics.Debug.WriteLine(SystemItemType.ToString & " qd-ad : " & (ad - qd).ToString)
        System.Diagnostics.Debug.WriteLine(SystemItemType.ToString & " ts-ad : " & (ad - ts).ToString)
    End Sub
End Class
