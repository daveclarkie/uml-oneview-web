<%@ Page Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="groupmanagement.aspx.vb" Inherits="backend_groupmanagement" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    
    <h3>Manage Groups</h3>
    
    <ul class="formcontrol list">
    <li><br /></li>
        <li><span class="label">Manage:</span>
        <asp:DropDownList ID="groupList" runat="server" AppendDataBoundItems="True">
            <asp:ListItem Value="-1">Select . . .</asp:ListItem>
        </asp:DropDownList></li>
    <li><br /></li>
    </ul>
    
<asp:Panel ID="manageuserssection" runat="server">
    <div>
        <div class="left c2f">
                    <h3 class="c2f">Active Members</h3>
                    
                    <ul class="list" id="ActiveUserList">
                    </ul>

        </div>

        <div class="right c2f">
                    <h3 class="c2f">Available Users</h3>
                    
                    <ul class="list" id="AvailableUserList">
                    </ul>


        </div>
    </div>
    
</asp:Panel>

</asp:Content>

