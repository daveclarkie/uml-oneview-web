<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrixtypes_ctl.ascx.vb" Inherits="matrixtypes_ctl" %>
<h5>matrixtypes</h5>
<ul class='formcontrol'>
<li runat="server" id="blktypename">
<span class='label'>typename</span>
<asp:TextBox EnableViewState="false" ID="typename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcountry_fk">
<span class='label'>Country<span class='pop'>This is the country that the matrix belongs to</span></span>
<asp:DropDownList ID="country_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkfuel_fk">
<span class='label'>Fuel<span class='pop'>This is the fuel used</span></span>
<asp:DropDownList ID="fuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkreporturl">
<span class='label'>Report path<span class='pop'>This is the path to the report that will generate the matrix ranking</span></span>
<asp:TextBox EnableViewState="false" ID="reporturl" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

