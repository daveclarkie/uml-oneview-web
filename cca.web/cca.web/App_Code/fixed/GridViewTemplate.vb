Public Class GridViewTemplate
    Implements ITemplate
    Dim _templateType As ListItemType
    Dim _columnName As String

    Public Sub New(ByVal type As ListItemType, ByVal colname As String)
        _templateType = type
        _columnName = colname
    End Sub

    Private Sub ITemplate_InstantiateIn(ByVal container As System.Web.UI.Control) Implements ITemplate.InstantiateIn
        Select Case _templateType
            Case ListItemType.Header
                Dim lbl As New Label()
                lbl.Text = _columnName
                container.Controls.Add(lbl)
            Case ListItemType.Item
                Dim lbl As New Label()
                lbl.Text = _columnName
                AddHandler lbl.DataBinding, AddressOf lbl_DataBinding
                container.Controls.Add(lbl)
        End Select
    End Sub

    Private Sub lbl_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbl As Label = DirectCast(sender, Label)
        Dim container As GridViewRow = DirectCast(lbl.NamingContainer, GridViewRow)
        Dim dataValue As Object = DataBinder.Eval(container.DataItem, _columnName)
        If Not dataValue Is DBNull.Value Then
            lbl.Text = dataValue.ToString()
        End If
    End Sub

End Class
