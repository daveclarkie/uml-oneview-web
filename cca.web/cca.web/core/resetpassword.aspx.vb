Partial Class core_resetpassword
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
       

    End Sub


    Public ReadOnly Property ccaSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.CCASystem, CDC)
        End Get
    End Property

    Public ReadOnly Property BackendAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.AccessToBackendSystems, CDC)
        End Get
    End Property
End Class
