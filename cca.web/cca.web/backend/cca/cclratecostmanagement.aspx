﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="cclratecostmanagement.aspx.vb" Inherits="backend_cca_cclratecostmanagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current CCL Rate Costs</h5>
<asp:Repeater ID="rptCCLRateCosts" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:100px;">
            <h4>Fuel</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid From</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid To</h4>
        </td>
        <td style="width:80px;">
            <h4>CCL Rate</h4>
        </td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("cclrate_pk") %>' style='background-color:<%# Container.DataItem("live")%>;'>
            <td>
                <%# Container.DataItem("fuel")%>
            </td>
            <td>
                <%# Container.DataItem("validfrom")%>
            </td>
            <td>
                <%# Container.DataItem("validto")%>
            </td>
            <td>
                <%# Container.DataItem("cclrate")%>
            </td>
            
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:600">
                <h5>CCL Rates are currently administered via Contract Manager</h5>
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
                

</asp:Content>