﻿// JScript File


function closeCreate(){
    chgNamedElementText("frmCreateNewUser",chgReplace,"");
    chgNamedElementText("rspCreateNewUser",chgReplace,"");
}

function createUser(){

    var postData="";
    
    postData=postData+"fn="+document.getElementById("frmFn").value+"&";
    postData=postData+"sn="+document.getElementById("frmSn").value+"&";
    postData=postData+"db="+document.getElementById("frmDb").value+"&";
    postData=postData+"pc="+document.getElementById("frmPc").value+"&";
    postData=postData+"nm="+document.getElementById("frmHn").value+"&";
    postData=postData+"ph="+document.getElementById("frmPh").value+"&";
    postData=postData+"em="+document.getElementById("frmEm").value;

    clearError();

    rqCFD=newRequest();
    if ( typeof rqCFD != "boolean" ){
        rqCFD.open("POST","/functions/createUser.ashx?a=0"+noCache(),false);
        rqCFD.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        setCallback(rqCFD,cnuResponse);
        rqCFD.send(postData);
    }


}


function testDuplicate(){

    var postData="";
    
    postData=postData+"fn="+document.getElementById("frmFn").value+"&";
    postData=postData+"sn="+document.getElementById("frmSn").value+"&";
    postData=postData+"db="+document.getElementById("frmDb").value+"&";
    postData=postData+"pc="+document.getElementById("frmPc").value+"&";
    postData=postData+"nm="+document.getElementById("frmHn").value+"&";
    postData=postData+"ph="+document.getElementById("frmPh").value+"&";
    postData=postData+"em="+document.getElementById("frmEm").value;

    rqCFD=newRequest();
    if ( typeof rqCFD != "boolean" ){
        rqCFD.open("POST","/functions/checkforduplicate.ashx?a=0"+noCache(),false);
        rqCFD.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        setCallback(rqCFD,cfdResponse);
        rqCFD.send(postData);
    }


}

var rqCFD;
function cfdResponse(){
    if (rqCFD.readyState==4){
        var rspn=rqCFD.responseText;
        var keys=rspn.split("|");
        var tStatus="";
        var tRowcount="";
        var tMatch="";
        var tGrid="";
        try {tStatus=keys[0]} catch (e) { tStatus="FAIL"}
        try {tRowcount=keys[1]} catch (e) { tRowcount="0"}
        try {tMatch=keys[2]} catch (e) { tMatch="0"}
        try {tGrid=keys[3]} catch (e) { tGrid=""}
        if ( tStatus=="FAIL" ){
            setError(tGrid);
        } else if ( tStatus=="OK" ){
            chgNamedElementText("rspCreateNewUser",chgReplace,"");
            if (tRowcount=="0"){
                createUser();
            } else {
                chgNamedElementText("rspCreateNewUser",chgReplace,"<span class='duperror'>Potential Duplicates Detected</span><br />");
                chgNamedElementText("rspCreateNewUser",chgAfter,tGrid);
                    chgNamedElementText("rspCreateNewUser",chgAfter,"<br />It is against company policy to create duplicates. If you do not believe that you are creating a duplicate, STOP and contact <a href='mailto:support@rsrvr.co.uk?subject=Duplicate'>support@rsrvr.co.uk</a>");
                if (tMatch=="0"){   // Match less than 60% accurate
                    chgNamedElementText("rspCreateNewUser",chgAfter,"<br />&nbsp;&nbsp;<a href='javascript:createUser();'>Continue new user creation.</a>");
                }
            }
        }
    }
}

function cnuResponse(){
    if (rqCFD.readyState==4){
        var rspn=rqCFD.responseText;
        var keys=rspn.split("|");
        var tStatus="";
        var tRowcount="";
        var tGrid="";
        try {tStatus=keys[0]} catch (e) { tStatus="FAIL"}
        try {tRowcount=keys[1]} catch (e) { tRowcount="0"}
        try {tGrid=keys[2]} catch (e) { tGrid=""}
        if ( tStatus=="FAIL" ){
            setError(tGrid);
        } else if ( tStatus=="OK" ){
            chgNamedElementText("rspCreateNewUser",chgReplace,"<a href='persons.aspx?pk="+tRowcount+"&md=ps"+noCache()+"'>New user created. Click here to view.</a>");
        }
    }
}

function displayExtendedForm(){
    var rqOrg=newRequest();
    var rspn=""; 
    if ( typeof rqOrg != "boolean" ){
        rqOrg.open("GET","/functions/frm_createuser.htm",false);
        rqOrg.send();
        rspn=rqOrg.responseText;
        chgNamedElementText("frmCreateNewUser",chgReplace,rspn);
        populateExtendedForm()
    }
}

function populateExtendedForm(){
    document.getElementById("frmFn").value=srchFN.value;
    document.getElementById("frmSn").value=srchSN.value;
    document.getElementById("frmDb").value=srchDB.value;
    document.getElementById("frmPc").value=srchPC.value;
}

function createNewFromSearch(){
    displayExtendedForm();
    populateExtendedForm();
}

function activateGeneralBtn(){
    chgElementText(srchBtn, chgReplace, "Locate");
    srchBtn.disabled = false;
}

function killBtn(){
    //    chgElementText(srchBtn,chgClear,""); 
    //    srchBtn.disabled = true;
    chgElementText(srchBtn, chgReplace, "Locate");
    srchBtn.disabled = false;
}

function checkDate(testString){
    var rv=0;
    try 
        {
        if (testString.length>=10) 
            {
            var d = new Date(testString);    
            var v = d.valueOf();
            if (!isNaN(v)) {rv=1;}
            }
        }
    catch (ex) {}
    return rv;
}

function doSearch(){

//    killBtn()

//    var nameAgreement = srchAgreement.value.length >= 3;
//    var nameTUID = srchTUID.value.length >= 3;
//    var nameFacility = srchFacility.value.length >= 3;
//    var fed = srchFederation.selectedIndex;
//    var fedt = srchFederation.options[fed];

//    var testLevel=0; 
//    /* testLevel
//        0 - Failed
//        1 - Passed
//    */
//    if (nameAgreement | nameTUID | nameFacility) { testLevel = 1; }
//    if (fedt>0) { testLevel = 1; }

//    if (testLevel == 1) { activateGeneralBtn(); }
    activateGeneralBtn();
}
