﻿// JScript File



function loadMembers(group_pk){

    AvailableUsers(group_pk);
    ActiveUsers(group_pk);
    
}//managegroups.ashx



function ActiveUsers(group_pk){
    
    var group = document.getElementById(group_pk);
    var OutputActive = document.getElementById("ActiveUserList");
    var rspn=""; 
    var rqMembers=newRequest();
    

    var keys="b="+group.value+"&c=0";         
    rqMembers.open("GET","/functions/managegroups.ashx?"+keys+noCache(),false);
    rqMembers.send();
    rspn=rqMembers.responseText;  
    chgNamedElementText("ActiveUserList",chgReplace,rspn);
    OutputActive.style.display="";
      
   
    

}


function AvailableUsers(group_pk){

    var group = document.getElementById(group_pk);
    var OutputAvailable = document.getElementById("AvailableUserList");
    var rspn=""; 
    var rqAvailable=newRequest();
    
    var keys="b="+group.value+"&c=1";         
    rqAvailable.open("GET","/functions/managegroups.ashx?"+keys+noCache(),false);
    rqAvailable.send();
    rspn=rqAvailable.responseText;  
    chgNamedElementText("AvailableUserList",chgReplace,rspn);
    OutputAvailable.style.display="";
    
    
}


// usergroup_pk,user_fk,group_fk,username                    



function Add(sourceLI, usergroup_pk,user_pk,group_pk,username){

   // usergroup_pk - will be empty/-1 because we're about to set it
   // user_pk,group_pk,username will all contain values

    var rqMembers=newRequest();
    
    var keys="pk="+group_pk+"&u="+user_pk;   
    rqMembers.open("GET","/functions/grpadd.ashx?pk="+group_pk+"&u="+user_pk+noCache(),false);
    rqMembers.send();
    rspn=rqMembers.responseText;  

    

    if(rspn.substring(0,2) == "OK"){
        usergroup_pk=rpl(rspn,"OK:","");        
    } else {
        alert("Ooopsie!");
        return;
    }

        
    var AvailableUser;
    var ActiveUser;
    var li;
    var a;    
    var img; 
    
    var originalLi = document.getElementById(sourceLI);
    
    AvailableUser= document.getElementById("AvailableUserList");  //current list object is assigned to
    ActiveUser= document.getElementById("ActiveUserList");  //current list object will be assigned to
    
    li = document.createElement("li");
    li.id = "AcUser_"+usergroup_pk;
    
    a = document.createElement("a");
    a.href = 'javascript:Remove("AcUser_'+usergroup_pk+'",'+usergroup_pk+','+user_pk+',' +group_pk+ ',"'+username+'");';
    
    img = document.createElement("img");
    img.src ="/design/images/icon_remove.png";
    img.alt="remove";
    img.className="manageGroup_Icon";

    a.appendChild(img);

    var tn;
    tn = document.createTextNode("   ");
    a.appendChild(tn);
    tn = document.createTextNode(username);
    a.appendChild(tn);
    
    li.appendChild(a);
    ActiveUser.appendChild(li);
    
    AvailableUser.removeChild(originalLi);
                      
}//grpadd.aspx

        
function Remove(sourceLI, usergroup_pk,user_pk,group_pk,username){


    var rqAvailable=newRequest();
         
    var keys="pk="+usergroup_pk;             
    rqAvailable.open("GET","/functions/grprem.aspx?pk="+usergroup_pk+noCache(),false);
    rqAvailable.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    rqAvailable.send();
    rspn=rqAvailable.responseText; 
        if(rspn == "OK"){
        
        
        var AvailableUser;
        var ActiveUser;
        var li;
        var a;    
        var img;
        
        var originalLi = document.getElementById(sourceLI);
        
        ActiveUser= document.getElementById("ActiveUserList");  //current list object is assigned to
        AvailableUser= document.getElementById("AvailableUserList");//current list object will be assigned to
      
        li = document.createElement("li");
        li.id = "AvUser_"+user_pk;
        a = document.createElement("a");
        a.href = 'javascript:Add("AvUser_'+user_pk+'",'+usergroup_pk+','+user_pk+',' +group_pk+ ',"'+username+'");';
                
        img = document.createElement("img");
        img.src ="/design/images/icon_add.png";
        img.alt ="add";
        img.className ="manageGroup_Icon";
        a.appendChild(img);
        
        var tn;
        tn = document.createTextNode("   ");
        a.appendChild(tn);
        tn = document.createTextNode(username);
        a.appendChild(tn);
        
        li.appendChild(a);
        AvailableUser.appendChild(li); 
            
        ActiveUser.removeChild(originalLi);
    
        }
        else{

        alert(rspn.responseText); 
        }
           
}//grprem.aspx