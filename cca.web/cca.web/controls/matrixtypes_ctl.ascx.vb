Partial Class matrixtypes_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omatrixtypes As Objects.matrixtypes
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_countrys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.country_fk.Items.Count=0 Then
        Me.country_fk.DataSource=CDC.ReadDataTable(CO)
        Me.country_fk.DataTextField = "value"
        Me.country_fk.DataValueField = "pk"
        Try
            Me.country_fk.DataBind
        Catch Ex as Exception
            Me.country_fk.SelectedValue=-1
            Me.country_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_fuels_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.fuel_fk.Items.Count=0 Then
        Me.fuel_fk.DataSource=CDC.ReadDataTable(CO)
        Me.fuel_fk.DataTextField = "value"
        Me.fuel_fk.DataValueField = "pk"
        Try
            Me.fuel_fk.DataBind
        Catch Ex as Exception
            Me.fuel_fk.SelectedValue=-1
            Me.fuel_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omatrixtypes is Nothing then
omatrixtypes = new Objects.matrixtypes(currentuser,actualuser,pk,CDC)
End If
Me.typename.Text=omatrixtypes.typename
Me.country_fk.SelectedValue=omatrixtypes.country_fk
Me.fuel_fk.SelectedValue=omatrixtypes.fuel_fk
Me.reporturl.Text=omatrixtypes.reporturl
_CurrentPk=omatrixtypes.matrixtype_pk
Status=omatrixtypes.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omatrixtypes is Nothing then
omatrixtypes = new Objects.matrixtypes(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omatrixtypes.typename=Me.typename.Text
omatrixtypes.country_fk=Me.country_fk.SelectedValue
omatrixtypes.fuel_fk=Me.fuel_fk.SelectedValue
omatrixtypes.reporturl=Me.reporturl.Text
result=omatrixtypes.Save()
if not result then throw omatrixtypes.LastError
_CurrentPk=omatrixtypes.matrixtype_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omatrixtypes.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrixtypes is Nothing then
   omatrixtypes = new Objects.matrixtypes(currentuser,actualuser,pk,CDC)
  End If
  omatrixtypes.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrixtypes is Nothing then
   omatrixtypes = new Objects.matrixtypes(currentuser,actualuser,pk,CDC)
  End If
  omatrixtypes.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrixtypes is Nothing then
   omatrixtypes = new Objects.matrixtypes(currentuser,pk,CDC)
  End If
  omatrixtypes.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.typename.Enabled=IIF(WL>=0,True,False)
  Me.blktypename.Visible=IIF(RL>=0,True,False)
  Me.country_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkcountry_fk.Visible=IIF(RL>=0,True,False)
  Me.fuel_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkfuel_fk.Visible=IIF(RL>=0,True,False)
  Me.reporturl.Enabled=IIF(WL>=0,True,False)
  Me.blkreporturl.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

