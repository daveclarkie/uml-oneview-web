
Partial Class design_masters_external
    Inherits DaveMaster
    Public PSM As MySM
    Public Sub InitMaster() Handles MyBase.InitSpecificMaster
        ' Place any page specific initialisation code here
        PSM = SM
        CheckSecurity()
    End Sub
    Public Sub CompleteMaster() Handles MyBase.CompleteSpecificMaster
        ' Place any page specific initialisation code here
    End Sub

    Private Sub CheckSecurity()

        Dim actlog As New activitylogs(SM.currentuser, SM.actualuser, CDC)
        actlog.ipaddress = Request.UserHostAddress
        actlog.useragent = Request.UserAgent
        actlog.user_fk = SM.currentuser
        actlog.Save()
        actlog = Nothing

    End Sub

End Class

