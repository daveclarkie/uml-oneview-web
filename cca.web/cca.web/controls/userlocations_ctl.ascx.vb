Partial Class userlocations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ouserlocations As Objects.userlocations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.location_fk.Items.Count=0 Then
        Me.location_fk.DataSource=CDC.ReadDataTable(CO)
        Me.location_fk.DataTextField = "value"
        Me.location_fk.DataValueField = "pk"
        Try
            Me.location_fk.DataBind
        Catch Ex as Exception
            Me.location_fk.SelectedValue=-1
            Me.location_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locationuses_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.locationuse_fk.Items.Count=0 Then
        Me.locationuse_fk.DataSource=CDC.ReadDataTable(CO)
        Me.locationuse_fk.DataTextField = "value"
        Me.locationuse_fk.DataValueField = "pk"
        Try
            Me.locationuse_fk.DataBind
        Catch Ex as Exception
            Me.locationuse_fk.SelectedValue=-1
            Me.locationuse_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ouserlocations is Nothing then
ouserlocations = new Objects.userlocations(currentuser,actualuser,pk,CDC)
End If
Me.user_fk.SelectedValue=ouserlocations.user_fk
Me.location_fk.SelectedValue=ouserlocations.location_fk
Me.locationuse_fk.SelectedValue=ouserlocations.locationuse_fk
_CurrentPk=ouserlocations.userlocation_pk
Status=ouserlocations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ouserlocations is Nothing then
ouserlocations = new Objects.userlocations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ouserlocations.user_fk=Me.user_fk.SelectedValue
ouserlocations.location_fk=Me.location_fk.SelectedValue
ouserlocations.locationuse_fk=Me.locationuse_fk.SelectedValue
result=ouserlocations.Save()
if not result then throw ouserlocations.LastError
_CurrentPk=ouserlocations.userlocation_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ouserlocations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserlocations is Nothing then
   ouserlocations = new Objects.userlocations(currentuser,actualuser,pk,CDC)
  End If
  ouserlocations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserlocations is Nothing then
   ouserlocations = new Objects.userlocations(currentuser,actualuser,pk,CDC)
  End If
  ouserlocations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ouserlocations is Nothing then
   ouserlocations = new Objects.userlocations(currentuser,pk,CDC)
  End If
  ouserlocations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  Me.location_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocation_fk.Visible=IIF(RL>=0,True,False)
  Me.locationuse_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocationuse_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

