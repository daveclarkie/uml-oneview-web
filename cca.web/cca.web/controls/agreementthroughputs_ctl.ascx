<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreementthroughputs_ctl.ascx.vb" Inherits="agreementthroughputs_ctl" %>
<h5>agreementthroughputs</h5>
<ul class='formcontrol'>
<li runat="server" id="blkagreement_fk">
<span class='label'>Agreement</span>
<asp:DropDownList ID="agreement_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkthroughput_fk">
<span class='label'>Throughput</span>
<asp:DropDownList ID="throughput_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkdatafrequency_fk">
<span class='label'>Data Frequency<span class='pop'>As a guide, how often should the data be received</span></span>
<asp:DropDownList ID="datafrequency_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

