﻿// JScript File

var tmrPres=setTimeout(checkPresence,3000);
var tmrPresRQ;
var tmrDisplay;

function checkPresence(){

    clearTimeout(tmrPres);
    hideNotice(); 
    tmrPresRQ=newRequest();
    //if ( typeof tmrPresRQ != "boolean" ){
    //    tmrPresRQ.open("GET","/functions/presence.ashx?a=0"+noCache(),true);
    //    setCallback(tmrPresRQ,checkPresenceResponse);
    //    tmrPresRQ.send();
    //}
}

function checkPresenceResponse(){
    if (tmrPresRQ.readyState==4){
        var rspn=tmrPresRQ.responseText;
        var keys=rspn.split("|");
        var tStatus="";
        var tAction=0;
        var tDelay=15000;
        var tMsg="";
        var tFn="";

        /* 

            '            Message format
            '            0status : OK/FAIL
            '            1action : 0 (No action) / 1 (General message) / 2 (jump to client) / 3 (shutdown / log out)
            '            2next check due in ? seconds
            '            3message
            '            4function

        */

        try {tStatus=keys[0]} catch (e) { tStatus="OTHER"}
        try {tAction=keys[1]} catch (e) { tAction="0"}
        try {tDelay=keys[2]} catch (e) { tDelay="15"}
        try {tMsg=keys[3]} catch (e) { tMsg=""}
        try {tFn=keys[4]} catch (e) { tFn=""}

        if ( tStatus=="OK" ){
            if ( tAction != 0 ){
                displayNotice(tMsg);
                if ( tFn != "" ) { eval(tFn); }
            }
        } else if ( tStatus=="FAIL" ){
            displayNotice("Something is going pop:-<br />"+tMsg);
            if ( tFn != "" ) { eval(tFn); }
        } else if ( tStatus=="DEAD" ){
            displayNotice(tMsg);
        } else {
            if ( tmrPresRQ.status!=200 ) {
                // can't check for presence - so you're out of here
               displayNotice("Ooops! Something has broken. Take a screenshot (press the  'PrintScreen' button) and let IT know!:\n" + rspn);
               // logOut()
            } else {
                alert(rspn);
            }
        }
        tmrPres=setTimeout(checkPresence,tDelay*1000);
    }
}

var noticeHeader="<div class='tmr_titlebar'>System Messages</div><div class='tmr_panel'><span class='tmr_notice'>";
var noticeFooter="</span></div>";

function displayNotice(notice){
    if ( !tmrDisplay ) { tmrDisplay=document.getElementById("tmrDisplay"); }
    tmrDisplay.innerHTML=noticeHeader+notice+noticeFooter;
    fadeIn(tmrDisplay);
}

function hideNotice(){
    if ( !tmrDisplay ) { tmrDisplay=document.getElementById("tmrDisplay"); }
    tmrDisplay.innerHTML="";
    fadeOut(tmrDisplay);
}
var setPresRQ;
function setPresence(){
    hideNotice(); 
    setPresRQ=newRequest();
    //if ( typeof setPresRQ != "boolean" ){
    //    setPresRQ.open("GET","/functions/setpresence.ashx?a=0"+noCache(),false);
    //    setCallback(setPresRQ,checkSetPresenceResponse);
    //    setPresRQ.send();
    //}
}

function checkSetPresenceResponse(){
    if (setPresRQ.readyState==4){
        var rspn=setPresRQ.responseText;
        var keys=rspn.split("|");
        var tStatus="";
        var tMsg="";
        try {tStatus=keys[0]} catch (e) { tStatus="OTHER"}
        try {tMsg=keys[1]} catch (e) { tMsg="Ooopsie!"}
        if ( tStatus=="FAIL" ){
            setError(tMsg);
        } else if ( tStatus=="OTHER" ){
            alert(rspn);
        }
    }
}

var ackMsgRQ;
function ackMessage(id){
    hideNotice(); 
    ackMsgRQ=newRequest();
    if ( typeof ackMsgRQ != "boolean" ){
        ackMsgRQ.open("GET","/functions/ackmessage.ashx?pk="+id+noCache(),false);
        setCallback(ackMsgRQ,ackMessageResponse);
        ackMsgRQ.send();
    }
}

function ackMessageResponse(){
    if (ackMsgRQ.readyState==4){
        var rspn=ackMsgRQ.responseText;
        var keys=rspn.split("|");
        var tStatus="";
        var tMsg="";
        try {tStatus=keys[0]} catch (e) { tStatus="OTHER"}
        try {tMsg=keys[1]} catch (e) { tMsg="Ooopsie!"}
        if ( tStatus=="FAIL" ){
            setError(tMsg);
        } else if ( tStatus=="OTHER" ){
            alert(rspn);
        }
    }
}

function jumpTo(user){
    window.location="/core/persons.aspx?md=ps&pk="+user+noCache();
}

function logOut(){
    window.location="/logout.aspx";
}


var tmrFade;
var opFade;
var chgFade;
var divFade;

function fadeIn(obj){
    divFade=obj;
    opFade=0;
    chgFade=0.1;
    tmrFade=setTimeout(setOpacity,25); 
}

function fadeOut(obj){
    divFade=obj;
    if (divFade.style.display!="none"){
        opFade=1;
        chgFade=-0.1;
        tmrFade=setTimeout(setOpacity,25); 
    } 
}

function setOpacity(){
    clearTimeout(tmrFade);
    var cont=0; 
    var ieVal;
    
    if ( opFade>=1 ){opFade=1;}
    if ( opFade<=0 ){opFade=0;}

    ieVal=opFade*100;

    try { divFade.style.filter="alpha(opacity="+ieVal+")"; } catch (eek1) {}
    try { divFade.style.opacity=opFade; } catch (eek2) {}

    if ( opFade<1 && chgFade>0 ){cont=1;}
    if ( opFade>0 && chgFade<0 ){cont=1;}
   
    if (opFade<=0){divFade.style.display="none";} else {divFade.style.display="block";}
    
    if ( cont==1 ){
        opFade=opFade+chgFade;
        tmrFade=setTimeout(setOpacity,25);
    }
}

