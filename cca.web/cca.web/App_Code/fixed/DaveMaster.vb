Public Class DaveMaster
    Inherits System.Web.UI.MasterPage

    Public CDC As DataCommon
    Public SM As MySM
    Public Event InitSpecificMaster()
    Public Event CompleteSpecificMaster()

    Protected Sub AddNotice(ByVal Notice As String)
        CType(Me.Page, DavePage).AddNotice(Notice)
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        CType(Me.Page, DavePage).ShareCDCSM(CDC, SM)
        RaiseEvent InitSpecificMaster()
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        RaiseEvent CompleteSpecificMaster()
    End Sub
End Class
