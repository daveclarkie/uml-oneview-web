<%@ WebHandler Language="VB" Class="checkstatus" %>
Public Class checkstatus
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim CRQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim Obj As users
        Dim Msg As String = "OK"
        Try
            Obj = New users(SM.currentuser, SM.actualuser, Integer.Parse(CRQ("pk")), CDC)
            Dim Manager As String = ""
            Dim p As New persons(SM.currentuser, SM.actualuser, CDC)
            p.user_fk = Obj.creator_fk
            For Each pDr As DataRow In p.UserRecords.Rows
                
                Manager = pDr("forename") & " " & pDr("surname")
                If Manager.Length > 0 Then Exit For
            Next
            
            
            
            Msg = "OK:" & Obj.rowstatus.ToString & ":" & Obj.readlevel & ":" & Obj.writelevel & ":" & Manager
            
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
    End Sub
 
End Class

