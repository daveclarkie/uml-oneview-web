<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="customers.aspx.vb" Inherits="coredemand_customers" title="bubbles" %>
<%@ Register Src="~/controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>
<%@ Register Src="~/controls/head/informations.ascx" TagName="informations" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        
        <%=IIf(CustomerStatus() = 1, "<h3>This is an Active Customer</h3> ", "<h6 style='background-color:red;'>This is a Prospect</h6>")%>
        <div id="left" style="vertical-align:middle;">
            <asp:Label ID="lbldemand_organisationname" runat="server" Text="" Font-Size="15pt"></asp:Label>
            <div id="right">
                <ul id="notifications" >
                    <li><uc1:informations ID="Informations1" Label="Supply Points" InformationItemType="organisation_supplypoints" runat="server" EnableViewState="false" /></li>
                    <li><uc1:informations ID="Informations2" Label="Active Contracts" InformationItemType="organisation_contracts" runat="server" EnableViewState="false" /></li>
                    <li><uc1:informations ID="Informations3" Label="CCA Agreements" InformationItemType="organisation_ccaagreements" runat="server" EnableViewState="false" /></li>
                    <li><uc1:informations ID="Informations4" Label="Appts" InformationItemType="organisation_appointments" runat="server" EnableViewState="false" /></li>
                </ul>
            </div>
        </div>
        
        <br />
        <br />
        <br />
        
        <ul id="innertab">
            <li class='<%=LIi("search")%>'><a href="search.aspx?<%=Local.noCache %>">New Search</a></li>
            <li class='<%=LIi("customer")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=cu<%=Local.noCache %>">Organisation</a></li>
            <li class='<%=LIi("site")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=si<%=Local.noCache %>">Supply List</a></li>
            <li class='<%=LIi("contract")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=ct<%=Local.noCache %>">Contracts</a></li>
            <li class='<%=LIi("cca")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=cca<%=Local.noCache %>">CCA Agreements</a></li>
            <li class='<%=LIi("appointments")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=ap<%=Local.noCache %>">Appointments</a></li>
            <li class='<%=LIi("notes")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=nt<%=Local.noCache %>">Notes</a></li>
            <%--<li class='<%=LIi("serviceagreements")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=sa<%=Local.noCache %>">Service Agreements</a></li>--%>
        </ul>

        <asp:MultiView runat="server" ID="mvDetail">
            <asp:View ID="vwCustomer" runat="server">
                <asp:PlaceHolder ID="OrganisationsControl" runat="server" />
                <table id="OrganisationsButton" runat="server"></table>
                <br />
                
                
            </asp:View>
            <asp:View ID="vwSiteInformation" runat="server">
                
                <h5>Supply List</h5>
                <asp:Repeater ID="rptSites" runat="server">
                    <HeaderTemplate>
                        <table style="width:9000px;">
                            <tr>
                                <%--<td style="width:50px;">
                        
                                </td>--%>
                                <td style="width:80px;">
                                    <h4>Supply ID</h4>
                                </td>
                                <td style="width:300px;">
                                    <h4>Supply Name</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Post Code</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Commodity</h4>
                                </td>
                                <td style="width:300px;">
                                    <h4>Meter</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                            <tr>
                            <%--<td style="text-indent:5px;">
                                <a href='customers.aspx?pg=si&pk=<%# sm.mainid %>&spk=<%# Container.DataItem("site_pk") %>'><h6>select</h6></a>
                            </td>--%>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("site_pk")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("sitename")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("pcode")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("commodity")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("meter")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
            </asp:View>
            <asp:View ID="vwMeterInformation" runat="server">
            
                <asp:PlaceHolder ID="MeterInformationControl" runat="server" />
                <table id="MeterInformationButton" runat="server">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveMeterInformation" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton ID="btnCancelMeterInformation" runat="server" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="vwServiceAgreements" runat="server">
            
            </asp:View>
            <asp:View ID="vwContractInformation" runat="server">
                <h5>Current Contracts</h5>
                <asp:Repeater ID="rptActiveContracts" runat="server">
                    <HeaderTemplate>
                        <table style="width:800px;">
                            <tr>
                                <td style="width:100px;">
                                    <h4>Contract ID</h4>
                                </td>
                                <td style="width:50px;">
                                    <h4>Product</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:200px;">
                                    <h4>Supplier</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Start Date</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>End Date</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                        <tr style='<%# returnselect(Container.DataItem("activestatus"))%>'>

                            <td style="text-indent:5px;">
                                <%# Container.DataItem("contractid")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("product")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("purchasetype")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("suppliername")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("start")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("end")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
                <br />
                <h5>Future Contracts</h5>
                <asp:Repeater ID="rptFutureContracts" runat="server">
                    <HeaderTemplate>
                        <table style="width:800px;">
                            <tr>
                                <td style="width:100px;">
                                    <h4>Contract ID</h4>
                                </td>
                                <td style="width:50px;">
                                    <h4>Product</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:200px;">
                                    <h4>Supplier</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Start Date</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>End Date</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                        <tr style='<%# returnselect(Container.DataItem("activestatus"))%>'>

                            <td style="text-indent:5px;">
                                <%# Container.DataItem("contractid")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("product")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("purchasetype")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("suppliername")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("start")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("end")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
                <br />
                <h5>Historic Contracts</h5>
                <asp:Repeater ID="rptHistoricContracts" runat="server">
                    <HeaderTemplate>
                        <table style="width:800px;">
                            <tr>
                                <td style="width:100px;">
                                    <h4>Contract ID</h4>
                                </td>
                                <td style="width:50px;">
                                    <h4>Product</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:200px;">
                                    <h4>Supplier</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Start Date</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>End Date</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                        <tr style='<%# returnselect(Container.DataItem("activestatus"))%>'>

                            <td style="text-indent:5px;">
                                <%# Container.DataItem("contractid")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("product")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("purchasetype")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("suppliername")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("start")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("end")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
            </asp:View>

            <asp:View ID="vwCCAAgreements" runat="server">
                
                <asp:Repeater ID="rptCCAAgreements" runat="server">
                    <HeaderTemplate>
                        <table style="width:800px;">
                            <tr>
                                <td style="width:50px;">
                                    <h4>ID</h4>
                                </td>
                                <td style="width:70px;">
                                    <h4>Federation</h4>
                                </td>
                                <td style="width:250px;">
                                    <h4>Name</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Measure</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Facility</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                        <tr>

                            <td style="text-indent:5px;">
                                <a href="../corecca/agreements.aspx?pk=<%# Container.DataItem("agreement_pk")%>&pg=ag<%=Local.noCache %>" target="_blank"><%# Container.DataItem("agreement_pk")%> </a>
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("federationnameshort")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("agreementname")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("agreementtypename")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("agreementmeasurename")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("facilitynumber")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
            </asp:View>
            
            <asp:View ID="vwAppointments" runat="server">
                <table style="text-align:left;">
                    <tr>
                        <td style="width:160px:">
                            <a href='customers.aspx?pg=ap&pk=<%= PSM.mainid %>&apt=1&apk=-1'><img src="../design/images/newsitevisit.png" /></a> 
                        </td>
                        <td style="width:160px:">
                            <a href='customers.aspx?pg=ap&pk=<%= PSM.mainid %>&apt=2&apk=-1'><img src="../design/images/newphonecall.png" /></a> 
                        </td>
                        <td style="width:160px:">
                            <img src="../design/images/newphonecall.png" style="visibility:hidden;" />
                        </td>
                        <td style="width:160px:">
                            <img src="../design/images/newphonecall.png" style="visibility:hidden;" />
                        </td>
                    </tr>
                </table>
                <asp:PlaceHolder ID="AppointmentControl" runat="server" />    
                <table id="AppointmentButton" runat="server" visible="false">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveAppointment" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnCancelAppointment" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
                <h5>Scheduled Appointments</h5>
                <asp:Repeater ID="rptScheduledAppointments" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td style="width:40px;">
                                
                                </td>
                                <td style="width:100px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:150px;">
                                    <h4>When</h4>
                                </td>
                                <td style="width:150px;">
                                    <h4>Purpose</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Who</h4>
                                </td>
                                <td></td>
                            </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("appointment_pk") %>'>
                                <td style="text-align:center;">
                                    <h6>view</h6>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmenttypename")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmentwhen")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmentpurposename")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("creatorname")%>
                                </td>
                                <td>
                                    <%--<a class="remove" title="Remove"  href='javascript:removeAppointment(<%# Container.DataItem("appointment_pk") %>)'>X</a> --%>                               
                                </td>
                            </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
                <br />
                <h5>Archived Appointments</h5>
                <asp:Repeater ID="rptArchivedAppointments" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td style="width:40px;">
                                
                                </td>
                                <td style="width:100px;">
                                    <h4>Type</h4>
                                </td>
                                <td style="width:150px;">
                                    <h4>When</h4>
                                </td>
                                <td style="width:150px;">
                                    <h4>Purpose</h4>
                                </td>
                                <td style="width:100px;">
                                    <h4>Who</h4>
                                </td>
                                <td></td>
                            </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("appointment_pk") %>'>
                                <td style="text-align:center;">
                                    <h6>view</h6>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmenttypename")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmentwhen")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("appointmentpurposename")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("creatorname")%>
                                </td>
                                <td>
                                    <%--<a class="remove" title="Remove"  href='javascript:removeAppointment(<%# Container.DataItem("appointment_pk") %>)'>X</a> --%>                               
                                </td>
                            </tr>
                    </ItemTemplate>  

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
                <br />
            </asp:View>
            
            <asp:View ID="vwNotes" runat="server">
                    <table style="width:100%; text-align:center;">
                        <tr>
                            <td>
                                <a href='customers.aspx?pg=nt&pk=<%= PSM.mainid %>&type=1&npk=-1'><h6>Add Organisation Note</h6></a> 
                            </td>
                            <td>
                                <a href='customers.aspx?pg=nt&pk=<%= PSM.mainid %>&type=2&npk=-1'><h6>Add Supply Note</h6></a> 
                            </td>
                            <%--<td>
                                <a href='customers.aspx?pg=nt&pk=<%= PSM.mainid %>&type=3&npk=-1'><h6>Add Meter Note</h6></a> 
                            </td>--%>
                        </tr>
                    </table>
                    <asp:PlaceHolder ID="NotesControl" runat="server" />            
                    <table id="NotesButton" runat="server">
                        <tr style="height:10px;">
                            
                        </tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveNotes" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelNotes" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="height:5px;">
                               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>Notes related to the Organisation</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptNotes" runat="server">
                                    <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width:5px;">
                                
                                            </td>
                                            <td style="transform: rotate(270deg);">
                                                <h4>Note Type</h4>
                                            </td>
                                            <td>
                                                <h4>Status</h4>
                                            </td>
                                            <td>
                                                <h4>Date</h4>
                                            </td>
                                            <td>
                                                <h4>Note</h4>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </HeaderTemplate> 
                                    <ItemTemplate>
                                        <tr id='node_<%# Container.DataItem("note_pk") %>'>
                                            <td style="text-align:center;">
                                            </td>
                                            <td class="verticalcell">
                                                <%# Container.DataItem("type")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("status")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("modified")%>
                                            </td>
                                            <td>
                                                <%# Container.DataItem("note")%>
                                            </td>
                                            <td>
                                                <a class="remove" title="Remove"  href='javascript:removeNote(<%# Container.DataItem("note_pk") %>)'>X</a>                                
                                            </td>
                                        </tr>
                                    </ItemTemplate> 
                                    <FooterTemplate>
                                        <tr style="height:5px;">
                                        </tr>
                                    </table>
                                    </FooterTemplate> 
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:View>
        </asp:MultiView>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="CustomerNav" runat="server" />

    </div>

</div>

</asp:Content>

