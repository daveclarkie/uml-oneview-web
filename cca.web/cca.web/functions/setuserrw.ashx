<%@ WebHandler Language="VB" Class="functions_setuserrwlevel" %>
Public Class functions_setuserrwlevel
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Obj As users
        Dim Msg As String = "OK"
        Try
            Obj = New users(SM.currentuser, SM.actualuser, Integer.Parse(Request.QueryString("pk")), CDC)

            Obj.readlevel = Integer.Parse(Request.QueryString("r"))
            Obj.writelevel = Integer.Parse(Request.QueryString("w"))
            If Not Obj.Save() Then
                Try
                    Msg = "FAIL:" & Obj.LastError.Message
                Catch ex As Exception
                    Msg = "FAIL:Does the user exist?"
                End Try
            End If
            
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
    End Sub
 

End Class

