Partial Class userhelpdesktemplate_combo_ctl
   Inherits DaveControl
   Public PSM As MySM

   Public WithEvents ouserhelpdesktemplate As Objects.userhelpdesktemplates
   Public WithEvents ohelpdesktemplate As Objects.helpdesktemplates
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=-1
   Dim _Status as Integer=0
   Dim _STCurrentPk as Integer=-1
   Dim _STStatus as Integer=0

   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       ControlInit
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub

   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property

   Public ReadOnly Property STCurrentPk() as Integer
        Get
            Return _STCurrentPk
        End Get
   End Property

   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property

  Public Sub ControlInit()
CO=new SqlClient.SqlCommand("rkg_helpdesktemplategroups_lookup")
CO.CommandType = CommandType.StoredProcedure
Me.helpdesktemplategroup_fk.DataSource=CDC.ReadDataTable(CO)
Me.helpdesktemplategroup_fk.DataTextField = "value"
Me.helpdesktemplategroup_fk.DataValueField = "pk"
Try
Me.helpdesktemplategroup_fk.DataBind
Catch Ex as Exception
Me.helpdesktemplategroup_fk.SelectedValue=-1
Me.helpdesktemplategroup_fk.DataBind
End Try
  End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
End Sub
Public Sub SetObjects(ByVal CurrentUser as Integer,ByVal ActualUser as Integer,Optional ByVal pk as Integer=-1)
    If ouserhelpdesktemplate is Nothing then
        ouserhelpdesktemplate = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
        If ouserhelpdesktemplate.helpdesktemplate_fk>0 then
            ohelpdesktemplate = new Objects.helpdesktemplates(currentuser,actualuser,ouserhelpdesktemplate.helpdesktemplate_fk,CDC)
        Else
            ohelpdesktemplate = new Objects.helpdesktemplates(currentuser,actualuser,-1,CDC)
        End If
    End If
    If ohelpdesktemplate is Nothing then
        If ouserhelpdesktemplate.helpdesktemplate_fk>0 then
            ohelpdesktemplate = new Objects.helpdesktemplates(currentuser,actualuser,ouserhelpdesktemplate.helpdesktemplate_fk,CDC)
        Else
            ohelpdesktemplate = new Objects.helpdesktemplates(currentuser,actualuser,-1,CDC)
        End If
    End If
End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1)
    SetObjects(currentuser,actualuser,pk)
SM.subId=ouserhelpdesktemplate.helpdesktemplate_fk
Me.templatename.Text=ohelpdesktemplate.templatename
Try
Me.helpdesktemplategroup_fk.SelectedValue=ohelpdesktemplate.helpdesktemplategroup_fk
Catch exhelpdesktemplategroup_fk as Exception
Me.helpdesktemplategroup_fk.SelectedValue=-1
End Try
Me.templatedisplayname.Text=ohelpdesktemplate.templatedisplayname
_CurrentPk=ouserhelpdesktemplate.userhelpdesktemplate_pk
_STCurrentPk=ohelpdesktemplate.helpdesktemplate_pk
Status=ouserhelpdesktemplate.rowstatus OR ohelpdesktemplate.rowstatus
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=-1) as boolean
    SetObjects(currentuser,actualuser,pk)
Dim STResult as boolean=False
Try
STResult=ohelpdesktemplate.Save()
if not STResult then throw ohelpdesktemplate.LastError
_STCurrentPk=ohelpdesktemplate.helpdesktemplate_pk
SM.subId=ohelpdesktemplate.helpdesktemplate_pk
Catch Ex as Exception
STResult=False
LastError=Ex
SM.subId=-1
End Try
Try
ouserhelpdesktemplate.user_fk=SM.targetuser
ouserhelpdesktemplate.helpdesktemplate_fk=SM.subId
ohelpdesktemplate.templatename=Me.templatename.Text
ohelpdesktemplate.helpdesktemplategroup_fk=Me.helpdesktemplategroup_fk.SelectedValue
ohelpdesktemplate.templatedisplayname=Me.templatedisplayname.Text
Catch MainEx as Exception
LastError=MainEx
End Try
Dim result as boolean=false
Try
Result=ouserhelpdesktemplate.Save()
if not Result then throw ouserhelpdesktemplate.LastError
_CurrentPk=ouserhelpdesktemplate.userhelpdesktemplate_pk
Catch Ex as Exception
Result=False
LastError=Ex
End Try
SM.linkId=_CurrentPk
Status=ouserhelpdesktemplate.rowstatus OR ohelpdesktemplate.rowstatus
Return result AND STResult
End Function
Public Function Enable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserhelpdesktemplate is Nothing then
ouserhelpdesktemplate = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
End If
ouserhelpdesktemplate.rowstatus=0
Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserhelpdesktemplate is Nothing then
ouserhelpdesktemplate = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
End If
ouserhelpdesktemplate.rowstatus=1
Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer, byval actualuser as integer,optional byval pk as integer=-1) as boolean
If ouserhelpdesktemplate is Nothing then
ouserhelpdesktemplate = new Objects.userhelpdesktemplates(currentuser,actualuser,pk,CDC)
End If
ouserhelpdesktemplate.rowstatus=2
Return Save(currentuser,actualuser,pk)
End Function
Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
Me.templatename.Enabled=IIF(WL>=1,True,False)
Me.blktemplatename.Visible=IIF(RL>=1,True,False)
Me.helpdesktemplategroup_fk.Enabled=IIF(WL>=0,True,False)
Me.blkhelpdesktemplategroup_fk.Visible=IIF(RL>=0,True,False)
Me.templatedisplayname.Enabled=IIF(WL>=1,True,False)
Me.blktemplatedisplayname.Visible=IIF(RL>=1,True,False)
End Sub
End Class
