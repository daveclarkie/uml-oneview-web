﻿
Partial Class corehelpdesk_sourcing_1_tender_request_default
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Dim cc As Object


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        SetControl("~/controls/helpdeskitem2details_ctl.ascx", detailcontrol)
    End Sub

    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM

        cc.Read(SM.currentuser, SM.actualuser, -1)

        TargetControl.Controls.Add(cc)

    End Sub


End Class
