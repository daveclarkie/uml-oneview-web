<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="persons.aspx.vb" Inherits="core_persons" title="agreements" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        <h3>Persons</h3> 

            <ul id="innertab">
                <li class='<%=LIi("person")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=ps<%=Local.noCache%>">Person</a></li>
                <li class='<%=LIi("location")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=lc<%=Local.noCache%>">Location</a></li>
                <li class='<%=LIi("phone")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=ph<%=Local.noCache%>">Phone</a></li>
                <li class='<%=LIi("email")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=em<%=Local.noCache%>">Email</a></li>
                <li class='<%=LIi("helpdesk")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=hd<%=Local.noCache%>">Helpdesk</a></li>
                <li class='<%=LIi("hierarchy")%>'><a href="persons.aspx?pk=<%=PSM.targetuser %>&pg=hy<%=Local.noCache%>">Hierarchy</a></li>
            </ul>

            <asp:MultiView runat="server" ID="mvPerson">
                <asp:View ID="vwPerson" runat="server">
                    <asp:PlaceHolder ID="personControl" runat="server" />
                    <table id="personButton" runat="server">
                        <tr style="height:10px;"></tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSavePerson" Text="<h6>Save Person</h6>" CausesValidation="true" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;"></td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwUserLocation" runat="server">
                    <h5>Current Locations</h5>
                    <asp:Repeater ID="rptUserLocation" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td style="width:50px;"></td>
                                    <td style="width:10px;"></td>
                                    <td style="width:250px;"><h4>Address</h4></td>
                                    <td style="width:50px;"><h4>Usage</h4></td>
                                    <td style="width:50px;"></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("userlocation_pk") %>'>
                                <td style="text-align:center;">
                                    <a href='persons.aspx?pg=lc&pk=<%= sm.targetuser %>&spk=<%# Container.DataItem("userlocation_pk") %>'><h6>Edit</h6></a> 
                                </td>
                                <td></td>
                                <td>
                                    <%# Container.DataItem("linktext")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("subtext")%>
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeLocation(<%# Container.DataItem("userlocation_pk") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                        </table>
                        <table style="width:100px;">
                            <tr style="height:5px;">
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                     <a href='persons.aspx?pg=lc&pk=<%= sm.targetuser %>&spk=-1'><h6>New Location</h6></a> 
                                </td>
                            </tr>
                        </table>
                        </FooterTemplate>                        
                    </asp:Repeater>
                    
                    <asp:PlaceHolder ID="UserLocationControl" runat="server" />            
                    <table id="UserLocationButton" runat="server">
                        <tr style="height:10px;">
                            
                        </tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveUserLocation" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelUserLocation" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwUserPhone" runat="server">
                    <h5>Current Phones</h5>
                    <asp:Repeater ID="rptUserPhone" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td style="width:50px;"></td>
                                    <td style="width:10px;"></td>
                                    <td style="width:200px;"><h4>Phone Number</h4></td>
                                    <td style="width:100px;"><h4>Location</h4></td>
                                    <td style="width:50px;"></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("userphone_pk") %>'>
                                <td style="text-align:center;">
                                    <a href='persons.aspx?pg=ph&pk=<%= sm.targetuser %>&spk=<%# Container.DataItem("userphone_pk") %>'><h6>Edit</h6></a> 
                                </td>
                                <td></td>
                                <td>
                                    <%# Container.DataItem("lastext")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("subtext")%>
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeLocation(<%# Container.DataItem("userphone_pk") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                        </table>
                        <table style="width:100px;">
                            <tr style="height:5px;">
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                     <a href='persons.aspx?pg=ph&pk=<%= sm.targetuser %>&spk=-1'><h6>New Phone</h6></a> 
                                </td>
                            </tr>
                        </table>
                        </FooterTemplate>
                        
                    </asp:Repeater>
                    
                    <asp:PlaceHolder ID="UserPhoneControl" runat="server" />            
                    <table id="UserPhoneButton" runat="server">
                        <tr style="height:10px;">
                            
                        </tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveUserPhone" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelUserPhone" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwUserEmail" runat="server">
                    <h5>Current Emails</h5>
                    <asp:Repeater ID="rptUserEmail" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td style="width:50px;"></td>
                                    <td style="width:10px;"></td>
                                    <td style="width:200px;"><h4>Email</h4></td>
                                    <td style="width:100px;"><h4>Location</h4></td>
                                    <td style="width:50px;"></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <tr id='node_<%# Container.DataItem("useremail_pk") %>'>
                                <td style="text-align:center;">
                                    <a href='persons.aspx?pg=em&pk=<%= sm.targetuser %>&spk=<%# Container.DataItem("useremail_pk") %>'><h6>Edit</h6></a> 
                                </td>
                                <td></td>
                                <td>
                                    <%# Container.DataItem("linktext")%>
                                </td>
                                <td>
                                    <%# Container.DataItem("subtext")%>
                                </td>
                                <td>
                                    <a class="remove" title="Remove"  href='javascript:removeEmail(<%# Container.DataItem("useremail_pk") %>)'>X</a>                                
                                </td>
                            </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                        </table>
                        <table style="width:100px;">
                            <tr style="height:5px;">
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                    <a href='persons.aspx?pg=em&pk=<%= sm.targetuser %>&spk=-1'><h6>New Email</h6></a> 
                                </td>
                            </tr>
                        </table>
                        </FooterTemplate> 
                        
                    </asp:Repeater>
                    
                    <asp:PlaceHolder ID="UserEmailControl" runat="server" />            
                    <table id="UserEmailButton" runat="server">
                        <tr style="height:10px;">
                            
                        </tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveUserEmail" Text="<h6>Save</h6>" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton ID="btnCancelUserEmail" runat="server" Text="<h4>Cancel</h4>" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
                
                <asp:View ID="vwHelpdesk" runat="server">
                        <asp:PlaceHolder ID="helpdeskControl" runat="server" />
                    <table id="helpdeskButton" runat="server">
                        <tr style="height:10px;"></tr>
                        <tr>
                            <td style="text-align:center; width:20%;">
                                <asp:Linkbutton runat="server" ID="btnSaveHelpdesk" Text="<h6>Save Details</h6>" CausesValidation="true" />
                            </td>
                            <td style="width:60%;"></td>
                            <td style="text-align:center; width:20%;"></td>
                        </tr>
                    </table>
                </asp:View>

                <asp:View ID="vwHierarchy" runat="server">
                        <asp:TreeView runat="server" ID="hierarchyTree"
  PopulateNodesFromClient="true"
  ShowLines="true" 
  ShowExpandCollapse="true"  ></asp:TreeView>
                    
                  



                </asp:View>
            </asp:MultiView>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="Rightnav1" runat="server" />

    </div>
</div>
</asp:Content>

