Partial Class sitevisitpersons_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ositevisitpersons As Objects.sitevisitpersons
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_sitevisits_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.sitevisit_fk.Items.Count=0 Then
        Me.sitevisit_fk.DataSource=CDC.ReadDataTable(CO)
        Me.sitevisit_fk.DataTextField = "value"
        Me.sitevisit_fk.DataValueField = "pk"
        Try
            Me.sitevisit_fk.DataBind
        Catch Ex as Exception
            Me.sitevisit_fk.SelectedValue=-1
            Me.sitevisit_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_persons_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.person_fk.Items.Count=0 Then
        Me.person_fk.DataSource=CDC.ReadDataTable(CO)
        Me.person_fk.DataTextField = "value"
        Me.person_fk.DataValueField = "pk"
        Try
            Me.person_fk.DataBind
        Catch Ex as Exception
            Me.person_fk.SelectedValue=-1
            Me.person_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ositevisitpersons is Nothing then
ositevisitpersons = new Objects.sitevisitpersons(currentuser,actualuser,pk,CDC)
End If
Me.sitevisit_fk.SelectedValue=ositevisitpersons.sitevisit_fk
Me.person_fk.SelectedValue=ositevisitpersons.person_fk
_CurrentPk=ositevisitpersons.sitevisitperson_pk
Status=ositevisitpersons.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ositevisitpersons is Nothing then
ositevisitpersons = new Objects.sitevisitpersons(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ositevisitpersons.sitevisit_fk=Me.sitevisit_fk.SelectedValue
ositevisitpersons.person_fk=Me.person_fk.SelectedValue
result=ositevisitpersons.Save()
if not result then throw ositevisitpersons.LastError
_CurrentPk=ositevisitpersons.sitevisitperson_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ositevisitpersons.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ositevisitpersons is Nothing then
   ositevisitpersons = new Objects.sitevisitpersons(currentuser,actualuser,pk,CDC)
  End If
  ositevisitpersons.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ositevisitpersons is Nothing then
   ositevisitpersons = new Objects.sitevisitpersons(currentuser,actualuser,pk,CDC)
  End If
  ositevisitpersons.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ositevisitpersons is Nothing then
   ositevisitpersons = new Objects.sitevisitpersons(currentuser,pk,CDC)
  End If
  ositevisitpersons.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.sitevisit_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksitevisit_fk.Visible=IIF(RL>=0,True,False)
  Me.person_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkperson_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

