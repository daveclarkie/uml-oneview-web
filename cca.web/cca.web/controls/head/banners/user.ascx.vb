
Partial Class design_elements_banners_user
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public ReadOnly Property ccaSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.CCASystem, CDC)
        End Get
    End Property
    Public ReadOnly Property matrixSystemAccess() As Boolean
        Get
            Return Security.GroupMember(SM.currentuser, SystemGroups.MatrixSystem, CDC)
        End Get
    End Property
End Class
