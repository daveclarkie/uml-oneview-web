﻿Partial Class coresurvey_details
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Dim dt As DataTable

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.pnl_survey.Visible = False
        Me.pnl_details.Visible = False
        Me.pnl_suppliersurvey.Visible = False


        Dim surveytype_fk As Integer = Request.QueryString("survey")
        Dim st As New surveytypes(SM.currentuser, SM.actualuser, CDC)
        If st.Read(surveytype_fk) Then : lbl_pageheader.Text = st.surveytypename & " Details" : End If

        LoadQuestions(surveytype_fk)
        Select Case surveytype_fk
            Case 1
                LoadAnswers(1, surveytype_fk, rpt_survey1_1)
                LoadAnswers(2, surveytype_fk, rpt_survey1_2)
                LoadAnswers(3, surveytype_fk, rpt_survey1_3)
                LoadAnswers(4, surveytype_fk, rpt_survey1_4)
                LoadAnswers(5, surveytype_fk, rpt_survey1_5)
                LoadAnswers(6, surveytype_fk, rpt_survey1_6)

                LoadNPSScore(1, surveytype_fk, lbl_survey1_nps1)
                LoadNPSScore(2, surveytype_fk, lbl_survey1_nps2)
                LoadNPSScore(3, surveytype_fk, lbl_survey1_nps3)
                LoadNPSScore(4, surveytype_fk, lbl_survey1_nps4)
                LoadNPSScore(5, surveytype_fk, lbl_survey1_nps5)
                LoadNPSScore(6, surveytype_fk, lbl_survey1_nps6)
                GoTo LoadNPSSurveys

            Case 2
                LoadAnswers(1, surveytype_fk, rpt_survey1_1)
                LoadAnswers(2, surveytype_fk, rpt_survey1_2)
                LoadAnswers(3, surveytype_fk, rpt_survey1_3)
                LoadAnswers(4, surveytype_fk, rpt_survey1_4)
                LoadAnswers(5, surveytype_fk, rpt_survey1_5)

                LoadNPSScore(1, surveytype_fk, lbl_survey1_nps1)
                LoadNPSScore(2, surveytype_fk, lbl_survey1_nps2)
                LoadNPSScore(3, surveytype_fk, lbl_survey1_nps3)
                LoadNPSScore(4, surveytype_fk, lbl_survey1_nps4)
                LoadNPSScore(5, surveytype_fk, lbl_survey1_nps5)
                rpt_survey1_6.Visible = False

                GoTo LoadNPSSurveys
            Case 3, 4, 5, 6
                GoTo LoadSupplierSurveys
        End Select

        Exit Sub

LoadNPSSurveys:
        Me.pnl_survey.Visible = True
        Me.pnl_details.Visible = True

        CO = New SqlClient.SqlCommand("rsp_surveys_tocomplete")
        CO.Parameters.AddWithValue("@surveytype_fk", surveytype_fk)
        CO.CommandType = CommandType.StoredProcedure
        dt = CDC.ReadDataTable(CO, 0)
        rpt_surveytocomplete.DataSource = dt
        rpt_surveytocomplete.DataBind()
        CO.Dispose()

        CO = New SqlClient.SqlCommand("rsp_surveys_summarytype")
        CO.Parameters.AddWithValue("@surveytype_fk", surveytype_fk)
        CO.CommandType = CommandType.StoredProcedure
        dt = CDC.ReadDataTable(CO, 0)
        rpt_surveysummarytype.DataSource = dt
        rpt_surveysummarytype.DataBind()
        CO.Dispose()
        Exit Sub
LoadSupplierSurveys:
        Me.pnl_suppliersurvey.Visible = True

        CO = New SqlClient.SqlCommand("rsp_surveys_suppliersurveys_sent")
        CO.Parameters.AddWithValue("@surveytype_fk", surveytype_fk)
        CO.CommandType = CommandType.StoredProcedure
        dt = CDC.ReadDataTable(CO, 0)
        rpt_suppliersurveys.DataSource = dt
        rpt_suppliersurveys.DataBind()
        CO.Dispose()


        Exit Sub

    End Sub

    Private Sub LoadQuestions(ByVal surveytype_fk As Integer)

        Select Case surveytype_fk
            Case 1
                lbl_survey1_0.Text = "The CM was satisfied..."
                lbl_survey1_1.Text = "Q1 - With The Accuracy Of The SSR"
                lbl_survey1_2.Text = "Q2 - With The Quality Format Of The SSR"
                lbl_survey1_3.Text = "Q3 - With The Direction/Recommendation"
                lbl_survey1_4.Text = "Q4 - With The Speed Of Response"
                lbl_survey1_5.Text = "Q5 - With The Professionalism From Sourcing"
                lbl_survey1_6.Text = "Q6 - The Client was satisfied with the overall quality of the Sourcing service"
            Case 2
                lbl_survey1_0.Text = "The Analyst was satisfied that the Client Manager..."
                lbl_survey1_1.Text = "Q1 - Had a good knowledge of their customer"
                lbl_survey1_2.Text = "Q2 - Organised his/her activities and workload well"
                lbl_survey1_3.Text = "Q3 - Was proactive in responding to questions"
                lbl_survey1_4.Text = "Q4 - Pressed for appropriate closure from the customer"
                lbl_survey1_5.Text = "Q5 - Treated me with respect in their interactions with me"
        End Select

    End Sub
    Private Sub LoadAnswers(ByVal question As Integer, ByVal surveytype_fk As Integer, ByVal control As Repeater)
        CO = New SqlClient.SqlCommand("rsp_surveys_summarydetails")
        CO.Parameters.AddWithValue("@surveytype_fk", surveytype_fk)
        CO.Parameters.AddWithValue("@question", question)
        CO.CommandType = CommandType.StoredProcedure
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        control.DataSource = dt
        control.DataBind()
        CO.Dispose()
    End Sub
    Private Sub LoadNPSScore(ByVal question As Integer, ByVal surveytype_fk As Integer, ByVal control As Label)
        Dim value As Double = "0.0000"
        CO = New SqlClient.SqlCommand("rsp_surveys_summarynpsscore")
        CO.Parameters.AddWithValue("@surveytype_fk", surveytype_fk)
        CO.Parameters.AddWithValue("@question", question)
        CO.CommandType = CommandType.StoredProcedure
        CDC.ReadScalarValue(value, CO)
        CO.Dispose()
        control.Text = String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0.00}", value)
    End Sub
End Class
