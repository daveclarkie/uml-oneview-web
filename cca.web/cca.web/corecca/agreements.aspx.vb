
Imports Microsoft.Reporting.WebForms
Imports System.Net
Imports System.Xml
Imports System.Web.UI.Control

Imports Dundas.Charting
Imports Dundas.Charting.WebControl
Imports Dundas.Charting.WebControl.Utilities
Imports Dundas.Charting.WebControl.ChartTypes

Imports System.Drawing
Imports System.Collections.Generic



Partial Class corecca_agreements
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Dim lbAvailableMeters As Object

    Shared cDefault As Color = System.Drawing.ColorTranslator.FromHtml("#009530")
    Shared cOne As Color = System.Drawing.ColorTranslator.FromHtml("#095783") ' Dark Green
    Shared cTwo As Color = System.Drawing.ColorTranslator.FromHtml("#2D04A2") ' Blue / Purple
    Shared cThree As Color = System.Drawing.ColorTranslator.FromHtml("#D5360D") ' Red
    Shared cFour As Color = System.Drawing.ColorTranslator.FromHtml("#DEA317") ' Light Orange
    Shared cFive As Color = System.Drawing.ColorTranslator.FromHtml("#44B934") ' Green
    Shared cSix As Color = System.Drawing.ColorTranslator.FromHtml("#0694d8") ' Light Blue
    Shared cSeven As Color = System.Drawing.ColorTranslator.FromHtml("#b760ee") ' Lilac
    Shared cEight As Color = System.Drawing.ColorTranslator.FromHtml("#7a5a10") ' Brown

    Shared colorSet() As Color = {cDefault, cOne, cTwo, cThree, cFour, cFive, cSix, cSeven, cEight}


    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        validpk = Integer.TryParse(Request.QueryString("pk"), pk)
        validspk = Integer.TryParse(Request.QueryString("spk"), spk)
        validdpk = Integer.TryParse(Request.QueryString("dpk"), dpk)

        If Not validpk Or pk = 0 Then
            ' we shouldn't be here
            Local.Bounce(SM, Response, "~/corecca/search.aspx", "no+valid+agreement+id")
        End If

        ' ===============================================
        '  New Target User?
        '        - Yes - Set Target User for session
        '        - No  - No action required  
        ' ===============================================

        If Not (SM.targetAgreement = pk) Then
            SM.targetAgreement = pk
            SM.mainid = SM.targetAgreement
        End If

        If Not (SM.targetAgreementSecondary = spk) Then
            SM.targetAgreementSecondary = spk
        End If

        If Not (SM.targetDataEntry = dpk) Then
            SM.targetDataEntry = dpk
        End If

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        CheckSessionValues()

        ' ===============================================
        '  Which Tab?
        ' ===============================================
        Try

            Select Case ModeCode
                Case "ag" : CurrentDisplay = AgreementPageMode.Agreement
                    vwSub = vwAgreement
                Case "tg" : CurrentDisplay = AgreementPageMode.AgreementTargets
                    vwSub = vwAgreementTargets
                Case "am" : CurrentDisplay = AgreementPageMode.AgreementMeterPoints
                    vwSub = vwAgreementMeterPoints
                Case "at" : CurrentDisplay = AgreementPageMode.AgreementThroughputs
                    vwSub = vwAgreementThroughputs
                Case "af" : CurrentDisplay = AgreementPageMode.AgreementFuels
                    vwSub = vwAgreementFuels
                Case "dm" : CurrentDisplay = AgreementPageMode.DataEntryConsumption
                    vwSub = vwDataEntryConsumption
                Case "dt" : CurrentDisplay = AgreementPageMode.DataEntryThroughputs
                    vwSub = vwDataEntryThroughputs
                Case "rp" : CurrentDisplay = AgreementPageMode.Reports
                    vwSub = vwAgreementReports
                Case "al" : CurrentDisplay = AgreementPageMode.ActivityLogs
                    vwSub = vwAgreementActivityLogs
                Case "da" : CurrentDisplay = AgreementPageMode.DataAnalysis
                    vwSub = vwAgreementDataAnalysis
                Case "rq" : CurrentDisplay = AgreementPageMode.AgreementRequests
                    vwSub = vwAgreementRequests
                Case "nt" : CurrentDisplay = AgreementPageMode.AgreementNotes
                    vwSub = vwAgreementNotes
                Case "ov" : CurrentDisplay = AgreementPageMode.Overview
                    vwSub = vwAgreementOverview
                Case "ap" : CurrentDisplay = AgreementPageMode.Appointments
                    vwSub = vwAgreementAppointments
                    
                Case "ct" : CurrentDisplay = AgreementPageMode.Contacts
                    vwSub = vwAgreementContacts
                Case "sm" : CurrentDisplay = AgreementPageMode.Summary
                    vwSub = vwAgreementSummary
                Case "sb" : CurrentDisplay = AgreementPageMode.Submission
                    vwSub = vwAgreementSubmission
                Case "rk" : CurrentDisplay = AgreementPageMode.Ranking
                    vwSub = vwAgreementRanking
                Case Else
                    ' we shouldn't be here
                    Local.Bounce(SM, Response, "~/corecca/search.aspx", "invalid+display+mode")
            End Select

            If SM.targetAgreement = -1 And Not CurrentDisplay = AgreementPageMode.Agreement Then
                Local.Bounce(SM, Response, "~/corecca/agreements.aspx", "No+Agreement&pk=-1&pg=ag")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/corecca/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Private Sub CheckSessionValues()
        'load agreement
        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property

    Dim cc As Object
    Dim maintab As String = "search"
    Dim due As DateTime
    Dim diff As Long

    Dim validpk As Boolean = False
    Dim validspk As Boolean = False
    Dim validdpk As Boolean = False
    Dim CurrentAction As Local.Action = Local.Action.None
    Dim CurrentDisplay As AgreementPageMode = AgreementPageMode.Agreement

    Dim vwSub As System.Web.UI.WebControls.View = vwAgreement

    Dim pk As Integer = 0
    Dim spk As Integer = 0
    Dim dpk As Integer = 0
    Dim md As Integer = 0

    Public Mode As AgreementPageMode = AgreementPageMode.Search

    Public Function TabSummary(ByVal item As String) As String
        Dim output As String = ""
        Select Case item
            'Case "sites" : output = sitecount
            'Case "meters" : output = 1
        End Select
        Return "(" & output & ")"
    End Function
    Private Sub SetMode()
        If Page.IsPostBack Then
            Mode = SM.AgreementPageMode
        Else
            SM.targetAgreement = Request.QueryString("pk")
            Mode = AgreementPageMode.Search
            Select Case Request.QueryString("pg")
                Case "ag" : Mode = AgreementPageMode.Agreement
                Case "am" : Mode = AgreementPageMode.AgreementMeterPoints
                Case "at" : Mode = AgreementPageMode.AgreementThroughputs
                Case "dm" : Mode = AgreementPageMode.DataEntryConsumption
                Case "dt" : Mode = AgreementPageMode.DataEntryThroughputs
                Case "tg" : Mode = AgreementPageMode.AgreementTargets
                Case "af" : Mode = AgreementPageMode.AgreementFuels
                Case "rp" : Mode = AgreementPageMode.Reports
                Case "al" : Mode = AgreementPageMode.ActivityLogs
                Case "da" : Mode = AgreementPageMode.DataAnalysis
                Case "rq" : Mode = AgreementPageMode.AgreementRequests
                Case "nt" : Mode = AgreementPageMode.AgreementNotes
                Case "ov" : Mode = AgreementPageMode.Overview
                Case "ap" : Mode = AgreementPageMode.Appointments
                Case "ct" : Mode = AgreementPageMode.Contacts
                Case "sm" : Mode = AgreementPageMode.Summary
                Case "sb" : Mode = AgreementPageMode.Submission
                Case "rk" : Mode = AgreementPageMode.Ranking
                Case Else : Mode = AgreementPageMode.Search
            End Select
            SM.AgreementPageMode = Mode
        End If
    End Sub

    Private Sub PageLoad()
        AgreementNav.Visible = True

        Select Case SM.AgreementPageMode
            Case AgreementPageMode.Agreement : InitAgreement(Page.IsPostBack)
            Case AgreementPageMode.AgreementMeterPoints : InitAgreementMeterPoints(Page.IsPostBack)
            Case AgreementPageMode.AgreementThroughputs : InitAgreementThroughputs(Page.IsPostBack)
            Case AgreementPageMode.AgreementTargets : InitAgreementTargets(Page.IsPostBack)
            Case AgreementPageMode.DataEntryConsumption : InitDataEntryConsumption(Page.IsPostBack)
            Case AgreementPageMode.DataEntryThroughputs : InitDataEntryThroughputs(Page.IsPostBack)
            Case AgreementPageMode.AgreementFuels : InitAgreementFuels(Page.IsPostBack)
            Case AgreementPageMode.Reports : InitAgreementReports(Page.IsPostBack)
            Case AgreementPageMode.ActivityLogs : InitAgreementActivityLogs(Page.IsPostBack)
            Case AgreementPageMode.DataAnalysis : InitAgreementDataAnalysis(Page.IsPostBack)
            Case AgreementPageMode.AgreementRequests : InitAgreementRequests(Page.IsPostBack)
            Case AgreementPageMode.AgreementNotes : InitAgreementNotes(Page.IsPostBack)
            Case AgreementPageMode.Overview : InitAgreementOverview(Page.IsPostBack)
            Case AgreementPageMode.Appointments : InitAgreementAppointments(Page.IsPostBack)
            Case AgreementPageMode.Contacts : InitAgreementContacts(Page.IsPostBack)
            Case AgreementPageMode.Summary : InitAgreementSummary(Page.IsPostBack)
            Case AgreementPageMode.Submission : InitAgreementSubmission(Page.IsPostBack)
            Case AgreementPageMode.Ranking : InitAgreementRanking(Page.IsPostBack)
            Case Else
                Response.Redirect("search.aspx")
        End Select

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "window-script", "loadAnalysisMeters();", True)

    End Sub
    Private Sub DisplayPage()

        '' ===============================================
        ''  Which Action?
        '' ===============================================

        If validspk Then
            CurrentAction = Local.Action.Read
        Else
            CurrentAction = Local.Action.None
        End If

        'Me.mvDetail.ActiveViewIndex = CurrentDisplay
        'If Not vwSub Is Nothing Then mvDetail.SetActiveView(vwSub)

        'Select Case CurrentDisplay
        '    Case Local.AgreementDisplay.Agreement : SetAgreement()
        'End Select

    End Sub
    Private Sub RefreshList()

        '


        If Mode = AgreementPageMode.Overview Then

            Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
            Dim o As New organisations(SM.currentuser, SM.actualuser, a.organisation_fk, CDC)
            lblAgreementServiceAgreementsCustomer.Text = o.customername & " (" & o.varAXRef & ")"

            CO = New SqlCommand
            CO.CommandType = CommandType.Text
            CO.CommandText = "USE [ax_warehouse]; Exec conmngr.GetCurrentClientData @accountnumber = '" & o.varAXRef & "'"
            'CO.Parameters.AddWithValue("@accountnumber", o.varAXRef)
            rptAgreementServiceAgreements.DataSource = CDC.ReadDataTable(CO, 1)
            rptAgreementServiceAgreements.DataBind()
            CO.Dispose()



        ElseIf Mode = AgreementPageMode.Appointments Then



        ElseIf Mode = AgreementPageMode.Agreement Then



        ElseIf Mode = AgreementPageMode.AgreementNotes Then
            
            Dim varFilter As Integer = 0
            varFilter = NotesFilter.SelectedItem.Value
            Dim varSort As String = ""
            varSort = NotesSortBy.SelectedItem.Value

            If varFilter > 0 Then
                varFilter = SM.currentuser
            End If

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementnotes_agreement"
            CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
            CO.Parameters.AddWithValue("@filter", varFilter)
            CO.Parameters.AddWithValue("@sort", varSort)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementNotes.DataSource = dt
            rptAgreementNotes.DataBind()
            CO.Dispose()


        ElseIf Mode = AgreementPageMode.AgreementRequests Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementrequests_agreement"
            CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            CO.Dispose()

            Dim id_list As String = ""
            If dt.Rows.Count > 0 Then
                For Each x As DataRow In dt.Rows
                    id_list &= "," & x("value")
                Next
                If id_list.Length > 0 Then
                    id_list = id_list.Remove(0, 1)
                End If
            End If

            If id_list.Length > 0 Then


                CO = New SqlCommand
                CO.CommandType = CommandType.Text

                Dim helpdesk_sql As String = "USE [servicedesk] "

                helpdesk_sql &= " SELECT "
                helpdesk_sql &= "	  WO.workorderid"
                helpdesk_sql &= ", UDF_CHAR1 [title]"
                helpdesk_sql &= ", (SELECT STATUSNAME FROM STATUSDEFINITION WHERE STATUSID=WOS.STATUSID)	[status]"
                helpdesk_sql &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= WOS.ownerid)		[technician] "
                helpdesk_sql &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= WO.requesterid)		[requester] "
                helpdesk_sql &= ", T.templatename"
                helpdesk_sql &= ", wo.title"
                helpdesk_sql &= ", [se].[fn_GetDateTime](wo.CREATEDTIME) [created]"

                helpdesk_sql &= " FROM"
                helpdesk_sql &= " servicereq_2401 SR"
                helpdesk_sql &= ", workorder WO"
                helpdesk_sql &= ", workorderstates WOS"
                helpdesk_sql &= ", servicecatalog_fields SCF "
                helpdesk_sql &= ", RequestTemplate_List T"
                helpdesk_sql &= " WHERE"
                helpdesk_sql &= " WO.workorderid=SR.workorderid"
                helpdesk_sql &= " AND WOS.workorderid=SR.workorderid"
                helpdesk_sql &= " AND SCF.workorderid=SR.workorderid"
                helpdesk_sql &= " AND T.TEMPLATEID=WO.TEMPLATEID"
                helpdesk_sql &= " AND WO.WORKORDERID IN (" & id_list & ")"

                CO.CommandText = helpdesk_sql
                dt = CDC.ReadDataTable(CO, 1)
                rptAgreementRequests.DataSource = dt
                rptAgreementRequests.DataBind()
                CO.Dispose()
            End If

        ElseIf Mode = AgreementPageMode.AgreementTargets Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementtargets_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementTarget.DataSource = dt
            rptAgreementTarget.DataBind()
            CO.Dispose()

        ElseIf Mode = AgreementPageMode.AgreementMeterPoints Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementmeterpoints_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementMeterPoints.DataSource = dt
            rptAgreementMeterPoints.DataBind()
            CO.Dispose()

        ElseIf Mode = AgreementPageMode.DataAnalysis Then

            If Not IsDate(datefrom.Text) Then : datefrom.Text = "01 Jan " & DatePart(DateInterval.Year, Now) : End If
            If Not IsDate(dateto.Text) Then : dateto.Text = "01 " & MonthName(DatePart(DateInterval.Month, DateAdd(DateInterval.Month, -1, Now)), True) & " " & DatePart(DateInterval.Year, Now) : End If

        ElseIf Mode = AgreementPageMode.AgreementThroughputs Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementthroughputs_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementThroughputs.DataSource = dt
            rptAgreementThroughputs.DataBind()
            CO.Dispose()

        ElseIf Mode = AgreementPageMode.AgreementFuels Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementfuels_agreement"
            CO.Parameters.AddWithValue("@agreement", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementFuels.DataSource = dt
            rptAgreementFuels.DataBind()
            CO.Dispose()

        ElseIf Mode = AgreementPageMode.DataEntryConsumption Then

            Dim amp As New agreementmeterpoints(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            Dim strSQL As String = "rsp_dataconsumptions_missing_meterpoint"
            CO.Parameters.AddWithValue("@meterpoint", amp.meterpoint_fk)
            CO.CommandText = strSQL
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptDataMissingConsumption.DataSource = dt
            rptDataMissingConsumption.DataBind()
            CO.Dispose()



            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_dataconsumptions_meterpoint"
            CO.Parameters.AddWithValue("@meterpoint", amp.meterpoint_fk)
            dt = CDC.ReadDataTable(CO, 0)
            rptDataEntryConsumption.DataSource = dt
            rptDataEntryConsumption.DataBind()
            CO.Dispose()




        ElseIf Mode = AgreementPageMode.DataEntryThroughputs Then


            Dim atp As New agreementthroughputs(SM.currentuser, SM.actualuser, SM.targetDataEntry, CDC)

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_datathroughputs_throughput"
            CO.Parameters.AddWithValue("@throughput", atp.throughput_fk)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptDataEntryThroughput.DataSource = dt
            rptDataEntryThroughput.DataBind()
            CO.Dispose()



        ElseIf Mode = AgreementPageMode.Reports Then

            'CO = New SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_bills_creditnotes"
            'CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
            'Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            'rptcreditnotes_latest.DataSource = dt
            'rptcreditnotes_latest.DataBind()
            'CO.Dispose()

        ElseIf Mode = AgreementPageMode.ActivityLogs Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreement_activitylogs"
            CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementActivityLogs.DataSource = dt
            rptAgreementActivityLogs.DataBind()
            CO.Dispose()



        ElseIf Mode = AgreementPageMode.Summary Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreement_summary_base"
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementSummaryBase.DataSource = dt
            rptAgreementSummaryBase.DataBind()
            CO.Dispose()

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreement_summary_reporting"
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
            dt = CDC.ReadDataTable(CO, 0)
            rptAgreementSummaryReporting.DataSource = dt
            rptAgreementSummaryReporting.DataBind()
            CO.Dispose()


            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreement_summary_submission"
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
            dt = CDC.ReadDataTable(CO, 0)
            rptAgreementSubmission.DataSource = dt
            rptAgreementSubmission.DataBind()
            CO.Dispose()

        ElseIf Mode = AgreementPageMode.Ranking Then


            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreement_ranking"
            CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptAgreementRanking.DataSource = dt
            rptAgreementRanking.DataBind()
            CO.Dispose()

        End If

    End Sub

    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Boolean = True
        If primary = True Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetAgreement)
        Else
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetAgreementSecondary)
        End If

        If Not (resDet) Then
            If Not cc.lasterror Is Nothing Then

                Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
                If errMsg.Length > 1 Then
                    AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
                Else
                    AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
                End If
            End If
        Else
            AddNotice("setNotice('Record Saved');")
            Dim filtered As New NameValueCollection(Request.QueryString)
            Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
            nameValueCollection.Remove("spk")
            Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString + "&r=Record+Saved"
            Response.Redirect(url)
            'RefreshList()
            PageLoad()
        End If
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("spk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub
    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control, ByVal isAgreement As Boolean)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM
        If isAgreement Then
            cc.Read(SM.currentuser, SM.actualuser, SM.targetAgreement)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        Else
            cc.Read(SM.currentuser, SM.actualuser, SM.targetAgreementSecondary)
            If Request.QueryString("spk") Is Nothing Then
                TargetControl.Controls.Clear()
                TargetButton.Visible = False
                RefreshList()
            Else
                TargetControl.Controls.Add(cc)
                TargetButton.Visible = True
                RefreshList()
            End If
        End If


    End Sub

    Private Sub InitAgreement(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreement)
        SetControl("~/controls/agreements_ctl.ascx", AgreementControl, AgreementButton, True)

        If agreementStatus() = 0 Then
            btnManageAgreement.Text = "<h6>Stop Managing</h6>"
        ElseIf agreementStatus() = 1 Then
            btnManageAgreement.Text = "<h6>Manage</h6>"
        End If
    End Sub
    Private Sub InitAgreementMeterPoints(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementMeterPoints)
        SetControl("~/controls/agreementmeterpoints_ctl.ascx", AgreementMeterPointControl, AgreementMeterPointButton, False)
    End Sub
    Private Sub InitAgreementThroughputs(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementThroughputs)
        SetControl("~/controls/combo/agreementthroughput_combo_ctl.ascx", AgreementThroughputControl, AgreementThroughputButton, False)
    End Sub
    Private Sub InitAgreementTargets(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementTargets)
        SetControl("~/controls/agreementtargets_ctl.ascx", AgreementTargetControl, AgreementTargetButton, False)
    End Sub
    Private Sub InitAgreementFuels(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementFuels)
        SetControl("~/controls/agreementfuels_ctl.ascx", AgreementFuelControl, AgreementFuelButton, False)
    End Sub
    Private Sub InitAgreementReports(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementReports)


        Dim ag As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        If ag.agreementtype_fk = 1 Then ' Absolute Agreement
            rp.Read(1)
        ElseIf ag.agreementtype_fk = 4 Then ' NOVEM Agreement
            rp.Read(2)
        ElseIf ag.agreementtype_fk = 5 Then ' MPMA Agreement
            rp.Read(3)
        Else ' Relative Agreement Types
            If ag.federation_fk = 5 Then        ' Cold Store Distribution Federation
                rp.Read(4)
            ElseIf ag.federation_fk = 14 Then   ' National Farmers Union Federation
                rp.Read(4)
            ElseIf ag.federation_fk = 21 Then   ' British Cement Association
                rp.Read(5)
            Else                                ' Others
                rp.Read(6)
            End If
        End If

        If rp.rowstatus = 0 Then

            'Me.hmrcreportfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
            'Me.hmrcreportfrom.Attributes.Add("onBlur", "clearNotice()")

            'Me.hmrcreportto.Attributes.Add("onFocus", "setNotice(dateFormat)")
            'Me.hmrcreportto.Attributes.Add("onBlur", "clearNotice()")

            CO = New SqlClient.SqlCommand("rsp_lkp_agreementtargets_agreement")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
            If Me.ddltarget.Items.Count = 0 Then
                Me.ddltarget.DataSource = CDC.ReadDataTable(CO)
                Me.ddltarget.DataTextField = "value"
                Me.ddltarget.DataValueField = "pk"
                Try
                    Me.ddltarget.DataBind()
                Catch Ex As Exception
                    Me.ddltarget.SelectedValue = -1
                    Me.ddltarget.DataBind()
                End Try
            End If

            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_lkp_agreementmeters_agreement"
            CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
            If Me.ddlreportmeters.Items.Count = 0 Then
                Me.ddlreportmeters.DataSource = CDC.ReadDataTable(CO)
                Me.ddlreportmeters.DataTextField = "value"
                Me.ddlreportmeters.DataValueField = "pk"
                Try
                    Me.ddlreportmeters.DataBind()
                Catch Ex As Exception
                    Me.ddlreportmeters.SelectedValue = -1
                    Me.ddlreportmeters.DataBind()
                End Try
            End If

        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", SM.currentuser)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

        If helpdesklogindetail_pk = 0 Then
            Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
            Me.lblformResponse.ForeColor = Drawing.Color.Red
            Me.panelError.Visible = False
            Me.panelErrorResponse.Visible = True
        End If

        RefreshList()

    End Sub
    Private Sub InitDataEntryConsumption(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwDataEntryConsumption)
        SetControl("~/controls/dataconsumptions_ctl.ascx", MeterPointConsumptionControl, MeterPointConsumptionButton, False)
    End Sub
    Private Sub InitDataEntryThroughputs(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwDataEntryThroughputs)
        SetControl("~/controls/datathroughputs_ctl.ascx", ThroughputConsumptionControl, ThroughputConsumptionButton, False)
    End Sub
    Private Sub InitAgreementActivityLogs(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementActivityLogs)
        RefreshList()

    End Sub
    Private Sub InitAgreementDataAnalysis(ByVal postback As Boolean)

        AgreementNav.Visible = False
        mvDetail.SetActiveView(vwAgreementDataAnalysis)
        RefreshList()
    End Sub
    Private Sub InitAgreementRequests(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementRequests)
        SetControl("~/controls/agreementrequests_ctl.ascx", AgreementRequestsControl, AgreementRequestsTable, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementNotes(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementNotes)
        SetControl("~/controls/custom/agreementnotes_ctl.ascx", AgreementNotesControl, AgreementNotesButton, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementOverview(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementOverview)
        RefreshList()
    End Sub
    Private Sub InitAgreementAppointments(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementAppointments)
        SetControl("~/controls/appointments_ctl.ascx", AgreementAppointmentsControl, AgreementAppointmentsButton, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementContacts(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementContacts)
        'SetControl("~/controls/appointments_ctl.ascx", AgreementAppointmentsControl, AgreementAppointmentsButton, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementSummary(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementSummary)
        'SetControl("~/controls/appointments_ctl.ascx", AgreementAppointmentsControl, AgreementAppointmentsButton, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementSubmission(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementSubmission)
        'SetControl("~/controls/appointments_ctl.ascx", AgreementAppointmentsControl, AgreementAppointmentsButton, False)
        RefreshList()
    End Sub
    Private Sub InitAgreementRanking(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAgreementRanking)
        'SetControl("~/controls/appointments_ctl.ascx", AgreementAppointmentsControl, AgreementAppointmentsButton, False)
        RefreshList()
    End Sub



    Protected Sub rptReportDataCheck_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If rptReportDataCheck.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                'Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyDataFooter"), Label)
                'lblEmptyDataFooter.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnSaveAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAgreement.Click
        SaveContol(True)
    End Sub
    Protected Sub btnSaveAgreementTarget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAgreementTarget.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveAgreementMeterPoint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAgreementMeterPoint.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveAgreementThroughput_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAgreementThroughput.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveAgreementFuel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAgreementFuel.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveAgreementRequests_Click(sender As Object, e As System.EventArgs) Handles btnSaveAgreementRequests.Click
        SaveContol(False)
    End Sub

    Protected Sub btnSaveAgreementNotes_Click(sender As Object, e As System.EventArgs) Handles btnSaveAgreementNotes.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveAgreementAppointments_Click(sender As Object, e As System.EventArgs) Handles btnSaveAgreementAppointments.Click
        SaveContol(False)
    End Sub


    Protected Sub btnCancelAgreementTarget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAgreementTarget.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelAgreementMeterPoint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAgreementMeterPoint.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelAgreementThroughput_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAgreementThroughput.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelMeterPointConsumption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelMeterPointConsumption.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelThroughputConsumption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelThroughputConsumption.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelAgreementRequests_Click(sender As Object, e As System.EventArgs) Handles btnCancelAgreementRequests.Click
        SaveContol(False)
    End Sub
    Protected Sub btnCancelAgreementAppointments_Click(sender As Object, e As System.EventArgs) Handles btnCancelAgreementAppointments.Click
        SaveContol(False)
    End Sub

    Protected Sub btnCheckMeterPointConsumption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckMeterPointConsumption.Click

        CO = New SqlCommand
        CO.CommandTimeout = 120
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_dataentryinvoicedconsumption_month_missing"
        CO.Parameters.AddWithValue("@agreementmeterpoint_pk", SM.targetDataEntry)
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        For Each dr As DataRow In dt.Rows
            If dr(4) > -1 Then
                Dim dc As New dataconsumptions(SM.currentuser, SM.actualuser, CDC)
                dc.meterpoint_fk = dr(1)
                dc.month_fk = dr(2)
                dc.year_fk = dr(3)
                dc.totalconsumption = dr(4)
                dc.eligibleconsumption = dr(4)
                dc.dataimportmethod_fk = 3 'Imported from Contract Manager
                dc.Save()
            End If

        Next

        Response.Redirect(Request.RawUrl)
        CO.Dispose()
    End Sub

    Public Function SelectedItemName() As String
        Dim itemclass As String = ""
        Select Case Mode
            Case AgreementPageMode.Agreement
                itemclass = ""
            Case AgreementPageMode.AgreementMeterPoints, AgreementPageMode.AgreementThroughputs
                Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
                itemclass &= "for - " & a.agreementname
            Case Else : itemclass = ""
        End Select

        Return itemclass
    End Function
    Public Function isLatestAgreement() As Boolean
        Dim _return As Boolean = False
        CO = New SqlCommand("rsp_agreement_checklatestversionlevel")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement_pk", SM.targetAgreement)
        CDC.ReadScalarValue(_return, CO)
        Return _return
    End Function
    Private Sub SnapshotAgreement()

        Dim dr As DataRow
        Dim dt As DataTable
        ' create new agreement
        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Dim a_new As New agreements(SM.currentuser, SM.actualuser, CDC)
        a_new.organisation_fk = a.organisation_fk
        a_new.agreementname = a.agreementname
        a_new.federation_fk = a.federation_fk
        a_new.agreementnumber = a.agreementnumber
        a_new.facilitynumber = a.facilitynumber
        a_new.targetunitidentifier = a.targetunitidentifier
        a_new.baseyearstart = a.baseyearstart
        a_new.baseyearend = a.baseyearend
        a_new.agreementtype_fk = a.agreementtype_fk
        a_new.agreementmeasure_fk = a.agreementmeasure_fk
        a_new.agreement_fk = a.agreement_pk
        a_new.notes = a.notes
        a_new.Save()

        ' copy targets
        CO = New SqlCommand("rkg_agreementtargets_byagreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement_fk", a.agreement_pk)
        dt = CDC.ReadDataTable(CO)
        For Each dr In dt.Rows
            Dim at As New agreementtargets(SM.currentuser, SM.actualuser, dr(0), CDC)
            Dim at_new As New agreementtargets(SM.currentuser, SM.actualuser, CDC)
            at_new.agreement_fk = a_new.agreement_pk
            at_new.targetdescription = at.targetdescription
            at_new.targetstart = at.targetstart
            at_new.targetend = at.targetend
            at_new.target = at.target
            at_new.Save()
        Next

        ' link meter points to new agreement
        CO = New SqlCommand("rkg_agreementmeterpoints_byagreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement_fk", a.agreement_pk)
        Dim dr_mps As DataRow, dt_mps As DataTable
        dt_mps = CDC.ReadDataTable(CO)
        For Each dr_mps In dt_mps.Rows
            Dim amp As New agreementmeterpoints(SM.currentuser, SM.actualuser, dr_mps(0), CDC)
            Dim amp_new As New agreementmeterpoints(SM.currentuser, SM.actualuser, CDC)
            amp_new.agreement_fk = a_new.agreement_pk
            amp_new.meterpoint_fk = amp.meterpoint_fk
            amp_new.datafrequency_fk = amp.datafrequency_fk
            amp_new.Save()
        Next

        ' link throughputs to new agreement
        CO = New SqlCommand("rkg_agreementthroughputs_byagreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement_fk", a.agreement_pk)
        Dim dr_tps As DataRow, dt_tps As DataTable
        dt_tps = CDC.ReadDataTable(CO)
        For Each dr_tps In dt_tps.Rows
            Dim atp As New agreementthroughputs(SM.currentuser, SM.actualuser, dr_tps(0), CDC)
            Dim atp_new As New agreementthroughputs(SM.currentuser, SM.actualuser, CDC)
            atp_new.agreement_fk = a_new.agreement_pk
            atp_new.throughput_fk = atp.throughput_fk
            atp_new.datafrequency_fk = atp.datafrequency_fk
            atp_new.Save()
        Next

        ' copy fuels to new agreement
        CO = New SqlCommand("rkg_agreementfuels_byagreement")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@agreement_fk", a.agreement_pk)
        Dim dr_f As DataRow, dt_f As DataTable
        dt_f = CDC.ReadDataTable(CO)
        For Each dr_f In dt_f.Rows
            Dim af As New agreementfuels(SM.currentuser, SM.actualuser, dr_f(0), CDC)
            Dim af_new As New agreementfuels(SM.currentuser, SM.actualuser, CDC)
            af_new.agreement_fk = a_new.agreement_pk
            af_new.fuel_fk = af.fuel_fk
            af_new.eligibility = af.eligibility
            af_new.validfrom = af.validfrom
            af_new.validto = af.validto
            af_new.relief = af.relief
            af_new.Save()
        Next

        SM.targetAgreement = a_new.agreement_pk
        SM.Save()
        Response.Redirect("agreements.aspx?pg=ag&pk=" & SM.targetAgreement, True)
    End Sub

    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""
        Select Case Mode
            Case AgreementPageMode.Agreement : itemclass = IIf(item = "agreement", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementRequests : itemclass = IIf(item = "requests", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementMeterPoints : itemclass = IIf(item = "meterpoint", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementThroughputs : itemclass = IIf(item = "throughput", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementTargets : itemclass = IIf(item = "target", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementFuels : itemclass = IIf(item = "fuel", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Reports : itemclass = IIf(item = "report", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.DataEntryConsumption : itemclass = IIf(item = "meterpoint", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.DataEntryThroughputs : itemclass = IIf(item = "throughput", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.ActivityLogs : itemclass = IIf(item = "activitylog", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.DataAnalysis : itemclass = IIf(item = "dataanalysis", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.AgreementNotes : itemclass = IIf(item = "notes", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Overview : itemclass = IIf(item = "overview", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Appointments : itemclass = IIf(item = "appointments", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Contacts : itemclass = IIf(item = "contacts", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Summary : itemclass = IIf(item = "summary", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Submission : itemclass = IIf(item = "submission", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case AgreementPageMode.Ranking : itemclass = IIf(item = "ranking", "selected", "") : itemclass = IIf(item = "dataentry", "hidden", itemclass)
            Case InvoicePageMode.Search : itemclass = ""
        End Select
        Return itemclass
    End Function
    Public Function IsStaff() As Boolean
        Return cca.common.Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
    End Function
    Public Function agreementStatus() As Integer
        Dim _return As Integer = -1

        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Try
            _return = a.rowstatus
        Catch ex As Exception
        End Try

        Return _return
    End Function

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMode()
        PageLoad()
    End Sub
    Protected Sub btnSnapshotAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSnapshotAgreement.Click
        SnapshotAgreement()
    End Sub

    Protected Sub ddltarget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltarget.SelectedIndexChanged
        If ddltarget.SelectedValue = -1 Then
            ddlperiod.Items.Clear()
            ddlperiod.Items.Add("Select...")

            btnExportInvoice.Enabled = False
            btnExportInvoice.Text = "<h4>Export Invoice Data</h4>"

            rptReportDataCheck.Visible = False
            rptReportDataCheck.DataSource = Nothing
            rptReportDataCheck.DataBind()

        Else
            CO = New SqlClient.SqlCommand("rsp_lkp_periods_agreementtarget")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@agreementtarget_fk", ddltarget.SelectedItem.Value)
            Me.ddlperiod.Items.Clear()
            Me.ddlperiod.DataSource = CDC.ReadDataTable(CO)
            Me.ddlperiod.DataTextField = "value"
            Me.ddlperiod.DataValueField = "pk"
            Try
                Me.ddlperiod.DataBind()
            Catch Ex As Exception
                Me.ddlperiod.SelectedValue = -1
                Me.ddlperiod.DataBind()
            End Try

            btnExportInvoice.Enabled = True
            btnExportInvoice.Text = "<h6>Export Invoice Data</h6>"

        End If
    End Sub

    Protected Sub ddlperiod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlperiod.SelectedIndexChanged
        If ddlperiod.SelectedIndex = -1 Then
            btnRunReport.Enabled = False
            btnRunReport.Text = "<h4>Run Report</h4>"

            rptReportDataCheck.Visible = False
            rptReportDataCheck.DataSource = Nothing
            rptReportDataCheck.DataBind()

        Else
            btnRunReport.Enabled = True
            btnRunReport.Text = "<h6>Run Report</h6>"


            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_reportdatacheck_targetdate"
            CO.Parameters.AddWithValue("@agreementtarget_fk", ddltarget.SelectedItem.Value)
            CO.Parameters.AddWithValue("@targetperiod", ddlperiod.SelectedItem.Value)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptReportDataCheck.DataSource = dt
            rptReportDataCheck.DataBind()
            CO.Dispose()
            rptReportDataCheck.Visible = True

        End If
    End Sub

    Protected Sub ddlreportmeters_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlreportmeters.SelectedIndexChanged
        If ddlreportmeters.SelectedIndex = -1 Then
            btnRunHMRCReport.Enabled = False
            btnRunHMRCReport.Text = "<h4>Run Report</h4>"
        Else
            btnRunHMRCReport.Enabled = True
            btnRunHMRCReport.Text = "<h6>Run Report</h6>"
        End If

    End Sub

    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


        Dim ag As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)
        Dim rp As New reports(SM.currentuser, SM.actualuser, CDC)

        If ag.agreementtype_fk = 1 Then ' Absolute Agreement
            rp.Read(1)
        ElseIf ag.agreementtype_fk = 4 Then ' NOVEM Agreement
            rp.Read(2)
        ElseIf ag.agreementtype_fk = 5 Then ' MPMA Agreement
            rp.Read(3)
        Else ' Relative Agreement Types
            If ag.federation_fk = 5 Then        ' Cold Store Distribution Federation
                rp.Read(4)
            ElseIf ag.federation_fk = 14 Then   ' National Farmers Union Federation
                rp.Read(4)
            ElseIf ag.federation_fk = 21 Then   ' British Cement Association
                rp.Read(5)
            Else                                ' Others
                rp.Read(6)
            End If
        End If


        If rp.rowstatus = 0 Then

            Dim qsAgreementTarget As Integer = ddltarget.SelectedValue
            Dim qsPeriod As Integer = ddlperiod.SelectedValue

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport

            'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)
            'Set the report server URL and report path
            'serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
            'serverReport.ReportPath = "/Customer Reports/Energy Management/Phase 2 - Absolute CCLA MTS"
            serverReport.ReportServerUrl = New Uri(rp.reportserverurl)
            serverReport.ReportPath = rp.reportpath


            Dim Target As New Microsoft.Reporting.WebForms.ReportParameter()
            Target.Name = "Target"
            Target.Values.Add(qsAgreementTarget)

            Dim Period As New Microsoft.Reporting.WebForms.ReportParameter()
            Period.Name = "Period"
            Period.Values.Add(qsPeriod)
            
            Dim filePath As String = sys.datalocation & "Reports\00.CCA MTS Reporting\" & SM.targetAgreement & "\"       'HttpContext.Current.Server.MapPath("~/App_Data/DS/")
            If (Not System.IO.Directory.Exists(filePath)) Then
                System.IO.Directory.CreateDirectory(filePath)
            End If

            Dim datestring As String = Now.ToString("yyyyMMdd_HHmmss_")

            Dim rt As New reporttypes(SM.currentuser, SM.actualuser, rp.reporttype_fk, CDC)

            Dim rl As New reportlogs(SM.currentuser, SM.actualuser, CDC)


            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Target, Period}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

            rl.report_fk = rp.report_pk
            rl.runfromurl = Request.Url.AbsoluteUri
            rl.param1name = "target"
            rl.param1value = qsAgreementTarget
            rl.param2name = "period"
            rl.param2value = qsPeriod
            rl.param3name = ""
            rl.param3value = ""
            rl.param4name = ""
            rl.param4value = ""
            rl.savedpath = filePath & datestring & rp.reportname + "." + extension
            rl.Save()

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' \\uk-ol1-file-01\Group Shares\Data\Reports\00.CCA MTS Reporting\
            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + rp.reportname + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)

            IO.File.WriteAllBytes(filePath & datestring & rp.reportname + "." + extension, bytes)

            Response.Flush()
            Response.[End]()


        Else

            AddNotice("setError('" & "Report currently offline.');")

        End If


    End Sub

    Protected Sub btnExportInvoice_Click(sender As Object, e As System.EventArgs) Handles btnExportInvoice.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim qsAgreementTarget As Integer = ddltarget.SelectedValue

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        'Set the report server URL and report path
        serverReport.ReportServerUrl = _
            New Uri(sys.ssrsurl)
        serverReport.ReportPath = "/Operational Reports/Energy Management/CCA Invoiced Consumption Export"

        Dim Target As New Microsoft.Reporting.WebForms.ReportParameter()
        Target.Name = "agreementtarget_pk"
        Target.Values.Add(qsAgreementTarget)

        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {Target}
        serverReport.SetParameters(parameters)

        Dim warnings As Warning() = {}
        Dim streamids As String() = {}
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim extension As String = ""
        Dim deviceInfo As String = ""
        deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
        Dim bytes As Byte() = serverReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamids, warnings)

        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType

        ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
        Response.AddHeader("Content-Disposition", "attachment; filename=CCA Invoice Data Export." + extension)
        'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.[End]()
    End Sub
    Protected Sub btnManageAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnManageAgreement.Click
        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.targetAgreement, CDC)

        If agreementStatus() = 0 Then
            a.Disable()
        ElseIf agreementStatus() = 1 Then
            a.Enable()
        End If
        InitAgreement(Page.IsPostBack)

    End Sub

    Protected Sub btnRunHMRCReport_Click(sender As Object, e As System.EventArgs) Handles btnRunHMRCReport.Click
        '
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim qsagreementmeterpoint_pk As Integer = ddlreportmeters.SelectedValue
        Dim qsdate_start As Date = hmrcreportfrom.Text
        Dim qsdate_end As Date = hmrcreportto.Text

        'Set the processing mode for the ReportViewer to Remote
        myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

        Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
        serverReport = myReportViewer.ServerReport

        'serverReport.ReportServerCredentials = New CustomReportCredentials(sys.ssrsusername, sys.ssrspassword, sys.ssrsdomain)

        'Set the report server URL and report path
        serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
        serverReport.ReportPath = "/UML/One View/MTS Reports/CCLA - HMRC Audit Report"




        Dim agreementmeterpoint_pk As New Microsoft.Reporting.WebForms.ReportParameter()
        agreementmeterpoint_pk.Name = "agreementmeterpoint_pk"
        agreementmeterpoint_pk.Values.Add(qsagreementmeterpoint_pk)

        Dim date_start As New Microsoft.Reporting.WebForms.ReportParameter()
        date_start.Name = "date_start"
        date_start.Values.Add(qsdate_start)

        Dim date_end As New Microsoft.Reporting.WebForms.ReportParameter()
        date_end.Name = "date_end"
        date_end.Values.Add(qsdate_end)


        'Set the report parameters for the report
        Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {agreementmeterpoint_pk, date_start, date_end}
        serverReport.SetParameters(parameters)

        Dim warnings As Warning() = {}
        Dim streamids As String() = {}
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim extension As String = ""
        Dim deviceInfo As String = ""
        deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
        Dim bytes As Byte() = serverReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType

        ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
        Response.AddHeader("Content-Disposition", "attachment; filename=HMRC Audit Report." + extension)
        'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.[End]()
    End Sub

    Protected Sub btnLogError_Click(sender As Object, e As System.EventArgs) Handles btnLogError.Click
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim URL As String = sys.helpdeskurl & "/servlets/RequestServlet"

        Dim x As New agreements(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        Dim title As String = "CCA Application Reporting Issue | Single | " & x.agreementname


        Dim Description As String = ""
        Description &= "<b>Single Agreement Report</b>"
        Description &= "<br />"
        Description &= "Description (optional):<br />" & Me.txtErrorDescription.Text.ToString & "<br /><br />"
        Description &= "<br />"
        Description &= "<b>Milestone Tracking Service Report (MTS)</b><br />"
        Description &= "Target: " & ddltarget.SelectedItem.Text & " (" & ddltarget.SelectedItem.Value & ") <br />"
        If ddlperiod.Items.Count > 0 Then
            Description &= "Period: " & ddlperiod.SelectedItem.Text & " (" & ddlperiod.SelectedItem.Value & ") <br />"
        End If
        Description &= "<br />"
        Description &= "<b>HMRC CCL Rate Audit Report</b><br />"
        Description &= "Meter: " & ddlreportmeters.SelectedItem.Text & " (" & ddlreportmeters.SelectedItem.Value & ") <br />"
        Description &= "From: " & hmrcreportfrom.Text & " <br />"
        Description &= "To: " & hmrcreportto.Text & " <br />"
        Description &= "<br />"
        Description &= "<br />"

        Description &= "Requested created via API link from application"


        Dim reqtemplate As String = "CCA Application Reporting Issue"

        Dim targetURL As String = "/servlets/RequestServlet"
        Dim operation As String = "AddRequest"
        Dim technician As String = "Dave Clarke"

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", SM.currentuser)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

        Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
        Me.lblformResponse.ForeColor = Drawing.Color.Red
        Me.panelError.Visible = False
        Me.panelErrorResponse.Visible = True

        If helpdesklogindetail_pk > 0 Then
            Dim hd As New helpdesklogindetails(SM.currentuser, SM.actualuser, helpdesklogindetail_pk, CDC)
            Dim hd_domain As New helpdeskdomains(SM.currentuser, SM.actualuser, hd.helpdeskdomain_fk, CDC)
            Dim hd_logon As New helpdesklogondomains(SM.currentuser, SM.actualuser, hd.helpdesklogondomain_fk, CDC)


            Dim username As String = hd.username
            Dim password As String = hd.password
            Dim domain_name As String = hd_domain.domain_name
            Dim logonDomainName As String = hd_logon.logon_domain_name

            Dim postData As String = ""
            postData &= "targetURL=" & targetURL
            postData &= "&reqtemplate=" & reqtemplate
            postData &= "&username=" & username
            postData &= "&password=" & password
            postData &= "&logonDomainName=" & logonDomainName
            If domain_name.Length > 0 Then postData &= "&DOMAIN_NAME=" & domain_name
            postData &= "&operation=" & operation

            postData &= "&Application URL=" & Request.Url.AbsoluteUri
            postData &= "&description=" & Description
            postData &= "&title=" & title


            PostTo(URL, postData)
        Else
            Me.lblformResponse.Text = "Your user account is not linked to helpdesk. Please contact Dave Clarke."
            Me.lblformResponse.ForeColor = Drawing.Color.Red
            Me.panelError.Visible = False
            Me.panelErrorResponse.Visible = True

        End If

    End Sub

    Protected Sub PostTo(url As String, postData As String)
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
        myWebRequest.Method = "POST"
        Dim byteArray As Byte() = System.Text.Encoding.[Default].GetBytes(postData)
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        myWebRequest.ContentLength = byteArray.Length
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        'Response.Write((DirectCast(myWebResponse, HttpWebResponse)).StatusDescription)
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = "<?xml version='1.0'?>" & reader.ReadToEnd()
        'Response.Write(responseFromServer + ":")

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim formResponse As String = "<table>"
        Dim outcome As String = xmlDoc.SelectSingleNode("/operation/operationstatus").InnerText
        formResponse &= "<tr><td width='100px'>Outcome</td> <td>" & outcome & "</td></tr>"
        If outcome = "Success" Then
            formResponse &= "<tr><td colspan='2'>The request ID is " & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "</td></tr>"
            formResponse &= "<tr><td colspan='2'><a style='color:blue;text-decoration:underline;' target='_blank' href='" & sys.helpdeskurl & "/WorkOrder.do?woMode=viewWO&woID=" & xmlDoc.SelectSingleNode("/operation/workorderid").InnerText & "&&fromListView=true'>View Here</a></td></tr>"
        ElseIf outcome = "Failure" Then
            Dim xmlReason As String = xmlDoc.SelectSingleNode("/operation/message").InnerText
            formResponse &= "<tr><td>Reason</td> <td>" & xmlReason & "</td></tr>"
            If xmlReason = "Invalid UserName or Password" Then
                formResponse &= "<tr><td></td> <td>Please contact <a style='color:blue;text-decoration:underline;' href='mailto:dave.clarke@ems.schneider-electric.com?Subject=CCA App Helpdesk User Account Error'>Dave Clarke</a></td></tr>"
            End If
        End If
        formResponse &= "</table>"

        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Me.lblformResponse.Text = formResponse
        Me.lblformResponse.ForeColor = Drawing.Color.Black
        Me.panelError.Visible = False
        Me.panelErrorResponse.Visible = True

    End Sub

    Protected Sub lnkviewdateperiod_Click(sender As Object, e As System.EventArgs) Handles lnkviewdateperiod.Click

        myReportViewer1.Visible = False

        If Not IsDate(datefrom.Text) Then : datefrom.Text = "01 Jan " & DatePart(DateInterval.Year, Now) : End If
        If Not IsDate(dateto.Text) Then : dateto.Text = "01 Dec " & DatePart(DateInterval.Year, Now) : End If

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_cca_selected_analysis_dataconsumptionmeters"
        CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
        CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        CO.Dispose()

        If dt.Rows.Count > 0 Then
            Dim meterlist As String = ""

            For Each x As DataRow In dt.Rows
                meterlist &= "," & x("agreementmeterpoint_pk")
            Next
            If meterlist.Length > 0 Then
                meterlist = meterlist.Remove(0, 1)
            End If

            'dataconsumptionsanalysistable(meterlist, datefrom.Text, dateto.Text, chkDataAnalysis.Checked)
            'dataconsumptionsanalysischart(meterlist, datefrom.Text, dateto.Text, chkDataAnalysis.Checked)

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer1.ServerReport

            'serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

            'Set the report server URL and report path
            serverReport.ReportServerUrl = New Uri("http://reports.mceg.local/ReportServer12")
            serverReport.ReportPath = "/UML/One View/CCA Reports/Data Analysis"

            Dim varMeterList As New Microsoft.Reporting.WebForms.ReportParameter()
            varMeterList.Name = "varMeterList"
            varMeterList.Values.Add(meterlist)

            Dim varDateFrom As New Microsoft.Reporting.WebForms.ReportParameter()
            varDateFrom.Name = "date_start"
            varDateFrom.Values.Add(datefrom.Text)

            Dim varDateTo As New Microsoft.Reporting.WebForms.ReportParameter()
            varDateTo.Name = "date_end"
            varDateTo.Values.Add(dateto.Text)

            Dim chkd As Integer = 0
            If chkDataAnalysis.Checked Then : chkd = 1 : End If

            Dim varChecked As New Microsoft.Reporting.WebForms.ReportParameter()
            varChecked.Name = "baseyear"
            varChecked.Values.Add(chkd)

            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeterList, varDateFrom, varDateTo, varChecked}
            serverReport.SetParameters(parameters)

            myReportViewer1.Visible = True
        Else
            AddNotice("setNotice('No Meters Selected');")
        End If
    End Sub

    Protected Sub lnkviewauditdata_Click(sender As Object, e As System.EventArgs) Handles lnkviewauditdata.Click

        myReportViewer1.Visible = False


        If Not IsDate(datefrom.Text) Then : datefrom.Text = "01 Jan " & DatePart(DateInterval.Year, Now) : End If
        If Not IsDate(dateto.Text) Then : dateto.Text = "01 Dec " & DatePart(DateInterval.Year, Now) : End If

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_cca_selected_analysis_dataconsumptionmeters"
        CO.Parameters.AddWithValue("@agreement_fk", SM.targetAgreement)
        CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        CO.Dispose()

        If dt.Rows.Count = 1 Then
            Dim meterlist As String = ""

            For Each x As DataRow In dt.Rows
                meterlist &= "," & x("agreementmeterpoint_pk")
            Next
            If meterlist.Length > 0 Then
                meterlist = meterlist.Remove(0, 1)
            End If

            'dataconsumptionsanalysistable(meterlist, datefrom.Text, dateto.Text, chkDataAnalysis.Checked)
            'dataconsumptionsanalysischart(meterlist, datefrom.Text, dateto.Text, chkDataAnalysis.Checked)

            'Set the processing mode for the ReportViewer to Remote
            myReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote

            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer1.ServerReport

            'serverReport.ReportServerCredentials = New clsReportServerCredentials("administrator", "chicago", "uml")

            'Set the report server URL and report path
            serverReport.ReportServerUrl = New Uri("http://reports.mceg.local/ReportServer12")
            serverReport.ReportPath = "/UML/One View/CCA Reports/HMRC Audit Data"

            Dim varMeterList As New Microsoft.Reporting.WebForms.ReportParameter()
            varMeterList.Name = "agreementmeterpoint_pk"
            varMeterList.Values.Add(meterlist)

            Dim varDateFrom As New Microsoft.Reporting.WebForms.ReportParameter()
            varDateFrom.Name = "date_start"
            varDateFrom.Values.Add(datefrom.Text)

            Dim varDateTo As New Microsoft.Reporting.WebForms.ReportParameter()
            varDateTo.Name = "date_end"
            varDateTo.Values.Add(dateto.Text)

            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {varMeterList, varDateFrom, varDateTo}
            serverReport.SetParameters(parameters)

            myReportViewer1.Visible = True

        ElseIf dt.Rows.Count > 1 Then
            AddNotice("setNotice('Only 1 Meter per Audit report');")
        Else
            AddNotice("setNotice('No Meters Selected');")
        End If
    End Sub


    'Private Sub dataconsumptionsanalysistable(ByVal meterlist As String, ByVal date_start As Date, ByVal date_end As Date, incBaseYear As Boolean)

    '    Dim BaseYear As Integer = 0
    '    If incBaseYear Then BaseYear = 1

    '    grdPivot.Columns.Clear()
    '    grdPivot.AutoGenerateColumns = False
    '    grdPivot.BorderStyle = BorderStyle.Solid
    '    grdPivot.BorderWidth = 0

    '    CO = New SqlCommand
    '    CO.CommandType = CommandType.StoredProcedure
    '    CO.CommandText = "rsp_dataconsumptions_comparisonoverview"
    '    CO.Parameters.AddWithValue("@varMeterList", meterlist)
    '    CO.Parameters.AddWithValue("@date_start", date_start)
    '    CO.Parameters.AddWithValue("@date_end", date_end)
    '    CO.Parameters.AddWithValue("@baseyear", BaseYear)
    '    Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
    '    dt = Local.GetInversedDataTable(dt, "date", "meter", "totalconsumption", "", True)

    '    Dim dc As DataColumn

    '    If dt.Rows.Count > 0 Then

    '        'Building all the columns in the table.

    '        For Each dc In dt.Columns
    '            Dim bField As New BoundField
    '            bField.HeaderStyle.BorderStyle = BorderStyle.Solid
    '            bField.HeaderStyle.BorderWidth = 1
    '            bField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
    '            bField.HeaderStyle.BackColor = cDefault
    '            bField.HeaderStyle.ForeColor = Color.White

    '            bField.ItemStyle.BorderStyle = BorderStyle.Solid
    '            bField.ItemStyle.BorderWidth = 1

    '            If dc.ColumnName = "meter" Then
    '                bField.DataField = dc.ColumnName
    '                bField.HeaderText = "Meter"
    '                bField.ItemStyle.Width = "100"
    '                grdPivot.Columns.Add(bField)

    '            ElseIf dc.ColumnName = "total" Then
    '                bField.DataField = dc.ColumnName
    '                bField.HeaderText = dc.ColumnName
    '                bField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
    '                grdPivot.Columns.Add(bField)

    '            Else
    '                bField.DataField = dc.ColumnName
    '                bField.HeaderText = dc.ColumnName
    '                bField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
    '                bField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
    '                grdPivot.Columns.Add(bField)

    '            End If
    '        Next
    '    End If



    '    grdPivot.DataSource = dt
    '    grdPivot.DataBind()
    '    CO.Dispose()


    'End Sub

    'Private Sub dataconsumptionsanalysischart(ByVal meterlist As String, ByVal date_start As Date, ByVal date_end As Date, incBaseYear As Boolean)

    '    Dim BaseYear As Integer = 0
    '    If incBaseYear Then BaseYear = 1

    '    With chartPivot
    '        .Serializer.Reset()
    '        .Visible = True
    '        .Series.Clear()
    '        .Titles.Clear()
    '        .Legends.Clear()
    '        .Legends.Add("Default")

    '        ' legend stuff
    '        .Legends(0).Docking = LegendDocking.Bottom
    '        .Legends(0).Alignment = StringAlignment.Far
    '        .Legends(0).Reversed = AutoBool.False
    '        .Legends(0).Position.Auto = True
    '        .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.SeriesSymbol, ""))
    '        .Legends(0).CellColumns.Add(New LegendCellColumn("", LegendCellColumnType.Text, "#LEGENDTEXT", Drawing.ContentAlignment.MiddleLeft))
    '        ' Zooming
    '        For Each chartarea1 As ChartArea In .ChartAreas
    '            chartarea1.CursorX.UserEnabled = True
    '        Next
    '        ' Setup Frame
    '        .BorderColor = System.Drawing.ColorTranslator.FromHtml("#009530") ' Corp Green
    '        .BorderStyle = ChartDashStyle.Solid
    '        .BorderWidth = 2
    '        .BorderSkin.SkinStyle = BorderSkinStyle.Emboss

    '        .ChartAreas.Add("Default")
    '        .ChartAreas(0).CursorX.UserEnabled = True
    '        .Width = grdPivot.Width
    '        .Height = "450"
    '        .Titles.Add("Data Analysis")
    '        .ChartAreas("Default").AxisY.Title = ""
    '        .ChartAreas("Default").AxisY.LabelStyle.Format = "#,##0.##"
    '        .PaletteCustomColors = colorSet



    '    End With



    '    Dim CN As New SqlConnection(CDC.ConnectionString(0))
    '    CN.Open()
    '    CO = New SqlCommand
    '    CO.Connection = CN
    '    CO.CommandType = CommandType.StoredProcedure
    '    CO.CommandText = "rsp_dataconsumptions_comparisonoverview"
    '    CO.Parameters.AddWithValue("@varMeterList", meterlist)
    '    CO.Parameters.AddWithValue("@date_start", date_start)
    '    CO.Parameters.AddWithValue("@date_end", date_end)
    '    CO.Parameters.AddWithValue("@table", "0")
    '    CO.Parameters.AddWithValue("@baseyear", BaseYear)

    '    Dim reader As SqlDataReader = CO.ExecuteReader
    '    System.Diagnostics.Debug.WriteLine(CDC.CurrentConnectionReader)

    '    chartPivot.Visible = True
    '    chartPivot.DataBindCrossTab(reader, "meter", "date", "totalconsumption", "")

    '    With chartPivot
    '        .Series(0)("PointWidth") = "0.5"
    '        .ChartAreas("Default").AxisX.Margin = False
    '        .ChartAreas("Default").AxisX.Interval = 1
    '        .Series(0)("EmptyPointValue") = "Zero"
    '        .PaletteCustomColors = colorSet

    '        .ChartAreas("Default").AxisX.Title = "Month"

    '        Dim series As Series
    '        For Each series In .Series
    '            series.EmptyPointStyle.Color = Color.Transparent
    '            series.Type = SeriesChartType.Line
    '            series.BorderWidth = 2
    '            series.XValueType = ChartValueTypes.DateTime
    '            series.Type = Dundas.Charting.WebControl.SeriesChartType.Line

    '            'MakeGroupLabels(chartPivot, series, "date")
    '        Next

    '    End With



    'End Sub

    Protected Overloads Sub MakeGroupLabels(ByVal chart As Chart, ByVal series As Series, ByVal ParamArray fields() As String)
        ' This is how many rows of labels we need to make.
        Dim numLabels As Integer = fields.Length

        Dim chartArea As Dundas.Charting.WebControl.ChartArea = chart.ChartAreas(series.ChartArea)
        Dim group As String() = New String(numLabels - 1) {}
        Dim labels As CustomLabel() = New CustomLabel(numLabels - 1) {}

        ' Initialize each row's current label.
        For i As Integer = 0 To numLabels - 1
            group(i) = ""
        Next

        ' For x-value indexed series, count from 1.
        Dim xValue As Integer = 1

        ' Set the PointWidth default value if it's null
        If series("PointWidth") Is Nothing Then
            series("PointWidth") = "0.8"
        End If

        ' Iterate over each point in the series.  Check the group fields and 
        ' update the ending value for each label or start a new label.
        For Each dataPoint As Dundas.Charting.WebControl.DataPoint In series.Points
            For i As Integer = 0 To numLabels - 1
                If group(i) = dataPoint(fields(i)) Then
                    ' Still in the same group.  Update the custom label's end position.
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                Else
                    ' Create a new label here with the group text.
                    group(i) = dataPoint(fields(i))
                    labels(i) = New CustomLabel()
                    labels(i).From = xValue - Double.Parse(series("PointWidth")) / 2
                    labels(i).[To] = xValue + Double.Parse(series("PointWidth")) / 2
                    labels(i).RowIndex = (i + 1)
                    labels(i).Text = group(i)
                    labels(i).LabelMark = LabelMark.LineSideMark
                    chartArea.AxisX.CustomLabels.Add(labels(i))
                End If
            Next
            xValue += 1
        Next
    End Sub

    Public Sub ExportToExcel(ByRef html As String, fileName As String)
        html = html.Replace("&gt;", ">")
        html = html.Replace("&lt;", "<")
        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.AddHeader("content-disposition", (Convert.ToString("attachment;filename=") & fileName) + "_" + DateTime.Now.ToString("M_dd_yyyy_H_M_s") + ".xls")
        HttpContext.Current.Response.ContentType = "application/xls"
        HttpContext.Current.Response.Write(html)
        HttpContext.Current.Response.[End]()
    End Sub


End Class
