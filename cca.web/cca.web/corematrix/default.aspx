<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="corematrix_Default" title="Untitled Page" %>
<%@ Register Src="../controls/nav/matrixrightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        <h3>Matrix Pricing</h3> 
        
        <ul id="innertab">
            <li class='<%=LIi("matrix")%>'>Matrix</li>
            <li class='<%=LIi("price")%>'><a href="default.aspx?pk=<%=PSM.matrix %>&pg=mp">Type</a></li>
            <li class='<%=LIi("detail")%>'><a href="default.aspx?pk=<%=PSM.matrix %>&pg=md">Detail</a></li>
            <li class='<%=LIi("report")%>'><a href="<%=matrixreporturl %>" target="_blank">Reports</a></li>
        </ul>

        <asp:MultiView runat="server" ID="mvDetail">
             <asp:View ID="vwMatrix" runat="server">
                <asp:PlaceHolder ID="MatrixControl" runat="server" />
                <table id="MatrixButton" runat="server">
                    <tr style="height:10px;"></tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveMatrix" Text="<h6>Save Matrix</h6>" CausesValidation="true" />
                        </td>                            
                    </tr>
                </table>
            </asp:View>

            <asp:View ID="vwPrice" runat="server">
            <h5>Current</h5>
            <br />
                <asp:Repeater ID="rptMatrixPrices" runat="server">
                    <HeaderTemplate>
                        <table>
                    </HeaderTemplate> 
                    <FooterTemplate>
                        </table>
                    </FooterTemplate> 
                    <ItemTemplate>
                        <tr id='node_<%# Container.DataItem("matrixprice_pk") %>'>
                            <td>
                                    
                            </td>
                            <td>
                                <%# Container.DataItem("tag")%>
                            </td>
                            <td>
                                <a class="remove" title="Remove"  href='javascript:removePrice(<%# Container.DataItem("matrixprice_pk") %>)'>X</a>                                
                            </td>
                        </tr>
                    </ItemTemplate> 
                </asp:Repeater>
                <br />
                <h5>Add New</h5>
                
                <asp:PlaceHolder ID="PriceControl" runat="server" />
                <table id="PriceButton" runat="server">
                    <tr style="height:10px;"></tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSavePrice" Text="<h6>Add</h6>" CausesValidation="true" />
                        </td>                            
                    </tr>
                </table>
            </asp:View>

            <asp:View ID="vwDetail" runat="server">
                <asp:PlaceHolder ID="DetailControl" runat="server" />
                <table id="DetailButton" runat="server">
                    <tr style="height:10px;"></tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveDetail" Text="<h6>Save</h6>" CausesValidation="true" />
                        </td>                            
                    </tr>
                </table>
            </asp:View>

        </asp:MultiView>
    </div>

    <div class="navigation">
        <uc0:rightnav ID="MatrixNav" runat="server" />
    </div>
</div>

</asp:Content>

