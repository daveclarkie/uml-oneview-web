Partial Class userphone_combo_ctl
    Inherits DaveControl
    Public PSM As MySM

    Public WithEvents ouserphone As Objects.userphones
    Public WithEvents ophone As Objects.phones
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = -1
    Dim _Status As Integer = 0
    Dim _STCurrentPk As Integer = -1
    Dim _STStatus As Integer = 0

    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        ControlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub

    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property

    Public ReadOnly Property STCurrentPk() As Integer
        Get
            Return _STCurrentPk
        End Get
    End Property

    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property

    Public Sub ControlInit()
        CO = New SqlClient.SqlCommand("rkg_contactlocations_lookup")
        CO.CommandType = CommandType.StoredProcedure
        Me.contactlocation_fk.DataSource = CDC.ReadDataTable(CO)
        Me.contactlocation_fk.DataTextField = "value"
        Me.contactlocation_fk.DataValueField = "pk"
        Try
            Me.contactlocation_fk.DataBind()
        Catch Ex As Exception
            Me.contactlocation_fk.SelectedValue = -1
            Me.contactlocation_fk.DataBind()
        End Try
        CO = New SqlClient.SqlCommand("rkg_countrys_lookup")
        CO.CommandType = CommandType.StoredProcedure
        Me.country_fk.DataSource = CDC.ReadDataTable(CO)
        Me.country_fk.DataTextField = "value"
        Me.country_fk.DataValueField = "pk"
        Try
            Me.country_fk.DataBind()
        Catch Ex As Exception
            Me.country_fk.SelectedValue = -1
            Me.country_fk.DataBind()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
    Public Sub SetObjects(ByVal CurrentUser As Integer, ByVal ActualUser As Integer, Optional ByVal pk As Integer = -1)
        If ouserphone Is Nothing Then
            ouserphone = New Objects.userphones(currentuser, actualuser, pk, CDC)
            If ouserphone.phone_fk > 0 Then
                ophone = New Objects.phones(currentuser, actualuser, ouserphone.phone_fk, CDC)
            Else
                ophone = New Objects.phones(currentuser, actualuser, -1, CDC)
            End If
        End If
        If ophone Is Nothing Then
            If ouserphone.phone_fk > 0 Then
                ophone = New Objects.phones(currentuser, actualuser, ouserphone.phone_fk, CDC)
            Else
                ophone = New Objects.phones(currentuser, actualuser, -1, CDC)
            End If
        End If
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1)
        SetObjects(currentuser, actualuser, pk)
        SM.subId = ouserphone.phone_fk
        Try
            Me.contactlocation_fk.SelectedValue = ouserphone.contactlocation_fk
        Catch excontactlocation_fk As Exception
            Me.contactlocation_fk.SelectedValue = -1
        End Try
        Try
            Me.country_fk.SelectedValue = ophone.country_fk
        Catch excountry_fk As Exception
            Me.country_fk.SelectedValue = -1
        End Try
        Me.phonenumber.Text = ophone.phonenumber
        _CurrentPk = ouserphone.userphone_pk
        _STCurrentPk = ophone.phone_pk
        Status = ouserphone.rowstatus Or ophone.rowstatus
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        SetObjects(currentuser, actualuser, pk)
        Dim STResult As Boolean = False
        Try
            ophone.country_fk = Me.country_fk.SelectedValue
            ophone.phonenumber = Me.phonenumber.Text
            STResult = ophone.Save()
            If Not STResult Then Throw ophone.LastError
            _STCurrentPk = ophone.phone_pk
            SM.subid = ophone.phone_pk
        Catch Ex As Exception
            STResult = False
            LastError = Ex
            SM.subid = -1
        End Try
        Try
            ouserphone.user_fk = SM.targetuser
            ouserphone.phone_fk = SM.subId
            ouserphone.contactlocation_fk = Me.contactlocation_fk.SelectedValue
        Catch MainEx As Exception
            LastError = MainEx
        End Try
        Dim result As Boolean = False
        Try
            Result = ouserphone.Save()
            If Not Result Then Throw ouserphone.LastError
            _CurrentPk = ouserphone.userphone_pk
        Catch Ex As Exception
            Result = False
            LastError = Ex
        End Try
        SM.linkId = _CurrentPk
        Status = ouserphone.rowstatus Or ophone.rowstatus
        Return result And STResult
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If ouserphone Is Nothing Then
            ouserphone = New Objects.userphones(currentuser, actualuser, pk, CDC)
        End If
        ouserphone.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If ouserphone Is Nothing Then
            ouserphone = New Objects.userphones(currentuser, actualuser, pk, CDC)
        End If
        ouserphone.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = -1) As Boolean
        If ouserphone Is Nothing Then
            ouserphone = New Objects.userphones(currentuser, actualuser, pk, CDC)
        End If
        ouserphone.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.contactlocation_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkcontactlocation_fk.Visible = IIF(RL >= 0, True, False)
        Me.country_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkcountry_fk.Visible = IIF(RL >= 0, True, False)
        Me.phonenumber.Enabled = IIF(WL >= 0, True, False)
        Me.blkphonenumber.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class
