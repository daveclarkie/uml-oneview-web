Partial Class backend_groupmanagement
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_groupmanagement_populate"
            groupList.DataTextField = "group"
            groupList.DataValueField = "group_pk"
            groupList.DataSource = CDC.ReadDataTable(CO)
            groupList.DataBind()
        End If


        If Not Security.GroupMember(SM.actualuser, SystemGroups.ManageGroups, CDC) Then
            Local.Bounce(SM, Response, "~/default.aspx", "Access+Denied")
        End If

        Me.groupList.Attributes.Add("onChange", "loadMembers('" + Me.groupList.ClientID + "');")



    End Sub

End Class
