Partial Class persons_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents opersons As Objects.persons
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_titles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.title_fk.Items.Count=0 Then
        Me.title_fk.DataSource=CDC.ReadDataTable(CO)
        Me.title_fk.DataTextField = "value"
        Me.title_fk.DataValueField = "pk"
        Try
            Me.title_fk.DataBind
        Catch Ex as Exception
            Me.title_fk.SelectedValue=-1
            Me.title_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.manageruser_fk.Items.Count=0 Then
        Me.manageruser_fk.DataSource=CDC.ReadDataTable(CO)
        Me.manageruser_fk.DataTextField = "value"
        Me.manageruser_fk.DataValueField = "pk"
        Try
            Me.manageruser_fk.DataBind
        Catch Ex as Exception
            Me.manageruser_fk.SelectedValue=-1
            Me.manageruser_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If opersons is Nothing then
opersons = new Objects.persons(currentuser,actualuser,pk,CDC)
End If
Me.title_fk.SelectedValue=opersons.title_fk
Me.forename.Text=opersons.forename
Me.middlename.Text=opersons.middlename
Me.surname.Text=opersons.surname
Me.jobtitle.Text=opersons.jobtitle
Me.organisation_fk.SelectedValue=opersons.organisation_fk
Me.manageruser_fk.SelectedValue=opersons.manageruser_fk
_CurrentPk=opersons.person_pk
Status=opersons.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If opersons is Nothing then
opersons = new Objects.persons(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
opersons.title_fk=Me.title_fk.SelectedValue
opersons.forename=Me.forename.Text
opersons.middlename=Me.middlename.Text
opersons.surname=Me.surname.Text
opersons.jobtitle=Me.jobtitle.Text
opersons.organisation_fk=Me.organisation_fk.SelectedValue
opersons.manageruser_fk=Me.manageruser_fk.SelectedValue
result=opersons.Save()
if not result then throw opersons.LastError
_CurrentPk=opersons.person_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=opersons.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If opersons is Nothing then
   opersons = new Objects.persons(currentuser,actualuser,pk,CDC)
  End If
  opersons.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If opersons is Nothing then
   opersons = new Objects.persons(currentuser,actualuser,pk,CDC)
  End If
  opersons.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If opersons is Nothing then
   opersons = new Objects.persons(currentuser,pk,CDC)
  End If
  opersons.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.title_fk.Enabled=IIF(WL>=0,True,False)
  Me.blktitle_fk.Visible=IIF(RL>=0,True,False)
  Me.forename.Enabled=IIF(WL>=0,True,False)
  Me.blkforename.Visible=IIF(RL>=0,True,False)
  Me.middlename.Enabled=IIF(WL>=0,True,False)
  Me.blkmiddlename.Visible=IIF(RL>=0,True,False)
  Me.surname.Enabled=IIF(WL>=0,True,False)
  Me.blksurname.Visible=IIF(RL>=0,True,False)
  Me.jobtitle.Enabled=IIF(WL>=0,True,False)
  Me.blkjobtitle.Visible=IIF(RL>=0,True,False)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  Me.manageruser_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkmanageruser_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

