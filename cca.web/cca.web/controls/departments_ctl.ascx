<%@ Control Language="VB" AutoEventWireup="false" CodeFile="departments_ctl.ascx.vb" Inherits="departments_ctl" %>
<h5>departments</h5>
<ul class='formcontrol'>
<li runat="server" id="blkdepartmentname">
<span class='label'>Department Name</span>
<asp:TextBox EnableViewState="false" ID="departmentname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkowner_user_fk">
<span class='label'>Owner<span class='pop'>Who is responsible for this department</span></span>
<asp:DropDownList ID="owner_user_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

