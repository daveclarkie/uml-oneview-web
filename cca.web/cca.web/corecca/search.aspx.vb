Partial Class corecca_search
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Public Sub Search()
        SearchAgreements()
    End Sub

    Public Sub SearchAgreements()

        Dim sfAgreement As String = LimitSearchTerms(agreement.Text)
        Dim sfAgreementName As String = LimitSearchTerms(agreementname.Text)
        Dim sfTUID As String = LimitSearchTerms(tuid.Text)
        Dim sfFacility As String = LimitSearchTerms(facility.Text)
        Dim sfOrganisation As String = LimitSearchTerms(organisation.Text)
        Dim sfMeter As String = LimitSearchTerms(meter.Text)
        Dim sfFederation As Integer = ddlfederation.SelectedItem.Value

        If sfAgreement.Length > 0 Or sfAgreementName.Length > 0 Or sfTUID.Length > 0 Or sfFacility.Length > 0 Or sfFederation > 0 Or sfOrganisation.Length > 0 Or sfMeter.Length > 0 Then
            CO = New SqlCommand()
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_agreementsearch"
            CDC = New DataCommon
            CO.Parameters.AddWithValue("@agreement", sfAgreement)
            CO.Parameters.AddWithValue("@agreementname", sfAgreementName)
            CO.Parameters.AddWithValue("@tuid", sfTUID)
            CO.Parameters.AddWithValue("@facility", sfFacility)
            CO.Parameters.AddWithValue("@federation", sfFederation)
            CO.Parameters.AddWithValue("@organisation", sfOrganisation)
            CO.Parameters.AddWithValue("@meter", sfMeter)
            CO.Parameters.AddWithValue("@w", "%")
            CO.Parameters.AddWithValue("@user", SM.currentuser)

            Dim dt As DataTable = CDC.ReadDataTable(CO)
            Dim objPds As New PagedDataSource
            objPds.DataSource = dt.DefaultView
            objPds.AllowPaging = True
            objPds.PageSize = 25
            Dim curpage As Integer

            If ViewState("Page") IsNot Nothing Then
                curpage = Convert.ToInt32(ViewState("Page"))
            Else
                ViewState("Page") = 1
                curpage = 1
            End If

            objPds.CurrentPageIndex = curpage - 1
            lblSearchPage.Text = "Page: " + (curpage).ToString() + " of " + objPds.PageCount.ToString()
            If objPds.PageCount > 1 Then
                lnkSearchNext.Text = " Next "
                lnkSearchPrev.Text = " Previous "
            Else
                lnkSearchNext.Text = ""
                lnkSearchPrev.Text = ""
            End If

            rptAgreement.DataSource = objPds
            rptAgreement.DataBind()

            If objPds.Count = 0 Then AddNotice("setNotice('No agreements matching your criteria were found.');")

            CO.Dispose()
            CO = Nothing
        Else
            AddNotice("setNotice('No search criteria entered');")
        End If

    End Sub

    Protected Sub lnkPrev_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        If ViewState("Page") > 1 Then
            ViewState("Page") = Convert.ToInt32(ViewState("Page")) - 1

            ' Reload control
            Search()
        End If
    End Sub

    Protected Sub lnkNext_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the next page
        ViewState("Page") = Convert.ToInt32(ViewState("Page")) + 1

        ' Reload control
        Search()
    End Sub

    Protected Sub lnkFirst_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        ViewState("Page") = Convert.ToInt32(1)

        ' Reload control
        Search()
    End Sub


    Protected Sub lnkLast_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Set viewstate variable to the previous page
        Dim x As Integer = TotalRowCount
        ViewState("Page") = Convert.ToInt32(PageCount)

        ' Reload control
        Search()
    End Sub

    Private Property TotalRowCount() As Integer
        Get
            Dim o As Object = ViewState("TotalRowCount")
            If o Is Nothing Then
                Return -1
            Else
                Return CInt(o)
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TotalRowCount") = value
        End Set
    End Property
    Private ReadOnly Property PageIndex() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageIndex")) Then
                Return Convert.ToInt32(Request.QueryString("pageIndex"))
            Else
                Return 0
            End If
        End Get
    End Property
    Private ReadOnly Property PageSize() As Integer
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("pageSize")) Then
                Return Convert.ToInt32(Request.QueryString("pageSize"))
            Else
                Return 4
            End If
        End Get
    End Property
    Private ReadOnly Property PageCount() As Integer
        Get
            If TotalRowCount <= 0 OrElse PageSize <= 0 Then
                Return 1
            Else
                Return ((TotalRowCount + PageSize) - 1) / PageSize
            End If
        End Get
    End Property


    Private Sub SetSearchRanges()
        If Not Page.IsPostBack Then

            Try
                ddlfederation.Items.Clear()

                CO = New SqlClient.SqlCommand("rkg_federations_lookup_withagreement")
                CO.CommandType = CommandType.StoredProcedure
                If Me.ddlfederation.Items.Count = 0 Then
                    Me.ddlfederation.DataSource = CDC.ReadDataTable(CO)
                    Me.ddlfederation.DataTextField = "value"
                    Me.ddlfederation.DataValueField = "pk"
                    Try
                        Me.ddlfederation.DataBind()
                    Catch Ex As Exception
                        Me.ddlfederation.SelectedValue = -1
                        Me.ddlfederation.DataBind()
                    End Try
                End If
            Catch ex As Exception
            End Try


        End If
    End Sub


    '================================
    '   "Validation" Procs
    '================================

    Private Function LimitSearchTerms(ByVal original As String) As String
        CDC.Sanitize(original)
        Return original
    End Function

    Private Function LimitSearchTermsNumeric(ByVal original As String) As Integer
        CDC.Sanitize(original)
        Dim i As Integer = 0
        If Integer.TryParse(original, i) Then
            Return i
        Else
            Return 0
        End If
    End Function


    '================================
    '   Page Specific Events
    '================================

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Search()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ViewState("Page") = 1
        Search()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetSearchRanges()
    End Sub

    Public Function returnselect(ByVal rowstatus As Integer, ByVal url As String) As String
        Dim _return As String = ""

        If rowstatus = 0 Then
            _return = "<a href='" & url & Local.noCache & "'><h6>select</h6></a>"
        Else
            _return = "<a href='" & url & Local.noCache & "'><h6 style='background-color:red;'>not managing</h6></a>"
        End If
        Return _return
    End Function


End Class
