<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="bubbles.aspx.vb" Inherits="corecca_bubbles" title="bubbles" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%--<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        
        <h3>Bubbles</h3> 
        
        <ul id="innertab">
            <li class='<%=LIi("bubble")%>'><a href="bubbles.aspx?pk=<%=PSM.targetbubble %>&pg=dt">Details</a></li>
            <li class='<%=LIi("target")%>'><a href="bubbles.aspx?pk=<%=PSM.targetbubble %>&pg=tg">Targets</a></li>
            <li class='<%=LIi("agreement")%>'><a href="bubbles.aspx?pk=<%=PSM.targetbubble %>&pg=ag">Agremeents</a></li>
            <li class='<%=LIi("report")%>'><a href="bubbles.aspx?pk=<%=PSM.targetbubble %>&pg=rp">Reports</a></li>
        </ul>

        <asp:MultiView runat="server" ID="mvDetail">
            <asp:View ID="vwBubble" runat="server">
                    <asp:PlaceHolder ID="BubbleControl" runat="server" />
                <table id="BubbleButton" runat="server">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveBubble" Text="<h6>Save Bubble</h6>" CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </asp:View>

            <asp:View ID="vwBubbleTargets" runat="server">
                <h5>Current Targets <%= selecteditemname()%></h5>
                <asp:Repeater ID="rptBubbleTarget" runat="server">
                    <HeaderTemplate><table>
                    <tr>
                        <td style="width:40px;">
                                
                        </td>
                        <td>
                            <h4>Description</h4>
                        </td>
                        <td>
                            <h4>Start</h4>
                        </td>
                        <td>
                            <h4>End</h4>
                        </td>
                        <td>
                            <h4>Target</h4>
                        </td>
                        <td>
                            <h4>EA Carbon Surplus</h4>
                        </td>
                        <td></td>
                    </tr>
                    </HeaderTemplate> 
                    <ItemTemplate>
                            
                            <%--<a href='Bubbles.aspx?pg=tg&pk=<%# Container.DataItem("Bubble_fk") %>&xt=<%# Container.DataItem("Bubbletarget_pk") %>'></a>--%>
                        <tr id='node_<%# Container.DataItem("bubbletarget_pk") %>'> 
                            <td style="text-align:center;">
                                <a href='bubbles.aspx?pg=tg&pk=<%# sm.targetBubble %>&spk=<%# Container.DataItem("bubbletarget_pk") %>'><h6>edit</h6></a>
                            </td>
                            <td>
                                <%# Container.DataItem("targetdescription")%>
                            </td>
                            <td>
                                <%# Container.DataItem("targetstart")%>
                            </td>
                            <td>
                                <%# Container.DataItem("targetend")%>
                            </td>
                            <td>
                                <%# FormatNumber(Container.DataItem("target"),2)%> %
                            </td>
                            <td>
                                <%# Container.DataItem("eacarbonsurplus")%>
                            </td>
                            <td>
                                <a class="remove" title="Remove"  href='javascript:removeTarget(<%# Container.DataItem("bubbletarget_pk") %>)'>X</a>                                
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr style="height:5px;">
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href='Bubbles.aspx?pg=tg&pk=<%= sm.targetBubble %>&spk=-1'><h6>New Target</h6></a> 
                            </td>
                        </tr>
                    </table>
                    </FooterTemplate> 
                </asp:Repeater>
                    
                <asp:PlaceHolder ID="BubbleTargetControl" runat="server" />            
                <table id="BubbleTargetButton" runat="server">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveBubbleTarget" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton ID="btnCancelBubbleTarget" runat="server" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
            </asp:View>

            <asp:View ID="vwBubbleAgreements" runat="server">
            <h5>Current Agreements <%= selecteditemname()%></h5>
            <asp:Repeater ID="rptBubbleAgreements" runat="server">
                <HeaderTemplate><table>
                <tr>
                    <td style="width:60px;">
                    
                    </td>
                    <td style="width:200px;">
                        <h4>Agreement Name</h4>
                    </td>
                    <td style="width:120px;">
                        <h4>Agreement Number</h4>
                    </td>
                    <td style="width:100px;">
                        <h4>Facility Number</h4>
                    </td>
                    <td style="width:80px;">
                        <h4>TUID</h4>
                    </td>
                    <td></td>
                </tr>
            </HeaderTemplate> 
            <ItemTemplate>
                    <tr id='node_<%# Container.DataItem("bubbleagreement_pk") %>'>
                    <td style="text-align:center;">
                        <%# returnselect(Container.DataItem("url"))%>
                    </td>
                    <td style="text-indent:5px;">
                        <%# Container.DataItem("agreementname")%> 
                    </td>
                    <td style="text-indent:5px;">
                        <%# Container.DataItem("agreementnumber")%> 
                    </td>
                    <td style="text-indent:5px;">
                        <%# Container.DataItem("facilitynumber")%> 
                    </td>
                    <td style="text-indent:5px;">
                        <%# Container.DataItem("tuid")%> 
                    </td>
                    <td style="text-indent:0px;">
                        <a class="remove" title="Remove"  href='javascript:removeAgreement(<%# Container.DataItem("bubbleagreement_pk") %>)'>X</a>                                
                    </td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                    <tr style="height:5px;">
                    </tr>
                </table>
                <table style="width:150px; text-align:center;">
                    <tr>
                        <td>
                            <a href='Bubbles.aspx?pg=ag&pk=<%= sm.targetBubble %>&spk=-1'><h6>Attach New Agreemment</h6></a> 
                        </td>
                    </tr>
                </table>
            </FooterTemplate> 
            </asp:Repeater>
                    
            <asp:PlaceHolder ID="BubbleAgreementControl" runat="server" />
            <table id="BubbleAgreementButton" runat="server">
                <tr style="height:10px;">
                            
                </tr>
                <tr>
                    <td style="text-align:center; width:20%;">
                        <asp:Linkbutton runat="server" ID="btnSaveBubbleAgreement" Text="<h6>Save</h6>" /> 
                    </td>
                    <td style="width:60%;"></td>
                    <td style="text-align:center; width:20%;">
                        <asp:Linkbutton ID="btnCancelBubbleAgreement" runat="server" Text="<h4>Cancel</h4>" />
                    </td>
                </tr>
            </table>
            </asp:View>

            <asp:View ID="vwBubbleReports" runat="server">
                <h3>Milestone Tracking Service Report (MTS)</h3>
                
                <table>
                    <tr>
                        <td style="width:150px;">
                            <b>Select Target</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddltarget" runat="server" AutoPostBack="true" Width="200px"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <b>Reporting Period</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlperiod" runat="server" AutoPostBack="true" Width="200px"></asp:DropDownList>
                        </td>
                    </tr>   
                                             
                        <tr>
                            <td style="height:5px;">
                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td valign="top">
                                <b>Data Status</b>
                            </td>
                            <td>
                                <asp:Repeater ID="rptReportDataCheck" runat="server" onitemdatabound="rptReportDataCheck_ItemDataBound">
                                    
                                    <HeaderTemplate><table>
                                        <tr>
                                            <td style="width:120px;">
                                                <h4>Agreement</h4>
                                            </td>
                                            <td style="width:120px;">
                                                <h4>Type</h4>
                                            </td>
                                            <td style="width:120px;">
                                                <h4>Name</h4>
                                            </td>
                                            <td style="width:80px;">
                                                <h4>Latest Data</h4>
                                            </td>
                                            <td style="width:80px;">
                                                <h4>OK to Run?</h4>
                                            </td>
                                        </tr>
                                    </HeaderTemplate> 
                                    <ItemTemplate>
                                        <tr style='background-color:<%# Container.DataItem("background")%> ;color:<%# Container.DataItem("colour")%>'>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("agreementname")%> 
                                            </td>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("fuel")%> 
                                            </td>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("meter")%> 
                                            </td>
                                            <td style="text-indent:5px;">
                                                <%# Container.DataItem("max_date")%> 
                                            </td>
                                            <td style="text-indent:0px;">
                                                <%# Container.DataItem("status")%> 
                                            </td>
                                        </tr>
                                    </ItemTemplate> 
                                    <FooterTemplate>
                                            <tr>
                                                <td colspan="5">
                                                    <asp:Label ID="lblEmptyData" Text="All data is up to date for selected period" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                        <tr>
                            <td style="height:5px;">
                            
                            </td>
                        </tr>
                        
                       <tr>
                        <td></td>
                        <td>
                            <asp:Linkbutton runat="server" ID="btnRunReport" Text="<h4>Run Report</h4>" Enabled="false" Width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Linkbutton runat="server" ID="btnExportInvoice" Text="<h4>Export Invoice Data</h4>" Enabled="false" Width="200px" />
                        </td>
                    </tr>
                </table>
                    <br />




                    <br />
                    <h3>Error Reporting</h3>
                    <asp:Panel ID="panelError" runat="server">
                        <table>
                            <tr>
                                <td style="width:150px;">
                                    <b>Description</b>
                                </td>
                                <td>
                                    <asp:TextBox EnableViewState="false" ID="txtErrorDescription" runat="server" cssClass="input_dtm" Width="297px" Height="100px" TextMode="MultiLine" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Linkbutton runat="server" ID="btnLogError" Text="<h6>Log Error</h6>" Enabled="true" Width="200px" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="panelErrorResponse" runat="server" Visible="false">
                        <asp:Label ID="lblformResponse" runat="server"></asp:Label>
                    </asp:Panel>
                    
                <rsweb:ReportViewer ID="myReportViewer" Height="500px" AsyncRendering="true" ShowPrintButton="false" runat="server" Width="400px" ShowRefreshButton="false" ShowToolBar="true" ShowZoomControl="false" ShowParameterPrompts="false" ShowBackButton="false" ShowCredentialPrompts="false" ShowFindControls="false" ShowPageNavigationControls="false" ShowPromptAreaButton="false">
                </rsweb:ReportViewer> 
            </asp:View>
        </asp:MultiView>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="BubbleNav" runat="server" />

    </div>

</div>

</asp:Content>

