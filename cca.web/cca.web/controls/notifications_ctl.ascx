<%@ Control Language="VB" AutoEventWireup="false" CodeFile="notifications_ctl.ascx.vb" Inherits="notifications_ctl" %>
<h5>notifications</h5>
<ul class='formcontrol'>
<li runat="server" id="blktitle">
<span class='label'>title</span>
<asp:TextBox EnableViewState="false" ID="title" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmessage">
<span class='label'>message</span>
<asp:TextBox EnableViewState="false" ID="message" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkpriority">
<span class='label'>priority</span>
<asp:TextBox EnableViewState="false" ID="priority" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkapproved">
<span class='label'>approved</span>
<asp:CheckBox ID="approved" runat="server" cssClass="input_chk" />
</li>
</ul>

