Partial Class appointments_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oappointments As Objects.appointments
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_organisations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.organisation_fk.Items.Count=0 Then
        Me.organisation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.organisation_fk.DataTextField = "value"
        Me.organisation_fk.DataValueField = "pk"
        Try
            Me.organisation_fk.DataBind
        Catch Ex as Exception
            Me.organisation_fk.SelectedValue=-1
            Me.organisation_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_sites_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.site_fk.Items.Count=0 Then
        Me.site_fk.DataSource=CDC.ReadDataTable(CO)
        Me.site_fk.DataTextField = "value"
        Me.site_fk.DataValueField = "pk"
        Try
            Me.site_fk.DataBind
        Catch Ex as Exception
            Me.site_fk.SelectedValue=-1
            Me.site_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_appointmenttypes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.appointmenttype_fk.Items.Count=0 Then
        Me.appointmenttype_fk.DataSource=CDC.ReadDataTable(CO)
        Me.appointmenttype_fk.DataTextField = "value"
        Me.appointmenttype_fk.DataValueField = "pk"
        Try
            Me.appointmenttype_fk.DataBind
        Catch Ex as Exception
            Me.appointmenttype_fk.SelectedValue=-1
            Me.appointmenttype_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_locations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.location_fk.Items.Count=0 Then
        Me.location_fk.DataSource=CDC.ReadDataTable(CO)
        Me.location_fk.DataTextField = "value"
        Me.location_fk.DataValueField = "pk"
        Try
            Me.location_fk.DataBind
        Catch Ex as Exception
            Me.location_fk.SelectedValue=-1
            Me.location_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg__lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.appointmentstatus_fk.Items.Count=0 Then
        Me.appointmentstatus_fk.DataSource=CDC.ReadDataTable(CO)
        Me.appointmentstatus_fk.DataTextField = "value"
        Me.appointmentstatus_fk.DataValueField = "pk"
        Try
            Me.appointmentstatus_fk.DataBind
        Catch Ex as Exception
            Me.appointmentstatus_fk.SelectedValue=-1
            Me.appointmentstatus_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_appointmentpurposes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.appointmentpurpose_fk.Items.Count=0 Then
        Me.appointmentpurpose_fk.DataSource=CDC.ReadDataTable(CO)
        Me.appointmentpurpose_fk.DataTextField = "value"
        Me.appointmentpurpose_fk.DataValueField = "pk"
        Try
            Me.appointmentpurpose_fk.DataBind
        Catch Ex as Exception
            Me.appointmentpurpose_fk.SelectedValue=-1
            Me.appointmentpurpose_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_phones_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.phone_fk.Items.Count=0 Then
        Me.phone_fk.DataSource=CDC.ReadDataTable(CO)
        Me.phone_fk.DataTextField = "value"
        Me.phone_fk.DataValueField = "pk"
        Try
            Me.phone_fk.DataBind
        Catch Ex as Exception
            Me.phone_fk.SelectedValue=-1
            Me.phone_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oappointments is Nothing then
oappointments = new Objects.appointments(currentuser,actualuser,pk,CDC)
End If
Me.organisation_fk.SelectedValue=oappointments.organisation_fk
Me.site_fk.SelectedValue=oappointments.site_fk
Me.appointmenttype_fk.SelectedValue=oappointments.appointmenttype_fk
Me.appointment.Text=oappointments.appointment.ToString("dd MMM yyyy")
If Me.appointment.Text="01 Jan 0001" then Me.appointment.Text=""
Me.appointment.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.appointment.Attributes.Add("onBlur", "clearNotice()")
Me.location_fk.SelectedValue=oappointments.location_fk
Me.appointmentstatus_fk.SelectedValue=oappointments.appointmentstatus_fk
Me.appointmentpurpose_fk.SelectedValue=oappointments.appointmentpurpose_fk
Me.appointmentpurposeother.Text=oappointments.appointmentpurposeother
Me.phone_fk.SelectedValue=oappointments.phone_fk
_CurrentPk=oappointments.appointment_pk
Status=oappointments.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oappointments is Nothing then
oappointments = new Objects.appointments(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oappointments.organisation_fk=Me.organisation_fk.SelectedValue
oappointments.site_fk=Me.site_fk.SelectedValue
oappointments.appointmenttype_fk=Me.appointmenttype_fk.SelectedValue
oappointments.appointment=CommonFN.CheckEmptyDate(Me.appointment.Text)
oappointments.location_fk=Me.location_fk.SelectedValue
oappointments.appointmentstatus_fk=Me.appointmentstatus_fk.SelectedValue
oappointments.appointmentpurpose_fk=Me.appointmentpurpose_fk.SelectedValue
oappointments.appointmentpurposeother=Me.appointmentpurposeother.Text
oappointments.phone_fk=Me.phone_fk.SelectedValue
result=oappointments.Save()
if not result then throw oappointments.LastError
_CurrentPk=oappointments.appointment_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oappointments.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointments is Nothing then
   oappointments = new Objects.appointments(currentuser,actualuser,pk,CDC)
  End If
  oappointments.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointments is Nothing then
   oappointments = new Objects.appointments(currentuser,actualuser,pk,CDC)
  End If
  oappointments.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oappointments is Nothing then
   oappointments = new Objects.appointments(currentuser,pk,CDC)
  End If
  oappointments.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.organisation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkorganisation_fk.Visible=IIF(RL>=0,True,False)
  Me.site_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksite_fk.Visible=IIF(RL>=0,True,False)
  Me.appointmenttype_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkappointmenttype_fk.Visible=IIF(RL>=0,True,False)
  Me.appointment.Enabled=IIF(WL>=0,True,False)
  Me.blkappointment.Visible=IIF(RL>=0,True,False)
  Me.location_fk.Enabled=IIF(WL>=0,True,False)
  Me.blklocation_fk.Visible=IIF(RL>=0,True,False)
  Me.appointmentstatus_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkappointmentstatus_fk.Visible=IIF(RL>=0,True,False)
  Me.appointmentpurpose_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkappointmentpurpose_fk.Visible=IIF(RL>=0,True,False)
  Me.appointmentpurposeother.Enabled=IIF(WL>=0,True,False)
  Me.blkappointmentpurposeother.Visible=IIF(RL>=0,True,False)
  Me.phone_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkphone_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

