﻿Partial Class coresurvey_default
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        CO = New SqlClient.SqlCommand("rsp_surveys_summarytable")
        CO.CommandType = CommandType.StoredProcedure
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptSurveySummary.DataSource = dt
        rptSurveySummary.DataBind()
        CO.Dispose()
        rptSurveySummary.Visible = True

    End Sub

    Protected Sub rptSurveySummary_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If rptSurveySummary.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyData"), Label)
                lblFooter.Visible = True
            End If
        End If
    End Sub
End Class
