﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="coresurvey_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<h3>Survey Summary</h3> 
<br />

<asp:Repeater ID="rptSurveySummary" runat="server" onitemdatabound="rptSurveySummary_ItemDataBound">
    <HeaderTemplate><table>
        <tr>
            <td style="width:200px;">
                <h4>Survey Name</h4>
            </td>
            <td style="width:80px;">
                <h4>Open</h4>
            </td>
            <td style="width:80px;">
                <h4>Cancelled</h4>
            </td>
            <td style="width:80px;">
                <h4>Complete</h4>
            </td>
        </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <a href='details.aspx?survey=<%# Container.DataItem("surveytype_pk") %>'><%# Container.DataItem("surveytypename")%></a>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("inprogress")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("cancelled")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("complete")%>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyData" Text="No Surveys Active" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>





</asp:Content>

