<%@ Page Language="VB" AutoEventWireup="false" CodeFile="credits.aspx.vb" Inherits="credits" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Credits</title>
    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Droid+Sans:400,700);

        * { padding: 0; margin: 0; }

        body, html
        {
	        width: 100%;
	        height: 100%;
	        font-family: "Droid Sans", arial, verdana, sans-serif;
	        font-weight: 700;
	        color: #ff6;
	        background-color: #000;
	        overflow: hidden;
	        background:black url("design/images/background.png");
        }
        
        p#start
        {
	        position: relative;
	        width: 16em;
	        font-size: 200%;
	        font-weight: 400;
	        margin: 20% auto;
	        color: #4ee;
	        opacity: 0;
	        z-index: 1;
	        -webkit-animation: intro 2s ease-out;
	        -moz-animation: intro 2s ease-out;
	        -ms-animation: intro 2s ease-out;
	        -o-animation: intro 2s ease-out;
	        animation: intro 2s ease-out;
        }

        @-webkit-keyframes intro {
	        0% { opacity: 1; }
	        90% { opacity: 1; }
	        100% { opacity: 0; }
        }

        @-moz-keyframes intro {
	        0% { opacity: 1; }
	        90% { opacity: 1; }
	        100% { opacity: 0; }
        }

        @-ms-keyframes intro {
	        0% { opacity: 1; }
	        90% { opacity: 1; }
	        100% { opacity: 0; }
        }

        @-o-keyframes intro {
	        0% { opacity: 1; }
	        90% { opacity: 1; }
	        100% { opacity: 0; }
        }

        @keyframes intro {
	        0% { opacity: 1; }
	        90% { opacity: 1; }
	        100% { opacity: 0; }
        }

        h1
        {
	        position: absolute;
	        width: 2.6em;
	        left: 50%;
	        top: 25%;
	        font-size: 10em;
	        text-align: center;
	        margin-left: -1.3em;
	        line-height: 0.8em;
	        letter-spacing: -0.05em;
	        color: #000;
	        text-shadow: -2px -2px 0 #ff6, 2px -2px 0 #ff6, -2px 2px 0 #ff6, 2px 2px 0 #ff6;
	        opacity: 0;
	        z-index: 1;
	        -webkit-animation: logo 5s ease-out 2.5s;
	        -moz-animation: logo 5s ease-out 2.5s;
	        -ms-animation: logo 5s ease-out 2.5s;
	        -o-animation: logo 5s ease-out 2.5s;
	        animation: logo 5s ease-out 2.5s;
        }

        h1 sub
        {
	        display: block;
	        font-size: 0.3em;
	        letter-spacing: 0;
	        line-height: 0.8em;
        }

        @-webkit-keyframes logo {
	        0% { -webkit-transform: scale(1); opacity: 1; }
	        50% { opacity: 1; }
	        100% { -webkit-transform: scale(0.1); opacity: 0; }
        }

        @-moz-keyframes logo {
	        0% { -moz-transform: scale(1); opacity: 1; }
	        50% { opacity: 1; }
	        100% { -moz-transform: scale(0.1); opacity: 0; }
        }

        @-ms-keyframes logo {
	        0% { -ms-transform: scale(1); opacity: 1; }
	        50% { opacity: 1; }
	        100% { -ms-transform: scale(0.1); opacity: 0; }
        }

        @-o-keyframes logo {
	        0% { -o-transform: scale(1); opacity: 1; }
	        50% { opacity: 1; }
	        100% { -o-transform: scale(0.1); opacity: 0; }
        }

        @keyframes logo {
	        0% { transform: scale(1); opacity: 1; }
	        50% { opacity: 1; }
	        100% { transform: scale(0.1); opacity: 0; }
        }

        /* the interesting 3D scrolling stuff */
        #titles
        {
	        position: absolute;
	        width: 18em;
	        height: 50em;
	        bottom: 0;
	        left: 50%;
	        margin-left: -9em;
	        font-size: 350%;
	        text-align: justify;
	        overflow: hidden;
	        -webkit-transform-origin: 50% 100%;
	        -moz-transform-origin: 50% 100%;
	        -ms-transform-origin: 50% 100%;
	        -o-transform-origin: 50% 100%;
	        transform-origin: 50% 100%;
	        -webkit-transform: perspective(300px) rotateX(25deg);
	        -moz-transform: perspective(300px) rotateX(25deg);
	        -ms-transform: perspective(300px) rotateX(25deg);
	        -o-transform: perspective(300px) rotateX(25deg);
	        transform: perspective(300px) rotateX(25deg);
        }

        #titles:after
        {
	        position: absolute;
	        content: ' ';
	        left: 0;
	        right: 0;
	        top: 0;
	        bottom: 60%;
	        background-image: -webkit-linear-gradient(top, rgba(0,0,0,1) 0%, transparent 100%);
	        background-image: -moz-linear-gradient(top, rgba(0,0,0,1) 0%, transparent 100%);
	        background-image: -ms-linear-gradient(top, rgba(0,0,0,1) 0%, transparent 100%);
	        background-image: -o-linear-gradient(top, rgba(0,0,0,1) 0%, transparent 100%);
	        background-image: linear-gradient(top, rgba(0,0,0,1) 0%, transparent 100%);
	        pointer-events: none;
        }

        #titles p
        {
	        text-align: justify;
	        margin: 0.8em 0;
        }

        #titles p.center
        {
	        text-align: center;
        }

        #titles a
        {
	        color: #ff6;
	        text-decoration: underline;
        }

        #titlecontent
        {
	        position: absolute;
	        top: 100%;
	        -webkit-animation: scroll 100s linear 4s infinite;
	        -moz-animation: scroll 100s linear 4s infinite;
	        -ms-animation: scroll 100s linear 4s infinite;
	        -o-animation: scroll 100s linear 4s infinite;
	        animation: scroll 100s linear 4s infinite;
        }

        /* animation */
        @-webkit-keyframes scroll {
	        0% { top: 100%; }
	        100% { top: -170%; }
        }

        @-moz-keyframes scroll {
	        0% { top: 100%; }
	        100% { top: -170%; }
        }

        @-ms-keyframes scroll {
	        0% { top: 100%; }
	        100% { top: -170%; }
        }

        @-o-keyframes scroll {
	        0% { top: 100%; }
	        100% { top: -170%; }
        }

        @keyframes scroll {
	        0% { top: 100%; }
	        100% { top: -170%; }	    
        }    
        
	    #warning {
		    color:rgb(75,213,238);
		    font-family:sans-serif;
		    font-size:24px;
		    line-height:1.5;
		    text-align:center;
		    width:80%;
		    margin:25% auto;
		    display:block;
	    }
	
	    #warning a:link, #warning a:visited {
		    color:rgb(75,213,238);
		    text-decoration:none;
		    border-bottom:1px dotted rgb(75,213,238);
	    }
	    
	    #warning a:hover, #warning a:active {
		    color:rgb(252,223,43);
		    border-bottom:1px solid rgb(252,223,43);
		    text-decoration:none;
	    }
    </style>
</head>

<body>
    <%--<p id="warning">I�m sorry, your browser doesn�t support the <a href="http://www.w3.org/TR/css3-3d-transforms/"><abbr>CSS</abbr> <abbr>3D</abbr> transforms</a> needed to display this demo. You need <a href="http://www.apple.com/safari/">Safari</a> or a <a href="http://www.chromium.org/getting-involved/dev-channel">dev version</a> of <a href="http://www.google.com/chrome">Chrome</a> to view it.</p>
    --%>
    <p id="start">A short time ago in a government department very, very close&hellip;</p>

    <h1>CCA WARS<sub>Climate Change Agreement</sub></h1>

    <div id="titles">
        <div id="titlecontent">

	        <p class="center">PHASE II<br />A PROGRAM FOR CCA'S</p>
	
            <p>The United Kingdom's Climate Change Programme was launched in November 2000 by the British government in response to its commitment agreed at the 1992 United Nations Conference on Environment and Development (UNCED). The 2000 programme was updated in March 2006 following a review launched in September 2004.</p>

            <p>In 2008, the UK was the world's 9th greatest producer of man-made carbon emissions, producing around 1.8% of the global total generated from fossil fuels.</p>

            <p>On 26 November 2008, after cross-party pressure over several years, led by environmental groups, the Climate Change Act became law. The Act puts in place a framework to achieve a mandatory 80% cut in the UK's carbon emissions by 2050 (compared to 1990 levels), with an intermediate target of between 34% by 2020 which would have risen in the event of a strong deal at the UN Climate Change Conference in Copenhagen.</p>

	        <p>When a climate change levy was introduced in the United Kingdom, the position of energy-intensive industries was considered, given their energy usage, the requirements of the Integrated Pollution Prevention and Control regime and their exposure to international competition. As a result, a 65% discount from the levy was allowed for those sectors that agreed targets for improving their energy efficiency or reducing carbon emissions. The discount will rise to 80% in 2013.</p>
	
	        <p>An 'energy-intensive' sector is one which carries out activities which are listed under Part A1 or A2 headings in Part 1 of Schedule 1 to the Pollution Prevention and Control (England and Wales) Regulations 2000 (Statutory Instrument 2000 No.1973), as amended by the Pollution Prevention and Control (England and Wales) (Amendment) Regulations 2001 (Statutory Instrument 2001 No. 503).</p>
	
	        <p>The regulations cover the ten main energy-intensive sectors of industry (aluminium, cement, ceramics, chemicals, food and drink, foundries, glass, non-ferrous metals, paper, and steel) and over thirty smaller sectors, and in agriculture, livestock units for the intensive rearing of pigs and poultry.</p>
	
            
        </div>
    </div>
	
</body>
</html>
