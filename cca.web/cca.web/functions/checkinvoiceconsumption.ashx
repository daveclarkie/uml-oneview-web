<%@ WebHandler Language="VB" Class="function_checkinvoiceconsumption" %>
Public Class function_checkinvoiceconsumption
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        
        Dim month As Integer = -1
        Dim year As Integer = -1
        Dim agreementmeterpoint As Integer = -1
        
        Try
            month = RF("b").ToString.Trim()
        Catch ex As Exception
            month = -1
        End Try
        Try
            year = RF("c").ToString.Trim()
        Catch ex As Exception
            year = -1
        End Try
        Try
            agreementmeterpoint = SM.targetDataEntry
        Catch ex As Exception
            agreementmeterpoint = -1
        End Try
        
        Dim outputvalue As String = ""
        Dim CO As New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_dataentryinvoicedconsumption_month"
        CO.Parameters.AddWithValue("@agreementmeterpoint_pk", agreementmeterpoint)
        CO.Parameters.AddWithValue("@month", month)
        CO.Parameters.AddWithValue("@year", year)
        CDC.ReadScalarValue(outputvalue, CO)
        
        Try
            Msg = outputvalue
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class