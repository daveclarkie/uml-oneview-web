<%@ Page Language="VB" MasterPageFile="~/design/masters/doc.master" AutoEventWireup="false" CodeFile="matrix1report.aspx.vb" Inherits="corematrix_reports_matrix1report" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h3>Matrix Report</h3>
 
 <table>
    <tr><td>Customer Name</td><td><asp:Label ID="lblCustomer" runat="server" Text="" ></asp:Label></td></tr>
    <tr><td>Matrix Type</td><td><asp:Label ID="lblMatrixType" runat="server" Text="" ></asp:Label></td></tr>
    <tr><td>Report Generated</td><td><asp:Label ID="lblReportGenerated" runat="server" Text="" ></asp:Label></td></tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
    <% 
        Dim dr As DataRow
        For Each dr In dt.Rows
            
            Dim p As New profiles(SM.currentuser, SM.actualuser, dr("profile_fk"), CDC)
            Dim d As New distributions(SM.currentuser, SM.actualuser, dr("distribution_fk"), CDC)
            Dim x As New matrix1details(SM.currentuser, SM.actualuser, dr("matrixdetail_fk"), CDC)
    %>
        

    <%if dr("pricerank") = 1 then %>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:20px;"> <td colspan="2"><hr /></td></tr>
        <tr><td>Meter</td><td><%=dr("meter") %></td></tr>
        <tr><td>Distribution</td><td><%=d.distribution_pk%> | <%=d.distribution_name%></td></tr>
        <tr><td>Profile Class</td><td><%=p.code%> | <%=p.description%></td></tr>
    <%If dr("annualcharge_day") = 0 Then%>
        <tr><td>Consumption</td><td><%=x.consumptionsingle%></td></tr>
    <%Else%>
        <tr><td>Day Consumption</td><td><%=x.consumptionday%></td></tr>
        <tr><td>Night Consumption</td><td><%=x.consumptionnight%></td></tr>
        <tr><td>Other Consumption</td><td><%=x.consumptionother%></td></tr>
    <%end if %>
        
        <tr style="height:10px;"></tr>
        
        <tr>
            <td colspan="2">
                <table>
                    <tr style="background-color:#009530; color:#FFFFFF;">
                        <td style="font-weight:bold; width:40px;">Rank</td>
                        <td style="font-weight:bold; width:180px;">Supplier</td>
                        <td style="font-weight:bold; width:100px;">Product</td>
                        <td style="font-weight:bold; width:100px;text-align:center;">Contract Length</td>
                        <td style="font-weight:bold; width:100px;text-align:right;">Standing Charge</td>
                    <%If dr("annualcharge_day") = 0 Then%>
                        <td style="font-weight:bold; width:80px;text-align:right;">Single Rate</td>
                    <%Else%>
                        <td style="font-weight:bold; width:60px;text-align:right;">Day</td>
                        <td style="font-weight:bold; width:60px;text-align:right;">Night</td>
                    <%end if %>
                        <td style="font-weight:bold; width:80px;text-align:right;">Total</td>
                    </tr>       
                    <tr>
                        <td><%=dr("pricerank")%></td>
                        <td><%=dr("supplier")%></td>
                        <td><%=dr("productname")%></td>
                        <td style="text-align:center;"><%=dr("contractlength")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualstandingcharge")).ToString("#,#.00")%></td>
                    <%If dr("annualcharge_day") = 0 Then%>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_single")).ToString("#,#.00")%></td>
                    <%Else%>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_day")).ToString("#,#.00")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_night")).ToString("#,#.00")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_other")).ToString("#,#.00")%></td>
                    <%end if %>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_total")).ToString("#,#.00")%></td>
                    </tr>        
    <%Else%>
           
                    <tr>
                        <td><%=dr("pricerank")%></td>
                        <td><%=dr("supplier")%></td>
                        <td><%=dr("productname")%></td>
                        <td style="text-align:center;"><%=dr("contractlength")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualstandingcharge")).ToString("#,#.00")%></td>
                    <%If dr("annualcharge_day") = 0 Then%>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_single")).ToString("#,#.00")%></td>
                    <%Else%>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_day")).ToString("#,#.00")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_night")).ToString("#,#.00")%></td>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_other")).ToString("#,#.00")%></td>
                    <%end if %>
                        <td style="text-align:right;">� <%=CDec(dr("annualcharge_total")).ToString("#,#.00") %></td>
                    </tr>        


    <%End If%>                         
    <%
    Next
    %>
                </table>
            </td>
        </tr>
    </table>
   
</asp:Content>

