Partial Class users_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ousers As Objects.users
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.readlevel.Text="0"
            Me.writelevel.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ousers is Nothing then
ousers = new Objects.users(currentuser,actualuser,pk,CDC)
End If
Me.username.Text=ousers.username
'Me.encpassword.Text=ousers.encpassword
Me.readlevel.Text=ousers.readlevel
Me.readlevel.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.readlevel.Attributes.Add("onBlur", "clearNotice()")
Me.writelevel.Text=ousers.writelevel
Me.writelevel.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.writelevel.Attributes.Add("onBlur", "clearNotice()")
Me.disconnect.Checked=ousers.disconnect
_CurrentPk=ousers.user_pk
Status=ousers.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ousers is Nothing then
ousers = new Objects.users(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ousers.username=Me.username.Text
'ousers.encpassword=Me.encpassword.Text
ousers.readlevel=Me.readlevel.Text
ousers.writelevel=Me.writelevel.Text
ousers.disconnect=Me.disconnect.Checked
result=ousers.Save()
if not result then throw ousers.LastError
_CurrentPk=ousers.user_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ousers.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousers is Nothing then
   ousers = new Objects.users(currentuser,actualuser,pk,CDC)
  End If
  ousers.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousers is Nothing then
   ousers = new Objects.users(currentuser,actualuser,pk,CDC)
  End If
  ousers.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousers is Nothing then
   ousers = new Objects.users(currentuser,pk,CDC)
  End If
  ousers.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.username.Enabled=IIF(WL>=0,True,False)
  Me.blkusername.Visible=IIF(RL>=0,True,False)
  Me.encpassword.Enabled=IIF(WL>=0,True,False)
  Me.blkencpassword.Visible=IIF(RL>=0,True,False)
  Me.readlevel.Enabled=IIF(WL>=0,True,False)
  Me.blkreadlevel.Visible=IIF(RL>=0,True,False)
  Me.writelevel.Enabled=IIF(WL>=0,True,False)
  Me.blkwritelevel.Visible=IIF(RL>=0,True,False)
  Me.disconnect.Enabled=IIF(WL>=0,True,False)
  Me.blkdisconnect.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

