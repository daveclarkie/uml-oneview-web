<%@ Control Language="VB" AutoEventWireup="false" CodeFile="groups_ctl.ascx.vb" Inherits="groups_ctl" %>
<h5>groups</h5>
<ul class='formcontrol'>
<li runat="server" id="blkgroupname">
<span class='label'>groupname</span>
<asp:TextBox EnableViewState="false" ID="groupname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkorganisation_fk">
<span class='label'>organisation_fk</span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkparentgroup_fk">
<span class='label'>parentgroup_fk</span>
<asp:DropDownList ID="parentgroup_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

