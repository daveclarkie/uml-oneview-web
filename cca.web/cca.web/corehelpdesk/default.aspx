﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="corehelpdesk_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<h3>Helpdesk</h3> 
<br />

<asp:Repeater ID="rptHelpdeskTemplates" runat="server" onitemdatabound="rptHelpdeskTemplates_ItemDataBound">
    <HeaderTemplate><table>
        <tr>
            <td style="width:75px;">
                
            </td>
            <td style="width:300px;">
                <h4>Site</h4>
            </td>
            <td style="width:100px;">
                <h4>Tickets</h4>
            </td>
            <td style="width:350px;">
            </td>
        </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr>
            <td style="text-align:center;">
                
                <a href='list.aspx?site=<%# Container.DataItem("site")%>'><h6>select</h6></a>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("site")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("tickets")%>
            </td>
            <td>
                
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyData" Text="No Helpdesk Templates Available" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>





</asp:Content>

