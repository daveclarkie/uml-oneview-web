Public Class SessionManagerOld

    ' Formatting Items


    Shared Function Display(ByVal displaydate As DateTime, Optional ByVal isEnd As Integer = 0) As String
        Return displaydate.ToString("dd MMM yyyy")
    End Function

    Shared Function Display(ByVal obj As DBNull, Optional ByVal isEnd As Integer = 0) As String
        Select Case isEnd
            Case 1
                Return "No date last occupied"
            Case Else
                Return "No date first occupied"
        End Select
    End Function

    Shared Function Wrong(ByVal dt As DateTime, ByVal LocUsage As Integer) As String
        Select Case LocUsage
            Case -1
                Return "badinfo"
            Case Else
                If dt > Now Or dt < Now.AddYears(-120) Then
                    Return "badinfo"
                Else
                    Return ""
                End If
        End Select
    End Function

    Shared Function Wrong(ByVal dt As DBNull, ByVal LocUsage As Integer) As String
        Return "badinfo"
    End Function

    Shared Function DueDate(ByVal Start As DateTime, ByVal Unit As Integer, ByVal duration As Integer) As DateTime
        Select Case Unit
            Case 13 'minute
                Return Start.AddMinutes(duration)
            Case 14 'hour
                Return Start.AddHours(duration)
            Case 15 'day
                Return Start.AddDays(duration)
            Case 16 'weekday
                Return Start.AddDays(CInt(duration * (5 / 7)))
            Case 17 'week
                Return Start.AddDays(duration * 7)
            Case 18 'month
                Return Start.AddMonths(duration)
            Case 19 'quarter
                Return Start.AddMonths(duration * 3)
            Case 20 'year
                Return Start.AddYears(duration)
            Case Else
                Return Start
        End Select
    End Function

    Shared Function DateOnly(ByVal Start As DateTime) As DateTime
        Dim ts As New TimeSpan(0, Start.Hour, Start.Minute, Start.Second, Start.Millisecond)
        Return Start.Subtract(ts)
    End Function


    ' Common functions


    Shared ReadOnly Property noCache() As String
        Get
            Return "&nc=" & Now.Ticks.ToString
        End Get
    End Property

    Shared ReadOnly Property ImgPath() As String
        Get
            Return "http://" & HttpContext.Current.Request.ServerVariables("HTTP_HOST") & "/design/images/"
        End Get
    End Property

    Shared ReadOnly Property BaseUrl() As String
        Get
            Return "http://" & HttpContext.Current.Request.ServerVariables("HTTP_HOST") & "/"
        End Get
    End Property

    ' Session values

    Shared Property HolidayView() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("HolidayView"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("HolidayView") = value
        End Set
    End Property


    Shared Property InvoicePageMode() As InvoicePageMode
        Get
            Return IntExists(HttpContext.Current.Session.Contents("InvoicePageMode"))
        End Get
        Set(ByVal value As InvoicePageMode)
            HttpContext.Current.Session.Contents("InvoicePageMode") = value
        End Set
    End Property

    Shared ReadOnly Property PersonPageModeValue() As String
        Get
            Select Case PersonPageMode
                Case PersonPageMode.Email : Return "ml"
                Case PersonPageMode.Location : Return "lc"
                Case PersonPageMode.Person : Return "ps"
                Case PersonPageMode.Phone : Return "ph"
                Case Else : Return ""
            End Select
        End Get
    End Property

    Shared Property PersonPageMode() As PersonPageMode
        Get
            Return IntExists(HttpContext.Current.Session.Contents("PersonPageMode"))
        End Get
        Set(ByVal value As PersonPageMode)
            HttpContext.Current.Session.Contents("PersonPageMode") = value
        End Set
    End Property

    Shared Property Message() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Message"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Message") = value
        End Set
    End Property

    Shared Property UserMessage() As Integer
        Get
            Return IIf(IntExists(HttpContext.Current.Session.Contents("UserMessage")) = 0, -1, IntExists(HttpContext.Current.Session.Contents("UserMessage")))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserMessage") = value
        End Set
    End Property

    Shared Property CurrentUser() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("CurrentUser"))
        End Get
        Set(ByVal value As Integer)
            If value <> HttpContext.Current.Session.Contents("CurrentUser") Then
                HttpContext.Current.Session.Contents("CurrentUser") = value
                ClearCurrentUser()
            End If
        End Set
    End Property

    Shared Property User() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("User"))
        End Get
        Set(ByVal value As Integer)
            If value <> HttpContext.Current.Session.Contents("User") Then
                HttpContext.Current.Session.Contents("User") = value
                ClearSelectedUser()
            End If
        End Set
    End Property

    Shared Property TaskAsk() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("TaskAsk"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("TaskAsk") = value
        End Set
    End Property

    Shared Property Job() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Job"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Job") = value
        End Set
    End Property

    Shared Property UserJob() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserJob"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserJob") = value
        End Set
    End Property

    Shared Property OptIn() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("OptIn"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("OptIn") = value
        End Set
    End Property

    Shared Property UserOptIn() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserOptIn"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserOptIn") = value
        End Set
    End Property

    Shared Property Income() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Income"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Income") = value
        End Set
    End Property

    Shared Property UserIncome() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserIncome"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserIncome") = value
        End Set
    End Property

    Shared Property Bank() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Bank"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Bank") = value
        End Set
    End Property

    Shared Property UserBank() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserBank"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserBank") = value
        End Set
    End Property

    Shared Property UserName() As String
        Get
            Return StrExists(HttpContext.Current.Session.Contents("UserName"))
        End Get
        Set(ByVal value As String)
            If value <> HttpContext.Current.Session.Contents("UserName") Then
                HttpContext.Current.Session.Contents("UserName") = value
            End If
        End Set
    End Property

    Shared Property TaskInvoiceRef() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("TaskInvoiceRef"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("TaskInvoiceRef") = value
        End Set
    End Property

    Shared Property UserInvoice() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserInvoice"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserInvoice") = value
        End Set
    End Property

    Shared Property Person() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Person"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Person") = value
        End Set
    End Property

    Shared Property CurrentUserName() As String
        Get
            Return StrExists(HttpContext.Current.Session.Contents("CurrentUserName"))
        End Get
        Set(ByVal value As String)
            If value <> HttpContext.Current.Session.Contents("CurrentUserName") Then
                HttpContext.Current.Session.Contents("CurrentUserName") = value
            End If
        End Set
    End Property

    Shared Property Location() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Location"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Location") = value
        End Set
    End Property

    Shared Property UserLocation() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserLocation"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserLocation") = value
        End Set
    End Property

    Shared Property Phone() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Phone"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Phone") = value
        End Set
    End Property

    Shared Property UserPhone() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserPhone"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserPhone") = value
        End Set
    End Property

    Shared Property Email() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Email"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Email") = value
        End Set
    End Property

    Shared Property UserEmail() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("UserEmail"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("UserEmail") = value
        End Set
    End Property

    Shared Property Task() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Task"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Task") = value
        End Set
    End Property

    Shared Property TaskInstance() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("TaskInstance"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("TaskInstance") = value
        End Set
    End Property

    Shared Property NotificationTarget() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("NotificationTarget"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("NotificationTarget") = value
        End Set
    End Property

    Shared Property Notification() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Notification"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Notification") = value
        End Set
    End Property

    Shared Property NotificationLog() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("NotificationLog"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("NotificationLog") = value
        End Set
    End Property

    Shared Property Invoice() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Invoice"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Invoice") = value
        End Set
    End Property

    Shared Property InvoiceRef() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("InvoiceRef"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("InvoiceRef") = value
        End Set
    End Property

    Shared Property InvoiceOrganisation() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("InvoiceOrganisation"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("InvoiceOrganisation") = value
        End Set
    End Property

    Shared Property ItemType() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("ItemType"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("ItemType") = value
        End Set
    End Property

    Shared Property ItemTypeName() As String
        Get
            Return StrExists(HttpContext.Current.Session.Contents("ItemTypeName"))
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session.Contents("ItemTypeName") = value
        End Set
    End Property

    Shared Property ItemDetail() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("ItemDetail"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("ItemDetail") = value
        End Set
    End Property

    Shared Property NoticeCount() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("NoticeCount"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("NoticeCount") = value
        End Set
    End Property

    Shared Property TaskCount() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("TaskCount"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("TaskCount") = value
        End Set
    End Property

    Shared Property CurrentUserReadLevel() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("CurrentUserReadLevel"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("CurrentUserReadLevel") = value
        End Set
    End Property

    Shared Property CurrentUserWriteLevel() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("CurrentUserWriteLevel"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("CurrentUserWriteLevel") = value
        End Set
    End Property

    Shared Property Backend() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Backend"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Backend") = value
        End Set
    End Property

    Private Shared Sub ClearCurrentUser()
        HttpContext.Current.Session.Remove("CurrentUserReadLevel")
        HttpContext.Current.Session.Remove("CurrentUserWriteLevel")
        HttpContext.Current.Session.Remove("CurrentUserName")
        ClearSelectedUser()
        ClearMisc()
    End Sub

    Private Shared Sub ClearMisc()
        HttpContext.Current.Session.Remove("NoticeCount")
        HttpContext.Current.Session.Remove("TaskCount")
        HttpContext.Current.Session.Remove("Backend")
        HttpContext.Current.Session.Remove("TaskAsk")
        HttpContext.Current.Session.Remove("TaskInvoiceRef")
        HttpContext.Current.Session.Remove("Task")
        HttpContext.Current.Session.Remove("TaskInstance")

        HttpContext.Current.Session.Remove("NotificationTarget")
        HttpContext.Current.Session.Remove("Notification")
        HttpContext.Current.Session.Remove("NotificationLog")
        HttpContext.Current.Session.Remove("Message")
    End Sub

    Private Shared Sub ClearSelectedUser()
        HttpContext.Current.Session.Remove("UserBank")
        HttpContext.Current.Session.Remove("UserJob")
        HttpContext.Current.Session.Remove("UserIncome")
        HttpContext.Current.Session.Remove("UserOptIn")
        HttpContext.Current.Session.Remove("UserInvoice")
        HttpContext.Current.Session.Remove("UserMessage")
        HttpContext.Current.Session.Remove("UserPhone")
        HttpContext.Current.Session.Remove("UserEmail")
        HttpContext.Current.Session.Remove("UserName")
        HttpContext.Current.Session.Remove("UserLocation")

        HttpContext.Current.Session.Remove("Bank")
        HttpContext.Current.Session.Remove("Job")
        HttpContext.Current.Session.Remove("OptIn")
        HttpContext.Current.Session.Remove("Income")
        HttpContext.Current.Session.Remove("Person")
        HttpContext.Current.Session.Remove("Location")
        HttpContext.Current.Session.Remove("Phone")
        HttpContext.Current.Session.Remove("Email")
        HttpContext.Current.Session.Remove("Invoice")
        HttpContext.Current.Session.Remove("InvoiceRef")
        HttpContext.Current.Session.Remove("InvoiceOrganisation")
        HttpContext.Current.Session.Remove("ItemType")
        HttpContext.Current.Session.Remove("ItemTypeName")
        HttpContext.Current.Session.Remove("ItemDetail")
    End Sub

    Private Shared Function IntExists(ByVal obj As Object) As Integer
        If obj Is Nothing Then
            Return 0
        Else
            Return obj
        End If
    End Function

    Private Shared Function StrExists(ByVal obj As Object) As String
        If obj Is Nothing Then
            Return ""
        Else
            Return obj
        End If
    End Function

    Shared Property Redirected() As Integer
        Get
            Return IntExists(HttpContext.Current.Session.Contents("Redirected"))
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session.Contents("Redirected") = value
        End Set
    End Property

    Public Shared Sub Bounce(ByVal DestURL As String)
        Try
            HttpContext.Current.Response.Redirect(DestURL)
            HttpContext.Current.Response.End()
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub FOAD(ByVal DestURL As String)
        Try
            If Redirected = 1 Then Exit Sub
            CurrentUser = 0
            Redirected = 1
            HttpContext.Current.Response.Redirect(DestURL)
            HttpContext.Current.Response.End()
        Catch ex As Exception
        End Try
    End Sub


End Class
