﻿// JScript File

/*
   Provide the XMLHttpRequest constructor for IE 5.x-6.x:
   Other browsers (including IE 7.x-9.x) do not redefine
   XMLHttpRequest if it already exists.
 
   This example is based on findings at:
   http://blogs.msdn.com/xmlteam/archive/2006/10/23/using-the-right-version-of-msxml-in-internet-explorer.aspx
*/

if (typeof XMLHttpRequest == "undefined")
  XMLHttpRequest = function () {
    try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); }
      catch (e) {}
    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
      catch (e) {}
    try { return new ActiveXObject("Microsoft.XMLHTTP"); }
      catch (e) {}
    //Microsoft.XMLHTTP points to Msxml2.XMLHTTP and is redundant
    throw new Error("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
  };


function setImages(cls){
    try {
        var imagesLoaded=eval(cls+"ImagesLoaded");
        var className=eval(cls+"ClassName");
        var imageUrl=eval(cls+"ImageUrl");
        var altTag=eval(cls+"AltTag");
        
        if (imagesLoaded==0){
            eval(cls+"ImagesLoaded=1");
            var elems=document.getElementsByTagName("img");
            for (var i=0;i<elems.length;i++){

                if (elems[i].className==className){
                    elems[i].setAttribute('src',imageUrl);
                    elems[i].setAttribute('alt',altTag);
                }
            }
        }
    }
    catch (e){
        debugW(e); 
    }
}


/*
File request and notification object
*/


var showPopUp=1;
var hidePopUp=0;
var methodGet="GET";
var methodPost="POST";

function requestAndNotify(){
    this.URL="";
    this.name="";
    this.cbWin=null; 
    this.cbFail=null; 
    this.method=methodGet; 
    this.params=null;
    this.popUp=null;
    this.popUpHeight=18;
    this.popUpId=null;
    this.popUpCssClass="wait";
    this.refreshRate=25;
    
    this.notificationPopUp = function(popUpAction){

        setImages(this.popUpCssClass);
        if (this.popUp!=null){this.popUp.doInstant(); this.popUp=null;}
        this.popUp=new popUpResize(this.popUpId,this.popUpHeight,popUpAction);
        if (popUpAction != hidePopUp){
            this.popUp.curSize=this.popUp.minSize; 
            this.popUp.intervalId=setInterval(this.name+'.popUp.doExpand()',this.refreshRate);
        } else {
            this.popUp.curSize=this.popUp.maxSize; 
            this.popUp.intervalId=setInterval(this.name+'.popUp.doShrink()',this.refreshRate);
        }
    }




    this.showMe = function(showPopUp){
        if (this.popUpCssClass != null) {
            this.PopUpAction=showPopUp;
            this.notificationPopUp(showPopUp);
        }
    }

    this.hideMe = function(){
        if (this.popUpCssClass != null) {
            this.PopUpAction=hidePopUp;
            this.notificationPopUp(hidePopUp);
        }
    }


    this.performRequest=function(){
        this.showMe();
        makeRequest(this);
    }

}

function makeRequest(oRAN){
    request=new XMLHttpRequest();
    request.onreadystatechange=function() {
        if (this.readyState==4) {
            var response=this.responseText;
            if (this.status == 200) {
                oRAN.cbWin(response);
            } else {
                oRAN.cbFail(response);
            }
            oRAN.hideMe();
        }
    }
    request.open(oRAN.method, oRAN.URL, true);
    if (oRAN.params != null && oRAN.method == methodPost){
        request.send(oRAN.params);
    } else {
        request.send();
    }
}


function popUpResize(targetId,height,popUpAction)
{
    this.target=document.getElementById(targetId);
    this.minSize=0;
    this.maxSize=height;
    this.curSize=0;
    this.intervalId;
    this.mode=popUpAction;
    
    this.doShrink = function ()
    {
        this.mode=0;
        if (this.curSize<=this.minSize){
            this.target.style.height=this.minSize+"px";
            this.curSize=this.minSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="none";
            
        } else {
            try {
                this.target.style.height=this.curSize+"px";
                }
            catch(e){
                }            
            this.curSize--;
        }
    }

    this.doExpand = function ()
    {
        this.mode=1;
        this.target.style.display="block";
        if (this.curSize>=this.maxSize){
            this.target.style.height=this.maxSize+"px";
            this.curSize=this.maxSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
        } else {
            this.curSize++;
            this.target.style.height=this.curSize+"px";
        }
    }
    
    this.doInstant = function()
    {
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="block";
            switch (this.mode){
                case 0: //shrinking
                    this.target.style.height=this.minSize+"px";
                    this.curSize=this.minSize;
                    this.target.style.display="none";
                    break;   
                case 1: //expanding
                    this.target.style.height=this.maxSize+"px";
                    this.curSize=this.maxSize;
                    break;   
            }
    
    }
    
}

