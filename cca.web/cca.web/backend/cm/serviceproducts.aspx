﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="serviceproducts.aspx.vb" Inherits="backend_cm_serviceproducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current Service Products</h5>
<asp:Repeater ID="rptCMServiceProducts" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:80px;">
            
        </td>
        <td style="width:130px;">
            <h4>Product</h4>
        </td>
        <td style="width:100px;">
            <h4>Department</h4>
        </td>
        <td style="width:300px;">
            <h4>Description</h4>
        </td>
        <td style="width:30px;">
            
        </td>
        <td></td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("serviceproduct_pk") %>'>
            <td>
                <a href='serviceproducts.aspx?pk=<%# Container.DataItem("serviceproduct_pk")%>'><h6>Edit</h6></a>
            </td>
            <td>
                <%# Container.DataItem("productname")%>
            </td>
            <td>
                <%# Container.DataItem("departmentname")%>
            </td>
            <td>
                <%# Container.DataItem("productdescription")%>
            </td>
            <td>
                <a class="remove" title="Remove"  href='javascript:removeServiceproduct(<%# Container.DataItem("serviceproduct_pk") %>)'>X</a>
            </td>
            
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>    
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:100px">
                <a href='serviceproducts.aspx?pk=-1'><h6>New Product</h6></a> 
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
             
<asp:PlaceHolder ID="ServiceProductControl" runat="server" />
<table id="ServiceProductButton" runat="server" style="width:100%;">
    <tr style="height:10px;">
                            
    </tr>
    <tr>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton runat="server" ID="btnSaveServiceProduct" Text="<h6>Save</h6>" />
        </td>
        <td style="width:60%;"></td>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton ID="btnCancelServiceProduct" runat="server" Text="<h4>Cancel</h4>" />
        </td>
    </tr>
</table> 

</asp:Content>