<%@ WebHandler Language="VB" Class="function_grpadd" %>
Public Class function_grpadd
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Try

            Dim u As Integer
            Dim g As Integer

            Dim rf As Object = Request.QueryString
            Dim ug As New usergroups(SM.currentuser, SM.actualuser, CDC)


            If Integer.TryParse(rf("u"), u) And Integer.TryParse(rf("pk"), g) Then

                Dim grp As New groups(SM.currentuser, SM.actualuser, g, CDC)

                Dim secure As Boolean = True

                ' Allowed to manage groups
                secure = secure And Security.GroupMember(SM.currentuser, SystemGroups.ManageGroups, CDC)


                If secure Then
                    ug.user_fk = u
                    ug.group_fk = g

                    If ug.Save() Then
                        Response.Write("OK:" & ug.usergroup_pk)
                    Else
                        Response.Write("Fail:" & ug.LastError.ToString)
                    End If

                Else
                    Response.Write("Fail: Access Denied")
                End If

            End If

        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class