<%@ WebHandler Language="VB" Class="functions_reportmaintenance" %>
Public Class functions_reportmaintenance
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = "OK"
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim Obj As objects.reports
        
        Dim pk As Integer = -1
        
        Try
            pk = RF("b").ToString.Trim()
        Catch ex As Exception
            pk = -1
        End Try
        
        
        Try
            Obj = New objects.reports(SM.currentuser, SM.actualuser, pk, CDC)
            If Obj.rowstatus = 1 Then
                If Not Obj.Enable Then
                    Msg = "FAIL:" & Obj.LastError.Message
                End If
            Else
                If Not Obj.Disable Then
                    Msg = "FAIL:" & Obj.LastError.Message
                End If
            End If
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        
        Response.Write(Msg)
    End Sub

End Class
