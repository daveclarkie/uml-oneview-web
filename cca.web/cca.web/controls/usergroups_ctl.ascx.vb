Partial Class usergroups_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ousergroups As Objects.usergroups
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_groups_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.group_fk.Items.Count=0 Then
        Me.group_fk.DataSource=CDC.ReadDataTable(CO)
        Me.group_fk.DataTextField = "value"
        Me.group_fk.DataValueField = "pk"
        Try
            Me.group_fk.DataBind
        Catch Ex as Exception
            Me.group_fk.SelectedValue=-1
            Me.group_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ousergroups is Nothing then
ousergroups = new Objects.usergroups(currentuser,actualuser,pk,CDC)
End If
Me.user_fk.SelectedValue=ousergroups.user_fk
Me.group_fk.SelectedValue=ousergroups.group_fk
_CurrentPk=ousergroups.usergroup_pk
Status=ousergroups.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ousergroups is Nothing then
ousergroups = new Objects.usergroups(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ousergroups.user_fk=Me.user_fk.SelectedValue
ousergroups.group_fk=Me.group_fk.SelectedValue
result=ousergroups.Save()
if not result then throw ousergroups.LastError
_CurrentPk=ousergroups.usergroup_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ousergroups.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousergroups is Nothing then
   ousergroups = new Objects.usergroups(currentuser,actualuser,pk,CDC)
  End If
  ousergroups.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousergroups is Nothing then
   ousergroups = new Objects.usergroups(currentuser,actualuser,pk,CDC)
  End If
  ousergroups.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ousergroups is Nothing then
   ousergroups = new Objects.usergroups(currentuser,pk,CDC)
  End If
  ousergroups.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  Me.group_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkgroup_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

