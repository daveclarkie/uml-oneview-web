<%@ WebHandler Language="VB" Class="function_orgdetail" %>
Public Class function_orgdetail
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim msg As String = ""
        
        Try

            msg = ""
            msg &= "<div class='modal_titlebar'>Organisation Detail</div><div class='modal_exitbar'><a href=""javascript:showHide('orgDisplay');"">X</a></div>"
            msg &= "<div class='modal_panel'><ul class='formcontrol'>"

            Dim hasPk As Boolean = False
            Dim pk As Integer = 0
            hasPk = Integer.TryParse(RQ("p"), pk)
            
            Dim o As New organisations(SM.currentuser, SM.actualuser, CDC)
            If hasPk And pk > 0 Then
                o.Read(pk)
                msg &= "<li><span class='label'>Name</span><div>" & o.customername & " / " & o.organisation_pk & "</div></li>"

                
                Dim CO As New SqlCommand()
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_organisationlocations_list"
                CO.Parameters.AddWithValue("@organisation_pk", pk)
                
                Dim dt As DataTable = CDC.ReadDataTable(CO)
                Dim dr As DataRow
                For Each dr In dt.Rows
                    msg &= "<li><span class='label'></span><div style='display:inline-block;'>" & dr("location") & "</div></li>"
                Next
                
            End If

            msg &= "</ul></div></div>"
            
        Catch LE As Exception
            msg = "FAIL:" & LE.Message
        End Try
        Response.Write(msg)
        
    End Sub

End Class