<%@ Control Language="VB" AutoEventWireup="false" CodeFile="systems.ascx.vb" Inherits="design_elements_navigation_systems" %>
<h3>Systems</h3>
<ul class="navigationmenu">
<li><a href="./default.aspx">Home</a></li>
<li><a href="./activity.aspx">User Activity</a></li>
<li><a href="./manageusers.aspx">User Management</a></li>
<li><a href="./groupmanagement.aspx">Manage Groups</a></li>
</ul>
<h3>User Interface</h3>
<ul class="navigationmenu">
<li><a href="../core/default.aspx">Back to the System</a></li>
</ul>
<h3>CCA Global Settings</h3>
<ul class="navigationmenu">
<li><a href="../../../backend/cca/cclratecostmanagement.aspx">CCL Rate Costs</a></li>
<li><a href="../../../backend/cca/cclratepercentagemanagement.aspx">CCL Rate Percentages</a></li>
<li><a href="../../../backend/cca/eligibilitymanagement.aspx">Eligibility Thresholds</a></li>
<li><a href="../../../backend/cca/fuelconversionmanagement.aspx">Fuel Conversions</a></li>
</ul>

<%  If HasDebuggingAccess Then%>
<h3>Switch User</h3>
<ul class="navigationmenu">
<asp:DropDownList ID="dgr" runat="server" />
<asp:LinkButton ID="btndgr" runat="server" Text="Login" CausesValidation="false" />
</ul>
<% end if %>

<h3>Log Out</h3>
<ul class="navigationmenu">
<li><a href="../logout.aspx">Log Out (<%=PSM.UserName(SM.currentuser)%>)</a></li>
</ul>

[systems]