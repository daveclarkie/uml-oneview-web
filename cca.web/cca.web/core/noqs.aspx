<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="noqs.aspx.vb" Inherits="core_noqs" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Error</h3>
    Your previous action resulted in a poorly formated request being passed to this
    page. Please pass the following information to dave.clarke@ems.schneider-electric.com<br />
    <br />
   <ul>
    <li>
        A description of the action you took immediately prior to  this message appearing.
    </li> 
   <li>
       Full URL:<br />
       <%=server.UrlDecode(Request.Querystring("p")) %>
   </li>
   <li>
       Referral URL:<br />
       <%=server.UrlDecode(Request.Querystring("r")) %>
   </li>
   </ul>
   
   
    
</asp:Content>

