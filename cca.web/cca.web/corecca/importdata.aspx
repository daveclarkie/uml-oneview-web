<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="importdata.aspx.vb" Inherits="corecca_importdata" title="import data" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        <h3>Import Data</h3> 
        
            <asp:Panel ID="Panel1" runat="server">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:linkButton ID="btnUpload" runat="server" Text="<h6>Upload</h6>" OnClick="btnUpload_Click" />
                <br />
                <asp:Label ID="lblMessage" runat="server" Text="" />
                <br />
                <h4>Example</h4>
                <img src="../design/images/downloadtemplate_example.png" alt="download template example" />
                <br />
                <a href="../files/Template.csv"><h6>download template</h6></a>
                
            </asp:Panel>

            <asp:Panel ID="Panel2" runat="server" Visible="false" >
                <asp:Label ID="Label5" runat="server" Text="File Name"/>
                <asp:Label ID="lblFileName" runat="server" Text=""/>
                <br />
                <asp:Button ID="btnSave" runat="server" Text="Save" 
                      OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                      OnClick="btnCancel_Click" />        
             </asp:Panel>
      
    </div>      
</div>
</asp:Content>

