Partial Class matrix2sources_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omatrix2sources As Objects.matrix2sources
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_matrixfiles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.matrixfile_fk.Items.Count=0 Then
        Me.matrixfile_fk.DataSource=CDC.ReadDataTable(CO)
        Me.matrixfile_fk.DataTextField = "value"
        Me.matrixfile_fk.DataValueField = "pk"
        Try
            Me.matrixfile_fk.DataBind
        Catch Ex as Exception
            Me.matrixfile_fk.SelectedValue=-1
            Me.matrixfile_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_suppliers_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.supplier_fk.Items.Count=0 Then
        Me.supplier_fk.DataSource=CDC.ReadDataTable(CO)
        Me.supplier_fk.DataTextField = "value"
        Me.supplier_fk.DataValueField = "pk"
        Try
            Me.supplier_fk.DataBind
        Catch Ex as Exception
            Me.supplier_fk.SelectedValue=-1
            Me.supplier_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_ldzs_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.ldz_fk.Items.Count=0 Then
        Me.ldz_fk.DataSource=CDC.ReadDataTable(CO)
        Me.ldz_fk.DataTextField = "value"
        Me.ldz_fk.DataValueField = "pk"
        Try
            Me.ldz_fk.DataBind
        Catch Ex as Exception
            Me.ldz_fk.SelectedValue=-1
            Me.ldz_fk.DataBind
        End Try
    End If
            Me.band_start.Text="0"
            Me.band_end.Text="0"
            Me.standingcharge.Text="0.00"
            Me.rate.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omatrix2sources is Nothing then
omatrix2sources = new Objects.matrix2sources(currentuser,actualuser,pk,CDC)
End If
Me.matrixfile_fk.SelectedValue=omatrix2sources.matrixfile_fk
Me.supplier_fk.SelectedValue=omatrix2sources.supplier_fk
Me.product.Text=omatrix2sources.product
Me.ldz_fk.SelectedValue=omatrix2sources.ldz_fk
Me.band_start.Text=omatrix2sources.band_start
Me.band_start.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.band_start.Attributes.Add("onBlur", "clearNotice()")
Me.band_end.Text=omatrix2sources.band_end
Me.band_end.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.band_end.Attributes.Add("onBlur", "clearNotice()")
Me.validfrom.Text=omatrix2sources.validfrom.ToString("dd MMM yyyy")
If Me.validfrom.Text="01 Jan 0001" then Me.validfrom.Text=""
Me.validfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.validfrom.Attributes.Add("onBlur", "clearNotice()")
Me.validto.Text=omatrix2sources.validto.ToString("dd MMM yyyy")
If Me.validto.Text="01 Jan 0001" then Me.validto.Text=""
Me.validto.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.validto.Attributes.Add("onBlur", "clearNotice()")
Me.standingcharge.Text=omatrix2sources.standingcharge
Me.standingcharge.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.standingcharge.Attributes.Add("onBlur", "clearNotice()")
Me.rate.Text=omatrix2sources.rate
Me.rate.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.rate.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=omatrix2sources.matrixsource_pk
Status=omatrix2sources.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omatrix2sources is Nothing then
omatrix2sources = new Objects.matrix2sources(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omatrix2sources.matrixfile_fk=Me.matrixfile_fk.SelectedValue
omatrix2sources.supplier_fk=Me.supplier_fk.SelectedValue
omatrix2sources.product=Me.product.Text
omatrix2sources.ldz_fk=Me.ldz_fk.SelectedValue
omatrix2sources.band_start=Me.band_start.Text
omatrix2sources.band_end=Me.band_end.Text
omatrix2sources.validfrom=CommonFN.CheckEmptyDate(Me.validfrom.Text)
omatrix2sources.validto=CommonFN.CheckEmptyDate(Me.validto.Text)
omatrix2sources.standingcharge=Me.standingcharge.Text
omatrix2sources.rate=Me.rate.Text
result=omatrix2sources.Save()
if not result then throw omatrix2sources.LastError
_CurrentPk=omatrix2sources.matrixsource_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omatrix2sources.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2sources is Nothing then
   omatrix2sources = new Objects.matrix2sources(currentuser,actualuser,pk,CDC)
  End If
  omatrix2sources.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2sources is Nothing then
   omatrix2sources = new Objects.matrix2sources(currentuser,actualuser,pk,CDC)
  End If
  omatrix2sources.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omatrix2sources is Nothing then
   omatrix2sources = new Objects.matrix2sources(currentuser,pk,CDC)
  End If
  omatrix2sources.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.matrixfile_fk.Enabled=IIF(WL>=99,True,False)
  Me.blkmatrixfile_fk.Visible=IIF(RL>=99,True,False)
  Me.supplier_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksupplier_fk.Visible=IIF(RL>=0,True,False)
  Me.product.Enabled=IIF(WL>=0,True,False)
  Me.blkproduct.Visible=IIF(RL>=0,True,False)
  Me.ldz_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkldz_fk.Visible=IIF(RL>=0,True,False)
  Me.band_start.Enabled=IIF(WL>=0,True,False)
  Me.blkband_start.Visible=IIF(RL>=0,True,False)
  Me.band_end.Enabled=IIF(WL>=0,True,False)
  Me.blkband_end.Visible=IIF(RL>=0,True,False)
  Me.validfrom.Enabled=IIF(WL>=0,True,False)
  Me.blkvalidfrom.Visible=IIF(RL>=0,True,False)
  Me.validto.Enabled=IIF(WL>=0,True,False)
  Me.blkvalidto.Visible=IIF(RL>=0,True,False)
  Me.standingcharge.Enabled=IIF(WL>=0,True,False)
  Me.blkstandingcharge.Visible=IIF(RL>=0,True,False)
  Me.rate.Enabled=IIF(WL>=0,True,False)
  Me.blkrate.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

