<%@ Control Language="VB" AutoEventWireup="false" CodeFile="resetpassword_ctl.ascx.vb" Inherits="resetpassword_ctl" %>

<h3>In order to change your password we require the following:-</h3>
<ul class="formcontrol">
<li><span class="label">Current Password</span><asp:TextBox ID="pwdCurrentPassword" runat="server" TextMode="Password" /><asp:Button ID="btnviewcurrent" runat="server" Text="view" /></li>
<li><span class="label">New Password</span><asp:TextBox ID="pwdNewPassword" runat="server" TextMode="Password" /><asp:Button ID="btnviewnew" runat="server" Text="view" /></li>
<li><span class="label">Confirm Password</span><asp:TextBox ID="pwdConfirmPassword" runat="server" TextMode="Password" /><asp:Button ID="btnviewconfirm" runat="server" Text="view" /></li>
<li><span class="label">&nbsp;</span><asp:Button ID="btnReset" runat="server" Text="Change Password" /></li>
<li><asp:Label ID="lblMessage" runat="server" Text="" /></li>
<%--<li>The password reset request will be sent to all email addresses linked to your account.</li>--%>
</ul>

