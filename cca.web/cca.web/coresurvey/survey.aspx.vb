﻿Partial Class coresurvey_survey
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Dim errormessage As String = ""
    Dim survey_pk As Integer
    Dim surveyurl_pk As Integer
    Dim surveydetail_pk As Integer
    Dim helpdesk_id As Integer
    Dim selecteduser_fk As Integer

    Dim cc As Object

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lbl_errormessage.Text = Nothing
        setform("clear")

        If Request.QueryString("surveyurl") <> Nothing Then
            Dim variableurl As String = Request.QueryString("surveyurl").ToString
            CDC.Sanitize(variableurl)
            Dim CO As New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_surveyurl_retrieve"
            CO.Parameters.AddWithValue("@url", variableurl)
            CDC.ReadScalarValue(surveyurl_pk, CO)

            Dim su As New surveyurls(-1, -1, surveyurl_pk, CDC)
            survey_pk = su.survey_fk

            If surveyurl_pk = -2 Then ' expired link
                errormessage &= vbCrLf & "It appears that this survey has already been completed."
            ElseIf surveyurl_pk = -1 Then ' invalid url passed
                errormessage &= vbCrLf & "The link you are using is invalid."
            ElseIf surveyurl_pk = 0 Then ' invalid url passed
                errormessage &= vbCrLf & "The link you are using is invalid."
            Else ' match
                errormessage = Nothing
                setForm("open")

                Dim s As New surveys(-1, -1, survey_pk, CDC)
                Dim sd As Object

                surveydetail_pk = s.surveydetail_fk

                Select Case s.surveytype_fk
                    Case 1 'CM survey
                        sd = New survey1details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                        helpdesk_id = sd.helpdesk_id

                        LoadHelpdeskForm(s.surveytype_fk)
                    Case 2 'analyst survey
                        sd = New survey2details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                        helpdesk_id = sd.helpdesk_id

                        LoadHelpdeskForm(s.surveytype_fk)
                    Case 7 'Budget Survey CM
                        sd = New survey7details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                        helpdesk_id = sd.helpdesk_id

                        LoadHelpdeskForm(s.surveytype_fk)
                    Case 8 'Budget Survey Technician
                        sd = New survey8details(-1, -1, surveydetail_pk, CDC)
                        selecteduser_fk = sd.selecteduser_fk
                        helpdesk_id = sd.helpdesk_id

                        LoadHelpdeskForm(s.surveytype_fk)

                    Case Else
                        errormessage = "Invalid Survey Type"
                        lbl_errormessage.Text = errormessage
                        Exit Sub
                End Select

                SM.actualuser = selecteduser_fk
                SM.targetuser = selecteduser_fk
                SM.Save()

                
                SetControl("~/controls/survey" & s.surveytype_fk & "details_ctl.ascx", SurveyControl, surveydetail_pk)
            End If

        Else
            If Request.QueryString("r") <> Nothing Then
                setForm("complete")
            End If
        End If

        lbl_errormessage.Text = errormessage

    End Sub

    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal PK As Integer)
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM

        cc.Read(SM.currentuser, SM.actualuser, PK)
        TargetControl.Controls.Add(cc)
        
    End Sub

    Protected Sub btn_savesurvey_Click(sender As Object, e As System.EventArgs) Handles btn_savesurvey.Click
        Dim resDet As Boolean = True
        resDet = cc.Save(selecteduser_fk, selecteduser_fk, surveydetail_pk)

        If Not (resDet) Then
            Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                lbl_errormessage.Text = "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "."
            Else
                lbl_errormessage.Text = "This record has not been saved because the " & errMsg(0).ToLower() & "."
            End If
        Else
            CloseSurvey()

            Dim filtered As New NameValueCollection(Request.QueryString)
            Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
            nameValueCollection.Remove("surveyurl")
            Dim url As String = HttpContext.Current.Request.Path + "?r=Survey+Complete"
            Response.Redirect(url)
        End If
    End Sub

    Private Sub CloseSurvey()

        ' disable URL
        Dim su As New surveyurls(selecteduser_fk, selecteduser_fk, surveyurl_pk, CDC)
        su.Disable()
        ' update status of survey to 4: complete
        Dim s As New surveys(selecteduser_fk, selecteduser_fk, survey_pk, CDC)
        s.surveystatus_fk = 4
        s.Save()

    End Sub

    Private Sub setForm(ByVal stage As String)
        If stage = "open" Then
            pnl_helpdesk.Visible = True
            pnl_survey.Visible = True
            pnl_complete.Visible = False
        ElseIf stage = "complete" Then
            pnl_helpdesk.Visible = False
            pnl_survey.Visible = False
            pnl_complete.Visible = True
        ElseIf stage = "clear" Then
            pnl_helpdesk.Visible = False
            pnl_survey.Visible = False
            pnl_complete.Visible = False
        End If
    End Sub

    Private Sub LoadHelpdeskForm(surveytype_fk As Integer)

        Dim sql_string As String = "" ' this will be replaced by stored proc eventually

        If surveytype_fk = 1 Or surveytype_fk = 2 Then
            sql_string &= "	USE servicedesk "

            sql_string &= "	SELECT  "
            sql_string &= "	FIN.workorderid															[workorderid] "
            sql_string &= ", title																	[subject] "
            sql_string &= ",(SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= FIN.requesterid)		    [Requester] "
            sql_string &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= FIN.ownerid)			[Technician] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.createdtime/1000, '1970-01-01'))			[Createdtime] "
            sql_string &= ", FIN.GUDF_char1														    [Client] "
            sql_string &= ", (SELECT QUEUENAME FROM QUEUEDEFINITION WHERE QUEUEID=FIN.QUEUEID)		[Country] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.UDF_date1/1000, '1970-01-01'))			[Date1] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.UDF_date2/1000, '1970-01-01'))			[Date2] "
            sql_string &= ", (SELECT STATUSNAME FROM STATUSDEFINITION WHERE STATUSID=FIN.STATUSID)	[Status] "
            sql_string &= ", FIN.GUDF_char8														    [Commodity] "

            sql_string &= "FROM  "
            sql_string &= "( "
            sql_string &= "SELECT  "
            sql_string &= "DATA.* "
            sql_string &= ", WOQ.QUEUEID "
            sql_string &= "FROM   "
            sql_string &= "( "
            sql_string &= "SELECT  "
            sql_string &= "wo.WORKORDERID "
            sql_string &= ", wo.TITLE "
            sql_string &= ", createdtime "
            sql_string &= ", GUDF_char1 "
            sql_string &= ", GUDF_char8 "
            sql_string &= ", UDF_char8 "
            sql_string &= ", UDF_char12 "
            sql_string &= ", UDF_char1 "
            sql_string &= ", UDF_date1 "
            sql_string &= ", UDF_date2 "
            sql_string &= ", ownerid "
            sql_string &= ", STATUSID "
            sql_string &= ", requesterid  "
            sql_string &= ", COMPLETEDTIME  "
            sql_string &= "FROM   "
            sql_string &= "servicereq_2102 as SR "
            sql_string &= "JOIN WORKORDER AS WO ON WO.workorderid=SR.workorderid "
            sql_string &= "JOIN WORKORDERSTATES as WOS ON WOS.workorderid=SR.workorderid "
            sql_string &= "JOIN SERVICECATALOG_FIELDS as SCF ON SCF.workorderid=SR.workorderid "
            sql_string &= ") as DATA "
            sql_string &= "LEFT JOIN Workorder_queue as WOQ ON WOQ.workorderid= DATA.workorderid "
            sql_string &= ") as fin "
            sql_string &= "WHERE  "
            sql_string &= "FIN.workorderid = " & helpdesk_id
        ElseIf surveytype_fk = 7 Or surveytype_fk = 8 Then
            sql_string &= "	USE servicedesk "

            sql_string &= "	SELECT  "
            sql_string &= "	FIN.workorderid															[workorderid] "
            sql_string &= ", title																	[subject] "
            sql_string &= ",(SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= FIN.requesterid)		    [Requester] "
            sql_string &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= FIN.ownerid)			[Technician] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.createdtime/1000, '1970-01-01'))			[Createdtime] "
            sql_string &= ", FIN.GUDF_char1														    [Client] "
            sql_string &= ", UDF_char10                                                     		[Country] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.UDF_date2/1000, '1970-01-01'))			[Date1] "
            sql_string &= ", DATEADD(HH,1,DATEADD(SS, FIN.UDF_date3/1000, '1970-01-01'))			[Date2] "
            sql_string &= ", (SELECT STATUSNAME FROM STATUSDEFINITION WHERE STATUSID=FIN.STATUSID)	[Status] "
            sql_string &= ", UDF_char5  														    [Commodity] "

            sql_string &= "FROM  "
            sql_string &= "( "
            sql_string &= "SELECT  "
            sql_string &= "DATA.* "
            sql_string &= ", WOQ.QUEUEID "
            sql_string &= "FROM   "
            sql_string &= "( "
            sql_string &= "SELECT  "
            sql_string &= "wo.WORKORDERID "
            sql_string &= ", wo.TITLE "
            sql_string &= ", createdtime "
            sql_string &= ", GUDF_char1 "
            sql_string &= ", GUDF_char8 "
            sql_string &= ", UDF_char5 "
            sql_string &= ", UDF_char10 "
            sql_string &= ", UDF_date2 "
            sql_string &= ", UDF_date3 "
            sql_string &= ", ownerid "
            sql_string &= ", STATUSID "
            sql_string &= ", requesterid  "
            sql_string &= ", COMPLETEDTIME  "
            sql_string &= "FROM   "
            sql_string &= "servicereq_5101 as SR "
            sql_string &= "JOIN WORKORDER AS WO ON WO.workorderid=SR.workorderid "
            sql_string &= "JOIN WORKORDERSTATES as WOS ON WOS.workorderid=SR.workorderid "
            sql_string &= "JOIN SERVICECATALOG_FIELDS as SCF ON SCF.workorderid=SR.workorderid "
            sql_string &= ") as DATA "
            sql_string &= "LEFT JOIN Workorder_queue as WOQ ON WOQ.workorderid= DATA.workorderid "
            sql_string &= ") as fin "
            sql_string &= "WHERE  "
            sql_string &= "FIN.workorderid = " & helpdesk_id
        End If

        CO = New SqlClient.SqlCommand(sql_string)
        CO.CommandType = CommandType.Text
        Dim dt As DataTable = CDC.ReadDataTable(CO, 1)

        For Each dr As DataRow In dt.Rows

            lbl_hd_requestid.Text = dr("workorderid")
            lbl_hd_requester.Text = dr("Requester")
            lbl_hd_technician.Text = dr("Technician")
            lbl_hd_created.Text = dr("Createdtime")
            lbl_hd_client.Text = dr("Client")
            lbl_hd_commodity.Text = dr("Commodity")
            lbl_hd_country.Text = dr("Country")
            lbl_hd_status.Text = dr("Status")

        Next

    End Sub


End Class
