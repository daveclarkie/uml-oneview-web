<%@ Control Language="VB" AutoEventWireup="false" CodeFile="eligibilitythresholds_ctl.ascx.vb" Inherits="eligibilitythresholds_ctl" %>
<h5>Add/Edit Threshold</h5>
<ul class='formcontrol'>
<li runat="server" id="blkthreshold">
<span class='label'>Threshold<span class='pop'>What is the cut off where the eligibility becomes fully exempt</span></span>
<asp:TextBox EnableViewState="false" ID="threshold" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcorrectionvalue">
<span class='label'>Correction Value<span class='pop'>If not fully exempt what is the correction factor to be applied</span></span>
<asp:TextBox EnableViewState="false" ID="correctionvalue" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blksubmissionfrom">
<span class='label'>Submission From<span class='pop'>When is the threshold to be used for submissions from</span></span>
<asp:TextBox EnableViewState="false" ID="submissionfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blksubmissionto">
<span class='label'>Submission To<span class='pop'>When is the threshold to be used for submissions to</span></span>
<asp:TextBox EnableViewState="false" ID="submissionto" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkreportfrom">
<span class='label'>Reporting From<span class='pop'>When is the threshold to be used for reporting from</span></span>
<asp:TextBox EnableViewState="false" ID="reportfrom" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkreportto">
<span class='label'>Reporting To<span class='pop'>When is the threshold to be used for reporting to</span></span>
<asp:TextBox EnableViewState="false" ID="reportto" runat="server" cssClass="input_dtm" />
</li>
</ul>

