
Partial Class functions_disconnect
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand
    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Response.Buffer = True
        Try
            Dim u As Integer = 0

            If Integer.TryParse(Request.QueryString("u"), u) Then
                ' TODO: Check for Referer
                ' TODO: Check for Admin access
                ' TODO: Check for Reset Password Permission
                ' TODO: Check for Is Inactive User

                Dim result As Boolean = False
                Dim log As String = ""
                Dim user_pk As Integer = 0

                Dim CO As New SqlClient.SqlCommand

                Dim user As New users(SM.currentuser, SM.actualuser, u, CDC)

                user.disconnect = 1
                If user.Save() Then
                    Response.Write("OK:" & u)
                Else
                    Throw New IndexOutOfRangeException(user.LastError.Message)
                End If
            Else
                Throw New IndexOutOfRangeException("Invalid data supplied")
            End If
        Catch Ex As Exception
            Response.Clear()
            Response.Write("Fail:" & Ex.Message)
        End Try

    End Sub
End Class
