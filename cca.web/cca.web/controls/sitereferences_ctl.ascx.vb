Partial Class sitereferences_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents ositereferences As Objects.sitereferences
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        Me.site_fk.Text = SM.subid
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If ositereferences Is Nothing Then
            ositereferences = New objects.sitereferences(currentuser, actualuser, pk, CDC)
            ositereferences.site_fk = SM.subid
        End If
        Me.site_fk.Text = ositereferences.site_fk
        Me.reference.Text = ositereferences.reference
        Me.referencepurpose.Text = ositereferences.referencepurpose
        _CurrentPk = ositereferences.sitereference_pk
        Status = ositereferences.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ositereferences Is Nothing Then
            ositereferences = New Objects.sitereferences(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            ositereferences.site_fk = Me.site_fk.Text
            ositereferences.reference = Me.reference.Text
            ositereferences.referencepurpose = Me.referencepurpose.Text
            result = ositereferences.Save()
            If Not result Then Throw ositereferences.LastError
            _CurrentPk = ositereferences.sitereference_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = ositereferences.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ositereferences Is Nothing Then
            ositereferences = New Objects.sitereferences(currentuser, actualuser, pk, CDC)
        End If
        ositereferences.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ositereferences Is Nothing Then
            ositereferences = New Objects.sitereferences(currentuser, actualuser, pk, CDC)
        End If
        ositereferences.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ositereferences Is Nothing Then
            ositereferences = New Objects.sitereferences(currentuser, pk, CDC)
        End If
        ositereferences.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.site_fk.Enabled = IIf(WL >= 99, True, False)
        Me.blksite_fk.Visible = IIf(RL >= 99, True, False)
        Me.reference.Enabled = IIF(WL >= 0, True, False)
        Me.blkreference.Visible = IIF(RL >= 0, True, False)
        Me.referencepurpose.Enabled = IIF(WL >= 0, True, False)
        Me.blkreferencepurpose.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

