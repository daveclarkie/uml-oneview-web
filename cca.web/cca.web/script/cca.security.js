﻿// JScript File

// detect auto filled passwords

var pw=0;

function detectAutoComplete(){
    var lms=document.getElementsByTagName("input")
    var lm; 
    var l; 
    var rl;
    var f=0;
    var pwdbox;
    var btnsub; 
    for (lm in lms){
        if ( isNumeric(lm) ){
            l=lms[lm]; //index returned
        } else {
            rl=rpl(lm,'\\$','_');
            l=document.getElementById(rl);  //id returned
        }

        if ( l ) {
            if (l.type=="password" && f==0) {
                pwdbox=l;
                pwdbox.onkeyup=function(){
                    pw++;
                }        
                pwdbox.onfocus=function(){
                    clearNotice(); 
                    pw=0;
                }        
                pwdbox.onblur=function(){
                    if (this.value.length > pw){this.value="";pw=0;setNotice("Pasted or auto-completed password detected.");}
                }
               f=1; 
            } // if password
            if (l.type=="submit" && f==1){
                btnsub=l;
                var origSubmit=btnsub.onclick;
                btnsub.onclick=function(){
                    if (pwdbox.value.length > pw){
                        pwdbox.value="";
                        pw=0;
                        setNotice("Pasted or auto-completed password detected.");
                    } else {
                        origSubmit();
                    }
                }
                break;
            } // if submit
        } // if l
    } // next
}


