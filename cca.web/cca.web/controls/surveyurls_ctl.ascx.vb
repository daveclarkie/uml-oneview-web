Partial Class surveyurls_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents osurveyurls As Objects.surveyurls
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_surveys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.survey_fk.Items.Count=0 Then
        Me.survey_fk.DataSource=CDC.ReadDataTable(CO)
        Me.survey_fk.DataTextField = "value"
        Me.survey_fk.DataValueField = "pk"
        Try
            Me.survey_fk.DataBind
        Catch Ex as Exception
            Me.survey_fk.SelectedValue=-1
            Me.survey_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If osurveyurls is Nothing then
osurveyurls = new Objects.surveyurls(currentuser,actualuser,pk,CDC)
End If
Me.survey_fk.SelectedValue=osurveyurls.survey_fk
Me.url.Text=osurveyurls.url
_CurrentPk=osurveyurls.surveyurl_pk
Status=osurveyurls.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If osurveyurls is Nothing then
osurveyurls = new Objects.surveyurls(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
osurveyurls.survey_fk=Me.survey_fk.SelectedValue
osurveyurls.url=Me.url.Text
result=osurveyurls.Save()
if not result then throw osurveyurls.LastError
_CurrentPk=osurveyurls.surveyurl_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=osurveyurls.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveyurls is Nothing then
   osurveyurls = new Objects.surveyurls(currentuser,actualuser,pk,CDC)
  End If
  osurveyurls.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveyurls is Nothing then
   osurveyurls = new Objects.surveyurls(currentuser,actualuser,pk,CDC)
  End If
  osurveyurls.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If osurveyurls is Nothing then
   osurveyurls = new Objects.surveyurls(currentuser,pk,CDC)
  End If
  osurveyurls.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.survey_fk.Enabled=IIF(WL>=0,True,False)
  Me.blksurvey_fk.Visible=IIF(RL>=0,True,False)
  Me.url.Enabled=IIF(WL>=0,True,False)
  Me.blkurl.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

