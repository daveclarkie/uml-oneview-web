<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="corematrix_search" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Search</h3>
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <ul id="" class='formcontrol'>
                <li><span class="label">Customer Name</span><asp:TextBox ID="txtCustomername" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Meter Number</span><asp:TextBox ID="txtMeternumber" runat="server" CssClass="input_str" /></li>
                <li><span class="label">&nbsp;</span><asp:LinkButton ID="btnSearch" runat="server" Text="Locate" CausesValidation="False" /><asp:LinkButton ID="btnAgreementSearch" runat="server" Text="" CausesValidation="False" /></li>
            </ul>
            <asp:LinkButton ID="btnCreate" runat="server" Text="" CausesValidation="False" />
        </asp:Panel>

        <asp:Repeater ID="rptMatrixSearch" runat="server">
            <HeaderTemplate><h3>Results - Matrix</h3><table style="width:700px;">
                        <tr>
                            <td>
                                
                            </td>
                            <td>
                                <h4>Customer</h4>
                            </td>
                            <td>
                                <h4>Matrix</h4>
                            </td>
                        </tr>
            </HeaderTemplate> 
            <FooterTemplate>
                
            </table>
            </FooterTemplate>
            <ItemTemplate>
            <tr>
                <td style="width:60px;">
                    <%# returnselect(Container.DataItem("rowstatus"), Container.DataItem("matrix_pk"))%>
                </td>
                <td style="width:100px;">
                    <%# Container.DataItem("customername")%> 
                </td>
                <td style="width:200px;">
                    <%# Container.DataItem("tag")%>
                </td>
            </tr>
            </ItemTemplate> 
            </asp:Repeater>

            <table>
                <tr style="height:10px;"></tr>
                <tr>
                    <td style="width:150px;">
                        <asp:Label ID="lblSearchPage" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchPrev" runat="server" text="" OnClick="lnkPrev_Click" />
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchNext" runat="server" text="" OnClick="lnkNext_Click" />
                    </td>
                </tr>
            </table>

</asp:Content>

