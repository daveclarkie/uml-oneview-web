Partial Class frequencys_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ofrequencys As Objects.frequencys
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ofrequencys is Nothing then
ofrequencys = new Objects.frequencys(currentuser,actualuser,pk,CDC)
End If
Me.frequencyname.Text=ofrequencys.frequencyname
_CurrentPk=ofrequencys.frequency_pk
Status=ofrequencys.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ofrequencys is Nothing then
ofrequencys = new Objects.frequencys(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ofrequencys.frequencyname=Me.frequencyname.Text
result=ofrequencys.Save()
if not result then throw ofrequencys.LastError
_CurrentPk=ofrequencys.frequency_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ofrequencys.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ofrequencys is Nothing then
   ofrequencys = new Objects.frequencys(currentuser,actualuser,pk,CDC)
  End If
  ofrequencys.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ofrequencys is Nothing then
   ofrequencys = new Objects.frequencys(currentuser,actualuser,pk,CDC)
  End If
  ofrequencys.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ofrequencys is Nothing then
   ofrequencys = new Objects.frequencys(currentuser,pk,CDC)
  End If
  ofrequencys.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.frequencyname.Enabled=IIF(WL>=0,True,False)
  Me.blkfrequencyname.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

