<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="userhelpdesktemplate_combo_ctl.ascx.vb" Inherits="userhelpdesktemplate_combo_ctl" %>
<h5>Detail</h5>
<ul class='formcontrol'>
<li runat="server" id="blktemplatename">
<span class='label'>Template Name<span class='pop'>This must match the record in Helpdesk to work</span></span>
<asp:TextBox EnableViewState="false" ID="templatename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkhelpdesktemplategroup_fk">
<span class='label'>helpdesktemplategroup_fk</span>
<asp:DropDownList ID="helpdesktemplategroup_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktemplatedisplayname">
<span class='label'>Group<span class='pop'>This is the grouping you want the template to be included in</span></span>
<asp:TextBox EnableViewState="false" ID="templatedisplayname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>
