Imports System.Data.SqlClient

Partial Class mainctls_notify
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Dim dt As DataTable
    Dim CO As SqlCommand

    ' TODO: Rebuild entire page - Was I pissed when I wrote this?

    Public Event CountChanged()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetItem()

        FillList()
    End Sub

    Private Sub SetItem()
        'CO = New SqlCommand()
        'CO.CommandType = CommandType.StoredProcedure
        'If Not Page.IsPostBack Then
        '    Dim tmp As Integer = 0
        '    If Integer.TryParse(Request.QueryString("notice"), tmp) Then
        '        SM.NotificationTarget = tmp
        '    Else
        '        CO.CommandText = "rsp_usernoticeslatest" ' most overdue
        '        CO.Parameters.AddWithValue("@user_pk", SM.CurrentUser)
        '        dt = CDC.ReadDataTable(CO)
        '        Try
        '            SM.NotificationTarget = dt.Rows(0).Item("notificationtarget_pk")
        '            SM.Notification = dt.Rows(0).Item("notification_pk")
        '        Catch ex As Exception
        '            SM.NotificationTarget = -1
        '            SM.Notification = -1
        '        End Try
        '        dt.Dispose()
        '    End If
        'End If

        'Dim nt As New notificationtargets(SM.CurrentUser, SM.ActualUser, CDC)
        'nt.Read(SM.NotificationTarget)
        'SM.Notification = nt.notification_fk
        'nt = Nothing
        'Dim n As New notifications(SM.CurrentUser, SM.ActualUser, CDC)
        'n.Read(SM.Notification)
        'Me.lblContent.Text = n.message
        'Me.lblTitle.Text = n.title
        'If Me.lblTitle.Text.Length = 0 Then Me.lblTitle.Text = "No Active Notices"
        'n = Nothing

        'MarkItemRead(False)

    End Sub

    Private Sub FillList()
        CO = New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@user_pk", SM.CurrentUser)

        CO.CommandText = "rsp_usernoticesnew"
        dt = CDC.ReadDataTable(CO, 0)
        rptlist.DataSource = dt
        rptlist.DataBind()

        CO.CommandText = "rsp_usernoticesunread"
        dt = CDC.ReadDataTable(CO, 0)
        rptlistunread.DataSource = dt
        rptlistunread.DataBind()

        CO.CommandText = "rsp_usernoticesread"
        dt = CDC.ReadDataTable(CO, 0)
        rptlistarchive.DataSource = dt
        rptlistarchive.DataBind()


        dt.Dispose()
        CO.Dispose()
    End Sub

    Private Sub MarkItemRead(ByVal isConfirmed As Boolean)
        'Dim CO As New SqlCommand
        'CO.CommandType = CommandType.StoredProcedure
        'CO.CommandText = "rsp_usernoticeslogentry" ' most overdue
        'CO.Parameters.AddWithValue("@notificationtarget_pk", SM.NotificationTarget)
        'CDC.ReadScalarValue(SM.NotificationLog, CO)
        'CO.Dispose()

        'Dim nl As New notificationlogs(SM.currentuser, SM.actualuser, CDC)
        'nl.Read(SM.NotificationLog)
        'If nl.notificationlog_pk = 0 Then
        '    nl.notificationtarget_fk = SM.NotificationTarget
        '    nl.displayed = Now
        'End If

        'If isConfirmed Then
        '    nl.confirmed = Now
        'End If
        'nl.Save()

        'SM.NotificationLog = nl.notificationlog_pk
        'If nl.confirmed <> Nothing Then
        '    Me.btnConfirm.Enabled = False
        'Else
        '    Me.btnConfirm.Enabled = True
        'End If
        'Me.btnConfirm.CommandArgument = SM.NotificationTarget
        'nl = Nothing


        'SimEvent()



    End Sub

    'Private Sub SimEvent()
    '    Dim ctl As Object = ParseControls(Page.Master)
    '    If Not ctl Is Nothing Then
    '        ctl.Refresh()
    '    End If
    'End Sub

    'Private Function ParseControls(ByRef ctl As Control) As Control
    '    Dim ic As Control
    '    Dim fnd As Control = Nothing
    '    Dim ctlId As String = ""

    '    For Each ic In ctl.Controls
    '        Try
    '            ctlId = ic.ID
    '        Catch ex As Exception
    '            ctlId = ""
    '        End Try
    '        If ctlId Is Nothing Then ctlId = ""
    '        If ctlId.Contains("Notifications1") Then
    '            Return ic
    '        Else
    '            If ic.HasControls Then
    '                fnd = ParseControls(ic)
    '                If Not fnd Is Nothing Then Return fnd
    '            End If
    '        End If
    '    Next
    '    Return Nothing
    'End Function



    'Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnConfirm.Command

    '    Dim tmp As Integer
    '    If Integer.TryParse(e.CommandArgument, tmp) Then
    '        SM.NotificationTarget = tmp
    '        MarkItemRead(True)
    '        FillList()
    '    End If

    'End Sub
End Class
