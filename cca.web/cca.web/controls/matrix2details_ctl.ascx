<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrix2details_ctl.ascx.vb" Inherits="matrix2details_ctl" %>
<h5>matrix2details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkmprn">
<span class='label'>MPRN</span>
<asp:TextBox EnableViewState="false" ID="mprn" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkpostcode">
<span class='label'>Post Code</span>
<asp:TextBox EnableViewState="false" ID="postcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkconsumption">
<span class='label'>Consumption</span>
<asp:TextBox EnableViewState="false" ID="consumption" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkcurrent_standingcharge">
<span class='label'>Current Standing Charge</span>
<asp:TextBox EnableViewState="false" ID="current_standingcharge" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkcurrent_rate">
<span class='label'>Current Rate</span>
<asp:TextBox EnableViewState="false" ID="current_rate" runat="server" cssClass="input_dbl" />
</li>
</ul>

