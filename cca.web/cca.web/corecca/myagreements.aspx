<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="myagreements.aspx.vb" Inherits="corecca_myagreements" title="myagreements" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<div class="inner" >
<div id="output" style="display:none; position:absolute; left:350px; top:20px;">

</div>
    <div class="content">
       <h5>Current Agreements That You Are Assigned To</h5>
               <br />
                <asp:Repeater ID="rptMyAgreements" runat="server">
                    <HeaderTemplate><table>
                    <tr>
                        <td style="width:40px;">
                                
                        </td>
                        <td style="width:50px;">
                            <h4>ID</h4>
                        </td>
                        <td style="width:350px;">
                            <h4>Agreement Name</h4>
                        </td>
                        <td style="width:150px;">
                            <h4>Last Report</h4>
                        </td>
                        <td></td>
                    </tr>
                </HeaderTemplate> 
                
                <ItemTemplate>
                    <tr id='node_<%# Container.DataItem("agreement_pk") %>'>
                        <td style="text-align:center; height:15px;">
                            <a href='agreements.aspx?pk=<%# Container.DataItem("agreement_pk") %>&pg=ag<%=Local.noCache %>'><h6>agreement</h6></a>
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("agreement_pk")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("agreementname")%> 
                        </td>
                        <td style="text-indent:5px;">
                            <%# Container.DataItem("lastreportdays")%> days
                        </td>
                        <td style="text-indent:0px;">
                           <%-- <a class="remove" title="Remove"  href='javascript:removeMeterpoint(<%# Container.DataItem("agreement_pk") %>)'> </a>      --%>                          
                        </td>
                    </tr>
                </ItemTemplate> 
                    
                <FooterTemplate>
                        <tr style="height:5px;">
                        </tr>
                    </table>
                    <table style="width:150px; text-align:center;">
                        <tr>
                            <td>
                                
                            </td>
                        </tr>
                    </table>
                </FooterTemplate> 
                </asp:Repeater>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="AgreementNav" runat="server" />

    </div>
</div>
</asp:Content>

