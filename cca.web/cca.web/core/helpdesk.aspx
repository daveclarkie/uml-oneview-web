<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="helpdesk.aspx.vb" Inherits="core_helpdesk" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
       
    <h3>Helpdesk</h3>
    <asp:Panel ID="panelForm" runat="server">
        <ul class='formcontrol'>
            <li runat="server" id="blkhelpdesktemplates">
                <span class='label'>Ticket Type</span>
                <asp:DropDownList ID="helpdesktemplates" runat="server" AutoPostBack="true" cssClass="input_ddl" />
            </li>
        </ul>        

        <asp:Panel ID="panel_nhu" runat="server" Visible="false">
            <h3>New User</h3>
            <ul class='formcontrol'>
                <li runat="server" id="blknhu_forename">
                    <span class='label'>Forename</span>
                    <asp:TextBox EnableViewState="false" ID="nhu_forename" TextMode="Singleline" runat="server" cssClass="input_str" />
                </li>
                
                <li runat="server" id="blknhu_surname">
                    <span class='label'>Surname</span>
                    <asp:TextBox EnableViewState="false" ID="nhu_surname" TextMode="Singleline" runat="server" cssClass="input_str" />
                </li>
                
                <li runat="server" id="blknhu_emailaddress">
                    <span class='label'>Email Address</span>
                    <asp:TextBox EnableViewState="false" ID="nhu_emailaddress" TextMode="Singleline" runat="server" cssClass="input_str" />
                </li>
                
                <li runat="server" id="blknhu_submit">
                    <span class='label'>.</span>
                    <asp:Linkbutton runat="server" ID="nhu_submit" Text="<h6>Create Ticket</h6>" Enabled="true" Width="200px" />
                </li>                                    
            </ul>
        </asp:Panel>

        
    </asp:Panel>

    <asp:Panel ID="panel_response" runat="server" Visible="false">
        <h5>Response from Helpdesk</h5>
        <asp:Label ID="formResponse" runat="server"></asp:Label>
    </asp:Panel>



        
</asp:Content>

