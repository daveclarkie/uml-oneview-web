Partial Class eligibilitythresholds_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oeligibilitythresholds As Objects.eligibilitythresholds
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.threshold.Text="0.00"
            Me.correctionvalue.Text="0.00"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oeligibilitythresholds is Nothing then
oeligibilitythresholds = new Objects.eligibilitythresholds(currentuser,actualuser,pk,CDC)
End If
Me.threshold.Text=oeligibilitythresholds.threshold
Me.threshold.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.threshold.Attributes.Add("onBlur", "clearNotice()")
Me.correctionvalue.Text=oeligibilitythresholds.correctionvalue
Me.correctionvalue.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.correctionvalue.Attributes.Add("onBlur", "clearNotice()")
Me.submissionfrom.Text=oeligibilitythresholds.submissionfrom.ToString("dd MMM yyyy")
If Me.submissionfrom.Text="01 Jan 0001" then Me.submissionfrom.Text=""
Me.submissionfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.submissionfrom.Attributes.Add("onBlur", "clearNotice()")
Me.submissionto.Text=oeligibilitythresholds.submissionto.ToString("dd MMM yyyy")
If Me.submissionto.Text="01 Jan 0001" then Me.submissionto.Text=""
Me.submissionto.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.submissionto.Attributes.Add("onBlur", "clearNotice()")
Me.reportfrom.Text=oeligibilitythresholds.reportfrom.ToString("dd MMM yyyy")
If Me.reportfrom.Text="01 Jan 0001" then Me.reportfrom.Text=""
Me.reportfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.reportfrom.Attributes.Add("onBlur", "clearNotice()")
Me.reportto.Text=oeligibilitythresholds.reportto.ToString("dd MMM yyyy")
If Me.reportto.Text="01 Jan 0001" then Me.reportto.Text=""
Me.reportto.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.reportto.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=oeligibilitythresholds.eligibilitythreshold_pk
Status=oeligibilitythresholds.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oeligibilitythresholds is Nothing then
oeligibilitythresholds = new Objects.eligibilitythresholds(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oeligibilitythresholds.threshold=Me.threshold.Text
oeligibilitythresholds.correctionvalue=Me.correctionvalue.Text
oeligibilitythresholds.submissionfrom=CommonFN.CheckEmptyDate(Me.submissionfrom.Text)
oeligibilitythresholds.submissionto=CommonFN.CheckEmptyDate(Me.submissionto.Text)
oeligibilitythresholds.reportfrom=CommonFN.CheckEmptyDate(Me.reportfrom.Text)
oeligibilitythresholds.reportto=CommonFN.CheckEmptyDate(Me.reportto.Text)
result=oeligibilitythresholds.Save()
if not result then throw oeligibilitythresholds.LastError
_CurrentPk=oeligibilitythresholds.eligibilitythreshold_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oeligibilitythresholds.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oeligibilitythresholds is Nothing then
   oeligibilitythresholds = new Objects.eligibilitythresholds(currentuser,actualuser,pk,CDC)
  End If
  oeligibilitythresholds.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oeligibilitythresholds is Nothing then
   oeligibilitythresholds = new Objects.eligibilitythresholds(currentuser,actualuser,pk,CDC)
  End If
  oeligibilitythresholds.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oeligibilitythresholds is Nothing then
   oeligibilitythresholds = new Objects.eligibilitythresholds(currentuser,pk,CDC)
  End If
  oeligibilitythresholds.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.threshold.Enabled=IIF(WL>=0,True,False)
  Me.blkthreshold.Visible=IIF(RL>=0,True,False)
  Me.correctionvalue.Enabled=IIF(WL>=0,True,False)
  Me.blkcorrectionvalue.Visible=IIF(RL>=0,True,False)
  Me.submissionfrom.Enabled=IIF(WL>=0,True,False)
  Me.blksubmissionfrom.Visible=IIF(RL>=0,True,False)
  Me.submissionto.Enabled=IIF(WL>=0,True,False)
  Me.blksubmissionto.Visible=IIF(RL>=0,True,False)
  Me.reportfrom.Enabled=IIF(WL>=0,True,False)
  Me.blkreportfrom.Visible=IIF(RL>=0,True,False)
  Me.reportto.Enabled=IIF(WL>=0,True,False)
  Me.blkreportto.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

