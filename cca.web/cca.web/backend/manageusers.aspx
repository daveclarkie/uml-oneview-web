<%@ Page Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="manageusers.aspx.vb" Inherits="backend_manageusers" title="Untitled Page" %>
<%@ Register Src="../controls/plugins/seekperson.ascx" TagName="seekperson" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<script language="javascript" type="text/javascript">
var user_pk=0;
</script>

    <h3>Selected User</h3>
    <input type="hidden" id="user_pk" name="user_pk" value="0" />
    <ul class="formcontrol list">
        <li><span class="label">User</span><span id="username" class="input">Not Selected</span><uc1:seekperson 
                ID="Seekperson1" runat="server" Label="Locate User" 
                TargetValueControlName="user_pk" 
                TargetDisplayControlName="username" 
                TargetFunctionName="loadgroups" /></li>
        <li><span class="label">Status</span><span id="userstatus" class="input">Not Checked</span></li>
        <li><span class="label">Read Level</span><input type="text" id="readlevel" name="readlevel" /> 0=Standard, 5+=Admin</li>
        <li><span class="label">Write Level</span><input type="text" id="writelevel" name="writelevel" /> 0=Standart, 5+=Admin</li>
        <li><span class="label">&nbsp;</span>
        <div id='img1' class="notipop" style="position:absolute;height:0px;display:none;"><img class="wait" /></div>
        <ul class="buttonlist">
            <li><a href="javascript:checkUserStatus();" id='lnk3' >Check Status</a></li>
            <li><a href="javascript:activateUser();" id='lnk1' >Activate</a></li>
            <li><a href="javascript:deactivateUser();" id='lnk2' >Deactivate</a></li>
            <li><a href="javascript:setUserRW();" id='A1' >Set R/W Level</a></li>
            <li><a href="javascript:reqGroupsAvl();" id='A2' >Group Refresh</a></li>
        </ul>
        </li>
    </ul> 

<asp:Panel ID="managegroupssection" runat="server">

    <div>
        <div class="left c2f">
                <h3 class="c2f">Assigned Groups</h3>
                
                <ul class="list" id="lstassgrp"></ul>
                 
        </div>    
       
        <div class="right c2f">
                <h3 class="c2f">Available Groups</h3>
                
                <ul class="list" id="lstavlgrp">
                </ul> 
                
        </div>
        
</div>    

</asp:Panel>

</asp:Content>

