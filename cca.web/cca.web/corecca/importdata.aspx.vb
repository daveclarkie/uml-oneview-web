Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration

Partial Class corecca_importdata
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand


    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Private Sub PageLoad()

    End Sub
    Private Sub DisplayPage()

    End Sub
    Private Sub RefreshList()

    End Sub

    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function IsStaff() As Boolean
        Return Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
    End Function
    Private Sub GetExcelSheets(ByVal FilePath As String, ByVal Extension As String, ByVal isHDR As String)
        Dim conStr As String = ""
        'Select Case Extension
        '    Case ".xls"
        '        'Excel 97-03 
        '        conStr = ConfigurationManager.ConnectionStrings("Excel03ConString").ConnectionString
        '        Exit Select
        '    Case ".xlsx"
        '        'Excel 07 
        '        conStr = ConfigurationManager.ConnectionStrings("Excel07ConString").ConnectionString
        '        Exit Select
        'End Select
        'If conStr.Length > 0 Then
        '    'Get the Sheets in Excel WorkBoo 
        '    conStr = String.Format(conStr, FilePath, isHDR)
        '    Dim connExcel As New OleDbConnection(conStr)
        '    Dim cmdExcel As New OleDbCommand()
        '    Dim oda As New OleDbDataAdapter()
        '    cmdExcel.Connection = connExcel
        '    connExcel.Open()

        '    'Bind the Sheets to DropDownList 
        '    ddlSheets.Items.Clear()
        '    ddlSheets.Items.Add(New ListItem("Select...", ""))
        '    ddlSheets.DataSource = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        '    ddlSheets.DataTextField = "TABLE_NAME"
        '    ddlSheets.DataValueField = "TABLE_NAME"
        '    ddlSheets.DataBind()
        '    connExcel.Close()
        'End If

        'txtTable.Text = ""
        lblFileName.Text = Path.GetFileName(FilePath)
        Panel2.Visible = True
        Panel1.Visible = False
    End Sub

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PageLoad()
    End Sub


    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        If FileUpload1.HasFile Then
            Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)

            Dim FilePath As String = sys.importfolder + FileName
            FileUpload1.SaveAs(FilePath)
            GetExcelSheets(FilePath, Extension, "Yes")
            ImportRecords()

        End If


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)
        ImportRecords()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs)
        Panel1.Visible = True
        Panel2.Visible = False
    End Sub
    Private Sub ImportRecords()
        CO = New SqlCommand("rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO)
        Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)

        Dim FileName As String = lblFileName.Text
        Dim Extension As String = Path.GetExtension(FileName)
        Dim FolderPath As String = sys.importfolder
        Dim CommandText As String = ""
        Select Case Extension
            Case ".xls"
                'Excel 97-03 
                CommandText = "xp_ImportFromExcel03"
                Exit Select
            Case ".xlsx"
                'Excel 07 
                CommandText = "xp_ImportFromExcel07"
                Exit Select
            Case ".csv"
                'CSV
                CommandText = "xp_ImportFromCSV"
                Exit Select
        End Select
        'Read Excel Sheet using Stored Procedure 
        'And import the data into Database Table 
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = CommandText
        CO.Parameters.Add("@SheetName", SqlDbType.VarChar).Value = ""
        CO.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = FolderPath + FileName
        CO.Parameters.Add("@HDR", SqlDbType.VarChar).Value = "Yes"
        CO.Parameters.Add("@TableName", SqlDbType.VarChar).Value = "_import_" + CStr(SM.currentuser)
        Try
            Dim count As Integer = 0
            CDC.Execute(CO)

            CO = New SqlCommand("rsp_dataimport_bulk")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.Add("@TableName", SqlDbType.VarChar).Value = "_import_" + CStr(SM.currentuser)
            Dim dt As New DataTable
            dt = CDC.ReadDataTable(CO)
            For Each dr As DataRow In dt.Rows
                If dr(6) = -1 Then 'Throughput
                    Dim dtp As New datathroughputs(SM.currentuser, SM.actualuser, CDC)
                    dtp.throughput_fk = dr(1)
                    dtp.month_fk = dr(2)
                    dtp.year_fk = dr(3)
                    dtp.totalthroughput = dr(4)
                    dtp.eligiblethroughput = dr(5)
                    dtp.dataimportmethod_fk = 2 'Manual Import
                    If Not dtp.Exists Then
                        dtp.Save()
                        count += 1
                    End If
                Else 'Consumption
                    CO = New SqlCommand("rsp_lkp_meterpoints_meter")
                    CO.CommandType = CommandType.StoredProcedure
                    CO.Parameters.Add("@meter", SqlDbType.VarChar).Value = dr(1)
                    Dim meterpoint_pk As String = ""
                    CDC.ReadScalarValue(meterpoint_pk, CO)

                    If Not meterpoint_pk Is Nothing Then
                        Dim dco As New dataconsumptions(SM.currentuser, SM.actualuser, CDC)
                        dco.meterpoint_fk = meterpoint_pk
                        dco.month_fk = dr(2)
                        dco.year_fk = dr(3)
                        dco.totalconsumption = dr(4)
                        dco.eligibleconsumption = dr(5)
                        dco.dataimportmethod_fk = 2 'Manual Import
                        If Not dco.Exists Then
                            dco.Save()

                            CO = New SqlCommand
                            CO.CommandType = CommandType.Text
                            CO.CommandText = "DELETE FROM [se_cca].[dbo].[system_missingdataconsumptions] WHERE [meterpoint_fk] = '" & meterpoint_pk & "' AND [month] = '01 " & MonthName(dr(2)) & " " & dr(3) & "'"
                            CDC.Execute(CO)

                            count += 1
                        End If
                    End If

                End If
            Next

            If System.IO.File.Exists(FolderPath + FileName) Then
                System.IO.File.Delete(FolderPath + FileName)
            End If

            lblMessage.ForeColor = System.Drawing.Color.Green
            lblMessage.Text = CStr(count) & " records inserted."
        Catch ex As Exception
            lblMessage.ForeColor = System.Drawing.Color.Red
            lblMessage.Text = ex.Message
        Finally
            CO.Dispose()
            Panel1.Visible = True
            Panel2.Visible = False
        End Try
    End Sub

End Class
