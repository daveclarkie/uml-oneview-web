Partial Class fuelconversions_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents ofuelconversions As Objects.fuelconversions
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_fuels_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.sourcefuel_fk.Items.Count = 0 Then
            Me.sourcefuel_fk.DataSource = CDC.ReadDataTable(CO)
            Me.sourcefuel_fk.DataTextField = "value"
            Me.sourcefuel_fk.DataValueField = "pk"
            Try
                Me.sourcefuel_fk.DataBind()
            Catch Ex As Exception
                Me.sourcefuel_fk.SelectedValue = -1
                Me.sourcefuel_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_units_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.sourceunit_fk.Items.Count = 0 Then
            Me.sourceunit_fk.DataSource = CDC.ReadDataTable(CO)
            Me.sourceunit_fk.DataTextField = "value"
            Me.sourceunit_fk.DataValueField = "pk"
            Try
                Me.sourceunit_fk.DataBind()
            Catch Ex As Exception
                Me.sourceunit_fk.SelectedValue = -1
                Me.sourceunit_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_fuels_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.targetfuel_fk.Items.Count = 0 Then
            Me.targetfuel_fk.DataSource = CDC.ReadDataTable(CO)
            Me.targetfuel_fk.DataTextField = "value"
            Me.targetfuel_fk.DataValueField = "pk"
            Try
                Me.targetfuel_fk.DataBind()
            Catch Ex As Exception
                Me.targetfuel_fk.SelectedValue = -1
                Me.targetfuel_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_units_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.targetunit_fk.Items.Count = 0 Then
            Me.targetunit_fk.DataSource = CDC.ReadDataTable(CO)
            Me.targetunit_fk.DataTextField = "value"
            Me.targetunit_fk.DataValueField = "pk"
            Try
                Me.targetunit_fk.DataBind()
            Catch Ex As Exception
                Me.targetunit_fk.SelectedValue = -1
                Me.targetunit_fk.DataBind()
            End Try
        End If
        Me.conversionfactor.Text = "0.00"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If ofuelconversions Is Nothing Then
            ofuelconversions = New Objects.fuelconversions(currentuser, actualuser, pk, CDC)
        End If
        Me.sourcefuel_fk.SelectedValue = ofuelconversions.sourcefuel_fk
        Me.sourceunit_fk.SelectedValue = ofuelconversions.sourceunit_fk
        Me.targetfuel_fk.SelectedValue = ofuelconversions.targetfuel_fk
        Me.targetunit_fk.SelectedValue = ofuelconversions.targetunit_fk
        Me.validfrom.Text = ofuelconversions.validfrom.ToString("dd MMM yyyy")
        If Me.validfrom.Text = "01 Jan 0001" Then Me.validfrom.Text = ""
        Me.validfrom.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.validfrom.Attributes.Add("onBlur", "clearNotice()")
        Me.validto.Text = ofuelconversions.validto.ToString("dd MMM yyyy")
        If Me.validto.Text = "01 Jan 0001" Then Me.validto.Text = ""
        Me.validto.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.validto.Attributes.Add("onBlur", "clearNotice()")
        Me.conversionfactor.Text = ofuelconversions.conversionfactor
        Me.conversionfactor.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.conversionfactor.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = ofuelconversions.fuelconversion_pk
        Status = ofuelconversions.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ofuelconversions Is Nothing Then
            ofuelconversions = New Objects.fuelconversions(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            ofuelconversions.sourcefuel_fk = Me.sourcefuel_fk.SelectedValue
            ofuelconversions.sourceunit_fk = Me.sourceunit_fk.SelectedValue
            ofuelconversions.targetfuel_fk = Me.targetfuel_fk.SelectedValue
            ofuelconversions.targetunit_fk = Me.targetunit_fk.SelectedValue
            ofuelconversions.validfrom = CommonFN.CheckEmptyDate(Me.validfrom.Text)
            ofuelconversions.validto = CommonFN.CheckEmptyDate(Me.validto.Text)
            ofuelconversions.conversionfactor = Me.conversionfactor.Text
            result = ofuelconversions.Save()
            If Not result Then Throw ofuelconversions.LastError
            _CurrentPk = ofuelconversions.fuelconversion_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = ofuelconversions.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ofuelconversions Is Nothing Then
            ofuelconversions = New Objects.fuelconversions(currentuser, actualuser, pk, CDC)
        End If
        ofuelconversions.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ofuelconversions Is Nothing Then
            ofuelconversions = New Objects.fuelconversions(currentuser, actualuser, pk, CDC)
        End If
        ofuelconversions.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ofuelconversions Is Nothing Then
            ofuelconversions = New Objects.fuelconversions(currentuser, pk, CDC)
        End If
        ofuelconversions.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.sourcefuel_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blksourcefuel_fk.Visible = IIF(RL >= 0, True, False)
        Me.sourceunit_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blksourceunit_fk.Visible = IIF(RL >= 0, True, False)
        Me.targetfuel_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetfuel_fk.Visible = IIF(RL >= 0, True, False)
        Me.targetunit_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetunit_fk.Visible = IIF(RL >= 0, True, False)
        Me.validfrom.Enabled = IIF(WL >= 0, True, False)
        Me.blkvalidfrom.Visible = IIF(RL >= 0, True, False)
        Me.validto.Enabled = IIF(WL >= 0, True, False)
        Me.blkvalidto.Visible = IIF(RL >= 0, True, False)
        Me.conversionfactor.Enabled = IIF(WL >= 0, True, False)
        Me.blkconversionfactor.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

