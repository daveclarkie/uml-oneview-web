﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="throughputs.ascx.vb" Inherits="controls_nav_throughputs" %>
 <%If LoadList() Then%>
    <div class="navigationmenu" id="pnlThroughputs" runat="server">
        <h4>Throughputs</h4>
        
        <asp:Repeater ID="rptThroughputs" runat="server">
            <HeaderTemplate>
                <table>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width:130px;"><%# IIf(Len(Container.DataItem("throughputname")) > 20, Left(Container.DataItem("throughputname"), 17) & "...", Container.DataItem("throughputname"))%></td>
                    <td style="width:100px;"><%# Container.DataItem("unitname")%></td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
<%End If%>