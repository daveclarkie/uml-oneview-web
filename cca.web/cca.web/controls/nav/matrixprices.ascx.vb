﻿
Partial Class controls_nav_matrixprices
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim CO As SqlClient.SqlCommand
    Dim dt As DataTable

    Dim iMatrix As Integer = 0

    Public Function LoadList() As Boolean
        If isViable() Then

            Dim m As New matrix(SM.currentuser, SM.actualuser, SM.matrix, CDC)

            CO = New SqlCommand("rsp_matrixprices_currentmatrix")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
            dt = CDC.ReadDataTable(CO)
            rptPrices.DataSource = dt
            rptPrices.DataBind()

            Return True
        Else
            Return False
        End If
    End Function


    Private Function isViable() As Boolean
        Try

            If Request.RawUrl.ToLower.Contains("/corematrix/") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function

End Class
