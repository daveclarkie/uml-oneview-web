Partial Class corepersons_addperson
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Dim cc As Object
    Dim pk As Integer = 0
    Dim spk As Integer = 0

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Private Sub RefreshList()

    End Sub

    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control, ByVal isPerson As Boolean)
        TargetControl.Controls.Clear()
        If isPerson Then
            cc = LoadControl(controlpath)
            cc.CDC = CDC
            cc.SM = SM

            CO = New SqlCommand("rsp_personfk_fromuserfk")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@user", SM.targetuser)
            Dim person_pk As Integer = -1
            CDC.ReadScalarValue(person_pk, CO)

            cc.Read(SM.targetuser, SM.actualuser, person_pk)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        End If
    End Sub
    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Integer = False
        Try
            If primary = True Then
                resDet = cc.Save(SM.currentuser, SM.actualuser, -1)
            Else
                resDet = cc.Save(SM.currentuser, SM.actualuser, spk)
            End If

            If resDet = 0 Then
                Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            Else
                AddNotice("setNotice('Person Saved');")
                If Not Request.QueryString("spk") Is Nothing Then
                    CancelContol()
                End If
                Dim filtered As New NameValueCollection(Request.QueryString)
                Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
                nameValueCollection.Remove("spk")
                'Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString + "&r=Record+Saved"
                Dim url As String = "/core/persons.aspx?pk=" & resDet & "&pg=ps" & Local.noCache
                Response.Redirect(url)
                'RefreshList()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("spk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub
    '================================
    '   "Validation" Procs
    '================================



    '================================
    '   Page Specific Events
    '================================


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetControl("~/controls/custom/custom_addpersons_ctl.ascx", personControl, personButton, True)

    End Sub

    Protected Sub btnSavePerson_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePerson.Click

        ' Check that the fields have been completed?




            AddNotice("setNotice('Blah');")
            AddNotice("setNotice('Blah2');")

        SaveContol(True)
    End Sub



End Class
