
Partial Class backend_stats
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CO As New SqlClient.SqlCommand
        Dim SIT As SystemItemType
        Try
            SIT = CType(Request.QueryString("s"), SystemItemType)
            AddNotice("setNotice('" & SIT.ToString & "');")
        Catch ex As Exception
            AddNotice("setNotice('Argh!!!!');")
            Exit Sub
        End Try

        Select Case SIT
            Case SystemItemType.ActiveUsers
                CO.CommandText = "rpt_sys_activeusers"
            Case SystemItemType.ActiveSessions
                CO.CommandText = "rpt_sys_sessions"
            Case SystemItemType.DormantAccounts
                CO.CommandText = "rpt_sys_dormantaccounts"
            Case SystemItemType.FailedLogins
                CO.CommandText = "rpt_sys_failedlogins"
            Case SystemItemType.MultipleLogins
                CO.CommandText = "rpt_sys_multiplelogins"
        End Select

        CO.CommandType = CommandType.StoredProcedure
        grdStats.DataSource = CDC.ReadDataTable(CO)
        grdStats.DataBind()


    End Sub

    Protected Sub grdStats_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStats.RowDataBound
        Dim c As TableCell
        For Each c In e.Row.Cells
            c.Text = Context.Server.HtmlDecode(c.Text)
        Next
    End Sub
End Class
