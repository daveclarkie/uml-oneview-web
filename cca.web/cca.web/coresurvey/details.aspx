﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="details.aspx.vb" Inherits="coresurvey_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <h3><asp:Label ID="lbl_pageheader" runat="server"></asp:Label></h3>
    <br />
    <table width="800px" class="nohover">
        <tr>
            <td>
                <asp:Panel ID="pnl_survey" runat="server" style="width:350px; ">
                    <asp:Label ID="lbl_survey1_0" runat="server"></asp:Label><br />
                    <br />
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_1" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps1" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_1" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
            
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_2" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps2" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_2" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
            
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_3" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps3" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_3" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
            
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_4" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps4" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_4" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
            
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_5" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps5" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_5" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
            
                    <h5 style="width:350px;"><asp:Label ID="lbl_survey1_6" runat="server"></asp:Label></h5>
                    <table style="width:350px;">
                        <tr>
                            <td style="width:250px;"><h4>NPS Score</h4></td>
                            <td style="width:100px;" colspan="2"><h4><asp:Label ID="lbl_survey1_nps6" runat="server"></asp:Label> %</h4></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rpt_survey1_6" runat="server">
                        <HeaderTemplate>
                            <table style="width:350px;">
                                <tr>
                                    <td style="width:250px;"><h5>Answer</h5></td>
                                    <td style="width:100px;" colspan="2"><h5>Responses</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("surveyanswername")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:right; width: 50px;"><%# Format(Container.DataItem("percentage"), "0.00")%> %</td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center; width: 50px;"><%# Container.DataItem("answers")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
        
                </asp:Panel>
                <asp:Panel ID="pnl_suppliersurvey" runat="server">
                    <asp:Label ID="lblsuppliersurvey_type" runat="server"></asp:Label>
                    <br />
                    <br />
                    <asp:Repeater ID="rpt_suppliersurveys" runat="server">
                        <HeaderTemplate>
                            <table style="width:700px;">
                                <tr>
                                    <td style="width:250px;"><h5>User</h5></td>
                                    <td style="width:100px;"><h5>Sent</h5></td>
                                    <td style="width:100px;"><h5>Status</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr style='background-color:<%# Container.DataItem("backcolour")%>'>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("username")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("datesent")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("statusname")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                </asp:Panel>
            </td>
            <td>
                <asp:Panel ID="pnl_details" runat="server" style="width:450px; height:100%;" ScrollBars="Vertical">
                <br />
                <br />
                    <h4>Summary</h4>
                    <asp:Repeater ID="rpt_surveysummarytype" runat="server">
                        <HeaderTemplate>
                            <table style="width:430px;">
                                <tr>
                                    <td style="width:200px;"><h5>Status</h5></td>
                                    <td style="width:50px;"><h5>Surveys</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("name")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle; text-align:center;   "><%# Container.DataItem("value")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                    <h4>Surveys to be completed...</h4>
                    <asp:Repeater ID="rpt_surveytocomplete" runat="server">
                        <HeaderTemplate>
                            <table style="width:430px;">
                                <tr>
                                    <td style="width:80px;"><h5>User</h5></td>
                                    <td style="width:50px;"><h5>Commodity</h5></td>
                                    <td style="width:80px;"><h5>Country</h5></td>
                                    <td style="width:80px;"><h5>Created</h5></td>
                                </tr>
                        </HeaderTemplate> 
                        <ItemTemplate>
                                <tr>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("user")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("commodity")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("country")%></td>
                                    <td style="text-indent:5px;height:20px; vertical-align:middle;"><%# Container.DataItem("created")%></td>
                                </tr>
                        </ItemTemplate> 
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </td>

        </tr>
    </table>
   
    
</asp:Content>

