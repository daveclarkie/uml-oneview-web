<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="customers.aspx.vb" Inherits="corecm_customers" title="bubbles" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">
        
        <%=IIf(CustomerStatus() = 1, "<h3>This is an Active Customer</h3> ", "<h6 style='background-color:red;'>This is a Prospect</h6>")%>
        
        <ul id="innertab">
            <li class='<%=LIi("search")%>'><a href="search.aspx?<%=Local.noCache %>">New Search</a></li>
            <li class='<%=LIi("customer")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=cu<%=Local.noCache %>">Organisation</a></li>
            <li class='<%=LIi("site")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=si<%=Local.noCache %>">Site</a></li>
            <li class='<%=LIi("serviceagreements")%>'><a href="customers.aspx?pk=<%=PSM.mainid %>&pg=sa<%=Local.noCache %>">Service Agreements</a></li>
        </ul>

        <asp:MultiView runat="server" ID="mvDetail">
            <asp:View ID="vwCustomer" runat="server">
                <asp:PlaceHolder ID="OrganisationsControl" runat="server" />
                <table id="OrganisationsButton" runat="server"></table>
                <br />
                <h5>Site List</h5>
                <asp:Repeater ID="rptSites" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td style="width:50px;">
                        
                                </td>
                                <td style="width:50px;">
                                    <h4>Site ID</h4>
                                </td>
                                <td style="width:250px;">
                                    <h4>Site Name</h4>
                                </td>
                                <td style="width:180px;">
                                    <h4>Address</h4>
                                </td>
                                <td style="width:80px;">
                                    <h4>Post Code</h4>
                                </td>
                                <td></td>
                            </tr>
                    </HeaderTemplate> 

                    <ItemTemplate>
                            <tr>
                            <td style="text-indent:5px;">
                                <a href='customers.aspx?pg=si&pk=<%# sm.mainid %>&spk=<%# Container.DataItem("site_pk") %>'><h6>select</h6></a>
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("site_pk")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("sitename")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("address_1")%> 
                            </td>
                            <td style="text-indent:5px;">
                                <%# Container.DataItem("pcode")%> 
                            </td>
                        </tr>
                    </ItemTemplate> 

                    <FooterTemplate>
                            <tr style="height:5px;">
                            </tr>
                        </table>
                    </FooterTemplate> 
                </asp:Repeater>
                
            </asp:View>
            <asp:View ID="vwSiteInformation" runat="server">
                <asp:PlaceHolder ID="SiteInformationControl" runat="server" />
                <table id="SiteInformationButton" runat="server">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveSiteInformation" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton ID="btnCancelSiteInformation" runat="server" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="vwMeterInformation" runat="server">
            
                <asp:PlaceHolder ID="MeterInformationControl" runat="server" />
                <table id="MeterInformationButton" runat="server">
                    <tr style="height:10px;">
                            
                    </tr>
                    <tr>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton runat="server" ID="btnSaveMeterInformation" Text="<h6>Save</h6>" />
                        </td>
                        <td style="width:60%;"></td>
                        <td style="text-align:center; width:20%;">
                            <asp:Linkbutton ID="btnCancelMeterInformation" runat="server" Text="<h4>Cancel</h4>" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="vwServiceAgreements" runat="server">
            
            </asp:View>
        </asp:MultiView>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="CustomerNav" runat="server" />

    </div>

</div>

</asp:Content>

