Partial Class logreportautomations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ologreportautomations As Objects.logreportautomations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_reportautomations_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.reportautomation_fk.Items.Count=0 Then
        Me.reportautomation_fk.DataSource=CDC.ReadDataTable(CO)
        Me.reportautomation_fk.DataTextField = "value"
        Me.reportautomation_fk.DataValueField = "pk"
        Try
            Me.reportautomation_fk.DataBind
        Catch Ex as Exception
            Me.reportautomation_fk.SelectedValue=-1
            Me.reportautomation_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ologreportautomations is Nothing then
ologreportautomations = new Objects.logreportautomations(currentuser,actualuser,pk,CDC)
End If
Me.reportautomation_fk.SelectedValue=ologreportautomations.reportautomation_fk
_CurrentPk=ologreportautomations.logreportautomation_pk
Status=ologreportautomations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ologreportautomations is Nothing then
ologreportautomations = new Objects.logreportautomations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ologreportautomations.reportautomation_fk=Me.reportautomation_fk.SelectedValue
result=ologreportautomations.Save()
if not result then throw ologreportautomations.LastError
_CurrentPk=ologreportautomations.logreportautomation_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ologreportautomations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreportautomations is Nothing then
   ologreportautomations = new Objects.logreportautomations(currentuser,actualuser,pk,CDC)
  End If
  ologreportautomations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreportautomations is Nothing then
   ologreportautomations = new Objects.logreportautomations(currentuser,actualuser,pk,CDC)
  End If
  ologreportautomations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ologreportautomations is Nothing then
   ologreportautomations = new Objects.logreportautomations(currentuser,pk,CDC)
  End If
  ologreportautomations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.reportautomation_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkreportautomation_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

