Imports System.Data.SqlClient

Partial Class controls_widgets_internaltasks
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim dt As DataTable
    Dim CO As SqlCommand

    Dim _Messages As String = ""
    Public Property Messages() As String
        Get
            Return _Messages
        End Get
        Set(ByVal value As String)
            If value = "__clear__" Then
                _Messages = ""
            Else
                _Messages &= value
                _Messages &= vbCrLf
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Security.GroupMember(SM.actualuser, SystemGroups.Staff, CDC) And Security.GroupMember(SM.actualuser, SystemGroups.Managers, CDC) Then
            Dim dt As DataTable
            Dim CO As SqlCommand
            CO = New SqlCommand("rsp_overdueinternaltasks")
            CO.CommandType = CommandType.StoredProcedure
            dt = CDC.ReadDataTable(CO)
            CO.Dispose()
            GridView1.DataSource = dt
            GridView1.DataBind()
        Else
            Me.Visible = False
        End If

    End Sub

End Class
