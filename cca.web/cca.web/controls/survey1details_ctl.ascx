<%@ Control Language="VB" AutoEventWireup="false" CodeFile="survey1details_ctl.ascx.vb" Inherits="survey1details_ctl" %>
<h4 style="width:555px;">CM Survey - 001</h4>
<ul class='formcontrol'>
<li runat="server" id="blkintro">
<span class='label'><strong>I was satisfied...</strong></span>
<asp:TextBox EnableViewState="false" ID="lblintro" runat="server" style="border:0px none black ; background-color:transparent;" Enabled="false" ForeColor="Transparent" />
</li>
<li runat="server" id="blkhelpdesk_id">
<span class='label'>Helpdesk ID<span class='pop'>The request id of the ticket from helpdesk</span></span>
<asp:TextBox EnableViewState="false" ID="helpdesk_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkselecteduser_fk">
<span class='label'>User<span class='pop'>The user_fk of the user who should be doing the survey</span></span>
<asp:DropDownList ID="selecteduser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion1_surveyanswerfivescale_fk">
<span class='label'>With the accuracy of the SSR<span class='pop'></span></span>
<asp:DropDownList ID="question1_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion2_surveyanswerfivescale_fk">
<span class='label'>With the quality format of the SSR<span class='pop'></span></span>
<asp:DropDownList ID="question2_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion3_surveyanswerfivescale_fk">
<span class='label'>With the direction/recommendation<span class='pop'>Provided by the Sourcing Team</span></span>
<asp:DropDownList ID="question3_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion4_surveyanswerfivescale_fk">
<span class='label'>With the speed of response<span class='pop'>From the Sourcing Team</span></span>
<asp:DropDownList ID="question4_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion5_surveyanswerfivescale_fk">
<span class='label'>With the professionalism from Sourcing<span class='pop'>That the Sourcing team conducted themselves professionally</span></span>
<asp:DropDownList ID="question5_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkcomments">
<span class='label'>Comments<span class='pop'>Any additional comments</span></span>
<asp:TextBox EnableViewState="false" ID="comments" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkquestion6_surveyanswerfivescale_fk">
<span class='label'>The Client was satisfied<span class='pop'>With the overall quality of the Sourcing service</span></span>
<asp:DropDownList ID="question6_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkcomments2">
<span class='label'>Comments<span class='pop'>Any additional comments</span></span>
<asp:TextBox EnableViewState="false" ID="comments2" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
</ul>

