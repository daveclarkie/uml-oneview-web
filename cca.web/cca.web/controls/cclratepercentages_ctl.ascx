<%@ Control Language="VB" AutoEventWireup="false" CodeFile="cclratepercentages_ctl.ascx.vb" Inherits="cclratepercentages_ctl" %>
<h5>cclratepercentages</h5>
<ul class='formcontrol'>
<li runat="server" id="blkfuel_fk">
<span class='label'>fuel_fk</span>
<asp:DropDownList ID="fuel_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkvalid_from">
<span class='label'>valid_from</span>
<asp:TextBox EnableViewState="false" ID="valid_from" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkvalid_to">
<span class='label'>valid_to</span>
<asp:TextBox EnableViewState="false" ID="valid_to" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkcclpercentage">
<span class='label'>cclpercentage</span>
<asp:TextBox EnableViewState="false" ID="cclpercentage" runat="server" cssClass="input_dbl" />
</li>
</ul>

