<%@ Control Language="VB" AutoEventWireup="false" CodeFile="agreementmeterpoints_ctl.ascx.vb" Inherits="agreementmeterpoints_ctl" %>
<h5>Meter Point Details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkagreement_fk">
<span class='label'>Agreement</span>
<asp:DropDownList ID="agreement_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmeterpoint_fk">
<span class='label'>Meter</span>
<asp:DropDownList ID="meterpoint_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkdatafrequency_fk">
<span class='label'>Data Frequency<span class='pop'>As a guide, how often should the data / invoices be received</span></span>
<asp:DropDownList ID="datafrequency_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkagreementthroughput_fk">
<span class='label'>Associated Throughput<span class='pop'>Is this consumption point associated directly to a throughput</span></span>
<asp:DropDownList ID="agreementthroughput_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

