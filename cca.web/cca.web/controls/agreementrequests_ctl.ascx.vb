Partial Class agreementrequests_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreementrequests As Objects.agreementrequests
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        Me.helpdesk_id.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()

        If oagreementrequests Is Nothing Then
            oagreementrequests = New Objects.agreementrequests(currentuser, actualuser, pk, CDC)
        End If
        If pk = -1 Then
            Me.agreement_fk.SelectedValue = SM.targetAgreement
        Else
            Me.agreement_fk.SelectedValue = oagreementrequests.agreement_fk
        End If
        Me.helpdesk_id.Text = oagreementrequests.helpdesk_id
        Me.helpdesk_id.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.helpdesk_id.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = oagreementrequests.agreementrequest_pk
        Status = oagreementrequests.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementrequests Is Nothing Then
            oagreementrequests = New Objects.agreementrequests(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreementrequests.agreement_fk = Me.agreement_fk.SelectedValue
            oagreementrequests.helpdesk_id = Me.helpdesk_id.Text
            result = oagreementrequests.Save()
            If Not result Then Throw oagreementrequests.LastError
            _CurrentPk = oagreementrequests.agreementrequest_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreementrequests.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementrequests Is Nothing Then
            oagreementrequests = New Objects.agreementrequests(currentuser, actualuser, pk, CDC)
        End If
        oagreementrequests.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementrequests Is Nothing Then
            oagreementrequests = New Objects.agreementrequests(currentuser, actualuser, pk, CDC)
        End If
        oagreementrequests.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementrequests Is Nothing Then
            oagreementrequests = New Objects.agreementrequests(currentuser, pk, CDC)
        End If
        oagreementrequests.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.agreement_fk.Enabled = IIf(WL >= 99, True, False)
        Me.blkagreement_fk.Visible = IIf(RL >= 99, True, False)
        Me.helpdesk_id.Enabled = IIF(WL >= 0, True, False)
        Me.blkhelpdesk_id.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

