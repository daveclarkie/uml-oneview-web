<%@ Control Language="VB" AutoEventWireup="false" CodeFile="users.ascx.vb" Inherits="navctls_users" %>
        <h3><%=Label%><a href="javascript:showhide('lst<%=Label.replace(" ","_")%>','lnk<%=Label.replace(" ","_")%>');" id="lnk<%=Label.replace(" ","_")%>">[Show]</a></h3>
        <ul class="navigationmenu" id="lst<%=Label.replace(" ","_")%>" style="display:none;">
            <asp:Repeater ID="rptuser" runat="server" EnableViewState="false">
            <ItemTemplate>
            <li>
                <a href='persons.aspx?pk=<%# Container.DataItem("user_pk") %>&md=ps<%=Local.noCache%>'><span><%# Container.DataItem("username") %></span></a><br />
                <span class="item"><%#Container.DataItem("mainaddress")%></span>
            </li>
            </ItemTemplate> 
            </asp:Repeater>
        </ul>
