<%@ Control Language="VB" AutoEventWireup="false" CodeFile="custom_addpersons_ctl.ascx.vb" Inherits="custom_addpersons_ctl" %>
<h5>Add New Person</h5>
<ul class='formcontrol'>
<li runat="server" id="blktitle_fk">
<span class='label'>Title</span>
<asp:DropDownList ID="title_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkforename">
<span class='label'>Forename</span>
<asp:TextBox EnableViewState="false" ID="forename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkmiddlename">
<span class='label'>middle name</span>
<asp:TextBox EnableViewState="false" ID="middlename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blksurname">
<span class='label'>surname</span>
<asp:TextBox EnableViewState="false" ID="surname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkjobtitle">
<span class='label'>job title</span>
<asp:TextBox EnableViewState="false" ID="jobtitle" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkorganisation_fk">
<span class='label'>Organisation</span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkemail">
<span class='label'>Email Address</span>
<asp:TextBox EnableViewState="false" ID="email" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

