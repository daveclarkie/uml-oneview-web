Imports System.Data.SqlClient

Public Class DavePage
    Inherits System.Web.UI.Page
    Public CDC As DataCommon
    'Public CDCH As DataCommonHelpdesk
    Public SM As MySM

    Public Event InitSpecificPage()
    Public Event CompleteSpecificPage()

    Dim CO As SqlCommand
    Public ntc As String = ""
    Dim l As Literal

    Private Sub InitNotice()
        ntc = "<script language=""javascript"">" & vbCrLf & "function myAlerts()" & vbCrLf & "{" & vbCrLf
    End Sub

    Private Sub CommitNotice()
        ntc = ntc & "}" & vbCrLf & "setOnLoad(myAlerts);" & vbCrLf & "</script>" & vbCrLf
        l = New Literal
        l.Text = ntc
        Me.Controls.Add(l)
    End Sub

    Public Sub AddNotice(ByVal Notice As String)
        ntc &= Notice.Replace(vbCrLf, "<br />")
        ntc &= vbCrLf
    End Sub

    Public Sub ShareCDCSM(ByRef xCDC As DataCommon, ByRef xSM As MySM)
        xCDC = CDC
        xSM = SM
    End Sub

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        CDC = New DataCommon(Local.AuthURL)
        'CDCH = New DataCommonHelpdesk(Local.AuthURL)
        SM = New MySM(Session.SessionID, Request, CDC)
        MyBase.OnPreInit(e)
        InitNotice()
        RaiseEvent InitSpecificPage()
    End Sub

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        RaiseEvent CompleteSpecificPage()
        CommitNotice()
        MyBase.OnLoadComplete(e)
    End Sub

    Protected Overrides Sub OnPreRenderComplete(ByVal e As System.EventArgs)
        MyBase.OnPreRenderComplete(e)
    End Sub

    Protected Overrides Sub OnUnload(ByVal e As System.EventArgs)
        If Not SM Is Nothing Then
            SM.Save()
            SM = Nothing
        End If
        CDC = Nothing
        'CDCH = Nothing
        MyBase.OnUnload(e)
    End Sub

    Public Property UpdateControl() As Object
        Get
            Return _UpdateControl
        End Get
        Set(ByVal value As Object)
            _UpdateControl = value
        End Set
    End Property

    Dim _UpdateControl As Object = Nothing

    Public Sub SetUpdateControl(ByRef updateControl As Object)
        _UpdateControl = updateControl
    End Sub

    'Protected Sub InitSpecificPage()
    '    ' Place any page specific initialisation code here
    'End Sub

    'Protected Sub CompleteSpecificPage()
    '    ' Place any page specific initialisation code here
    'End Sub

End Class
