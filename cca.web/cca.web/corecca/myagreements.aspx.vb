
Partial Class corecca_myagreements
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Dim lbAvailableMeters As Object

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_myagreements"
        CO.Parameters.AddWithValue("@user_fk", SM.currentuser)
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        rptMyAgreements.DataSource = dt
        rptMyAgreements.DataBind()
        CO.Dispose()
    End Sub

  

End Class
