Partial Class sites_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents osites As Objects.sites
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_organisations_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.organisation_fk.Items.Count = 0 Then
            Me.organisation_fk.DataSource = CDC.ReadDataTable(CO)
            Me.organisation_fk.DataTextField = "value"
            Me.organisation_fk.DataValueField = "pk"
            Try
                Me.organisation_fk.DataBind()
            Catch Ex As Exception
                Me.organisation_fk.SelectedValue = -1
                Me.organisation_fk.DataBind()
            End Try
        End If
        Me.deadoralive.Text = "0"
        Me.Accmgrid.Text = "0"
        Me.CountyID.Text = "0"
        Me.StatusID.Text = "0"
        Me.Sameasbillingadress.Text = "0"
        Me.voltageID.Text = "0"
        Me.MaxDemand.Text = "0"
        Me.prodelecneg.Text = "0"
        Me.prodgasneg.Text = "0"
        Me.produtilisys.Text = "0"
        Me.prodCRC.Text = "0"
        Me.prodWater.Text = "0"
        Me.federationid.Text = "0"
        Me.fuelid.Text = "0"
        Me.CSC.Text = "0"
        Me.FKsitegroupid.Text = "0"
        Me.Dservices_optout.Text = "0"
        Me.Bill_check_optout.Text = "0"
        Me.CCL_mgmt.Text = "0"
        Me.Lat.Text = "0.00"
        Me.Long.Text = "0.00"
        Me.intCountryFK.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If osites Is Nothing Then
            osites = New Objects.sites(currentuser, actualuser, pk, CDC)
        End If
        Me.organisation_fk.SelectedValue = osites.organisation_fk
        Me.sitename.Text = osites.sitename
        Me.deadoralive.Text = osites.deadoralive
        Me.deadoralive.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.deadoralive.Attributes.Add("onBlur", "clearNotice()")
        Me.ContactName.Text = osites.ContactName
        Me.Phone_1.Text = osites.Phone_1
        Me.FaxNumber.Text = osites.FaxNumber
        Me.Accmgrid.Text = osites.Accmgrid
        Me.Accmgrid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.Accmgrid.Attributes.Add("onBlur", "clearNotice()")
        Me.Address_1.Text = osites.Address_1
        Me.Address_2.Text = osites.Address_2
        Me.Address_3.Text = osites.Address_3
        Me.Address_4.Text = osites.Address_4
        Me.CountyID.Text = osites.CountyID
        Me.CountyID.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CountyID.Attributes.Add("onBlur", "clearNotice()")
        Me.Pcode.Text = osites.Pcode
        Me.Email.Text = osites.Email
        Me.StatusID.Text = osites.StatusID
        Me.StatusID.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.StatusID.Attributes.Add("onBlur", "clearNotice()")
        Me.Customersiteref.Text = osites.Customersiteref
        Me.Tenderstdt.Text = osites.Tenderstdt.ToString("dd MMM yyyy")
        If Me.Tenderstdt.Text = "01 Jan 0001" Then Me.Tenderstdt.Text = ""
        Me.Tenderstdt.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.Tenderstdt.Attributes.Add("onBlur", "clearNotice()")
        Me.Tenderlastdt.Text = osites.Tenderlastdt.ToString("dd MMM yyyy")
        If Me.Tenderlastdt.Text = "01 Jan 0001" Then Me.Tenderlastdt.Text = ""
        Me.Tenderlastdt.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.Tenderlastdt.Attributes.Add("onBlur", "clearNotice()")
        Me.tenderprtdate.Text = osites.tenderprtdate.ToString("dd MMM yyyy")
        If Me.tenderprtdate.Text = "01 Jan 0001" Then Me.tenderprtdate.Text = ""
        Me.tenderprtdate.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.tenderprtdate.Attributes.Add("onBlur", "clearNotice()")
        Me.tenderclsddate.Text = osites.tenderclsddate.ToString("dd MMM yyyy")
        If Me.tenderclsddate.Text = "01 Jan 0001" Then Me.tenderclsddate.Text = ""
        Me.tenderclsddate.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.tenderclsddate.Attributes.Add("onBlur", "clearNotice()")
        Me.Notes.Text = osites.Notes
        Me.Sameasbillingadress.Text = osites.Sameasbillingadress
        Me.Sameasbillingadress.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.Sameasbillingadress.Attributes.Add("onBlur", "clearNotice()")
        Me.tendbenchmark.Text = osites.tendbenchmark
        Me.typeofop.Text = osites.typeofop
        Me.voltageID.Text = osites.voltageID
        Me.voltageID.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.voltageID.Attributes.Add("onBlur", "clearNotice()")
        Me.MaxDemand.Text = osites.MaxDemand
        Me.MaxDemand.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.MaxDemand.Attributes.Add("onBlur", "clearNotice()")
        Me.prodelecneg.Text = osites.prodelecneg
        Me.prodelecneg.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodelecneg.Attributes.Add("onBlur", "clearNotice()")
        Me.prodgasneg.Text = osites.prodgasneg
        Me.prodgasneg.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodgasneg.Attributes.Add("onBlur", "clearNotice()")
        Me.produtilisys.Text = osites.produtilisys
        Me.produtilisys.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.produtilisys.Attributes.Add("onBlur", "clearNotice()")
        Me.prodCRC.Text = osites.prodCRC
        Me.prodCRC.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodCRC.Attributes.Add("onBlur", "clearNotice()")
        Me.prodWater.Text = osites.prodWater
        Me.prodWater.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.prodWater.Attributes.Add("onBlur", "clearNotice()")
        Me.federationid.Text = osites.federationid
        Me.federationid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.federationid.Attributes.Add("onBlur", "clearNotice()")
        Me.fuelid.Text = osites.fuelid
        Me.fuelid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.fuelid.Attributes.Add("onBlur", "clearNotice()")
        Me.CSC.Text = osites.CSC
        Me.CSC.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CSC.Attributes.Add("onBlur", "clearNotice()")
        Me.CSCdate.Text = osites.CSCdate.ToString("dd MMM yyyy")
        If Me.CSCdate.Text = "01 Jan 0001" Then Me.CSCdate.Text = ""
        Me.CSCdate.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.CSCdate.Attributes.Add("onBlur", "clearNotice()")
        Me.MDdate.Text = osites.MDdate.ToString("dd MMM yyyy")
        If Me.MDdate.Text = "01 Jan 0001" Then Me.MDdate.Text = ""
        Me.MDdate.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.MDdate.Attributes.Add("onBlur", "clearNotice()")
        Me.FKsitegroupid.Text = osites.FKsitegroupid
        Me.FKsitegroupid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.FKsitegroupid.Attributes.Add("onBlur", "clearNotice()")
        Me.Dservices_optout.Text = osites.Dservices_optout
        Me.Dservices_optout.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.Dservices_optout.Attributes.Add("onBlur", "clearNotice()")
        Me.Bill_check_optout.Text = osites.Bill_check_optout
        Me.Bill_check_optout.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.Bill_check_optout.Attributes.Add("onBlur", "clearNotice()")
        Me.CCL_mgmt.Text = osites.CCL_mgmt
        Me.CCL_mgmt.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.CCL_mgmt.Attributes.Add("onBlur", "clearNotice()")
        Me.varMarker.Text = osites.varMarker
        Me.Lat.Text = osites.Lat
        Me.Lat.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.Lat.Attributes.Add("onBlur", "clearNotice()")
        Me.Long.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
        Me.Long.Attributes.Add("onBlur", "clearNotice()")
        Me.varSiteFriendlyName.Text = osites.varSiteFriendlyName
        Me.intCountryFK.Text = osites.intCountryFK
        Me.intCountryFK.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.intCountryFK.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = osites.site_pk
        Status = osites.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osites Is Nothing Then
            osites = New Objects.sites(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            osites.organisation_fk = Me.organisation_fk.SelectedValue
            osites.sitename = Me.sitename.Text
            osites.deadoralive = Me.deadoralive.Text
            osites.ContactName = Me.ContactName.Text
            osites.Phone_1 = Me.Phone_1.Text
            osites.FaxNumber = Me.FaxNumber.Text
            osites.Accmgrid = Me.Accmgrid.Text
            osites.Address_1 = Me.Address_1.Text
            osites.Address_2 = Me.Address_2.Text
            osites.Address_3 = Me.Address_3.Text
            osites.Address_4 = Me.Address_4.Text
            osites.CountyID = Me.CountyID.Text
            osites.Pcode = Me.Pcode.Text
            osites.Email = Me.Email.Text
            osites.StatusID = Me.StatusID.Text
            osites.Customersiteref = Me.Customersiteref.Text
            osites.Tenderstdt = CommonFN.CheckEmptyDate(Me.Tenderstdt.Text)
            osites.Tenderlastdt = CommonFN.CheckEmptyDate(Me.Tenderlastdt.Text)
            osites.tenderprtdate = CommonFN.CheckEmptyDate(Me.tenderprtdate.Text)
            osites.tenderclsddate = CommonFN.CheckEmptyDate(Me.tenderclsddate.Text)
            osites.Notes = Me.Notes.Text
            osites.Sameasbillingadress = Me.Sameasbillingadress.Text
            osites.tendbenchmark = Me.tendbenchmark.Text
            osites.typeofop = Me.typeofop.Text
            osites.voltageID = Me.voltageID.Text
            osites.MaxDemand = Me.MaxDemand.Text
            osites.prodelecneg = Me.prodelecneg.Text
            osites.prodgasneg = Me.prodgasneg.Text
            osites.produtilisys = Me.produtilisys.Text
            osites.prodCRC = Me.prodCRC.Text
            osites.prodWater = Me.prodWater.Text
            osites.federationid = Me.federationid.Text
            osites.fuelid = Me.fuelid.Text
            osites.CSC = Me.CSC.Text
            osites.CSCdate = CommonFN.CheckEmptyDate(Me.CSCdate.Text)
            osites.MDdate = CommonFN.CheckEmptyDate(Me.MDdate.Text)
            osites.FKsitegroupid = Me.FKsitegroupid.Text
            osites.Dservices_optout = Me.Dservices_optout.Text
            osites.Bill_check_optout = Me.Bill_check_optout.Text
            osites.CCL_mgmt = Me.CCL_mgmt.Text
            osites.varMarker = Me.varMarker.Text
            osites.Lat = Me.Lat.Text
            osites.varSiteFriendlyName = Me.varSiteFriendlyName.Text
            osites.intCountryFK = Me.intCountryFK.Text
            result = osites.Save()
            If Not result Then Throw osites.LastError
            _CurrentPk = osites.site_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = osites.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osites Is Nothing Then
            osites = New Objects.sites(currentuser, actualuser, pk, CDC)
        End If
        osites.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osites Is Nothing Then
            osites = New Objects.sites(currentuser, actualuser, pk, CDC)
        End If
        osites.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osites Is Nothing Then
            osites = New Objects.sites(currentuser, pk, CDC)
        End If
        osites.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.organisation_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkorganisation_fk.Visible = IIF(RL >= 0, True, False)
        Me.sitename.Enabled = IIF(WL >= 0, True, False)
        Me.blksitename.Visible = IIF(RL >= 0, True, False)
        Me.deadoralive.Enabled = IIF(WL >= 0, True, False)
        Me.blkdeadoralive.Visible = IIF(RL >= 0, True, False)
        Me.ContactName.Enabled = IIF(WL >= 0, True, False)
        Me.blkContactName.Visible = IIF(RL >= 0, True, False)
        Me.Phone_1.Enabled = IIF(WL >= 0, True, False)
        Me.blkPhone_1.Visible = IIF(RL >= 0, True, False)
        Me.FaxNumber.Enabled = IIF(WL >= 0, True, False)
        Me.blkFaxNumber.Visible = IIF(RL >= 0, True, False)
        Me.Accmgrid.Enabled = IIF(WL >= 0, True, False)
        Me.blkAccmgrid.Visible = IIF(RL >= 0, True, False)
        Me.Address_1.Enabled = IIF(WL >= 0, True, False)
        Me.blkAddress_1.Visible = IIF(RL >= 0, True, False)
        Me.Address_2.Enabled = IIF(WL >= 0, True, False)
        Me.blkAddress_2.Visible = IIF(RL >= 0, True, False)
        Me.Address_3.Enabled = IIF(WL >= 0, True, False)
        Me.blkAddress_3.Visible = IIF(RL >= 0, True, False)
        Me.Address_4.Enabled = IIF(WL >= 0, True, False)
        Me.blkAddress_4.Visible = IIF(RL >= 0, True, False)
        Me.CountyID.Enabled = IIF(WL >= 0, True, False)
        Me.blkCountyID.Visible = IIF(RL >= 0, True, False)
        Me.Pcode.Enabled = IIF(WL >= 0, True, False)
        Me.blkPcode.Visible = IIF(RL >= 0, True, False)
        Me.Email.Enabled = IIF(WL >= 0, True, False)
        Me.blkEmail.Visible = IIF(RL >= 0, True, False)
        Me.StatusID.Enabled = IIF(WL >= 0, True, False)
        Me.blkStatusID.Visible = IIF(RL >= 0, True, False)
        Me.Customersiteref.Enabled = IIF(WL >= 0, True, False)
        Me.blkCustomersiteref.Visible = IIF(RL >= 0, True, False)
        Me.Tenderstdt.Enabled = IIf(WL >= 99, True, False)
        Me.blkTenderstdt.Visible = IIf(RL >= 99, True, False)
        Me.Tenderlastdt.Enabled = IIf(WL >= 99, True, False)
        Me.blkTenderlastdt.Visible = IIf(RL >= 99, True, False)
        Me.tenderprtdate.Enabled = IIf(WL >= 99, True, False)
        Me.blktenderprtdate.Visible = IIf(RL >= 99, True, False)
        Me.tenderclsddate.Enabled = IIf(WL >= 99, True, False)
        Me.blktenderclsddate.Visible = IIf(RL >= 99, True, False)
        Me.Notes.Enabled = IIF(WL >= 0, True, False)
        Me.blkNotes.Visible = IIF(RL >= 0, True, False)
        Me.Sameasbillingadress.Enabled = IIF(WL >= 0, True, False)
        Me.blkSameasbillingadress.Visible = IIF(RL >= 0, True, False)
        Me.tendbenchmark.Enabled = IIf(WL >= 99, True, False)
        Me.blktendbenchmark.Visible = IIf(RL >= 99, True, False)
        Me.typeofop.Enabled = IIf(WL >= 99, True, False)
        Me.blktypeofop.Visible = IIf(RL >= 99, True, False)
        Me.voltageID.Enabled = IIF(WL >= 0, True, False)
        Me.blkvoltageID.Visible = IIF(RL >= 0, True, False)
        Me.MaxDemand.Enabled = IIF(WL >= 0, True, False)
        Me.blkMaxDemand.Visible = IIF(RL >= 0, True, False)
        Me.prodelecneg.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodelecneg.Visible = IIf(RL >= 99, True, False)
        Me.prodgasneg.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodgasneg.Visible = IIf(RL >= 99, True, False)
        Me.produtilisys.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodutilisys.Visible = IIf(RL >= 99, True, False)
        Me.prodCRC.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodCRC.Visible = IIf(RL >= 99, True, False)
        Me.prodWater.Enabled = IIf(WL >= 99, True, False)
        Me.blkprodWater.Visible = IIf(RL >= 99, True, False)
        Me.federationid.Enabled = IIf(WL >= 99, True, False)
        Me.blkfederationid.Visible = IIf(RL >= 99, True, False)
        Me.fuelid.Enabled = IIf(WL >= 99, True, False)
        Me.blkfuelid.Visible = IIf(RL >= 99, True, False)
        Me.CSC.Enabled = IIF(WL >= 0, True, False)
        Me.blkCSC.Visible = IIF(RL >= 0, True, False)
        Me.CSCdate.Enabled = IIf(WL >= 99, True, False)
        Me.blkCSCdate.Visible = IIf(RL >= 99, True, False)
        Me.MDdate.Enabled = IIf(WL >= 99, True, False)
        Me.blkMDdate.Visible = IIf(RL >= 99, True, False)
        Me.FKsitegroupid.Enabled = IIf(WL >= 99, True, False)
        Me.blkFKsitegroupid.Visible = IIf(RL >= 99, True, False)
        Me.Dservices_optout.Enabled = IIf(WL >= 99, True, False)
        Me.blkDservices_optout.Visible = IIf(RL >= 99, True, False)
        Me.Bill_check_optout.Enabled = IIf(WL >= 99, True, False)
        Me.blkBill_check_optout.Visible = IIf(RL >= 99, True, False)
        Me.CCL_mgmt.Enabled = IIf(WL >= 99, True, False)
        Me.blkCCL_mgmt.Visible = IIf(RL >= 99, True, False)
        Me.varMarker.Enabled = IIf(WL >= 99, True, False)
        Me.blkvarMarker.Visible = IIf(RL >= 99, True, False)
        Me.Lat.Enabled = IIf(WL >= 99, True, False)
        Me.blkLat.Visible = IIf(RL >= 99, True, False)
        Me.Long.Enabled = IIf(WL >= 99, True, False)
        Me.blkLong.Visible = IIf(RL >= 99, True, False)
        Me.varSiteFriendlyName.Enabled = IIf(WL >= 0, True, False)
        Me.blkvarSiteFriendlyName.Visible = IIF(RL >= 0, True, False)
        Me.intCountryFK.Enabled = IIF(WL >= 0, True, False)
        Me.blkintCountryFK.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

