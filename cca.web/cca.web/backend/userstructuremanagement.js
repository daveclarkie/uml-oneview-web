﻿// JScript File

function loadMembers(user_pk){

    AvailableUsers(user_pk);
    ActiveUsers(user_pk);

} //loadMembers

function ActiveUsers(user_pk) {

    var manager = document.getElementById(user_pk);
    var OutputActive = document.getElementById("ActiveUserList");
    var rspn=""; 
    var rqMembers=newRequest();
    

    var keys="b="+manager.value+"&c=0";         
    rqMembers.open("GET","/functions/manageuserstructures.ashx?"+keys+noCache(),false);
    rqMembers.send();
    rspn=rqMembers.responseText;  
    chgNamedElementText("ActiveUserList",chgReplace,rspn);
    OutputActive.style.display="";
} //ActiveUsers

function AvailableUsers(user_pk) {

    var manager = document.getElementById(user_pk);
    var OutputAvailable = document.getElementById("AvailableUserList");
    var rspn=""; 
    var rqAvailable=newRequest();

    var keys = "b=" + manager.value + "&c=1";
    rqAvailable.open("GET", "/functions/manageuserstructures.ashx?" + keys + noCache(), false);
    rqAvailable.send();
    rspn=rqAvailable.responseText;  
    chgNamedElementText("AvailableUserList",chgReplace,rspn);
    OutputAvailable.style.display="";
} //AvailableUsers

function Add(sourceLI, userstructure_pk,user_pk,manageruser_pk,username){

    // userstructure_pk - will be empty/-1 because we're about to set it
    // user_pk,manageruser_pk,username will all contain values

    var rqMembers=newRequest();
    rqMembers.open("GET", "/functions/userstructureadd.ashx?mu=" + manageruser_pk + "&u=" + user_pk + noCache(), false);
    rqMembers.send();
    rspn=rqMembers.responseText;  

    if(rspn.substring(0,2) == "OK"){
        userstructure_pk = rpl(rspn, "OK:", "");        
    } else {
        alert("Ooopsie!");
        return;
    }

        
    var AvailableUser;
    var ActiveUser;
    var li;
    var a;    
    var img; 
    
    var originalLi = document.getElementById(sourceLI);
    
    AvailableUser= document.getElementById("AvailableUserList");  //current list object is assigned to
    ActiveUser= document.getElementById("ActiveUserList");  //current list object will be assigned to
    
    li = document.createElement("li");
    li.id = "AcUser_" + userstructure_pk;
    
    a = document.createElement("a");
    a.href = 'javascript:Remove("AcUser_' + userstructure_pk + '",' + userstructure_pk + ',' + user_pk + ',' + manageruser_pk + ',"' + username + '");';
    
    img = document.createElement("img");
    img.src ="/design/images/icon_remove.png";
    img.alt="remove";
    img.className="manageGroup_Icon";

    a.appendChild(img);

    var tn;
    tn = document.createTextNode("   ");
    a.appendChild(tn);
    tn = document.createTextNode(username);
    a.appendChild(tn);
    
    li.appendChild(a);
    ActiveUser.appendChild(li);
    
    AvailableUser.removeChild(originalLi);

} //userstructureadd.ashx


function Remove(sourceLI, userstructure_pk, user_pk, manageruser_pk, username) {


    var rqAvailable=newRequest();
    rqAvailable.open("GET", "/functions/userstructurerem.ashx?pk=" + userstructure_pk + noCache(), false);
    rqAvailable.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    rqAvailable.send();
    rspn=rqAvailable.responseText; 
        if(rspn == "OK"){
        
        
        var AvailableUser;
        var ActiveUser;
        var li;
        var a;    
        var img;
        
        var originalLi = document.getElementById(sourceLI);
        
        ActiveUser= document.getElementById("ActiveUserList");  //current list object is assigned to
        AvailableUser= document.getElementById("AvailableUserList");//current list object will be assigned to
      
        li = document.createElement("li");
        li.id = "AvUser_"+user_pk;
        a = document.createElement("a");
        a.href = 'javascript:Add("AvUser_' + user_pk + '",' + userstructure_pk + ',' + user_pk + ',' + manageruser_pk + ',"' + username + '");';
                
        img = document.createElement("img");
        img.src ="/design/images/icon_add.png";
        img.alt ="add";
        img.className ="manageGroup_Icon";
        a.appendChild(img);
        
        var tn;
        tn = document.createTextNode("   ");
        a.appendChild(tn);
        tn = document.createTextNode(username);
        a.appendChild(tn);
        
        li.appendChild(a);
        AvailableUser.appendChild(li); 
            
        ActiveUser.removeChild(originalLi);
    
        }
        else{

        alert(rspn.responseText); 
        }

} //userstructurerem.aspx