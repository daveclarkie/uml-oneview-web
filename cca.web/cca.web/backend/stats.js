﻿// JScript File

function disconnect(userid){
    alert("done");
}


var selUser;

function callback_disconnect(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail" || xmlhttp.status==404){
                if (xmlhttp.status==404){ 
                    alert("API 404");
                } else {
                    alert("Attempted action failed:-\n"+rpl(rspn,"Fail:",""));
                }
            }else{
                var lnk=document.getElementById("lnk"+selUser);
                chgElementText(lnk,chgReplace,"Disconnect Set");
            }
            showAsyncAction(icontrgt,0,"wait") 
        }
    }
}

function disconnect(usrid){
    selUser=usrid;
    icontrgt="img"+usrid;
    setReqHttp();
    if (hasXmlhttp==true){
        showAsyncAction(icontrgt,1,"wait") 
        xmlhttp.open("GET","../functions/disconnect.aspx?u="+usrid+noCache());
        callback_disconnect();
        xmlhttp.send(null);   
    }
   else
   {
    alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
   } 
}
