Imports System.Net.Mail
Partial Class resetpassword_ctl
    Inherits DaveControl
    Public PSM As MySM

    Dim epwd() As Byte = Nothing


    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        ctlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oUsers As objects.users

    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        Me.btnviewcurrent.Visible = False
        Me.btnviewnew.Visible = False
        Me.btnviewconfirm.Visible = False

        If _read Then Exit Sub
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, ByVal type As Integer, Optional ByVal pk As Integer = 0, Optional ByVal notepk As Integer = 0) As Boolean
        Dim result As Boolean = False

        result = oUsers.Save

        Return result
    End Function

    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.currentuser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.currentuser, Me.GetType().ToString, CDC)
    End Sub


    Protected Sub btnviewcurrent_Click(sender As Object, e As System.EventArgs) Handles btnviewcurrent.Click
        Me.pwdCurrentPassword.TextMode = TextBoxMode.SingleLine
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Dim loginOK As Boolean = False
        Dim newPasswordOK As Boolean = False
        Dim vUsername As String = ""

        Dim user_pk As Integer = 0
        If Not IsNothing(SM.actualuser) Then


            Try ' Check the existing password
                CO = New SqlClient.SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_usernamesearch "
                CO.Parameters.AddWithValue("@user_pk", SM.actualuser)
                CDC.ReadScalarValue(vUsername, CO)

                CO = New SqlClient.SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_login"
                CO.Parameters.AddWithValue("@usr", vUsername)
                CO.Parameters.AddWithValue("@pwd", Me.pwdCurrentPassword.Text)
                Dim dr As DataRow = CDC.ReadDataRow(CO)

                If Not dr Is Nothing Then
                    loginOK = True
                Else
                    GoTo ErrorOutput
                End If
                dr = Nothing
            Catch ex As Exception

            End Try




            Try  ' Check the new = confirm password
                If Me.pwdNewPassword.Text.Length < 6 Then

                    lblMessage.Text = "Your new password must be atleast 6 characters long."
                    lblMessage.CssClass = "error"

                    Exit Sub
                End If
            Catch ex As Exception

            End Try


            Try  ' Check the new = confirm password
                If Me.pwdNewPassword.Text = Me.pwdConfirmPassword.Text Then
                    newPasswordOK = True
                Else
                    GoTo ErrorOutput
                End If
            Catch ex As Exception

            End Try

            ' at this point all 3 passwords are correct.. so change it :)
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure

            CO.CommandText = "rsp_CreatePasswdEnc"
            CO.Parameters.AddWithValue("@pwd", Me.pwdNewPassword.Text)
            CDC.ReadScalarValue(epwd, CO)
            CO.Parameters.Clear()

            Dim u As New users(SM.actualuser, SM.actualuser, SM.actualuser, CDC)
            u.encpassword = epwd
            If u.Save() Then



                Dim TargetAccounts As String = ""
                CO.CommandText = "[rsp_useremails_forsystemmail]"
                CO.Parameters.Clear()
                CO.Parameters.AddWithValue("@user_pk", SM.actualuser)
                Dim sdt As DataTable = CDC.ReadDataTable(CO)
                Dim sdr As DataRow

                For Each sdr In sdt.Rows
                    TargetAccounts &= sdr("email")
                    TargetAccounts &= ","
                Next

                If TargetAccounts.Length > 1 Then
                    TargetAccounts = TargetAccounts.Trim(",".ToCharArray)
                End If

                TargetAccounts = "dave.clarke@ems.schneider-electric.com"

                ' Reset Password Task
                Dim strTo() As String = TargetAccounts.Split(",".ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
                Dim insMail As New MailMessage(New MailAddress("donotreply@ems.schneider-electric.com"), New MailAddress(TargetAccounts))

                Dim emailpk As Integer = 0

                CO.CommandText = "rsp_primaryemail"
                CO.Parameters.Clear()
                CO.Parameters.AddWithValue("@user_pk", SM.actualuser)

                Dim edr As DataRow = CDC.ReadDataRow(CO)
                emailpk = edr("email_pk")
                edr = Nothing

                Dim em As New emails(SM.actualuser, SM.actualuser, emailpk, CDC)


                Dim msg As String = ""
                msg &= "Your password for the system ( http://" & Request.ServerVariables("HTTP_HOST") & "/ ) has been changed." & "</br></br>"
                msg &= "You can now log in using:</br>"
                msg &= "<b>Username:</b> '" & vUsername & "' or '" & em.email & "'</br>"
                msg &= "<b>Password:</b> " & Me.pwdNewPassword.Text & "</br>"
                msg &= "</br></br>" & "This is an automated message from an account that is unable to receive email. This email account is not associated with a human being. Replying to this message will result in the mail server refusing to deliver your response."

                With insMail
                    .Subject = "Password Change"
                    .IsBodyHtml = True
                    .Body = htmlBody(msg)
                    .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
                End With
                Dim smtp As New System.Net.Mail.SmtpClient
                smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
                smtp.Host = "10.44.33.23"
                smtp.Port = 25


                Try
                    smtp.Send(insMail)

                    lblMessage.Text = "Your new password has been changed."
                    lblMessage.CssClass = "ok"

                Catch ex As Exception
                    lblMessage.Text = ex.Message
                    lblMessage.CssClass = "error"
                End Try
            Else
                GoTo ErrorOutput
            End If
        Else
            lblMessage.Text = "You need to be logged in to reset your password."
            lblMessage.CssClass = "error"
        End If

        Exit Sub
ErrorOutput:

        If Not loginOK Then
            lblMessage.Text = "Current Password is invalid."
            lblMessage.CssClass = "error"
        ElseIf Not newPasswordOK Then
            lblMessage.Text = "New Passwords don't match, please try again."
            lblMessage.CssClass = "error"
        Else
            lblMessage.Text = CDC.LastError.Message
            lblMessage.CssClass = "error"
        End If


    End Sub

    Public Function htmlBody(ByVal body As String) As String
        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
        htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P>"
        htmlMessageBody = htmlMessageBody + body
        htmlMessageBody = htmlMessageBody + "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"
        Return htmlMessageBody
    End Function

End Class

