<%@ Control Language="VB" AutoEventWireup="false" CodeFile="seekperson.ascx.vb" Inherits="controls_plugins_seekperson" %>
<a href="javascript:seek_<%=ControlName %>('<%=TargetValueControlName %>','<%=TargetDisplayControlName %>')"><%=Label %></a>
<div id="seek_<%=ControlName %>_control" class="seek_container" style="display:none;">
    <div class="seek_control">    
        <div id="seek_<%=ControlName %>_titlebar" class="seek_titlebar">User Search</div><div id="seek_<%=ControlName %>_exitbar" class="seek_exitbar"><a href="javascript:seek_<%=ControlName %>_exit()">X</a></div>
        <div id="seek_<%=ControlName %>_panel" class="seek_panel">
            <div id="seek_<%=ControlName %>_search" class="seek_search" style="display:none;">
            
            <span class="seek_title">Search</span><br />
         
            <ul class="seek_ul_form">
                <li><span class="label">Forename</span><input type="text" name="fn_<%=ControlName %>" ID="fn_<%=ControlName %>" /></li>
                <li><span class="label">Surname</span><input type="text" name="sn_<%=ControlName %>" ID="sn_<%=ControlName %>" /></li>
                <li><span class="label">Address</span><input type="text" name="loc_<%=ControlName %>" ID="loc_<%=ControlName %>" /></li>
                <li><span class="label">Email</span><input type="text" name="email_<%=ControlName %>" ID="email_<%=ControlName %>" /></li>
               <% If SM.currentuser = 10 Then%> 
               <li><span class="label">UID</span><input type="text" name="pk_<%=ControlName %>" ID="pk_<%=ControlName %>" /></li> 
               <% Else%>
               <li><span class="label">&nbsp;</span><input type="hidden" name="pk_<%=ControlName %>"  ID="pk_<%=ControlName %>" /></li> 
               <% End If %>
               <li><span class="label">&nbsp;</span><a href="javascript:seek_<%=ControlName %>_search()">Search</a></li> 
            </ul>
            
            </div>
            <div id="seek_<%=ControlName %>_wait" class="seek_wait" style="display:none;">
            
            <span class="seek_title">Searching....</span><img src="/design/images/ajax-loader-system.gif" />

            </div>

            <div id="seek_<%=ControlName %>_result" class="seek_result" style="display:none;">
            
            <span class="seek_title">Results</span><br />

            <ul id="ul_<%=ControlName %>"  class="seek_ul_result">
            </ul>
            
            
            
            </div>
       </div> 
   </div>
</div>

<script language="javascript" type="text/javascript">

var target_value_<%=ControlName %>;
var target_display_<%=ControlName %>;


function clear_<%=ControlName %>_list(){
    var ulres=document.getElementById("ul_<%=ControlName %>");
    try
        { 
            while (ulres.hasChildNodes)
            {
                ulres.removeChild(ulres.lastChild);
            }
        }
   catch (e) {
   } 
}

function fill_<%=ControlName %>_list(results){
    var ulres=document.getElementById("ul_<%=ControlName %>");
    var user_pk;
    var username;
    var forename;
    var surname;
    var primaryaddress;
    var emailhome;
    var emailoffice;
    var active;

    var lstuser=results.getElementsByTagName("users");

    for(var i=0; i<lstuser.length; i++)
    {
   
   

    
        user_pk=lstuser[i].getElementsByTagName("user_pk")[0].childNodes[0].nodeValue;
        try {username=lstuser[i].getElementsByTagName("username")[0].childNodes[0].nodeValue;} catch (e) {username='N/A';}
        try {forename=lstuser[i].getElementsByTagName("forename")[0].childNodes[0].nodeValue;} catch (e) {forename='N/A';}
        try {surname=lstuser[i].getElementsByTagName("surname")[0].childNodes[0].nodeValue;} catch (e) {surname='N/A';}
        try {primaryaddress=lstuser[i].getElementsByTagName("primaryaddress")[0].childNodes[0].nodeValue;} catch (e) {primaryaddress='N/A';}
        try {emailhome=lstuser[i].getElementsByTagName("emailhome")[0].childNodes[0].nodeValue;} catch (e) {emailhome='N/A';}
        try {emailoffice=lstuser[i].getElementsByTagName("emailoffice")[0].childNodes[0].nodeValue;} catch (e) {emailoffice='N/A';}
        try {active=lstuser[i].getElementsByTagName("rowstatus")[0].childNodes[0].nodeValue;} catch (e) {active='N/A';}

        li=document.createElement("li");

        a=document.createElement("a");
        txt=document.createTextNode(forename+" "+surname+" ["+user_pk+"]");
        a.href="javascript:seek_<%=ControlName %>_return('"+user_pk+"','"+forename+" "+surname+"')";


        br1=document.createElement("br");
        br2=document.createElement("br");

        sp1=document.createElement("span");
        txt1=document.createTextNode(primaryaddress);
        sp1.className="seek_location"

        sp2=document.createElement("span");
        txt2=document.createTextNode(emailhome+" / "+emailoffice);
        sp2.className="seek_email"

        a.appendChild(txt);
        sp1.appendChild(txt1);
        sp2.appendChild(txt2);

        li.appendChild(a);
        li.appendChild(sp1);
        li.appendChild(br2);
        li.appendChild(sp2);

        ulres.appendChild(li);  

    }

}

function callback_seek_<%=ControlName %>_result()
{
    xmlhttp.onreadystatechange=function() 
    {

        if (xmlhttp.readyState==4) 
        {
            var sps=document.getElementById('seek_<%=ControlName %>_search');
            var spw=document.getElementById('seek_<%=ControlName %>_wait');
            var spr=document.getElementById('seek_<%=ControlName %>_result');

            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0)
            {
                alert(xmlhttp.responseText);
                sps.style.display="block"; 
                spw.style.display="none"; 
                spr.style.display="none"; 
            }
            else
            {
                c=xmlDoc.getElementsByTagName("no-one").length;
                if (c!=0)
                {
                    alert("No users matched your criteria");
                    sps.style.display="block"; 
                    spw.style.display="none"; 
                    spr.style.display="none"; 
                }
                else
                {
                    c=xmlDoc.getElementsByTagName("results").length;
                    if (c!=0){
                        clear_<%=ControlName %>_list();
                        fill_<%=ControlName %>_list(xmlDoc);
                        sps.style.display="none"; 
                        spw.style.display="none"; 
                        spr.style.display="block"; 

                    }else{
                        alert(xmlhttp.responseText);
                        sps.style.display="block"; 
                        spw.style.display="none"; 
                        spr.style.display="none"; 

                    }
                }
            }


        }
    }
}

function seek_<%=ControlName %>_search(){
    var fn=document.getElementById('fn_<%=ControlName %>').value;
    var sn=document.getElementById('sn_<%=ControlName %>').value;
    var loc=document.getElementById('loc_<%=ControlName %>').value;
    var email=document.getElementById('email_<%=ControlName %>').value;
    var pk=document.getElementById('pk_<%=ControlName %>').value;

    var sps=document.getElementById('seek_<%=ControlName %>_search');
    var spw=document.getElementById('seek_<%=ControlName %>_wait');
    var spr=document.getElementById('seek_<%=ControlName %>_result');
    sps.style.display="none"; 
    spw.style.display="block"; 
    spr.style.display="none"; 

    setReqHttp();

    var postData="fn="+fn;
    postData=postData+"&sn="+sn;
    postData=postData+"&loc="+loc;
    postData=postData+"&email="+email;
    postData=postData+"&pk="+pk;

    xmlhttp.open("POST","/functions/seekp.aspx?a=0"+noCache(),false);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    callback_seek_<%=ControlName %>_result();
    xmlhttp.send(postData);

}

function seek_<%=ControlName %>_return(selPk,selValue){
    chgNamedElementText(target_display_<%=ControlName %>,chgReplace,selValue); 
    chgNamedElementText(target_value_<%=ControlName %>,chgReplace,selPk);  
    var spc=document.getElementById('seek_<%=ControlName %>_control');
    spc.style.display="none";
   <% If TargetFunctionName.length>0 Then %>
   try {
   <%=TargetFunctionName %>(selPk);
   }
   catch (e) {
    alert("error:"+e);
   }
   <% End If %>  
}

function seek_<%=ControlName %>_exit(){
   var spc=document.getElementById('seek_<%=ControlName %>_control');
   spc.style.display="none"; 
}

function seek_<%=ControlName %>(targetvalue,targetdisplay){
   var spc=document.getElementById('seek_<%=ControlName %>_control');
   var ttl=document.getElementById('seek_<%=ControlName %>_titlebar');
   var sps=document.getElementById('seek_<%=ControlName %>_search');
   var spr=document.getElementById('seek_<%=ControlName %>_result');
   var spw=document.getElementById('seek_<%=ControlName %>_wait');
   target_value_<%=ControlName %>=targetvalue;
   target_display_<%=ControlName %>=targetdisplay;
    chgNamedElementText(target_display_<%=ControlName %>,chgReplace,''); 
    document.getElementById(target_value_<%=ControlName %>).value=''; 

   spc.style.display="block"; 
   sps.style.display="block"; 
   spw.style.display="none"; 
   spr.style.display="none"; 

}


</script>  