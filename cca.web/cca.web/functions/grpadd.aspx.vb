
Partial Class functions_grpadd
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand
    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Dim ok As Boolean = False
        Try

            Dim u As Integer
            Dim g As Integer

            Dim rf As Object = Request.QueryString
            Dim ug As New usergroups(SM.currentuser, SM.actualuser, CDC)


            If Integer.TryParse(rf("u"), u) And Integer.TryParse(rf("pk"), g) Then

                Dim grp As New groups(SM.currentuser, SM.actualuser, g, CDC)

                Dim secure As Boolean = True

                ' Allowed to manage groups
                secure = secure And Security.GroupMember(SM.currentuser, SystemGroups.ManageGroups, CDC)

                If secure Then
                    ug.user_fk = u
                    ug.group_fk = g

                    If ug.Save() Then
                        Response.Write("OK")
                    Else
                        Response.Write("Fail:" & ug.LastError.ToString)
                    End If

                Else
                    Response.Write("Fail: Access Denied")
                End If

            End If


        Catch Ex As Exception
            Response.Clear()
            Response.Write("Fail:" & Ex.Message)
        End Try

        Try
            Response.End()
        Catch ex As Exception
        End Try


    End Sub


End Class
