Partial Class emails_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oemails As Objects.emails
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oemails is Nothing then
oemails = new Objects.emails(currentuser,actualuser,pk,CDC)
End If
Me.email.Text=oemails.email
_CurrentPk=oemails.email_pk
Status=oemails.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oemails is Nothing then
oemails = new Objects.emails(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oemails.email=Me.email.Text
result=oemails.Save()
if not result then throw oemails.LastError
_CurrentPk=oemails.email_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oemails.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oemails is Nothing then
   oemails = new Objects.emails(currentuser,actualuser,pk,CDC)
  End If
  oemails.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oemails is Nothing then
   oemails = new Objects.emails(currentuser,actualuser,pk,CDC)
  End If
  oemails.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oemails is Nothing then
   oemails = new Objects.emails(currentuser,pk,CDC)
  End If
  oemails.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.email.Enabled=IIF(WL>=0,True,False)
  Me.blkemail.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

