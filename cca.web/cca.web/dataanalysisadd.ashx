<%@ WebHandler Language="VB" Class="function_dataanalysisadd" %>
Public Class function_dataanalysisadd
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Try
        
            Dim ag As Integer
            Dim mp As String
            Dim u As Integer

            Dim rf As Object = Request.QueryString
            Dim dca As New dataconsumptionanalysis(SM.currentuser, SM.actualuser, CDC)

            mp = rf("mp")
            If Integer.TryParse(rf("ag"), ag) And Integer.TryParse(rf("u"), u) Then

                dca.agreement_fk = ag
                dca.meterpoint_fk = mp
                dca.user_fk = u
                 
                If dca.Save() Then
                    Response.Write("OK:" & dca.dataconsumptionanalysis_pk)
                Else
                    Response.Write("Fail:" & dca.LastError.ToString)
                End If

            End If


        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class