<%@ Control Language="VB" AutoEventWireup="false" CodeFile="user.ascx.vb" Inherits="design_elements_banners_user" %>
<%@ Register Src="../informations.ascx" TagName="informations" TagPrefix="uc3" %>
<%@ Register Src="../notifications.ascx" TagName="notifications" TagPrefix="uc2" %>
<%@ Register Src="../tasks.ascx" TagName="tasks" TagPrefix="uc1" %>
<h1 id="logo">Schneider Electric</h1>
<div id="right">
    <ul id="notifications">
        <%  If ccaSystemAccess Then%>
            <li><uc3:informations ID="Informations3" Label="Electricity" InformationItemType="CountMeterPointsType1" runat="server" EnableViewState="false" /></li>
            <li><uc3:informations ID="Informations4" Label="Gas" InformationItemType="CountMeterPointsType2" runat="server" EnableViewState="false" /></li>
            <li><uc3:informations ID="Informations5" Label="Other<br>Fuels" InformationItemType="CountMeterPointsType3" runat="server" EnableViewState="false" /></li>
            <li><uc3:informations ID="Informations1" Label="Agreements" InformationItemType="CountAgreements" runat="server" EnableViewState="false" /></li>
            <li><uc3:informations ID="Informations2" Label="Reports<br>Today" InformationItemType="CountCCAReports" runat="server" EnableViewState="false" /></li>
        <%End If%>
        
        <li><uc3:informations ID="Informations6" Label="My Open<br>Requests" InformationItemType="helpdesk_openrequests" runat="server" EnableViewState="false" /></li>
    </ul>
</div>
