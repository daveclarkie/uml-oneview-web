<%@ Control Language="VB" AutoEventWireup="false" CodeFile="survey7details_ctl.ascx.vb" Inherits="survey7details_ctl" %>
<h5>survey7details</h5>
<ul class='formcontrol'>
<li runat="server" id="blkhelpdesk_id">
<span class='label'>Helpdesk ID<span class='pop'>The request id of the ticket from helpdesk</span></span>
<asp:TextBox EnableViewState="false" ID="helpdesk_id" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkselecteduser_fk">
<span class='label'>User<span class='pop'>The user_fk of the user who should be doing the survey</span></span>
<asp:DropDownList ID="selecteduser_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion1_surveyanswerfivescale_fk">
<span class='label'>With the accuracy of the budgets received</span>
<asp:DropDownList ID="question1_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion2_surveyanswerfivescale_fk">
<span class='label'>With the quality of assumptions received</span>
<asp:DropDownList ID="question2_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion3_surveyanswerfivescale_fk">
<span class='label'>With the speed of response from the Budgeting team</span>
<asp:DropDownList ID="question3_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion4_surveyanswerfivescale_fk">
<span class='label'>The Budgeting team conducted themselves professionally</span>
<asp:DropDownList ID="question4_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkquestion5_surveyanswerfivescale_fk">
<span class='label'>The Client was satisfied with the overall quality of the budgeting service</span>
<asp:DropDownList ID="question5_surveyanswerfivescale_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>

