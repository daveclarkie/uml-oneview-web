<%@ Control Language="VB" AutoEventWireup="false" CodeFile="user.ascx.vb" Inherits="design_elements_navigation_user" %>
<%@ Register Src="../users.ascx" TagName="users" TagPrefix="uc1" %>

<h3>General</h3>
<ul class="navigationmenu">
    <li><a href="../../../core/default.aspx">Home</a></li>
    <li><a href="../../../corepersons/search.aspx?<%=Local.noCache %>">Person Search</a></li>
<%--<li><a href="../../../corehelpdesk/default.aspx">Helpdesk</a></li>--%>
<%  If BackendAccess() Then%>
    <li><a href="../../../backend/default.aspx">Systems</a></li>
<%  End If%>
</ul>


<h3>Functional Helpdesk View</h3>
<ul class="navigationmenu">
    <li><a href="../../../corehelpdesk/default.aspx?<%=Local.noCache %>">Search</a></li>
</ul>


<%  If cmSystemAccess Then%>
<h3>Contract Manager</h3>
<ul class="navigationmenu">
    <li><a href="../../../corecm/rfqgeneration.aspx?pg=cu<%=Local.noCache %>">RFQ Generation</a></li>
    <li><a href="../../../corecm/search.aspx?<%=Local.noCache %>">Search</a></li>
</ul>
<% End If%>

<%  If demandSystemAccess Then%>
<h3>The Demand Guys</h3>
<ul class="navigationmenu">
    <li><a href="../../../coredemand/search.aspx?<%=Local.noCache %>">Search</a></li>
</ul>
<% End If%>

<%  If ccaSystemAccess Then%>
<h3>Climate Change Agreements</h3>
<ul class="navigationmenu">
    <li><a href="../../../corecca/myagreements.aspx?<%=Local.noCache %>"><strong>My Agreements</strong></a></li>
    <li>.........</li>
    <li><a href="../../../corecca/agreements.aspx?pk=-1&pg=ag<%=Local.noCache %>">New Agreement</a></li>
    <li><a href="../../../corecca/bubbles.aspx?pk=-1&pg=dt<%=Local.noCache %>">New Bubble</a></li>
    <li><a href="../../../corecca/importdata.aspx" target="_blank">Import Data</a></li>
    <li><a href="../../../corecca/search.aspx">Agreement Search</a></li>
    <li><a href="../../../credits.aspx" style="text-decoration:none;" target="_blank">CCA Credits</a></li>
</ul>
<% End If%>

<%  If matrixSystemAccess Then%>
<h3>Matrix Pricing System</h3>
<ul class="navigationmenu">
    <li><a href="../../../corematrix/default.aspx?pk=-1&pg=mn<%=Local.noCache %>">New Matrix Price</a></li>
    <li><a href="../../../corematrix/upload.aspx">Upload Matrix</a></li>
    <li><a href="../../../corematrix/search.aspx">Search</a></li>
</ul>
<% End If%>

<h3>Other</h3>
<ul class="navigationmenu">
    <li><a href="../../../coresurvey/default.aspx">Surveys</a></li>
</ul>

<h3>User Details</h3>
<ul class="navigationmenu">
    <li><a href="../../../core/persons.aspx?pk=<%=PSM.CurrentUser %>&pg=ps<%=Local.noCache %>">My Details</a></li>
    <li><a href="../../../core/resetpassword.aspx">Reset Password</a></li>
</ul>

<h3>Log Out</h3>
<ul class="navigationmenu">
    <li><a href="../../../logout.aspx">Log Out (<%=PSM.UserName(SM.CurrentUser) %>)</a></li>
</ul>


<div style="height:3px;font-size:3px;">&nbsp;</div>

