// JScript File

var currentpackinvoice;
var currentpack;

function showInfoReqPopUp(inv,ref){
    currentpackinvoice=inv;
    currentpack=ref; 
    var des=document.getElementById("optname"+currentpackinvoice);
    var currentitemname;
    if (hasInnerText){
            currentitemname=des.innerText;
    }else{
            currentitemname=des.textContent;
    }
    chgNamedElementText("detailName",chgReplace,currentitemname);
    var popUp=document.getElementById("makeTask");
    popUp.style.display="block";
}

function hideInfoReqPopUp(){
    var popUp=document.getElementById("makeTask");
    popUp.style.display="none";
    chgNamedElementText("detailName",chgReplace,"&nbsp;"); 
}


function confInfoReq(){

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            var rspn=xmlhttp.responseText;
            if (rspn.substring(0,4)=="Fail" || xmlhttp.status==404){
                if (xmlhttp.status==404){ 
                    alert("API 404");
                } else {
                    alert("Failed to create new task:-\n"+rpl(rspn,"Fail:",""));
                }
            }else{

                window.location.reload();
            }
        }
    }
}

function fireInfoReq(){
    setReqHttp();

    var rsn=document.getElementById("reason");
    reason=rsn.value; 
    reason=rpl(reason,"=","[equ]");

    var postData="rsn="+reason;
    postData=postData+"&inv="+currentpackinvoice;
    postData=postData+"&ref="+currentpack;
    var popUp=document.getElementById("makeTask");
    popUp.style.display="none";
    if (hasXmlhttp==true){
        xmlhttp.open("POST","/functions/inforequest.ashx?a=0"+noCache(),false);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        confInfoReq();
        xmlhttp.send(postData);
    }
   else
   {
    alert("Required functionality is either not supported or blocked by your browser.\n This feature is known to work in Internet Explorer 5.5+, Firefox 3+ (Win/Linux), Chrome, Chromium, Safari 4+, Midori and Opera 11+.\n Epiphany and Konqueror have not been fully tested but should work. This test is based on functionality available to javascript rather than browser detection.");
   } 
}
