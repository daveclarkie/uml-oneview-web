<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="corepersons_search" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Search</h3>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <ul id="" class='formcontrol'>
                <li><span class="label">Organisation</span><asp:Dropdownlist ID="ddlorganisation" runat="server" CssClass="input_ddl" /></li>
                <li><span class="label">Forename</span><asp:TextBox ID="forename" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Surname</span><asp:TextBox ID="surname" runat="server" CssClass="input_str" /></li>
                <li><span class="label">Email</span><asp:TextBox ID="email" runat="server" CssClass="input_str" /></li>
                <li><span class="label">&nbsp;</span><asp:LinkButton ID="btnSearch" runat="server" Text="Locate" CausesValidation="False" /><asp:LinkButton ID="btnPersonSearch" runat="server" Text="" CausesValidation="False" /></li>
            </ul>
            <asp:LinkButton ID="btnCreate" runat="server" Text="" CausesValidation="False" />
        </asp:Panel>
        
            <div id="frmCreateNewUser" class="modal_control"><div id="rspCreateNewUser"></div></div>
                  
        <asp:Repeater ID="rptPersons" runat="server">
            <HeaderTemplate>
                <h3>Results - People</h3>
                <table >
                    <tr>
                        <td>
                                
                        </td>
                        <td>
                            <h4>Name</h4>
                        </td>
                        <td>
                            <h4>Job Title</h4>
                        </td>
                        <td>
                            <h4>Type</h4>
                        </td>
                        <td>
                            <h4>Organisation</h4>
                        </td>
                    </tr>
            </HeaderTemplate> 
            <ItemTemplate>
                    <tr>
                        <td style="width:60px;">
                            <%# returnselect(Container.DataItem("rowstatus"), Container.DataItem("url"))%>
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("personname")%>
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("jobtitle")%> 
                        </td>
                        <td style="width:80px;">
                            <%# Container.DataItem("type")%> 
                        </td>
                        <td style="width:200px;">
                            <%# Container.DataItem("organisation")%>
                        </td>
                    </tr>
            </ItemTemplate> 
            <FooterTemplate>
                
                </table>
            </FooterTemplate>
            </asp:Repeater>

            <table>
                <tr style="height:10px;"></tr>
                <tr>
                    <td style="width:150px;">
                        <asp:Label ID="lblSearchPage" runat="server"></asp:Label>
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchPrev" runat="server" text="" OnClick="lnkPrev_Click" />
                    </td>
                    <td style="width:150px;">
                        <asp:LinkButton id="lnkSearchNext" runat="server" text="" OnClick="lnkNext_Click" />
                    </td>
                </tr>
            </table>

            <script language="javascript" type="text/javascript">
                var srchOrganisation = document.getElementById("<%=ddlorganisation.ClientID %>");
                var srchBtn = document.getElementById("<%=btnSearch.ClientID %>");
                var srchForename = document.getElementById("<%=forename.ClientID %>");
                var srchSurname = document.getElementById("<%=surname.ClientID %>");
                var srchEmail = document.getElementById("<%=email.ClientID %>");
                

                setEvent(srchOrganisation, "change", doSearch);
                setEvent(srchForename, "keyup", doSearch);
                setEvent(srchSurname, "keyup", doSearch);
                setEvent(srchEmail, "keyup", doSearch);

                //setOnLoad(killBtn);
            </script>

</asp:Content>

