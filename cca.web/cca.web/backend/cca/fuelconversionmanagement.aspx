﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="fuelconversionmanagement.aspx.vb" Inherits="backend_cca_fuelconversionmanagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current Fuel Conversions</h5>
<asp:Repeater ID="rptFuelConversions" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:80px;">
            
        </td>
        <td style="width:100px;">
            <h4>Source Fuel</h4>
        </td>
        <td style="width:50px;">
            <h4>Unit</h4>
        </td>
        <td style="width:100px;">
            <h4>Target Fuel</h4>
        </td>
        <td style="width:50px;">
            <h4>Unit</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid From</h4>
        </td>
        <td style="width:80px;">
            <h4>Valid To</h4>
        </td>
        <td style="width:80px;">
            <h4>Conversion</h4>
        </td>
        <td style="width:30px;">
            
        </td>
        <td></td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("fuelconversion_pk") %>'>
            <td>
                <a href='fuelconversionmanagement.aspx?pk=<%# Container.DataItem("fuelconversion_pk")%>'><h6>Edit</h6></a>
            </td>
            <td>
                <%# Container.DataItem("sourcefuel")%>
            </td>
            <td>
                <%# Container.DataItem("sourceunit")%>
            </td>
            <td>
                <%# Container.DataItem("targetfuel")%>
            </td>
            <td>
                <%# Container.DataItem("targetunit")%>
            </td>
            <td>
                <%# Container.DataItem("validfrom")%>
            </td>
            <td>
                <%# Container.DataItem("validto")%>
            </td>
            <td>
                <%# Container.DataItem("conversionfactor")%>
            </td>
            <td>
                <a class="remove" title="Remove"  href='javascript:removeFuelConversion(<%# Container.DataItem("fuelconversion_pk") %>)'>X</a>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:100px">
                <a href='fuelconversionmanagement.aspx?pk=-1'><h6>New Conversion</h6></a> 
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
                    
<asp:PlaceHolder ID="FuelConversionControl" runat="server" />
<table id="FuelConversionButton" runat="server" style="width:100%;">
    <tr style="height:10px;">
                            
    </tr>
    <tr>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton runat="server" ID="btnSaveFuelConversion" Text="<h6>Save</h6>" />
        </td>
        <td style="width:60%;"></td>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton ID="btnCancelFuelConversion" runat="server" Text="<h4>Cancel</h4>" />
        </td>
    </tr>
</table>
</asp:Content>

