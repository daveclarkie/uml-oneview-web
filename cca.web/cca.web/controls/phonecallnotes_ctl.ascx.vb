Partial Class phonecallnotes_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ophonecallnotes As Objects.phonecallnotes
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_phonecalls_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.phonecall_fk.Items.Count=0 Then
        Me.phonecall_fk.DataSource=CDC.ReadDataTable(CO)
        Me.phonecall_fk.DataTextField = "value"
        Me.phonecall_fk.DataValueField = "pk"
        Try
            Me.phonecall_fk.DataBind
        Catch Ex as Exception
            Me.phonecall_fk.SelectedValue=-1
            Me.phonecall_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_notes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.note_fk.Items.Count=0 Then
        Me.note_fk.DataSource=CDC.ReadDataTable(CO)
        Me.note_fk.DataTextField = "value"
        Me.note_fk.DataValueField = "pk"
        Try
            Me.note_fk.DataBind
        Catch Ex as Exception
            Me.note_fk.SelectedValue=-1
            Me.note_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ophonecallnotes is Nothing then
ophonecallnotes = new Objects.phonecallnotes(currentuser,actualuser,pk,CDC)
End If
Me.phonecall_fk.SelectedValue=ophonecallnotes.phonecall_fk
Me.note_fk.SelectedValue=ophonecallnotes.note_fk
_CurrentPk=ophonecallnotes.phonecallnote_pk
Status=ophonecallnotes.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ophonecallnotes is Nothing then
ophonecallnotes = new Objects.phonecallnotes(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ophonecallnotes.phonecall_fk=Me.phonecall_fk.SelectedValue
ophonecallnotes.note_fk=Me.note_fk.SelectedValue
result=ophonecallnotes.Save()
if not result then throw ophonecallnotes.LastError
_CurrentPk=ophonecallnotes.phonecallnote_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ophonecallnotes.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallnotes is Nothing then
   ophonecallnotes = new Objects.phonecallnotes(currentuser,actualuser,pk,CDC)
  End If
  ophonecallnotes.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallnotes is Nothing then
   ophonecallnotes = new Objects.phonecallnotes(currentuser,actualuser,pk,CDC)
  End If
  ophonecallnotes.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ophonecallnotes is Nothing then
   ophonecallnotes = new Objects.phonecallnotes(currentuser,pk,CDC)
  End If
  ophonecallnotes.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.phonecall_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkphonecall_fk.Visible=IIF(RL>=0,True,False)
  Me.note_fk.Enabled=IIF(WL>=0,True,False)
  Me.blknote_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

