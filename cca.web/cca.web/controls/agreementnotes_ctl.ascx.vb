Partial Class agreementnotes_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreementnotes As Objects.agreementnotes
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_notes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.note_fk.Items.Count = 0 Then
            Me.note_fk.DataSource = CDC.ReadDataTable(CO)
            Me.note_fk.DataTextField = "value"
            Me.note_fk.DataValueField = "pk"
            Try
                Me.note_fk.DataBind()
            Catch Ex As Exception
                Me.note_fk.SelectedValue = -1
                Me.note_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oagreementnotes Is Nothing Then
            oagreementnotes = New Objects.agreementnotes(currentuser, actualuser, pk, CDC)
        End If
        Me.agreement_fk.SelectedValue = oagreementnotes.agreement_fk
        Me.note_fk.SelectedValue = oagreementnotes.note_fk
        _CurrentPk = oagreementnotes.agreementnote_pk
        Status = oagreementnotes.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementnotes Is Nothing Then
            oagreementnotes = New Objects.agreementnotes(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreementnotes.agreement_fk = Me.agreement_fk.SelectedValue
            oagreementnotes.note_fk = Me.note_fk.SelectedValue
            result = oagreementnotes.Save()
            If Not result Then Throw oagreementnotes.LastError
            _CurrentPk = oagreementnotes.agreementnote_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreementnotes.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementnotes Is Nothing Then
            oagreementnotes = New Objects.agreementnotes(currentuser, actualuser, pk, CDC)
        End If
        oagreementnotes.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementnotes Is Nothing Then
            oagreementnotes = New Objects.agreementnotes(currentuser, actualuser, pk, CDC)
        End If
        oagreementnotes.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreementnotes Is Nothing Then
            oagreementnotes = New Objects.agreementnotes(currentuser, pk, CDC)
        End If
        oagreementnotes.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.agreement_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreement_fk.Visible = IIF(RL >= 0, True, False)
        Me.note_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blknote_fk.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

