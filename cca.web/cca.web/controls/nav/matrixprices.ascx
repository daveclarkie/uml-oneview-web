﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="matrixprices.ascx.vb" Inherits="controls_nav_matrixprices" %>
<%If LoadList() Then%>
    <h3>Current Matrix</h3>
    <div class="navigationmenu" id="pnlOtherVersions" runat="server">
        <asp:Repeater ID="rptPrices" runat="server">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td><h4>Description</h4></td>
                    </tr>        
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Container.DataItem("shorttag")%> 
                    </td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
<%End If%>
