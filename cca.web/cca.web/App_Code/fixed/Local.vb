Imports System.Collections.Generic
Imports System.IO
Imports System.Net

Public Class Local

    Public Const AuthURL As String = ""


    Public Enum Action As Integer
        None = 0
        Read = 1
        Save = 2
    End Enum


    Shared Function ImgPath(ByRef RQ As HttpRequest) As String
        Return "http://" & RQ.ServerVariables("HTTP_HOST") & "/design/images/"
    End Function

    Shared Function BaseUrl(ByRef RQ As HttpRequest) As String
        Return "http://" & RQ.ServerVariables("HTTP_HOST") & "/"
    End Function

    Shared Function noCache() As String
        Return "&nc=" & Now.Ticks.ToString
    End Function

    Shared Function LoadPageSpecificJs(ByRef Server As HttpServerUtility, ByVal PageName As String) As String
        Dim js As String = ""
        Dim pth As String = "/" & PageName.Replace("_aspx", "").Replace("_", "/") & ".js"
        If IO.File.Exists(Server.MapPath(pth)) Then
            js = "<script type=""text/javascript"" language=""javascript"" src=""" & pth & """></script>"
        End If
        Return js
    End Function

    Shared Sub Bounce(ByRef SM As MySM, ByRef RQ As HttpResponse, ByVal DestURL As String, ByVal Reason As String)
        Try
            SM.Save()
            RQ.Redirect(DestURL & "?r=" & Reason, False)
        Catch ex As Exception
        End Try
    End Sub

    Shared Sub FOAD(ByRef RQ As HttpResponse, ByRef SM As MySM, ByVal Reason As String, Optional ByVal DestURL As String = "~/disconnect.aspx")
        Try
            If SM.redirected = 1 Then Exit Sub
            ' SM.Purge
            Bounce(SM, RQ, DestURL, Reason)
        Catch ex As Exception
        End Try
    End Sub

    Shared Function Display(ByVal displaydate As DateTime, Optional ByVal isEnd As Integer = 0) As String
        Return displaydate.ToString("dd MMM yyyy")
    End Function

    Shared Function Display(ByVal obj As DBNull, Optional ByVal isEnd As Integer = 0) As String
        Select Case isEnd
            Case 1
                Return "No date last occupied"
            Case Else
                Return "No date first occupied"
        End Select
    End Function

    Shared Function DateOnly(ByVal Start As DateTime) As DateTime
        Dim ts As New TimeSpan(0, Start.Hour, Start.Minute, Start.Second, Start.Millisecond)
        Return Start.Subtract(ts)
    End Function

    Shared Function DueDate(ByVal Start As DateTime, ByVal Unit As Integer, ByVal duration As Integer) As DateTime
        Select Case Unit
            Case 13 'minute
                Return Start.AddMinutes(duration)
            Case 14 'hour
                Return Start.AddHours(duration)
            Case 15 'day
                Return Start.AddDays(duration)
            Case 16 'weekday
                Return Start.AddDays(CInt(duration * (5 / 7)))
            Case 17 'week
                Return Start.AddDays(duration * 7)
            Case 18 'month
                Return Start.AddMonths(duration)
            Case 19 'quarter
                Return Start.AddMonths(duration * 3)
            Case 20 'year
                Return Start.AddYears(duration)
            Case Else
                Return Now
        End Select
    End Function

    Shared Function IntExists(ByVal obj As Object) As Integer
        If obj Is Nothing Then
            Return 0
        Else
            Return obj
        End If
    End Function

    Shared Function StrExists(ByVal obj As Object) As String
        If obj Is Nothing Then
            Return ""
        Else
            Return obj
        End If
    End Function

    Shared Function SystemType() As String
        Dim Host As String = HttpContext.Current.Request.Url.ToString.Split("/".ToCharArray(), 5, StringSplitOptions.RemoveEmptyEntries)(1)
        Host = Host.ToLower
        Dim subDomain As String = Host.Split(".".ToCharArray, 2)(0)
        Dim system As String = ""
        Select Case True
            Case subDomain.StartsWith("live"), subDomain.StartsWith("dev"), Host.StartsWith("seweb.live."), Host.StartsWith("oneview.")
                system = "live"
            Case Host.StartsWith("seweb.testdev.")
                system = "testing"
            Case subDomain.StartsWith("localhost"), subDomain.StartsWith("127"), Host.StartsWith("seweb.dev.")
                system = "development"
            Case Else
                system = "unauthorised"
        End Select

        Return system
    End Function

    Shared Function GetInversedDataTable(ByVal table As DataTable, ByVal columnX As String, ByVal columnY As String, ByVal columnZ As String, ByVal nullValue As String, ByVal sumValues As Boolean) As DataTable
        ' ColumnX = Is the Column that you want to make cross tab (Column shown in header)
        ' ColumnY = Is the column that will be put in the left column (company_code)
        ' ColumnZ = Is the total value from combining columnX and columnY (PAY_AMOUNT)

        'Create a DataTable to Return
        Dim returnTable As New DataTable()
        If columnX = "" Then
            columnX = table.Columns(0).ColumnName
        End If

        'Add a Column at the beginning of the table
        returnTable.Columns.Add(columnY)

        'Read all DISTINCT values from columnX Column in the provided DataTale
        Dim columnXValues As New List(Of String)()

        For Each dr As DataRow In table.Rows
            Dim columnXTemp As String = dr(columnX).ToString()
            If Not columnXValues.Contains(columnXTemp) Then
                'Read each row value, if it's different from others provided, add to the list of values and creates a new Column with its value.
                columnXValues.Add(columnXTemp)
                returnTable.Columns.Add(columnXTemp)
            End If
        Next

        'Verify if Y and Z Axis columns re provided
        If columnY <> "" AndAlso columnZ <> "" Then
            'Read DISTINCT Values for Y Axis Column
            Dim columnYValues As New List(Of String)()

            For Each dr As DataRow In table.Rows
                If Not columnYValues.Contains(dr(columnY).ToString()) Then
                    columnYValues.Add(dr(columnY).ToString())
                End If
            Next

            'Loop all Column Y Distinct Value
            For Each columnYValue As String In columnYValues
                'Creates a new Row
                Dim drReturn As DataRow = returnTable.NewRow()
                drReturn(0) = columnYValue
                'foreach column Y value, The rows are selected distincted
                Dim rows As DataRow() = table.[Select]((columnY & "='") + columnYValue & "'")

                'Read each row to fill the DataTable
                For Each dr As DataRow In rows
                    Dim rowColumnTitle As String = dr(columnX).ToString()

                    'Read each column to fill the DataTable
                    For Each dc As DataColumn In returnTable.Columns
                        If dc.ColumnName = rowColumnTitle Then
                            'If Sum of Values is True it try to perform a Sum
                            'If sum is not possible due to value types, the value displayed is the last one read
                            If sumValues Then
                                Try
                                    drReturn(rowColumnTitle) = Convert.ToDecimal(drReturn(rowColumnTitle)) + Convert.ToDecimal(dr(columnZ))
                                Catch
                                    drReturn(rowColumnTitle) = dr(columnZ)
                                End Try
                            Else
                                drReturn(rowColumnTitle) = dr(columnZ)
                            End If
                        End If
                    Next
                Next

                returnTable.Rows.Add(drReturn)
            Next
        Else
            Throw New Exception("The columns to perform inversion are not provided")
        End If

        'if a nullValue is provided, fill the datable with it
        If nullValue <> "" Then
            For Each dr As DataRow In returnTable.Rows
                For Each dc As DataColumn In returnTable.Columns
                    If dr(dc.ColumnName).ToString() = "" Then
                        dr(dc.ColumnName) = nullValue
                    End If
                Next
            Next
        End If
        Return returnTable

    End Function


    Shared Sub OpenCustomFile(ByVal url As String, ByVal filename As String)
        Dim stream As Stream = Nothing
        Dim bytesToRead As Integer = 10000
        Dim buffer As Byte() = New Byte(bytesToRead - 1) {}

        Try
            Dim fileReq As HttpWebRequest = CType(HttpWebRequest.Create(url), HttpWebRequest)
            Dim fileResp As HttpWebResponse = CType(fileReq.GetResponse(), HttpWebResponse)
            If fileReq.ContentLength > 0 Then fileResp.ContentLength = fileReq.ContentLength
            stream = fileResp.GetResponseStream()
            Dim resp = HttpContext.Current.Response
            resp.ContentType = "application/octet-stream"
            resp.AddHeader("Content-Disposition", "attachment; filename=""" & filename & """")
            resp.AddHeader("Content-Length", fileResp.ContentLength.ToString())
            Dim length As Integer

            Do

                If resp.IsClientConnected Then
                    length = stream.Read(buffer, 0, bytesToRead)
                    resp.OutputStream.Write(buffer, 0, length)
                    resp.Flush()
                    buffer = New Byte(bytesToRead - 1) {}
                Else
                    length = -1
                End If
            Loop While length > 0

        Finally

            If stream IsNot Nothing Then
                stream.Close()
            End If
        End Try
    End Sub

End Class
