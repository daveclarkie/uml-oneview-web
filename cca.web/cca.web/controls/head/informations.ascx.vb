Imports System.Data.SqlClient

Partial Class headctls_informations
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        

    End Sub
    Dim _label As String = ""
    Dim _items As Integer = 0
    Dim _informationItemType As InformationItemType = InformationItemType.CountOrganisations

    Public ReadOnly Property Items() As Integer
        Get
            Return _items
        End Get
    End Property

    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public ReadOnly Property State() As String
        Get
            Select Case InformationItemType
                Case InformationItemType.CountOrganisations
                    Return " green"
                Case cca.common.InformationItemType.organisation_appointments, cca.common.InformationItemType.organisation_ccaagreements, cca.common.InformationItemType.organisation_contracts, cca.common.InformationItemType.organisation_supplypoints
                    Select Case _items
                        Case 0 : Return " red"
                        Case Else : Return " green"
                    End Select
                Case Else
                    Select Case _items
                        Case 0 : Return " red"
                        Case Is < 6 : Return " yellow"
                        Case Else : Return " green"
                    End Select
            End Select
            Return ""
        End Get
    End Property

    Public Property InformationItemType() As InformationItemType
        Get
            Return _informationItemType
        End Get
        Set(ByVal value As InformationItemType)
            _informationItemType = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Refresh()
    End Sub

    Public Sub Refresh()
        Dim ts As Long
        Dim qs As Long
        Dim qd As Long
        Dim ad As Long

        ts = Now.Ticks
        If CDC Is Nothing Then CDC = New DataCommon

        Dim CO As New SqlCommand

        Select Case InformationItemType
            Case InformationItemType.CountOrganisations
                CO.CommandText = "rsp_sys_info_countorganisations"
            Case InformationItemType.CountAgreements
                CO.CommandText = "rsp_sys_info_countagreements"
            Case InformationItemType.CountMeterPoints
                CO.CommandText = "rsp_sys_info_countactivemeterpoints"
            Case InformationItemType.CountThroughputs
                CO.CommandText = "rsp_sys_info_countsitethroughputs"
            Case InformationItemType.CountSites
                CO.CommandText = "rsp_sys_info_countsites"
            Case InformationItemType.CountMeterPointsType1
                CO.CommandText = "rsp_sys_info_countactivemeterpoints"
                CO.Parameters.AddWithValue("@type", 1)
            Case InformationItemType.CountMeterPointsType2
                CO.CommandText = "rsp_sys_info_countactivemeterpoints"
                CO.Parameters.AddWithValue("@type", 2)
            Case InformationItemType.CountMeterPointsType3
                CO.CommandText = "rsp_sys_info_countactivemeterpoints"
                CO.Parameters.AddWithValue("@type", 3)
            Case InformationItemType.CountUKElecPrices
                CO.CommandText = "rsp_sys_info_countmatrix_ukelecprices"
            Case InformationItemType.CountUKGasPrices
                CO.CommandText = "rsp_sys_info_countmatrix_ukgasprices"
            Case InformationItemType.CountMatrixToday
                CO.CommandText = "rsp_sys_info_countmatrix_today"
            Case InformationItemType.CountCCAReports
                CO.CommandText = "rsp_sys_info_countmatrix_today"
            Case InformationItemType.helpdesk_openrequests
                CO.CommandText = "SELECT COUNT(a.WORKORDERID) [value] FROM WorkOrder a, WorkOrderStates b WHERE a.WORKORDERID = b.WORKORDERID AND b.STATUSID IN (SELECT STATUSID FROM dbo.StatusDefinition WHERE STATUSNAME NOT LIKE 'Closed%' AND STATUSNAME NOT LIKE 'On Hold%' AND STATUSNAME NOT LIKE 'Resolved%') AND b.OWNERID = " & HelpdeskUserID(SM.currentuser)
            Case InformationItemType.countmyopensurveys
                CO.CommandText = "rsp_sys_info_countmyopen_surveys"
            Case InformationItemType.organisation_supplypoints
                CO.CommandText = "rsp_sys_info_organisation_supplypoints"
                CO.Parameters.AddWithValue("@organisation_pk", CInt(Request.QueryString("pk")))
            Case InformationItemType.organisation_contracts
                CO.CommandText = "rsp_sys_info_organisation_contracts"
                CO.Parameters.AddWithValue("@organisation_pk", CInt(Request.QueryString("pk")))
            Case InformationItemType.organisation_ccaagreements
                CO.CommandText = "rsp_sys_info_organisation_ccaagreements"
                CO.Parameters.AddWithValue("@organisation_pk", CInt(Request.QueryString("pk")))
            Case InformationItemType.organisation_appointments
                CO.CommandText = "rsp_sys_info_organisation_appointments"
                CO.Parameters.AddWithValue("@organisation_pk", CInt(Request.QueryString("pk")))
            Case Else
                CO.Dispose()
                Exit Sub
        End Select

        qs = Now.Ticks

        Select Case True
            Case InformationItemType.ToString.StartsWith("helpdesk_")
                CO.CommandType = CommandType.Text
                Try
                    CDC.ReadScalarValue(_items, CO, 1)
                Catch ex As Exception

                End Try
            Case Else
                CO.CommandType = CommandType.StoredProcedure
                CDC.ReadScalarValue(_items, CO)
        End Select

        CO.Dispose()
        qd = Now.Ticks

        Dim ctl As String = ""
        'ctl &= "<a href=""./stats.aspx?s="
        'ctl &= Integer.Parse(InformationItemType).ToString
        'ctl &= """ class=""notifylink"">"
        ctl &= "<a class=""notifylink"">"
        ctl &= "<div class=""notify"
        ctl &= State
        ctl &= """><span class=""count"">"
        ctl &= Items
        ctl &= "</span><br /><span class=""counttype"">"
        ctl &= Label
        ctl &= "</span></div></a>"
        informationitem.Text = ctl
        ad = Now.Ticks

        'System.Diagnostics.Debug.WriteLine(" HD : " & HelpdeskUserID(SM.currentuser))
        'System.Diagnostics.Debug.WriteLine(InformationItemType.ToString & " ts-qs : " & (qs - ts).ToString)
        'System.Diagnostics.Debug.WriteLine(InformationItemType.ToString & " qs-qd : " & (qd - qs).ToString)
        'System.Diagnostics.Debug.WriteLine(InformationItemType.ToString & " qd-ad : " & (ad - qd).ToString)
        'System.Diagnostics.Debug.WriteLine(InformationItemType.ToString & " ts-ad : " & (ad - ts).ToString)
    End Sub

    Private Function HelpdeskUserID(ByVal currentuser_pk As Integer) As Integer
        Dim _output As Integer = -1
        Dim CO As New SqlCommand

        CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("user_fk", currentuser_pk)
        Dim helpdesklogindetail_pk As Integer = -1
        CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

        Dim hd As New helpdesklogindetails(SM.currentuser, SM.actualuser, helpdesklogindetail_pk, CDC)
        Dim hdd As New helpdeskdomains(SM.currentuser, SM.actualuser, hd.helpdeskdomain_fk, CDC)


        Dim x As String = ""

        CO = New SqlCommand("SELECT a.[USERID] FROM sduser a, AaaUser b, AaaLogin c WHERE a.USERID = b.USER_ID AND a.USERID = c.USER_ID AND c.DOMAINNAME = '" & hdd.domain_name & "' AND c.NAME = '" & hd.username & "'")
        CO.CommandType = CommandType.Text
        Dim helpdeskuser_id As Integer = -1
        Try
            CDC.GetConnection_debug(x, CO, 1)
            CDC.ReadScalarValue(helpdeskuser_id, CO, 1)
        Catch ex As Exception
            'MsgBox(ex.Message.ToString)
        End Try
        

        _output = helpdeskuser_id

        Return _output
    End Function
End Class
