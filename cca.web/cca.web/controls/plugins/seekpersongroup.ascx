<%@ Control Language="VB" AutoEventWireup="false" CodeFile="seekpersongroup.ascx.vb" Inherits="controls_plugins_seekpersongroup" %>
<a href="javascript:seek_<%=ControlName %>('<%=TargetValueControlName %>','<%=TargetDisplayControlName %>','<%=FilterGroup %>')"><%=Label %></a>
<div id="seek_<%=ControlName %>_control" class="seek_container" style="display:none;">
    <div class="seek_control">    
        <div id="seek_<%=ControlName %>_titlebar" class="seek_titlebar">User Search</div><div id="seek_<%=ControlName %>_exitbar" class="seek_exitbar"><a href="javascript:seek_<%=ControlName %>_exit()">X</a></div>
        <div id="seek_<%=ControlName %>_panel" class="seek_panel">
            <div id="seek_<%=ControlName %>_search" class="seek_search" style="display:none;">
            
                <span class="seek_title">Search</span><br />
             
                <ul class="seek_ul_form">
                    <li><span class="label">Forename</span><input type="text" name="fn_<%=ControlName %>" id="fn_<%=ControlName %>" /></li>
                    <li><span class="label">Surname</span><input type="text" name="sn_<%=ControlName %>" id="sn_<%=ControlName %>" /></li>
                    <li><span class="label">Username</span><input type="text" name="un_<%=ControlName %>" id="un_<%=ControlName %>" /></li>
                    <li><span class="label">&nbsp;</span><a href="javascript:seek_<%=ControlName %>_search()">Search</a></li> 
                </ul>
            
            </div>
            
            <div id="seek_<%=ControlName %>_wait" class="seek_wait" style="display:none;">
            
                <span class="seek_title">Searching....</span><img alt="" src="/design/images/ajax-loader-system.gif" />

            </div>
            
            <div id="seek_<%=ControlName %>_result" class="seek_result" style="display:none;">
            
                <span class="seek_title">Results</span><br />

                <ul id="ul_<%=ControlName %>"  class="seek_ul_result">
                </ul>            
            </div>
       </div> 
   </div>
</div>

<script language="javascript" type="text/javascript">

var target_value_<%=ControlName %>;
var target_display_<%=ControlName %>;
var target_filter_<%=ControlName %>;


function clear_<%=ControlName %>_list(){
    var ulres=document.getElementById("ul_<%=ControlName %>");
    try
        { 
            while (ulres.hasChildNodes)
            {
                ulres.removeChild(ulres.lastChild);
            }
        }
   catch (e) {
   } 
}

function fill_<%=ControlName %>_list(results){
    var ulres=document.getElementById("ul_<%=ControlName %>");
    var user_pk;
    var username;
    var forename;
    var surname;
    var active;

    var lstuser=results.getElementsByTagName("users");

    for(var i=0; i<lstuser.length; i++)
    {
        user_pk=lstuser[i].getElementsByTagName("user_pk")[0].childNodes[0].nodeValue;
        try {username=lstuser[i].getElementsByTagName("username")[0].childNodes[0].nodeValue;} catch (e) {username='N/A';}
        try {forename=lstuser[i].getElementsByTagName("forename")[0].childNodes[0].nodeValue;} catch (e) {forename='N/A';}
        try {surname=lstuser[i].getElementsByTagName("surname")[0].childNodes[0].nodeValue;} catch (e) {surname='N/A';}
        try {active=lstuser[i].getElementsByTagName("rowstatus")[0].childNodes[0].nodeValue;} catch (e) {active='N/A';}

        li=document.createElement("li");

        a=document.createElement("a");
        txt=document.createTextNode(forename+" "+surname+" ["+user_pk+"]");
        a.href="javascript:seek_<%=ControlName %>_return('"+user_pk+"','"+forename+" "+surname+"')";
     
        a.appendChild(txt);
        li.appendChild(a);
        
        ulres.appendChild(li);  

    }

}

function callback_seek_<%=ControlName %>_result()
{
    xmlhttp.onreadystatechange=function() 
    {

        if (xmlhttp.readyState==4) 
        {
            var sps=document.getElementById('seek_<%=ControlName %>_search');
            var spw=document.getElementById('seek_<%=ControlName %>_wait');
            var spr=document.getElementById('seek_<%=ControlName %>_result');

            xmlDoc=loadXMLString(xmlhttp.responseText);
            var c=xmlDoc.getElementsByTagName("error").length;
            if (c!=0)
            {
                alert(xmlhttp.responseText);
                sps.style.display="block"; 
                spw.style.display="none"; 
                spr.style.display="none"; 
            }
            else
            {
                c=xmlDoc.getElementsByTagName("no-one").length;
                if (c!=0)
                {
                    alert("No users matched your criteria");
                    sps.style.display="block"; 
                    spw.style.display="none"; 
                    spr.style.display="none"; 
                }
                else
                {
                    c=xmlDoc.getElementsByTagName("results").length;
                    if (c!=0){
                        clear_<%=ControlName %>_list();
                        fill_<%=ControlName %>_list(xmlDoc);
                        sps.style.display="none"; 
                        spw.style.display="none"; 
                        spr.style.display="block"; 

                    }else{
                        alert(xmlhttp.responseText);
                        sps.style.display="block"; 
                        spw.style.display="none"; 
                        spr.style.display="none"; 

                    }
                }
            }


        }
    }
}

function seek_<%=ControlName %>_search(){
    var fn=document.getElementById('fn_<%=ControlName %>').value;
    var sn=document.getElementById('sn_<%=ControlName %>').value;
    var un=document.getElementById('un_<%=ControlName %>').value;

    var sps=document.getElementById('seek_<%=ControlName %>_search');
    var spw=document.getElementById('seek_<%=ControlName %>_wait');
    var spr=document.getElementById('seek_<%=ControlName %>_result');
    sps.style.display="none"; 
    spw.style.display="block"; 
    spr.style.display="none"; 

    setReqHttp();

    var postData="fn="+fn;
    postData=postData+"&sn="+sn;
    postData=postData+"&un="+un;
    postData=postData+"&gp="+target_filter_<%=ControlName %>;
    
    xmlhttp.open("POST","/functions/seekpgroup.ashx?a=0"+noCache(),false);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    callback_seek_<%=ControlName %>_result();
    xmlhttp.send(postData);

}

function seek_<%=ControlName %>_return(selPk,selValue){
    chgNamedElementText(target_display_<%=ControlName %>,chgReplace,selValue); 
    chgNamedElementText(target_value_<%=ControlName %>,chgReplace,selPk);  
    var spc=document.getElementById('seek_<%=ControlName %>_control');
    spc.style.display="none";
   <% If TargetFunctionName.length>0 Then %>
   try {
   <%=TargetFunctionName %>(selPk);
   }
   catch (e) {
    alert("error:"+e);
   }
   <% End If %>  
}

function seek_<%=ControlName %>_exit(){
   var spc=document.getElementById('seek_<%=ControlName %>_control');
   spc.style.display="none"; 
}

function seek_<%=ControlName %>(targetvalue,targetdisplay,targetfilter){
   var spc=document.getElementById('seek_<%=ControlName %>_control');
   var ttl=document.getElementById('seek_<%=ControlName %>_titlebar');
   var sps=document.getElementById('seek_<%=ControlName %>_search');
   var spr=document.getElementById('seek_<%=ControlName %>_result');
   var spw=document.getElementById('seek_<%=ControlName %>_wait');
   target_value_<%=ControlName %>=targetvalue;
   target_display_<%=ControlName %>=targetdisplay;
   target_filter_<%=ControlName %>=targetfilter;
   
    chgNamedElementText(target_display_<%=ControlName %>,chgReplace,''); 
    document.getElementById(target_value_<%=ControlName %>).value=''; 

   spc.style.display="block"; 
   sps.style.display="block"; 
   spw.style.display="none"; 
   spr.style.display="none"; 

}


</script>  