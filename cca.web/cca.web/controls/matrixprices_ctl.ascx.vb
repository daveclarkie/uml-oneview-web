Partial Class matrixprices_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents omatrixprices As objects.matrixprices
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rsp_matrixtypes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
        If Me.matrixtype_fk.Items.Count = 0 Then
            Me.matrixtype_fk.DataSource = CDC.ReadDataTable(CO)
            Me.matrixtype_fk.DataTextField = "value"
            Me.matrixtype_fk.DataValueField = "pk"
            Try
                Me.matrixtype_fk.DataBind()
            Catch Ex As Exception
                Me.matrixtype_fk.SelectedValue = -1
                Me.matrixtype_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If omatrixprices Is Nothing Then
            omatrixprices = New Objects.matrixprices(currentuser, actualuser, pk, CDC)
        End If
        Me.matrixtype_fk.SelectedValue = omatrixprices.matrixtype_fk
        Me.matrixdetail_fk.text = omatrixprices.matrixdetail_fk
        Me.matrix_fk.Text = omatrixprices.matrix_fk
        _CurrentPk = omatrixprices.matrixprice_pk
        Status = omatrixprices.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If omatrixprices Is Nothing Then
            omatrixprices = New Objects.matrixprices(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            omatrixprices.matrixtype_fk = Me.matrixtype_fk.SelectedValue
            omatrixprices.matrixdetail_fk = SM.matrixdetail
            omatrixprices.matrix_fk = SM.matrix
            result = omatrixprices.Save()
            If Not result Then Throw omatrixprices.LastError
            _CurrentPk = omatrixprices.matrixprice_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = omatrixprices.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If omatrixprices Is Nothing Then
            omatrixprices = New Objects.matrixprices(currentuser, actualuser, pk, CDC)
        End If
        omatrixprices.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If omatrixprices Is Nothing Then
            omatrixprices = New Objects.matrixprices(currentuser, actualuser, pk, CDC)
        End If
        omatrixprices.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If omatrixprices Is Nothing Then
            omatrixprices = New Objects.matrixprices(currentuser, pk, CDC)
        End If
        omatrixprices.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.matrixtype_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkmatrixtype_fk.Visible = IIF(RL >= 0, True, False)
        Me.matrixdetail_fk.Enabled = IIF(WL >= 99, True, False)
        Me.blkmatrixdetail_fk.Visible = IIF(RL >= 99, True, False)
        Me.matrix_fk.Enabled = IIF(WL >= 99, True, False)
        Me.blkmatrix_fk.Visible = IIF(RL >= 99, True, False)
    End Sub
End Class

