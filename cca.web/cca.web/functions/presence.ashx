<%@ WebHandler Language="VB" Class="functions_presence" %>
Public Class functions_presence
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim CRQ As Object = Request.QueryString
        Dim Msg As String = ""
        Dim Fn As String = ""
        Try
            Dim lastAction As Integer = -1 ' time since last activity 
            Dim Deceased As Integer = -1 ' is a member of the household deceased 
            Dim CO As New SqlCommand
            Dim mesg As messages = Nothing
            Dim usrmsg_pk As Integer = -1
            Dim usermesg As usermessages = Nothing
            Dim ts As TimeSpan
            CO = New SqlCommand
            CO.CommandText = "rsp_primarymessage"
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@pk", SM.currentuser)
            If SM.targetuser >0 Then  ' don't return message relating to currently selected user
                CO.Parameters.AddWithValue("@user", SM.targetuser)
            Else
                CO.Parameters.AddWithValue("@user", 0)
            End If
           Dim noMesg as Boolean=True
           If CDC.ReadScalarValue(usrmsg_pk, CO) Then
                If usrmsg_pk>0 Then
                usermesg = New usermessages(SM.currentuser, SM.actualuser, usrmsg_pk, CDC)
                mesg = New messages(SM.currentuser, SM.actualuser, usermesg.message_fk, CDC)
                noMesg=False                
                End If 
           End If 
            
            CO = New SqlCommand
            CO.CommandText = "rsp_checkin"
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@pk", SM.currentuser)
            CDC.ReadScalarValue(lastAction, CO)
            
            'co = new SqlCommand
            'CO.CommandText = "rsp_household_deceased"
            'CO.CommandType = CommandType.StoredProcedure
            'CO.Parameters.AddWithValue("@u", SM.targetuser)
            'CDC.ReadScalarValue(Deceased, CO)
                        
            CO.Dispose()            
            
            '        /* 
            '            Message format
            '            status : OK/FAIL
            '            action : 0 (No action) / 1 (General message) / 2 (jump to client) / 3 (shutdown / log out)
            '            next check due in ? seconds
            '            message
            '            function
            '        */
            
            Msg = ""
            Fn = ""
            Dim mesgSet As Boolean = False
            If not noMesg Then
                Select Case mesg.messagetype_fk
                    Case 1 ' General
                        Msg = "OK|1|10|" & mesg.message & "<br /><a href=""javascript:ackMessage(" & usermesg.usermessage_pk & ");"">Acknowledge Message</a>"
                    Case 2 ' Jump to client
                        Msg = "OK|2|10|" & mesg.message & "<br /><a href=""javascript:ackMessage(" & usermesg.usermessage_pk & ");jumpTo(" & mesg.targetuser_fk & ");"">View Client</a>"
                    Case 3 ' Shutdown
                        ts = Now.Subtract(mesg.created)
                        Select Case ts.TotalMinutes
                            Case Is < 10.0
                                Msg = "OK|3|10|" & mesg.message & "<br />Automated log out begins in " & CInt(-(ts.Subtract(New TimeSpan(0, 10, 0))).TotalSeconds).ToString & " seconds. Save your work and log out"
                                Disconnect()
                            Case 10.0 To 15.0
                                Msg = "OK|3|10|" & mesg.message & "<br />Automated log out active. Good bye."
                                Fn = "|logOut()"
                        End Select
                End Select
            End If
            
            'IF Deceased > 0 THEN
            '    If Msg = "" Then Msg = "OK|1|10|" Else Msg &= "<br />"
            '    Msg &= "Notification: There is a deceased member of the household."
            
            'END IF
            
            'If lastAction >= 300 Then
                
            '    If Msg = "" Then Msg = "OK|1|10|" Else Msg &= "<br />"
            '    Select Case lastAction
            '        Case 600 To 630
            '            Msg &= "Automated log out active. Good bye! (normally)"
            '            'Fn = "|logOut()"
            '            'Disconnect()
            '        Case Is > 300
            '            Msg &= "Still here? You have " & (600 - lastAction).ToString & " seconds before you are automatically logged out<br /><a href=""javascript:setPresence();"">Confirm presence</a>"
            '    End Select
            'Else
            '    If Msg = "" Then Msg = "OK|0|10|"
            'End If
            
            If Msg = "" Then Msg = "OK|0|10|"
        Catch LE As Exception
            Msg = "FAIL|0|10|" & LE.Message
        End Try
        Response.Write(Msg & Fn)
    End Sub
 
    Private Sub Disconnect()
        Dim u as New users(SM.currentuser,SM.actualuser,SM.currentuser,CDC)
        u.disconnect=True 
        u.save 
        u=nothing
    End Sub

End Class

