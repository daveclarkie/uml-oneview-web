﻿<%@ Page Language="VB" MasterPageFile="~/design/masters/external.master" AutoEventWireup="false" CodeFile="survey.aspx.vb" Inherits="coresurvey_survey" title="Survey Centre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <h3>Welcome to the Schneider Electric Survey Centre</h3>

    <asp:Label ID="lbl_errormessage" runat="server" ForeColor="Red" ></asp:Label><br />
    <asp:Panel ID="pnl_helpdesk" runat="server" Visible="false">
        <h4 style="width:555px;">Request Summary</h4>
        <ul class='formcontrol' style="background-color:#FFFFA3;width:555px;">
            <li runat="server" id="Li1">
                <span class='label'><strong>request ID</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_requestid" runat="server" Enabled="false" Width="250px" BackColor="Transparent"  />
            </li>
            <li runat="server" id="Li2">
                <span class='label'><strong>Created</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_created" runat="server" Enabled="false" Width="250px"  BackColor="Transparent" />
            </li>
            <li runat="server" id="Li3">
                <span class='label'><strong>Client</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_client" runat="server" Enabled="false" Width="250px"  BackColor="Transparent" />
            </li>
            <li runat="server" id="Li8">
                <span class='label'><strong>Commodity</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_commodity" runat="server" Enabled="false" Width="250px"  BackColor="Transparent" />
            </li>
            <li runat="server" id="Li4">
                <span class='label'><strong>Client Manager</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_requester" runat="server" Enabled="false" Width="250px" BackColor="Transparent"  />
            </li>
            <li runat="server" id="Li5">
                <span class='label'><strong>Analyst</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_technician" runat="server" Enabled="false" Width="250px" BackColor="Transparent"  />
            </li>
            <li runat="server" id="Li6">
                <span class='label'><strong>Country</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_country" runat="server" Enabled="false" Width="250px" BackColor="Transparent"  />
            </li>
            <li runat="server" id="Li7">
                <span class='label'><strong>Status</strong></span>
                <asp:TextBox EnableViewState="false" ID="lbl_hd_status" runat="server" Enabled="false" Width="250px" BackColor="Transparent"  />
            </li>
        </ul>
    </asp:Panel>
    <br />
    
    <asp:Panel ID="pnl_survey" runat="server" Visible="false">
        <asp:PlaceHolder ID="SurveyControl" runat="server" />
        <table id="AgreementTargetButton" runat="server" style="width:455px;">
            <tr style="height:10px;">
                            
            </tr>
            <tr>
                <td style="text-align:center;">
                    <asp:Linkbutton runat="server" ID="btn_savesurvey" Text="<h6>Save</h6>" Width="555" />
                </td>
                
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnl_complete" runat="server" Visible="false">
        <asp:Label ID="lbl_completemessage" runat="server" Text="Thank you for taking time to complete the survey."></asp:Label>

    </asp:Panel>


</asp:Content>


