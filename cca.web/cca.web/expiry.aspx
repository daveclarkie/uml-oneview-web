<%@ Page Language="VB" MasterPageFile="~/design/masters/external.master" AutoEventWireup="false" CodeFile="expiry.aspx.vb" Inherits="expiry" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h3>Current Session Expired</h3>
    You have been logged out of the system due to your session expiring. Click <a href="/login.aspx">here</a> to log in.
</asp:Content>

