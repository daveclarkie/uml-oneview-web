<%@ WebHandler Language="VB" Class="function_orgsearch" %>
Public Class function_orgsearch
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub
    
    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Dim RQ As Object = Request.QueryString
        Dim RF As Object = Request.Form
        Dim R As Object = Response
        Dim name As String = ""
        Dim pcode As String = ""
        Dim create As Integer = 0
        Try
            name = RF("n").ToString.Trim()
        Catch ex As Exception
            name = ""
        End Try

        Try
            pcode = RF("p").ToString.Trim()
        Catch ex As Exception
            pcode = ""
        End Try

        Try
            create = RF("c")
        Catch ex As Exception
            create = 0
        End Try


        Try
            If name.Length = 0 And create = 0 Then
                Msg = ""
                Msg &= "<div class='modal_titlebar'>Locate Organisation</div><div class='modal_exitbar'><a href=""javascript:showHide('orgDisplay');"">X</a></div>"
                Msg &= "   <div class='modal_panel' id='orgDisplayInner'>"
                Msg &= "       <ul class='formcontrol'>"
                Msg &= "           <li><span class='label'>Organisation</span><input type='text' id='orgname_search' name='orgname_search'></li>"
                Msg &= "           <li><span class='label'>Postcode</span><input type='text' id='orgpcode_search' name='orgpcode_search'></li>"
                Msg &= "           <li><span class='label'>&nbsp;</span><a href='javascript:searchOrganisation()'>Create/Locate</a></li>"
                Msg &= "       </ul>"
                Msg &= "   </div>"
                Msg &= "</div>"
            ElseIf name.Length > 0 And create = 0 Then



                Msg = ""
                Msg &= "<div class='modal_titlebar'>Locate Organisation</div><div class='modal_exitbar'><a href=""javascript:showHide('orgDisplay');"">X</a></div>"
                Msg &= "   <div class='modal_panel' id='orgDisplayInner' style='max-height:300px;overflow:auto;'>"
                Msg &= "   <ul class='formcontrol'>"
                Dim CO As New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "[rsp_organisation_locate]"
                CO.Parameters.AddWithValue("@org", "%" & name & "%")
                CO.Parameters.AddWithValue("@pcode", "%" & pcode & "%")
                Dim dt As DataTable = CDC.ReadDataTable(CO)
                
                Dim gv As New Repeater
                gv.EnableViewState = False
                gv.ItemTemplate = New ListItemTemplate()
                gv.DataSource = dt
                gv.DataBind()
                Dim stringWrite As New System.IO.StringWriter
                Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
                gv.RenderControl(htmlWrite)
                Msg &= stringWrite.ToString()
                
                if dt.Rows.Count = 0 then
                    Msg &= "No matching organisations were found."
                end if
                msg &= "<br>"               

                Msg &= "To add a new Organisation please do this in Contract Manager."
                                
                Msg &= "   </ul>"
                Msg &= "   </div>"
                Msg &= "</div>"
            Else
                Msg = ""
                Msg &= "<div class='modal_titlebar'>Create Organisation</div><div class='modal_exitbar'><a href=""javascript:showHide('orgDisplay');"">X</a></div>"
                Msg &= "   <div class='modal_panel' id='orgDisplayInner' style='height:350px;'>"

                Msg &= "   <iframe class='modal_iframe'  style='height:99%; width:99%;' src='/functions/organisations.aspx?n=" & name & "&p=" & pcode & "'></iframe>"

                Msg &= "   </div>"
                Msg &= "</div>"
            End If
        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        R.Write(Msg)
        
    End Sub
    
    Public Class ListItemTemplate
        Implements ITemplate
        Public Sub New()
        End Sub

        Private Sub ITemplate_InstantiateIn(ByVal container As System.Web.UI.Control) Implements ITemplate.InstantiateIn
            Dim lit As New Literal
            AddHandler lit.DataBinding, AddressOf lbl_DataBinding
            container.Controls.Add(lit)
        End Sub

        Private Sub lbl_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim lit As Literal = DirectCast(sender, Literal)
            Dim data As Object = DirectCast(lit.NamingContainer, RepeaterItem).DataItem
            Dim dbr As DataRowView = DirectCast(data, DataRowView)

            Dim s As String = ""
        
            s &= "<li><a href='javascript:setOrganisation("
            s &= dbr.Item("organisation_pk")
            s &= ","""
            s &= dbr.Item("organisation")
            s &= """)'  title="""
            s &= dbr.Item("shortname")
            s &= """>"
            s &= dbr.Item("organisation")
            s &= "</a>"
            s &= "<br />"
            s &= dbr.Item("location")
            s &= "</li>"
        
            lit.Text = s
        

        End Sub


   

    End Class

End Class