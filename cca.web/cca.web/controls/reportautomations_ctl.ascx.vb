Partial Class reportautomations_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents oreportautomations As Objects.reportautomations
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_reports_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.report_fk.Items.Count=0 Then
        Me.report_fk.DataSource=CDC.ReadDataTable(CO)
        Me.report_fk.DataTextField = "value"
        Me.report_fk.DataValueField = "pk"
        Try
            Me.report_fk.DataBind
        Catch Ex as Exception
            Me.report_fk.SelectedValue=-1
            Me.report_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.user_fk.Items.Count=0 Then
        Me.user_fk.DataSource=CDC.ReadDataTable(CO)
        Me.user_fk.DataTextField = "value"
        Me.user_fk.DataValueField = "pk"
        Try
            Me.user_fk.DataBind
        Catch Ex as Exception
            Me.user_fk.SelectedValue=-1
            Me.user_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_emails_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.email_fk.Items.Count=0 Then
        Me.email_fk.DataSource=CDC.ReadDataTable(CO)
        Me.email_fk.DataTextField = "value"
        Me.email_fk.DataValueField = "pk"
        Try
            Me.email_fk.DataBind
        Catch Ex as Exception
            Me.email_fk.SelectedValue=-1
            Me.email_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_frequencys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.frequency_fk.Items.Count=0 Then
        Me.frequency_fk.DataSource=CDC.ReadDataTable(CO)
        Me.frequency_fk.DataTextField = "value"
        Me.frequency_fk.DataValueField = "pk"
        Try
            Me.frequency_fk.DataBind
        Catch Ex as Exception
            Me.frequency_fk.SelectedValue=-1
            Me.frequency_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_reportformats_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.reportformat_fk.Items.Count=0 Then
        Me.reportformat_fk.DataSource=CDC.ReadDataTable(CO)
        Me.reportformat_fk.DataTextField = "value"
        Me.reportformat_fk.DataValueField = "pk"
        Try
            Me.reportformat_fk.DataBind
        Catch Ex as Exception
            Me.reportformat_fk.SelectedValue=-1
            Me.reportformat_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If oreportautomations is Nothing then
oreportautomations = new Objects.reportautomations(currentuser,actualuser,pk,CDC)
End If
Me.report_fk.SelectedValue=oreportautomations.report_fk
Me.user_fk.SelectedValue=oreportautomations.user_fk
Me.email_fk.SelectedValue=oreportautomations.email_fk
Me.frequency_fk.SelectedValue=oreportautomations.frequency_fk
Me.startdate.Text=oreportautomations.startdate.ToString("dd MMM yyyy")
If Me.startdate.Text="01 Jan 0001" then Me.startdate.Text=""
Me.startdate.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.startdate.Attributes.Add("onBlur", "clearNotice()")
Me.reportformat_fk.SelectedValue=oreportautomations.reportformat_fk
_CurrentPk=oreportautomations.reportautomation_pk
Status=oreportautomations.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If oreportautomations is Nothing then
oreportautomations = new Objects.reportautomations(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
oreportautomations.report_fk=Me.report_fk.SelectedValue
oreportautomations.user_fk=Me.user_fk.SelectedValue
oreportautomations.email_fk=Me.email_fk.SelectedValue
oreportautomations.frequency_fk=Me.frequency_fk.SelectedValue
oreportautomations.startdate=CommonFN.CheckEmptyDate(Me.startdate.Text)
oreportautomations.reportformat_fk=Me.reportformat_fk.SelectedValue
result=oreportautomations.Save()
if not result then throw oreportautomations.LastError
_CurrentPk=oreportautomations.reportautomation_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=oreportautomations.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportautomations is Nothing then
   oreportautomations = new Objects.reportautomations(currentuser,actualuser,pk,CDC)
  End If
  oreportautomations.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportautomations is Nothing then
   oreportautomations = new Objects.reportautomations(currentuser,actualuser,pk,CDC)
  End If
  oreportautomations.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If oreportautomations is Nothing then
   oreportautomations = new Objects.reportautomations(currentuser,pk,CDC)
  End If
  oreportautomations.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.report_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkreport_fk.Visible=IIF(RL>=0,True,False)
  Me.user_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkuser_fk.Visible=IIF(RL>=0,True,False)
  Me.email_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkemail_fk.Visible=IIF(RL>=0,True,False)
  Me.frequency_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkfrequency_fk.Visible=IIF(RL>=0,True,False)
  Me.startdate.Enabled=IIF(WL>=0,True,False)
  Me.blkstartdate.Visible=IIF(RL>=0,True,False)
  Me.reportformat_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkreportformat_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

