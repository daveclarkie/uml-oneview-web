
Partial Class controls_nav_rightnav
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Dim _Messages As String = ""
    Public Property Messages() As String
        Get
            Return _Messages
        End Get
        Set(ByVal value As String)
            If value = "__clear__" Then
                _Messages = ""
            Else
                _Messages &= value
                _Messages &= vbCrLf
            End If
        End Set
    End Property




End Class
