<%@ Control Language="VB" AutoEventWireup="false" CodeFile="reports_ctl.ascx.vb" Inherits="reports_ctl" %>
<h5>reports</h5>
<ul class='formcontrol'>
<li runat="server" id="blkreportname">
<span class='label'>Report Name</span>
<asp:TextBox EnableViewState="false" ID="reportname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkreporttype_fk">
<span class='label'>Report Type</span>
<asp:DropDownList ID="reporttype_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkreportserverurl">
<span class='label'>Report Server URL</span>
<asp:TextBox EnableViewState="false" ID="reportserverurl" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkreportpath">
<span class='label'>Report Path on Server</span>
<asp:TextBox EnableViewState="false" ID="reportpath" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
</ul>

