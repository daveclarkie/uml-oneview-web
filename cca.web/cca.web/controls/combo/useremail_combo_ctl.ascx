<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="useremail_combo_ctl.ascx.vb" Inherits="useremail_combo_ctl" %>
<h5>Detail</h5>
<ul class='formcontrol'>
<li runat="server" id="blkemail">
<span class='label'>Email Address</span>
<asp:TextBox EnableViewState="false" ID="email" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkcontactlocation_fk">
<span class='label'>Contact Location</span>
<asp:DropDownList ID="contactlocation_fk" runat="server" cssClass="input_ddl" />
</li>
</ul>
