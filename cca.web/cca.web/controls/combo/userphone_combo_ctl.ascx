<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="userphone_combo_ctl.ascx.vb" Inherits="userphone_combo_ctl" %>
<h5>Detail</h5>
<ul class='formcontrol'>
<li runat="server" id="blkcontactlocation_fk">
<span class='label'>Contact Location</span>
<asp:DropDownList ID="contactlocation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkcountry_fk">
<span class='label'>Country</span>
<asp:DropDownList ID="country_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkphonenumber">
<span class='label'>Phone Number</span>
<asp:TextBox EnableViewState="false" ID="phonenumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>
