﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/external.master" AutoEventWireup="false" CodeFile="ticket.aspx.vb" Inherits="corehelpdesk_ticket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">

<h3>Helpdesk</h3> 
<br />
<asp:Repeater ID="rpthelpdeskinfo" runat="server" >
    <HeaderTemplate><table>
        <tr>
            <td style="width:75px;">
                <h4>Column</h4>
            </td>
            <td style="width:400px;">
                <h4>Value</h4>
            </td>
            <td style="width:100px;">
               
            </td>
        </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("column")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("value")%>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyDataInfo" Text="No Tickets Available" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>
                   
<h5>Attachments</h5>

<asp:Repeater ID="rptHelpdeskAttachments" OnItemCommand="rptHelpdeskAttachments_ItemCommand" runat="server" onitemdatabound="rptHelpdeskAttachments_ItemDataBound" >
    <HeaderTemplate><table>
        <tr>
            <td style="width:75px;">
                <h4>ID</h4>
            </td>
            <td style="width:400px;">
                <h4>Filename</h4>
            </td>
            <td style="width:100px;">
               
            </td>
        </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("attachmentid")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("attachmentname")%>
            </td>
           <td style="text-align:center;">
                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("attachmentname") + ";" + Eval("path")%>' CommandName="download"><h4>Download</h4></asp:LinkButton>
            </td>
            
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyDataAttachments" Text="There were no attachments in the original ticket" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>



<h5> Conversation History</h5>
<asp:Repeater ID="rptHelpdeskConversations" runat="server"  onitemdatabound="rptHelpdeskConversations_ItemDataBound">
    <HeaderTemplate>
            <table>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr style="border-top:10px solid #009530;">
            <td style="width:75px;">
                <h4>ID</h4>
            </td>
            <td style="width:100px;">
                <h4>Requester</h4>
            </td>
            <td style="width:100px;">
                <h4>Created</h4>
            </td>
            <td style="width:200px;">
                <h4>Type</h4>
            </td>
        </tr>
        <tr>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("conversationid")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("requester")%>
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("created on")%>...
            </td>
            <td style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("type")%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h4>Description</h4>
            </td>
        </tr>
        <tr>
            <td  colspan="4" style="text-indent:5px;height:20px; vertical-align:middle;">
                <%# Container.DataItem("description")%>
            </td>
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblEmptyDataConversations" Text="There were no conversations in the original ticket" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>





</asp:Content>

