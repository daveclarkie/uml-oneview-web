<%@ WebHandler Language="VB" Class="function_userstructureadd" %>
Public Class function_userstructureadd
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon(Local.AuthURL)
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim Msg As String = ""
        Try

            Dim u As Integer
            Dim mu As Integer

            Dim rf As Object = Request.QueryString
            Dim us As New userstructures(SM.currentuser, SM.actualuser, CDC)


            If Integer.TryParse(rf("u"), u) And Integer.TryParse(rf("mu"), mu) Then

                Dim secure As Boolean = True

                ' Allowed to manage groups
                secure = secure And Security.GroupMember(SM.currentuser, SystemGroups.ManageUsers, CDC)


                If secure Then
                    us.user_fk = u
                    us.manageruser_fk = mu

                    If us.Save() Then
                        Response.Write("OK:" & us.userstructure_pk)
                    Else
                        Response.Write("Fail:" & us.LastError.ToString)
                    End If

                Else
                    Response.Write("Fail: Access Denied")
                End If

            End If

        Catch LE As Exception
            Msg = "FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
        
    End Sub

End Class