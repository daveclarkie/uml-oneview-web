
Partial Class backend_manageusers
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here

        If Not Security.GroupMember(SM.currentuser, SystemGroups.ManageUsers, CDC) Then
            Local.Bounce(SM, Response, "~/default.aspx", "Access+Denied")
        End If

        Me.managegroupssection.Visible = Security.GroupMember(SM.currentuser, SystemGroups.ManageGroups, CDC)

    End Sub

End Class
