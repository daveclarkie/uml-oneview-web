
Partial Class functions_grpasg
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand
    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Try

            Dim rf As Object = Request.QueryString
            Dim CO As New SqlClient.SqlCommand
            CO.CommandText = "[rsp_groupsassigned]"
            CO.CommandType = CommandType.StoredProcedure
            'CO.Parameters.AddWithValue("@user", SM.CurrentUser)
            If rf("u") = "" Then
                CO.Parameters.AddWithValue("@user", 0)
            Else
                CO.Parameters.AddWithValue("@user", rf("u"))
            End If
            Dim xml As String = ""
            If CDC.ReadScalarValue(xml, CO) Then
                Response.Write("<results>")
                Response.Write(xml)
                Response.Write("</results>")
            Else
                Response.Write("<nothing />")
            End If

        Catch Ex As Exception
            Response.Clear()
            Response.Write("<error>" & Ex.ToString & "</error>")
        End Try

        Try
            Response.End()
        Catch ex As Exception
        End Try


    End Sub
End Class
