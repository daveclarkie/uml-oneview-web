Partial Class bubbleagreements_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents obubbleagreements As Objects.bubbleagreements
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_bubbles_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.bubble_fk.Items.Count = 0 Then
            Me.bubble_fk.DataSource = CDC.ReadDataTable(CO)
            Me.bubble_fk.DataTextField = "value"
            Me.bubble_fk.DataValueField = "pk"
            Try
                Me.bubble_fk.DataBind()
                Me.bubble_fk.SelectedValue = SM.targetBubble
            Catch Ex As Exception
                Me.bubble_fk.SelectedValue = -1
                Me.bubble_fk.DataBind()
            End Try
        End If

        Dim b As New bubbles(SM.currentuser, SM.actualuser, SM.targetBubble, CDC)
        CO = New SqlClient.SqlCommand("rsp_bubbleagreements_lookup_organisation")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@bubble_fk", SM.targetBubble)
        CO.Parameters.AddWithValue("@organisation_fk", b.organisation_fk)

        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If obubbleagreements Is Nothing Then
            obubbleagreements = New Objects.bubbleagreements(currentuser, actualuser, pk, CDC)
        End If
        Me.bubble_fk.SelectedValue = obubbleagreements.bubble_fk
        Me.agreement_fk.SelectedValue = obubbleagreements.agreement_fk
        _CurrentPk = obubbleagreements.bubbleagreement_pk
        Status = obubbleagreements.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If obubbleagreements Is Nothing Then
            obubbleagreements = New Objects.bubbleagreements(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            obubbleagreements.bubble_fk = Me.bubble_fk.SelectedValue
            obubbleagreements.agreement_fk = Me.agreement_fk.SelectedValue
            result = obubbleagreements.Save()
            If Not result Then Throw obubbleagreements.LastError
            _CurrentPk = obubbleagreements.bubbleagreement_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = obubbleagreements.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If obubbleagreements Is Nothing Then
            obubbleagreements = New Objects.bubbleagreements(currentuser, actualuser, pk, CDC)
        End If
        obubbleagreements.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If obubbleagreements Is Nothing Then
            obubbleagreements = New Objects.bubbleagreements(currentuser, actualuser, pk, CDC)
        End If
        obubbleagreements.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If obubbleagreements Is Nothing Then
            obubbleagreements = New Objects.bubbleagreements(currentuser, pk, CDC)
        End If
        obubbleagreements.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.bubble_fk.Enabled = IIf(WL >= 99, True, False)
        Me.blkbubble_fk.Visible = IIf(RL >= 99, True, False)
        Me.agreement_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreement_fk.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

