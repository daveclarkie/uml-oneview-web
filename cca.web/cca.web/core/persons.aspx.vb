Imports System.Configuration

Partial Class core_persons
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        validpk = Integer.TryParse(Request.QueryString("pk"), pk)
        validspk = Integer.TryParse(Request.QueryString("spk"), spk)

        If Not validpk Or pk = 0 Then
            ' we shouldn't be here
            Local.Bounce(SM, Response, "~/core/search.aspx", "no+valid+client+id")
        End If

        ' ===============================================
        '  New Target User?
        '        - Yes - Set Target User for session
        '        - No  - No action required  
        ' ===============================================

        If Not (SM.targetuser = pk) Then
            SM.targetuser = pk
            SM.mainid = SM.targetuser
        End If

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        ' ===============================================
        '  Which Tab?
        ' ===============================================
        Try

            Select Case ModeCode
                Case "ps" : CurrentDisplay = PersonPageMode.Person
                    vwSub = vwPerson
                Case "lc" : CurrentDisplay = PersonPageMode.Location
                    vwSub = vwUserLocation
                Case "ph" : CurrentDisplay = PersonPageMode.Phone
                    vwSub = vwUserPhone
                Case "em" : CurrentDisplay = PersonPageMode.Email
                    vwSub = vwUserEmail
                Case "hd" : CurrentDisplay = PersonPageMode.Helpdesk
                    vwSub = vwHelpdesk
                Case "hy" : CurrentDisplay = PersonPageMode.Hierarchy
                    vwSub = vwHierarchy
                Case Else
                    ' we shouldn't be here
                    Local.Bounce(SM, Response, "~/core/search.aspx", "invalid+display+mode")
            End Select

            If SM.targetuser = -1 And Not CurrentDisplay = PersonPageMode.Person Then
                Local.Bounce(SM, Response, "~/core/search.aspx", "No+person")
            End If

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/core/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property

    Dim cc As Object
    Dim maintab As String = "search"
    Dim due As DateTime
    Dim diff As Long

    Dim validpk As Boolean = False
    Dim validspk As Boolean = False

    Dim CurrentAction As Local.Action = Local.Action.None
    Dim CurrentDisplay As PersonPageMode = PersonPageMode.Person

    Dim vwSub As System.Web.UI.WebControls.View = vwPerson

    Dim pk As Integer = 0
    Dim spk As Integer = 0
    Dim md As Integer = 0

    Public Mode As PersonPageMode = PersonPageMode.Person

    Private Sub SetMode()
        If Page.IsPostBack Then
            Mode = SM.PersonPageMode
        Else
            SM.targetuser = Request.QueryString("pk")
            Mode = PersonPageMode.Search
            Select Case Request.QueryString("pg")
                Case "ps" : Mode = PersonPageMode.Person
                Case "lc" : Mode = PersonPageMode.Location
                Case "ph" : Mode = PersonPageMode.Phone
                Case "em" : Mode = PersonPageMode.Email
                Case "hd" : Mode = PersonPageMode.Helpdesk
                Case "hy" : Mode = PersonPageMode.Hierarchy
                Case Else : Mode = PersonPageMode.Search
            End Select
            SM.PersonPageMode = Mode
        End If
    End Sub

    Private Sub PageLoad()
        Select Case SM.PersonPageMode
            Case PersonPageMode.Person : InitPerson(Page.IsPostBack)
            Case PersonPageMode.Location : InitUserLocations(Page.IsPostBack)
            Case PersonPageMode.Phone : InitUserPhones(Page.IsPostBack)
            Case PersonPageMode.Email : InitUserEmails(Page.IsPostBack)
            Case PersonPageMode.Helpdesk : InitUserhelpdesk(Page.IsPostBack)
            Case PersonPageMode.Hierarchy : InitUserhierarchy(Page.IsPostBack)
            Case Else
                Response.Redirect("search.aspx")
        End Select
    End Sub

    Private Sub DisplayPage()

        '' ===============================================
        ''  Which Action?
        '' ===============================================

        If validspk Then
            CurrentAction = Local.Action.Read
        Else
            CurrentAction = Local.Action.None
        End If

    End Sub

    Private Sub RefreshList()

        If Mode = PersonPageMode.Location Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_userlocations"
            CO.Parameters.AddWithValue("@user_pk", SM.targetuser)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptUserLocation.DataSource = dt
            rptUserLocation.DataBind()
            CO.Dispose()

        ElseIf Mode = PersonPageMode.Phone Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_userphones"
            CO.Parameters.AddWithValue("@user_pk", SM.targetuser)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptUserPhone.DataSource = dt
            rptUserPhone.DataBind()
            CO.Dispose()

        ElseIf Mode = PersonPageMode.Email Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_useremails"
            CO.Parameters.AddWithValue("@user_pk", SM.targetuser)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptUserEmail.DataSource = dt
            rptUserEmail.DataBind()
            CO.Dispose()

        ElseIf Mode = PersonPageMode.Hierarchy Then


        End If
    End Sub

    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control, ByVal isPerson As Boolean)
        TargetControl.Controls.Clear()
        If isPerson Then
            cc = LoadControl(controlpath)
            cc.CDC = CDC
            cc.SM = SM

            CO = New SqlCommand("rsp_personfk_fromuserfk")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@user", SM.targetuser)
            Dim person_pk As Integer = -1
            CDC.ReadScalarValue(person_pk, CO)

            cc.Read(SM.targetuser, SM.actualuser, person_pk)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        ElseIf controlpath.EndsWith("helpdesklogindetails_ctl.ascx") Then
            cc = LoadControl(controlpath)
            cc.CDC = CDC
            cc.SM = SM

            CO = New SqlCommand("rsp_helpdesklogindetails_userfk")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("user_fk", SM.targetuser)
            Dim helpdesklogindetail_pk As Integer = -1
            CDC.ReadScalarValue(helpdesklogindetail_pk, CO)

            cc.Read(SM.currentuser, SM.actualuser, helpdesklogindetail_pk)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        Else
            If Not Request.QueryString("spk") Is Nothing Then
                cc = LoadControl(controlpath)
                cc.CDC = CDC
                cc.SM = SM
                cc.Read(SM.currentuser, SM.actualuser, spk)
                TargetControl.Controls.Add(cc)
                TargetButton.Visible = True
                RefreshList()
            Else
                TargetButton.Visible = False
                RefreshList()
            End If
        End If
    End Sub
    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Boolean = False
        Try
            If primary = True Then
                resDet = cc.Save(SM.currentuser, SM.actualuser, SM.targetuser)
            Else
                resDet = cc.Save(SM.currentuser, SM.actualuser, spk)
            End If

            If Not (resDet) Then
                Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
                If errMsg.Length > 1 Then
                    AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
                Else
                    AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
                End If
            Else
                AddNotice("setNotice('Record Saved');")
                If Not Request.QueryString("spk") Is Nothing Then
                    CancelContol()
                End If
                Dim filtered As New NameValueCollection(Request.QueryString)
                Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
                nameValueCollection.Remove("spk")
                Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString + "&r=Record+Saved"
                Response.Redirect(url)
                'RefreshList()
                PageLoad()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("spk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub

    Private Sub InitPerson(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwPerson)
        SetControl("~/controls/persons_ctl.ascx", personControl, personButton, True)
    End Sub
    Private Sub InitUserLocations(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwUserLocation)
        SetControl("~/controls/combo/userlocation_combo_ctl.ascx", UserLocationControl, UserLocationButton, False)
    End Sub
    Private Sub InitUserPhones(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwUserPhone)
        SetControl("~/controls/combo/userphone_combo_ctl.ascx", UserPhoneControl, UserPhoneButton, False)
    End Sub
    Private Sub InitUserEmails(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwUserEmail)
        SetControl("~/controls/combo/useremail_combo_ctl.ascx", UserEmailControl, UserEmailButton, False)
    End Sub
    Private Sub InitUserhelpdesk(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwHelpdesk)
        SetControl("~/controls/helpdesklogindetails_ctl.ascx", helpdeskControl, helpdeskButton, False)
    End Sub
    Private Sub InitUserhierarchy(ByVal postback As Boolean)
        mvPerson.SetActiveView(vwHierarchy)
        RefreshList()
    End Sub

    Protected Sub btnSavePerson_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePerson.Click
        SaveContol(True)
    End Sub
    Protected Sub btnSaveUserLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUserLocation.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveUserPhone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUserPhone.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveUserEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUserEmail.Click
        SaveContol(False)
    End Sub
    Protected Sub btnSaveHelpdesk_Click(sender As Object, e As System.EventArgs) Handles btnSaveHelpdesk.Click
        SaveContol(True)
    End Sub

    Protected Sub btnCancelUserLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUserLocation.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelUserPhone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUserPhone.Click
        CancelContol()
    End Sub
    Protected Sub btnCancelUserEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUserEmail.Click
        CancelContol()
    End Sub

    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""
        Select Case Mode
            Case PersonPageMode.Person : itemclass = IIf(item = "person", "selected", "")
            Case PersonPageMode.Location : itemclass = IIf(item = "location", "selected", "")
            Case PersonPageMode.Phone : itemclass = IIf(item = "phone", "selected", "")
            Case PersonPageMode.Email : itemclass = IIf(item = "email", "selected", "")
            Case PersonPageMode.Helpdesk : itemclass = IIf(item = "helpdesk", "selected", "")
            Case PersonPageMode.Hierarchy : itemclass = IIf(item = "hierarchy", "selected", "")
        End Select
        Return itemclass
    End Function

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMode()
        PageLoad()
        PopulateRootLevel()

    End Sub


    Private Sub PopulateRootLevel()
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_users_heirarchy_childnodecount"
        CO.Parameters.AddWithValue("@manageruser_pk", SM.targetuser)
        Dim dtTree As DataTable = CDC.ReadDataTable(CO, 0)

        PopulateNodes(dtTree, hierarchyTree.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("username").ToString()
            tn.Value = dr("user_pk").ToString()
            nodes.Add(tn)

            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As Integer, ByVal parentNode As TreeNode)
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_users_heirarchysub_childnodecount"
        CO.Parameters.AddWithValue("@user_pk", parentid)
        Dim dtTree As DataTable = CDC.ReadDataTable(CO, 0)
        PopulateNodes(dtTree, parentNode.ChildNodes)
    End Sub

    Protected Sub hierarchyTree_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles hierarchyTree.TreeNodePopulate
        PopulateSubLevel(CInt(e.Node.Value), e.Node)
    End Sub


End Class