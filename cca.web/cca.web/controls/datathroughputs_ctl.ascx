<%@ Control Language="VB" AutoEventWireup="false" CodeFile="datathroughputs_ctl.ascx.vb" Inherits="datathroughputs_ctl" %>
<h5>datathroughputs</h5>
<ul class='formcontrol'>
<li runat="server" id="blkthroughput_fk">
<span class='label'>Throughput</span>
<asp:DropDownList ID="throughput_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkmonth_fk">
<span class='label'>month_fk</span>
<asp:DropDownList ID="month_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkyear_fk">
<span class='label'>Year</span>
<asp:DropDownList ID="year_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blktotalthroughput">
<span class='label'>Total Throughput<span class='pop'>This is the total throughput for the period</span></span>
<asp:TextBox EnableViewState="false" ID="totalthroughput" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkeligiblethroughput">
<span class='label'>Eligible Throughput<span class='pop'>This is the total throughput for the period</span></span>
<asp:TextBox EnableViewState="false" ID="eligiblethroughput" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkdataimportmethod_fk">
<span class='label'>dataimportmethod_fk</span>
<asp:DropDownList ID="dataimportmethod_fk" runat="server" cssClass="input_ddl" />
</li>


<li runat="server" id="Li1">
<span class='label'>The data was imported via </span>
<asp:label ID="importedmethod" runat="server" cssClass="input_str" />
</li>

<li runat="server" id="Li2">
<span class='label'>Created</span>
<asp:label ID="created" runat="server" cssClass="input_str" />
</li>

<li runat="server" id="Li3">
<span class='label'>Last Modified</span>
<asp:label ID="modified" runat="server" cssClass="input_str" />
</li>

</ul>

