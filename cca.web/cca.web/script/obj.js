﻿/* ============================================================= */
/*                   Homemade JS objects                         */
/* ============================================================= */

/* ============================================================= */
/*                   Top Down Element Resize                     */
/* ============================================================= */
/* Main Functions                                                */
/*  chgNamedElementText (id,chgMode[,content])                   */
/*  chgElementText (object,chgMode[,content])                    */
/* ============================================================= */

var refreshRate=25;

function TDeR(elem)
{
    this.target=document.getElementById(elem);
    this.minSize=0;
    this.maxSize=18;
    this.curSize=0;
    this.intervalId;
    this.mode=0;
    
    this.doShrink = function ()
    {
        this.mode=0;
        if (this.curSize<=this.minSize){
            this.target.style.height=this.minSize+"px";
            this.curSize=this.minSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="none";
            
        } else {
            try {
                this.target.style.height=this.curSize+"px";
                }
            catch(e){
                }            
            this.curSize--;
        }
    }

    this.doExpand = function ()
    {
        this.mode=1;
        this.target.style.display="block";
        if (this.curSize>=this.maxSize){
            this.target.style.height=this.maxSize+"px";
            this.curSize=this.maxSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
        } else {
            this.curSize++;
            this.target.style.height=this.curSize+"px";
        }
    }
    
    this.instant = function()
    {
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="block";
            switch (this.mode){
                case 0: //shrinking
                    this.target.style.height=this.minSize+"px";
                    this.curSize=this.minSize;
                    this.target.style.display="none";
                    break;   
                case 1: //expanding
                    this.target.style.height=this.maxSize+"px";
                    this.curSize=this.maxSize;
                    break;   
            }
    
    }
    
}

function popUpResize(targetId,height)
{
    this.target=document.getElementById(targetId);
    this.minSize=0;
    this.maxSize=height;
    this.curSize=0;
    this.intervalId;
    this.mode=0;
    
    this.doShrink = function ()
    {
        this.mode=0;
        if (this.curSize<=this.minSize){
            this.target.style.height=this.minSize+"px";
            this.curSize=this.minSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="none";
            
        } else {
            try {
                this.target.style.height=this.curSize+"px";
                }
            catch(e){
                }            
            this.curSize--;
        }
    }

    this.doExpand = function ()
    {
        this.mode=1;
        this.target.style.display="block";
        if (this.curSize>=this.maxSize){
            this.target.style.height=this.maxSize+"px";
            this.curSize=this.maxSize;
            clearInterval(this.intervalId);
            this.intervalId=null;
        } else {
            this.curSize++;
            this.target.style.height=this.curSize+"px";
        }
    }
    
    this.doInstant = function()
    {
            clearInterval(this.intervalId);
            this.intervalId=null;
            this.target.style.display="block";
            switch (this.mode){
                case 0: //shrinking
                    this.target.style.height=this.minSize+"px";
                    this.curSize=this.minSize;
                    this.target.style.display="none";
                    break;   
                case 1: //expanding
                    this.target.style.height=this.maxSize+"px";
                    this.curSize=this.maxSize;
                    break;   
            }
    
    }
    
}