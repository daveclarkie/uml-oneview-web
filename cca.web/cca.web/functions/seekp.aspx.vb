
Partial Class functions_seekp
    Inherits DavePage : Public PSM As MySM = SM
    Dim CO As SqlCommand
    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        Try

            Dim rf As Object = Request.Form
            Dim CO As New SqlClient.SqlCommand
            CO.CommandText = "[rsp_seekp]"
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@me", SM.currentuser)
            If rf("pk") = "" Then
                CO.Parameters.AddWithValue("@pk", 0)
            Else
                CO.Parameters.AddWithValue("@pk", rf("pk"))
            End If
            CO.Parameters.AddWithValue("@fn", rf("fn"))
            CO.Parameters.AddWithValue("@sn", rf("sn"))
            CO.Parameters.AddWithValue("@loc", rf("loc"))
            CO.Parameters.AddWithValue("@email", rf("email"))
            Dim xml As String = ""
            If CDC.ReadScalarValue(xml, CO) Then
                Response.Write("<results>")
                Response.Write(xml)
                Response.Write("</results>")
            Else
                Response.Write("<no-one />")
            End If

        Catch Ex As Exception
            Response.Clear()
            Response.Write("<error>" & Ex.ToString & "</error>")
        End Try

        Try
            Response.End()
        Catch ex As Exception
        End Try


    End Sub
End Class
