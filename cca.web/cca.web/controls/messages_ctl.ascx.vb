Partial Class messages_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents omessages As Objects.messages
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_users_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.targetuser_fk.Items.Count=0 Then
        Me.targetuser_fk.DataSource=CDC.ReadDataTable(CO)
        Me.targetuser_fk.DataTextField = "value"
        Me.targetuser_fk.DataValueField = "pk"
        Try
            Me.targetuser_fk.DataBind
        Catch Ex as Exception
            Me.targetuser_fk.SelectedValue=-1
            Me.targetuser_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_messagetypes_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.messagetype_fk.Items.Count=0 Then
        Me.messagetype_fk.DataSource=CDC.ReadDataTable(CO)
        Me.messagetype_fk.DataTextField = "value"
        Me.messagetype_fk.DataValueField = "pk"
        Try
            Me.messagetype_fk.DataBind
        Catch Ex as Exception
            Me.messagetype_fk.SelectedValue=-1
            Me.messagetype_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If omessages is Nothing then
omessages = new Objects.messages(currentuser,actualuser,pk,CDC)
End If
Me.message.Text=omessages.message
Me.targetuser_fk.SelectedValue=omessages.targetuser_fk
Me.messagetype_fk.SelectedValue=omessages.messagetype_fk
_CurrentPk=omessages.message_pk
Status=omessages.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If omessages is Nothing then
omessages = new Objects.messages(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
omessages.message=Me.message.Text
omessages.targetuser_fk=Me.targetuser_fk.SelectedValue
omessages.messagetype_fk=Me.messagetype_fk.SelectedValue
result=omessages.Save()
if not result then throw omessages.LastError
_CurrentPk=omessages.message_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=omessages.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omessages is Nothing then
   omessages = new Objects.messages(currentuser,actualuser,pk,CDC)
  End If
  omessages.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omessages is Nothing then
   omessages = new Objects.messages(currentuser,actualuser,pk,CDC)
  End If
  omessages.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If omessages is Nothing then
   omessages = new Objects.messages(currentuser,pk,CDC)
  End If
  omessages.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.message.Enabled=IIF(WL>=0,True,False)
  Me.blkmessage.Visible=IIF(RL>=0,True,False)
  Me.targetuser_fk.Enabled=IIF(WL>=0,True,False)
  Me.blktargetuser_fk.Visible=IIF(RL>=0,True,False)
  Me.messagetype_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkmessagetype_fk.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

