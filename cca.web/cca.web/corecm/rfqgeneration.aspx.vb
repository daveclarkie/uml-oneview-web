Partial Class corecm_rfqgeneration
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        CheckSessionValues()

        ' ===============================================
        '  Which Tab?
        ' ===============================================
        Try

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/corecm/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Private Sub CheckSessionValues()
        'load agreement
        Dim a As New agreements(SM.currentuser, SM.actualuser, SM.mainid, CDC)
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property

    Dim cc As Object

    Public Mode As CustomerPageMode = CustomerPageMode.Search

    Public Function TabSummary(ByVal item As String) As String
        Dim output As String = ""
        Select Case item
            'Case "sites" : output = sitecount
            'Case "meters" : output = 1
        End Select
        Return "(" & output & ")"
    End Function

    Private Sub PageLoad()
        If Not Page.IsPostBack Then
            ResetCustomers()
        End If
    End Sub
    Private Sub DisplayPage()

    End Sub

    Private Sub ResetCustomers()
        pnlsearchcustomer.Visible = True
        pnlselectcustomer.Visible = False

        txtSearch.Text = Nothing
        txtSearch.Focus()
        lblCustomerName.Text = Nothing
        lblCustomerID.Text = Nothing

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rkg_organisations_lookup"
        Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
        ddlCustomer.DataSource = dt
        ddlCustomer.DataTextField = "value"
        Me.ddlCustomer.DataValueField = "pk"
        ddlCustomer.DataBind()
        CO.Dispose()

    End Sub
    Private Sub ResetSites()

        If lblCustomerID.Text = "" Then
            rptSiteList.DataSource = Nothing
            rptSiteList.DataBind()
        Else
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_sites_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", ddlCustomer.SelectedItem.Value)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptSiteList.DataSource = dt
            rptSiteList.DataBind()
            CO.Dispose()
        End If

    End Sub
    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""

        Return itemclass
    End Function

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PageLoad()
    End Sub
    Protected Sub ddlCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCustomer.SelectedIndexChanged
        SM.mainid = ddlCustomer.SelectedValue
        SM.Save()

        If SM.mainid = -1 Then
            lblCustomerName.Text = "invalid"
            lblCustomerID.Text = -1
            pnlsearchcustomer.Visible = True
            pnlselectcustomer.Visible = False
        Else
            lblCustomerName.Text = ddlCustomer.SelectedItem.Text.ToString
            lblCustomerID.Text = ddlCustomer.SelectedItem.Value.ToString
            pnlsearchcustomer.Visible = False
            pnlselectcustomer.Visible = True
            ResetSites()

            'CO = New SqlCommand
            'CO.CommandType = CommandType.StoredProcedure
            'CO.CommandText = "rsp_sites_lookup_organisationfk"
            'CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            'Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            'rptSites.DataSource = dt
            'rptSites.DataBind()
            'CO.Dispose()
        End If
    End Sub

    Protected Sub lnkchangecustomer_Click(sender As Object, e As System.EventArgs) Handles lnkchangecustomer.Click
        ResetCustomers()
        ResetSites()
    End Sub
End Class
