Partial Class helpdeskitem2details_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents ohelpdeskitem2details As Objects.helpdeskitem2details
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.requestid.Text="0"
    CO=new SqlClient.SqlCommand("rkg_countrys_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.country_fk.Items.Count=0 Then
        Me.country_fk.DataSource=CDC.ReadDataTable(CO)
        Me.country_fk.DataTextField = "value"
        Me.country_fk.DataValueField = "pk"
        Try
            Me.country_fk.DataBind
        Catch Ex as Exception
            Me.country_fk.SelectedValue=-1
            Me.country_fk.DataBind
        End Try
    End If
    CO=new SqlClient.SqlCommand("rkg_heldpesk_servicelevels_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.heldpesk_servicelevel_fk.Items.Count=0 Then
        Me.heldpesk_servicelevel_fk.DataSource=CDC.ReadDataTable(CO)
        Me.heldpesk_servicelevel_fk.DataTextField = "value"
        Me.heldpesk_servicelevel_fk.DataValueField = "pk"
        Try
            Me.heldpesk_servicelevel_fk.DataBind
        Catch Ex as Exception
            Me.heldpesk_servicelevel_fk.SelectedValue=-1
            Me.heldpesk_servicelevel_fk.DataBind
        End Try
    End If
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If ohelpdeskitem2details is Nothing then
ohelpdeskitem2details = new Objects.helpdeskitem2details(currentuser,actualuser,pk,CDC)
End If
Me.requestid.Text=ohelpdeskitem2details.requestid
Me.requestid.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.requestid.Attributes.Add("onBlur", "clearNotice()")
Me.country_fk.SelectedValue=ohelpdeskitem2details.country_fk
Me.heldpesk_servicelevel_fk.SelectedValue=ohelpdeskitem2details.heldpesk_servicelevel_fk
_CurrentPk=ohelpdeskitem2details.helpdeskitemdetail_pk
Status=ohelpdeskitem2details.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If ohelpdeskitem2details is Nothing then
ohelpdeskitem2details = new Objects.helpdeskitem2details(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
ohelpdeskitem2details.requestid=Me.requestid.Text
ohelpdeskitem2details.country_fk=Me.country_fk.SelectedValue
ohelpdeskitem2details.heldpesk_servicelevel_fk=Me.heldpesk_servicelevel_fk.SelectedValue
result=ohelpdeskitem2details.Save()
if not result then throw ohelpdeskitem2details.LastError
_CurrentPk=ohelpdeskitem2details.helpdeskitemdetail_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=ohelpdeskitem2details.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdeskitem2details is Nothing then
   ohelpdeskitem2details = new Objects.helpdeskitem2details(currentuser,actualuser,pk,CDC)
  End If
  ohelpdeskitem2details.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdeskitem2details is Nothing then
   ohelpdeskitem2details = new Objects.helpdeskitem2details(currentuser,actualuser,pk,CDC)
  End If
  ohelpdeskitem2details.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If ohelpdeskitem2details is Nothing then
   ohelpdeskitem2details = new Objects.helpdeskitem2details(currentuser,pk,CDC)
  End If
  ohelpdeskitem2details.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.requestid.Enabled=IIF(WL>=0,True,False)
  Me.blkrequestid.Visible=IIF(RL>=0,True,False)
        Me.country_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkcountry_fk.Visible = IIf(RL >= 0, True, False)
        Me.heldpesk_servicelevel_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkheldpesk_servicelevel_fk.Visible = IIf(RL >= 0, True, False)
  End Sub
End Class

