Partial Class bubbletargets_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents obubbletargets As Objects.bubbletargets
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
    CO=new SqlClient.SqlCommand("rkg_bubbles_lookup")
    CO.CommandType = CommandType.StoredProcedure
    If Me.bubble_fk.Items.Count=0 Then
        Me.bubble_fk.DataSource=CDC.ReadDataTable(CO)
        Me.bubble_fk.DataTextField = "value"
        Me.bubble_fk.DataValueField = "pk"
        Try
            Me.bubble_fk.DataBind
        Catch Ex as Exception
            Me.bubble_fk.SelectedValue=-1
            Me.bubble_fk.DataBind
        End Try
    End If
            Me.target.Text="0.00"
            Me.eacarbonsurplus.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If obubbletargets is Nothing then
obubbletargets = new Objects.bubbletargets(currentuser,actualuser,pk,CDC)
End If
Me.bubble_fk.SelectedValue=obubbletargets.bubble_fk
Me.targetdescription.Text=obubbletargets.targetdescription
Me.targetstart.Text=obubbletargets.targetstart.ToString("dd MMM yyyy")
If Me.targetstart.Text="01 Jan 0001" then Me.targetstart.Text=""
Me.targetstart.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.targetstart.Attributes.Add("onBlur", "clearNotice()")
Me.targetend.Text=obubbletargets.targetend.ToString("dd MMM yyyy")
If Me.targetend.Text="01 Jan 0001" then Me.targetend.Text=""
Me.targetend.Attributes.Add("onFocus", "setNotice(dateFormat)")
Me.targetend.Attributes.Add("onBlur", "clearNotice()")
Me.target.Text=obubbletargets.target
Me.target.Attributes.Add("onFocus", "setNotice('Format: (n)n(.nn) eg 1234.56')")
Me.target.Attributes.Add("onBlur", "clearNotice()")
Me.eacarbonsurplus.Text=obubbletargets.eacarbonsurplus
Me.eacarbonsurplus.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.eacarbonsurplus.Attributes.Add("onBlur", "clearNotice()")
_CurrentPk=obubbletargets.bubbletarget_pk
Status=obubbletargets.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If obubbletargets is Nothing then
obubbletargets = new Objects.bubbletargets(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
obubbletargets.bubble_fk=Me.bubble_fk.SelectedValue
obubbletargets.targetdescription=Me.targetdescription.Text
obubbletargets.targetstart=CommonFN.CheckEmptyDate(Me.targetstart.Text)
obubbletargets.targetend=CommonFN.CheckEmptyDate(Me.targetend.Text)
obubbletargets.target=Me.target.Text
obubbletargets.eacarbonsurplus=Me.eacarbonsurplus.Text
result=obubbletargets.Save()
if not result then throw obubbletargets.LastError
_CurrentPk=obubbletargets.bubbletarget_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=obubbletargets.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbletargets is Nothing then
   obubbletargets = new Objects.bubbletargets(currentuser,actualuser,pk,CDC)
  End If
  obubbletargets.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbletargets is Nothing then
   obubbletargets = new Objects.bubbletargets(currentuser,actualuser,pk,CDC)
  End If
  obubbletargets.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If obubbletargets is Nothing then
   obubbletargets = new Objects.bubbletargets(currentuser,pk,CDC)
  End If
  obubbletargets.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.bubble_fk.Enabled=IIF(WL>=0,True,False)
  Me.blkbubble_fk.Visible=IIF(RL>=0,True,False)
  Me.targetdescription.Enabled=IIF(WL>=0,True,False)
  Me.blktargetdescription.Visible=IIF(RL>=0,True,False)
  Me.targetstart.Enabled=IIF(WL>=0,True,False)
  Me.blktargetstart.Visible=IIF(RL>=0,True,False)
  Me.targetend.Enabled=IIF(WL>=0,True,False)
  Me.blktargetend.Visible=IIF(RL>=0,True,False)
  Me.target.Enabled=IIF(WL>=0,True,False)
  Me.blktarget.Visible=IIF(RL>=0,True,False)
  Me.eacarbonsurplus.Enabled=IIF(WL>=0,True,False)
  Me.blkeacarbonsurplus.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

