<%@ Control Language="VB" AutoEventWireup="false" CodeFile="systemparameters_ctl.ascx.vb" Inherits="systemparameters_ctl" %>
<h5>systemparameters</h5>
<ul class='formcontrol'>
<li runat="server" id="blkcustomerlocation">
<span class='label'>customerlocation</span>
<asp:TextBox EnableViewState="false" ID="customerlocation" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkssrsdomain">
<span class='label'>ssrsdomain</span>
<asp:TextBox EnableViewState="false" ID="ssrsdomain" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkssrsusername">
<span class='label'>ssrsusername</span>
<asp:TextBox EnableViewState="false" ID="ssrsusername" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkssrspassword">
<span class='label'>ssrspassword</span>
<asp:TextBox EnableViewState="false" ID="ssrspassword" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkssrsurl">
<span class='label'>ssrsurl</span>
<asp:TextBox EnableViewState="false" ID="ssrsurl" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkimportfolder">
<span class='label'>importfolder</span>
<asp:TextBox EnableViewState="false" ID="importfolder" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkmatriximportfolder">
<span class='label'>matriximportfolder</span>
<asp:TextBox EnableViewState="false" ID="matriximportfolder" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkdatalocation">
<span class='label'>datalocation</span>
<asp:TextBox EnableViewState="false" ID="datalocation" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blksmtpserver">
<span class='label'>smtpserver</span>
<asp:TextBox EnableViewState="false" ID="smtpserver" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkhelpdeskurl">
<span class='label'>helpdeskurl</span>
<asp:TextBox EnableViewState="false" ID="helpdeskurl" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkapplicationdirectory">
<span class='label'>applicationdirectory</span>
<asp:TextBox EnableViewState="false" ID="applicationdirectory" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkintervaldatadirectory">
<span class='label'>intervaldatadirectory</span>
<asp:TextBox EnableViewState="false" ID="intervaldatadirectory" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktyrrelldatadirectory">
<span class='label'>tyrrelldatadirectory</span>
<asp:TextBox EnableViewState="false" ID="tyrrelldatadirectory" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkhelpdeskRESTAPIurl">
<span class='label'>helpdeskRESTAPIurl</span>
<asp:TextBox EnableViewState="false" ID="helpdeskRESTAPIurl" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkhelpdeskRESTAPIkey">
<span class='label'>helpdeskRESTAPIkey</span>
<asp:TextBox EnableViewState="false" ID="helpdeskRESTAPIkey" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

