<%@ Control Language="VB" AutoEventWireup="false" CodeFile="reportlogs_ctl.ascx.vb" Inherits="reportlogs_ctl" %>
<h5>reportlogs</h5>
<ul class='formcontrol'>
<li runat="server" id="blkreport_fk">
<span class='label'>report_fk</span>
<asp:DropDownList ID="report_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blkrunfromurl">
<span class='label'>runfromurl</span>
<asp:TextBox EnableViewState="false" ID="runfromurl" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkparam1name">
<span class='label'>param1name</span>
<asp:TextBox EnableViewState="false" ID="param1name" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam1value">
<span class='label'>param1value</span>
<asp:TextBox EnableViewState="false" ID="param1value" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam2name">
<span class='label'>param2name</span>
<asp:TextBox EnableViewState="false" ID="param2name" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam2value">
<span class='label'>param2value</span>
<asp:TextBox EnableViewState="false" ID="param2value" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam3name">
<span class='label'>param3name</span>
<asp:TextBox EnableViewState="false" ID="param3name" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam3value">
<span class='label'>param3value</span>
<asp:TextBox EnableViewState="false" ID="param3value" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam4name">
<span class='label'>param4name</span>
<asp:TextBox EnableViewState="false" ID="param4name" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkparam4value">
<span class='label'>param4value</span>
<asp:TextBox EnableViewState="false" ID="param4value" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blksavedpath">
<span class='label'>savedpath</span>
<asp:TextBox EnableViewState="false" ID="savedpath" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
</ul>

