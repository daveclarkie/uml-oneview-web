<%@ Page Language="VB" MasterPageFile="~/design/masters/user.master" AutoEventWireup="false" CodeFile="rfqgeneration.aspx.vb" Inherits="corecm_rfqgeneration" title="RFQ Generation" %>
<%@ Register Src="../controls/nav/rightnav.ascx" TagName="rightnav" TagPrefix="uc0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<div class="inner">
    <div class="content">

        <h3>RFQ Generation</h3> 

        <asp:Panel ID="pnlsearchcustomer" runat="server">
            <table>
                <tr>
                    <td style="width:150px;">
                        <b>Select Customer</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSearch" runat="server" onkeyup = "FilterItems(this.value)" Width="250px"></asp:TextBox><br />
                        <asp:ListBox ID="ddlCustomer" runat="server" AutoPostBack="true" Width="200px"></asp:ListBox>
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
            
        <asp:Panel ID="pnlselectcustomer" runat="server">
            <table>
                <tr>
                    <td style="width:150px;">
                        <b>Selected Customer</b>
                    </td>
                    <td>
                        <asp:label ID="lblCustomerName" runat="server" /><asp:label ID="lblCustomerID" runat="server" Visible="false" />
                        <asp:LinkButton ID="lnkchangecustomer" runat="server" Text="[change]" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
            
        <asp:Panel ID="pnlselectsites" runat="server">
            <table>
                <tr>
                    <td style="width:150px;">
                        <b>Select Site</b>
                    </td>
                    <td>
                        <asp:Repeater ID="rptSiteList" runat="server">
                            <HeaderTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <h4>site id</h4>
                                    </td>
                                    <td>
                                        <h4>site name</h4>
                                    </td>
                                    <td></td>
                                </tr>
                        </HeaderTemplate> 
                            <ItemTemplate>
                            
                                <tr id='node_<%# Container.DataItem("site_pk") %>'>
                                    <td>
                                        <%# Container.DataItem("site_pk")%>
                                    </td>
                                    <td>
                                        <%# Container.DataItem("sitename")%>
                                    </td>
                                    <td>
                                        <a class="remove" title="Remove">X</a>                                
                                    </td>
                                </tr>
                        </ItemTemplate> 
                            <FooterTemplate>
                            </table>
                        </FooterTemplate> 
                        </asp:Repeater>                        
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <ul id="innertab">
            <li class='<%=LIi("customer")%>'><a href="rfqgeneration.aspx?pk=<%=PSM.mainid %>&pg=cu">Generate New RFQ</a></li>
        </ul>

    </div>

    <div class="navigation">
        <uc0:rightnav ID="CustomerNav" runat="server" />
    </div>

</div>

<script type = "text/javascript">
    var ddlText, ddlValue, ddl, lblMesg;
    function CacheItems() {
        ddlText = new Array();
        ddlValue = new Array();
        ddl = document.getElementById("<%=ddlCustomer.ClientID %>");
        lblMesg = document.getElementById("<%=lblMessage.ClientID%>");
        for (var i = 0; i < ddl.options.length; i++) {
            ddlText[ddlText.length] = ddl.options[i].text;
            ddlValue[ddlValue.length] = ddl.options[i].value;
        }
    }

    window.onload = CacheItems;

</script>

</asp:Content>
