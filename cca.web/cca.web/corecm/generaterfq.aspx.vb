﻿Imports Microsoft.Reporting.WebForms
Imports System.Net
Imports System.Xml

Imports System
Imports System.IO
Imports System.Text

Partial Class corecm_generaterfq
    Inherits ExternalPage : Public PSM As MySM = SM : Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        RunReport()
    End Sub
    Dim errormessage As String = ""
    Dim cc As Object

    Private Sub RunReport()
        Try

            Dim variable_pk As Integer = Request.QueryString("rfq_pk").ToString
            Dim variable_file As String = Request.QueryString("filename").ToString

            CO = New SqlCommand("rsp_systemparameters_latest")
            CO.CommandType = CommandType.StoredProcedure
            Dim systemparameter_pk As Integer = -1
            CDC.ReadScalarValue(systemparameter_pk, CO)
            Dim sys As New systemparameters(SM.currentuser, SM.actualuser, systemparameter_pk, CDC)


            CO = New SqlCommand("rsp_rfq_read")
            CO.CommandType = CommandType.StoredProcedure
            CO.Parameters.AddWithValue("@rfq_pk", variable_pk)
            Dim dt As New DataTable
            dt = CDC.ReadDataTable(CO)

            Dim fueltype_fk As Integer = -1
            fueltype_fk = dt.Rows(0).Item("rfqfueltype_fk")


            myReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            Dim serverReport As Microsoft.Reporting.WebForms.ServerReport
            serverReport = myReportViewer.ServerReport

            serverReport.ReportServerUrl = New Uri(sys.ssrsurl)
            If fueltype_fk = 1 Then
                serverReport.ReportPath = "/UML/Contract Manager/System Generated Reports/UK Electricity RFQ"
            Else
                serverReport.ReportPath = "/UML/Contract Manager/System Generated Reports/UK Gas RFQ"
            End If

            Dim param As New Microsoft.Reporting.WebForms.ReportParameter()
            param.Name = "rfq_pk"
            param.Values.Add(variable_pk)

            'Set the report parameters for the report
            Dim parameters() As Microsoft.Reporting.WebForms.ReportParameter = {param}
            serverReport.SetParameters(parameters)

            Dim warnings As Warning() = {}
            Dim streamids As String() = {}
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim extension As String = ""
            Dim deviceInfo As String = ""
            deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>"
            Dim bytes As Byte() = serverReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamids, warnings)

            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType

            ' This header is for saving it as an Attachment and popup window should display to to offer save as or open a PDF file 
            Response.AddHeader("Content-Disposition", "attachment; filename=" + variable_file + "." + extension)
            'Response.AddHeader("content-disposition", "inline; filename=MTSReport." + extension)
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            Print(ex.Message)
        End Try
    End Sub
  
End Class
