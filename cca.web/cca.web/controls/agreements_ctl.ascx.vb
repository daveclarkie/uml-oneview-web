Partial Class agreements_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents oagreements As Objects.agreements
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        CO = New SqlClient.SqlCommand("rkg_organisations_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.organisation_fk.Items.Count = 0 Then
            Me.organisation_fk.DataSource = CDC.ReadDataTable(CO)
            Me.organisation_fk.DataTextField = "value"
            Me.organisation_fk.DataValueField = "pk"
            Try
                Me.organisation_fk.DataBind()
            Catch Ex As Exception
                Me.organisation_fk.SelectedValue = -1
                Me.organisation_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_federations_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.federation_fk.Items.Count = 0 Then
            Me.federation_fk.DataSource = CDC.ReadDataTable(CO)
            Me.federation_fk.DataTextField = "value"
            Me.federation_fk.DataValueField = "pk"
            Try
                Me.federation_fk.DataBind()
            Catch Ex As Exception
                Me.federation_fk.SelectedValue = -1
                Me.federation_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_agreementtypes_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreementtype_fk.Items.Count = 0 Then
            Me.agreementtype_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreementtype_fk.DataTextField = "value"
            Me.agreementtype_fk.DataValueField = "pk"
            Try
                Me.agreementtype_fk.DataBind()
            Catch Ex As Exception
                Me.agreementtype_fk.SelectedValue = -1
                Me.agreementtype_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_agreementmeasures_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreementmeasure_fk.Items.Count = 0 Then
            Me.agreementmeasure_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreementmeasure_fk.DataTextField = "value"
            Me.agreementmeasure_fk.DataValueField = "pk"
            Try
                Me.agreementmeasure_fk.DataBind()
            Catch Ex As Exception
                Me.agreementmeasure_fk.SelectedValue = -1
                Me.agreementmeasure_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_agreements_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreement_fk.Items.Count = 0 Then
            Me.agreement_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreement_fk.DataTextField = "value"
            Me.agreement_fk.DataValueField = "pk"
            Try
                Me.agreement_fk.DataBind()
            Catch Ex As Exception
                Me.agreement_fk.SelectedValue = -1
                Me.agreement_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rsp_users_lookup_ccatechnicalusers")
        CO.CommandType = CommandType.StoredProcedure
        If Me.technicaluser_fk.Items.Count = 0 Then
            Me.technicaluser_fk.DataSource = CDC.ReadDataTable(CO)
            Me.technicaluser_fk.DataTextField = "value"
            Me.technicaluser_fk.DataValueField = "pk"
            Try
                Me.technicaluser_fk.DataBind()
            Catch Ex As Exception
                Me.technicaluser_fk.SelectedValue = -1
                Me.technicaluser_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rsp_users_lookup_ccareportingusers")
        CO.CommandType = CommandType.StoredProcedure
        If Me.reportinguser_fk.Items.Count = 0 Then
            Me.reportinguser_fk.DataSource = CDC.ReadDataTable(CO)
            Me.reportinguser_fk.DataTextField = "value"
            Me.reportinguser_fk.DataValueField = "pk"
            Try
                Me.reportinguser_fk.DataBind()
            Catch Ex As Exception
                Me.reportinguser_fk.SelectedValue = -1
                Me.reportinguser_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rsp_users_lookup_ccaaccountmanagerusers")
        CO.CommandType = CommandType.StoredProcedure
        If Me.accountmanageruser_fk.Items.Count = 0 Then
            Me.accountmanageruser_fk.DataSource = CDC.ReadDataTable(CO)
            Me.accountmanageruser_fk.DataTextField = "value"
            Me.accountmanageruser_fk.DataValueField = "pk"
            Try
                Me.accountmanageruser_fk.DataBind()
            Catch Ex As Exception
                Me.accountmanageruser_fk.SelectedValue = -1
                Me.accountmanageruser_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_agreementstages_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.agreementstage_fk.Items.Count = 0 Then
            Me.agreementstage_fk.DataSource = CDC.ReadDataTable(CO)
            Me.agreementstage_fk.DataTextField = "value"
            Me.agreementstage_fk.DataValueField = "pk"
            Try
                Me.agreementstage_fk.DataBind()
            Catch Ex As Exception
                Me.agreementstage_fk.SelectedValue = -1
                Me.agreementstage_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If oagreements Is Nothing Then
            oagreements = New Objects.agreements(currentuser, actualuser, pk, CDC)
        End If
        Me.organisation_fk.SelectedValue = oagreements.organisation_fk
        Me.agreementname.Text = oagreements.agreementname
        Me.federation_fk.SelectedValue = oagreements.federation_fk
        Me.agreementnumber.Text = oagreements.agreementnumber
        Me.facilitynumber.Text = oagreements.facilitynumber
        Me.targetunitidentifier.Text = oagreements.targetunitidentifier
        Me.baseyearstart.Text = oagreements.baseyearstart.ToString("dd MMM yyyy")
        If Me.baseyearstart.Text = "01 Jan 0001" Then Me.baseyearstart.Text = ""
        Me.baseyearstart.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.baseyearstart.Attributes.Add("onBlur", "clearNotice()")
        Me.baseyearend.Text = oagreements.baseyearend.ToString("dd MMM yyyy")
        If Me.baseyearend.Text = "01 Jan 0001" Then Me.baseyearend.Text = ""
        Me.baseyearend.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.baseyearend.Attributes.Add("onBlur", "clearNotice()")
        Me.agreementtype_fk.SelectedValue = oagreements.agreementtype_fk
        Me.agreementmeasure_fk.SelectedValue = oagreements.agreementmeasure_fk
        Me.agreement_fk.SelectedValue = oagreements.agreement_fk
        Me.submeterinstalled.Checked = oagreements.submeterinstalled
        Me.notes.Text = oagreements.notes
        Me.agreementstart.Text = oagreements.agreementstart.ToString("dd MMM yyyy")
        If Me.agreementstart.Text = "01 Jan 0001" Then Me.agreementstart.Text = ""
        Me.agreementstart.Attributes.Add("onFocus", "setNotice(dateFormat)")
        Me.agreementstart.Attributes.Add("onBlur", "clearNotice()")
        Me.includecrcsaving.Checked = oagreements.includecrcsaving
        Me.technicaluser_fk.SelectedValue = oagreements.technicaluser_fk
        Me.reportinguser_fk.SelectedValue = oagreements.reportinguser_fk
        Me.accountmanageruser_fk.SelectedValue = oagreements.accountmanageruser_fk
        Me.agreementstage_fk.SelectedValue = oagreements.agreementstage_fk
        _CurrentPk = oagreements.agreement_pk
        Status = oagreements.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreements Is Nothing Then
            oagreements = New Objects.agreements(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            oagreements.organisation_fk = Me.organisation_fk.SelectedValue
            oagreements.agreementname = Me.agreementname.Text
            oagreements.federation_fk = Me.federation_fk.SelectedValue
            oagreements.agreementnumber = Me.agreementnumber.Text
            oagreements.facilitynumber = Me.facilitynumber.Text
            oagreements.targetunitidentifier = Me.targetunitidentifier.Text
            oagreements.baseyearstart = CommonFN.CheckEmptyDate(Me.baseyearstart.Text)
            oagreements.baseyearend = CommonFN.CheckEmptyDate(Me.baseyearend.Text)
            oagreements.agreementtype_fk = Me.agreementtype_fk.SelectedValue
            oagreements.agreementmeasure_fk = Me.agreementmeasure_fk.SelectedValue
            oagreements.agreement_fk = Me.agreement_fk.SelectedValue
            oagreements.submeterinstalled = Me.submeterinstalled.Checked
            oagreements.notes = Me.notes.Text
            oagreements.agreementstart = CommonFN.CheckEmptyDate(Me.agreementstart.Text)
            oagreements.includecrcsaving = Me.includecrcsaving.Checked
            oagreements.technicaluser_fk = Me.technicaluser_fk.SelectedValue
            oagreements.reportinguser_fk = Me.reportinguser_fk.SelectedValue
            oagreements.accountmanageruser_fk = Me.accountmanageruser_fk.SelectedValue
            oagreements.agreementstage_fk = Me.agreementstage_fk.SelectedValue
            result = oagreements.Save()
            If Not result Then Throw oagreements.LastError
            _CurrentPk = oagreements.agreement_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = oagreements.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreements Is Nothing Then
            oagreements = New Objects.agreements(currentuser, actualuser, pk, CDC)
        End If
        oagreements.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreements Is Nothing Then
            oagreements = New Objects.agreements(currentuser, actualuser, pk, CDC)
        End If
        oagreements.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If oagreements Is Nothing Then
            oagreements = New Objects.agreements(currentuser, pk, CDC)
        End If
        oagreements.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.organisation_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkorganisation_fk.Visible = IIF(RL >= 0, True, False)
        Me.agreementname.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementname.Visible = IIF(RL >= 0, True, False)
        Me.federation_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkfederation_fk.Visible = IIF(RL >= 0, True, False)
        Me.agreementnumber.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementnumber.Visible = IIF(RL >= 0, True, False)
        Me.facilitynumber.Enabled = IIF(WL >= 0, True, False)
        Me.blkfacilitynumber.Visible = IIF(RL >= 0, True, False)
        Me.targetunitidentifier.Enabled = IIF(WL >= 0, True, False)
        Me.blktargetunitidentifier.Visible = IIF(RL >= 0, True, False)
        Me.baseyearstart.Enabled = IIF(WL >= 0, True, False)
        Me.blkbaseyearstart.Visible = IIF(RL >= 0, True, False)
        Me.baseyearend.Enabled = IIF(WL >= 0, True, False)
        Me.blkbaseyearend.Visible = IIF(RL >= 0, True, False)
        Me.agreementtype_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementtype_fk.Visible = IIF(RL >= 0, True, False)
        Me.agreementmeasure_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementmeasure_fk.Visible = IIF(RL >= 0, True, False)
        Me.agreement_fk.Enabled = IIF(WL >= 1, True, False)
        Me.blkagreement_fk.Visible = IIF(RL >= 1, True, False)
        Me.submeterinstalled.Enabled = IIF(WL >= 0, True, False)
        Me.blksubmeterinstalled.Visible = IIF(RL >= 0, True, False)
        Me.notes.Enabled = IIf(WL >= 99, True, False)
        Me.blknotes.Visible = IIf(RL >= 99, True, False)
        Me.agreementstart.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementstart.Visible = IIF(RL >= 0, True, False)
        Me.includecrcsaving.Enabled = IIF(WL >= 0, True, False)
        Me.blkincludecrcsaving.Visible = IIF(RL >= 0, True, False)
        Me.technicaluser_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blktechnicaluser_fk.Visible = IIf(RL >= 0, True, False)
        Me.reportinguser_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkreportinguser_fk.Visible = IIf(RL >= 0, True, False)
        Me.accountmanageruser_fk.Enabled = IIf(WL >= 0, True, False)
        Me.blkaccountmanageruser_fk.Visible = IIf(RL >= 0, True, False)
        Me.agreementstage_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkagreementstage_fk.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

