Public Class MySM
    Inherits objects.sessions
    Shadows CDC As DataCommon

    Public Sub New(ByVal Session As Integer, ByRef Request As HttpRequest, ByRef xCDC As DataCommon)
        MyBase.New(-1, -1, xCDC)
        CDC = xCDC
        MyBase.Read(Session)
    End Sub

    Public Sub New(ByVal SSID As String, ByRef Request As HttpRequest, ByRef xCDC As DataCommon)
        MyBase.New(-1, -1, xCDC)
        Dim ss_pk As Integer = -1
        Dim ss_id As String = SSID
        Dim ss_ip As String = Request.UserHostAddress
        CDC = xCDC
        Dim CO As New SqlCommand("rsp_getsession")
        CO.CommandType = CommandType.StoredProcedure
        CO.Parameters.AddWithValue("@SSID", ss_id)
        CO.Parameters.AddWithValue("@SSIP", ss_ip)
        CDC.ReadScalarValue(ss_pk, CO)
        MyBase.Read(ss_pk)

        If ss_pk = -1 Then
            MyBase.sessionid = ss_id
            MyBase.ip = ss_ip
            MyBase.creator_fk = -1
            MyBase.editor_fk = -1
            MyBase.currentuser = 0
            MyBase.actualuser = 0
            MyBase.targetuser = 0
            MyBase.targetagreement = 0
            MyBase.Save()
        End If

    End Sub

    Public Shadows Sub Save()
        MyBase.Save()
    End Sub

    Public Sub Purge()
        MyBase.Delete()
    End Sub

    Public Function PersonFromUser(ByVal user As Integer) As Integer
        Dim retval As Integer
        Dim CO As New SqlCommand()
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_getpersonfromuser"
        CO.Parameters.AddWithValue("@user_pk", user)
        CDC.ReadScalarValue(retval, CO)
        CO.Dispose()
        Return retval
    End Function

    Public Function UserName(ByVal user_pk As Integer, Optional ByVal format As Integer = 3) As String
        Dim retval As String = ""

        Dim CO As New SqlCommand("select dbo.formatpersonname(" & user_pk & "," & format.ToString & ")")
        CDC.ReadScalarValue(retval, CO)

        Return retval
    End Function

    Public Function UserLocation(ByVal location As Integer, Optional ByVal format As Integer = 3) As String
        Dim retval As String = ""

        Dim CO As New SqlCommand("select dbo.formatlocation(dbo.primarylocation_fk(" & location & ")," & format.ToString & ")")
        CDC.ReadScalarValue(retval, CO)

        Return retval
    End Function

    Public ReadOnly Property PersonPageModeValue() As String
        Get
            Select Case MyBase.PersonPageMode
                Case 4 : Return "ml"
                Case 2 : Return "lc"
                Case 1 : Return "ps"
                Case 3 : Return "ph"
                Case Else : Return ""
            End Select
        End Get
    End Property


End Class
