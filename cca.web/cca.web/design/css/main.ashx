<%@ WebHandler Language="VB" Class="main" %>

Imports System
Imports System.Web

Public Class main : Implements IHttpHandler
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim RU As String = context.Request.Url.ToString
        Dim Host As String = context.Request.Url.ToString.Split("/".ToCharArray(), 5, StringSplitOptions.RemoveEmptyEntries)(1)
        Host = Host.ToLower
        context.Response.Clear()
        context.Response.ContentType = "text/css"
        Dim subDomain As String = Host.Split(".".ToCharArray, 2)(0)
        Dim system As String = ""
        Select Case True
            Case subDomain.StartsWith("live"), subDomain.StartsWith("dev"), Host.StartsWith("seweb.live."), Host.StartsWith("oneview.mceg.")
                system = "live"
            Case Host.StartsWith("seweb.testdev.")
                system = "testing"
            Case subDomain.StartsWith("localhost"), subDomain.StartsWith("127"), Host.StartsWith("seweb.dev.")
                system = "development"
            Case Else
                system = "unauthorised"
        End Select
        
        
        'Select Case system
        '    Case "live"
        '        context.Response.Write("//body {background-color: rgba(255,255,255,0) !important}/* for " & Host & " | system: " & system & " | Subdomain: " & subDomain & " */")
        '    Case "testing"
        '        context.Response.Write("body {background: url('/design/images/testbg.png') !important}/* for " & Host & " | system: " & system & " | Subdomain: " & subDomain & " */")
        '    Case "development"
        '        context.Response.Write("body {background: url('/design/images/bg_dev.png') !important}/* for " & Host & " | system: " & system & " | Subdomain: " & subDomain & " */")
        '    Case Else
        '        context.Response.Write("body {background-color:#ffffff !important}/* for " & Host & " | system: " & system & " | Subdomain: " & subDomain & " */")
        'End Select

        Dim dom As String = ""
        Try
            dom = Host.Split(".".ToCharArray, 2)(1)
        Catch ex As Exception
            dom = Host
        End Try
        
        Select Case system
            Case "live"
                context.Response.Write(".banner h1 {background: url('../images/logo_live.png') no-repeat scroll right top transparent !important})")
            Case "testing"
                context.Response.Write(".banner h1 {background: url('../images/logo_testing.png') no-repeat scroll right top transparent !important})")
            Case "development"
                context.Response.Write(".banner h1 {background: url('../images/logo_live.png') no-repeat scroll right top transparent !important})")
            Case Else
                context.Response.Write(".banner h1 {background: url('../images/logo_notauth.png') no-repeat scroll right top transparent !important})")
        End Select
    End Sub
 

End Class