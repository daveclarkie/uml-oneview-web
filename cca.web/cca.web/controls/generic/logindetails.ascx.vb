Imports System.Net.Mail

Partial Class mainctls_logindetails
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Dim result As Boolean = False
        Dim log As String = ""
        Dim user_pk As Integer = 0
        Dim userfound As Boolean = False
        Dim newpassword As String = ""
        Dim dr As DataRow
        CDC.Sanitize(Me.forename.Text)
        CDC.Sanitize(Me.email.Text)

        Dim CO As New SqlClient.SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_CreatePasswd"
        CDC.ReadScalarValue(newpassword, CO)


        CO = New SqlClient.SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_FindUser"
        CO.Parameters.AddWithValue("@usr", "")
        CO.Parameters.AddWithValue("@name", Me.forename.Text)
        CO.Parameters.AddWithValue("@email", Me.email.Text)
        Dim dt As DataTable = CDC.ReadDataTable(CO)

        If dt.Rows.Count <> 0 Then
            userfound = True
            dr = dt.Rows(0)
            Dim user As New users(-1, -1, dr("user_pk"), CDC)
            CO = New SqlClient.SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_CreatePasswdEnc"
            CO.Parameters.AddWithValue("@pwd", newpassword)
            CDC.ReadScalarValue(user.encpassword, CO)
            userfound = userfound And user.Save()
            Dim TargetAccounts As String = ""
          
            CO.CommandText = "[rsp_useremails_forsystemmail]"
            CO.Parameters.Clear()
            CO.Parameters.AddWithValue("@user_pk", user.user_pk)
            Dim sdt As DataTable = CDC.ReadDataTable(CO)
            Dim sdr As DataRow

            For Each sdr In sdt.Rows
                TargetAccounts &= sdr("email")
                TargetAccounts &= ","
            Next

            If TargetAccounts.Length > 1 Then
                TargetAccounts = TargetAccounts.Trim(",".ToCharArray)
            End If

            ' Reset Password Task
            Dim strTo() As String = TargetAccounts.Split(",".ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
            Dim insMail As New MailMessage(New MailAddress("donotreply@ems.schneider-electric.com"), New MailAddress(TargetAccounts))

            Dim emailpk As Integer = 0

            CO.CommandText = "rsp_primaryemail"
            CO.Parameters.Clear()
            CO.Parameters.AddWithValue("@user_pk", user.user_pk)

            Dim edr As DataRow = CDC.ReadDataRow(CO)
            emailpk = edr("email_pk")
            edr = Nothing

            Dim em As New emails(user.user_pk, user.user_pk, emailpk, CDC)


            Dim msg As String = ""
            msg &= "Your system ( http://" & Request.ServerVariables("HTTP_HOST") & "/ ) account has been activated." & "</br></br>"
            msg &= "You can now log in using:</br>"
            msg &= "<b>Username:</b> " & em.email & "</br>"
            msg &= "<b>Password:</b> " & newpassword & "</br>"
            msg &= "</br></br>" & "This is an automated message from an account that is unable to receive email. This email account is not associated with a human being. Replying to this message will result in the mail server refusing to deliver your response."

            With insMail
                .Subject = "Password Reset"
                .IsBodyHtml = True
                .Body = htmlBody(msg)
                .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
            End With
            Dim smtp As New System.Net.Mail.SmtpClient
            smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            smtp.Host = "10.44.33.23"
            smtp.Port = 25


            Try
                smtp.Send(insMail)

                lblMessage.Text = "Your new password has been emailed to you."
                lblMessage.CssClass = "ok"

            Catch ex As Exception
                log = ex.Message

                lblMessage.CssClass = "error"
                If userfound Then
                    lblMessage.Text = "Unable to deliver email:-<br />" & log
                Else
                    lblMessage.Text = "No user accounts found with the supplied forename (forename) and email address"
                End If

            End Try
        End If

    End Sub

    Public Function htmlBody(ByVal body As String) As String
        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
        htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P>"
        htmlMessageBody = htmlMessageBody + body
        htmlMessageBody = htmlMessageBody + "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"
        Return htmlMessageBody
    End Function

   End Class
