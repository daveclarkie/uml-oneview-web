Partial Class meterpoints_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents ometerpoints As Objects.meterpoints
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub

        Me.channel.Text = "0"
        CO = New SqlClient.SqlCommand("rkg_fuels_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.fuel_fk.Items.Count = 0 Then
            Me.fuel_fk.DataSource = CDC.ReadDataTable(CO)
            Me.fuel_fk.DataTextField = "value"
            Me.fuel_fk.DataValueField = "pk"
            Try
                Me.fuel_fk.DataBind()
            Catch Ex As Exception
                Me.fuel_fk.SelectedValue = -1
                Me.fuel_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_units_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.unit_fk.Items.Count = 0 Then
            Me.unit_fk.DataSource = CDC.ReadDataTable(CO)
            Me.unit_fk.DataTextField = "value"
            Me.unit_fk.DataValueField = "pk"
            Try
                Me.unit_fk.DataBind()
            Catch Ex As Exception
                Me.unit_fk.SelectedValue = -1
                Me.unit_fk.DataBind()
            End Try
        End If
        CO = New SqlClient.SqlCommand("rkg_sites_lookup")
        CO.CommandType = CommandType.StoredProcedure
        If Me.umlsite_fk.Items.Count = 0 Then
            Me.umlsite_fk.DataSource = CDC.ReadDataTable(CO)
            Me.umlsite_fk.DataTextField = "value"
            Me.umlsite_fk.DataValueField = "pk"
            Try
                Me.umlsite_fk.DataBind()
            Catch Ex As Exception
                Me.umlsite_fk.SelectedValue = -1
                Me.umlsite_fk.DataBind()
            End Try
        End If
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As String = "0")
        ctlInit()
        If ometerpoints Is Nothing Then
            ometerpoints = New objects.meterpoints(currentuser, actualuser, pk, CDC)
        End If
        Me.meter_fk.SelectedValue = ometerpoints.meter_fk
        Me.tablename.Text = ometerpoints.tablename
        Me.meter.Text = ometerpoints.meter
        Me.channel.Text = ometerpoints.channel
        Me.channel.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.channel.Attributes.Add("onBlur", "clearNotice()")
        Me.fuel_fk.SelectedValue = ometerpoints.fuel_fk
        Me.unit_fk.SelectedValue = ometerpoints.unit_fk
        Me.umlsite_fk.SelectedValue = ometerpoints.umlsite_fk
        Me.sitename.Text = ometerpoints.sitename
        Me.displayname.Text = ometerpoints.displayname
        _CurrentPk = ometerpoints.meterpoint_pk
        Status = ometerpoints.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ometerpoints Is Nothing Then
            ometerpoints = New Objects.meterpoints(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            ometerpoints.meter_fk = Me.meter_fk.SelectedValue
            ometerpoints.tablename = Me.tablename.Text
            ometerpoints.meter = Me.meter.Text
            ometerpoints.channel = Me.channel.Text
            ometerpoints.fuel_fk = Me.fuel_fk.SelectedValue
            ometerpoints.unit_fk = Me.unit_fk.SelectedValue
            ometerpoints.umlsite_fk = Me.umlsite_fk.SelectedValue
            ometerpoints.sitename = Me.sitename.Text
            ometerpoints.displayname = Me.displayname.Text
            result = ometerpoints.Save()
            If Not result Then Throw ometerpoints.LastError
            _CurrentPk = ometerpoints.meterpoint_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = ometerpoints.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ometerpoints Is Nothing Then
            ometerpoints = New Objects.meterpoints(currentuser, actualuser, pk, CDC)
        End If
        ometerpoints.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ometerpoints Is Nothing Then
            ometerpoints = New Objects.meterpoints(currentuser, actualuser, pk, CDC)
        End If
        ometerpoints.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If ometerpoints Is Nothing Then
            ometerpoints = New Objects.meterpoints(currentuser, pk, CDC)
        End If
        ometerpoints.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.meter_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkmeter_fk.Visible = IIF(RL >= 0, True, False)
        Me.tablename.Enabled = IIF(WL >= 0, True, False)
        Me.blktablename.Visible = IIF(RL >= 0, True, False)
        Me.meter.Enabled = IIF(WL >= 0, True, False)
        Me.blkmeter.Visible = IIF(RL >= 0, True, False)
        Me.channel.Enabled = IIF(WL >= 0, True, False)
        Me.blkchannel.Visible = IIF(RL >= 0, True, False)
        Me.fuel_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkfuel_fk.Visible = IIF(RL >= 0, True, False)
        Me.unit_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkunit_fk.Visible = IIF(RL >= 0, True, False)
        Me.umlsite_fk.Enabled = IIF(WL >= 0, True, False)
        Me.blkumlsite_fk.Visible = IIF(RL >= 0, True, False)
        Me.sitename.Enabled = IIF(WL >= 0, True, False)
        Me.blksitename.Visible = IIF(RL >= 0, True, False)
        Me.displayname.Enabled = IIF(WL >= 0, True, False)
        Me.blkdisplayname.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

