﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="targets.ascx.vb" Inherits="controls_nav_targets" %>
 <%If LoadList() Then%>
    <div class="navigationmenu" id="pnlTargets" runat="server">
        <h4>Targets</h4>
        
        <asp:Repeater ID="rptTargets" runat="server">
            <HeaderTemplate>
                <table>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width:160px;"><%# IIf(Len(Container.DataItem("targetdescription")) > 26, Left(Container.DataItem("targetdescription"), 23) & "...", Container.DataItem("targetdescription"))%></td>
                    <td style="width:50px; text-align:right;"><%# FormatNumber(Container.DataItem("target"),3) %> %</td>
                </tr>
            </ItemTemplate> 
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
<%End If%>