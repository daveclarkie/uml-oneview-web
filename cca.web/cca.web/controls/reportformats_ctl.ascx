<%@ Control Language="VB" AutoEventWireup="false" CodeFile="reportformats_ctl.ascx.vb" Inherits="reportformats_ctl" %>
<h5>reportformats</h5>
<ul class='formcontrol'>
<li runat="server" id="blkformatname">
<span class='label'>formatname</span>
<asp:TextBox EnableViewState="false" ID="formatname" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkfileextenstion">
<span class='label'>fileextenstion</span>
<asp:TextBox EnableViewState="false" ID="fileextenstion" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkformatnicename">
<span class='label'>formatnicename</span>
<asp:TextBox EnableViewState="false" ID="formatnicename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

