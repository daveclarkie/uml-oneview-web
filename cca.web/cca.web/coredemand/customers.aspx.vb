Imports Microsoft.Reporting.WebForms

Partial Class coredemand_customers
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM

        validpk = Integer.TryParse(Request.QueryString("pk"), pk) 'customer
        validspk = Integer.TryParse(Request.QueryString("spk"), spk) 'site
        validrpk = Integer.TryParse(Request.QueryString("mpk"), mpk) 'meter
        validnpk = Integer.TryParse(Request.QueryString("npk"), npk) 'note
        validapk = Integer.TryParse(Request.QueryString("apk"), apk) 'appointment
        validapt = Integer.TryParse(Request.QueryString("apt"), apt) 'appointmenttype

        'If Not validpk Or Not pk > 0 Then
        '    ' we shouldn't be here
        '    Local.Bounce(SM, Response, "~/corecm/customer.aspx", "no+valid+customer+id")
        'End If

        ' ===============================================
        '  New Target User?
        '        - Yes - Set Target User for session
        '        - No  - No action required  
        ' ===============================================

        If Not (SM.mainid = pk) Then
            SM.mainid = pk
        End If

        If Not (SM.subid = spk) Then
            SM.subid = spk
        End If

        If Not (Request.QueryString("r") Is Nothing) Then 'reason
            AddNotice("setNotice('" + Replace(Request.QueryString("r"), "+", " ") + "');")
        End If

        CheckSessionValues()

        ' ===============================================
        '  Which Tab?
        ' ===============================================
        Try

            Select Case ModeCode
                Case "cu" : CurrentDisplay = CustomerPageMode.Customer
                    vwSub = vwCustomer
                Case "si" : CurrentDisplay = CustomerPageMode.SiteInformation
                    vwSub = vwSiteInformation
                Case "mi" : CurrentDisplay = CustomerPageMode.MeterInformation
                    vwSub = vwMeterInformation
                Case "sa" : CurrentDisplay = CustomerPageMode.ServiceAgreements
                    vwSub = vwServiceAgreements
                Case "ct" : CurrentDisplay = CustomerPageMode.ContractInformation
                    vwSub = vwContractInformation
                Case "cca" : CurrentDisplay = CustomerPageMode.CCAAgreements
                    vwSub = vwCCAAgreements
                Case "ap" : CurrentDisplay = CustomerPageMode.Appointments
                    vwSub = vwAppointments
                Case "nt" : CurrentDisplay = CustomerPageMode.Notes
                    vwSub = vwNotes
                Case Else
                    ' we shouldn't be here
                    Local.Bounce(SM, Response, "~/corecm/search.aspx", "invalid+display+mode")
            End Select

        Catch noQSPart As Exception
            Local.Bounce(SM, Response, "~/corecm/search.aspx", Server.UrlEncode(noQSPart.Message))
        End Try

    End Sub

    Private Sub CheckSessionValues()
        'load organisation
        Dim o As New organisations(SM.currentuser, SM.actualuser, SM.mainid, CDC)
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        DisplayPage()
    End Sub

    Public ReadOnly Property ModeCode() As String
        Get
            Return Request.QueryString("pg")
        End Get
    End Property

    Dim cc As Object
    Dim maintab As String = "search"
    Dim due As DateTime
    Dim diff As Long

    Dim validpk As Boolean = False
    Dim validspk As Boolean = False
    Dim validrpk As Boolean = False
    Dim validnpk As Boolean = False
    Dim validapk As Boolean = False
    Dim validapt As Boolean = False
    Dim CurrentAction As Local.Action = Local.Action.None
    Dim CurrentDisplay As CustomerPageMode = CustomerPageMode.Customer

    Dim vwSub As System.Web.UI.WebControls.View = vwCustomer

    Dim pk As Integer = 0
    Dim spk As Integer = 0
    Dim mpk As Integer = 0
    Dim npk As Integer = 0
    Dim apk As Integer = 0
    Dim apt As Integer = 0
    Dim md As Integer = 0

    Public Mode As CustomerPageMode = CustomerPageMode.Search

    Public Function TabSummary(ByVal item As String) As String
        Dim output As String = ""
        Select Case item
            'Case "sites" : output = sitecount
            'Case "meters" : output = 1
        End Select
        Return "(" & output & ")"
    End Function
    Private Sub SetMode()
        If Page.IsPostBack Then
            Mode = SM.CustomerPageMode
        Else
            SM.mainid = Request.QueryString("pk")
            Mode = CustomerPageMode.Search
            Select Case Request.QueryString("pg")
                Case "cu" : Mode = CustomerPageMode.Customer
                Case "si" : Mode = CustomerPageMode.SiteInformation
                Case "mi" : Mode = CustomerPageMode.MeterInformation
                Case "sa" : Mode = CustomerPageMode.ServiceAgreements
                Case "ct" : Mode = CustomerPageMode.ContractInformation
                Case "cca" : Mode = CustomerPageMode.CCAAgreements
                Case "ap" : Mode = CustomerPageMode.Appointments
                Case "nt" : Mode = CustomerPageMode.Notes
                Case Else : Mode = CustomerPageMode.Search
            End Select
            SM.CustomerPageMode = Mode
        End If
    End Sub

    Private Sub PageLoad()
        SetMode()

        Select Case SM.CustomerPageMode
            Case CustomerPageMode.Customer : InitCustomer(Page.IsPostBack)
            Case CustomerPageMode.SiteInformation : initSiteInformation(Page.IsPostBack)
            Case CustomerPageMode.MeterInformation : InitMeterInformation(Page.IsPostBack)
            Case CustomerPageMode.ServiceAgreements : InitServiceAgreements(Page.IsPostBack)
            Case CustomerPageMode.ContractInformation : InitContractInformation(Page.IsPostBack)
            Case CustomerPageMode.CCAAgreements : InitCCAAgreements(Page.IsPostBack)
            Case CustomerPageMode.Appointments : InitAppointments(Page.IsPostBack)
            Case CustomerPageMode.Notes : InitNotes(Page.IsPostBack)
            Case Else
                Response.Redirect("search.aspx")
        End Select

        Dim o As New organisations(SM.currentuser, SM.targetuser, SM.mainid, CDC)
        lbldemand_organisationname.Text = o.customername


    End Sub
    Private Sub DisplayPage()
        If validspk Then
            CurrentAction = Local.Action.Read
        Else
            CurrentAction = Local.Action.None
        End If
    End Sub
    Private Sub RefreshList()
        If Mode = CustomerPageMode.Customer Then
            If SM.mainid = -1 Then
                rptSites.Dispose()
            Else
               

            End If
        ElseIf Mode = CustomerPageMode.SiteInformation Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_supplies_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptSites.DataSource = dt
            rptSites.DataBind()
            CO.Dispose()

        ElseIf Mode = CustomerPageMode.ContractInformation Then
            
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_contracts_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            CO.Parameters.AddWithValue("@status", "Active")
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptActiveContracts.DataSource = dt
            rptActiveContracts.DataBind()
            CO.Dispose()

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_contracts_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            CO.Parameters.AddWithValue("@status", "Future")
            dt = CDC.ReadDataTable(CO, 0)
            rptFutureContracts.DataSource = dt
            rptFutureContracts.DataBind()
            CO.Dispose()

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_contracts_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            CO.Parameters.AddWithValue("@status", "Historic")
            dt = CDC.ReadDataTable(CO, 0)
            rptHistoricContracts.DataSource = dt
            rptHistoricContracts.DataBind()
            CO.Dispose()

        ElseIf Mode = CustomerPageMode.CCAAgreements Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_ccaagreements_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptCCAAgreements.DataSource = dt
            rptCCAAgreements.DataBind()
            CO.Dispose()

        ElseIf Mode = CustomerPageMode.Notes Then

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_notes_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptNotes.DataSource = dt
            rptNotes.DataBind()
            CO.Dispose()


        ElseIf Mode = CustomerPageMode.Appointments Then
            
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_appointments_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            CO.Parameters.AddWithValue("@status", "Open")
            Dim dt As DataTable = CDC.ReadDataTable(CO, 0)
            rptScheduledAppointments.DataSource = dt
            rptScheduledAppointments.DataBind()
            CO.Dispose()

            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "rsp_appointments_lookup_organisationfk"
            CO.Parameters.AddWithValue("@organisation_fk", SM.mainid)
            CO.Parameters.AddWithValue("@status", "Closed")
            dt = CDC.ReadDataTable(CO, 0)
            rptArchivedAppointments.DataSource = dt
            rptArchivedAppointments.DataBind()
            CO.Dispose()


        End If



    End Sub


    Private Sub SaveContol(ByVal primary As Boolean)
        Dim resDet As Boolean = True
        If primary = True Then
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.mainid)
        Else
            resDet = cc.Save(SM.currentuser, SM.actualuser, SM.subid)
        End If

        If Not (resDet) Then
            Dim errMsg() As String = cc.LastError.Message.Split((vbCr & ":").ToCharArray())
            If errMsg.Length > 1 Then
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".<br />" & "Please check the " & errMsg(3) & " field; " & errMsg(1) & "');")
            Else
                AddNotice("setError('" & "This record has not been saved because the " & errMsg(0).ToLower() & ".');")
            End If
        Else
            AddNotice("setNotice('Record Saved');")
            Dim filtered As New NameValueCollection(Request.QueryString)
            Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
            nameValueCollection.Remove("apk")
            nameValueCollection.Remove("apt")
            Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString + "&r=Record+Saved"
            Response.Redirect(url)
            RefreshList()
            PageLoad()
        End If
    End Sub
    Private Sub CancelContol()
        Dim filtered As New NameValueCollection(Request.QueryString)
        Dim nameValueCollection As Object = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())
        nameValueCollection.Remove("spk")
        Dim url As String = HttpContext.Current.Request.Path + "?" + nameValueCollection.ToString
        Response.Redirect(url)
    End Sub
    Private Sub SetControl(ByVal controlpath As String, ByVal TargetControl As Control, ByVal TargetButton As Control, ByVal isMain As Boolean, ByVal pk As Integer)
        'If TargetButton.ID.ToString Then

        'End If
        TargetControl.Controls.Clear()
        cc = LoadControl(controlpath)
        cc.CDC = CDC
        cc.SM = SM
        If isMain Then
            cc.Read(SM.currentuser, SM.actualuser, SM.mainid)
            TargetControl.Controls.Add(cc)
            TargetButton.Visible = True
            RefreshList()
        Else
            cc.Read(SM.currentuser, SM.actualuser, pk)
            If Request.QueryString("spk") Is Nothing And Request.QueryString("npk") Is Nothing And Request.QueryString("mpk") Is Nothing And Request.QueryString("apk") Is Nothing Then
                TargetControl.Controls.Clear()
                TargetButton.Visible = False
                RefreshList()
            Else
                TargetControl.Controls.Add(cc)
                TargetButton.Visible = True
                RefreshList()
            End If
        End If


    End Sub


    Private Sub InitCustomer(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwCustomer)
        CustomerNav.Visible = True

        RefreshList()
        SetControl("~/controls/organisations_ctl.ascx", OrganisationsControl, OrganisationsButton, True, SM.mainid)
    End Sub
    Private Sub initSiteInformation(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwSiteInformation)
        CustomerNav.Visible = False
        RefreshList()
    End Sub
    Private Sub InitMeterInformation(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwMeterInformation)
        SetControl("~/controls/meterpoints_ctl.ascx", MeterInformationControl, MeterInformationButton, False, mpk)
        RefreshList()
    End Sub
    Private Sub InitServiceAgreements(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwServiceAgreements)
        CustomerNav.Visible = False
        RefreshList()
    End Sub
    Private Sub InitContractInformation(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwContractInformation)
        CustomerNav.Visible = False
        RefreshList()
    End Sub
    Private Sub InitCCAAgreements(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwCCAAgreements)
        CustomerNav.Visible = False
        RefreshList()
    End Sub
    Private Sub InitAppointments(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwAppointments)
        RefreshList()
        If apt = 1 Then ' site visit
            SetControl("~/controls/custom/custom_appointments_visit_ctl.ascx", AppointmentControl, AppointmentButton, False, apk)
            CustomerNav.Visible = False
            RefreshList()
        ElseIf apt = 2 Then ' phone call
            SetControl("~/controls/custom/custom_appointments_phone_ctl.ascx", AppointmentControl, AppointmentButton, False, apk)
            CustomerNav.Visible = False
            RefreshList()
        Else

        End If


    End Sub
    Protected Sub btnSaveAppointment_Click(sender As Object, e As System.EventArgs) Handles btnSaveAppointment.Click
        SaveContol(False)
    End Sub
    Protected Sub btnCancelAppointment_Click(sender As Object, e As System.EventArgs) Handles btnCancelAppointment.Click
        CancelContol()
    End Sub
   


    Private Sub InitNotes(ByVal postback As Boolean)
        mvDetail.SetActiveView(vwNotes)
        SetControl("~/controls/custom/customernotes_ctl.ascx", NotesControl, NotesButton, False, npk)
        CustomerNav.Visible = False
        RefreshList()
    End Sub
    Protected Sub btnSaveNotes_Click(sender As Object, e As System.EventArgs) Handles btnSaveNotes.Click
        SaveContol(False)
    End Sub
    Protected Sub btnCancelNotes_Click(sender As Object, e As System.EventArgs) Handles btnCancelNotes.Click
        CancelContol()
    End Sub

    Public Function SelectedItemName() As String
        Dim itemclass As String = ""
        Select Case Mode
            Case CustomerPageMode.Customer
                itemclass = ""
            Case Else
                Dim s As New sites(SM.currentuser, SM.actualuser, SM.subid, CDC)
                itemclass &= "for - " & s.sitename
        End Select

        Return itemclass
    End Function

    '================================
    '   "Spaghetti" Procs
    '================================
    Public Function LIi(ByVal item As String) As String
        Dim itemclass As String = ""
        Select Case Mode
            Case CustomerPageMode.Customer : itemclass = IIf(item = "customer", "selected", "")
            Case CustomerPageMode.SiteInformation : itemclass = IIf(item = "site", "selected", "")
            Case CustomerPageMode.MeterInformation : itemclass = IIf(item = "meter", "selected", "")
            Case CustomerPageMode.ServiceAgreements : itemclass = IIf(item = "serviceagreements", "selected", "")
            Case CustomerPageMode.ContractInformation : itemclass = IIf(item = "contract", "selected", "")
            Case CustomerPageMode.CCAAgreements : itemclass = IIf(item = "cca", "selected", "")
            Case CustomerPageMode.Appointments : itemclass = IIf(item = "appointments", "selected", "")
            Case CustomerPageMode.Notes : itemclass = IIf(item = "notes", "selected", "")
            Case CustomerPageMode.Search : itemclass = IIf(item = "search", "selected", "")

        End Select
        Return itemclass
    End Function
    Public Function IsStaff() As Boolean
        Return Security.GroupMember(SM.currentuser, SystemGroups.Staff, CDC)
    End Function
    Public Function CustomerStatus() As Integer
        Dim _return As Integer = -1

        Dim c As New organisations(SM.currentuser, SM.actualuser, SM.mainid, CDC)
        Try
            _return = c.custorprospect
        Catch ex As Exception
        End Try

        Return _return
    End Function

    '================================
    '   "Handled" Procs
    '================================
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMode()
        PageLoad()


    End Sub

    Public Function returnselect(ByVal activestatus As String) As String
        Dim _return As String = ""

        If activestatus = "Active" Then
            _return = ""
        ElseIf activestatus = "Historic" Then
            _return = "background-color:#B10043;color:#FFFFFF;"
        Else
            _return = "background-color:#E47F00;"
        End If
        Return _return
        
    End Function


End Class
