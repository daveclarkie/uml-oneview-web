
Partial Class design_elements_navigation_external
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM

        Integer.TryParse(Request.QueryString("debug"), debug)
        If debug = 1 Then Me.lbl_hostpath.Text = AppDomain.CurrentDomain.BaseDirectory

        Me.lbl_hostpath.Text &= System.Web.HttpContext.Current.Server.MapPath("DataCommon.dll")

    End Sub
    Dim hostlist As String = "127.0.0.1,192.168.69.6,192.168.69.68,192.168.69.69,192.168.69.96,78.158.51.104,192.168.69.5,local,::"
    Dim hostpath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
    Dim hosts() As String = hostlist.Split(",")
    Dim debug As Integer = 1

    Public ReadOnly Property allowedRemoteHost() As Boolean
        Get
            Dim r As Boolean = False
            Dim h As String
            For Each h In hosts
                r = r Or Request.UserHostAddress.StartsWith(h)
            Next
            Return r
        End Get
    End Property

    Public ReadOnly Property isTestSystem() As Boolean
        Get
            Dim r As Boolean = False
            Dim Host As String = Request.Url.ToString.Split("/".ToCharArray(), 5, StringSplitOptions.RemoveEmptyEntries)(1)
            Host = Host.ToLower.Split(".".ToCharArray)(0)

            Select Case True
                Case Host = "test"
                    r = True
                Case Host.StartsWith("localhost")
                    r = True
            End Select
            Return r
        End Get
    End Property

    Private Sub doLogin(ByVal UID As Integer)
        Dim loginOK As Boolean = False
        Dim CO As New SqlClient.SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_Login1"
        CO.Parameters.AddWithValue("@uid", UID)
        Dim dr As DataRow = CDC.ReadDataRow(CO)
        If Not dr Is Nothing Then
            SM.currentuser = dr("user_pk")
            SM.actualuser = dr("user_pk")
            Dim u As New userloginlogs(SM.currentuser, SM.actualuser, CDC)
            u.attemptresult = True
            u.creator_fk = SM.CurrentUser
            u.editor_fk = SM.actualuser
            u.ipaddress = Request.UserHostAddress
            u.useragentstring = Request.UserAgent
            u.rowstatus = 0
            u.username = SM.UserName(SM.currentuser)
            u.probableuser_fk = SM.currentuser
            u.userpassw = "Auto"
            u.Save()
            loginOK = True
        End If
        dr = Nothing
        CO = Nothing
        If loginOK Then
            Local.Bounce(SM, Response, "~/core/default.aspx", "loginok")
        End If
    End Sub

    Protected Sub clickLogin(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim user_pk As Integer = 0
        If Not Integer.TryParse(e.CommandArgument, user_pk) Then Exit Sub
        doLogin(user_pk)
    End Sub
End Class
