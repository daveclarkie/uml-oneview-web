Partial Class notifications_ctl
   Inherits DaveControl
   Public PSM As MySM
   Public Sub InitControl() Handles MyBase.InitSpecificControl
       ' Place any page specific initialisation code here
       PSM = SM
       CtlInit()
   End Sub
   Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
      ' Place any page specific initialisation code here
   End Sub
   Public WithEvents onotifications As Objects.notifications
   Dim CO As SqlClient.SqlCommand
   Dim _CurrentPk as Integer=0
   Dim _Status as Integer=0
   Public LastError As Exception
   Public ReadOnly Property CurrentPk() as Integer
        Get
            Return _CurrentPk
        End Get
   End Property
   Public Property Status() as Integer
        Get
            Return _Status
        End Get
        Set (ByVal value As integer)
            _Status=value
            SetControlStatus
        End Set
   End Property
   Dim _read as boolean=false
   Public Sub ctlInit()
        If _read Then Exit Sub
            Me.priority.Text="0"
        _read=True
   End Sub
Public Sub Read(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0)
  ctlInit()
If onotifications is Nothing then
onotifications = new Objects.notifications(currentuser,actualuser,pk,CDC)
End If
Me.title.Text=onotifications.title
Me.message.Text=onotifications.message
Me.priority.Text=onotifications.priority
Me.priority.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
Me.priority.Attributes.Add("onBlur", "clearNotice()")
Me.approved.Checked=onotifications.approved
_CurrentPk=onotifications.notification_pk
Status=onotifications.rowstatus
_read=True
End Sub
Public Function Save(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
If onotifications is Nothing then
onotifications = new Objects.notifications(currentuser,actualuser,pk,CDC)
End If
Dim result as boolean=false
Try
onotifications.title=Me.title.Text
onotifications.message=Me.message.Text
onotifications.priority=Me.priority.Text
onotifications.approved=Me.approved.Checked
result=onotifications.Save()
if not result then throw onotifications.LastError
_CurrentPk=onotifications.notification_pk
Catch Ex as Exception
result=False
LastError=Ex
End Try
Status=onotifications.rowstatus
Return result
End Function
Public Function Enable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotifications is Nothing then
   onotifications = new Objects.notifications(currentuser,actualuser,pk,CDC)
  End If
  onotifications.rowstatus=0
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Disable(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotifications is Nothing then
   onotifications = new Objects.notifications(currentuser,actualuser,pk,CDC)
  End If
  onotifications.rowstatus=1
  Return Save(currentuser,actualuser,pk)
End Function
Public Function Delete(byval currentuser as integer,byval actualuser as integer,optional byval pk as integer=0) as boolean
  If onotifications is Nothing then
   onotifications = new Objects.notifications(currentuser,pk,CDC)
  End If
  onotifications.rowstatus=2
  Return Save(currentuser,actualuser,pk)
End Function
  Public Sub SetControlStatus()
  Dim RL as Integer=cca.common.Security.ReadLevel(SM.CurrentUser,Me.GetType().ToString,CDC)
  Dim WL as Integer=cca.common.Security.WriteLevel(SM.CurrentUser,me.GetType().ToString,CDC)
  Me.title.Enabled=IIF(WL>=0,True,False)
  Me.blktitle.Visible=IIF(RL>=0,True,False)
  Me.message.Enabled=IIF(WL>=0,True,False)
  Me.blkmessage.Visible=IIF(RL>=0,True,False)
  Me.priority.Enabled=IIF(WL>=0,True,False)
  Me.blkpriority.Visible=IIF(RL>=0,True,False)
  Me.approved.Enabled=IIF(WL>=0,True,False)
  Me.blkapproved.Visible=IIF(RL>=0,True,False)
  End Sub
End Class

