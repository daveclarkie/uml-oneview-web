<%@ WebHandler Language="VB" Class="functions_removemeterpoint" %>
Public Class functions_removemeterpoint
    Implements IHttpHandler, IRequiresSessionState
    Dim CDC As DataCommon
    Dim SM As MySM
    Dim Request As HttpRequest
    Dim Response As HttpResponse

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Request = context.Request
        Response = context.Response
        CDC = New DataCommon
        SM = New MySM(context.Session.SessionID, Request, CDC)
        Page_Load()
    End Sub

    Public Sub VerifyRenderingInServerForm(ByRef control As Control)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub Page_Load()
        Dim CRQ As Object = Request.QueryString
        Dim Obj As objects.agreementmeterpoints
        Dim Msg as string="OK"
        Try
            Obj = New objects.agreementmeterpoints(SM.currentuser, SM.actualuser, Integer.Parse(CRQ("pk")), CDC)
            If Not Obj.Disable() then
                Msg="FAIL:" & Obj.LastError.Message
            End If
        Catch LE as Exception
            Msg="FAIL:" & LE.Message
        End Try
        Response.Write(Msg)
    End Sub

End Class
