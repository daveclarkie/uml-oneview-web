Partial Class suppliers_ctl
    Inherits DaveControl
    Public PSM As MySM
    Public Sub InitControl() Handles MyBase.InitSpecificControl
        ' Place any page specific initialisation code here
        PSM = SM
        CtlInit()
    End Sub
    Public Sub CompleteControl() Handles MyBase.CompleteSpecificControl
        ' Place any page specific initialisation code here
    End Sub
    Public WithEvents osuppliers As Objects.suppliers
    Dim CO As SqlClient.SqlCommand
    Dim _CurrentPk As Integer = 0
    Dim _Status As Integer = 0
    Public LastError As Exception
    Public ReadOnly Property CurrentPk() As Integer
        Get
            Return _CurrentPk
        End Get
    End Property
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
            SetControlStatus()
        End Set
    End Property
    Dim _read As Boolean = False
    Public Sub ctlInit()
        If _read Then Exit Sub
        Me.supply_electricity.Text = "0"
        Me.supply_gas.Text = "0"
        _read = True
    End Sub
    Public Sub Read(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0)
        ctlInit()
        If osuppliers Is Nothing Then
            osuppliers = New Objects.suppliers(currentuser, actualuser, pk, CDC)
        End If
        Me.supplier_name.Text = osuppliers.supplier_name
        Me.supply_electricity.Text = osuppliers.supply_electricity
        Me.supply_electricity.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.supply_electricity.Attributes.Add("onBlur", "clearNotice()")
        Me.supply_gas.Text = osuppliers.supply_gas
        Me.supply_gas.Attributes.Add("onFocus", "setNotice('Format: (n)n eg 4736')")
        Me.supply_gas.Attributes.Add("onBlur", "clearNotice()")
        _CurrentPk = osuppliers.supplier_pk
        Status = osuppliers.rowstatus
        _read = True
    End Sub
    Public Function Save(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osuppliers Is Nothing Then
            osuppliers = New Objects.suppliers(currentuser, actualuser, pk, CDC)
        End If
        Dim result As Boolean = False
        Try
            osuppliers.supplier_name = Me.supplier_name.Text
            osuppliers.supply_electricity = Me.supply_electricity.Text
            osuppliers.supply_gas = Me.supply_gas.Text
            result = osuppliers.Save()
            If Not result Then Throw osuppliers.LastError
            _CurrentPk = osuppliers.supplier_pk
        Catch Ex As Exception
            result = False
            LastError = Ex
        End Try
        Status = osuppliers.rowstatus
        Return result
    End Function
    Public Function Enable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osuppliers Is Nothing Then
            osuppliers = New Objects.suppliers(currentuser, actualuser, pk, CDC)
        End If
        osuppliers.rowstatus = 0
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Disable(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osuppliers Is Nothing Then
            osuppliers = New Objects.suppliers(currentuser, actualuser, pk, CDC)
        End If
        osuppliers.rowstatus = 1
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Function Delete(ByVal currentuser As Integer, ByVal actualuser As Integer, Optional ByVal pk As Integer = 0) As Boolean
        If osuppliers Is Nothing Then
            osuppliers = New Objects.suppliers(currentuser, pk, CDC)
        End If
        osuppliers.rowstatus = 2
        Return Save(currentuser, actualuser, pk)
    End Function
    Public Sub SetControlStatus()
        Dim RL As Integer = cca.common.Security.ReadLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Dim WL As Integer = cca.common.Security.WriteLevel(SM.CurrentUser, Me.GetType().ToString, CDC)
        Me.supplier_name.Enabled = IIF(WL >= 0, True, False)
        Me.blksupplier_name.Visible = IIF(RL >= 0, True, False)
        Me.supply_electricity.Enabled = IIF(WL >= 0, True, False)
        Me.blksupply_electricity.Visible = IIF(RL >= 0, True, False)
        Me.supply_gas.Enabled = IIF(WL >= 0, True, False)
        Me.blksupply_gas.Visible = IIF(RL >= 0, True, False)
    End Sub
End Class

