<%@ Control Language="VB" AutoEventWireup="false" CodeFile="sites_ctl.ascx.vb" Inherits="sites_ctl" %>
<h5>sites</h5>
<ul class='formcontrol'>
<li runat="server" id="blkorganisation_fk">
<span class='label'>organisation_fk</span>
<asp:DropDownList ID="organisation_fk" runat="server" cssClass="input_ddl" />
</li>
<li runat="server" id="blksitename">
<span class='label'>sitename</span>
<asp:TextBox EnableViewState="false" ID="sitename" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkdeadoralive">
<span class='label'>deadoralive</span>
<asp:TextBox EnableViewState="false" ID="deadoralive" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkContactName">
<span class='label'>ContactName</span>
<asp:TextBox EnableViewState="false" ID="ContactName" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkPhone_1">
<span class='label'>Phone_1</span>
<asp:TextBox EnableViewState="false" ID="Phone_1" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkFaxNumber">
<span class='label'>FaxNumber</span>
<asp:TextBox EnableViewState="false" ID="FaxNumber" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkAccmgrid">
<span class='label'>Accmgrid</span>
<asp:TextBox EnableViewState="false" ID="Accmgrid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkAddress_1">
<span class='label'>Address_1</span>
<asp:TextBox EnableViewState="false" ID="Address_1" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkAddress_2">
<span class='label'>Address_2</span>
<asp:TextBox EnableViewState="false" ID="Address_2" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkAddress_3">
<span class='label'>Address_3</span>
<asp:TextBox EnableViewState="false" ID="Address_3" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkAddress_4">
<span class='label'>Address_4</span>
<asp:TextBox EnableViewState="false" ID="Address_4" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkCountyID">
<span class='label'>CountyID</span>
<asp:TextBox EnableViewState="false" ID="CountyID" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkintCountryFK">
<span class='label'>intCountryFK</span>
<asp:TextBox EnableViewState="false" ID="intCountryFK" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkPcode">
<span class='label'>Pcode</span>
<asp:TextBox EnableViewState="false" ID="Pcode" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkEmail">
<span class='label'>Email</span>
<asp:TextBox EnableViewState="false" ID="Email" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkStatusID">
<span class='label'>StatusID</span>
<asp:TextBox EnableViewState="false" ID="StatusID" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCustomersiteref">
<span class='label'>Customersiteref</span>
<asp:TextBox EnableViewState="false" ID="Customersiteref" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkTenderstdt">
<span class='label'>Tenderstdt</span>
<asp:TextBox EnableViewState="false" ID="Tenderstdt" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkTenderlastdt">
<span class='label'>Tenderlastdt</span>
<asp:TextBox EnableViewState="false" ID="Tenderlastdt" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blktenderprtdate">
<span class='label'>tenderprtdate</span>
<asp:TextBox EnableViewState="false" ID="tenderprtdate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blktenderclsddate">
<span class='label'>tenderclsddate</span>
<asp:TextBox EnableViewState="false" ID="tenderclsddate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkkVA">
<span class='label'>kVA</span>
</li>
<li runat="server" id="blkNotes">
<span class='label'>Notes</span>
<asp:TextBox EnableViewState="false" ID="Notes" TextMode="Multiline" runat="server" cssClass="input_box" />
</li>
<li runat="server" id="blkSameasbillingadress">
<span class='label'>Sameasbillingadress</span>
<asp:TextBox EnableViewState="false" ID="Sameasbillingadress" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blktendbenchmark">
<span class='label'>tendbenchmark</span>
<asp:TextBox EnableViewState="false" ID="tendbenchmark" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blktypeofop">
<span class='label'>typeofop</span>
<asp:TextBox EnableViewState="false" ID="typeofop" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkvoltageID">
<span class='label'>voltageID</span>
<asp:TextBox EnableViewState="false" ID="voltageID" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkMaxDemand">
<span class='label'>MaxDemand</span>
<asp:TextBox EnableViewState="false" ID="MaxDemand" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodelecneg">
<span class='label'>prodelecneg</span>
<asp:TextBox EnableViewState="false" ID="prodelecneg" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodgasneg">
<span class='label'>prodgasneg</span>
<asp:TextBox EnableViewState="false" ID="prodgasneg" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodutilisys">
<span class='label'>produtilisys</span>
<asp:TextBox EnableViewState="false" ID="produtilisys" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodCRC">
<span class='label'>prodCRC</span>
<asp:TextBox EnableViewState="false" ID="prodCRC" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkprodWater">
<span class='label'>prodWater</span>
<asp:TextBox EnableViewState="false" ID="prodWater" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkfederationid">
<span class='label'>federationid</span>
<asp:TextBox EnableViewState="false" ID="federationid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkfuelid">
<span class='label'>fuelid</span>
<asp:TextBox EnableViewState="false" ID="fuelid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCSC">
<span class='label'>CSC</span>
<asp:TextBox EnableViewState="false" ID="CSC" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCSCdate">
<span class='label'>CSCdate</span>
<asp:TextBox EnableViewState="false" ID="CSCdate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkMDdate">
<span class='label'>MDdate</span>
<asp:TextBox EnableViewState="false" ID="MDdate" runat="server" cssClass="input_dtm" />
</li>
<li runat="server" id="blkFKsitegroupid">
<span class='label'>FKsitegroupid</span>
<asp:TextBox EnableViewState="false" ID="FKsitegroupid" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkDservices_optout">
<span class='label'>Dservices_optout</span>
<asp:TextBox EnableViewState="false" ID="Dservices_optout" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkBill_check_optout">
<span class='label'>Bill_check_optout</span>
<asp:TextBox EnableViewState="false" ID="Bill_check_optout" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkCCL_mgmt">
<span class='label'>CCL_mgmt</span>
<asp:TextBox EnableViewState="false" ID="CCL_mgmt" runat="server" cssClass="input_num"  />
</li>
<li runat="server" id="blkvarMarker">
<span class='label'>varMarker</span>
<asp:TextBox EnableViewState="false" ID="varMarker" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
<li runat="server" id="blkLat">
<span class='label'>Lat</span>
<asp:TextBox EnableViewState="false" ID="Lat" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkLong">
<span class='label'>Long</span>
<asp:TextBox EnableViewState="false" ID="Long" runat="server" cssClass="input_dbl" />
</li>
<li runat="server" id="blkvarSiteFriendlyName">
<span class='label'>varSiteFriendlyName</span>
<asp:TextBox EnableViewState="false" ID="varSiteFriendlyName" TextMode="Singleline" runat="server" cssClass="input_str" />
</li>
</ul>

