Partial Class corematrix_reports_matrix1report
    Inherits DavePage : Public PSM As MySM
    Dim CO As SqlCommand
    Public dt As DataTable

    Public Sub InitPage() Handles MyBase.InitSpecificPage
        ' Place any page specific initialisation code here
        PSM = SM
    End Sub
    Public Sub CompletePage() Handles MyBase.CompleteSpecificPage
        ' Place any page specific initialisation code here
        Dim matrix As Integer = -1
        Integer.TryParse(Request.QueryString("m"), matrix)


        SM.matrix = matrix
        SM.Save()

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "rsp_matrix1report"
        CO.Parameters.AddWithValue("@matrix_pk", SM.matrix)
        dt = CDC.ReadDataTable(CO, 0)
        CO.Dispose()
        If dt.Rows.Count > 0 Then
            lblCustomer.Text = dt.Rows(0).Item("customer")
            lblMatrixType.Text = dt.Rows(0).Item("typename")
        End If
        lblReportGenerated.Text = Now.ToString("dd MMM yyyy HH:mm:ss")


    End Sub

    Public Sub Search()

    End Sub

    '================================
    '   "Validation" Procs
    '================================

    Private Function LimitSearchTerms(ByVal original As String) As String
        CDC.Sanitize(original)
        Return original
    End Function

    Private Function LimitSearchTermsNumeric(ByVal original As String) As Integer
        CDC.Sanitize(original)
        Dim i As Integer = 0
        If Integer.TryParse(original, i) Then
            Return i
        Else
            Return 0
        End If
    End Function


    '================================
    '   Page Specific Events
    '================================


End Class
