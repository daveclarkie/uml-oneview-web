﻿<%@ Page Title="" Language="VB" MasterPageFile="~/design/masters/systems.master" AutoEventWireup="false" CodeFile="departments.aspx.vb" Inherits="backend_cm_departments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
<h5>Current Departments</h5>
<asp:Repeater ID="rptCMDepartments" runat="server">
    <HeaderTemplate><table>
    <tr>
        <td style="width:80px;">
            
        </td>
        <td style="width:100px;">
            <h4>Departments</h4>
        </td>
        <td style="width:80px;">
            <h4>Owner</h4>
        </td>
        <td style="width:30px;">
            
        </td>
        <td></td>
    </tr>
    </HeaderTemplate> 
    <ItemTemplate>
        <tr id='node_<%# Container.DataItem("department_pk") %>'>
            <td>
                <a href='departments.aspx?pk=<%# Container.DataItem("department_pk")%>'><h6>Edit</h6></a>
            </td>
            <td>
                <%# Container.DataItem("departmentname")%>
            </td>
            <td>
                <%# Container.DataItem("ownername")%>
            </td>
            <td>
                <a class="remove" title="Remove"  href='javascript:removeDepartment(<%# Container.DataItem("department_pk") %>)'>X</a>
            </td>
            
        </tr>
    </ItemTemplate> 
    <FooterTemplate>
    </table>    
    <table>
        <tr style="height:5px;">
        </tr>
        <tr>
            <td style="width:100px">
                <a href='departments.aspx?pk=-1'><h6>New Department</h6></a> 
            </td>
        </tr>
    </table>
    </FooterTemplate> 
</asp:Repeater>
             
<asp:PlaceHolder ID="DepartmentControl" runat="server" />
<table id="DepartmentButton" runat="server" style="width:100%;">
    <tr style="height:10px;">
                            
    </tr>
    <tr>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton runat="server" ID="btnSaveDepartment" Text="<h6>Save</h6>" />
        </td>
        <td style="width:60%;"></td>
        <td style="text-align:center; width:20%;">
            <asp:Linkbutton ID="btnCancelDepartment" runat="server" Text="<h4>Cancel</h4>" />
        </td>
    </tr>
</table> 

</asp:Content>